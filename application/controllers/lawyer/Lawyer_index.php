<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Lawyer_index extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Users_model');
		$this->load->model('Central_model');
		$this->load->model('admin/Leads_model');
    }
    public function register()
    {
        $data = array();
        $this->load->model('admin/users_model');
		if($this->input->method(TRUE) == 'POST') {
			$this->load->library('form_validation');
			$path = './uploads';
			$data               = $this->input->post();
			$rules = array(
				[
					'field' => 'name',
					'label' => 'First Name',
					'rules' => (isset($data['name']) ? 'trim|required' : ''),
				],
				[
					'field' => 'lname',
					'label' => 'Last Name',
					'rules' => (isset($data['lname']) ? 'trim|required' : ''),
				],
				[
					'field' => 'email',
					'label' => 'Email',
					'rules' => (isset($data['email']) ? 'trim|required|valid_email|callback_check_email' : ''),
				],
				[
					'field' => 'phone',
					'label' => 'Phone',
					'rules' => (isset($data['phone']) ? 'trim|required' : ''),
				],
				[
					'field' => 'law_firms',
					'label' => 'Law Firm',
					'rules' => (isset($data['law_firms']) ? 'trim|required' : ''),
				],
				[
					'field' => 'case_type[]',
					'label' => 'Case Type',
					'rules' => 'trim|required',
				],
				[
					'field' => 'jurisdiction[]',
					'label' => 'Jurisdiction',
					'rules' => 'trim|required',
				],
				[
					'field' => 'address',
					'label' => 'Address',
					'rules' => (isset($data['address']) ? 'trim|required' : ''),
				],
				[
					'field' => 'city',
					'label' => 'City',
					'rules' => (isset($data['city']) ? 'trim|required' : ''),
				],
				[
					'field' => 'state',
					'label' => 'State',
					'rules' => (isset($data['state']) ? 'trim|required' : ''),
				],
				[
					'field' => 'zipcode',
					'label' => 'Zip Code',
					'rules' => (isset($data['zipcode']) ? 'trim|required' : ''),
				],
				[
					'field' => 'initial_setup_charge',
					'label' => 'Initial Setup Charge',
					'rules' => (isset($data['initial_setup_charge']) ? 'trim|required' : ''),
				],
				[
					'field' => 'fee_type',
					'label' => 'Fee Type',
					'rules' => (isset($data['fee_type']) ? 'trim|required' : ''),
				],
				[
					'field' => 'law_firm_fee',
					'label' => 'Law Firm Fee',
					'rules' => (isset($data['law_firm_fee']) ? 'trim|required' : ''),
				],
				[
					'field' => 'image',
					'label' => 'Image',
					'rules' => 'callback_upload_image['.$path.']',
				],
				[
					'field' => 'active',
					'label' => 'Active',
					'rules' => (isset($data['active']) ? 'trim|required' : ''),
				],
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
				$this->load->helper('string');
				$data['role']        = '2';
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['password']   = sha1($this->input->post('password'));
				$data['unique_code'] = random_string('alnum',20);
				$data['image']      = $_FILES['image']['name'];
				unset($data['submit']);
				$LawyerAddTemplate   = $this->Leads_model->template(1);
				$name     = $this->input->post('name');
				$lname    = $this->input->post('lname');
				$fullname = $name . " " . $lname;
				$username = $this->input->post('name');
				$password = $this->input->post('password');
				$subject  = $LawyerAddTemplate->subject;
				$message = $LawyerAddTemplate->content;
				$message = str_replace("{email}", $data['email'], $message);
				$message = str_replace("{password}", $password, $message);
				$message = str_replace("{name}", $name, $message);
				$message = str_replace("{username}", $fullname, $message);
				$message = str_replace("{organization name}", "Pride legal", $message);
				$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
				$message = str_replace("{rootpath}", base_url(), $message);
				$message = str_replace("{image}", base_url().'assets/admin/images/pride_legal_image.png', $message);
				$message = str_replace("{link}", base_url().'admin/ResetPassword/reset/lawyer/'.$data['unique_code'], $message);
				$from               = $LawyerAddTemplate->email_from;
				$name          = $this->config->item('project_title');
				$to                 = $data['email'];
				/*Send Email*/
				send_email($to,$from,$name,$subject,$message);
				/*Send SMS*/
				$sms_message = "Hi ".$data['name']." ".$data['lname'].", \n".$subject.".\nYour Email: ".$data['email']."\nTemporary password: ".$password;
				sms($data['phone'], $sms_message);
				if ($data['role'] == 2) {
					$data['case_type']    = implode(",", $this->input->post('case_type'));
					$data['jurisdiction'] = implode(",", $this->input->post('jurisdiction'));
				} //$data['role'] == 2
				$result = $this->Users_model->save($data);
				if ($result) {
					$lawyer_data                     = array();
					$lawyer_all_data                 = $this->Users_model->fetchRow($result);
					$lawyer_data['lawyer_id']        = $lawyer_all_data->id;
					$lawyer_data['amount']           = $lawyer_all_data->initial_setup_charge;
					$lawyer_data['description']           = 'Intial Setup Charge';
					$lawyer_data['transaction_date'] = date('Y-m-d H:i:s');
					$lawyer_data['bill_type'] = 1;
					$this->Users_model->saveLawyerInitialSetupCharge($lawyer_data);
					$this->session->set_flashdata('message', '<p class="message_success">Lawyer Registered successfully. You will recieve an email after admin approvel. Please remember your email and password</p>');
					redirect($this->config->item('base_url') . 'lawyer/lawyer_index/register/2');
				} //$result
			}
		}
        $data['law_firms']          = $this->Users_model->fetchAllLawFirms();
		$data['case_type']          = $this->Users_model->fetchAllCaseTpe();
		$data['jurisdictions']      = $this->Users_model->fetchAllJurisdictions();
		$data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
        $data['class_leads']        = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('lawyer/register');
        $this->load->view('layouts/admin/footer', $data);
    }
	public function check_email($email, $id= Null)
	{
		if($id)
			$count = $this->Central_model->count_rows("users", array("email" => $email), array("id" => $id));
		else 
			$count = $this->Central_model->count_rows("users", array("email" => $email));
		
		if($count > 0)
		{
			$this->form_validation->set_message('check_email', "User email already exists. Duplicates not allowed!");
			return FALSE;
		}
		
		return TRUE;
	}
	public function upload_image($name, $path=Null)
	{
		$config['upload_path'] = $path;
		$config['max_size'] = 1024 * 10;
		$config['allowed_types'] = 'gif|png|jpg|jpeg';
		//$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		if(isset($_FILES['image']) && !empty($_FILES['image']['name']))
		{
			if($this->upload->do_upload('image'))
			{
				$upload_data = $this->upload->data();
				$_POST['image'] = $upload_data['file_name'];
				return TRUE;
			}
			else
			{
				$this->form_validation->set_message('upload_image', "The File type you are uploading is not allowed!");
				return FALSE;
			}
		}
		return TRUE;
	}
}
