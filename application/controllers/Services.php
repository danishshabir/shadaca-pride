<?php



defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('Services_model');
		$this->load->model('admin/Forgot_password_model'); 
        $this->load->model('admin/Leads_model');
        $this->load->model('admin/Users_model');
			

		}
		
		
	public function login(){
		     
			$response = array(); 
			 
		   $result = $this->Services_model->login($this->input->get('username'), sha1($this->input->get('password')));
			
			
		    if ($result) 
			{
                                                                  
              $response = array('logged_in' => TRUE, 'id' => $result->id, 'role' => $result->role, 'message'=>'Logged in successfully.');
				      
            } 
			
			else 
			{	
				 $response = array('logged_in' => FALSE, 'message'=>'Invalid username or password.');
	
            }
			echo json_encode($response);
			exit;
	}
	
	public function forgotpassword(){
		$response = array();
		$email = $this->input->get('email');
		$checkUser = $this->Services_model->checkEmail($email);
		if($checkUser)
		{
		
			$data['unique_code'] = generateRandomString("25");
			
			$this->Forgot_password_model->unique_str($data, $email);
			
			$res = $this->Forgot_password_model->fetchByCode($data['unique_code']);
			
			$config = Array(
			  'protocol' => 'smtp',
			  'smtp_host' => 'ssl://smtp.googlemail.com',
			  'smtp_port' => 465,
			  'smtp_user' => 'usmanmazhar@astutesol.com', // change it to yours
			  'smtp_pass' => 'usman123!@#', // change it to yours
			  'mailtype' => 'html',
			  'charset' => 'iso-8859-1',
			  'wordwrap' => TRUE
			);
			
			$message = 'Hi '.$res->name.', <br /><br /> You Can change your password using the following URL: '.base_url().'admin/forgotpassword/index/'.$data['unique_code'].'<br /><br />Thank you.';
			$this->load->library('email', $config);
			$this->email->set_mailtype('html');
			$this->email->set_newline("\r\n");
			$this->email->from('usmanmazhar@astutesol.com'); // change it to yours
			$this->email->to($email);// change it to yours
			$this->email->subject('Forgot Password');
			$this->email->message($message);
			if($this->email->send())
			{
				$response = array('success'=>'1', 'message' => 'A link is sent to your email. You can change your password using that link.');
			}
			else
			{
				$response = array('success'=>'0', 'message' => 'Email not sent please try again later');
			}
		}
		else
		{
			$response = array('success'=>'0', 'message' => 'Invalid email no such user exist');
		}
		echo json_encode($response);
		exit;
	}
 
 
    /**
     * Services::migrate_lead()
     * 
     * @return void
     */
    public function migrate_lead($form_id) {
        $data = $this->input->post();
        
        /*$save = array();
        $save['first_name'] = $data[0];
        $save['email'] = $data[1];
        $save['work_phone'] = $data[2];
        $save['city'] = $data[3];
        $state = $this->db->query("SELECT id AS state FROM jurisdiction_states WHERE state_short_name = '".strtoupper($data[4])."'")->row()->state;
        $state = $state ? $state : 5;
        $save['state'] = $state;
        $save['zip'] = $data[5];
        $save['case_description'] = htmlspecialchars($data[6]);
        $save['lead_status'] = 4;
        $save['created_at'] = date('Y-m-d H:i:s');*/
        
        
        if($form_id==3)
        {
            $save = array();
            $name_array = explode(" ", $data[0]);
            $first_name = "";
            $last_name = "";
            $iCount = 1;
            foreach($name_array as $key) {    
                if($iCount < count($name_array) )
                {
                    if($first_name != "")
                    {
                        $first_name .= " " . $key;
                    }
                    else
                    {
                        $first_name .= $key;
                    }
                    
                }
                else
                {
                    $last_name .= $key;
                }
                $iCount++;
            }
            $save['first_name'] = $first_name;
            $save['last_name'] = $last_name;
            $save['work_phone'] = $data[1];
            $save['email'] = $data[2];
            $save['city'] = 'Irvine';
            $state = 5;
            $save['state'] = $state;
            $save['case_description'] = htmlspecialchars($data[3]);
            $save['lead_status'] = 4;
            $save['jurisdiction'] = '39'; //  CA - Los Angeles (6) 
            $save['market'] = '6'; //  6-Los Angeles/San Bernardino - CA 
            $save['created_at'] = date('Y-m-d H:i:s');
            
        }
        
        /*  Old code, commented out
        
        $save = array();
        $save['first_name'] = $data[0];
        $save['last_name'] = $data[1];
        $save['mobile'] = $data[2];
        $save['email'] = $data[3];
        $save['city'] = $data[4];
        $state = $this->db->query("SELECT id AS state FROM jurisdiction_states WHERE state_short_name = '".strtoupper($data[5])."'")->row()->state;
        $state = $state ? $state : 5;
        $save['state'] = $state;
        $save['case_description'] = htmlspecialchars($data[6]);
        $save['lead_status'] = 4;
        $save['jurisdiction'] = '39'; //  CA - Los Angeles (6) 
        $save['market'] = '6'; //  6-Los Angeles/San Bernardino - CA 
        $save['created_at'] = date('Y-m-d H:i:s');
        
        */
        $insert_id = $this->Leads_model->save($save);
        mail("anwar.shahid@gmail.com", "auto lead call", $this->db->last_query());
        // if lead is not saved we escape
        if(!$insert_id) return false;
        
        $template = $this->Leads_model->template(17);
        
        $subject = $template->subject;
        
        $message = $template->content;
        
        $from = $template->email_from;
        
        $message = str_replace("{project_title}", $this->config->item('project_title'), $message);
        $message = str_replace("{name}", $save['first_name'], $message);
        $message = str_replace("{email}", $save['email'], $message);
        $message = str_replace("{phone}", $save['mobile'], $message);
        $message = str_replace("{description}", $save['case_description'], $message);
        $message = str_replace("{lead_date}", date('m-d-Y h:i:s A', strtotime($save['created_at'])), $message);
        $message = str_replace("{link}", '<a href="'.base_url("admin/leads/edit/{$insert_id}").'">'.base_url("admin/leads/edit/{$insert_id}").'</a>', $message);
        
        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        $super_admin = $this->Leads_model->fetchSuperAdminRecord();
        $to = "anwar.shahid@gmail.com";//$super_admin->superAdminEmail;
        $cc = "shahid@astutesol.com";//$super_admin->secondAdminEmail;
        
        // Additional headers
        $headers .= "Cc: $cc" . "\r\n";
        $headers .= "From: ".$this->config->item('project_title')." <$from>" . "\r\n";

        mail($to, $subject, $message, $headers);
    }
		

}