<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

	function __construct() {
        parent::__construct();
       
    }
	
	//Main index function for the controller admin login
	//loding the admin login view
	public function index(){
		
       redirect('admin');
	}
	
		
}

/* End of file Index.php */
/* Location: ./application/controllers/admin/Index.php */