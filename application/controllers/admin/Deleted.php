<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Deleted extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('latest_url', current_url());
        chechUserSession();
        $this->load->model('admin/Leads_model');
		$this->load->model('Central_model');
    }
    public function index()
    {
        $data                  = array();
        $data['class_leads']   = 'active';
        $data['deleted_leads'] = $this->Central_model->select_all_array('deleted_leads', array());
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/leads/delete', $data);
        $this->load->view('layouts/admin/footer', $data); 
    }
    
    /**
     * Deleted::searchByCaseId()
     * 
     * @return void
     */
    public function searchByCaseId(){
        $case_id = $this->input->post('case_id');
        $html = '';
        $result = $this->db->query("SELECT * FROM `deleted_leads` WHERE RIGHT(`lead_id_number`, 5) LIKE '%$case_id%'")->result();
        foreach($result as $lead) {
            if($lead->lead_status != 1) { 
    			$assigned_detail = getLeadsLawyer($lead->id);
    			$assigned_lawyer_law_firm = getLeadsLawyerLawfirm($lead->id);
    		}
            $caseTypeName =  getCaseTypeName($lead->case_type);
            $newJurisdiction = getMarketName($lead->market);
            $stateName =  getStateName($newJurisdiction->state);
            $html .= '<tr>
						<td>'.$lead->lead_id_number.'</td>
						<td>'.($lead->lead_status != 1 && $lead->lead_status != 4 ? $assigned_lawyer_law_firm['assigned_to'] : 'Not Assigned').'</td>
						<td>'.($lead->lead_status != 1 && $lead->lead_status != 4 ? $assigned_detail['assigned_to'] : 'Not Assigned').'</td>
						<td>'.$lead->first_name.' '.$lead->last_name.'</td>
						<td>'.$caseTypeName[0]['type'].'</td>
						<td>'.$stateName[0]['state_short_name'] . $newJurisdiction->market_name . " - " . $stateName[0]['state_short_name'].'</td> 
						<td>'.$lead->case_description.'</td>
						<td>'.$lead->deleted_by.'</td>
						<td>'.date('M d, Y h:i a', strtotime($lead->deleted_on)).'</td>
						<td><a href="'.base_url().'admin/leads/viewLead/'.$lead->id.'/deletedLead">View Lead</a>&nbsp;|&nbsp;<a href="'.base_url().'admin/leads/undelete/'.$lead->id.'">Undelete Lead</a></td>
					</tr>';
        }
        if(!$html) $html = '<tr><td colspan="10" class="text-center">No records found!</td></tr>';
        echo $html;
        exit();
    }
}
?>