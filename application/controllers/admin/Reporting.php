<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Reporting extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('latest_url', current_url());
        chechUserSession();
        $this->load->model('admin/Leads_model');
        $this->load->model('admin/Users_model');
        $this->load->model('admin/Reporting_model');
		$this->load->model('admin/Setting_model');
        $this->load->model('Central_model');
    }
    public function index()
    {
        $this->admin();
    }
	public function law_firms()
	{
		$data                   = array();
        $data['class_casetype'] = 'active';
        $data['casetype']       = $this->Setting_model->lawType();
        $data['case_type']      = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/reporting/law_firms', $data);
        $this->load->view('layouts/admin/footer', $data);
	}
    public function lawyers_report($lawfirm_id=Null)
    {
		$lawfirm_id = ($this->session->userdata('role') == 6 ? $this->session->userdata('id') : $lawfirm_id);
        $data                = array();
        $data['lawyers'] = $this->Users_model->LawyerBilling($lawfirm_id, 2, 1);
        $data['class']       = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/reporting/lawyers', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function lawyerOweAmount()
    {
        $data      = array();
        $user_role = $this->session->userdata('role');
        if ($user_role == '1') {
            $lawyer_id = $this->uri->segment(4);
        } else {
            $lawyer_id = $this->session->userdata('id');
        }
        $data['all_lawyer']             = $this->Leads_model->fetchLawyerRow($lawyer_id);
        $lead_bill_new                  = 0;
        $monthly_bill_new               = 0;
        $initial_setup_new              = 0;
        $lawyer_leads_price             = $this->Reporting_model->fetchLawyerAllLeadsPrice($lawyer_id);
        $data['lead_bill_new']          = $lawyer_leads_price[0]->total;
        $Lawyer_all_monthly_bill        = $this->Reporting_model->fetchLawyerMonthlyBillTotal($lawyer_id);
        $data['monthly_bill_new']       = $Lawyer_all_monthly_bill[0]->total;
        $initial_setup_new              = $this->Reporting_model->fetchLawyerIntialSetupChargeTotal($lawyer_id);
        $data['initial_setup_new_bill'] = $initial_setup_new[0]->total;
        $one_time_new                   = $this->Reporting_model->fetchOneTimeChargeTotal($lawyer_id);
        $data['one_time_new_bill']      = $one_time_new[0]->total;
        $lawyerPaidTransactions_new     = $this->Reporting_model->fetchLawyerPaidTransactions($lawyer_id);
        $data['lawyerPaidTransactions'] = $lawyerPaidTransactions_new[0]->total;
        $data['total']                  = ($data['lead_bill_new'] + $data['monthly_bill_new'] + $data['initial_setup_new_bill'] + $data['one_time_new_bill']) - $data['lawyerPaidTransactions'];
        $total_arrays[]                 = $data['total'];
        $data['totalArrays']            = $total_arrays;
        $data['class']                  = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/reporting/lawyer_owe_amount');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function reportings()
    {
        $data      = array();
        $user_role = $this->session->userdata('role');
        if ($user_role == '1') {
            $data['id'] = $this->uri->segment(4);
        } else {
            $data['id'] = $this->session->userdata('id');
        }
		$data['lawyer'] = $this->Central_model->first("users","id",$data['id']);
        $data['lead_bill_new'] = $this->Reporting_model->fetchLawyerAllLeadsPrice($data['id'])[0]->total;
        $data['monthly_bill_new'] = $this->Reporting_model->fetchLawyerMonthlyBillTotal($data['id'])[0]->total;
        $data['total_charge_bill']  = $this->Reporting_model->total_charge_bill($data['id'])[0]->total;
        $lawyerPaidTransactions_new     = $this->Reporting_model->fetchLawyerPaidTransactions($data['id']);
		$data['total']                  = ($data['lead_bill_new'] + $data['monthly_bill_new'] + $data['total_charge_bill']) - $lawyerPaidTransactions_new[0]->total;
        $total_payments_new             = $this->Reporting_model->fetchLawyerAllPayments($data['id']);
        $data['total_payments']         = $total_payments_new[0]->total; 
        $data['transactions']     = $this->Reporting_model->search_transactions(array("lawyer_id" => $data['id']));
        $data['class']                  = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/reporting/reportings', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function LawyerPeriodicReporting()
    {
        $data                     = array();
        $html                     = '';
        $lawyer_id                = $this->input->post('lawyer_id');
        $date_from                = $this->input->post('date_from');
        $date_to                  = $this->input->post('date_to');
        $lawyer_data              = getLawyer($lawyer_id);
        $data['periodic_billing'] = $this->Reporting_model->fetchPeriodicLeads($lawyer_id, $date_from, $date_to);
        foreach ($data['periodic_billing'] as $periodic_billing) {
            $periodic_billing = $periodic_billing->price;
            $periodic_billing_new += $periodic_billing;
        }
        $periodic_billing_new;
        $data['Lawyer_all_monthly_bill'] = $this->Reporting_model->fetchLawyerMonthlyBill($lawyer_id);
        foreach ($data['Lawyer_all_monthly_bill'] as $lawyerMonthlyBills) {
            $monthly_bill = $lawyerMonthlyBills->monthly_bill;
            $monthly_bill_new += $monthly_bill;
        }
        $data['monthly_bill_new']           = $monthly_bill_new;
        $data['Lawyer_intial_setup_charge'] = $this->Reporting_model->fetchLawyerIntialSetupCharge($lawyer_id);
        foreach ($data['Lawyer_intial_setup_charge'] as $lawyerInitialSetupCharge) {
            $initial_setup = $lawyerInitialSetupCharge->amount;
            $initial_setup_new += $initial_setup;
        }
        $data['initial_setup_new_bill'] = $initial_setup_new;
        $data['total']                  = $periodic_billing_new + $monthly_bill_new + $initial_setup_new;
        $html .= '

					<div class="table-responsive">      
					  <div class="clearfix"></div> 
					  <div class="margin-top-10"></div>  
					 
					  
					  <div class="loader" style="display:none;"></div>
					  <span id="Success"  class="center-block" style="color:#3e923a; font-size:18px; display:none; text-align:center;font-weight:600; margin-bottom:20px;">Updated Successfully</span>
					  
					<table  class="display table table-bordered table-striped" id="dynamic-table">			  
					  <thead>    
						  <tr>     
						<th>Lawyer Name</th>   
						<th>Leads Bill</th>   
						<th>Monthly Bill</th>         
						<th>One time charge </th> 			
						<th>Initial Setup charge </th> 	 			
						<th>Total </th> 			
						</tr>                  
					  </thead>    
						 <tbody>
								<tr>
								 <td>' . $lawyer_data->name . " " . $lawyer_data->lname . '</td>
								 <td>$' . $periodic_billing_new . '.00</td>
								 <td>$' . $data['monthly_bill_new'] . '.00</td>
								 <td></td>
								 <td>$' . $data['initial_setup_new_bill'] . '.00</td>
								 <td>$' . $data['total'] . '.00</td>
								</tr>
					   </tbody>		
					</table>  
					
				</div><!--/table-responsive-->';
        $resArr['success'] = $html;
        echo $html;
        exit();
    }
    public function SearchLawyer()
    {
        $data             = array();
        $name             = $this->input->post('name');
        $law_firm         = $this->input->post('law_firm');
        $data['law_firm'] = $this->Central_model->first("law_firms", "id", $law_firm);
        $data['lawyers']  = $this->Users_model->LawyerBilling($law_firm, 2, 1, array("name" => $name), "after");
        $html = $this->load->view('admin/users/billings/lawyers_billing', $data, true);
        $data['success'] = 1;
        $data['html']    = $html;
        echo json_encode($data);
        exit();
    }
    public function SearchTransactionsByDates()
    {
        $data                = array();
        $html                = '';
        $lawyer_id           = $this->input->post('lawyer_id');
        $date_from           = $this->input->post('date_from');
        $date_to             = $this->input->post('date_to');
        $data['all_lawyers'] = $this->Reporting_model->fetchLawyerTrasanctionsOnDateRange($lawyer_id, $date_from, $date_to);
        $html .= '<div class="table-responsive">      
					  <div class="clearfix"></div> 
					  <div class="margin-top-10"></div>  
					 
					  
					  <div class="loader" style="display:none;"></div>
					  <span id="Success"  class="center-block" style="color:#3e923a; font-size:18px; display:none; text-align:center;font-weight:600; margin-bottom:20px;">Updated Successfully</span>
					  
					<table  class="display table table-bordered table-striped" id="dynamic-table">			  
					  <thead>    
						  <tr>     
						<th>Credit type</th>   
						<th>Credit debit</th>   
						<th>Amount</th>         
						<th>Transaction date </th> 			
						</tr>                  
					  </thead>    
						 <tbody>';
        foreach ($data['all_lawyers'] as $lawyer_data) {
            if ($lawyer_data->credit_debit == 'D') {
                $credit_debit = 'Debit';
            } else {
                $credit_debit = 'Credit';
            }
            $total_bill_var = number_format($lawyer_data->amount, 2);
            $d              = $lawyer_data->transaction_date;
            $date           = strtotime($d);
            $new_date       = date('M d Y - H:i', $date);
            $html .= '<tr>
								 <td>' . $lawyer_data->card_type . '</td>
								 <td>' . $credit_debit . '</td>
								 <td>$' . $total_bill_var . '</td>
								 <td>' . $new_date . '</td>
								</tr>';
        }
        $html .= '
					   </tbody>
					   </table>
					   </div>';
        $resArr['success'] = $html;
        echo $html;
        exit();
    }
    public function search_transactions()
    {
        $data = array();
        $data = $this->input->post();
        $data['date_from'] = _dateFormat($data['date_from']);
        $data['date_to'] = _dateFormat($data['date_to']);
		$data['transactions'] = $this->Reporting_model->search_transactions($data);
		$data['html'] = $this->load->view('admin/users/transactions/transactions', $data, true);
		echo json_encode($data); 
		exit;
        
    }
    function ExportTransactionsOnDates($lawyer_id, $date_from, $date_to, $charges)
    {
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        if ($charges == 'C' && $date_from != '1969-12-31' && $date_to != '1969-12-31') {
            $query = $this->db->query("select * from (SELECT amount as payment_amount , 'Payment' as description, transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 2) table1 WHERE transaction_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' order by transaction_date DESC");
        } else if ($charges == 'C' && $date_from == '1969-12-31' && $date_to == '1969-12-31') {
            $query = $this->db->query("select * from (SELECT amount as payment_amount , 'Payment' as description, transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 2) table1 WHERE transaction_date != '' order by transaction_date DESC");
        } else if ($charges == 'B' && $date_from != '1969-12-31' && $date_to != '1969-12-31') {
            $query = $this->db->query("select * from (SELECT `leads`.`price` as lead_bill ,  'Lead bill' as description ,`leads`.`created_at` as transaction_date  FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`lawyer_id` = '" . $lawyer_id . "'
					UNION ALL
					SELECT monthly_bill as monthly_bill , 'Monthly bill' as description , created_at as transaction_date FROM `monthly_payment` WHERE `lawyer_id` = '" . $lawyer_id . "'
					UNION ALL
					SELECT sum(`amount`) as intial_amount , 'Initial setup charge' as description,  transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 3
					UNION ALL
					SELECT sum(`amount`) as one_time_amount , 'One time charge' as description, transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 4
					UNION ALL
					SELECT amount as payment_amount , 'Payment' as description, transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 2) table1 WHERE transaction_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' order by transaction_date DESC");
        } else if ($charges == 'B' && $date_from == '1969-12-31' && $date_to == '1969-12-31') {
            $query = $this->db->query("select * from (SELECT `leads`.`price` as lead_bill ,  'Lead bill' as description ,`leads`.`created_at` as transaction_date  FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`lawyer_id` = '" . $lawyer_id . "'
					UNION ALL
					SELECT monthly_bill as monthly_bill , 'Monthly bill' as description , created_at as transaction_date FROM `monthly_payment` WHERE `lawyer_id` = '" . $lawyer_id . "'
					UNION ALL
					SELECT sum(`amount`) as intial_amount , 'Initial setup charge' as description,  transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 3
					UNION ALL
					SELECT sum(`amount`) as one_time_amount , 'One time charge' as description, transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 4
					UNION ALL
					SELECT amount as payment_amount , 'Payment' as description, transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 2) table1 WHERE transaction_date != '' order by transaction_date DESC");
        } else if ($charges == 'D' && $date_from != '1969-12-31' && $date_to != '1969-12-31') {
            $query = $this->db->query("select * from (SELECT `leads`.`price` as lead_bill ,  'Lead bill' as description ,`leads`.`created_at` as transaction_date  FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`lawyer_id` = '" . $lawyer_id . "'
					UNION ALL
					SELECT monthly_bill as monthly_bill , 'Monthly bill' as description , created_at as transaction_date FROM `monthly_payment` WHERE `lawyer_id` = '" . $lawyer_id . "'
					UNION ALL
					SELECT sum(`amount`) as intial_amount , 'Initial setup charge' as description,  transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 3
					UNION ALL
					SELECT sum(`amount`) as one_time_amount , 'One time charge' as description, transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 4
					) table1 WHERE transaction_date BETWEEN '" . $date_from . "' AND '" . $date_to . "' order by transaction_date DESC");
        } else if ($charges == 'D' && $date_from == '1969-12-31' && $date_to == '1969-12-31') {
            $query = $this->db->query("select * from (SELECT `leads`.`price` as lead_bill ,  'Lead bill' as description ,`leads`.`created_at` as transaction_date  FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`lawyer_id` = '" . $lawyer_id . "'
					UNION ALL
					SELECT monthly_bill as monthly_bill , 'Monthly bill' as description , created_at as transaction_date FROM `monthly_payment` WHERE `lawyer_id` = '" . $lawyer_id . "'
					UNION ALL
					SELECT sum(`amount`) as intial_amount , 'Initial setup charge' as description,  transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 3
					UNION ALL
					SELECT sum(`amount`) as one_time_amount , 'One time charge' as description, transaction_date FROM `credit_card_transactions` WHERE `lawyer_id` = '" . $lawyer_id . "' AND `status` = 4
					) table1 WHERE transaction_date !='' order by transaction_date DESC");
        }
        $delimiter = ",";
        $newline   = "\r\n";
        $data      = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        force_download('CSV_Report.csv', $data);
    }
    function export_transactions($date_from, $date_to, $charges)
    {
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');	
		$transactions = $this->Reporting_model->export_transactions($date_from, $date_to, $charges);
        $delimiter = ",";
        $newline   = "\r\n";
        $data      = $this->dbutil->csv_from_result($transactions, $delimiter, $newline);
        force_download('CSV_Report.csv', $data);
    }
    function ExportTransactions($id)
    {
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $query     = $this->db->query("SELECT amount,credit_debit,transaction_date FROM credit_card_transactions WHERE lawyer_id=" . $id);
        $delimiter = ",";
        $newline   = "\r\n";
        $data      = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        force_download('CSV_Report.csv', $data);
    }
}

