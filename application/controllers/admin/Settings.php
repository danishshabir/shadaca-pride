<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Settings extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('latest_url', current_url());
        chechUserSession();
        $this->load->model('admin/Leads_model');
        $this->load->model('admin/Users_model');
        $this->load->model('admin/Activity_model');
        $this->load->model('admin/Survey_model');
        $this->load->model('admin/Setting_model');
        $this->load->model('Central_model');
		$this->load->library('Backup_Database');
    }
    public function survey()
    {
        $data                 = array();
        $data['class_survey'] = 'active';
        $data['questions']    = $this->Survey_model->get_question();
        $data['case_type']    = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/settings/survey', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function setting()
    {
        //$sms_sent = sms('+1 310-435-2176', 'testing sms');
        //echo '<pre>';print_r($sms_sent);exit();
        $data                       = array();
        $data['class_setting']      = 'active';
        $data['settings']           = $this->Setting_model->fetchSettings();
        $data['admins']             = $this->Setting_model->fetchAllAdmins();
        $data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
        $data['case_type']          = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/settings/setting', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function save_settings()
    {
        $data               = array();
        $activity           = array();
        $data               = $this->input->post();
        if($data['super_admin_id'] != $data['second_super_admin_id']){
            $data['updated_at'] = date('Y:m:d H:i:s');
            
            // commented by hassan, because the invoice will not be date anymore. It'll be the number of
            // days to be added in current date
            #$data['invoice_due_date'] = _dateTimeFormat($data['invoice_due_date']);
            
            //$data['invoice_due_date'] = date('Y:m:d H:i:s', strtotime($data['invoice_due_date']));
            unset($data['_wysihtml5_mode']);
            $result = $this->Setting_model->save_survey($data);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success">Settings saved successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/setting');
            } //$result
        }else{
            $this->session->set_flashdata('message', '<p style="color: red; text-align: center;">Same admins cannot be selected.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/setting');
        }
    }
    public function save_survey()
    {
        $data                   = array();
        $activity               = array();
        $data['question_title'] = $this->input->post('question_title');
        $types                  = $this->input->post('option_answer');
        $types_radio            = $this->input->post('option_answer_radio');
        $data['created_at']     = date('Y-m-d H:i:s');
        $insert_id              = "";
        $data['question_type']  = $this->input->post('question_type');
        unset($data['submit']);
        $insert_id = $this->Survey_model->save_survey($data);
        if ($this->input->post('question_type') == "checkbox") {
            foreach ($types as $type) {
                $data2['option_answer'] = $type;
                $data2['question_id']   = $insert_id;
                $data2['created_at']    = date('Y-m-d H:i:s');
                $insert_id2             = $this->Survey_model->save_survey_options($data2);
            } //$types as $type
        } //$this->input->post('question_type') == "checkbox"
        if ($this->input->post('question_type') == "radio") {
            foreach ($types_radio as $type_radio) {
                $data2['option_answer'] = $type_radio;
                $data2['question_id']   = $insert_id;
                $data2['created_at']    = date('Y-m-d H:i:s');
                $insert_id2             = $this->Survey_model->save_survey_options($data2);
            } //$types_radio as $type_radio
        } //$this->input->post('question_type') == "radio"
        if ($insert_id > 0) {
            $this->session->set_flashdata('message', '<p class="message_success">Question added successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/survey');
        } //$insert_id > 0
    }
    public function survey_form()
    {
        $user_id               = $this->session->userdata('id');
        $data                  = array();
        $data['user_id']       = $user_id;
        $data['class_leads']   = 'active';
        $data['question_data'] = $this->Survey_model->get_question();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/settings/survey_form', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addsurvey_form()
    {
        $user_id      = $this->session->userdata('id');
        $data         = array();
        $qid_array    = $this->input->post('question_id');
        $qtype_array  = $this->input->post('question_type');
        $qcheck_array = $this->input->post('options_checkbox_array');
        $qtext        = $this->input->post('question_text');
        $created_at   = date('Y-m-d H:i:s');
        $count_text   = 0;
        $count_radio  = 0;
        foreach ($qid_array as $index => $value) {
            if ($qtype_array[$index] == "text") {
                $data1['question_id']      = $qid_array[$index];
                $data1['user_id']          = $user_id;
                $data1['answer']           = $qtext[$count_text];
                $data1['checked_answered'] = "1";
                $data1['created_at']       = $created_at;
                $this->Survey_model->add_question_answer($data1);
                $count_text = $count_text + 1;
            } //$qtype_array[$index] == "text"
            if ($qtype_array[$index] == "checkbox") {
                foreach ($qcheck_array as $index1 => $value1) {
                    $data3['question_id']      = $qid_array[$index];
                    $data3['user_id']          = $user_id;
                    $checkbox_answer           = $this->Survey_model->get_checkbox_answer($qcheck_array[$index1]);
                    $data3['answer']           = $checkbox_answer->option_answer;
                    $data3['checked_answered'] = "1";
                    $data3['created_at']       = $created_at;
                    $this->Survey_model->add_question_answer($data3);
                } //$qcheck_array as $index1 => $value1
            } //$qtype_array[$index] == "checkbox"
            if ($qtype_array[$index] == "radio") {
                $qradio                    = $this->input->post('radio_option');
                $data2['question_id']      = $qid_array[$index];
                $data2['user_id']          = $user_id;
                $data2['answer']           = $qradio[$count_radio];
                $data2['checked_answered'] = "1";
                $data2['created_at']       = $created_at;
                $this->Survey_model->add_question_answer($data2);
                $count_radio = $count_radio + 1;
            } //$qtype_array[$index] == "radio"
        } //$qid_array as $index => $value
        unset($data['submit']);
        $this->session->set_flashdata('message', '<p class="message_success">Survey Completed.</p>');
        redirect($this->config->item('base_url') . 'admin/settings/survey_form');
    }
    public function deleteSurvey($id)
    {
        if ($id) {
            $result = $this->Survey_model->deleteRow($id);
            if ($result) {
                $this->Survey_model->deleteAnswers($id);
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/survey');
            } //$result
        } //$id
    }
    public function list_survey()
    {
        $data                 = array();
        $data['class_survey'] = 'active';
        $data['users']        = $this->Users_model->get_users();
        $data['case_type']    = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/settings/list_survey', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function view_survey($user_id)
    {
        $data                 = array();
        $data['class_survey'] = 'active';
        $data['answers']      = $this->Survey_model->get_user_answers($user_id);
        $data['case_type']    = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/settings/view_survey', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function caseType()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['casetype']       = $this->Setting_model->caseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/list', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function market()
    {
        $data           = array();
        $data['market'] = 'active';
        $data['market'] = $this->Setting_model->marketRecords();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/market', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function jurisdiction_n()
    {
        $data                   = array();
        $data['jurisdiction_n'] = 'active';
        $data['jurisdiction_n'] = $this->Setting_model->jurisdiction_n();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/jurisdiction_n', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function intaker()
    {
        $data            = array();
        $data['intaker'] = 'active';
        $data['intaker'] = $this->Setting_model->intakerRecords();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/intaker', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function source()
    {
        $data           = array();
        $data['source'] = 'active';
        $data['source'] = $this->Setting_model->sourceRecords();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/source', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function tags_label()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['tag_type']       = $this->Setting_model->tagsType();
        $data['case_type']      = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/tags_labels', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function jurisdiction()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['jurisdiction']   = $this->Setting_model->fetchJurisdiction();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/jurisdiction/jurisdiction', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function market_n()
    {
        $data             = array();
        $data['market_n'] = 'active';
        $data['market_n'] = $this->Setting_model->fetchMarket_n();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/market/market', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function jurisdictionFee()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['jurisdiction']   = $this->Setting_model->fetchJurisdictionFee();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/jurisdiction/jurisdictionfee', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function marketFee()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['market_fee']     = $this->Setting_model->fetchMarketFee();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/market/marketfee', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function email_templates()
    {
		if ($this->session->userdata('role') == 1) {
            $super_admin = $this->Leads_model->fetchSuperAdminRecord();
			$data                    = array();
			$data['admin_email'] = $super_admin->superAdminEmail;
			$data['class_casetype']  = 'active';
			$data['email_templates'] = $this->Setting_model->emailType();
			$data['case_type']       = $this->Leads_model->fetchCaseType();

			$this->load->view('layouts/admin/header', $data);
			$this->load->view('layouts/admin/sidebar', $data);
			$this->load->view('admin/casetype/email_templates', $data);
			$this->load->view('layouts/admin/footer', $data);
		}
    }
    public function lawFirms()
    {
		if($this->session->view_as != ''){
			$admin_id = $this->session->backup_admin_id;
			$this->session->set_userdata(array('role' => 1, 'id' => $admin_id, 'view_as' => ''));
		}
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['casetype']       = $this->Setting_model->lawType();
        //echo '<pre>';print_r($data['casetype']);exit();
        $data['case_type']      = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/law_list', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function editCasetype()
    {
        $data                = array();
        $data['id']          = $this->uri->segment(4);
        $data['class_leads'] = 'active';
        $data['lead']        = $this->Setting_model->fetchRow($data['id']);
        $data['case_type']   = $this->Setting_model->fetchCaseType();
        if (!empty($data['lead'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/casetype/edit_type', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['lead'])
    }
    public function editJurisdiction_n()
    {
        $data                       = array();
        $data['id']                 = $this->uri->segment(4);
        $data['editJurisdiction_n'] = 'active';
        $data['jurisdiction_n']     = $this->Setting_model->fetchJurisdiction_n_Row($data['id']);
        if (!empty($data['jurisdiction_n'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/casetype/edit_jurisdiction_n', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['jurisdiction_n'])
    }
    public function editIntaker()
    {
        $data                = array();
        $data['id']          = $this->uri->segment(4);
        $data['editIntaker'] = 'active';
        $data['intaker']     = $this->Setting_model->fetchIntakerRow($data['id']);
        if (!empty($data['intaker'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/casetype/edit_intaker', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['intaker'])
    }
    public function editSource()
    {
        $data               = array();
        $data['id']         = $this->uri->segment(4);
        $data['editSource'] = 'active';
        $data['source']     = $this->Setting_model->fetchSourceRow($data['id']);
        if (!empty($data['source'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/casetype/edit_source', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['source'])
    }
    public function update_Jurisdiction_n()
    {
        $id                 = $this->uri->segment(4);
        $data               = array();
        $data               = $this->input->post();
        $data['updated_at'] = date('m/d/Y H:i:s');
        unset($data['submit']);
        $update = $this->Setting_model->updateJurisdiction_n_Row($data, $id);
        if ($update) {
            $this->session->set_flashdata('message', '<p class="message_success"> Jurisdiction updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/jurisdiction_n');
        } //$update
    }
    public function update_intaker()
    {
        $id                 = $this->uri->segment(4);
        $data               = array();
        $data               = $this->input->post();
        $data['updated_at'] = date('m/d/Y H:i:s');
        unset($data['submit']);
        $update = $this->Setting_model->updateIntakerRow($data, $id);
        if ($update) {
            $this->session->set_flashdata('message', '<p class="message_success"> Intaker updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/intaker');
        } //$update
    }
    public function update_source()
    {
        $id                 = $this->uri->segment(4);
        $data               = array();
        $data               = $this->input->post();
        $data['updated_at'] = date('m/d/Y H:i:s');
        unset($data['submit']);
        $update = $this->Setting_model->updateSourceRow($data, $id);
        if ($update) {
            $this->session->set_flashdata('message', '<p class="message_success"> Updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/source');
        } //$update
    }
    public function editLawtype($id)
    {
        $data                       = array();
		$id = ($this->session->userdata('role') == 6 ? $this->session->userdata('id') : $id);
		if($this->input->method(TRUE) == 'POST') {
			$this->load->library('form_validation');
			$path = './uploads';
			$data = $this->input->post();
			$rules = array(
				[
					'field' => 'law_firm',
					'label' => 'First Name',
					'rules' => (isset($data['law_firm']) ? 'trim|required' : ''),
				],
				[
					'field' => 'contact_last_name',
					'label' => 'Last Name',
					'rules' => (isset($data['contact_last_name']) ? 'trim|required' : ''),
				],
				[
					'field' => 'phone',
					'label' => 'Phone',
					'rules' => (isset($data['phone']) ? 'trim|required' : ''),
				],
				[
					'field' => 'contact_name',
					'label' => 'Contact Name',
					'rules' => (isset($data['contact_name']) ? 'trim|required' : ''),
				],
				[
					'field' => 'contact_email',
					'label' => 'Email',
					'rules' => (isset($data['contact_email']) ? 'trim|required|valid_email|callback_check_email['.$id.']' : ''),
				],
				[
					'field' => 'street',
					'label' => 'Street',
					'rules' => (isset($data['street']) ? 'trim|required' : ''),
				],
				[
					'field' => 'city',
					'label' => 'City',
					'rules' => (isset($data['city']) ? 'trim|required' : ''),
				],
				[
					'field' => 'state',
					'label' => 'State',
					'rules' => (isset($data['state']) ? 'trim|required' : ''),
				],
				[
					'field' => 'zip',
					'label' => 'Zip Code',
					'rules' => (isset($data['zip']) ? 'trim|required' : ''),
				],
				[
					'field' => 'fee_type',
					'label' => 'Fee Type',
					'rules' => (isset($data['fee_type']) ? 'trim|required' : ''),
				],
				[
					'field' => 'law_firm_fee',
					'label' => 'Fee',
					'rules' => (isset($data['law_firm_fee']) ? 'trim|required' : ''),
				],
				[
					'field' => 'image',
					'label' => 'Image',
					'rules' => 'callback_upload_image['.$path.']',
				]
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
				$data['updated_at'] = date('m-d-Y h:i:s');
				if ($_FILES['image']['name'] != '') {
					$data['image'] = ($_FILES["image"]["name"]);
				}
				else {
					$data['image'] = $this->input->post('image');
				}
				$result = $this->Leads_model->updateLawCase($data, $id);
				if ($result) {
					if($this->session->userdata('role') == 6)
					{
						$this->session->set_flashdata('message', '<p class="message_success">Law firm Profile updated successfully.</p>');
						redirect($this->config->item('base_url') . 'admin/users/userProfile/' . $id);
					}
					else
					{
						$this->session->set_flashdata('message', '<p class="message_success">Law firm updated successfully.</p>');
						redirect($this->config->item('base_url') . 'admin/settings/lawFirms/' . $id);
					}
				} //$result
			}
		}
        $data['id']                 = $id;
        $data['class_leads']        = 'active';
        $data['lead']               = $this->Setting_model->fetchLawRow($data['id']);
        $data['case_type']          = $this->Leads_model->fetchCaseType();
        $data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
        if (!empty($data['lead'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/casetype/edit_law_type', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['lead'])
    }
    public function editTagtype()
    {
        $data                = array();
        $data['id']          = $this->uri->segment(4);
        $data['class_leads'] = 'active';
        $data['tag']         = $this->Setting_model->fetchTagRow($data['id']);
        $data['case_type']   = $this->Leads_model->fetchCaseType();
        if (!empty($data['tag'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/casetype/edit_tag_type', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['tag'])
    }
    public function editMarket()
    {
        $data                 = array();
        $data['id']           = $this->uri->segment(4);
        $data['editMarket_n'] = 'active';
        $data['states']       = $this->Setting_model->fetchAllStates();
        $data['market']       = $this->Setting_model->fetchMarketRow($data['id']);
        if (!empty($data['market'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/casetype/edit_market', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['market'])
    }
    public function editMarket_n()
    {
        $data                 = array();
        $data['id']           = $this->uri->segment(4);
        $data['editMarket_n'] = 'active';
        $data['states']       = $this->Setting_model->fetchAllStates();
        $data['market']       = $this->Setting_model->fetchMarket_n_Row($data['id']);
        if (!empty($data['market'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/market/edit_market_n', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['market'])
    }
    public function editJurisdiction()
    {
        $data                 = array();
        $data['id']           = $this->uri->segment(4);
        $data['class_leads']  = 'active';
        $data['states']       = $this->Setting_model->fetchAllStates();
        $data['jurisdiction'] = $this->Setting_model->fetchJurisdictionRow($data['id']);
        if (!empty($data['jurisdiction'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/jurisdiction/edit_jurisdiction', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['jurisdiction'])
    }
    public function editJurisdictionFee()
    {
        $data                     = array();
        $data['id']               = $this->uri->segment(4);
        $data['class_leads']      = 'active';
        $data['jurisdiction']     = $this->Setting_model->fetchAllJurisdiction();
        $data['caseType']         = $this->Setting_model->fetchAllCaseType($data['id']);
        $data['jurisdiction_fee'] = $this->Setting_model->fetchJurisdictionFeeRow($data['id']);
        if (!empty($data['jurisdiction'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/jurisdiction/edit_jurisdiction_fee', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['jurisdiction'])
    }
    public function editMarketFee()
    {
        $data                = array();
        $data['id']          = $this->uri->segment(4);
        $data['class_leads'] = 'active';
        $data['markets']     = $this->Setting_model->fetchAllMarkets();
        $data['caseType']    = $this->Setting_model->fetchAllCaseType($data['id']);
        $data['market_fee']  = $this->Setting_model->fetchMarketFeeRow($data['id']);
        if (!empty($data['markets'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/market/edit_market_fee', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['markets'])
    }
    public function updateMarket($id)
    {
        $data   = array();
        $data   = $this->input->post();
        $result = $this->Setting_model->updateMarket($data, $id);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success"> Updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/market');
        } //$result
    }
    public function updateMarket_n($id)
    {
        $data   = array();
        $data   = $this->input->post();
        $result = $this->Setting_model->updateMarket_n($data, $id);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success"> Updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/market_n');
        } //$result
    }
    public function updateJurisdiction($id)
    {
        $data   = array();
        $data   = $this->input->post();
        $result = $this->Setting_model->updateJurisdiction($data, $id);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/jurisdiction');
        } //$result
    }
    public function updateJurisdictionFee($id)
    {
        $data   = array();
        $data   = $this->input->post();
        $result = $this->Setting_model->updateJurisdictionFee($data, $id);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/jurisdictionFee');
        } //$result
    }
    public function updateMarketFee($id)
    {
        $data   = array();
        $data   = $this->input->post();
        $result = $this->Setting_model->updateMarketFee($data, $id);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/marketFee');
        } //$result
    }
    public function editEmailTemplate()
    {
		if ($this->session->userdata('role') == 1) {
			$data                  = array();
			$data['id']            = $this->uri->segment(4);
			$data['class_leads']   = 'active';
			$data['EmailTemplate'] = $this->Setting_model->fetchTemplateRow($data['id']);
			$data['case_type']     = $this->Leads_model->fetchCaseType();
			if (!empty($data['EmailTemplate'])) {
				$this->load->view('layouts/admin/header', $data);
				$this->load->view('layouts/admin/sidebar', $data);
				$this->load->view('admin/casetype/editEmailTemplate', $data);
				$this->load->view('layouts/admin/footer', $data);
			} //!empty($data['EmailTemplate'])
		}
    }
    public function addCasetype()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['casetype']       = $this->Setting_model->caseType();
        $data['case_type']      = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/add', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addJurisdiction_n()
    {
        $data              = array();
        $data['addMarket'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/addJurisdiction_n', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addIntaker()
    {
        $data              = array();
        $data['addMarket'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/add_intaker', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addSource()
    {
        $data              = array();
        $data['addMarket'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/add_source', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addFirmtype()
    {
        $data          = array();
		if($this->input->method(TRUE) == 'POST') {
			$this->load->library('form_validation');
			$path = './uploads';
			$data = $this->input->post();
			$rules = array(
				[
					'field' => 'law_firm',
					'label' => 'First Name',
					'rules' => (isset($data['law_firm']) ? 'trim|required' : ''),
				],
				[
					'field' => 'contact_last_name',
					'label' => 'Last Name',
					'rules' => (isset($data['contact_last_name']) ? 'trim|required' : ''),
				],
				[
					'field' => 'phone',
					'label' => 'Phone',
					'rules' => (isset($data['phone']) ? 'trim|required' : ''),
				],
				[
					'field' => 'contact_name',
					'label' => 'Contact Name',
					'rules' => (isset($data['contact_name']) ? 'trim|required' : ''),
				],
				[
					'field' => 'contact_email',
					'label' => 'Email',
					'rules' => (isset($data['contact_email']) ? 'trim|required|valid_email|callback_check_email' : ''),
				],
				[
					'field' => 'street',
					'label' => 'Street',
					'rules' => (isset($data['street']) ? 'trim|required' : ''),
				],
				[
					'field' => 'city',
					'label' => 'City',
					'rules' => (isset($data['city']) ? 'trim|required' : ''),
				],
				[
					'field' => 'state',
					'label' => 'State',
					'rules' => (isset($data['state']) ? 'trim|required' : ''),
				],
				[
					'field' => 'zip',
					'label' => 'Zip Code',
					'rules' => (isset($data['zip']) ? 'trim|required' : ''),
				],
				[
					'field' => 'fee_type',
					'label' => 'Fee Type',
					'rules' => (isset($data['fee_type']) ? 'trim|required' : ''),
				],
				[
					'field' => 'law_firm_fee',
					'label' => 'Fee',
					'rules' => (isset($data['law_firm_fee']) ? 'trim|required' : ''),
				],
				[
					'field' => 'image',
					'label' => 'Image',
					'rules' => 'callback_upload_image['.$path.']',
				]
			);
            
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
				$this->load->helper('string');
                $data['created_at'] = date('Y-m-d H:i:s');
				$data['password'] = sha1($this->input->post('password'));
                $data['verified'] = 0;
				$data['unique_code'] = random_string('alnum',20);
                $data['internal_notes'] = $this->input->post('internal_notes');
                $data['shared_notes'] = $this->input->post('shared_notes');
				$data['image'] = $_FILES['image']['name'];
				$max_rec = $this->Central_model->select_max_field('law_firms', array(), 'customer_id');
				$data['customer_id'] = (isset($max_rec->customer_id) ? $max_rec->customer_id + 1 : '10000000');
				$role = $this->Central_model->first("role","role","Lawfirm Admin");
				$data['role'] = $role->id;
				unset($data['submit']); unset($data['submit_form_type']);
				$template = $this->Leads_model->template(11);
				$fullname = $this->input->post('contact_name');
				$name     = $this->config->item('project_title');
				$username = $this->input->post('contact_name'); 
				$password = $this->input->post('password');
				$subject  = $template->subject;
				$message = $template->content;
				$message = str_replace("{email}", $data['contact_email'], $message);
				$message = str_replace("{password}", $password, $message);
				$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
				$message = str_replace("{name}", $fullname, $message);
				$message = str_replace("{username}", $data['contact_email'], $message);
				$message = str_replace("{organization name}", $this->config->item('project_title'), $message);
				$message = str_replace("{rootpath}", base_url(), $message);
				$message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
				$message = str_replace("{link}", '<a href="'.base_url().'admin/ResetPassword/reset/law_firms/'.$data['unique_code'].'">'.base_url().'admin/ResetPassword/reset/law_firms/'.$data['unique_code'].'</a>', $message);
				$from    = $template->email_from;
				$to      = $data['contact_email'];
				send_email($to,$from,$name,$subject,$message);
				/*Send SMS*/
				$sms_message = $template->text_message;
				$sms_message = str_replace("{project_title}", $this->config->item('project_title'), $sms_message);
				sms(1, $data['phone'], $sms_message);
				$result = $this->Setting_model->saveLawType($data);
				if ($result) {
					if($this->input->post('submit_form_type') == 0) {
						$this->session->set_flashdata('message', '<p class="message_success">Saved successfully.</p>');
						redirect($this->config->item('base_url') . 'admin/settings/lawFirms');
					} else {
						$this->session->set_userdata('lawfirm_id', $result);
						redirect($this->config->item('base_url') . 'admin/users/add/2');
					}
				} //$result
			}
		}
        $data['class_casetype']     = 'active';
        $data['casetype']           = $this->Setting_model->lawType();
        $data['case_type']          = $this->Leads_model->fetchCaseType();
        $data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/add_law', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
	public function check_email($email, $id= Null)
	{
		if($id)
			$count = $this->Central_model->count_rows("law_firms", array("contact_email" => $email), array("id" => $id));
		else 
			$count = $this->Central_model->count_rows("law_firms", array("contact_email" => $email));
		
		if($count > 0)
		{
			$this->form_validation->set_message('check_email', "Law Firm email already exists. Duplicates not allowed!");
			return FALSE;
		}
		
		return TRUE;
	}
	public function upload_image($name, $path=Null)
	{
		$config['upload_path'] = $path;
		$config['max_size'] = 1024 * 10;
		$config['allowed_types'] = 'gif|png|jpg|jpeg';
		//$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		if(isset($_FILES['image']) && !empty($_FILES['image']['name']))
		{
			if($this->upload->do_upload('image'))
			{
				$upload_data = $this->upload->data();
				$_POST['image'] = $upload_data['file_name'];
				return TRUE;
			}
			else
			{
				$this->form_validation->set_message('upload_image', "The File type you are uploading is not allowed!");
				return FALSE;
			}
		}
		return TRUE;
	}
    public function addTagtype()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['casetype']       = $this->Setting_model->lawType();
        $data['case_type']      = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/add_tag', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addJurisdiction()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['states']         = $this->Setting_model->fetchAllStates();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/jurisdiction/add_jurisdiction', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addMarket_n()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['states']         = $this->Setting_model->fetchAllStates();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/market/add_market', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addMarket()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['states']         = $this->Setting_model->fetchAllStates();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/add_market', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addJurisdictionFee()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['jurisdiction']   = $this->Setting_model->fetchAllJurisdiction();
        $data['caseType']       = $this->Setting_model->fetchAllCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/jurisdiction/add_jurisdiction_fee', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addMarketFee()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['jurisdiction']   = $this->Setting_model->fetchAllMarkets();
        $data['caseType']       = $this->Setting_model->fetchAllCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/market/add_market_fee', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function addEmailTemplate()
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['casetype']       = $this->Setting_model->lawType();
        $data['case_type']      = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/add_email_template', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function saveCaseType()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $result   = $this->Setting_model->saveCaseType($data);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Saved successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/caseType');
        } //$result
    }
    public function delete_law()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteLawRow($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/lawFirms');
            } //$result
        } //$id
    }
    public function delete_tag()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteTagRow($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/tags_label');
            } //$result
        } //$id
    }
    public function delete_jurisdiction()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteJurisdictionRow($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/jurisdiction');
            } //$result
        } //$id
    }
    public function delete_market_n()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteMarket_n_Row($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/market_n');
            } //$result
        } //$id
    }
    public function delete_market()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteMarket($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/market');
            } //$result
        } //$id
    }
    public function delete_jurisdiction_fee()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteJurisdictionFeeRow($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/jurisdictionFee');
            } //$result
        } //$id
    }
    public function delete_market_fee()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteMarketFeeRow($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/marketFee');
            } //$result
        } //$id
    }
    public function delete_email_template()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteTemplateRow($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/email_templates');
            } //$result
        } //$id
    }
    public function delete_casetype()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteCaseRow($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/caseType');
            } //$result
        } //$id
    }
    public function deleteJurisdiction_n_Row()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteJurisdiction_n_Row($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/jurisdiction_n');
            } //$result
        } //$id
    }
    public function delete_intaker()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteIntakerRow($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/intaker');
            } //$result
        } //$id
    }
    public function delete_source()
    {
        $id = $this->uri->segment(4);
        if ($id) {
            $result = $this->Setting_model->deleteSourceRow($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/source');
            } //$result
        } //$id
    }
    public function saveTag()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $result   = $this->Setting_model->saveTagType($data);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Saved successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/tags_label');
        } //$result
    }
    public function saveJurisdiction_n()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $result   = $this->Setting_model->saveJurisdiction_n($data);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Saved successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/jurisdiction_n');
        } //$result
    }
    public function saveIntaker()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $result   = $this->Setting_model->saveIntaker($data);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Saved successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/intaker');
        } //$result
    }
    public function saveSource()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $result   = $this->Setting_model->saveSource($data);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Saved successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/source');
        } //$result
    }
    public function saveJurisdiction()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $result   = $this->Setting_model->saveJurisdiction($data);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Jurisdiction saved successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/jurisdiction');
        } //$result
    }
    public function saveMarket_n()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $result   = $this->Setting_model->saveMarket_n($data);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Market saved successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/market_n');
        } //$result
    }
    public function saveMarket()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $result   = $this->Setting_model->saveMarket($data);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Market saved successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/market');
        } //$result
    }
    public function saveJurisdictionFee()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $result   = $this->Setting_model->saveJurisdictionFee($data);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Saved successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/jurisdictionFee');
        } //$result
    }
    public function saveMarketFee()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $market   = $data['market'];
        $case_type= $data['case_type'];
        $check = $this->Setting_model->checkIfMarketFeeExists($market, $case_type);
        if($check < 1){
            $result   = $this->Setting_model->saveMarketFee($data);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success">Saved successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/settings/marketFee');
            } //$result
        }else{
            $this->session->set_flashdata('message', '<p style="color: red; text-align: center; font-size: 15px; ">Selected Market And Case Type Already Existed.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/marketFee');
        }
    }
    public function saveEmailTemplate()
    {
        $data     = array();
        $activity = array();
        $data     = $this->input->post();
        $result   = $this->Setting_model->saveEmailTemplate($data);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Email template saved successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/email_templates');
        } //$result
    }
    public function addCasetypeById($id)
    {
        $data                   = array();
        $data['class_casetype'] = 'active';
        $data['casetype']       = $this->Setting_model->caseTypeRow($id);
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/casetype/edit', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
	public function login_users()
	{
		$data                   = array();
        $data['login_users'] = 'active';
        $data['login_users'] = $this->Setting_model->login_users();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/settings/login_users', $data);
        $this->load->view('layouts/admin/footer', $data);
	}
	public function backup_database()
	{
		$host = $this->db->hostname;
		$username = $this->db->username;
		$password = $this->db->password;
		$database = $this->db->database;
		$charset = 'utf-8';
				
		$backupDatabase = new Backup_Database($host, $username, $password, $database, $charset);
		$backupDatabase->backupTables($tables = '*', $host, $username, $password, $database, $outputDir = '../../databaseBackup');
		
		$this->load->view('layouts/admin/header');
        $this->load->view('layouts/admin/sidebar');
        $this->load->view('admin/settings/db_backup_done');
        $this->load->view('layouts/admin/footer');
	}
	public function export_tables()
	{
		$this->load->dbutil();
		$delimiter = ",";
		$newline = "\r\n";
		$enclosure = '"';
		$this->load->helper('file');
		$this->load->helper('download');
		$input = $this->input->get('name');
		if($input == 'leads'){
			$query = $this->db->query("SELECT * FROM leads");
			$name = 'leads';
		}else if($input == 'lawyers'){
			$query = $this->db->query("SELECT * FROM users WHERE role = 2");
			$name = 'lawyers';
		}else if($input == 'lawfirms'){
			$query = $this->db->query("SELECT * FROM law_firms WHERE role = 6");
			$name = 'lawfirm';
		}else if($input == 'lawyerstaff'){
			$query = $this->db->query("SELECT * FROM users WHERE role = 7");
			$name = 'lawyerstaff';
		}
		$csv_data = $this->dbutil->csv_from_result($query, $delimiter, $newline, $enclosure);
		//write_file(APPPATH.'csv-sheets/csv-'.$name.'-'.time().'.csv', $csv_data);
		$download_name = 'csv-'.$name.'-rms-'.time().'.csv';
		force_download($download_name, $csv_data);
	}
	
	public function maintenance($status = '')
	{
		$last_url = $_SERVER['HTTP_REFERER'];
		if($this->session->role == 1){
			if($status == 1){
				$this->session->set_userdata('maintenance_status', 1);
				$data['mode_status'] = 1;
                $this->Setting_model->update_maintenance_data($data);
				# $this->db->update('maintenance_mode', $data);
				$this->session->set_flashdata('message', '<p class="message_success">Maintenance Mode Activated.</p>');
			}else{
				$this->session->set_userdata('maintenance_status', 0);
				$data['mode_status'] = 0;
                $this->Setting_model->update_maintenance_data($data);
			    # $this->db->update('maintenance_mode', $data);
				$this->session->set_flashdata('message', '<p class="message_success">Maintenance Mode Deactivated.</p>');
			}
		}
		redirect($last_url);
	}
	
	public function whitelist_ips($func = '', $id = '')
	{
		if($func != '' && $func == 'edit_ip' && $id != ''){
		    $maintenance_data = $this->Setting_model->get_maintenance_data($id);
			// $query = $this->db->query("SELECT * FROM maintenance_mode WHERE id = '$id'");
			$view_data['edit_ip'] = $maintenance_data;
		}else if($func != '' && $func == 'update_ip' && $id != ''){
			$data['white_listed_ips'] = $this->input->post('ip_address');
            $this->Setting_model->update_maintenance_data($data, $id);
			//$query = $this->db->query("UPDATE maintenance_mode SET white_listed_ips = '$ip' WHERE id = '$id'");
			$this->session->set_flashdata('message', '<p class="message_success">IP Address Updated</p>');
			redirect(base_url() . 'admin/settings/whitelist_ips');
		}else if($func != '' && $func == 'remove_ip' && $id != ''){
		    $this->Setting_model->remove_maintenance_data($id);
			// $this->db->query("DELETE FROM maintenance_mode WHERE id = '$id'");
			$this->session->set_flashdata('message', '<p class="message_success">IP Address Removed</p>');
			redirect(base_url() . 'admin/settings/whitelist_ips');
		}
        $view_data['ips'] = $this->Setting_model->get_maintenance_data($id = '', $return_type = 'result_array');
        /**
        * $select_query = $this->db->query("SELECT * FROM maintenance_mode ORDER BY id ASC");
        * $view_data['ips'] = $select_query->result_array();
        */
		if($this->input->method(TRUE) == 'POST'){
			$data['mode_status'] = ( $this->session->maintenance_status == 1 ? 1 : 0 );
			$data['white_listed_ips'] = $this->input->post('ip_address');
			if($data['white_listed_ips'] != ''){
			    $this->Setting_model->save_maintenance_data($data);
				//$this->db->insert('maintenance_mode', $data);
				$this->session->set_flashdata('message', '<p class="message_success">IP Address Added</p>');
			}else{
				$this->session->set_flashdata('message', '<p class="message_success" style="color: red !important;">Type An IP Address First!</p>');
			}
			redirect($_SERVER['HTTP_REFERER']);
		}
		$this->load->view('layouts/admin/header');
        $this->load->view('layouts/admin/sidebar');
        $this->load->view('admin/settings/whitelist_ips', $view_data);
        $this->load->view('layouts/admin/footer');
	}

	public function test_email_template()
    {

        $templateId = $this->uri->segment(4);
        $emailAddress = $this->uri->segment(5);

        $template = $this->Leads_model->template($templateId);

        $subject = $template->subject;
        $to = $emailAddress;
        $from = $template->email_from;
        $message = $template->content;
        $message = str_replace("{project_title}", $this->config->item('project_title'),
            $message);
        $message = str_replace("{rootpath}", base_url(), $message);
        $message = str_replace("{image}", '<img src="' . base_url() . 'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);

        send_email($emailAddress, $from, $name, $subject, $message);
        /*Send SMS*/
        $this->session->set_flashdata('message',
            '<p class="message_success">Test email sent to ' . $emailAddress . '.</p>');
        redirect($this->config->item('base_url') . 'admin/settings/email_templates');


    }
}