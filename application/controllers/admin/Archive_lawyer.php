<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Archive_lawyer extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('latest_url', current_url());
        chechUserSession();
        $this->load->model('admin/Leads_model');
		$this->load->model('Central_model');
    }
    public function index()
    {
        $user_role = $this->session->role;
        $has_lawyers = false;
        if($user_role == 2){
            $lawyer_id = $this->session->id;
            $archive_leads = $this->db->query("SELECT `leads`.*,`ltl`.`unique_code` AS `lawyer_to_lead_unique_code` FROM `leads` LEFT JOIN `lawyers_to_leads` AS `ltl` ON `leads`.`id` = `ltl`.`lead_id` WHERE `leads`.`is_archive_lawyer` = 1 AND `ltl`.`lawyer_id` = '{$lawyer_id}' ORDER BY `first_name` DESC");
            $has_lawyers = true;
        }else if($user_role == 7){
            $lawyerstaff_id = $this->session->id;
            $lawyers_ids = $this->db->query("SELECT `lawyers_id` FROM `lawyers_lawstaff` WHERE `lawyerstaff_id` = '{$lawyerstaff_id}'")->result_array();
            $array_for_implode = array();
            foreach($lawyers_ids as $lawyer_id){
                $array_for_implode[] = $lawyer_id['lawyers_id'];
            }
            $where_in = implode(",", $array_for_implode);
            if($where_in) {
                $has_lawyers = true;
                $archive_leads = $this->db->query("SELECT `leads`.*,`ltl`.`unique_code` AS `lawyer_to_lead_unique_code` FROM `leads` LEFT JOIN `lawyers_to_leads` AS `ltl` ON `leads`.`id` = `ltl`.`lead_id` WHERE `leads`.`is_archive_lawyer` = 1 AND `ltl`.`lawyer_id` IN ({$where_in}) ORDER BY `first_name` DESC");
            }
        }else if($user_role == 6){
            $lawfirm_id = $this->session->id;
            $lawyers_ids = $this->db->query("SELECT `id` FROM `users` WHERE `law_firms` = '{$lawfirm_id}'")->result_array();
            $array_for_implode = array();
            foreach($lawyers_ids as $lawyer_id){
                $array_for_implode[] = $lawyer_id['id'];
            }
            $where_in = implode(",", $array_for_implode);
            if($where_in) {
                $has_lawyers = true;
                $archive_leads = $this->db->query("SELECT `leads`.*,`ltl`.`unique_code` AS `lawyer_to_lead_unique_code` FROM `leads` LEFT JOIN `lawyers_to_leads` AS `ltl` ON `leads`.`id` = `ltl`.`lead_id` WHERE `leads`.`is_archive_lawyer` = 1 AND `ltl`.`lawyer_id` IN ({$where_in}) ORDER BY `first_name` DESC");
            }
        }
        $data                  = array();
        $data['class_leads']   = 'active';
        if($has_lawyers) {
            $data['archive_leads'] = $archive_leads->result();
        }else{
            $data['archive_leads'] = '';
        }
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/leads/archieve_lawyer', $data);
        $this->load->view('layouts/admin/footer', $data); 
    }
}
?>