<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Login_model');
        $this->load->model('Central_model');
        $this->load->model('admin/Setting_model');
    }
    public function index()
    {
        if ($this->input->get('id')) {
            $checkEmailTrackExist      = $this->Login_model->checkEmailTrack($this->input->get('userid'), $this->input->get('id'));
            $data['user_id']           = $this->input->get('userid');
            $data['email_tracking_id'] = $this->input->get('id');
            $this->Login_model->saveEmailTrackOpenRecords($data);
            $this->Login_model->updateEmailOpenCount($this->input->get('id'));
            if (!$checkEmailTrackExist) {
                $this->Login_model->saveEmailTrack($data);
            }
        }
        $this->load->view('layouts/admin/header');
        $this->load->view('admin/login/login');
        $this->load->view('layouts/admin/footer');
    }
    public function login()
    {
        $email = $this->session->flashdata('email') != '' ? $this->session->flashdata('email') : $this->input->post('email');
        $password = $this->session->flashdata('password') != '' ? $this->session->flashdata('password') : $this->input->post('password');
        $logging_in_user_data = $this->Central_model->select_array('users', array('email' => $email));
        if ($logging_in_user_data == false) {
            $logging_in_user_data = $this->Central_model->select_array('law_firms', array('contact_email' => $email));
        }
        if($logging_in_user_data->verified == 1){
            if ($this->session->userdata('loggin_Blocked') && $logging_in_user_data && $logging_in_user_data->can_login == '1') {
                $this->session->unset_userdata('logged_attempt');
                $this->session->unset_userdata('loggin_Blocked');
            }
            if ($logging_in_user_data && $logging_in_user_data->can_login != '1') {
                $this->session->set_flashdata('message', 'You are blocked, please try again in an hour!');
                redirect($this->config->item('base_url') . 'admin/Login');
            } else {
                if ($this->session->userdata('loggin_Blocked')) {
                    $this->session->set_flashdata('message', 'You have been blocked, please try again in an hour!');
                    redirect($this->config->item('base_url') . 'admin/Login');
                }
                $this->form_validation->set_rules('email', 'Email', 'trim|required', '');
                $this->form_validation->set_rules('password', 'Password', 'trim|required', '');
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                if ($this->session->flashdata('email') == '' && $this->session->flashdata('password') == '' && $this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('message', 'Email and password required!');
                    redirect($this->config->item('base_url') . 'admin/Login');
                } else {
                    $user_table = $this->Central_model->first("users", "email", $email, "password", sha1($password));
                    if ($user_table->law_firms)
                        $count_lawers = $this->Central_model->count_rows("users", array(
                            "law_firms" => $user_table->law_firms
                        ));
                    if ($count_lawers == 1) {
                        $result = $this->Central_model->first("law_firms", "id", $user_table->law_firms);
                        if ($result->contact_email != $email)
                            $result = $user_table;
                    } else if (!$user_table)
                        $result = $this->Central_model->first("law_firms", "contact_email", $email, "password", sha1($password));
                    else
                        $result = $user_table;
                    if ($result) {
    					if ($result->role == 2 || $result->role == 1 || $result->role == 7) {
    						$full_name = $result->name . ' ' . $result->lname;
    					} elseif ($result->role == 6) {
    						$full_name = $result->law_firm;
    					}
                        $this->session->set_userdata(array(
                            'logged_in' => TRUE,
                            'id' => $result->id,
                            'name' => ($result->role == 6 ? $result->contact_name : $result->name),
                            'email' => ($result->role == 6 ? $result->contact_email : $result->email),
                            'full_name' => $full_name,
                            'role' => $result->role
                        ));
                        if ($this->session->userdata('latest_url')){
                            setcookie("status", "", time() - 3600, "/pride");
                            setcookie("case_type", "", time() - 3600, "/pride");
                            setcookie("market", "", time() - 3600, "/pride");
                            setcookie("advance_search_type", "", time() - 3600, "/pride");
                            setcookie("advanceSearch", "", time() - 3600, "/pride");
                            setcookie("sort", "", time() - 3600, "/pride");
                            setcookie("column", "", time() - 3600, "/pride");
                            redirect($this->session->userdata('latest_url'));
                        }else {
                            if ($this->session->userdata('role') == 7 || $this->session->userdata('role') == 2 || $this->session->userdata('role') == 1) {
                                $update_can_login_column = $this->Central_model->update_row('users', array(
                                    'last_login_date' => date("Y-m-d H:i:s"),
                                    'ip_address' => $_SERVER['REMOTE_ADDR']
                                ), array(
                                    'id' => $result->id,
                                    'role' => $result->role
                                ));
                            } else if ($this->session->userdata('role') == 6) {
                                $update_can_login_column = $this->Central_model->update_row('law_firms', array(
                                    'last_login_date' => date("Y-m-d H:i:s"),
                                    'ip_address' => $_SERVER['REMOTE_ADDR']
                                ), array(
                                    'id' => $result->id,
                                    'role' => $result->role
                                ));
                            }
                            if ($this->session->userdata('role') == 7 || $this->session->userdata('role') == 2 || $this->session->userdata('role') == 6) {
                                //redirect($this->config->item('base_url') . 'admin/pages/welcome');
                                if($this->session->go_to_lead_id != '' && $this->session->go_to_lead_code != ''){
                                    $lead_id = $this->session->go_to_lead_id;
                                    $lead_code = $this->session->go_to_lead_code;
                                    unset($this->session->go_to_lead_id);
                                    unset($this->session->go_to_lead_code);
                                    redirect($this->config->item('base_url') . 'admin/leads/view/' . $lead_id . '/' . $lead_code);
                                }else{
                                    redirect($this->config->item('base_url') . 'admin/dashboard/manage');
                                }
                            } else {
    							$data = $this->Setting_model->get_maintenance_data($id = 1);
    							$status = $data->mode_status;
    							$this->session->set_userdata('maintenance_status', $status);
                                setcookie("status", "", time() - 3600, "/pride");
                                setcookie("case_type", "", time() - 3600, "/pride");
                                setcookie("market", "", time() - 3600, "/pride");
                                setcookie("advance_search_type", "", time() - 3600, "/pride");
                                setcookie("advanceSearch", "", time() - 3600, "/pride");
                                setcookie("sort", "", time() - 3600, "/pride");
                                setcookie("column", "", time() - 3600, "/pride");
                                redirect($this->config->item('base_url') . 'admin/dashboard/manage');
                            }
                        }
                    } else {
                        if ($this->session->userdata('logged_attempt')) {
                            $countt = $this->session->userdata('logged_attempt') + 1;
                            $this->session->set_userdata('logged_attempt', $countt);
                            if ($countt >= 5) {
                                if ($logging_in_user_data->role == 1 || $logging_in_user_data->role == 2 || $logging_in_user_data->role == 7) {
                                    $update_can_login_column = $this->Central_model->update_row('users', array(
                                        'can_login' => '0'
                                    ), array(
                                        'email' => $email
                                    ));
                                } elseif ($logging_in_user_data->role == 6) {
                                    $update_can_login_column = $this->Central_model->update_row('law_firms', array(
                                        'can_login' => '0'
                                    ), array(
                                        'contact_email' => $email
                                    ));
                                }
                                $this->session->set_userdata('loggin_Blocked', '1');
                                $this->session->set_flashdata('message', 'You have tried to enter your username/password wrong multiple times and system has blocked you. Please try after an hour.');
                                redirect($this->config->item('base_url') . 'admin/Login');
                            }
                        } else {
                            $this->session->set_userdata('logged_attempt', 1);
                        }
                        $this->session->set_flashdata('message', 'Invalid username or password.');
                        redirect($this->config->item('base_url') . 'admin/Login');
                    }
                }
            }
        }else{
            $this->session->set_flashdata('message', 'Please verify your account before login.');
            redirect($this->config->item('base_url') . 'admin/Login');
        }
    }
    function logout()
    {
		/* $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role');
        $this->session->unset_userdata('latest_url'); */
        if($this->session->role == 1){
            $data['mode_status'] = 0;
            $this->Setting_model->update_maintenance_data($data);
        }
        $this->session->sess_destroy();
        redirect($this->config->item('base_url') . 'admin/login');
    }
    
    function check_redirection($lead_id, $unique_code)
    {
        $lead = $this->Central_model->select_array('lawyers_to_leads', array('lead_id' => $lead_id, 'unique_code' => $unique_code));
        //debug($lead);
        if($lead){
            $billed_id = $lead->id;
            redirect(base_url() . 'admin/leads/updateLawyerToLeadBilled/' . $billed_id . '/' . $unique_code);
        }else{
            show_404();
        }
    }
}