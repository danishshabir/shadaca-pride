<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class EmailTracking extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('latest_url', current_url());
        chechUserSession();
        $this->load->model('admin/Users_model');
        $this->load->model('admin/Mail_model');
        $this->load->library('email');
    }
    public function index()
    {
        $data                        = array();
        $data['class_admin']         = 'active';
        $id                          = $this->session->userdata('id');
        $template_id                 = 1;
        $data['email_template_data'] = $this->Mail_model->fetchEmailTemplatedata($template_id);
        $data['users']               = $this->Mail_model->fetchallusers();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/mail/email_editor', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function tracking_record()
    {
        $data['class_admin'] = 'active';
        $id                  = $this->session->userdata('id');
        $data['all_record']  = $this->Mail_model->getAllRecord();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/mail/tracking_record');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function subscriberDetail()
    {
        $track_id            = $this->input->post('track_id');
        $sender_details_id   = array();
        $data['class_admin'] = 'active';
        $sender_details_id   = explode(',', $this->input->post('send_to'));
        foreach ($sender_details_id as $sender_details_ids) {
            $sender_details             = $this->Mail_model->fetchsubscriberDetail($sender_details_ids);
            $data['subscriberDetail'][] = $sender_details;
        }
        $data['email_track_id'] = $track_id;
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/mail/subscriberDetail', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
}
