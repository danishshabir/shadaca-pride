<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Users extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('latest_url', current_url());
        $type_reg = $this->uri->segment(3);
        if ($type_reg != 'register') {
            chechUserSession();
        } //$type_reg != 'register'
        $this->load->model('admin/Users_model');
        $this->load->model('admin/Leads_model');
		$this->load->model('admin/Setting_model');
        $this->load->library('email');
        $this->load->library('form_validation');
		$this->load->model('Central_model');
    }
    public function index()
    {
        $this->admin();
    }
    public function admin()
    {
        $data                = array();
        $role                = 1;
        $data['users']       = $this->Users_model->fetchAllUsers($role);
        $data['case_type']   = $this->Leads_model->fetchCaseType();
        $data['class_admin'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/admins', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function view_lawyer($id)
    {
        $data           = array();
        $data['id']     = $this->uri->segment(4);
        $data['lawyer'] = $this->Users_model->fetchRowLawyer($data['id']);
		
        $this->load->view('layouts/admin/header');
        $this->load->view('layouts/admin/sidebar');
        $this->load->view('admin/users/lawyers_list', $data);
        $this->load->view('layouts/admin/footer');
    }
    public function view_lawyer_staff($id)
    {
        $data           = array();
        $data['id']     = $this->uri->segment(4);
        $data['lawyer'] = $this->Users_model->fetchRowLawyer($data['id']);
        $lawyer_id      = $data['lawyer']->lawyer;
        (count(explode(',', $lawyer_id)) > 1 ? $data['curr_lawyer'] = 'All' : $data['curr_lawyer'] = $this->Users_model->fetchLawyerDetails($lawyer_id));
        /**
        * echo '<pre>';
        * print_r($data['curr_lawyer']); exit;
        */
        $this->load->view('layouts/admin/header');
        $this->load->view('layouts/admin/sidebar');
        $this->load->view('admin/users/lawyer_staff_list', $data);
        $this->load->view('layouts/admin/footer');
    }
    public function lawyers()
    {
        $data                 = array();
        $role                 = 2;
        $data['users']        = $this->Users_model->fetchAllUsers($role);
        $data['case_type']    = $this->Leads_model->fetchCaseType();
        $data['class_lawyer'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/lawyers', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function lawyerStaff()
    {
		if($this->session->view_as != ''){
			$admin_id = $this->session->backup_admin_id;
			$this->session->set_userdata(array('role' => 1, 'id' => $admin_id, 'view_as' => ''));
		}
        $data                 = array();
        $role                 = 7;
        $data['users']        = $this->Users_model->fetchAllUsers($role, $view = 'lawyer_staff');
        foreach($data['users'] as $user){
            $lawyer_id = $user->lawyer;
            (count(explode(',', $lawyer_id)) > 1 ? $data['curr_lawyer'][] = 'All' : $data['curr_lawyer'][] = $this->Users_model->fetchLawyerDetails($lawyer_id));
        }
        $data['case_type']    = $this->Leads_model->fetchCaseType();
        $data['class_lawyer'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/lawyerStaff', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function operators()
    {
        $data                 = array();
        $role                 = 5;
        $data['users']        = $this->Users_model->fetchAllOperatorsUsers($role);
        $data['case_type']    = $this->Leads_model->fetchCaseType();
        $data['class_lawyer'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/operators', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function add()
    {
        $data = array();
        $this->load->model('admin/users_model');
		if($this->input->method(TRUE) == 'POST') {
			$this->load->library('form_validation');
			$path = './uploads';
			$data               = $this->input->post();
			$rules = array(
				[
					'field' => 'name',
					'label' => 'First Name',
					'rules' => (isset($data['name']) ? 'trim|required' : ''),
				],
				[
					'field' => 'lname',
					'label' => 'Last Name',
					'rules' => (isset($data['lname']) ? 'trim|required' : ''),
				],
				[
					'field' => 'email',
					'label' => 'Email',
					'rules' => (isset($data['email']) ? 'trim|required|valid_email|callback_check_email' : ''),
				],
				[
					'field' => 'phone',
					'label' => 'Phone',
					'rules' => (isset($data['phone']) ? 'trim|required' : ''),
				],
				[
					'field' => 'law_firms',
					'label' => 'Law Firm',
					'rules' => (isset($data['law_firms']) ? 'trim|required' : ''),
				],
				[
					'field' => 'address',
					'label' => 'Address',
					'rules' => (isset($data['address']) ? 'trim|required' : ''),
				],
				[
					'field' => 'city',
					'label' => 'City',
					'rules' => (isset($data['city']) ? 'trim|required' : ''),
				],
				[
					'field' => 'state',
					'label' => 'State',
					'rules' => (isset($data['state']) ? 'trim|required' : ''),
				],
				[
					'field' => 'zipcode',
					'label' => 'Zip Code',
					'rules' => (isset($data['zipcode']) ? 'trim|required' : ''),
				],
				[
					'field' => 'initial_setup_charge',
					'label' => 'Initial Setup Charge',
					'rules' => (isset($data['initial_setup_charge']) ? 'trim|required' : ''),
				],
				[
					'field' => 'fee_type',
					'label' => 'Fee Type',
					'rules' => (isset($data['fee_type']) ? 'trim|required' : ''),
				],
				[
					'field' => 'law_firm_fee',
					'label' => 'Law Firm Fee',
					'rules' => (isset($data['law_firm_fee']) ? 'trim|required' : ''),
				],
				[
					'field' => 'image',
					'label' => 'Image',
					'rules' => 'callback_upload_image['.$path.']',
				],
				[
					'field' => 'active',
					'label' => 'Active',
					'rules' => (isset($data['active']) ? 'trim|required' : ''),
				],
				[
					'field' => 'case_type[]',
					'label' => 'Case Type',
					'rules' => ($this->uri->segment(4) == 2 ? 'trim|required' : ''),
				],
				[
					'field' => 'jurisdiction[]',
					'label' => 'Jurisdiction',
					'rules' => ($this->uri->segment(4) == 2 ? 'trim|required' : ''),
				],
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
				$this->load->helper('string');
				$type_reg           = $this->uri->segment(3);
				$data['role']       = $this->uri->segment(4);
                $data['send_email'] = 1;
				$data['created_at'] = date('Y-m-d H:i:s');
				$data['password']   = sha1($this->input->post('password'));
                if($data['role'] == 1){
                    $data['verified'] = 1;
                }else{
                    $data['verified'] = 0;
                }
				$data['unique_code'] = random_string('alnum',20);
				$data['image']      = $_FILES['image']['name'];
				unset($data['submit']);
				$LawyerAddTemplate   = $this->Leads_model->template(1);
				$name     = $this->input->post('name');
				$lname    = $this->input->post('lname');
				$fullname = $name . " " . $lname;
				$username = $this->input->post('name');
				$password = $this->input->post('password');
				$subject  = $LawyerAddTemplate->subject;
				$message = $LawyerAddTemplate->content;
				$message = str_replace("{email}", $data['email'], $message);
				$message = str_replace("{password}", $password, $message);
				$message = str_replace("{name}", $name, $message);
				$message = str_replace("{username}", $data['email'], $message);
				$message = str_replace("{organization name}", "Pride legal", $message);
				$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
				$message = str_replace("{rootpath}", base_url(), $message);
				$message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
				$message = str_replace("{link}", '<a href="'.base_url().'admin/ResetPassword/reset/lawyer/'.$data['unique_code'].'">'.base_url().'admin/ResetPassword/reset/lawyer/'.$data['unique_code'].'</a>', $message);
				$from               = $LawyerAddTemplate->email_from;
				$name          = $this->config->item('project_title');
				$to                 = $data['email'];
				/*Send Email*/
				send_email($to,$from,$name,$subject,$message);
				/*Send SMS*/
				$sms_message = $LawyerAddTemplate->text_message;
				$sms_message = str_replace("{project_title}", $this->config->item('project_title'), $sms_message);
				sms(1, $data['phone'], $sms_message);
				if ($data['role'] == 2) {
					$data['case_type']    = implode(",", $this->input->post('case_type'));
					$data['jurisdiction'] = implode(",", $this->input->post('jurisdiction'));
				} //$data['role'] == 2
                if ($data['role'] == 7) {
					if($this->input->post('lawyer') == 0){
						$lawyer_ids = array();
						$all_lawyers = $this->Users_model->getLawfirmLawyers($this->input->post('law_firms'));
						foreach($all_lawyers as $lawyer){
							$lawyer_ids[] = $lawyer['id'];
						}
						$data['lawyer'] = implode(',', $lawyer_ids);
					}else{
						$lawyer_ids = $this->input->post('lawyer');
						$data['lawyer'] = $lawyer_ids;
					}
                    $data['send_email'] = 0;
				}
				$result = $this->Users_model->save($data);
                if($data['role'] == 7){
					$lawyerstaff['lawfirm_id'] = $this->input->post('law_firms');
					$lawyerstaff['lawyerstaff_id'] = $result;
					if($this->input->post('lawyer') == 0){
						$all_lawyers = $this->Users_model->getLawfirmLawyers($this->input->post('law_firms'));
						foreach($all_lawyers as $lawyer){
							$lawyerstaff['lawyers_id'] = $lawyer['id'];
                            $lawyerstaff['send_email'] = $lawyer['send_email'];
							$this->Users_model->add_lawyerstaff($lawyerstaff);
						}
					}else{
						$lawyerstaff['lawyers_id'] = $this->input->post('lawyer');
                        $lawyer = $this->Users_model->fetchLawyerDetails($lawyerstaff['lawyers_id']);
                        $lawyerstaff['send_email'] = $lawyer->send_email;
						$this->Users_model->add_lawyerstaff($lawyerstaff);
					}					
				}
				if ($result) {
					$lawyer_data                     = array();
					$lawyer_all_data                 = $this->Users_model->fetchRow($result);
					$lawyer_data['lawyer_id']        = $lawyer_all_data->id;
					$lawyer_data['amount']           = $lawyer_all_data->initial_setup_charge;
					$lawyer_data['status']           = '3';
					$lawyer_data['transaction_date'] = date('Y-m-d H:i:s');
					$this->Users_model->saveLawyerInitialSetupCharge($lawyer_data);
					$this->session->set_flashdata('message', '<p class="message_success">Added successfully.</p>');
					if ($data['role'] == 1) {
						redirect($this->config->item('base_url') . 'admin/users/admin');
					} //$data['role'] == 1
					elseif ($data['role'] == 2) {
						if ($type_reg == 'register') {
							redirect($this->config->item('base_url') . 'lawyer/lawyer_index/register/2');
						} //$type_reg == 'register'
						else {
							redirect($this->config->item('base_url') . 'admin/users/lawyers');
						}
					} //$data['role'] == 2
                    elseif ($data['role'] == 7) {
						if ($type_reg == 'register') {
							redirect($this->config->item('base_url') . 'lawyer/lawyer_index/register/7');
						} //$type_reg == 'register'
						else {
							redirect($this->config->item('base_url') . 'admin/users/lawyerStaff');
						}
					} //$data['role'] == 7
					elseif ($data['role'] == 5) {
						if ($type_reg == 'register') {
							redirect($this->config->item('base_url') . 'lawyer/lawyer_index/register/5');
						} //$type_reg == 'register'
						else {
							redirect($this->config->item('base_url') . 'admin/users/operators');
						}
					} //$data['role'] == 5
				} //$result
			}
		}
		$data['law_firms'] = $this->users_model->lawTypee();
		$data['role']      = $this->uri->segment(4);
		if ($data['role'] == 1) {
			$data['type']        = 'Admin';
			$data['class_admin'] = 'active';
		} //$data['role'] == 1
		elseif ($data['role'] == 2) {
			$data['type']         = 'Lawyer';
			$data['class_lawyer'] = 'active';
		} //$data['role'] == 2
        elseif ($data['role'] == 7) {
			$data['type']         = 'Lawyer Staff';
			$data['class_lawyer'] = 'active';
		} //$data['role'] == 7
		elseif ($data['role'] == 5) {
			$data['type']         = 'Operator';
			$data['class_lawyer'] = 'active';
		} //$data['role'] == 5
		$data['law_firms']          = $this->Users_model->fetchAllLawFirms();
		$data['case_type']          = $this->Users_model->fetchAllCaseTpe();
		$data['jurisdictions']      = $this->Users_model->fetchAllJurisdictions();
		$data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/add', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
	public function check_email($email, $id= Null)
	{
		if($id)
			$count = $this->Central_model->count_rows("users", array("email" => $email), array("id" => $id));
		else 
			$count = $this->Central_model->count_rows("users", array("email" => $email));
		
		if($count > 0)
		{
			$this->form_validation->set_message('check_email', "User email already exists. Duplicates not allowed!");
			return FALSE;
		}
		
		return TRUE;
	}
	public function upload_image($name, $path=Null)
	{
		$config['upload_path'] = $path;
		$config['max_size'] = 1024 * 10;
		$config['allowed_types'] = 'gif|png|jpg|jpeg';
		//$config['encrypt_name'] = TRUE;
		$this->load->library('upload', $config);
		if(isset($_FILES['image']) && !empty($_FILES['image']['name']))
		{
			if($this->upload->do_upload('image'))
			{
				$upload_data = $this->upload->data();
				$_POST['image'] = $upload_data['file_name'];
				return TRUE;
			}
			else
			{
				$this->form_validation->set_message('upload_image', "The File type you are uploading is not allowed!");
				return FALSE;
			}
		}
		return TRUE;
	}
    public function edit($id)
    {
        $data = array();
        $this->load->model('admin/users_model');
		if($this->input->method(TRUE) == 'POST') {
			$this->load->library('form_validation');
			$path = './uploads';
			$data               = $this->input->post();
			$rules = array(
				[
					'field' => 'name',
					'label' => 'First Name',
					'rules' => (isset($data['name']) ? 'trim|required' : ''),
				],
				[
					'field' => 'lname',
					'label' => 'Last Name',
					'rules' => (isset($data['lname']) ? 'trim|required' : ''),
				],
				[
					'field' => 'email',
					'label' => 'Email',
					'rules' => (isset($data['email']) ? 'trim|required|valid_email|callback_check_email['.$id.']' : ''),
				],
				[
					'field' => 'phone',
					'label' => 'Phone',
					'rules' => (isset($data['phone']) ? 'trim|required' : ''),
				],
				[
					'field' => 'law_firms',
					'label' => 'Law Firm',
					'rules' => (isset($data['law_firms']) ? 'trim|required' : ''),
				],
				[
					'field' => 'address',
					'label' => 'Address',
					'rules' => (isset($data['address']) ? 'trim|required' : ''),
				],
				[
					'field' => 'city',
					'label' => 'City',
					'rules' => (isset($data['city']) ? 'trim|required' : ''),
				],
				[
					'field' => 'state',
					'label' => 'State',
					'rules' => (isset($data['state']) ? 'trim|required' : ''),
				],
				[
					'field' => 'zipcode',
					'label' => 'Zip Code',
					'rules' => (isset($data['zipcode']) ? 'trim|required' : ''),
				],
				[
					'field' => 'initial_setup_charge',
					'label' => 'Initial Setup Charge',
					'rules' => (isset($data['initial_setup_charge']) ? 'trim|required' : ''),
				],
				[
					'field' => 'fee_type',
					'label' => 'Fee Type',
					'rules' => (isset($data['fee_type']) ? 'trim|required' : ''),
				],
				[
					'field' => 'law_firm_fee',
					'label' => 'Law Firm Fee',
					'rules' => (isset($data['law_firm_fee']) ? 'trim|required' : ''),
				],
				[
					'field' => 'image',
					'label' => 'Image',
					'rules' => 'callback_upload_image['.$path.']',
				],
				[
					'field' => 'active',
					'label' => 'Active',
					'rules' => (isset($data['active']) ? 'trim|required' : ''),
				],
				[
					'field' => 'case_type[]',
					'label' => 'Case Type',
					'rules' => ($this->input->post('role') == 2 ? 'trim|required' : ''),
				],
				[
					'field' => 'jurisdiction[]',
					'label' => 'Jurisdiction',
					'rules' => ($this->input->post('role') == 2 ? 'trim|required' : ''),
				],
				/* [
					'field' => 'holiday_from',
					'label' => 'Holiday From',
					'rules' => ($this->input->post('role') == 2 ? 'trim|required' : ''),
				],
				[
					'field' => 'holiday_to',
					'label' => 'Holiday To',
					'rules' => ($this->input->post('role') == 2 ? 'trim|required' : ''),
				], */
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
				$data['holiday_from'] = _dateFormat($data['holiday_from']);
				$data['holiday_to'] = _dateFormat($data['holiday_to']);
				$data['updated_at'] = date('Y-m-d H:i:s');
				if ($_FILES['image']['name'] != '') {
					$data['image'] = ($_FILES["image"]["name"]);
				} //$img != ''
				else {
					$data['image'] = $this->input->post('image');
				}
				if ($this->input->post('role') == 2) {
					$data['case_type']    = implode(",", $this->input->post('case_type'));
					$data['jurisdiction'] = implode(",", $this->input->post('jurisdiction'));
				} //$this->input->post('role') == 2
				unset($data['submit']);
                if ($this->input->post('role') == 7) {
					$this->Users_model->delete_from_lawyers_lawstaff($id);
					$lawyerstaff['lawfirm_id'] = $this->input->post('law_firms');
					$lawyerstaff['lawyerstaff_id'] = $id;
					if($this->input->post('lawyer') == 0){
						$all_lawyers = $this->Users_model->getLawfirmLawyers($this->input->post('law_firms'));
						foreach($all_lawyers as $lawyer){
							$lawyerstaff['lawyers_id'] = $lawyer['id'];
							$lawyerstaff['send_email'] = $lawyer['send_email'];
							$this->Users_model->add_lawyerstaff($lawyerstaff);
							$lawyer_ids[] = $lawyer['id'];
						}
						$data['lawyer'] = implode(',', $lawyer_ids);
					}else{
						$lawyerstaff['lawyers_id'] = $this->input->post('lawyer');
                        $lawyer = $this->Users_model->fetchLawyerDetails($lawyerstaff['lawyers_id']);
                        $lawyerstaff['send_email'] = $lawyer->send_email;
						$this->Users_model->add_lawyerstaff($lawyerstaff);
						$lawyer_ids = $this->input->post('lawyer');
						$data['lawyer'] = $lawyer_ids;
					}
				}
				$update = $this->Users_model->update($data, $id);
				if ($update) {
					$this->session->set_flashdata('message', '<p class="message_success"> Updated successfully.</p>');
					redirect($this->config->item('base_url') . 'admin/users/edit/' . $id);
				} //$update
			}
		}
        $data['law_firms']          = $this->users_model->lawTypee();
        $data['id']                 = $id;
        $data['user']               = $this->Users_model->fetchRow($data['id']);
        $lawfirm_id                 = $data['user']->law_firms;
		$lawyer_id                  = $data['user']->lawyer;
        (count(explode(',', $lawyer_id)) > 1 ? $data['curr_lawyer'] = 'All' : $data['curr_lawyer'] = $this->Users_model->fetchLawyerDetails($lawyer_id));
        (count(explode(',', $lawyer_id)) > 1 ? $data['other_lawyers'] = $this->Users_model->getLawfirmLawyers($lawfirm_id) : $data['other_lawyers'] = $this->Users_model->getLawfirmLawyers($lawfirm_id, $lawyer_id));
        $data['case_type']          = $this->Users_model->fetchAllCaseTpe();
        $data['jurisdiction']       = $this->Users_model->fetchAllJurisdictions();
        $data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
        $role                       = $data['user']->role;
        if ($role == 1) {
            $data['type']        = 'Admin';
            $data['class_admin'] = 'active';
        } //$role == 1
        elseif ($role == 2) {
            $data['type']         = 'Lawyer';
            $data['class_lawyer'] = 'active';
        } //$role == 2
        elseif ($role == 7) {
            $data['type']         = 'Lawyer Staff';
            $data['class_lawyer'] = 'active';
        } //$role == 7
            elseif ($role == 5) {
            $data['type']         = 'Operator';
            $data['class_lawyer'] = 'active';
        } //$role == 5
        if (!empty($data['user'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/users/edit', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['user'])
    }
    public function updateProfile()
    {
        $id                 = $this->session->userdata('id');
        $data               = array();
        $data               = $this->input->post();
        $data['updated_at'] = date('Y-m-d H:i:s');
        $image_hidden       = $this->input->post('image');
        $img                = $_FILES['image']['name'];
        if ($img != '') {
            $type     = $_FILES['image']['type'];
            $size     = $_FILES['image']['size'];
            $img_temp = $_FILES['image']['tmp_name'];
            $error    = $_FILES['image']['error'];
            if ($type == "image/png" || $type == "image/jpeg" || $type == "image/jpg") {
                move_uploaded_file($img_temp, "uploads/$img");
                $data['image'] = ($_FILES["image"]["name"]);
            } //$type == "image/png" || $type == "image/jpeg" || $type == "image/jpg"
        } //$img != ''
        else {
            $data['image'] = $image_hidden;
        }
        unset($data['submit']);
        $update = $this->Users_model->update($data, $id);
        if ($update) {
            $this->session->set_flashdata('message', '<p class="message_success"> Updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/users/userProfile/' . $id);
        } //$update
    }
    public function update_holiday()
    {
        $id   = $this->uri->segment(4);
        $data = array();
        $data = $this->input->post();
		$data['holiday_from'] = _dateFormat($data['holiday_from']);
		$data['holiday_to'] = _dateFormat($data['holiday_to']);
        unset($data['submit']);
        $update = $this->Users_model->update_vacation($data, $id);
        $this->session->set_flashdata('message', '<p class="message_success"> Holiday Added successfully.</p>');
        redirect($this->config->item('base_url') . 'admin/dashboard/manage/' . $id);
    }
    public function delete()
    {
        $id   = $this->uri->segment(4);
        $role = $this->uri->segment(5);
        if ($id) {
            if($role == 7){
				$this->Users_model->delete_lawstaff($id);
			}
            $result = $this->Users_model->deleteRow($id);
            if ($result) {
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                if ($role == 1) {
                    redirect($this->config->item('base_url') . 'admin/users/admin/');
                } //$role == 1
                elseif ($role == 2) {
                    redirect($this->config->item('base_url') . 'admin/users/lawyers/');
                } //$role == 2
                elseif ($role == 7) {
                    redirect($this->config->item('base_url') . 'admin/users/lawyerStaff/');
                } //$role == 7
                elseif ($role == 5) {
                    redirect($this->config->item('base_url') . 'admin/users/operators/');
                } //$role == 5
            } //$result
        } //$id
    }
    public function register()
    {
        $data         = array();
        $data['role'] = $this->uri->segment(4);
        if ($data['role'] == 1) {
            $data['type']        = 'Admin';
            $data['class_admin'] = 'active';
        } //$data['role'] == 1
        elseif ($data['role'] == 2) {
            $data['type']         = 'Lawyer';
            $data['class_lawyer'] = 'active';
        } //$data['role'] == 2
        $this->load->view('admin/users/register', $data);
    }
    public function follow_up()
    {
        $get_lawyer_assigned = $this->Users_model->fetchAllAssignLawyers();
        $hours               = $this->Users_model->fetchSetting('follow_up_hours');
        foreach ($get_lawyer_assigned as $assigned) {
            $date = new DateTime($assigned->assigned_date);
            $date->modify("+" . $hours->follow_up_hours . " hours");
            $email_time     = $date->format("Y-m-d H:i:s");
            $current_date   = date('Y-m-d H:i:s');
            $last_cron_date = date('Y-m-d H:i:s', strtotime('-1 hours'));
            if ($email_time > $last_cron_date && $email_time <= $current_date) {
                $to                 = $assigned->email;
                $from               = 'support@pridelegal.com';
                $name          = $this->config->item('project_title');
                $subject            = 'Lead Assigned';
                $message            = 'Hi ' . $assigned->name . ', <br /><br /> The Lead name ' . $assigned->lead_name . ' is assigned to you.<br /> Please login to your Pride Legal account on the following URl: <a href="' . base_url() . 'admin" >' . base_url() . 'admin</a> to see the latest leads.<br /><br /> Thanks<br />Regards: '.$this->config->item('project_title');
                send_email($to,$from,$name,$subject,$message);
            } //$email_time > $last_cron_date && $email_time <= $current_date
        } //$get_lawyer_assigned as $assigned
    }
    public function resetPassword()
    {
        $data                = array();
		$id = $this->session->userdata('id');
		if($this->session->userdata('role') == 6) 
			$this->Central_model->update("law_firms",array("login_first_time" => 1),'id',$id);
        $data['class_admin'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/reset_password', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
	public function company_logo()
    {
		$data      = array();
		$id = $this->session->userdata('id');
		$user_role = $this->session->userdata('role');
		if($user_role == 6) 
		{
			$data['law_firm'] = $this->Central_model->first("law_firms","id",$id);
			 $this->load->view('layouts/admin/header', $data);
			$this->load->view('layouts/admin/sidebar', $data);
			$this->load->view('admin/users/law_firm_logo', $data);
			$this->load->view('layouts/admin/footer', $data);
		}
	}
    public function userProfile()
    {
        $data      = array();
        $user_role = $this->session->userdata('role');
		if($user_role == 6) 
		{
			$id = $this->session->userdata('id');
			$data['class_leads']        = 'active';
			$data['lead']               = $this->Setting_model->fetchLawRow($id);
			$data['case_type']          = $this->Leads_model->fetchCaseType();
			$data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
			$data['users'] = $this->Central_model->select_all_array('users', array('law_firms'=> $id, 'role'=> 2));
            foreach($data['users'] as $user){
                $lawyer_ids[] = $user->id;
            }
            $data['lawstaff'] = $this->Users_model->getLawyersLawstaff($lawyer_ids, $type = 'multi');
            //last_query();
            foreach($data['lawstaff'] as $lawstaff){
                $lawyer_id = $lawstaff['lawyer'];
                (count(explode(',', $lawyer_id)) > 1 ? $data['curr_lawyer'][] = 'All' : $data['curr_lawyer'][] = $this->Users_model->fetchLawyerDetails($lawyer_id));                
            }
			$data['law_firm'] = $this->Central_model->first("law_firms","id",$id);
			$view = 'admin/users/law_firm_profile';
		} else {
			if ($user_role == '1') {
				$id = $this->uri->segment(4);
			} //$user_role == '1'
			else {
				$id = $this->session->userdata('id');
                $data['lawstaff'] = $this->Users_model->getLawyersLawstaff($id);
			}
			$data['jurisdiction']     = $this->Users_model->fetchAllJurisdictions();
			$data['user_information'] = $this->Leads_model->fetchUserInformation($id);
            $lawyer_id = $data['user_information']->lawyer;
            (count(explode(',', $lawyer_id)) > 1 ? $data['curr_lawyer'] = 'All' : $data['curr_lawyer'] = $this->Users_model->fetchLawyerDetails($lawyer_id));
			$data['class_admin']      = 'active';
			$view = 'admin/users/user_Profile';
		}
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view($view, $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function UpdateUserProfile()
    {
        $data      = array();
        $user_role = $this->session->userdata('role');
        if ($user_role == '1') {
            $id = $this->uri->segment(4);
        } //$user_role == '1'
        else {
            $id = $this->session->userdata('id');
        }
        $data['user_information']   = $this->Leads_model->fetchUserInformation($id);
        $data['class_admin']        = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/edit_Profile', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function reset_password()
    {
		$this->load->library('form_validation');
		$id               = $this->input->post('user_id');
		$rules = array(
			[
				'field' => 'old',
				'label' => 'Old Password',
				'rules' => 'callback_check_old_password[' . $id . ']',
			],
			[
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'callback_valid_password',
			],
			[
				'field' => 'confirm_password',
				'label' => 'Confirm Password',
				'rules' => 'matches[password]',
			],
		);
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run())
		{
			$old              = $this->input->post('old');
			$password         = $this->input->post('password');
			$confirm_password = $this->input->post('confirm_password');
			$data['password'] = sha1($this->input->post('password'));
			if($this->session->userdata('role') == 6)
			{
				$result = $this->Central_model->update("law_firms",$data,'id',$id);
			}
			else 
			{
				$result = $this->Central_model->update("users",$data,'id',$id);
			}
			if ($result) {
				$template = $this->Leads_model->template(3);
				$to                     = ($this->session->userdata('role') == 6 ? $userRecord->contact_email : $userRecord->email);
				$from                   = $template->email_from;
				$name              = $this->config->item('project_title');;
				$subject                = $template->subject;
				$password               = sha1($this->input->post('password'));
				$message = $template->content;
				$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
				$message = str_replace("{name}", ($this->session->userdata('role') == 6 ? $userRecord->contact_name : $userRecord->name), $message);
				$message = str_replace("{username}", ($this->session->userdata('role') == 6 ? $userRecord->contact_name : $userRecord->name), $message);
				$message = str_replace("{rootpath}", base_url(), $message);
				$message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);	
				send_email($to,$from,$name,$subject,$message);
				/*Send SMS*/
				$sms_message = $template->text_message;
				$sms_message = str_replace("{project_title}", $this->config->item('project_title'), $sms_message);
				sms(1, $userRecord->phone, $sms_message);
				$this->session->set_flashdata('message_success', '<p class="message_success">Password Reset successfully.</p>');
				redirect($this->config->item('base_url') . 'admin/users/resetPassword/');
			} else { //$result
				$this->session->set_flashdata('message_success', '<p class="message_success">Same password again set successfully.</p>');
				echo "asd";
				redirect($this->config->item('base_url') . 'admin/users/resetPassword/');
			}
		} else {
			$data['class_admin'] = 'active';
			$this->load->view('layouts/admin/header', $data);
			$this->load->view('layouts/admin/sidebar', $data);
			$this->load->view('admin/users/reset_password', $data);
			$this->load->view('layouts/admin/footer', $data);
		}
    }
	public function check_old_password($old = '', $id)
	{
		if($this->session->userdata('role') == 6) {
			$count = $this->Central_model->count_rows("law_firms", array("id" => $id, "password" => sha1($old)));
			$userRecord       = $this->Central_model->first("law_firms", "id", $id);
		} else {
			$count        = $this->Central_model->count_rows("users", array("id" => $id, "password" => sha1($old)));
			$userRecord       = $this->Central_model->first("users", "id", $id);
		}
		
		if (!$count) {
           $this->form_validation->set_message('check_old_password', "Old password doesn't match.");
			return FALSE;
        } //!$count
		
		return TRUE;
	}
	public function valid_password($password = '')
	{
		$password = trim($password);
		$regex_lowercase = '/[a-z]/';
		$regex_uppercase = '/[A-Z]/';
		$regex_number = '/[0-9]/';
		$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';
		if (empty($password))
		{
			$this->form_validation->set_message('valid_password', 'The {field} field is required.');
			return FALSE;
		}
		if (preg_match_all($regex_lowercase, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least one lowercase letter.');
			return FALSE;
		}
		if (preg_match_all($regex_uppercase, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least one uppercase letter.');
			return FALSE;
		}
		if (preg_match_all($regex_number, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must have at least one number.');
			return FALSE;
		}
		/* if (preg_match_all($regex_special, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must have at least one special character.' . ' ' . htmlentities('!@#$%^&*()\-_=+{};:,<.>§~'));
			return FALSE;
		} */
		if (strlen($password) < 6)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least 6 characters in length.');
			return FALSE;
		}
		if (strlen($password) > 32)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field cannot exceed 32 characters in length.');
			return FALSE;
		}
		return TRUE;
	}
	
	public function manage_payment_profile($id)
	{
		$data      = array();
		$this->load->library(array('functions'));
        $user_role = $this->session->userdata('role');
		$id = ($user_role == 6 ? $this->session->userdata('id') : $id);
		if($this->input->method(TRUE) == "POST")
		{
			$data = $this->input->post();
			unset($data['submit']);
			$state_rec = $this->Central_model->select_max_field("jurisdiction_states", array("id" => $data['state']), 'name');
			$data['state'] = $state_rec->name;
			$response = $this->functions->createCustomerProfile($data);
			if($response['success'] == 0)
			{
				$this->session->set_flashdata('message', '<p class="message_warning"> '.$response['error'].'</p>');
			} else if($response['success'] == 1) {
				$credit_card_info = array(
					'profile_id' => $response['profile_id'],
					'customer_profile_id' => $response['customer_profile_id'],
					'law_firm' => $id
				); //'cw_code' => $data['cw_code'],
				$this->Central_model->save('credit_cards', $credit_card_info);
				$this->session->set_flashdata('message', '<p class="message_success"> Payment Profile Added Successfully!</p>');
			}
			redirect($this->config->item('base_url') . 'admin/users/manage_payment_profile/' . $id);
		} 
		else 
		{
			$data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
			$data['law_firm'] = $this->Central_model->first("law_firms","id",$id);
			$data['CustomerProfiles'] = $this->Central_model->select_all_array("credit_cards",array("law_firm" => $id));
			$this->load->view('layouts/admin/header', $data);
			$this->load->view('layouts/admin/sidebar', $data);
			$this->load->view('admin/users/manage_payment_profile', $data);
			$this->load->view('layouts/admin/footer', $data);
		}
	}
	public function delete_payment_profile($law_firm_id, $customer_profile_id)
	{
		$user_role = $this->session->userdata('role');
		$law_firm_id = ($user_role == 6 ? $this->session->userdata('id') : $law_firm_id);
		$customer_profile = $this->Central_model->first("credit_cards","law_firm", $law_firm_id, "id", $customer_profile_id);
		if($customer_profile)
		{
			$this->load->library(array('functions'));
			$response = $this->functions->deleteCustomerPaymentProfile($customer_profile->customer_profile_id,$customer_profile->profile_id);
			if($response['success'] == 1)
			{
				$this->Central_model->del("credit_cards",array("id" => $customer_profile_id));
				$this->session->set_flashdata('message', '<p class="message_success">'.$response['msg'].'</p>');
			} else {
				$this->session->set_flashdata('message', '<p class="message_warning"> '.$response['error'].'</p>');
			}
			
			redirect($this->config->item('base_url') . 'admin/users/manage_payment_profile/' . $law_firm_id);
		} 
	}
	public function default_payment_profile($law_firm_id, $customer_profile_id)
	{
		$user_role = $this->session->userdata('role');
		$law_firm_id = ($user_role == 6 ? $this->session->userdata('id') : $law_firm_id);
		$customer_profile = $this->Central_model->first("credit_cards","law_firm", $law_firm_id, "id", $customer_profile_id);
		if($customer_profile)
		{
			$this->Central_model->update("credit_cards", array("default" => 0), "law_firm", $law_firm_id);
			$this->Central_model->update("credit_cards", array("default" => 1), "law_firm", $law_firm_id, "id", $customer_profile_id);
			$this->session->set_flashdata('message', '<p class="message_success">Payment Profile Update Successfully.</p>');
			redirect($this->config->item('base_url') . 'admin/users/manage_payment_profile/' . $law_firm_id);
		}
	}
	public function update_lawfirm_profile($id)
    {
        $data               = array();
		$id                 = $this->session->userdata('id');
        $data               = $this->input->post();
        $data['updated_at'] = date('m-d-Y h:i:s');
        $image_hidden       = $this->input->post('image');
        $img                = $_FILES['image']['name'];
		if ($img != '') {
			$type     = $_FILES['image']['type'];
			$size     = $_FILES['image']['size'];
			$img_temp = $_FILES['image']['tmp_name'];
			$error    = $_FILES['image']['error'];
			if ($type == "image/png" || $type == "image/jpeg" || $type == "image/jpg") {
				move_uploaded_file($img_temp, "uploads/$img");
				$data['image'] = ($_FILES["image"]["name"]);
			} //$type == "image/png" || $type == "image/jpeg" || $type == "image/jpg"
		} //$img != ''
		else {
			$data['image'] = $image_hidden;
		}
		$result = $this->Leads_model->updateLawCase($data, $id);
		if ($result) {
			$this->session->set_flashdata('message', '<p class="message_success">Law firm Profile updated successfully.</p>');
			redirect($this->config->item('base_url') . 'admin/users/company_logo/' . $id);
		} //$result
    }

	public function unblockUser()
	{
		$update_can_login_column = false;
		// updating can_login for this user to be 1
		$id = $this->input->post('id');
		$role = $this->input->post('role');
		$this->session->unset_userdata('logged_attempt');
		$this->session->unset_userdata('loggin_Blocked');
		if ($role == 1 || $role == 2 || $role == 7)
		{
			$update_can_login_column = $this->Central_model->update_row('users', array('can_login' => '1'), array('id' => $id));
		}elseif ($role == 6){
			$update_can_login_column = $this->Central_model->update_row('law_firms', array('can_login' => '1'), array('id' => $id));
		}
		if ($update_can_login_column)
		{
			$response['status'] = true;
			$response['row_id'] = $id;
		}else{
			$response['status'] = false;
			$response['row_id'] = $id;
		}
		echo json_encode($response);
		exit();
	}
	
	public function text_notification_setting()
	{
		$data = array();
		$id = $this->session->userdata('id');
		$role = $this->session->userdata('role');
		if($this->input->method(TRUE) == 'POST' && $role == 2) {
			$this->load->library('form_validation');
			$data = $this->input->post();
			$rules = array(
				[
					'field' => 'phone',
					'label' => 'Phone',
					'rules' => 'trim|required',
				],
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
				$update = $this->Central_model->update("users",$data,'id',$id);
				$this->session->set_flashdata('message', '<p class="message_success"> Updated successfully.</p>');
				redirect($this->config->item('base_url') . 'admin/users/text_notification_setting');
			}
		}
		
		$data['user'] = $this->Central_model->first("users", "id", $id);
		$this->load->view('layouts/admin/header', $data);
		$this->load->view('layouts/admin/sidebar', $data);
		$this->load->view('admin/users/text_notification_setting', $data);
		$this->load->view('layouts/admin/footer', $data);
	}
	
	public function frm_view($id)
	{  
		$data = array();
		
		$data['result'] = $this->Users_model->get_firm($id);
	    
		$this->load->view('layouts/admin/header', $data);
		$this->load->view('layouts/admin/sidebar', $data);
		$this->load->view('admin/users/lawfirm_view', $data);
		$this->load->view('layouts/admin/footer', $data);
	}
	public function view_as($id, $user = null)
	{
		// setting admin id for backup
		$this->session->set_userdata(array('backup_admin_id' => $this->session->id));
		if($user != null && $user == 'lawfirm'){
			$role = 6;			
		}else if($user != null && $user == 'lawstaff'){
			$role = 7;			
		}else{
			$role = 2;			
		}
		$this->session->set_userdata(array('role' => $role, 'id' => $id, 'view_as' => 'admin'));			
		redirect($this->config->item('base_url') . 'admin/dashboard/manage');
	}
	
	public function return_to_admin()
	{
		if($this->session->role == 2){
			$redirect = 'users/lawyers';
		}else if($this->session->role == 7){
			$redirect = 'users/lawyerStaff';
		}else if($this->session->role == 6){
			$redirect = 'settings/lawFirms';
		}
		$admin_id = $this->session->backup_admin_id;
		$this->session->set_userdata(array('role' => 1, 'id' => $admin_id, 'view_as' => ''));
		redirect($this->config->item('base_url') . 'admin/'.$redirect);
	}
    
    public function send_email_to_staff()
    {
        $data['send_email'] = $this->input->post('send_email');
        if($this->session->role == 1 || $this->session->role == 6 || $this->session->role == 2){
            $id = $this->input->post('staff_id');
            $this->Users_model->update($data, $id);
            //echo $this->db->last_query(); exit();
        }
        /*
        else{
            $lawyer_id = $this->session->id;
            $this->Users_model->update($data, $lawyer_id);
            $this->Users_model->update_lawyers_lawstaff($data, $lawyer_id);
            $this->session->set_flashdata('message', '<p class="message_success">'.( $data['send_email'] == 1 ? 'Email Notifications For Lawyer Staff Turned On Successfully!' : 'Email Notifications For Lawyer Staff Turned Off Successfully!' ).'</p>');
        }
        */
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function send_email_to_lawyers()
	{
		 $data['statement_email'] = $this->input->post('statement_email');
        if($this->session->role == 1 || $this->session->role == 6 || $this->session->role == 2){
            $id = $this->input->post('id');
            $this->Users_model->update($data, $id);
            //echo $this->db->last_query(); exit();
        }
        /*
        else{
            $lawyer_id = $this->session->id;
            $this->Users_model->update($data, $lawyer_id);
            $this->Users_model->update_lawyers_lawstaff($data, $lawyer_id);
            $this->session->set_flashdata('message', '<p class="message_success">'.( $data['send_email'] == 1 ? 'Email Notifications For Lawyer Staff Turned On Successfully!' : 'Email Notifications For Lawyer Staff Turned Off Successfully!' ).'</p>');
        }
        */
        //redirect($_SERVER['HTTP_REFERER']);
	}
}

