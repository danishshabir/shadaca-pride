<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Mail extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        chechUserSession();
        $this->load->model('admin/Users_model');
        $this->load->model('admin/Mail_model');
        $this->load->library('email');
		$this->load->model('Central_model');
    }
    public function index()
    {
        $data['class_admin'] = 'active';
        $id                  = $this->session->userdata('id');
        $data['inbox_arr']   = $this->Mail_model->getAllData('reciever', $id, '1');
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('layouts/admin/mail_header', $data);
        $this->load->view('admin/mail/inbox', $data);
        $this->load->view('layouts/admin/mail_footer', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function compose_mail()
    {
        $data['class_admin'] = 'active';
        $id            = $this->session->userdata('id');
		if($this->session->userdata('role') == 1) {
			$data['users'] = $this->Central_model->select_all_array('users', array('role'=> 2));	
			$data['law_frims'] = $this->Central_model->select_all_array('law_firms', array());	
		} else if($this->session->userdata('role') == 6 || $this->session->userdata('role') == 2) {
			$data['users'] = $this->Central_model->select_all_array('users', array('role'=> 1));
		}
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('layouts/admin/mail_header', $data);
        $this->load->view('admin/mail/compose_mail', $data);
        $this->load->view('layouts/admin/mail_footer', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function sendMessage()
    {
        $to_arr  = $this->input->post('to');
        $message = $this->input->post('message');
		$sender_role = $this->session->userdata('role');
        foreach ($to_arr as $to) {
			$part = explode('|', $to);
			$to = $part[0];
			$reciever_role = $part[1];
            $data['sender_id']   = $this->session->userdata('id');
            $data['reciever_id'] = $to;
            $data['message']     = $message;
            $data['sender_role']     = $sender_role;
            $data['reciever_role']     = $reciever_role;
            $insert_ids[]        = $this->Mail_model->saveMessage($data);
        }
        if (!empty($insert_ids)) {
            $this->session->set_flashdata('message', '<p class="message_success">Message sent successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/mail');
        }
    }
    public function replyMessage()
    {
        $to                  = $this->input->post('reciever_id');
        $message             = $this->input->post('message');
        $data['sender_id']   = $this->session->userdata('id');
        $data['reciever_id'] = $to;
        $data['message']     = $message;
        $data['reciever_role']     = $this->input->post('reciever_role');
        $data['sender_role']     = $this->session->userdata('role');
        $insert_ids          = $this->Mail_model->saveMessage($data);
        if (!empty($insert_ids)) {
            $this->session->set_flashdata('message', '<p class="message_success">Message sent successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/mail/read');
        }
    }
    public function outbox()
    {
        $data['class_admin'] = 'active';
        $id                  = $this->session->userdata('id');
        $data['outbox_arr']  = $this->Mail_model->getAllData('sender', $id, '1');
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('layouts/admin/mail_header', $data);
        $this->load->view('admin/mail/outbox', $data);
        $this->load->view('layouts/admin/mail_footer', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function trash()
    {
        $data['class_admin'] = 'active';
        $id                  = $this->session->userdata('id');
        $data['trash_arr']   = $this->Mail_model->getAllData('reciever', $id, '2');
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('layouts/admin/mail_header', $data);
        $this->load->view('admin/mail/trash', $data);
        $this->load->view('layouts/admin/mail_footer', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function archived()
    {
        $data['class_admin']  = 'active';
        $id                   = $this->session->userdata('id');
        $data['archived_arr'] = $this->Mail_model->getAllData('reciever', $id, '3');
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('layouts/admin/mail_header', $data);
        $this->load->view('admin/mail/archived', $data);
        $this->load->view('layouts/admin/mail_footer', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function read()
    {
        $data['class_admin'] = 'active';
        if ($this->input->post('message_id') == '') {
            $id = $this->session->userdata('message_id');
        } else {
            $id = $this->input->post('message_id');
            $this->session->set_userdata(array(
                "message_id" => $id
            ));
        }
        $user_id        = $this->session->userdata('id');
        $date['status'] = 1;
        $this->Mail_model->updateStatus($date, $id);
        $data['read'] = $this->Mail_model->getMailById($id);
        $module_type  = $this->input->post('type');
        $this->session->set_userdata(array(
            "module_type" => $module_type
        ));
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('layouts/admin/mail_header', $data);
        if ($module_type == 'outbox') {
            $this->load->view('admin/mail/read', $data);
        } else {
            $this->load->view('admin/mail/read', $data);
        }
        $this->load->view('layouts/admin/mail_footer', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function changeAction()
    {
        $message_ids = $this->input->post('message_ids[]');
        foreach ($message_ids as $message_id) {
            $date['action'] = $this->input->post('action');
            $this->Mail_model->updateStatus($date, $message_id);
        }
    }
    public function sendEmail()
    {
        $data                         = array();
        $ans_query                    = array();
        $data                         = $this->input->post();
        $subject                      = $this->input->post('subject');
        $data['subject']              = $subject;
        $data['date_time']            = date('Y-m-d H:i:s');
        $data['Sent_to']              = implode(',', $this->input->post('Sent_to'));
        $data['number_of_recipients'] = count($this->input->post('Sent_to'));
        $sent_to                      = $this->input->post('Sent_to');
        $insert_email                 = $this->Mail_model->insert_mail_data($data);
        foreach ($sent_to as $res) {
            $fetch_record       = $this->Mail_model->fetch_record($res);
            $subject            = $this->input->post('subject');
            $data['subject']    = $subject;
            $data['detail']     = $this->input->post('detail');
            $message           = str_replace("{email_id}", $insert_email, $data['detail']);
            $message           = str_replace("{id}", $fetch_record->id, $message);
            $message       = str_replace("{name}", $fetch_record->name, $message);
            $message        = str_replace("{username}", $fetch_record->name, $message);
            $message  = str_replace("{organization name}", 'Pride Legal', $message);
            $message = str_replace("www.shadaca.com", base_url().'admin/login/?userid=' . $fetch_record->id . '&id=' . $insert_email, $message);
            $data['date_time']  = date('Y-m-d H:i:s');
            $email              = $fetch_record->email;
            $to                 = $email;
            $from               = 'support@pridelegal.com';
            $name          = $this->config->item('project_title');
            send_email($to,$from,$name,$subject,$message);
            $this->session->set_flashdata('message', '<p class="message_success">Email Sent Successfully.</p>');
        }
        if ($insert_email) {
            redirect($this->config->item('base_url') . 'admin/EmailTracking');
        }
    }
}
