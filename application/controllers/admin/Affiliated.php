<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Affiliated extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        chechUserSession();
        $this->load->model('admin/Affiliated_model');
        $this->load->model('admin/Leads_model');
        $this->load->model('admin/Activity_model');
        $this->load->helper('cookie');
    }
    public function index()
    {
        $data                     = array();
        $data['class_affiliated'] = 'active';
        $data['affiliated']       = $this->Affiliated_model->getAllAffiliated();
        $data['case_type']        = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/affiliated/list', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function add()
    {
        $data                     = array();
        $data['class_affiliated'] = 'active';
        $data['case_type']        = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/affiliated/add', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function save()
    {
        $data  = array();
        $data  = $this->input->post();
        $check = $this->Affiliated_model->checkSource($data['name']);
        if ($check) {
            $this->session->set_flashdata('email', $data['email']);
            $this->session->set_flashdata('phone', $data['phone']);
            $this->session->set_flashdata('address', $data['address']);
            $this->session->set_flashdata('gender', $data['gender']);
            $this->session->set_flashdata('message', '<p class="message_warning">Source name already exist.</p>');
            redirect($this->config->item('base_url') . 'admin/affiliated/add');
        } else {
            $insert_id = $this->Affiliated_model->saveAffiliated($data);
            if ($insert_id > 0) {
                $this->session->set_flashdata('message', '<p class="message_success">Source added successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/affiliated');
            }
        }
    }
    public function edit($id)
    {
        $data                     = array();
        $data['class_affiliated'] = 'active';
        $data['res']              = $this->Affiliated_model->getAffiliatedById($id);
        $data['case_type']        = $this->Leads_model->fetchCaseType();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/affiliated/edit', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function update($id)
    {
        $data   = array();
        $data   = $this->input->post();
        $result = $this->Affiliated_model->updateAffiliated($data, $id);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Source updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/affiliated/edit/' . $id);
        }
    }
    public function delete($id)
    {
        $result = $this->Affiliated_model->deleteAffiliated($id);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Source deleted successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/affiliated');
        }
    }
    public function lead($source)
    {
        $source = str_replace('%20', ' ', $source);
        setcookie('lead_source', $source, time() + (86400 * 30), "/");
        $data                = array();
        $data['class_leads'] = 'active';
        $data['case_type']   = $this->Leads_model->fetchCaseType();
        $data['source']      = $source;
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/affiliated/lead', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function saveLead()
    {
        $data               = array();
        $activity           = array();
        $data               = $this->input->post();
        $data['created_at'] = date('Y-m-d H:i:s');
        unset($data['submit']);
        $insert_id = $this->Leads_model->save($data);
        if ($insert_id > 0) {
            $activity['created_at'] = date('Y-m-d H:i:s');
            $activity['lead_id']    = $insert_id;
            $activity['event']      = 'Entry Created';
            $insert_id              = $this->Activity_model->save($activity);
            $this->session->set_flashdata('message', '<p class="message_success">Lead added successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/affiliated/lead/' . $data['source']);
        }
    }
}
