<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Forgotpassword extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Leads_model');
        $this->load->model('admin/Users_model');
        $this->load->model('admin/Forgot_password_model');
		$this->load->model('Central_model');
    }
    public function index($type, $unique_code)
    {
		$data                  = array();
		$unique_code = ($this->input->post('unique_code') ? $this->input->post('unique_code') : $unique_code);
        $data['class']                  = 'active';
        $data['user'] = $this->Central_model->first(($type == 'lawyer' ? 'users' : 'law_firms'), 'unique_code', $unique_code);
		if($this->input->method(TRUE) == 'POST') {
			$this->load->library('form_validation');
			$rules = array(
				[
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'callback_valid_password',
				],
				[
					'field' => 'confirm_password',
					'label' => 'Confirm Password',
					'rules' => 'matches[password]',
				],
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
				$data['password'] = sha1($this->input->post('password'));
				$result = $this->Central_model->update(($type == 'lawyer' ? 'users' : 'law_firms'), array('password' => $data['password']), 'id', $data['user']->id);
				if($result) {
					$template = $this->Leads_model->template(3);
					$arr                    = $template->content;
					$to                     = ($type == 'lawyer' ? $data['user']->email : $data['user']->contact_email);
					$from                   = $template->email_from;
					$subject                = $template->subject;
					$message = $template->content;
					$message = str_replace("{name}", ($type == 'lawyer' ? $data['user']->name.' '.$data['user']->lname : $data['user']->contact_name.' '.$data['user']->contact_last_name), $message);
					$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
					$message = str_replace("{rootpath}", base_url(), $message);
					$message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
					send_email($to,$from,$this->config->item('project_title'),$subject,$message);
					/*Send SMS*/
					$sms_message = $template->text_message;
					$sms_message = str_replace("{project_title}", $this->config->item('project_title'), $sms_message);
					sms(1, $data['user']->phone, $sms_message);
					$data['success'] = 1;
					$this->session->set_flashdata('email', ($type == 'lawyer' ? $data['user']->email : $data['user']->contact_email));
					$this->session->set_flashdata('password', $this->input->post('password'));
					$this->session->set_flashdata('message', '<p class="message_success">Password changed successfully.</p>');
					redirect($this->config->item('base_url').'admin/login');
				} else {
					$data['success'] = 0;
					$this->session->set_flashdata('message', '<p class="message_warning">This is your old password!</p>');
					//redirect($this->config->item('base_url') . 'admin/forgotpassword/index/'.$type.'/' . $unique_code);
					$this->load->view('layouts/admin/header');
					$this->load->view('admin/forgotpassword/forgotpassword', $data);
					$this->load->view('layouts/admin/footer');
				}
			}
		}else{
			$this->load->view('layouts/admin/header');
			$this->load->view('admin/forgotpassword/forgotpassword', $data);
			$this->load->view('layouts/admin/footer');
		}
        
    }
    public function forgotpassword()
    {
        $email               = $this->input->post('email');
        $data['unique_code'] = generateRandomString("25");
		$record = $this->Central_model->select_array('users', array('email' => $email));  // for role 1,2        		
        if ($record == false)
        {
            $record = $this->Central_model->select_array('law_firms', array('contact_email' => $email)); //for role 6
			$first_name = 	$record->contact_name;
			$last_name = 	$record->contact_last_name;
			$link = base_url()."admin/forgotpassword/index/law_firms/" . $data['unique_code'];
			if($record) $this->Central_model->update('law_firms', $data, 'contact_email', $email);
        } else {
			$first_name = 	$record->name;
			$last_name = 	$record->lname;
			$link = base_url()."admin/forgotpassword/index/lawyer/" . $data['unique_code'];
			$this->Central_model->update('users', $data, 'email', $email);
		}
		if($record) {
			$template = $this->Leads_model->template(2);
			$subject = $template->subject;
			$message = $template->content;
			$message = str_replace("{name}", $first_name.' '.$last_name, $message);
			$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
			$message = str_replace("{rootpath}", base_url(), $message);
			$message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
			$message = str_replace("{link href}", "<a href='".$link."'>".base_url()."forgot_Password</a>", $message);
			/*Send Email*/
			$from = $template->email_from;
			$name = $this->config->item('project_title');
			$to   = $email;
			$email_sent = send_email($to,$from,$name,$subject,$message);
			/*Send SMS*/
			$sms_message = $template->text_message;
			$sms_message = str_replace("{project_title}", $this->config->item('project_title'), $sms_message);
			sms(1, $record->phone, $sms_message);
			//$this->session->set_flashdata('message', '<p class="message_success">Reset password request send successfully.</p>');
		}
        echo 'Reset password request send successfully.';
        exit();
    }
	public function valid_password($password = '')
	{
		$password = trim($password);
		$regex_lowercase = '/[a-z]/';
		$regex_uppercase = '/[A-Z]/';
		$regex_number = '/[0-9]/';
		$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>ยง~]/';
		if (empty($password))
		{
			$this->form_validation->set_message('valid_password', 'The {field} field is required.');
			return FALSE;
		}
		if (preg_match_all($regex_lowercase, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least one lowercase letter.');
			return FALSE;
		}
		if (preg_match_all($regex_uppercase, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least one uppercase letter.');
			return FALSE;
		}
		if (preg_match_all($regex_number, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must have at least one number.');
			return FALSE;
		}
		/* if (preg_match_all($regex_special, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must have at least one special character.' . ' ' . htmlentities('!@#$%^&*()\-_=+{};:,<.>ยง~'));
			return FALSE;
		} */
		if (strlen($password) < 6)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least 6 characters in length.');
			return FALSE;
		}
		if (strlen($password) > 32)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field cannot exceed 32 characters in length.');
			return FALSE;
		}
		return TRUE;
	}
}
