<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Pages extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('latest_url', current_url());
        chechUserSession();
		$this->load->model('Central_model');
    }
    public function index()
    {
        $data                  = array();
        $data['class']                  = 'active';
        $data['pages'] = $this->Central_model->select_all_array('pages', array(), array(), array(), 'title', 'ASC');
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/pages/index', $data);
        $this->load->view('layouts/admin/footer', $data); 
    }
	public function edit($id) {
		$data                  = array();
        $data['class']                  = 'active';
		if($this->input->method(TRUE) == 'POST') {
			$result = $this->Central_model->update('pages', array('title' => $this->input->post('title'), 'content' => $this->input->post('content')), 'id', $id);
			if($result) {
				$data['message'] = 'Page updated successfully.';
			}
		}
		$data['page'] = $this->Central_model->first('pages', 'id', $id);
		$this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/pages/edit', $data);
        $this->load->view('layouts/admin/footer', $data); 
	}
	public function welcome()
	{
		$data = array();
		$data['note'] = true;
		$data['page'] = $this->Central_model->first('pages', 'role', $this->session->userdata('role'));        		if($this->session->userdata('role') == 2){		$data['account_notes'] =  $this->Central_model->first('users', 'id', $this->session->userdata('id'));		}else if($this->session->userdata('role') == 6){			$data['account_notes'] =  $this->Central_model->first('law_firms', 'id', $this->session->userdata('id'));		}		
		$this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/pages/page', $data);	
        $this->load->view('layouts/admin/footer', $data); 
	}
	public function cms($id)
	{
		if($this->session->userdata('role') == 2 || $this->session->userdata('role') == 6) {
			$data = array();
			$data['page'] = $this->Central_model->first('pages', 'id', $id);
			$this->load->view('layouts/admin/header', $data);
			$this->load->view('layouts/admin/sidebar', $data);
			$this->load->view('admin/pages/page', $data);
			$this->load->view('layouts/admin/footer', $data); 
		}
	}
}
?>