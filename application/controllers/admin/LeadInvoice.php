<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class LeadInvoice extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        chechUserSession();
        $this->load->model('admin/Invoice_model');
        $this->load->model('admin/Users_model');
		$this->load->model('Central_model');
        $this->load->library('mpdf');
    }
    public function index()
    {
        $id                            = 1;
        $data                          = array();
        $data['id']                    = $this->session->userdata('id');
        $data['class']                 = 'active';
        $data['payments']              = $this->Invoice_model->fetchLeadsPaymentBilling($data['id']);
        $data['InvoiceDetails']        = $this->Invoice_model->fetchInvoiceDueDate($id);
        $data['lawyer_detail']         = $this->Invoice_model->fetchLawyerDetails($data['id']);
        $lawyer_lawfirm_id             = $data['lawyer_detail']->law_firms;
        $data['lawyer_lawfirm_detail'] = $this->Invoice_model->fetchLawyerLawFirmDetails($lawyer_lawfirm_id);
        $this->load->view('admin/invoice/invoice', $data);
    }
    public function LawyerLeadInvoice($lawyer_id, $type = NULL, $from_date = NULL, $to_date = NULL)
    {
        $data = array();
        $data['lawyer_id'] = $lawyer_id;
		if($from_date && $to_date) {
			$data['charges'] = $type;
			$data['date_from'] = _dateFormat($from_date);
			$data['date_to'] = _dateFormat($to_date);
			$data['transactions_month'] = date('m-d-Y', strtotime($data['date_from'])) .' to '.date('m-d-Y', strtotime($data['date_to']));
		} else {
		      //, strtotime('last month')
			$data['transactions_month'] = date('m-01-Y') .' to '.date('m-d-Y');
		}
        $data['class'] = 'active';
        $data['InvoiceDetails'] = $this->Central_model->first("settings","id",1);
        $data['transactions'] = $this->Invoice_model->transactions($data);
        //echo $this->db->last_query(); die;
        $data['lawyer'] = $this->Central_model->first("users","id",$data['lawyer_id']);
        $law_firm_fee = number_format($data['lawyer']->law_firm_fee, 2);
        $data['law_firm'] = $this->Central_model->first("law_firms","id",$data['lawyer']->law_firms);
        $data['jurisdiction_state'] = $this->Central_model->first("jurisdiction_states","id",$data['law_firm']->state);
        $html = $this->load->view('admin/users/transactions/invoice', $data, true);
        if(isset($_GET['no_print'])) {
            exit($html);
        }
        
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output('invoice-' . time() . '.pdf', 'I');
        echo $html;
    }
}
