<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
	
class Transaction extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Leads_model');
        $this->load->model('admin/Users_model');
        $this->load->model('admin/Reporting_model');
        $this->load->model('Central_model');
    }
    function chargeCreditCard()
    {
        $data                   = array();
        $id                     = $_POST['lawyer_id'];
        $amount                 = $_POST['amount'];
        $card_no                = $_POST['card_no'];
        $exp_date               = date('m/d', strtotime($_POST['exp_date']));
        $card_code              = $_POST['credit_card_code'];
        $role                   = $this->session->userdata('role');
        $newCard                = $_POST['newCard'];
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
        $refId      = 'ref' . time();
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($card_no);
        $creditCard->setExpirationDate($exp_date);
        $creditCard->setCardCode($card_code);
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);
        $order = new AnetAPI\OrderType();
        $order->setDescription("New Item");
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setOrder($order);
        $transactionRequestType->setPayment($paymentOne);
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response   = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        if ($response != null) {
            if ($response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK) {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getMessages() != null) {
                    if ($tresponse->getResponseCode() == '1') {
                        $data['lawyer_id']        = $id;
                        $data['amount']           = $_POST['amount'];
                        $data['status']           = $_POST['status'];
                        $data['transaction_date'] = date('Y-m-d H:i:s');
                        $data['credit_debit']     = 'D';
                        $data['card_type']        = $_POST['card_type'];
                        if ($role == '1') {
                            $data['transaction_by'] = '1';
                        } //$role == '1'
                        else {
                            $data['transaction_by'] = '2';
                        }
                        $lawyerDetails    = $this->Reporting_model->fetchLawyerdetail($id);
                        $LawyerRefrenceId = $this->Reporting_model->checkLawyerRefrenceId($id);
                        if (empty($LawyerRefrenceId) || empty($LawyerRefrenceId->refrence_id) || $newCard == '1') {
                            $customer_profile_id     = $this->createCustomerProfile($lawyerDetails->email);
                            $dataNew['refrence_id']  = $refId;
                            $dataNew['cust_prof_id'] = $customer_profile_id->getCustomerProfileId();
                            $paymentProfiles         = $customer_profile_id->getCustomerPaymentProfileIdList();
                            $paymentProfiles[0];
                            $dataNew['paym_prof_id'] = $paymentProfiles[0];
                            $LawyerRefrenceUpdate    = $this->Reporting_model->updateLawyerRefrenceId($id, $dataNew);
                        } //empty($LawyerRefrenceId) || empty($LawyerRefrenceId->refrence_id) || $newCard == '1'
                        $Transaction = $this->Reporting_model->insertTransactionRecord($data);
                        if ($Transaction) {
                            $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #3e923a; font-size: 18px;'>" . $tresponse->getMessages()[0]->getDescription() . "</p>");
                            echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/makePayment/' . $id . "'  </script>";
                        } //$Transaction
                        else {
                            $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>" . $tresponse->getMessages()[0]->getDescription() . "</p>");
                            echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/makePayment/' . $id . "'  </script>";
                        }
                    } //$tresponse->getResponseCode() == '1'
                } //$tresponse != null && $tresponse->getMessages() != null
                else {
                    if ($tresponse->getErrors() != null) {
                        $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>Transaction Failed " . $tresponse->getMessages()[0]->getDescription() . "</p>");
                        echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/makePayment/' . $id . "'  </script>";
                    } //$tresponse->getErrors() != null
                }
            } //$response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK
            else {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>" . $tresponse->getErrors()[0]->getErrorText() . "</p>");
                    echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/makePayment/' . $id . "'  </script>";
                } //$tresponse != null && $tresponse->getErrors() != null
                else {
                    $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>" . $tresponse->getMessages()[0]->getMessage() . "</p>");
                    echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/makePayment/' . $id . "'  </script>";
                }
            }
        } //$response != null
        else {
            $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'> No response returned </p>");
            echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/makePayment/' . $id . "'  </script>";
        }
        if (!defined('DONT_RUN_SAMPLES'))
            chargeCreditCard(\SampleCode\Constants::SAMPLE_AMOUNT);
    }
    function chargeExistingCreditCard()
    {
        $data                     = array();
        $customerProfileId        = $_POST['customerProfileId'];
        $customerPaymentProfileId = $_POST['customerPaymentProfileId'];
        $profile_response         = $this->getCustomerPaymentProfile($customerProfileId, $customerPaymentProfileId);
        $id                       = $_POST['lawyer_id'];
        $card_code                = $_POST['card_code'];
        $amount                   = $_POST['amount'];
        $role                     = $this->session->userdata('role');
        $card_no                  = $profile_response->getPaymentProfile()->getPayment()->getCreditCard()->getCardNumber();
        $exp_date                 = $profile_response->getPaymentProfile()->getPayment()->getCreditCard()->getExpirationDate();
        $merchantAuthentication   = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
        $refId      = 'ref' . time();
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($card_no);
        $creditCard->setExpirationDate($exp_date);
        $creditCard->setCardCode($card_code);
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);
        $order = new AnetAPI\OrderType();
        $order->setDescription("New Item");
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setOrder($order);
        $transactionRequestType->setPayment($paymentOne);
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response   = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        if ($response != null) {
            if ($response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK) {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getMessages() != null) {
                    if ($tresponse->getResponseCode() == '1') {
                        $data['lawyer_id'] = $id;
                        $data['amount']    = $_POST['amount'];
                        $data['status']    = $_POST['status'];
                        if ($role == '1') {
                            $data['transaction_by'] = '1';
                        } //$role == '1'
                        else {
                            $data['transaction_by'] = '2';
                        }
                        $Transaction = $this->Reporting_model->insertTransactionRecord($data);
                        if ($Transaction) {
                            $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #3e923a; font-size: 18px;'>" . $tresponse->getMessages()[0]->getDescription() . "</p>");
                            echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBilling/' . $id . "'  </script>";
                        } //$Transaction
                        else {
                            $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>" . $tresponse->getMessages()[0]->getDescription() . "</p>");
                            echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBilling/' . $id . "'  </script>";
                        }
                    } //$tresponse->getResponseCode() == '1'
                } //$tresponse != null && $tresponse->getMessages() != null
                else {
                    if ($tresponse->getErrors() != null) {
                        $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>Transaction Failed " . $tresponse->getMessages()[0]->getDescription() . "</p>");
                        echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBilling/' . $id . "'  </script>";
                    } //$tresponse->getErrors() != null
                }
            } //$response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK
            else {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>" . $tresponse->getErrors()[0]->getErrorText() . "</p>");
                    echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBilling/' . $id . "'  </script>";
                } //$tresponse != null && $tresponse->getErrors() != null
                else {
                    $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>" . $tresponse->getMessages()[0]->getMessage() . "</p>");
                    echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBilling/' . $id . "'  </script>";
                }
            }
        } //$response != null
        else {
            $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'> No response returned </p>");
            echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBilling/' . $id . "'  </script>";
        }
        if (!defined('DONT_RUN_SAMPLES'))
            chargeCreditCard(\SampleCode\Constants::SAMPLE_AMOUNT);
    }
    function oneTimeCharge()
    {
        $data        = array();
        $id          = $_POST['lawyer_id'];
        $amount      = $_POST['amount'];
        $card_no     = $_POST['card_no'];
        $exp_date    = date('m/d', strtotime($_POST['exp_date']));
        $card_code   = $_POST['credit_card_code'];
        $description = $_POST['description'];
        $quantity    = $_POST['quantity'];
        $role        = $this->session->userdata('role');
        ;
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
        $refId      = 'ref' . time();
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($card_no);
        $creditCard->setExpirationDate($exp_date);
        $creditCard->setCardCode($card_code);
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);
        $order = new AnetAPI\OrderType();
        $order->setDescription("New Item");
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setOrder($order);
        $transactionRequestType->setPayment($paymentOne);
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response   = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        if ($response != null) {
            if ($response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK) {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getMessages() != null) {
                    if ($tresponse->getResponseCode() == '1') {
                        $data['lawyer_id']        = $id;
                        $data['amount']           = $_POST['amount'];
                        $data['status']           = $_POST['status'];
                        $data['transaction_date'] = date('Y-m-d H:i:s');
                        $data['credit_debit']     = 'D';
                        $data['card_type']        = $_POST['card_type'];
                        $data['description']      = $_POST['description'];
                        $data['quantity']         = $_POST['quantity'];
                        if ($role == '1') {
                            $data['transaction_by'] = '1';
                        } //$role == '1'
                        else {
                            $data['transaction_by'] = '2';
                        }
                        $LawyerRefrenceId = $this->Reporting_model->checkLawyerRefrenceId($id);
                        if (empty($LawyerRefrenceId) || empty($LawyerRefrenceId->refrence_id)) {
                            $dataNew['refrence_id'] = $refId;
                            $LawyerRefrenceUpdate   = $this->Reporting_model->updateLawyerRefrenceId($id, $dataNew);
                        } //empty($LawyerRefrenceId) || empty($LawyerRefrenceId->refrence_id)
                        $Transaction = $this->Reporting_model->insertTransactionRecord($data);
                        if ($Transaction) {
                            $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #3e923a; font-size: 18px;'>" . $tresponse->getMessages()[0]->getDescription() . "</p>");
                            echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' . $id . "'  </script>";
                        } //$Transaction
                        else {
                            $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>" . $tresponse->getMessages()[0]->getDescription() . "</p>");
                            echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' . $id . "'  </script>";
                        }
                    } //$tresponse->getResponseCode() == '1'
                } //$tresponse != null && $tresponse->getMessages() != null
                else {
                    if ($tresponse->getErrors() != null) {
                        $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>Transaction Failed " . $tresponse->getMessages()[0]->getDescription() . "</p>");
                        echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' . $id . "'  </script>";
                    } //$tresponse->getErrors() != null
                }
            } //$response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK
            else {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>" . $tresponse->getErrors()[0]->getErrorText() . "</p>");
                    echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' . $id . "'  </script>";
                } //$tresponse != null && $tresponse->getErrors() != null
                else {
                    $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>" . $tresponse->getMessages()[0]->getMessage() . "</p>");
                    echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' . $id . "'  </script>";
                }
            }
        } //$response != null
        else {
            $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'> No response returned </p>");
            echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' . $id . "'  </script>";
        }
        if (!defined('DONT_RUN_SAMPLES'))
            chargeCreditCard(\SampleCode\Constants::SAMPLE_AMOUNT);
    }
    function getCustomerPaymentProfile($customerProfileId, $customerPaymentProfileId)
    {
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
        $refId   = 'ref' . time();
        $request = new AnetAPI\GetCustomerPaymentProfileRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setCustomerProfileId($customerProfileId);
        $request->setCustomerPaymentProfileId($customerPaymentProfileId);
        $controller = new AnetController\GetCustomerPaymentProfileController($request);
        $response   = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        if (($response != null)) {
            if ($response->getMessages()->getResultCode() == "Ok") {
                $response->getPaymentProfile()->getCustomerPaymentProfileId();
                $response->getPaymentProfile()->getbillTo()->getAddress();
                $response->getPaymentProfile()->getPayment()->getCreditCard()->getCardNumber();
                $response->getPaymentProfile()->getPayment()->getCreditCard()->getExpirationDate();
                if ($response->getPaymentProfile()->getSubscriptionIds() != null) {
                    if ($response->getPaymentProfile()->getSubscriptionIds() != null) {
                        echo "List of subscriptions:";
                        foreach ($response->getPaymentProfile()->getSubscriptionIds() as $subscriptionid)
                            echo $subscriptionid . "\n";
                    } //$response->getPaymentProfile()->getSubscriptionIds() != null
                } //$response->getPaymentProfile()->getSubscriptionIds() != null
            } //$response->getMessages()->getResultCode() == "Ok"
            else {
                echo "GetCustomerPaymentProfile ERROR :  Invalid response\n";
                $errorMessages = $response->getMessages()->getMessage();
                echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "\n";
            }
        } //($response != null)
        else {
            echo "NULL Response Error";
        }
        return $response;
        if (!defined('DONT_RUN_SAMPLES'))
            getCustomerPaymentProfile();
    }
    function chargeCustomerProfile()
    {
		$this->load->library(array('functions'));
        $data                     = array();
		$customer_profile = $this->Central_model->first("credit_cards","id",$this->input->post('customer_profile'));
		$customer_profile_data = $this->functions->getCustomerProfile($customer_profile->customer_profile_id,$customer_profile->profile_id);
        $data['lawyer_id']        = $this->input->post('lawyer_id');
        $data['amount']           = $this->input->post('amount');
        $data['status']           = '2';
        $data['transaction_by']   = $customer_profile->law_firm;
        $data['transaction_date'] = date('Y-m-d h-i-s');
        $data['card_type'] = $customer_profile_data['card_type'];
        $data['credit_debit'] = $customer_profile_data['card_number'];
		$response = $this->functions->chargeCustomerProfile($customer_profile,$data);
		if($response['success'] == 0)
		{
			$this->session->set_flashdata('message', '<p class="message_success"> '.$response['error'].'</p>');
		} else if($response['success'] == 1) {
			$this->session->set_flashdata('message', '<p class="message_warning"> '.$response['msg'].'</p>');
		}
		redirect($this->config->item('base_url') . 'admin/dashboard/makePayment/' . $this->input->post('lawfirm_id'). '/' .$this->input->post('lawyer_id'));
    }
    function chargeBatchProcessing()
    {
		$this->load->library(array('functions'));
        $data                   = array();
		$json_array                   = array();
        $data                   = $this->input->post();
		unset($data['lawfirm_id']);
        $amounts                = $data['amount'];
        $i                      = 0;
        $count                  = count($data['lawyer_id']);
		$customer_profile = $this->Central_model->first("credit_cards","law_firm",$this->input->post('lawfirm_id'), "default", 1);
		$customer_profile_data = $this->functions->getCustomerProfile($customer_profile->customer_profile_id,$customer_profile->profile_id);
        if (count($data['lawyer_id']) > 0) {
            foreach ($data['lawyer_id'] as $lawyer_id) {
                /* $user                     = $this->Leads_model->fetchLawyerRow($lawyer_id);
                $profileid                = $user->cust_prof_id;
                $paymentprofileid         = $user->paym_prof_id; */
                $amount                   = $amounts[$i];
                $id                       = $lawyer_id;
                $role                     = $this->session->userdata('role');
                $data['lawyer_id']        = $id;
                $data['amount']           = $amount;
                $data['status']           = '2';
                $data['transaction_by']   = $this->input->post('lawfirm_id');
                $data['transaction_date'] = date('Y-m-d h-i-s');
				$data['card_type'] = $customer_profile_data['card_type'];
				$data['credit_debit'] = $customer_profile_data['card_number'];
				$response = $this->functions->chargeCustomerProfile($customer_profile,$data);
				if($response['success'] == 0)
				{
					$json_array[$lawyer_id]['text'] = $response['error'];
					$json_array[$lawyer_id]['success'] = $response['success'];
					$this->session->set_flashdata('message', '<p style="font-family: inherit;text-align: center; margin-top: 20px; color: green; font-size: 16px;"> Transactions has been successfully approved.</p>');
				} else {
					$json_array[$lawyer_id]['text'] = $response['msg'];
					$json_array[$lawyer_id]['success'] = $response['success'];
				}
                $i++;
            } //$data['lawyer_id'] as $lawyer_id
        } //count($data['lawyer_id']) > 0
        else {
			$json_array['text'] = "Please select first any lawyer!";
			$json_array['success'] = 0;
        }
		$json_array['lawfirm_id'] = $this->input->post('lawfirm_id');
		echo json_encode($json_array);
		exit();
    }
    public function addBatchProcessing($lawfirm_id)
    {
        $data      = array();
        $user_role = $this->session->userdata('role');
        $data      = $this->input->post();
        unset($data['dynamic-table_length']);
        $i = 0;
        if (count($data['lawyers_ids']) > 0) {
            foreach ($data['lawyers_ids'] as $lawyer_data) {
                $lead_bill_new                  = 0;
                $monthly_bill_new               = 0;
                $lawyer_leads_price             = $this->Reporting_model->fetchLawyerAllLeadsPrice($lawyer_data);
                #echo $this->db->last_query() . "<br />";
                $Lawyer_all_monthly_bill        = $this->Reporting_model->fetchLawyerMonthlyBillTotal($lawyer_data);
                #echo $this->db->last_query() . "<br />";
                $total_charge_bill              = $this->Reporting_model->total_charge_bill($lawyer_data);
                #echo $this->db->last_query() . "<br />";
                $lawyerPaidTransactions_new     = $this->Reporting_model->fetchLawyerPaidTransactions($lawyer_data);
                #echo $this->db->last_query() . "<br />";
                $data['total']                  = ($lawyer_leads_price[0]->total + $Lawyer_all_monthly_bill[0]->total + $total_charge_bill[0]->total) - $lawyerPaidTransactions_new[0]->total;
                $total_arrays[]                 = $data['total'];
                $total_arrays_ids[]             = $lawyer_data;
            } //$data['lawyers_ids'] as $lawyer_data
        } //count($data['lawyers_ids']) > 0
        #die();
        $data['totalArrays']    = $total_arrays;
        $data['totalArraysIds'] = $total_arrays_ids;
        $data['class_admin']    = 'active';
        $data['lawfirm_id']    = $lawfirm_id;
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/batch_processing', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
}
