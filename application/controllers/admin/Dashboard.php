<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Dashboard extends CI_Controller
{
    private $limit;
    private $page;
    private $offset;
    private $total;
    private $num_links;
    
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('latest_url', current_url());
        chechUserSession();
        $this->load->model('admin/Leads_model');
        $this->load->model('admin/Users_model');
        $this->load->model('admin/Reporting_model');
        $this->load->model('admin/Setting_model');
        $this->load->model('admin/Invoice_model');
		$this->load->model('Central_model');
        
        $this->limit = isset($_GET["per_page"]) && $_GET["per_page"] != "" ? $_GET["per_page"] : 100;
        $this->page = isset($_GET["page"]) ? $_GET["page"] : 1;
        $this->offset = ($this->page - 1) * $this->limit;
        $this->total = $this->db->query("SELECT id FROM leads WHERE lead_status NOT IN('3', '11', '12', '13', '14')")->num_rows();
        $this->num_links = ceil($this->total / $this->limit);
    }
    
    public function getLawfirmLawyers()
	{
		$lawfirm_id = $this->input->post('id');
		$data = $this->Users_model->getLawfirmLawyers($lawfirm_id);
		$html = "";
		foreach($data as $lawyer){
			$html .= "<option id='fee' value=".$lawyer['id'].">".$lawyer['name']." ".$lawyer['lname']."</option>";
		}
		if(count($data) > 1){
			$html .= "<option value='0'>Select All</option>";
		}
		echo $html;
		/* $res = [
			"html" => $html,
		];
		echo json_encode($res);
		exit; */
	}
    
    public function manage($search = '', $val = '')
    {
        $data   = array();
        if ($this->session->userdata('role') == 1) {
            $data['case_type']   = $this->Leads_model->fetchCaseType();
            $data['all_markets'] = $this->Setting_model->marketRecords();
            $data["num_links"] = $this->num_links;
            $data['leads']       = $this->Leads_model->fetchLeadsLimit($this->limit, $this->offset);
            //echo $this->db->last_query(); exit();
            $data['lawyers']     = $this->Users_model->fetchAllUsers('2');
            $data['class_type']  = $val;
            $view                = 'admin/dashboard/manage_lead';
        } elseif ($this->session->userdata('role') == 2 || $this->session->userdata('role') == 6 || $this->session->userdata('role') == 7) {
            if ($search == 'All') {
                $status = 0;
            } elseif ($search == 'New') {
                $status = 2;
            } elseif ($search == 'Opened') {
                $status = 5;
            } elseif ($search == 'NotRetained') {
                $status = 6;
            } elseif ($search == 'Completed') {
                $status = 10;
            } elseif ($search == 'Contacted') {
                $status = 8;
            } elseif ($search == 'Declined') {
                $status = 15;
            }else{
                $status = '';
            }
            $data['case_type']  = $this->Leads_model->fetchCaseType();
            $data['class_type'] = $search;
            $view               = 'admin/dashboard/manage';
            if ($this->session->userdata('role') == 6)
            {
                $data['leads'] = $this->Leads_model->LawfirmsLeads($this->session->userdata('id'), '5', $status);		
            } else if($this->session->userdata('role') == 7) {
				$lawyerstaff_id = $this->session->id;
				$data['leads'] = $this->Leads_model->fetchLeadsOfLawyersLimit($lawyerstaff_id, '5', $status, $type = 'multi');
				
			} else {
                $data['leads'] = $this->Leads_model->fetchLeadsOfLawyersLimit($this->session->userdata('id'), '5', $status);
            }
        }
        $this->session->set_flashdata('class_type', $data['class_type']);
        $data['class'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view($view, $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function detailedView($search = '', $val = '')
    {
        $data   = array();
        if ($this->session->userdata('role') == 1) {
            $data['case_type']  = $this->Leads_model->fetchCaseType();
            $data['leads']      = $this->Leads_model->fetchLeadsLimit();
            $data['lawyers']    = $this->Users_model->fetchAllUsers('2');
            $data['class_type'] = $val;
            $view               = 'admin/dashboard/detailedView';
        }
        $this->session->set_flashdata('class_type', $data['class_type']);
        $data['class'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view($view, $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function Vacation_mode()
    {
        if ($this->session->userdata('role') == 2) {
			$data['lawyer'] = $this->Central_model->first("users","id",$this->session->userdata('id'));
			$data['class'] = 'active';
			$this->load->view('layouts/admin/header', $data);
			$this->load->view('layouts/admin/sidebar', $data);
			$this->load->view('admin/users/add_holiday');
			$this->load->view('layouts/admin/footer', $data);
        }
    }
    public function check_user_password()
    {
        $id = $this->uri->segment(4);
        $this->load->model('admin/Activity_model');
    }
    public function getLawFirm()
    {
		$data = array();
        $cat_id = $this->input->post('id');
        $this->load->model('admin/Users_model');
        $products = $this->Users_model->fetchLawfee($cat_id);
        $data['fee'] = $products->law_firm_fee;
        $data['fee_type'] = $products->fee_type;
		echo json_encode($data);
        exit;
    }
    /**
     * Dashboard::fetch_price()
     * 
     * @return void
     */
    public function fetch_price() {
        $market = $this->input->post('market');
        $case_type = $this->input->post('case_type');
        $row = $this->db->where(array('market' => $market, 'case_type' => $case_type))->get('market_fee')->row();
        if($row)
            echo $row->fee;
        else
            echo 0.00;
    }
    public function getJurisdiction()
    {
        $cat_id = $this->input->post('id');
        $state = $this->input->post('s');
        $case_type = $this->input->post('ct');
        $this->load->model('admin/Users_model');
        $products = $this->Users_model->fetchJurisdictionPrice($cat_id, $state, $case_type);
        if($products)
            echo $fee = $products->fee;
        else
            echo 0.00;
        exit;
    }
    public function getMarketCaseTypePrice()
    {
        $market_id = $this->input->post('m_id');
        $cat_id    = $this->input->post('id');
        $this->load->model('admin/Users_model');
        $products = $this->Users_model->fetchMarketCaseTypePrice($cat_id, $market_id);
        echo $fee = $products->fee;
        exit;
    }
    public function getStateId()
    {
        $cat_id = $this->input->post('id');
        $this->load->model('admin/Users_model');
        $products = $this->Users_model->fetchStateId($cat_id);
        echo $fee = $products->state;
        exit;
    }
    public function LawyerBilling($lawfirm_id)
    {
		$lawfirm_id = ($this->session->userdata('role') == 6 ? $this->session->userdata('id') : $lawfirm_id);
        $data = array();
        $data['law_firms']   = $this->Users_model->fetchAllLawFirms();
		$data['law_firm'] = $this->Central_model->first("law_firms","id",$lawfirm_id);
        $data['lawyers'] = $this->Users_model->LawyerBilling($lawfirm_id, 2, 1);
        #echo $this->db->last_query(); die;
        $data['class']       = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/admin_billing');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function lawyerAllBills()
    {
        $data      = array();
        $user_role = $this->session->userdata('role');
        if ($user_role == '1' || $user_role == '6') {
            $lawyer_id = $this->uri->segment(4);
        } else {
            $lawyer_id = $this->session->userdata('id');
        }
        $data['lawyer'] = $this->Central_model->first("users","id",$lawyer_id);
        $lawyer_leads_price             = $this->Reporting_model->fetchLawyerAllLeadsPrice($lawyer_id);
        //echo $this->db->last_query(); exit();
		$Lawyer_all_monthly_bill        = $this->Reporting_model->fetchLawyerMonthlyBillTotal($lawyer_id);
		$total_charge_bill                   = $this->Reporting_model->total_charge_bill($lawyer_id);
		$lawyerPaidTransactions_new     = $this->Reporting_model->fetchLawyerPaidTransactions($lawyer_id);
		$data['total']                  = ($lawyer_leads_price[0]->total + $Lawyer_all_monthly_bill[0]->total + $total_charge_bill[0]->total) - $lawyerPaidTransactions_new[0]->total;
        $total_arrays[]                 = $data['total'];
        $data['totalArrays']            = $total_arrays;
        $data['class']                  = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/lawyer_all_billing');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function lawyerMonthlyBill()
    {
        $data         = array();
        $html         = '';
        $id           = $this->input->post('hidden_id');
        $data['role'] = $this->session->userdata('role');
        if ($data['role'] == '1') {
            $data['id'] = $id;
            $link       = '<a href="' . base_url() . 'admin/leadInvoice/LawyerLeadInvoice/' . $id . '" target="_blank"><button class="btn btn-success">Print </button></a>';
        } else {
            $data['id'] = $this->session->userdata('id');
            $link       = '<a href="' . base_url() . 'admin/leadInvoice" target="_blank"><button class="btn btn-success">Print </button></a>';
        }
        $month                     = $this->input->post('month_bill');
        $new_date                  = date("m-Y", strtotime($month));
        $month_no                  = date("n", strtotime($month));
        $year                      = " - " . date("Y", strtotime($month));
        $month_names               = array(
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
        $data['monthly_billings']  = $this->Users_model->fetchMonthlyTotalAjax($data['id'], $new_date);
        $data['lead_bilings']      = $this->Users_model->fetchLeadsPaymentTotal($data['id']);
        $data['monthly_paid']      = $this->Users_model->fetchMonthlyPaidTotal($data['id']);
        $data['lead_bilings_paid'] = $this->Users_model->fetchLeadsPaymentPaidTotal($data['id']);
        $data['total_bill_paid']   = $data['monthly_paid']->total_m_bill + $data['lead_bilings_paid']->total_lead_bill;
        $data['remaining_bill']    = ($data['monthly_billings']->total_m_bill + $data['lead_bilings']->total_lead_bill) - $data['total_bill_paid'];
        $html .= '
	          
							<div class="btn-group pull-right">
                                  ' . $link . ';	  
                              </div>
			  
			  
              <table  class="display table table-bordered table-striped">
			  <thead>
			  	
			  <tr>     
			  <th style="padding:15px 5px;">Month</th>   
			  <th style="padding:15px 5px;">' . $month_names[$month_no - 1] . $year . '
			  </th>        
			  </tr> 
			  <tr>	

			  <tr>     
			  <th style="padding:15px 5px;">Monthly Invoice</th>   
			  <th style="padding:15px 5px;"> $' . $data['monthly_billings']->total_m_bill . '
			  </th>        
			  </tr> 
			  <tr>     
			  <th style="padding:15px 5px;">Leads Bill</th>   
			  <th style="padding:15px 5px;"> $' . $data['lead_bilings']->total_lead_bill . '
			  
			  
			  </th> 
			  </tr> 
			  <tr>     
			  <th style="padding:15px 5px;">Paid Bill</th>   
			  <th style="padding:15px 5px;"> $' . $data['lead_bilings_paid']->total_lead_bill . ' 
			   
			 
			  </th>
			  </tr> 
			  <tr>     
			  <th style="padding:15px 5px;">Remaining Balance</th>   
			  <th style="padding:15px 5px;"> $' . $data['remaining_bill'] . '
			  
			  
			  <tr>     
			  <th style="padding:15px 5px;">Status</th>   
			  <th style="padding:15px 5px;">Not paid</th> 
			  </tr> 
			  
			  </th> 
			  </tr>
			  </thead>       
			  <tbody>          
			  <tr>        
			  <td></td>  
			  </tr>     
			  </tbody> 
			  </table>  
	 ';
        echo $html;
    }
    public function ChecklawyerBilling()
    {
        $data         = array();
        $html         = '';
        $lawyer_name  = $this->input->post('lawyer_name');
        $billing_type = $this->input->post('billing_type');
        $date_from    = $this->input->post('date_from');
        $date_to      = $this->input->post('date_to');
        if ($billing_type == '1') {
            $data['payments']  = $this->Users_model->fetchLeadPaymentOfLawyer($lawyer_name, $date_from, $date_to);
            $resArr            = array();
            $resArr['error']   = "";
            $resArr['success'] = "";
            $resArr['html']    = '';
            if (sizeof($data['payments']) > 0) {
                $resArr['success'] = "Success";
            } else {
                $resArr['error'] = "Failed";
            }
            $html .= '
 <div class="table-responsive">    
<div class="clearfix"></div>
<div class="margin-top-10"></div>  
<table class="display table table-bordered table-striped">
 <thead> 
<tr>
<th>Fee</th>
<th>Name</th>
<th>Email</th>
</tr>
</thead>';
            foreach ($data['payments'] as $payment) {
                $html .= '<tr>      
<td> $' . $payment->fee . '</td> 
<td>' . $payment->first_name . ' ' . $payment->last_name . '</td>  
<td>' . $payment->email . '</td>  
</tr>';
                $total_Billing += $payment->fee;
            }
            $html .= '<td style="width:150px; font-weight:700; background-color:#ffdddd;"> Total = $' . $total_Billing . '</td>';
            $resArr['html'] = $html;
            echo $html;
            exit();
        } elseif ($billing_type == '2') {
            $new_date_from             = date("m-Y", strtotime($date_from));
            $new_date_to               = date("m-Y", strtotime($date_to));
            $data['monthly_billings']  = $this->Users_model->fetchLawyerMonthlyTotal($lawyer_name, $new_date_from, $new_date_to);
            $data['lead_bilings']      = $this->Users_model->fetchLeadsPaymentTotal($lawyer_name);
            $data['monthly_paid']      = $this->Users_model->fetchMonthlyPaidTotal($lawyer_name);
            $data['lead_bilings_paid'] = $this->Users_model->fetchLeadsPaymentPaidTotal($lawyer_name);
            $data['total_bill_paid']   = $data['monthly_paid']->total_m_bill + $data['lead_bilings_paid']->total_lead_bill;
            $data['remaining_bill']    = ($data['monthly_billings']->total_m_bill + $data['lead_bilings']->total_lead_bill) - $data['total_bill_paid'];
            $html .= '

<div class="table-responsive">      
<div class="clearfix">                                                        </div>  
<div class="margin-top-10"></div>  
<table  class="display table table-bordered table-striped">
<thead>    
<tr>';
            $html .= '<th style="padding:15px 5px;">Monthly Invoice</th>';
            $html .= '<th style="padding:15px 5px;"> $ ' . $data['monthly_billings']->total_m_bill . '</th>        
</tr> 
<tr>     
<th style="padding:15px 5px;">Leads Bill</th>   
<th style="padding:15px 5px;"> $ ' . $data['lead_bilings']->total_lead_bill . '</th> 
</tr> 
<tr>     
<th style="padding:15px 5px;">Paid Bill</th>   
<th style="padding:15px 5px;"> $ ' . $data['lead_bilings_paid']->total_lead_bill . '</th> 
</tr> 
<tr>     
<th style="padding:15px 5px;">Remaining Balance</th>   
<th style="padding:15px 5px;"> $' . $data['remaining_bill'] . '</th> 
</tr> 
</thead>       
<tbody>          
<tr>        
<td></td>  
</tr>     
</tbody>
</table>  
<br/>
</div>';
            $resArr['html'] = $html;
            echo $html;
            exit();
        }
    }
    public function Billing()
    {
        $data             = array();
        $data['id']       = $this->session->userdata('id');
        $data['payments'] = $this->Users_model->fetchPayment($data['id']);
        $data['class']    = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/billing');
        $this->load->view('layouts/admin/footer', $data);
    }
    /**
     * Dashboard::LawyerBills()
     * 
     * @return void
     */
    public function LawyerBills()
    {
        $data      = array();
        $user_role = $this->session->userdata('role');
        if ($user_role == '1' || $user_role == '6' || $user_role == 7) {
            $data['id'] = $this->uri->segment(4);
        } else {
            $data['id'] = $this->session->userdata('id');
        }
        $data['lawyer'] = $this->Central_model->first("users","id",$data['id']);
        $data['lead_bill_new']             = $this->Reporting_model->fetchLawyerAllLeadsPrice($data['id'])[0]->total;
		$data['monthly_bill_new']        = $this->Reporting_model->fetchLawyerMonthlyBillTotal($data['id'])[0]->total;
		$data['total_charge_bill']                   = $this->Reporting_model->total_charge_bill($data['id'])[0]->total;
		$lawyerPaidTransactions_new     = $this->Reporting_model->fetchLawyerPaidTransactions($data['id']);
		$data['total']                  = ($data['lead_bill_new'] + $data['monthly_bill_new'] + $data['total_charge_bill']) - $lawyerPaidTransactions_new[0]->total;
        $data['total_payments']         = $lawyerPaidTransactions_new[0]->total;
        //$data['transactions']     = $this->Reporting_model->search_transactions(array('lawyer_id' => $data['id'], 'charges' => 3));
        $data['transactions'] = $this->Invoice_model->transactions(array('lawyer_id' => $data['id'], 'charges' => 3));
        $data['class']                  = 'active';
        
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/lawyer_payments', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function LawyerAllLeads()
    {
        $data      = array();
        $user_role = $this->session->userdata('role');
        if ($user_role == '1' || $user_role == '6') {
            $data['id'] = $this->uri->segment(4);
        } else {
            $data['id'] = $this->session->userdata('id');
        }
		if($this->input->method(TRUE) == 'POST') {
			$data['leads_details'] = $this->Users_model->fetchLawyerAllLeads($data['id'], array("date_from" => _dateFormat($this->input->post('date_from')), "date_to" => _dateFormat($this->input->post('date_to'))));
			$data['html'] = $this->load->view('admin/users/billings/leads_bill', $data, true);
			echo json_encode($data); 
			exit;
		} else {
			$data['lawyer'] = $this->Central_model->first("users","id",$data['id']);
			$data['leads_details']          = $this->Users_model->fetchLawyerAllLeads($data['id']);
			$lawyer_leads_price             = $this->Reporting_model->fetchLawyerAllLeadsPrice($data['id']);
			$Lawyer_all_monthly_bill        = $this->Reporting_model->fetchLawyerMonthlyBillTotal($data['id']);
			$total_charge_bill                   = $this->Reporting_model->total_charge_bill($data['id']);
			$lawyerPaidTransactions_new     = $this->Reporting_model->fetchLawyerPaidTransactions($data['id']);
			$data['total']                  = ($lawyer_leads_price[0]->total + $Lawyer_all_monthly_bill[0]->total + $total_charge_bill[0]->total) - $lawyerPaidTransactions_new[0]->total;
			$data['class']                  = 'active';
			$this->load->view('layouts/admin/header', $data);
			$this->load->view('layouts/admin/sidebar', $data);
			$this->load->view('admin/users/lawyer_all_leads', $data);
			$this->load->view('layouts/admin/footer', $data);
		}
    }
    public function reportings()
    {
        $data                  = array();
        $data['id']            = $this->uri->segment(4);
        $data['leads_details'] = $this->Users_model->fetchLawyerAllLeads($data['id']);
        $data['class']         = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/reporting/reportings', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function creditCard()
    {
        $data          = array();
        $data['id']    = $this->uri->segment(4);
        $data['class'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/add_credit_card', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function saveCreditCard()
    {
        $data               = array();
        $data['card_type']  = $this->input->post('card_type');
        $data['card_no']    = $this->input->post('card_no');
        $data['exp_date']   = $this->input->post('exp_date');
        $data['ccv']        = $this->input->post('ccv');
        $data['created_at'] = date('Y:m:d H:i:s');
        $data['lawyer_id']  = $this->session->userdata('id');
        $insert_id          = $this->Users_model->saveLawyercreditcardDetails($data);
        if ($insert_id) {
            $this->session->set_flashdata("message", "<p class='message_success'>Added successfully</p>");
            redirect($this->config->item('base_url') . 'admin/dashboard/creditCard');
        } else {
            $this->session->set_flashdata("message", "<p class='message_warning'>Failed</p>");
            redirect($this->config->item('base_url') . 'admin/dashboard/creditCard');
        }
        $data['class'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/add_credit_card', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function MonthlyBilling()
    {
        $user_role      = $this->session->userdata('role');
        $data           = array();
        $data['id']     = $this->session->userdata('id');
        $current_date   = date("m-Y");
        $data['months'] = $this->Users_model->fetchDates($data['id']);
        foreach ($data['months'] as $month) {
            $month_date             = $month->created_at;
            $month_no               = date("n", strtotime($month_date));
            $year                   = " - " . date("Y", strtotime($month_date));
            $month_names            = array(
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            );
            $data['calculate_date'] = $month_names[$month_no - 1] . $year;
            $data['calculate_date'];
            $month_year                = $month->date_time;
            $data['monthly_billings']  = $this->Users_model->fetchMonthlyTotal($data['id'], $month_year);
            $data['lead_bilings']      = $this->Users_model->fetchLeadsPaymentTotal($data['id']);
            $data['monthly_paid']      = $this->Users_model->fetchMonthlyPaidTotal($data['id']);
            $data['lead_bilings_paid'] = $this->Users_model->fetchLeadsPaymentPaidTotal($data['id']);
            $data['total_bill_paid']   = $data['monthly_paid']->total_m_bill + $data['lead_bilings_paid']->total_lead_bill;
            $data['remaining_bill']    = ($data['monthly_billings']->total_m_bill + $data['lead_bilings']->total_lead_bill) - $data['total_bill_paid'];
            if ($data['monthly_billings']->payment_status == '0') {
                $status = 'Not paid';
            } else {
                $status = 'paid';
            }
            if ($user_role == '2') {
                $link = '<a href="' . base_url() . 'admin/leadInvoice" target="_blank"><button class="btn btn-success">Print </button></a>';
            } else {
                $link = '<a href="' . base_url() . 'admin/leadInvoice/LawyerLeadInvoice/' . $id . '" target="_blank"><button class="btn btn-success">Print </button></a>';
            }
            $data['html'] .= '

							<div class="btn-group pull-right">
                                  ' . $link . '	  
                              </div>


<table  class="display table table-bordered table-striped">
			  <thead>    
			  
			  <tr>     
			  <th style="padding:15px 5px; width:400px;">Month</th>   
			  <th style="padding:15px 5px; width:400px;">' . $data['calculate_date'] . '
			  </th>        
			  </tr>
			  
			  <tr>     
			  <th style="padding:15px 5px; width:400px;">Monthly Invoice</th>   
			  <th style="padding:15px 5px; width:400px;"> $' . $data['monthly_billings']->total_m_bill . '</th>        
			   
			  </tr> 
			  
			  <tr>     
			  <th style="padding:15px 5px; width:400px;">Leads Bill</th>   
			  <th style="padding:15px 5px; width:400px;"> $' . $data['lead_bilings']->total_lead_bill . '</th> 
			  </tr> 


				<tr>     
			  <th style="padding:15px 5px; width:400px;">Paid Bill</th>   
			  <th style="padding:15px 5px; width:400px;"> $' . $data['lead_bilings_paid']->total_lead_bill . '</th> 
			  </tr> 

			  
			  <tr>     
			  <th style="padding:15px 5px; width:400px;">Remaining Balance</th>   
			  <th style="padding:15px 5px; width:400px;">$' . $data['remaining_bill'] . '</th> 
			  </tr> 
			  
			  
			  
			  <tr>     
			  <th style="padding:15px 5px; width:400px;">Status</th>   
			  <th style="padding:15px 5px; width:400px;">' . $status . '</th> 
			  </tr> 

			  
			  </thead>       
			 
			   </table>
			   <div class="height" style="height:50px;"></div>
			   <div class="clearfix"></div>
';
        }
        $data['class'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/monthly_billing', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function LawyerMonthlyBilling()
    {
        $user_role      = $this->session->userdata('role');
        $data           = array();
        $data['id']     = $this->uri->segment(4);
        $current_date   = date("m-Y");
        $data['months'] = $this->Users_model->fetchDates($data['id']);
        foreach ($data['months'] as $month) {
            $month_date             = $month->created_at;
            $month_no               = date("n", strtotime($month_date));
            $year                   = " - " . date("Y", strtotime($month_date));
            $month_names            = array(
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            );
            $data['calculate_date'] = $month_names[$month_no - 1] . $year;
            $data['calculate_date'];
            $month_year                = $month->date_time;
            $data['monthly_billings']  = $this->Users_model->fetchMonthlyTotal($data['id'], $month_year);
            $data['lead_bilings']      = $this->Users_model->fetchLeadsPaymentTotal($data['id']);
            $data['monthly_paid']      = $this->Users_model->fetchMonthlyPaidTotal($data['id']);
            $data['lead_bilings_paid'] = $this->Users_model->fetchLeadsPaymentPaidTotal($data['id']);
            $data['total_bill_paid']   = $data['monthly_paid']->total_m_bill + $data['lead_bilings_paid']->total_lead_bill;
            $data['remaining_bill']    = ($data['monthly_billings']->total_m_bill + $data['lead_bilings']->total_lead_bill) - $data['total_bill_paid'];
            if ($user_role == '2') {
                $link = '<a href="' . base_url() . 'admin/leadInvoice" target="_blank"><button class="btn btn-success">Print </button></a>';
            } else {
                $link = '<a href="' . base_url() . 'admin/leadInvoice/LawyerLeadInvoice/' . $data['id'] . '" target="_blank"><button class="btn btn-success">Print </button></a>';
            }
            $data['html'] .= '


						<div class="btn-group pull-right">
                          ' . $link . '       	  
                         </div>

<table  class="display table table-bordered table-striped">
			  <thead>    
			  
			  <tr>     
			  <th style="padding:15px 5px; width:400px;">Month</th>   
			  <th style="padding:15px 5px; width:400px;">' . $data['calculate_date'] . '
			  </th>        
			  </tr>
			  
			  <tr>     
			  <th style="padding:15px 5px; width:400px;">Monthly Invoice</th>   
			  <th style="padding:15px 5px; width:400px;">$' . $data['monthly_billings']->total_m_bill . '</th>        
			   
			  </tr> 
			  
			  <tr>     
			  <th style="padding:15px 5px; width:400px;">Leads Bill</th>   
			  <th style="padding:15px 5px; width:400px;">$' . $data['lead_bilings']->total_lead_bill . '</th> 
			  </tr> 


				<tr>     
			  <th style="padding:15px 5px; width:400px;">Paid Bill</th>   
			  <th style="padding:15px 5px; width:400px;"> $' . $data['lead_bilings_paid']->total_lead_bill . '</th> 
			  </tr> 

			  
			  <tr>     
			  <th style="padding:15px 5px; width:400px;">Remaining Balance</th>   
			  <th style="padding:15px 5px; width:400px;">$' . $data['remaining_bill'] . '</th> 
			  </tr> 
			  
			  
			  
			  <tr>     
			  <th style="padding:15px 5px; width:400px;">Status</th>   
			  <th style="padding:15px 5px; width:400px;"> Not paid </th> 
			  </tr> 

			  
			  </thead>       
			 
			   </table>
			   <div class="height" style="height:50px;"></div>
			   <div class="clearfix"></div>
';
        }
        $data['class'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/monthly_billing', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function History()
    {
        $data      = array();
        $user_role = $this->session->userdata('role');
        if ($user_role == '1' || $user_role == '6') {
            $data['id'] = $this->uri->segment(4);
        } else {
            $data['id'] = $this->session->userdata('id');
        }
		$data['lawyer'] = $this->Central_model->first("users","id",$data['id']);
        $data['transactions']  = $this->Reporting_model->search_transactions(array('lawyer_id' => $data['id'], 'charges' => 3));
        $lawyer_leads_price             = $this->Reporting_model->fetchLawyerAllLeadsPrice($data['id']);
		$Lawyer_all_monthly_bill        = $this->Reporting_model->fetchLawyerMonthlyBillTotal($data['id']);
		$total_charge_bill                   = $this->Reporting_model->total_charge_bill($data['id']);
		$lawyerPaidTransactions_new     = $this->Reporting_model->fetchLawyerPaidTransactions($data['id']);
		$data['total_balance']                  = ($lawyer_leads_price[0]->total + $Lawyer_all_monthly_bill[0]->total + $total_charge_bill[0]->total) - $lawyerPaidTransactions_new[0]->total;
        $data['class'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/history');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function makePayment($lawfirm_id, $lawyer_id)
    {
		$lawfirm_id = ($this->session->userdata('role') == 6 ? $this->session->userdata('id') : $lawfirm_id);
        $data               = array();
		$data['lawfirm_id']      = $lawfirm_id;
        $data['lawyer'] = $this->Central_model->first("users","id",$lawyer_id);
		$data['customer_profiles'] = $this->Central_model->select_all_array("credit_cards",array("law_firm" => $lawfirm_id));
        $data['class']      = 'active';
		$this->load->library(array('functions'));
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/makepayment');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function paid()
    {
        $data                   = array();
        $resArr                 = array();
        $data['payment_status'] = $this->input->post('status');
        $lead_id                = $this->input->post('lead_id');
        $lawyer_id              = $this->input->post('lawyer_id');
        $leadPaid               = $this->input->post('leadPaid');
        $response               = $this->Users_model->updateStatus($data, $lead_id, $lawyer_id);
        echo $response;
        exit;
    }
    public function addPayment()
    {
        $data                     = array();
        $data                     = $this->input->post();
        $data['transaction_date'] = date('Y-m-d H:i:s');
        $this->load->model('admin/Leads_model');
        $insert_id = $this->Leads_model->insertData($data);
        if ($insert_id > 0) {
            $this->session->set_flashdata('message', '<p class="message_success"> Added successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/dashboard/makePayment');
        }
    }
    public function oneTimeCharge()
    {
        $data = array();
		$id = $this->uri->segment(4);
		$count = $this->Central_model->count_rows("users", array("id" => $id));
		if($this->input->method(TRUE) == 'POST' && $count > 0) {
			$this->load->library('form_validation');
			$data = $this->input->post();
			$data['transaction_date'] = date('Y-m-d H:i:s');
			$data['lawyer_id'] = $this->uri->segment(4);
			$data['bill_type'] = 2;
			$rules = array(
				[
					'field' => 'amount',
					'label' => 'Amount',
					'rules' => 'trim|required',
				],
				[
					'field' => 'description',
					'label' => 'Description',
					'rules' => 'trim|required',
				],
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
				$this->load->model('admin/Leads_model');
				$insert_id = $this->Leads_model->insertData($data);
				 if ($insert_id > 0) {
					$this->session->set_flashdata('message', '<p class="message_success"> Added successfully.</p>');
					redirect($this->config->item('base_url') . 'admin/dashboard/LawyerBills/'.$id);
				}
			}
		} 
        $data['class']      = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/onetimecharge');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function viewBillInvoice()
    {
        $data                     = array();
        $data['id']               = $this->session->userdata('id');
        $data['Monthly_billings'] = $this->Users_model->fetchMonthlyDetail($data['id']);
        $data['class']            = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/monthly_billing_invoice');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function checkPaymentStatus()
    {
        $PaymentRecords = array();
        $check          = array();
        $PaymentRecords = $this->Users_model->fetchAllPaymentRecords();
        foreach ($PaymentRecords as $PaymentRecord) {
            $PaymentRecord->lawyer_id;
            $created_at = $PaymentRecord->created_at;
            echo "<br>";
            exit;
            $Fifteen_Days_date    = date('m-d-Y', strtotime("+15 day", strtotime('07-05-2016')));
            $Thirty_Days_date     = date('m-d-Y', strtotime("+30 day", strtotime('07-05-2016')));
            $FourtyFive_Days_date = date('m-d-Y', strtotime("+45 day", strtotime('07-05-2016')));
            $Sixty_Days_date      = date('m-d-Y', strtotime("+60 day", strtotime('07-05-2016')));
            if ($created_at > $Fifteen_Days_date && $created_at < $Thirty_Days_date) {
                $checkPayments = $this->Users_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. 15 Day Reminder';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            } elseif ($created_at > $Thirty_Days_date && $created_at < $FourtyFive_Days_date) {
                $checkPayments = $this->Users_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. 30 Day Reminder';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            } elseif ($created_at > $FourtyFive_Days_date && $created_at < $Sixty_Days_date) {
                $checkPayments = $this->Users_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. 45 Day Reminder';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            } elseif ($created_at > $Sixty_Days_date) {
                $checkPayments = $this->Users_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. 60 Day Reminder';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            } else {
                $checkPayments = $this->Users_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. No one';
                $message       = 'Alert. No one';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            }
        }
    }
    public function SearchByStatus()
    {
        $data                = array();
        $status              = $this->input->post('status');
        $count = $this->Users_model->fetchLawyersOnStatusCount($status);
        $data['leads'] = $this->Users_model->fetchLawyersOnStatus($status, $this->limit, $this->offset);
        $data["num_links"] = ceil(count($count) / $this->limit);
        $data['success'] = 1;
        $data['html']    = $this->load->view('admin/dashboard/leads/leads', $data, true);
        echo json_encode($data);
        exit();
    }
    public function searchByCaseType()
    {
        $data                = array();
        $html                = '';
        $case_type           = $this->input->post('case_type');
        $count = $this->Users_model->fetchLawyersOnCaseTypeCount($case_type);
        $data['leads'] = $this->Users_model->fetchLawyersOnCaseType($case_type, $this->limit, $this->offset);
        $data["num_links"] = ceil(count($count) / $this->limit);
        $data['success'] = 1;
        $data['html']    = $this->load->view('admin/dashboard/leads/leads', $data, true);
        echo json_encode($data);
        exit();
    }
    public function searchByMarket()
    {
        $data                = array();
        $html                = '';
        $market              = $this->input->post('market');        
        $count = $this->Users_model->fetchLawyersOnMarketCount($market);
        $data['leads'] = $this->Users_model->fetchLawyersOnMarket($market, $this->limit, $this->offset);
        $data["num_links"] = ceil(count($count) / $this->limit);
        $data['success'] = 1;
        $data['html']    = $this->load->view('admin/dashboard/leads/leads', $data, true);
        echo json_encode($data);
        exit();
    }
    public function searchByCaseId()
    {
        $data                = array();
        $case_id             = $this->input->post('case_id');
        $count = $this->Users_model->fetchLawyersOnCaseIdCount($case_id);
        $data['leads'] = $this->Users_model->fetchLawyersOnCaseId($case_id, $this->limit, $this->offset);
        $data["num_links"] = ceil(count($count) / $this->limit);
        $data['success'] = 1;
        $data['html']    = $this->load->view('admin/dashboard/leads/leads', $data, true);
        echo json_encode($data);
        exit();
    }
    public function advanceSearch()
    {
        $data                = array();
        $text                = $this->input->post('text');
        $advance_search_type = $this->input->post('advance_search_type');
        $count = $this->Users_model->advanceSearchCount($text, $advance_search_type);
        $data['leads'] = $this->Users_model->advanceSearch($text, $advance_search_type, $this->limit, $this->offset);
        $data["num_links"] = ceil(count($count) / $this->limit);
		$data['success'] = 1;
        $data['html']    = $this->load->view('admin/dashboard/leads/leads', $data, true);
        echo json_encode($data);
        exit();
    }
	
	public function billings()
	{
		$data = array();	
		$data['law_firms'] = $this->Users_model->lawFirms();
		$data['class']       = 'active';
		$this->load->view('layouts/admin/header', $data);
		$this->load->view('layouts/admin/sidebar', $data);
		$this->load->view('admin/users/billings');
		$this->load->view('layouts/admin/footer', $data);
	}
    
    /**
     * Dashboard::statements()
     * 
     * @param string $lawyer_id
     * @return void
     */
    
    public function statements($lawyer_id = '' , $history = NULL)
    {
       
        $data       = array();
        $role       = $this->session->userdata('role');
        $user_id    = $this->session->userdata('id');
        
        if($role == 1) {
            // admin
            $past_statement = false;
            if(isset($history)){   
              
                $past_statement = true ;       
            }
            $data['statements'] = $this->Invoice_model->fetchAllStatements($lawyer_id ,$past_statement);
           
        }else if($role == 6) {
            // lawfirm
            $data['statements'] = $this->Invoice_model->fetchStatementsByLawfirm($user_id, $lawyer_id);
            
        }else if($role == 2) {
            // lawyer
            $past_statement = false;
            if (isset($_GET['p']) && $_GET['p'] == 'history')
                {
                        $past_statement = true;
                }

        $data['statements'] = $this->Invoice_model->fetchStatementsByLawyer($user_id, $past_statement);
            
        }else if($role == 7) {
            // lawyer staff
            $data['statements'] = $this->Invoice_model->fetchStatementsByLawyerStaff($user_id);
        }
        $this->load->view('layouts/admin/header');
        $this->load->view('layouts/admin/sidebar');
        $this->load->view('admin/dashboard/statements', $data);
        $this->load->view('layouts/admin/footer');
    }
    
    /**
     * Dashboard::view_statement_pdf()
     * 
     * @param mixed $file_name
     * @return void
     */
    public function view_statement_pdf($file_name)
    {
        $filename = $file_name;
        #$file_path = realpath(BASEPATH . '/../pdfs' . '/' . $file_name);
        $file_path = realpath($_SERVER['DOCUMENT_ROOT'] . '/../pdfs' . '/' . $file_name);
        $file_path_url = base_url('pdfs/' . $file_name);
        header("Content-Length: " . filesize ($file_path) ); 
        header("Content-type: application/pdf"); 
        header("Content-disposition: inline;     
        filename=".basename($file_path));
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        $filepath = readfile($file_path);
    }
    
    /**
     * Dashboard::not_retained_declined()
     * 
     * @return void
     */
    public function not_retained_declined()
    {
        $user_role = $this->session->userdata('role');
        
        if($user_role == 2) {
            $lawyer_id = $this->session->userdata('id');
        }else if($user_role == 6){
            $lawyer_id = array();
            $lawfirm_id = $this->session->userdata('id');
            $lawyers = $this->db->query("SELECT `id` FROM `users` WHERE `role` = '2' AND `law_firms` = '{$lawfirm_id}'")->result();
            foreach($lawyers as $lawyer) {
                $lawyer_id[] = $lawyer->id;
            }
        }
        
        $data = array();
        if($user_role == 2) {
            $data['leads'] = $this->Leads_model->fetchNotRetainedDeclinedLeads($lawyer_id);
        }else if($user_role == 6) {
            $data['leads'] = $this->Leads_model->fetchNotRetainedDeclinedLeadsMultiple($lawyer_id);
        }
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/dashboard/not_retained_declined', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
}
