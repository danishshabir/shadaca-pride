<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Archive extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('latest_url', current_url());
        chechUserSession();
        $this->load->model('admin/Leads_model');
		$this->load->model('Central_model');
    }
    public function index()
    {
        $data                  = array();
        $data['class_leads']   = 'active';
        $data['archive_leads'] = $this->Central_model->select_all_array('leads', array('is_archive' => 1), array(), array(), 'first_name', 'DESC');
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/leads/archieve', $data);
        $this->load->view('layouts/admin/footer', $data); 
    }
    
    /**
     * Archive::searchByCaseId()
     * 
     * @return void
     */
    public function searchByCaseId(){
        $case_id = $this->input->post('case_id');
        $html = '';
        $result = $this->db->query("SELECT * FROM `leads` WHERE is_archive = 1 AND RIGHT(`lead_id_number`, 5) LIKE '%$case_id%' ORDER BY first_name DESC")->result();
        foreach($result as $lead) {
            if($lead->lead_status != 1)
			{ 
				$assigned_detail = getLeadsLawyer($lead->id);
				$assigned_lawyer_law_firm = getLeadsLawyerLawfirm($lead->id);
			}
            if($lead->lead_status == 11){
                $status = "Closed - Incomplete";
            }else if($lead->lead_status == 12){
                $status = "Case Complete - Fee Received";
            }else if($lead->lead_status == 13){
                $status = "Closed - Unassigned";
            }else if($lead->lead_status == 14){
                $status = "Closed - Declined";
            }else if($lead->lead_status == 3){
                $status = "Case Complete - Retained";
            }else if($lead->lead_status == 20){
                $status = 'Closed - Contacted';
            }
            $caseTypeName =  getCaseTypeName($lead->case_type);
            $newJurisdiction = getMarketName($lead->market);
            $stateName =  getStateName($newJurisdiction->state);
            $html .= '<tr>
                        <td>'.$status.'</td>
						<td>'.$lead->lead_id_number.'</td>
						<td>'.($lead->lead_status != 1 && $lead->lead_status != 4 ? ($assigned_lawyer_law_firm['assigned_to'] != "" ? $assigned_lawyer_law_firm['assigned_to'] : "Not Assigned") : 'Not Assigned').'</td>
						<td>'.($lead->lead_status != 1 && $lead->lead_status != 4 ? ($assigned_detail['assigned_to'] != "" ? $assigned_detail['assigned_to'] : "Not Assigned") : 'Not Assigned').'</td>
						<td>'.$lead->first_name.' '.$lead->last_name.'</td>
						<td>'.$caseTypeName[0]['type'].'</td>
						<td>'.$stateName[0]['state_short_name'] . $newJurisdiction->market_name . " - " . $stateName[0]['state_short_name'].'</td> 
						<td>'.($lead->case_description == '' ? 'N/A' : $lead->case_description).'</td>
						<td>'.($lead->date_closed == '' ? 'N/A' : date('M d, Y h:i a', strtotime($lead->date_closed))).'</td>
						<td>'.($lead->date_archived == '' ? 'N/A' : date('M d, Y h:i a', strtotime($lead->date_archived))).'</td>
                        <td>
                            <a href="'.base_url().'admin/leads/view/'.$lead->id.'">View Lead</a>';
                            if(!in_array($lead->lead_status, array(3, 12, 20))){
                                $html .= '| <a onclick="confirm(\'Are you sure you want to perform this action?\') ? window.location.href = \''.base_url("admin/leads/updateLeadStatus/$lead->id/reopen_lead").'\' : false" href="javascript:void(0);">Reopen Lead</a>';
                            }
                        $html .= '</td>
					</tr>';
        }
        if(!$html) $html = '<tr><td colspan="11" class="text-center">No records found!</td></tr>';
        echo $html;
        exit();
    }
}
?>