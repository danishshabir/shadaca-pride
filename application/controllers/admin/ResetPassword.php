<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class ResetPassword extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->load->model('Central_model');
    }
    public function reset($type, $code)
    {
        $data                  = array();
        $data['class']                  = 'active';
		$data['user'] = $this->Central_model->first(($type == 'lawyer' || $type == 'lawstaff' ? 'users' : 'law_firms'), 'unique_code', $code);		
		if($this->input->method(TRUE) == 'POST') {
			$this->load->library('form_validation');
			$rules = array(
				[
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'callback_valid_password',
				],
				[
					'field' => 'confirm_password',
					'label' => 'Confirm Password',
					'rules' => 'matches[password]',
				],
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
				$data['password'] = sha1($this->input->post('password'));
                $result = $this->Central_model->update(($type == 'lawyer' || $type == 'lawstaff' ? 'users' : 'law_firms'), array('password' => $data['password'], 'verified' => 1), 'id', $data['user']->id);
				$this->session->set_flashdata('email', $this->input->post('email'));
				$this->session->set_flashdata('password', $this->input->post('password'));							
				redirect($this->config->item('base_url').'admin/login/login');
			}
		}
        $this->load->view('layouts/admin/header');
        $this->load->view('admin/reset-password/index', $data);
        $this->load->view('layouts/admin/footer', $data); 
    }
    
    public function reset_admin($type, $code)
    {
        $data                  = array();
        $data['class']                  = 'active';
		$data['user'] = $this->Central_model->first(($type == 'lawyer' || $type == 'lawstaff' ? 'users' : 'law_firms'), 'unique_code', $code);		
		if($this->input->method(TRUE) == 'POST') {
			$this->load->library('form_validation');
			$rules = array(
				[
					'field' => 'password',
					'label' => 'Password',
					'rules' => 'callback_valid_password',
				],
				[
					'field' => 'confirm_password',
					'label' => 'Confirm Password',
					'rules' => 'matches[password]',
				],
			);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run())
			{
                if($data['user']->verified == 1){
                    $result = $this->Central_model->update(($type == 'lawyer' || $type == 'lawstaff' ? 'users' : 'law_firms'), array('password' => sha1($this->input->post('password'))), 'id', $data['user']->id);
                    $this->session->set_flashdata('message', '<p class="message_success">Password Reset Successfully</p>');
    				if($type == 'lawyer'){
    					redirect($this->config->item('base_url') . 'admin/users/lawyers');
    				}else if($type == 'lawstaff'){
    					redirect($this->config->item('base_url') . 'admin/users/lawyerStaff');
    				}else if($type == 'law_firms'){
    					redirect($this->config->item('base_url') . 'admin/settings/lawFirms');
    				}
                }else{
                    $this->session->set_flashdata('message','<p style="text-align: center; color: red; font-size: larger;">This Account Is Not Verified.</p>');
                    if($type == 'lawyer'){
						redirect($this->config->item('base_url') . 'admin/users/lawyers');
					}else if($type == 'lawstaff'){
						redirect($this->config->item('base_url') . 'admin/users/lawyerStaff');
					}else if($type == 'law_firms'){
						redirect($this->config->item('base_url') . 'admin/settings/lawFirms');
					}
                }
			}
		}
        $this->load->view('layouts/admin/header');
        $this->load->view('admin/reset-password/index', $data);
        $this->load->view('layouts/admin/footer', $data); 
    }
    
	public function valid_password($password = '')
	{
		$password = trim($password);
		$regex_lowercase = '/[a-z]/';
		$regex_uppercase = '/[A-Z]/';
		$regex_number = '/[0-9]/';
		$regex_special = '/[!@#$%^&*()\-_=+{};:,<.>ยง~]/';
		if (empty($password))
		{
			$this->form_validation->set_message('valid_password', 'The {field} field is required.');
			return FALSE;
		}
		if (preg_match_all($regex_lowercase, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least one lowercase letter.');
			return FALSE;
		}
		if (preg_match_all($regex_uppercase, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least one uppercase letter.');
			return FALSE;
		}
		if (preg_match_all($regex_number, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must have at least one number.');
			return FALSE;
		}
		/* if (preg_match_all($regex_special, $password) < 1)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must have at least one special character.' . ' ' . htmlentities('!@#$%^&*()\-_=+{};:,<.>ยง~'));
			return FALSE;
		} */
		if (strlen($password) < 6)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field must be at least 6 characters in length.');
			return FALSE;
		}
		if (strlen($password) > 32)
		{
			$this->form_validation->set_message('valid_password', 'The {field} field cannot exceed 32 characters in length.');
			return FALSE;
		}
		return TRUE;
	}
}
?>