<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Register extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Leads_model');
        $this->load->model('admin/Users_model');
    }
    public function manage($search = '', $val = '')
    {
        $data   = array();
        $status = '';
        if ($this->session->userdata('role') == 1) {
            if ($search == 'Pending') {
                $status = 1;
            } elseif ($search == 'Assigned') {
                $status = 2;
            } elseif ($search == 'Completed') {
                $status = 3;
            } elseif ($search == 'Drafted') {
                $status = 4;
            } elseif ($search == 'All') {
                $status = '';
            } elseif ($search == 'case_type') {
                $status = $search;
                $val    = str_replace("-", " ", $val);
            } elseif ($search == 'lawyer') {
                $status = $search;
            }
            $data['case_type']  = $this->Leads_model->fetchCaseType();
            $data['leads']      = $this->Leads_model->fetchLeadsLimit('5', $status, $val);
            $data['lawyers']    = $this->Users_model->fetchAllUsers('2');
            $data['class_type'] = $val;
            $view               = 'admin/dashboard/manage_lead';
        }
        if ($this->session->userdata('role') == 5) {
            if ($search == 'Pending') {
                $status = 1;
            } elseif ($search == 'Assigned') {
                $status = 2;
            } elseif ($search == 'Completed') {
                $status = 3;
            } elseif ($search == 'Drafted') {
                $status = 4;
            } elseif ($search == 'All') {
                $status = '';
            } elseif ($search == 'case_type') {
                $status = $search;
                $val    = str_replace("-", " ", $val);
                $val    = str_replace("_", "/", $val);
            } elseif ($search == 'operator') {
                $status = $search;
            }
            $data['case_type']  = $this->Leads_model->fetchCaseType();
            $data['leads']      = $this->Leads_model->fetchLeadsLimit('5', $status, $val);
            $data['lawyers']    = $this->Users_model->fetchAllUsers('2');
            $data['class_type'] = $val;
            $view               = 'admin/dashboard/manage_lead';
        } elseif ($this->session->userdata('role') == 2) {
            if ($search == 'Pending') {
                $status = 2;
            } elseif ($search == 'Completed') {
                $status = 3;
            } elseif ($search == 'All') {
                $status = '';
            }
            $data['case_type']  = $this->Leads_model->fetchCaseType();
            $data['class_type'] = $search;
            $view               = 'admin/dashboard/manage';
            $data['leads']      = $this->Leads_model->fetchLeadsOfLawyersLimit($this->session->userdata('id'), '5', $status);
        }
        $this->session->set_flashdata('class_type', $data['class_type']);
        $data['class'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view($view, $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function detailedView($search = '', $val = '')
    {
        $data   = array();
        $status = '';
        if ($this->session->userdata('role') == 1) {
            if ($search == 'Pending') {
                $status = 1;
            } elseif ($search == 'Assigned') {
                $status = 2;
            } elseif ($search == 'Completed') {
                $status = 3;
            } elseif ($search == 'Drafted') {
                $status = 4;
            } elseif ($search == 'All') {
                $status = '';
            } elseif ($search == 'case_type') {
                $status = $search;
                $val    = str_replace("-", " ", $val);
            } elseif ($search == 'lawyer') {
                $status = $search;
            }
            $data['case_type']  = $this->Leads_model->fetchCaseType();
            $data['leads']      = $this->Leads_model->fetchLeadsLimit('5', $status, $val);
            $data['lawyers']    = $this->Users_model->fetchAllUsers('2');
            $data['class_type'] = $val;
            $view               = 'admin/dashboard/detailedView';
        }
        if ($this->session->userdata('role') == 5) {
            if ($search == 'Pending') {
                $status = 1;
            } elseif ($search == 'Assigned') {
                $status = 2;
            } elseif ($search == 'Completed') {
                $status = 3;
            } elseif ($search == 'Drafted') {
                $status = 4;
            } elseif ($search == 'All') {
                $status = '';
            } elseif ($search == 'case_type') {
                $status = $search;
                $val    = str_replace("-", " ", $val);
            } elseif ($search == 'operator') {
                $status = $search;
            }
            $data['case_type']  = $this->Leads_model->fetchCaseType();
            $data['leads']      = $this->Leads_model->fetchLeadsLimit('5', $status, $val);
            $data['lawyers']    = $this->Users_model->fetchAllUsers('2');
            $data['class_type'] = $val;
            $view               = 'admin/dashboard/detailedView';
        }
        $this->session->set_flashdata('class_type', $data['class_type']);
        $data['class'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view($view, $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function Vacation_mode()
    {
        $search = '';
        $data   = array();
        $status = '';
        if ($this->session->userdata('role') == 5) {
            if ($search == 'Pending') {
                $status = 1;
            } elseif ($search == 'Assigned') {
                $status = 2;
            } elseif ($search == 'Completed') {
                $status = 3;
            } elseif ($search == 'Drafted') {
                $status = 4;
            } elseif ($search == 'All') {
                $status = '';
            } elseif ($search == 'case_type') {
                $status = $search;
                $val    = str_replace("-", " ", $val);
            } elseif ($search == 'operator') {
                $status = $search;
            }
        } elseif ($this->session->userdata('role') == 2) {
            if ($search == 'Pending') {
                $status = 2;
            } elseif ($search == 'Completed') {
                $status = 3;
            } elseif ($search == 'All') {
                $status = '';
            }
            $data['case_type']  = $this->Leads_model->fetchCaseType();
            $data['class_type'] = $search;
            $view               = 'admin/dashboard/manage';
            $data['leads']      = $this->Leads_model->fetchLeadsOfLawyersLimit($this->session->userdata('id'), '5', $status);
        }
        $this->session->set_flashdata('class_type', $data['class_type']);
        $data['class'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/add_holiday');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function check_user_password()
    {
        $id = $this->uri->segment(4);
        $this->load->model('admin/Activity_model');
    }
    public function getLawFirm()
    {
        $cat_id = $this->input->post('id');
        $this->load->model('admin/Users_model');
        $products = $this->Users_model->fetchLawfee($cat_id);
        echo $fee = $products->law_firm_fee;
        exit;
    }
    public function getStateId()
    {
        $cat_id = $this->input->post('id');
        $this->load->model('admin/Users_model');
        $products = $this->Users_model->fetchStateId($cat_id);
        echo $fee = $products->state;
        exit;
    }
    public function Billing()
    {
        $data             = array();
        $data['id']       = $this->session->userdata('id');
        $data['payments'] = $this->Users_model->fetchPayment($data['id']);
        $data['class']    = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/billing');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function MonthlyBilling()
    {
        $data                      = array();
        $data['id']                = $this->session->userdata('id');
        $current_date              = date("m-Y");
        $data['monthly_billings']  = $this->Users_model->fetchMonthlyTotal($data['id']);
        $data['lead_bilings']      = $this->Users_model->fetchLeadsPaymentTotal($data['id']);
        $data['monthly_paid']      = $this->Users_model->fetchMonthlyPaidTotal($data['id']);
        $data['lead_bilings_paid'] = $this->Users_model->fetchLeadsPaymentPaidTotal($data['id']);
        $data['total_bill_paid']   = $data['monthly_paid']->total_m_bill + $data['lead_bilings_paid']->total_lead_bill;
        $data['remaining_bill']    = ($data['monthly_billings']->total_m_bill + $data['lead_bilings']->total_lead_bill) - $data['total_bill_paid'];
        $data['class']             = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/monthly_billing', $data);
        $this->load->view('layouts/admin/footer', $data);
    }
    public function viewBillInvoice()
    {
        $data                     = array();
        $data['id']               = $this->session->userdata('id');
        $data['Monthly_billings'] = $this->Users_model->fetchMonthlyDetail($data['id']);
        $data['class']            = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/users/monthly_billing_invoice');
        $this->load->view('layouts/admin/footer', $data);
    }
    public function checkPaymentStatus()
    {
        $PaymentRecords = array();
        $check          = array();
        $PaymentRecords = $this->Users_model->fetchAllPaymentRecords();
        foreach ($PaymentRecords as $PaymentRecord) {
            $PaymentRecord->lawyer_id;
            $created_at = $PaymentRecord->created_at;
            echo "<br>";
            exit;
            $Fifteen_Days_date    = date('m-d-Y', strtotime("+15 day", strtotime('07-05-2016')));
            $Thirty_Days_date     = date('m-d-Y', strtotime("+30 day", strtotime('07-05-2016')));
            $FourtyFive_Days_date = date('m-d-Y', strtotime("+45 day", strtotime('07-05-2016')));
            $Sixty_Days_date      = date('m-d-Y', strtotime("+60 day", strtotime('07-05-2016')));
            if ($created_at > $Fifteen_Days_date && $created_at < $Thirty_Days_date) {
                $checkPayments = $this->Users_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. 15 Day Reminder';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            } elseif ($created_at > $Thirty_Days_date && $created_at < $FourtyFive_Days_date) {
                $checkPayments = $this->Users_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. 30 Day Reminder';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            } elseif ($created_at > $FourtyFive_Days_date && $created_at < $Sixty_Days_date) {
                $checkPayments = $this->Users_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. 45 Day Reminder';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            } elseif ($created_at > $Sixty_Days_date) {
                $checkPayments = $this->Users_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. 60 Day Reminder';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            } else {
                $checkPayments = $this->Users_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. No one';
                $message       = 'Alert. No one';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            }
        }
    }
}
