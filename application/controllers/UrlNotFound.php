<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UrlNotFound extends CI_Controller {

	function __construct() {
        parent::__construct();
       
    }
	
	//Main index function for the controller UrlNotFound login
	//loding the 404  view
	public function index(){
		
        $this->load->view('layouts/admin/header');  
		$this->load->view('errors/html/error_404');         	
	    $this->load->view('layouts/admin/footer');  
	}
	
		
}

/* End of file Index.php */
/* Location: ./application/controllers/UrlNotFound.php */