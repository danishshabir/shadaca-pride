<?php
defined('BASEPATH') OR exit('No direct script access allowed');
set_time_limit(0);
class Cronjob extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Cron_model');
        $this->load->model('Central_model');
		$this->load->model('admin/Leads_model');
        $this->load->model('admin/Users_model');
        $this->load->model('admin/Reporting_model');
        $this->load->model('admin/Invoice_model');
        $this->load->library('mpdf');
    }
    public function checkPaymentFifteenDate()
    {
        $PaymentRecords = array();
        $check          = array();
        $PaymentRecords = $this->Cron_model->fetchAllPaymentRecords();
        foreach ($PaymentRecords as $PaymentRecord) {
            $PaymentRecord->lawyer_id;
            $created_at        = $PaymentRecord->created_at;
            $created_at        = date('Y-m-d', strtotime($created_at));
            $today_date        = date("m-d-Y");
            $fifteen_Days_date = date('m-d-Y', strtotime("+15 day", strtotime($created_at)));
            $today_date        = date("m-d-Y");
            if ($fifteen_Days_date == $today_date) {
                $checkPayments = $this->Cron_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. This is automatic payment reminder from PrideLegal.com, your account is 15 days overdue.';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            }
        }
    }
    public function checkPaymentThirtyDate()
    {
        $PaymentRecords = array();
        $check          = array();
        $PaymentRecords = $this->Cron_model->fetchAllPaymentRecords();
        foreach ($PaymentRecords as $PaymentRecord) {
            $PaymentRecord->lawyer_id;
            $created_at       = $PaymentRecord->created_at;
            $created_at       = date('Y-m-d', strtotime($created_at));
            $today_date       = date("m-d-Y");
            $thirty_Days_date = date('m-d-Y', strtotime("+30 day", strtotime($created_at)));
            $today_date       = date("m-d-Y");
            if ($thirty_Days_date == $today_date) {
                $checkPayments = $this->Cron_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. This is automatic payment reminder from PrideLegal.com, your account is 30 days overdue.';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            }
        }
    }
    public function checkPaymentFourtyFiveDate()
    {
        $PaymentRecords = array();
        $check          = array();
        $PaymentRecords = $this->Cron_model->fetchAllPaymentRecords();
        foreach ($PaymentRecords as $PaymentRecord) {
            $PaymentRecord->lawyer_id;
            $created_at           = $PaymentRecord->created_at;
            $created_at           = date('Y-m-d', strtotime($created_at));
            $today_date           = date("m-d-Y");
            $fourtyFive_Days_date = date('m-d-Y', strtotime("+45 day", strtotime($created_at)));
            $today_date           = date("m-d-Y");
            if ($fourtyFive_Days_date == $today_date) {
                $checkPayments = $this->Cron_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $to            = $checkPayments->email;
                $subject       = 'Alert. This is automatic payment reminder from PrideLegal.com, your account is 45 days overdue.';
                $message       = 'Check Mail';
                $headers       = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($to, $subject, $message, $headers);
            }
        }
    }
    public function checkPaymentSixtyDate()
    {
        $PaymentRecords = array();
        $check          = array();
        $PaymentRecords = $this->Cron_model->fetchAllPaymentRecords();
        foreach ($PaymentRecords as $PaymentRecord) {
            $PaymentRecord->lawyer_id;
            $created_at      = $PaymentRecord->created_at;
            $created_at      = date('Y-m-d', strtotime($created_at));
            $today_date      = date("m-d-Y");
            $sixty_Days_date = date('m-d-Y', strtotime("+60 day", strtotime($created_at)));
            $today_date      = date("m-d-Y");
            if ($sixty_Days_date == $today_date) {
                $checkPayments  = $this->Cron_model->fetchLawyerRecord($PaymentRecord->lawyer_id);
                $data           = array();
                $data['active'] = 0;
                $this->Cron_model->suspendLawyer($checkPayments->id, $data);
            }
        }
    }
    public function add_monthly_bill()
    {
        $MonthlyBill = array();
        $records  = $this->Central_model->select_all_array("users", array("active" => 1, "fee_type" => 0));
        foreach ($records as $row) {
            $current_date     = date("m-Y");
            $created_at       = date("d-m-Y h:i:s");
            $lawyer_id        = $row->id;
            $count = $this->Central_model->count_rows("monthly_payment", array("lawyer_id" => $lawyer_id, "date_time" => $current_date, "payment_status" => '0'));
            if ($count > '0') {
                echo "Already Exist";
                echo "<br>";
            } else {
                $data                   = array();
                $data['lawyer_id']      = $lawyer_id;
                $data['monthly_bill']   = $row->law_firm_fee;
                $data['created_at']     = date("Y-m-d H:i:s");
                $data['date_time']      = $current_date;
                $data['payment_Status'] = '0';
                $data['email_status']   = '0';
                $this->Central_model->save("monthly_payment", $data);
                echo "Monthly bill added successfully";
                echo "<br>";
            }
        }
    }
	public function add_annualy_bill()
    {
        $MonthlyBill = array();
        $records  = $this->Central_model->select_all_array("users", array("active" => 1, "fee_type" => 1));
        foreach ($records as $row) {
            $current_date     = date("Y");
            $created_at       = date("d-m-Y h:i:s");
            $lawyer_id        = $row->id;
            $count = $this->Central_model->count_rows("annualy_payment", array("lawyer_id" => $lawyer_id, "date_time" => $current_date, "payment_status" => '0'));
            if ($count > '0') {
                echo "Already Exist";
                echo "<br>";
            } else {
                $data                   = array();
                $data['lawyer_id']      = $lawyer_id;
                $data['monthly_bill']   = $row->law_firm_fee;
                $data['created_at']     = date("Y-m-d H:i:s");
                $data['date_time']      = $current_date;
                $data['payment_Status'] = '0';
                $data['email_status']   = '0';
                $this->Central_model->save("annualy_payment", $data);
                echo "Monthly bill added successfully";
                echo "<br>";
            }
        }
    }
    public function completeLeadsMoveToArchive()
    {
        $AllLeads = array();
        $data     = array();
		$archive_leads = array();
        $AllLeads = $this->Cron_model->fetchAllLeads();
        foreach ($AllLeads as $row) {
            $start          = $row->lead_complete_time;
            $end            = date("Y-m-d");
            $startTimeStamp = strtotime($start);
            $endTimeStamp   = strtotime($end);
            $timeDiff       = abs($endTimeStamp - $startTimeStamp);
            $numberDays     = $timeDiff / 86400;
            $numberDays     = intval($numberDays);
            if($numberDays > 30) {
               $id = $this->Central_model->update("leads", array("is_archive" => 1), 'id', $row->id);
			   array_push($archive_leads, $id);
            }
        }
        if (!empty($archive_leads)) {
            echo "Lead Moved";
        } else {
            echo "No Any Complete lead available which is more than 30 Days older";
        }
    }
	
    /**
     * Cronjob::leadAlertReminder()
     * 
     * @return void
     */
    public function leadAlertReminder() {
        set_time_limit(0);
        $AllLeads           = array();
        $data               = array();
		$leads = $this->Cron_model->unopened_leads();
        foreach ($leads as $row) {
            if($row->reminder_email_sent != 1) {
                $day1      = $row->assigned_date;
                $day1      = strtotime($day1);
                $day2      = date('y-m-d H:i:s');
                $day2      = strtotime($day2);
                $diffHours = round(($day2 - $day1) / 3600);
                //12
                if ($diffHours > 24) {
                    $template   = $this->Leads_model->template(7); 
    				$assigned_to = getLeadsLawyerLawfirm($row->lead_id);
    				$subject = $template->subject;
    				$message = $template->content;
    				$message = str_replace("{name}", $row->user_first_name.' '.$row->user_last_name, $message);
    				$message = str_replace("{date} {time}", date('m-d-Y H:i:s a'), $message);
    				$message = str_replace("{law_office_name}", $assigned_to['assigned_to'], $message);
    				$message = str_replace("{link}", '<a href="'.base_url().'admin/leads/view/'.$row->lead_id.'">Click Here</a>', $message);
    				$message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/hl_logo_em.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
    				$subject = $template->subject;
                    $from                     = $template->email_from;
                    $name = $this->config->item('project_title');
                    $to = $row->user_email;
                    send_email($to, $from, $name, $subject, $message); 
    				/*Send Sms*/
    				$sms_message = $template->text_message;
                    $sms_message = str_replace("{case_number}", $row->lead_id_number, $sms_message);
    				sms($row->user_is_sms, $row->user_phone, $sms_message);
                    $this->db->query("UPDATE lawyers_to_leads SET reminder_email_sent = 1 WHERE lead_id = '{$row->lead_id}'");  
                }
            }
        }
        echo "Email sent to lawyers"; 
    }

    public function clearLawyerHolidays()
    {
        $lawyers = $this->Cron_model->fetchLawyersOnHolidays();
        echo '<pre>';print_r($lawyers);exit();
    }

    public function move_to_archive()
    {
        $now = date("Y-m-d H:i:s");
        //$past_date = date("Y-m-d H:i:s", strtotime("-4 hours"));
        #$past_date = date("Y-m-d H:i:s", strtotime("-10 minutes"));
        $past_date = date("Y-m-d H:i:s", strtotime("-31 days"));
        $this->db->query("UPDATE leads SET is_archive = 1, is_archive_lawyer = 1, date_archived = '".$now."' WHERE lead_status IN(3,11,12,13,14) AND is_archive = 0 AND is_archive_lawyer = 0 AND date_closed <= '".$past_date."'");
        die("Completed/Closed Leads Moved To Archive!");
    }
    
    /**
     * Cronjob::print_statements()
     * 
     * @return void
     * @version 1.0
     */
    /*public function print_statements($date_from = '', $date_to = '')
    {
        $data = array();
        
        $data['date_from'] = date('Y-m-d', strtotime('-1 months', strtotime(date('Y-m-01'))));
        
        $data['date_to'] = date('Y-m-01');
        
        if($date_from) $data['date_from'] = $date_from;
        
        if($date_to) $data['date_to'] = $date_to;
        
        $data['transactions'] = $this->Reporting_model->search_transactions($data);
        
        $data['transactions_month'] = date('m-d-Y', strtotime($data['date_from'])) .' to '.date('m-d-Y', strtotime($data['date_to']));
        
        $data['class'] = 'active';
        
        $data['InvoiceDetails'] = $this->Central_model->first("settings","id",1);
        
        echo $html = $this->load->view('admin/users/transactions/invoice_cronjob', $data, true);
        
        #debug($data2);
    }*/
    
    /**
     * Cronjob::print_statements()
     * 
     * @return void
     */
	 
	public function print_statements($month_to_subract = NULL)
    {

        $lawyers = $this->Users_model->fetchAllUsers('2');
        #$lawyers = $this->db->query("SELECT * FROM users WHERE id = '333'")->result();
        #$path = realpath(BASEPATH . '/../pdfs/');
        $path = realpath($_SERVER['DOCUMENT_ROOT'] . '/../pdfs/');


        $from_date = "";

        $month =  "";

        $year = "";

        $days = "";

        $to_date = "";
        $double_last_month_days = "";
        $double_last_month = "";


        if($month_to_subract != NULL)
        {
            //invoice_month is -1
            //earlier transaction month is -2
            $from_date = date('Y-m-d', strtotime("-$month_to_subract months", strtotime(date('Y-m-01'))));
            //$month = date('m' ) - 9;
            $month =  date('m', strtotime("-$month_to_subract months", strtotime(date('Y-m-01'))));

            $year = date('Y', strtotime($from_date));

            $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

            $to_date = date("Y-m-d", strtotime("$year-$month-$days"));
            $month_to_subract++;
            $double_last_month_days = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime("-$month_to_subract months", time())), date('Y'));
            $double_last_month = date("Y-m-$double_last_month_days", strtotime("-$month_to_subract months", time()));

        }
        else
        {
            //invoice_month is -1
            //earlier transaction month is -2
            $from_date = date('Y-m-d', strtotime('-9 months', strtotime(date('Y-m-01'))));
            //$month = date('m' ) - 9;
            $month =  date('m', strtotime('-9 months', strtotime(date('Y-m-01'))));

            $year = date('Y', strtotime($from_date));

            $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

            $to_date = date("Y-m-d", strtotime("$year-$month-$days"));

            $double_last_month_days = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime('-10 months', time())), date('Y'));
            $double_last_month = date("Y-m-$double_last_month_days", strtotime('-10 months', time()));
        }
        $statement_date = date('m-d-Y', strtotime($to_date . ' +1 day'));

        $type = 3;

        $tmp_array = array();
        foreach($lawyers as $lawyer) {
            $total = 0;
            //echo "select * from (SELECT `leads`.`price` as amount , CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`billed` = 'yes' UNION ALL SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment` UNION ALL SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1 UNION ALL SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2 UNION ALL SELECT `amount` as amount , CONCAT('Credit/debit payment detail (Discover (', credit_debit, '))') as description, transaction_date , lawyer_id as lawyer_id, 'Payment' as status FROM `credit_card_transactions` WHERE `status` = 2 ) table1 WHERE transaction_date != '' AND lawyer_id = {$lawyer->id} AND Date(transaction_date) <= date('{$double_last_month}') order by transaction_date DESC";
            //exit();
            $totals = $this->db->query("select * from (SELECT `leads`.`price` as amount , CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`billed` = 'yes' UNION ALL SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment` UNION ALL SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1 UNION ALL SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2 UNION ALL SELECT `amount` as amount , CONCAT('Credit/debit payment detail (Discover (', credit_debit, '))') as description, transaction_date , lawyer_id as lawyer_id, 'Payment' as status FROM `credit_card_transactions` WHERE `status` = 2 ) table1 WHERE transaction_date != '' AND lawyer_id = {$lawyer->id} AND Date(transaction_date) <= date('{$double_last_month}') order by transaction_date DESC")->result();
            foreach($totals as $row) {
                if(stripos($row->status, 'Payment') !== FALSE)
                    $total = $total - $row->amount;
                else
                    $total = $total + $row->amount;
            }
            $tmp_array['lawyer_'.$lawyer->id] = $total;
        }
        foreach($lawyers as $lawyer) {
            $lawyer_id = $lawyer->id;
            //echo $lawyer_id . '<br />';
            $lawyer_fname = $lawyer->name;
            $lawyer_lname = $lawyer->lname;
            $random_str = '';
            $charlist = 'abcdefghijklmnopqrstuvwxyz0123456789';
            for($i = 0; $i < 10; $i++){
                $random_str .= $charlist[rand(0, (strlen($charlist) - 1))];
            }
            
            $data = array();
            $data['lawyer_id'] = $lawyer_id;
            $data['lawyer_fname'] = $lawyer_fname;
            $data['lawyer_lname'] = $lawyer_lname;
			$data['charges'] = $type;
			$data['date_from'] = $from_date;
			$data['date_to'] = $to_date;
			$data['statement_date'] = $statement_date;
			$data['transactions_month'] = date('m-d-Y', strtotime($from_date)) .' to '.date('m-d-Y', strtotime($to_date));
            //$data['transactions_month'] = date('F, Y', strtotime($from_date));
            $data['class'] = 'active';
            $data['InvoiceDetails'] = $this->Central_model->first("settings","id",1);
            $data['transactions'] = $this->Invoice_model->transactions($data);

            /*
            echo $this->db->last_query();
            echo "<br><br><br><br>";
            print_r($data['transactions']);
            */

            //$data['transactions'] = $this->Reporting_model->search_transactions_cronjob($data);
            //echo $this->db->last_query(); die;
            $data['lawyer'] = $this->Central_model->first("users","id",$data['lawyer_id']);
            $law_firm_fee = number_format($data['lawyer']->law_firm_fee, 2);
            $data['law_firm'] = $this->Central_model->first("law_firms","id",$data['lawyer']->law_firms);
            $data['jurisdiction_state'] = $this->Central_model->first("jurisdiction_states","id",$data['law_firm']->state);
            $data['lawyer_carried_total'] = $tmp_array['lawyer_'.$lawyer_id];
            $html = $this->load->view('admin/users/transactions/invoice_cronjob', $data, true);
			
            #$file_name = 'invoice-' . $random_str . '-' . $lawyer_id . '.pdf';
            $file_name = 'Statement-' . date('Y', strtotime($from_date)) . '-' . ucfirst(date('F', strtotime($from_date))) . '-' . $lawyer_id . '.pdf';

            $monthSave = date('F', strtotime($from_date));
            $yearSave = date('Y', strtotime($from_date));
            $save = array(
                'lawyer_id' => $lawyer_id,
                'month' => $monthSave,
                'year' => $yearSave,
                'file_name' => $file_name,
                'created_at' => date('Y-m-d H:i:s', strtotime($statement_date))
            );

            if(!isset($_GET['debug'])) {
                if(!$this->Central_model->update_monthly_statement($save, $lawyer_id, $monthSave, $yearSave)) {
                    $this->Central_model->save('monthly_statements', $save);
                }
                $this->mpdf = new mPDF();
                $this->mpdf->WriteHTML($html);
                $this->mpdf->Output($path . '/' . $file_name, 'F');
            }


            /*

            $save = array(
                'lawyer_id' => $lawyer_id,
                'month' => date('F', strtotime($from_date)),
                'year' => date('Y', strtotime($from_date)),
                'file_name' => $file_name,
                'created_at' => date('Y-m-d H:i:s')
            );


            if(!isset($_GET['debug'])) {

                //$this->Central_model->save('monthly_statements', $save);
                $this->mpdf = new mPDF();
                $this->mpdf->WriteHTML($html);
                $this->mpdf->Output($path . '/' . $file_name, 'F');
            }
*/
            /* sending email */
            
			/*$template   = $this->Leads_model->template(18); 
			$subject = $template->subject;
			$message = $template->content;
			$message = str_replace("{name}", $lawyer->name.' '.$lawyer->lname, $message);
			$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
			$message = str_replace("{month} {year}", date('F Y', strtotime($from_date)), $message);
			$message = str_replace("{link}", '<a href="'.base_url('admin/dashboard/statements').'">'.base_url('admin/dashboard/statements').'</a>', $message);
            $message = str_replace("{image}", '<img src="' . base_url() . 'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
            $from    = $template->email_from;
            $name = $this->config->item('project_title');
            $to = $lawyer->email;
            //$to = 'hassan_ejaz@astutesol.com';
            //send_email($to, $from, $name, $subject, $message);*/
            echo $html;
            echo "<br><br><br><br>";
            if(isset($_GET['debug'])) exit();
        }
    } 
    public function print_statements_2($month_to_subract = NULL)
    {
        $lawyers = $this->Users_model->fetchAllUsers('2');
        #$lawyers = $this->db->query("SELECT * FROM users WHERE id = '333'")->result();
        #$path = realpath(BASEPATH . '/../pdfs/');
        $path = realpath($_SERVER['DOCUMENT_ROOT'] . '/../pdfs/');

        $from_date = "";

        $month =  "";

        $year = "";

        $days = "";

        $to_date = "";
        $double_last_month_days = "";
        $double_last_month = "";


        if($month_to_subract != NULL)
        {
            //invoice_month is -1
            //earlier transaction month is -2
            $from_date = date('Y-m-d', strtotime("-$month_to_subract months", strtotime(date('Y-m-01'))));
            //$month = date('m' ) - 9;
            $month =  date('m', strtotime("-$month_to_subract months", strtotime(date('Y-m-01'))));

            $year = date('Y', strtotime($from_date));

            $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

            $to_date = date("Y-m-d", strtotime("$year-$month-$days"));
            $month_to_subract++;
            $double_last_month_days = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime("-$month_to_subract months", time())), date('Y'));
            $double_last_month = date("Y-m-$double_last_month_days", strtotime("-$month_to_subract months", time()));

        }
        else
        {
            //invoice_month is -1
            //earlier transaction month is -2
            $from_date = date('Y-m-d', strtotime('-1 months', strtotime(date('Y-m-01'))));
            //$month = date('m' ) - 9;
            $month =  date('m', strtotime('-1 months', strtotime(date('Y-m-01'))));

            $year = date('Y', strtotime($from_date));

            $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

            $to_date = date("Y-m-d", strtotime("$year-$month-$days"));

            $double_last_month_days = cal_days_in_month(CAL_GREGORIAN, date('m', strtotime('-2 months', time())), date('Y'));
            $double_last_month = date("Y-m-$double_last_month_days", strtotime('-2 months', time()));
        }
        $statement_date = date('m-d-Y', strtotime($to_date . ' +1 day'));

        $type = 3;
        $tmp_array = array();
        foreach($lawyers as $lawyer) {
            $total = 0;
            $totals = $this->db->query("select * from (SELECT `leads`.`price` as amount , CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`billed` = 'yes' UNION ALL SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment` UNION ALL SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1 UNION ALL SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2 UNION ALL SELECT `amount` as amount , CONCAT('Credit/debit payment detail (Discover (', credit_debit, '))') as description, transaction_date , lawyer_id as lawyer_id, 'Payment' as status FROM `credit_card_transactions` WHERE `status` = 2 ) table1 WHERE transaction_date != '' AND lawyer_id = {$lawyer->id} AND Date(transaction_date) <= date('{$double_last_month}') order by transaction_date DESC")->result();
            foreach($totals as $row) {
                if(stripos($row->status, 'Payment') !== FALSE)
                    $total = $total - $row->amount;
                else
                    $total = $total + $row->amount;
            }
            $tmp_array['lawyer_'.$lawyer->id] = $total;
        }
        foreach($lawyers as $lawyer) {
			
            $lawyer_id = $lawyer->id;
            //echo $lawyer_id . '<br />';
            $lawyer_fname = $lawyer->name;
            $lawyer_lname = $lawyer->lname;
            $random_str = '';
            $charlist = 'abcdefghijklmnopqrstuvwxyz0123456789';
            for($i = 0; $i < 10; $i++){
                $random_str .= $charlist[rand(0, (strlen($charlist) - 1))];
            }
            
            $data = array();
            $data['lawyer_id'] = $lawyer_id;
            $data['lawyer_fname'] = $lawyer_fname;
            $data['lawyer_lname'] = $lawyer_lname;
			$data['charges'] = $type;
			$data['date_from'] = $from_date;
			$data['date_to'] = $to_date;
			$data['transactions_month'] = date('m-d-Y', strtotime($from_date)) .' to '.date('m-d-Y', strtotime($to_date));
            //$data['transactions_month'] = date('F, Y', strtotime($from_date));
            $data['class'] = 'active';
            $data['InvoiceDetails'] = $this->Central_model->first("settings","id",1);
            $data['transactions'] = $this->Invoice_model->transactions($data);
            //$data['transactions'] = $this->Reporting_model->search_transactions_cronjob($data);
            //echo $this->db->last_query(); die;
            $data['lawyer'] = $this->Central_model->first("users","id",$data['lawyer_id']);
            $law_firm_fee = number_format($data['lawyer']->law_firm_fee, 2);
            $data['law_firm'] = $this->Central_model->first("law_firms","id",$data['lawyer']->law_firms);
            $data['jurisdiction_state'] = $this->Central_model->first("jurisdiction_states","id",$data['law_firm']->state);
            $data['lawyer_carried_total'] = $tmp_array['lawyer_'.$lawyer_id];
            $html = $this->load->view('admin/users/transactions/invoice_cronjob', $data, true);
            #$file_name = 'invoice-' . $random_str . '-' . $lawyer_id . '.pdf';
            $file_name = 'Statement-' . date('Y', strtotime($from_date)) . '-' . ucfirst(date('F', strtotime($from_date))) . '-' . $lawyer_id . '.pdf';
            $monthSave = date('F', strtotime($from_date));
            $yearSave = date('Y', strtotime($from_date));
            $save = array(
                'lawyer_id' => $lawyer_id,
                'month' => $monthSave,
                'year' => $yearSave,
                'file_name' => $file_name,
                'created_at' => date('Y-m-d H:i:s', strtotime($statement_date))
            );

            // if(!isset($_GET['debug'])) {
            //     if(!$this->Central_model->update_monthly_statement($save, $lawyer_id, $monthSave, $yearSave)) {
            //         $this->Central_model->save('monthly_statements', $save);
            //     }
            //     //$this->mpdf = new mPDF();
            //     //$this->mpdf->WriteHTML($html);
            //     //$this->mpdf->Output($path . '/' . $file_name, 'F');
            // }
			
            /* sending email */
            /*$template   = $this->Leads_model->template(18); 
			$subject = $template->subject;
			$message = $template->content;
			$message = str_replace("{name}", $lawyer->name.' '.$lawyer->lname, $message);
			$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
			$message = str_replace("{month} {year}", date('F Y', strtotime($from_date)), $message);
			$message = str_replace("{link}", '<a href="'.base_url('admin/dashboard/statements').'">'.base_url('admin/dashboard/statements').'</a>', $message);
            $message = str_replace("{image}", '<img src="' . base_url() . 'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
            $from    = $template->email_from;
            $name = $this->config->item('project_title');
            $to = $lawyer->email;
			$to = 'haris_riaz@astutesol.com';
            //send_email($to, $from, $name, $subject, $message);
            
            //echo "<br>";
            if(isset($_GET['debug'])) exit(); */
			//echo $html;
        }

        // echo '<pre>' ;
        // print_r($lawyers); 
        // die ;
		
		$is_emails = array_filter($lawyers, function ($e) {
			return $e->statement_email == 1;
        });
		foreach($is_emails as $is_email){
			 /* sending email */
            $template   = $this->Leads_model->template(18); 
			$subject = $template->subject;
			$message = $template->content;
			$message = str_replace("{name}", $lawyer->name.' '.$lawyer->lname, $message);
			$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
			$message = str_replace("{month} {year}", date('F Y', strtotime($from_date)), $message);
			$message = str_replace("{link}", '<a href="'.base_url('admin/dashboard/statements').'">'.base_url('admin/dashboard/statements').'</a>', $message);
            $message = str_replace("{image}", '<img src="' . base_url() . 'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
            $from    = $template->email_from; 
            $name = $this->config->item('project_title');
            $to = $is_email->email;
			//$to = 'danish@astutesol.com';
            send_email($to, $from, $name, $subject, $message);
			echo '<pre>', print_r($is_email,true),'</pre>';
		}
		
    }
    
    /**
     * Cronjob::archive_30_days_old_contacted_leads()
     * 
     * @return void
     */
    public function archive_30_days_old_contacted_leads()
    {
        $past_date = date("Y-m-d H:i:s", strtotime("-30 days"));
        //echo $past_date .'<br />';
        $this->db->query("UPDATE `leads` SET `is_archive` = '1', `is_archive_lawyer` = '1', `lead_status` = '20' WHERE `lead_status` IN (8) AND `created_at` < '{$past_date}'");
        echo $this->db->affected_rows() . " row(s) have been updated.";
        exit();
    }
}