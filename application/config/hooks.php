<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

/* For Maintenance Mode */
if (!defined('BASEPATH')) exit('No direct script access allowed');
$hook['post_controller'][] = array(
  'class' => 'maintenance',
  'function' => 'maintenance',
  'filename' => 'maintenance.php',
  'filepath' => 'hooks',
  'params' => array()
);




?>