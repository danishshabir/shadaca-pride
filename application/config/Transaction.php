<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	require 'auth/vendor/autoload.php';
    use net\authorize\api\contract\v1 as AnetAPI;
    use net\authorize\api\controller as AnetController;

	define("AUTHORIZENET_API_LOGIN_ID", "277SGxtK8");
    define("AUTHORIZENET_TRANSACTION_KEY", "5337kd3cH6G5cuA7");
    define("AUTHORIZENET_SANDBOX", true);
	
		
	class Transaction extends CI_Controller {

	function __construct() {
        parent::__construct();
		//chechUserSession();
		$this->load->model('admin/Leads_model');
		$this->load->model('admin/Users_model');	
		$this->load->model('admin/Reporting_model');	
		
    }
	
		 

  function chargeCreditCard(){
	  
	  
	  $data = array();
	  $id = $_POST['lawyer_id'];
	  $amount = $_POST['amount'];
	  $card_no = $_POST['card_no'];
	  $exp_date = date('m/d', strtotime($_POST['exp_date']));
	  $card_code = $_POST['credit_card_code'];
	  $newCard = $_POST['newCard'];
	  $role = $this->session->userdata('role');
	  //$name = $_POST['name'];
	  $email = 'ta@yahoo.com';

	  //require_once(APPPATH.'third_party/sdk/vendor/ajbdev/authorizenet-php-api/AuthorizeNet.php');
	  //require_once(APPPATH.'third_party/auth/PaymentTransactions/charge-credit-card.php');
	  
	  
      // Common setup for API credentials
      $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
      $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
      $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
      $refId = 'ref' . time();

      // Create the payment data for a credit card
      $creditCard = new AnetAPI\CreditCardType();
      $creditCard->setCardNumber($card_no);
      $creditCard->setExpirationDate($exp_date);
      $creditCard->setCardCode($card_code);
      $paymentOne = new AnetAPI\PaymentType();
      $paymentOne->setCreditCard($creditCard);

      $order = new AnetAPI\OrderType();
      $order->setDescription("New Item");

      //create a transaction
      $transactionRequestType = new AnetAPI\TransactionRequestType();
      $transactionRequestType->setTransactionType( "authCaptureTransaction"); 
      $transactionRequestType->setAmount($amount);
      $transactionRequestType->setOrder($order);
      $transactionRequestType->setPayment($paymentOne);
      

      $request = new AnetAPI\CreateTransactionRequest();
      $request->setMerchantAuthentication($merchantAuthentication);
      $request->setRefId($refId);
      $request->setTransactionRequest( $transactionRequestType);
      $controller = new AnetController\CreateTransactionController($request);
      $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
      /*
	  echo "<pre>";
	  print_r($response);
	  exit;
	  */
	  
      if ($response != null)
      {
        if($response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK)
        {
          $tresponse = $response->getTransactionResponse();
          
	        if ($tresponse != null && $tresponse->getMessages() != null)   
          {
			//echo "Charge Credit Card Reference id : " . $refId  . "\n"; 
				//echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
            /*
				echo "Charge Credit Card AUTH CODE : " . $tresponse->getAuthCode() . "\n";
				echo "Charge Credit Card TRANS ID  : " . $tresponse->getTransId() . "\n";
				echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n"; 
				echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
				echo " Card Code verfication : " . $tresponse->getCvvResultCode() . "\n";	
			*/
				if($tresponse->getResponseCode() == '1'){
					$data['lawyer_id'] = $id;
					//$data['transaction_type	'] = $tresponse->accountType();
					$data['amount'] = $_POST['amount'];
					$data['status'] = $_POST['status'];
					$data['transaction_date'] = date('Y-m-d H:i:s');
					$data['credit_debit'] = 'D';
					$data['card_type'] = $_POST['card_type'];
					 
					
					if($role == '1'){
						$data['transaction_by'] = '1';
					}
					else{
						$data['transaction_by'] = '2';
					}
					
					// For checking refernce Id against lawyer.
					$LawyerRefrenceId = $this->Reporting_model->checkLawyerRefrenceId($id);
							
					if(empty($LawyerRefrenceId) || empty($LawyerRefrenceId->refrence_id)){
						// update Lawyer Refernce Id
						
						//$customer_profile_id = $this->createCustomerProfile($email);
						//$dataNew['cust_prof_id'];
						//$dataNew['paym_prof_id'];
						
						
						$dataNew['refrence_id'] = $refId;
						$LawyerRefrenceUpdate = $this->Reporting_model->updateLawyerRefrenceId($id,$dataNew);
						//echo $LawyerRefrenceUpdate;
					}
					else if($newCard == '1' || !empty($newCard)){
						// update Lawyer Refernce Id
						$dataNew['refrence_id'] = $refId;
						$LawyerRefrenceUpdate = $this->Reporting_model->updateLawyerRefrenceId($id,$dataNew);
						//echo $LawyerRefrenceUpdate;
					}
					
					
					$Transaction = $this->Reporting_model->insertTransactionRecord($data);
					if($Transaction){
						 //echo "here in if transaction if condition";
						 $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #3e923a; font-size: 18px;'>".$tresponse->getMessages()[0]->getDescription()."</p>");
							//redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id); 
						   
							echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
						   

					}
					else{
							//echo "here in transaction else condition";
							$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$tresponse->getMessages()[0]->getDescription()."</p>");		
							 //redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id); 
							echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
						   
						}
					
				}
				
          }
          else
          {
            //echo "Transaction Failed \n";
            if($tresponse->getErrors() != null)
            {
              //echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
              //echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n"; 
			  //echo "here in transaction Failed condition";
			   //$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$tresponse->getErrors()[0]->getErrorCode()."</p>");	
			   $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>Transaction Failed ".$tresponse->getMessages()[0]->getDescription()."</p>");
			   echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
						   	
            }
          }
        }
        else
        {
		  //echo "here in transaction Response Failed condition";
          //echo "Transaction Failed \n";
          $tresponse = $response->getTransactionResponse();
          if($tresponse != null && $tresponse->getErrors() != null)
          {
			//echo "here in if transaction Response Failed condition";
            //echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
            //echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";   
			//$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$tresponse->getErrors()[0]->getErrorCode()."</p>");	
			//redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id);
			$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>".$tresponse->getErrors()[0]->getErrorText()."</p>");
			echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
							

			
          }
          else
          {
			//echo "here in else transaction Response Failed condition";
            //echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
            //echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
			   //$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$response->getMessages()->getMessage()[0]->getCode()."</p>");	
			   //redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id);
			$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>".$tresponse->getMessages()[0]->getMessage()."</p>");
			echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
			
				
          }
        }      
      }
      else
      {
        //echo  "No response returned \n";
		
		$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'> No response returned </p>");
		echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
			
      }

      //return $response;
	  //$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$response."</p>");	
	  //redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id);
	  
	  
	 if(!defined('DONT_RUN_SAMPLES'))
      chargeCreditCard(\SampleCode\Constants::SAMPLE_AMOUNT);
	
  }
  
  
  
  
  	 

  function oneTimeCharge(){
	  
	  
	  $data = array();
	  $id = $_POST['lawyer_id'];
	  $amount = $_POST['amount'];
	  $card_no = $_POST['card_no'];
	  $exp_date = date('m/d', strtotime($_POST['exp_date']));
	  $card_code = $_POST['credit_card_code'];
	  $description = $_POST['description'];
	  $quantity = $_POST['quantity'];
	  $role = $this->session->userdata('role');;
	  //$name = $_POST['name'];
	 
	  //require_once(APPPATH.'third_party/sdk/vendor/ajbdev/authorizenet-php-api/AuthorizeNet.php');
	  //require_once(APPPATH.'third_party/auth/PaymentTransactions/charge-credit-card.php');
	  
	  
      // Common setup for API credentials
      $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
      $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
      $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
      $refId = 'ref' . time();

      // Create the payment data for a credit card
      $creditCard = new AnetAPI\CreditCardType();
      $creditCard->setCardNumber($card_no);
      $creditCard->setExpirationDate($exp_date);
      $creditCard->setCardCode($card_code);
      $paymentOne = new AnetAPI\PaymentType();
      $paymentOne->setCreditCard($creditCard);

      $order = new AnetAPI\OrderType();
      $order->setDescription("New Item");

      //create a transaction
      $transactionRequestType = new AnetAPI\TransactionRequestType();
      $transactionRequestType->setTransactionType( "authCaptureTransaction"); 
      $transactionRequestType->setAmount($amount);
      $transactionRequestType->setOrder($order);
      $transactionRequestType->setPayment($paymentOne);
      

      $request = new AnetAPI\CreateTransactionRequest();
      $request->setMerchantAuthentication($merchantAuthentication);
      $request->setRefId($refId);
      $request->setTransactionRequest( $transactionRequestType);
      $controller = new AnetController\CreateTransactionController($request);
      $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
      /*
	  echo "<pre>";
	  print_r($response);
	  exit;
	  */
	  
      if ($response != null)
      {
        if($response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK)
        {
          $tresponse = $response->getTransactionResponse();
          
	        if ($tresponse != null && $tresponse->getMessages() != null)   
          {
			//echo "Charge Credit Card Reference id : " . $refId  . "\n"; 
				//echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
            /*
				echo "Charge Credit Card AUTH CODE : " . $tresponse->getAuthCode() . "\n";
				echo "Charge Credit Card TRANS ID  : " . $tresponse->getTransId() . "\n";
				echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n"; 
				echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
				echo " Card Code verfication : " . $tresponse->getCvvResultCode() . "\n";	
			*/
				if($tresponse->getResponseCode() == '1'){
					$data['lawyer_id'] = $id;
					//$data['transaction_type	'] = $tresponse->accountType();
					$data['amount'] = $_POST['amount'];
					$data['status'] = $_POST['status'];
					$data['transaction_date'] = date('Y-m-d H:i:s');
					$data['credit_debit'] = 'D';
					$data['card_type'] = $_POST['card_type'];
					$data['description'] = $_POST['description'];
					$data['quantity'] = $_POST['quantity'];
					
					if($role == '1'){
						$data['transaction_by'] = '1';
					}
					else{
						$data['transaction_by'] = '2';
					}
					
					// For checking refernce Id against lawyer.
					$LawyerRefrenceId = $this->Reporting_model->checkLawyerRefrenceId($id);
							
					if(empty($LawyerRefrenceId) || empty($LawyerRefrenceId->refrence_id)){
						// update Lawyer Refernce Id
						$dataNew['refrence_id'] = $refId;
						$LawyerRefrenceUpdate = $this->Reporting_model->updateLawyerRefrenceId($id,$dataNew);
						//echo $LawyerRefrenceUpdate;
					}
					
					
					$Transaction = $this->Reporting_model->insertTransactionRecord($data);
					if($Transaction){
						 //echo "here in if transaction if condition";
						 $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #3e923a; font-size: 18px;'>".$tresponse->getMessages()[0]->getDescription()."</p>");
							//redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id); 
						   
							echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
						   

					}
					else{
							//echo "here in transaction else condition";
							$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$tresponse->getMessages()[0]->getDescription()."</p>");		
							 //redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id); 
							echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
						   
						}
					
				}
				
          }
          else
          {
            //echo "Transaction Failed \n";
            if($tresponse->getErrors() != null)
            {
              //echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
              //echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n"; 
			  //echo "here in transaction Failed condition";
			   //$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$tresponse->getErrors()[0]->getErrorCode()."</p>");	
			   $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>Transaction Failed ".$tresponse->getMessages()[0]->getDescription()."</p>");
			   echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
						   	
            }
          }
        }
        else
        {
		  //echo "here in transaction Response Failed condition";
          //echo "Transaction Failed \n";
          $tresponse = $response->getTransactionResponse();
          if($tresponse != null && $tresponse->getErrors() != null)
          {
			//echo "here in if transaction Response Failed condition";
            //echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
            //echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";   
			//$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$tresponse->getErrors()[0]->getErrorCode()."</p>");	
			//redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id);
			$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>".$tresponse->getErrors()[0]->getErrorText()."</p>");
			echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
							

			
          }
          else
          {
			//echo "here in else transaction Response Failed condition";
            //echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
            //echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
			   //$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$response->getMessages()->getMessage()[0]->getCode()."</p>");	
			   //redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id);
			$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'>".$tresponse->getMessages()[0]->getMessage()."</p>");
			echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
			
				
          }
        }      
      }
      else
      {
        //echo  "No response returned \n";
		
		$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #E02700; font-size: 18px;'> No response returned </p>");
		echo "<script> window.location='" . $this->config->item('base_url') . 'admin/dashboard/LawyerBillings/' .$id. "'  </script>";
			
      }

      //return $response;
	  //$this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$response."</p>");	
	  //redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id);
	  
	  
	 if(!defined('DONT_RUN_SAMPLES'))
      chargeCreditCard(\SampleCode\Constants::SAMPLE_AMOUNT);
	
  }
  
  
  
  // For creating customer profile
  
  function createCustomerProfile($email){
	  
	  $email = "we@gmail.com";
      // Common setup for API credentials
      $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
      $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
      $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
      $refId = 'ref' . time();

	  // Create the payment data for a credit card
	  $creditCard = new AnetAPI\CreditCardType();
	  $creditCard->setCardNumber("6011000000000012");
	  $creditCard->setExpirationDate("04/15");
	  $paymentCreditCard = new AnetAPI\PaymentType();
	  $paymentCreditCard->setCreditCard($creditCard);

	  // Create the Bill To info
	  $billto = new AnetAPI\CustomerAddressType();
	  $billto->setFirstName("Mohsin");
	  $billto->setLastName("Shaikh");
	  $billto->setCompany("Astutesol");
	  $billto->setAddress("Lahore PCSIR");
	  $billto->setCity("Lahore");
	  $billto->setState("punjab");
	  $billto->setZip("44628");
	  $billto->setCountry("PAK");
	  
	 // Create a Customer Profile Request
	 //  1. create a Payment Profile
	 //  2. create a Customer Profile   
	 //  3. Submit a CreateCustomerProfile Request
	 //  4. Validate Profiiel ID returned

	  $paymentprofile = new AnetAPI\CustomerPaymentProfileType();

	  $paymentprofile->setCustomerType('individual');
	  $paymentprofile->setBillTo($billto);
	  $paymentprofile->setPayment($paymentCreditCard);
	  $paymentprofiles[] = $paymentprofile;
	  $customerprofile = new AnetAPI\CustomerProfileType();
	  $customerprofile->setDescription("Customer 2 Test PHP");

	  $customerprofile->setMerchantCustomerId("M_".$email);
	  $customerprofile->setEmail($email);
	  $customerprofile->setPaymentProfiles($paymentprofiles);

	  $request = new AnetAPI\CreateCustomerProfileRequest();
	  $request->setMerchantAuthentication($merchantAuthentication);
	  $request->setRefId( $refId);
	  $request->setProfile($customerprofile);
	  $controller = new AnetController\CreateCustomerProfileController($request);
	  $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
	  
	  
	  if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
	  {
		  echo "Succesfully create customer profile : " . $response->getCustomerProfileId() . "\n";
		  $paymentProfiles = $response->getCustomerPaymentProfileIdList();
		  echo "SUCCESS: PAYMENT PROFILE ID : " . $paymentProfiles[0] . "\n";
	   }
	  else
	  {
		  echo "ERROR :  Invalid response\n";
		  $errorMessages = $response->getMessages()->getMessage();
          echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
	  }
	  return $response;
	  
	   if(!defined('DONT_RUN_SAMPLES'))
      createCustomerProfile("test123@test.com");
    
  }
 
  
  
  
  
  
  
  
  
  
  
  /*
    function chargeCreditCard(){
	  $data = array();
	  $id = $_POST['lawyer_id'];
	  require_once(APPPATH.'third_party/sdk/vendor/ajbdev/authorizenet-php-api/AuthorizeNet.php');
	  $sale = new AuthorizeNetAIM;
	    $sale->amount = $_POST['amount'];
		$sale->card_num = $_POST['card_no']; 
		//'6011000000000012';
		$sale->exp_date = $_POST['exp_date'];
		//'04/15';
		$response = $sale->authorizeAndCapture();
		
		

		if ($response->approved == '1') {
			
			$transaction_id = $response->transaction_id;
			$data['lawyer_id'] = $id;
			$data['transaction_type	'] = $response->transaction_type;
			//$data['invoice_number'] = $response->invoice_number;
			$data['amount'] = $_POST['amount'];
			$data['status'] = $_POST['status'];
			$data['transaction_date'] = date('m d Y H:i:s');
			$data['credit_debit'] = 'D';
			
			$Transaction = $this->Reporting_model->insertTransactionRecord($data);
			if($Transaction){
				 $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #3e923a; font-size: 18px;'>".$response->response_reason_text."</p>");
			}
			
		}
		else{
				 $this->session->set_flashdata("message", "<p style='font-family: inherit;text-align: center; margin-top: 20px; color: #CC0000; font-size: 18px;'>".$response->response_reason_text."</p>");		
		}
		   redirect($this->config->item('base_url') . 'admin/dashboard/LawyerAllLeads/'.$id);
		
  }
  */
 
	  
	
		
}






/* End of file Index.php */
/* Location: ./application/controllers/admin/Index.php */