<?php 
	$CI =& get_Instance();
	$query = $CI->db->query("SELECT * FROM maintenance_mode");
	$rows = $query->result_array();
	$ips = array();
	foreach($rows as $row){
		if($row['id'] != 1){
			$ips[] .= $row['white_listed_ips'];
		}
		$status = $row['mode_status'];
	}
	if($CI->session->role != 1){
		if(!in_array($_SERVER['REMOTE_ADDR'], $ips)){
			if($status == 1){
				$config['site_under_maintenance'] = true;
			}else{
				$config['site_under_maintenance'] = false;
			}
		}else{
			$config['site_under_maintenance'] = false;
		}
	}else{
		$config['site_under_maintenance'] = false;
	}
?>