<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* Name:  Twilio
	*
	* Author: Ben Edmunds
	*		  ben.edmunds@gmail.com
	*         @benedmunds
	*
	* Location:
	*
	* Created:  03.29.2011
	*
	* Description:  Twilio configuration settings.
	*
	*
	*/
	/**
	 * Mode ("sandbox" or "prod")
	 **/
	$config['mode']   = 'prod';
	/**
	 * Account SID
	 **/
	$config['account_sid']   = 'AC4713480add2c1247d6f1c45623e7e361';
	/**
	 * Auth Token
	 **/
	$config['auth_token']    = '7593233b78004348d319d4c05e257477';
	/**
	 * API Version
	 **/
	$config['api_version']   = '2010-04-01';
	/**
	 * Twilio Phone Number
	 **/
	$config['number']        = '+14242303535';
/* End of file twilio.php */