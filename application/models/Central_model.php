<?php
class Central_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    
    public function save($tb, $data)
    {
        $this->db->set($data);
        $this->db->insert($tb);
        $insertId = $this->db->insert_id();

        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        } 
    }
    public function update($tb, $data, $where, $id, $and_w = NULL, $and = NULL)
    {
        $this->db->where($where, $id);
        if (isset($and) and !empty($and)) {
            $this->db->where($and_w, $and);
        } //isset($and) and !empty($and)
        $this->db->update($tb, $data);
        //$this->db->last_query();
        if ($this->db->affected_rows() > 0) {
            return $id;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    public function update_monthly_statement($data, $lawyer_id, $month, $year)
    {
        $this->db->where('lawyer_id',$lawyer_id);
        $query=$this->db->get('lawyers_to_leads');
        if($query->num_rows() > 0){
            return true ;
            // $this->db->where(array(
            //     'lawyer_id' => $lawyer_id,
            //     'month' => $month,
            //     'year' => $year,
            // ));
            // $this->db->update('monthly_statements', $data);
            // echo $this->db->last_query();
          
            // if ($this->db->affected_rows() > 0) {
            //     echo 'row affected update' ;
            //     return true ;
            // } 
            // else {
            //     return false ;
            // }
        }else{
            return false ;
        }

       
    }
	public function update_row($tb, $data, array $whr)
    {
        foreach ($whr as $where => $id) {
            $this->db->where($where, $id);
        } //$whr as $where => $id
        $this->db->update($tb, $data);
        $this->db->last_query();
        if ($this->db->affected_rows() > 0) {
            return $id;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    public function del($tb, array $arr)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        } //$arr as $where => $id
        $this->db->delete($tb);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    public function first($tb, $where, $id, $and_w = NULL, $and = NULL)
    {
        if (!empty($where)) {
            $this->db->where($where, $id);
        } //!empty($where)
        if (isset($and) and !empty($and)) {
            $this->db->where($and_w, $and);
        } //isset($and) and !empty($and)
        $query  = $this->db->get($tb);
        $result = $query->row();
        if ($query->num_rows() > 0) {
            return $result;
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    public function all_fetch($tb, $where = NULL, $id = NULL, $order = NULL)
    {
        if (!empty($where)) {
            $this->db->where($where, $id);
        } //!empty($where)
        if (!empty($order)) {
            $this->db->order_by($order, "ASC");
        } //!empty($order)
        $query  = $this->db->get($tb);
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    public function all_fetch_not_in($tb, $where = NULL, $id = NULL)
    {
        if (!empty($where)) {
            $this->db->where_not_in($where, $id);
        } //!empty($where)
        $query  = $this->db->get($tb);
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    public function select_all_array($tb, array $arr, array $like_array = NULL, $search_type = Null, $order_by = Null, $order_type = Null)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        } //$arr as $where => $id
		foreach ($like_array as $where => $string) {
            $this->db->like($where, $string , $search_type);
        } //$like_array as $where => $string
		if($order_by) $this->db->order_by($order_by, $order_type);
        $query  = $this->db->get($tb);
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    public function select_array($tb, array $arr)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        } //$arr as $where => $id
        $query  = $this->db->get($tb);
        $result = $query->row();
        if ($query->num_rows() > 0) {
            return $result;
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    public function update_array($tb, $data, $arr)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        } //$arr as $where => $id
        $this->db->update($tb, $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    public function count_rows($tb, array $arr,array $arr2 = NULL)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        } //$arr as $where => $id
		foreach ($arr2 as $where => $id) {
            $this->db->where($where.' !=', $id);
        } //$arr2 as $where => $id
        $query = $this->db->get($tb);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    public function user_info($tb, $tb2, array $arr)
    {
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        } //$arr as $where => $id
        $this->db->join($tb, '' . $tb . '.uid = ' . $tb2 . '.uid');
        $query  = $this->db->get($tb2);
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    public function select_max_field($tb, array $arr, $field)
    {
        $this->db->select_max($field);
        foreach ($arr as $where => $id) {
            $this->db->where($where, $id);
        } //$arr as $where => $id
        $result = $this->db->get($tb)->row();
        return $result;
    }
    public function unique_row($tb, $where, $id, $and_w = NULL, $and = NULL, $and_other = NULL, $and_us = NULL)
    {
        if (!empty($where)) {
            $this->db->where($where, $id);
        } //!empty($where)
        if (isset($and) and !empty($and)) {
            $this->db->where($and_w, $and);
        } //isset($and) and !empty($and)
        if (isset($and_us) and !empty($and_us)) {
            $this->db->where($and_other, $and_us);
        } //isset($and_us) and !empty($and_us)
        $query  = $this->db->get($tb);
        $result = $query->row();
        if ($query->num_rows() > 0) {
            return $result;
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
}