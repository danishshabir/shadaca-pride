<?php
class Cron_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function fetchAllPaymentRecords()
    {
        $this->db->select('*');
        $this->db->from('monthly_payment');
        $this->db->where('payment_status', 0);
        $query = $this->db->get();
        return $query->result();
    }
    public function fetchLawyerRecord($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function fetchAllLawyers()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('active', 1);
        $query = $this->db->get();
        return $query->result();
    }
    function fetchLawyersOnHolidays()
    {
        $current_date = date('Y-m-d');
        $query = "SELECT * FROM `users` WHERE `role` = 2 AND `holiday_to` != '0000-00-00' AND `holiday_to` < '".$current_date."' AND `active` = 1";
        $query = $this->db->query($query);
        return $query->result();
    }
    function fetchStateName($id)
    {
        $this->db->select('*');
        $this->db->from('jurisdiction_states');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchLawyerReminderTemplate()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', 7);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
    }
    public function fetchRow($id)
    {
        $this->db->select('*');
        $this->db->from('leads');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchLawyerRow($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchAllLawyersLeads()
    {
        $this->db->select('*');
        $this->db->from('lawyers_to_leads');
        $query = $this->db->get();
        return $query->result();
    }
    function fetchAllLeads()
    {
        $this->db->select('*');
        $this->db->from('leads');
        $this->db->where('lead_status', 3);
        $query = $this->db->get();
        return $query->result();
    }
    function InsertMonthlyBillAgainstMonth($data)
    {
        $this->db->set($data);
        $this->db->insert('monthly_payment');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function suspendLawyer($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function checkAssignedLeads($lawyer_id)
    {
        $this->db->select('*');
        $this->db->from('monthly_payment');
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->where('date_time', $current_date);
        $this->db->where('payment_status', '0');
        $query = $this->db->get();
        return $query->row();
    }
	function unopened_leads()
	{
		$this->db->select('leads.*, lawyers_to_leads.*, users.name as user_first_name, users.lname as user_last_name, users.email as user_email, users.is_sms as user_is_sms, users.phone as user_phone, jurisdiction_states.name as state_name');
		$this->db->from('leads');
		$this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id', 'INNER');
		$this->db->join('users', 'users.id = lawyers_to_leads.lawyer_id', 'left');
		$this->db->join('jurisdiction_states', 'jurisdiction_states.id = users.state', 'left');
		$this->db->where('leads.lead_open_datetime', NULL);
		$query = $this->db->get();
		return $query->result();
	}
}