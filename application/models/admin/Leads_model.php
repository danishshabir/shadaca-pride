<?php
class Leads_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function getLeadsPrice($market, $case_type)
    {
        $this->db->select('mf.fee');
        $this->db->from('market_fee mf');
        $this->db->join('market_n mn', 'mf.market = mn.id');
        $this->db->join('case_type ct', 'mf.case_type = ct.id');
        $this->db->where('mn.id', $market);
        $this->db->where('ct.id', $case_type);
        $result = $this->db->get();
        return $result->row();
    }
    
    /* Added By Hassan on date 16-Nov-2017 For Getting Notes with Names Who Posted Them */
    public function get_lawyers_id($id)
    {
      $this->db->select('lawyer_id');
      $this->db->from('lawyers_to_leads');
      $this->db->where('lead_id', $id);
      $result = $this->db->get();
      return $result->row();
  }

  public function get_lawyers_to_leads($lead_id, $lawyerstaff_id, $unique_code)
  {
      $this->db->select('*');
      $this->db->from('lawyers_to_leads');
      $this->db->join('lawyers_lawstaff', 'lawyers_to_leads.lawyer_id = lawyers_lawstaff.lawyers_id');
      $this->db->where('lead_id', $lead_id);
      $this->db->where('lawyers_lawstaff.lawyerstaff_id', $lawyerstaff_id);
      $this->db->where('unique_code', $unique_code);
      $result = $this->db->get();
      if($result->num_rows() > 0){
         return $result->num_rows();
     }else{
         return false;
     }
 }

 public function get_lawyer_notes($lead_id, $user_id = null)
 {
   /* Working query before the update to show admin notes on lawyer side */
	    //$this->db->select('ln.*, u1.name as first_name, u1.lname as last_name, u2.name as staff_first_name, u2.lname as staff_last_name , lf.law_firm as law_firm_name');
   /* ^^^ */
   $this->db->select('ln.*, u1.name as first_name, u1.lname as last_name, u2.name as staff_first_name, u2.lname as staff_last_name,u3.name as admin_first_name, u3.lname as admin_last_name , lf.law_firm as law_firm_name');
   $this->db->from('lead_notes ln');
   $this->db->join('users u1', 'u1.id = ln.user_id', 'LEFT');
   $this->db->join('users u2', 'u2.id = ln.lawyer_staff', 'LEFT');
   /* Remove this if any problem appears in notes */
   $this->db->join('users u3', 'u3.id = ln.admin_id', 'LEFT');
   /* ^^^ */
   $this->db->join('law_firms lf', 'lf.id = ln.lawfirm_id', 'LEFT');
   $this->db->where('ln.lead_id', $lead_id);
   $this->db->where('ln.role !=', '1');
   if($user_id)$this->db->where('ln.user_id ', $user_id);
   /* No need to uncomment */
		//if($lawyerstaff_id)$this->db->where('ln.lawyer_staff ', $lawyerstaff_id);
   $query = $this->db->get();
        //echo $this->db->last_query(); exit();
   if ($query->num_rows() > 0) {
    return $query->result();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    public function get_lead_contact_attempts($lead_id, $lawyer_id)
    {
      $this->db->select('lca.*, u1.name as lawyer_first_name, u1.lname as lawyer_last_name, u2.name as staff_first_name, u2.lname as staff_last_name, u3.name as admin_first_name, u3.lname as admin_last_name, lf.law_firm');
      $this->db->from('lead_contact_attempts lca');
      $this->db->join('users u1', 'u1.id = lca.lawyer_id', 'LEFT');
      $this->db->join('users u2', 'u2.id = lca.lawyer_staff', 'LEFT');
      $this->db->join('users u3', 'u3.id = lca.admin_id', 'LEFT');
      $this->db->join('law_firms lf', 'lf.id = lca.lawfirm_id', 'LEFT');
      $this->db->where('lca.lead_id', $lead_id);
      $this->db->where('lca.action', 'Contact Attempted');
      $this->db->where('lca.lawyer_id', $lawyer_id);
      $query = $this->db->get();
      if ($query->num_rows() > 0) {
        return $query->result();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    public function get_admin_notes($user_id, $lead_id, $role)
    {
      $this->db->select('ln.*, u1.name as first_name, u1.lname as last_name');
      $this->db->from('lead_notes ln');
      $this->db->join('users u1', 'u1.id = ln.user_id', 'LEFT');
      $this->db->where('ln.user_id', $user_id);
      $this->db->where('ln.lead_id', $lead_id);
      $this->db->where('ln.role', $role);
      $query = $this->db->get();
      if ($query->num_rows() > 0) {
        return $query->result();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }

    public function insertIntoDelete($data)
    {
      $this->db->insert('deleted_leads', $data);
  }
  function fetchLawyerLastLeadDate($lawyer_id)
  {
    $this->db->select('*');
    $this->db->from('lawyers_to_leads');
    $this->db->where('lawyer_id', $lawyer_id);
    $query = $this->db->get();
    $this->db->order_by('id', 'DESC');
    if ($query->num_rows() > 0) {
        return $query->row();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    function fetchLawyerAssignLeadTemplate()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', 6);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    function fetchLawyerResettPasswordTemplate()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', 2);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    function fetchLawyerAddTemplate()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    function fetchLawyerCompleteTemplate()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', 10);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    function fetchLawyerClosedTemplate()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', 9);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    function fetchLeadTemplateToAdmin()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', 5);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    function fetchLeadAssignLawyerTemplate()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', 6);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    public function updateTypeCase($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('case_type', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function updateLawCase($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('law_firms', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function updateTag($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tags_labels', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function fetchAllLeads()
    {
        $this->db->select('*');
        $this->db->from('leads');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchCaseType()
    {
        $this->db->select('*');
        $this->db->from('case_type');
        $this->db->order_by('type', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchTagType()
    {
        $this->db->select('*');
        $this->db->from('tags_labels');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchLeadDetail($id , $view = '')
    {
      if($view !== '' && $view == 'deletedLeads'){
         $this->db->select('id,case_id,lead_id_number,first_name,last_name,email,caller_id,caller_name, home_phone, work_phone, mobile,my_notes,street,city,state,zip,case_type,jurisdiction,market,source,intaker,case_description,contact_status,lead_status,created_at,updated_at,labels,price,lead_open_datetime,lead_complete_time,is_archive');
         $this->db->from('deleted_leads');			
     }else{			
         $this->db->select('*');
         $this->db->from('leads');
     }
     $this->db->where('id', $id);
     $query = $this->db->get();
     if ($query->num_rows() > 0) {
        return $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchStateName($id)
    {
        $this->db->select('*');
        $this->db->from('jurisdiction_states');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchMarketName($id)
    {
        $this->db->select('*');
        $this->db->from('market_n');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchCasetypeName($id)
    {
        $this->db->select('*');
        $this->db->from('case_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }

    /* Updated function below */
    /*
    function fetchLeadsLimit()
    {
        $this->db->select('leads.*, lawyers_to_leads.lawyer_id as lawyer_id, , lawyers_to_leads.billed as billed, , lawyers_to_leads.assigned_date as assigned_date, , lawyers_to_leads.lead_open_time as lead_open_time');
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ', 'left');
		$this->db->from('leads');
		$this->db->where('is_archive', 0);
        #$this->db->where('is_archive_lawyer', 0);
        $this->db->where_not_in('lead_status', array("3", "11", "12", "13", "14"));
        #$this->db->where('is_archive_admin', 0);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    */

    function fetchLeadsLimit($limit, $offset)
    {
        // Getting cookies
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status']:'';
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:'';
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:'';
        $advanceSearch = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:'';
        
        $this->db->select('leads.*, lawyers_to_leads.lawyer_id as lawyer_id, , lawyers_to_leads.billed as billed, , lawyers_to_leads.assigned_date as assigned_date, , lawyers_to_leads.lead_open_time as lead_open_time, CONCAT(leads.first_name, " " ,leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, lf.law_firm as lawfirm_name, ct.type as casetype, m.market_name, ls.name as status_name, (CASE WHEN leads.lead_status = 6 THEN 3 WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 2 THEN 5 WHEN leads.lead_status = 4 THEN 4 WHEN leads.lead_status = 5 THEN 6 WHEN leads.lead_status = 8 THEN 7 WHEN leads.lead_status = 15 THEN 2 WHEN leads.lead_status = 9 THEN 2 ELSE 99 END) AS default_order');
        
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ', 'left');
        $this->db->join("users as u", "lawyers_to_leads.lawyer_id = u.id", "left");
        $this->db->join("law_firms as lf", "u.law_firms = lf.id", "left");
        $this->db->join("case_type as ct", "leads.case_type = ct.id", "left");
        $this->db->join("market_n as m", "leads.market = m.id", "left");
        $this->db->join("lead_status as ls", "ls.id = leads.lead_status", "left");
        $this->db->join("leads as l", "l.id = lawyers_to_leads.lead_id", "left");        
        
        $this->db->from('leads');
        $this->db->where('leads.is_archive', 0);
        if($status != "" && $status != 0 && $status != 10){
            $this->db->where("leads.lead_status", $status);
        }
        if($case_type != ""){
            $this->db->where("leads.case_type", $case_type);
        }
        if($market != ""){
            $this->db->where("leads.market", $market);
        }
        if($advance_search_type != "" && $advanceSearch != ""){
            if($advance_search_type == "Lawyer"){
                $this->db->like("CONCAT(u.name, ' ', u.lname)", $advanceSearch);
            }else if($advance_search_type == "Law_Firm"){
                $this->db->like("lf.law_firm", $advanceSearch);
            }else if($advance_search_type == "Lead_Name"){
                $this->db->like("CONCAT(l.first_name, ' ', l.last_name)", $advanceSearch);
            }
        }
        if($status == ""){
            $this->db->where_not_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }else if($status == "10"){
            $this->db->where_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }
        $column = "default_order";
        $sort = "asc";
		if(isset($_GET["sort"])){
	      $column = $_GET["name"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        $this->db->limit($limit, $offset);
        $this->db->order_by($column, $sort);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    
    function fetchLeadsLayers($id)
    {
        $this->db->select('lawyers_to_leads.lawyer_id,lawyers_to_leads.created_at as assigned_date, users.name as assigned_to,users.lname as last_name,users.law_firms as law_firm_id,law_firms.law_firm as law_firm_name');
        $this->db->from('lawyers_to_leads');
        $this->db->join('users', 'lawyers_to_leads.lawyer_id = users.id ', 'left');
        $this->db->join('law_firms', 'users.law_firms = law_firms.id ', 'left');
        $this->db->where('lead_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    /* Updated Function Below(For getting second super admin email) */
    /*
    function fetchSuperAdminRecord()
    {
        $this->db->select('settings.super_admin_id,users.id,users.email as superAdminEmail,users.phone as superAdminPhone,users.is_sms as is_sms');
        $this->db->from('settings');
        $this->db->join('users', 'users.id = settings.super_admin_id ', 'left');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    */
    function fetchSuperAdminRecord()
    {        
        $this->db->select('s.super_admin_id,u1.id,u1.email as superAdminEmail,u2.email as secondAdminEmail, u1.phone as superAdminPhone,u1.is_sms as is_sms');
        $this->db->from('settings as s');
        $this->db->join('users as u1', 'u1.id = s.super_admin_id ', 'left');
        $this->db->join('users as u2', 's.second_super_admin_id = u2.id', 'left');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchLawsNames($id)
    {
        $this->db->select('*');
        $this->db->from('law_firms');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $result = $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchUserInformation($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $result = $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchArchieveLead($id)
    {
        $this->db->select('*');
        $this->db->from('leads');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $result = $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchJurisdictionFee($id)
    {
        $this->db->select('*');
        $this->db->from('Jurisdiction_fee');
        $this->db->where('Jurisdiction', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $result = $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchLeadsContactDateTime($id, $lawyer_id, $action)
    {
        $this->db->select('*');
        $this->db->from('lead_contact_attempts');
        $this->db->where('lead_id', $id);
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->where('action', $action);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchAllLeadsOfLawyers($id)
    {
        $this->db->select('leads.id,leads.first_name,leads.last_name,leads.caller_name,leads.caller_id,leads.email,leads.case_type,leads.labels,leads.lead_status,lawyers_to_leads.created_at');
        $this->db->from('leads');
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ');
        $this->db->where('lawyers_to_leads.lawyer_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function fetchLeadsOfLawyersLimit($id, $limit, $status, $type = null)
    {
        $this->db->select('leads.id,leads.first_name,leads.last_name,leads.caller_name,leads.caller_id,leads.email,leads.mobile,leads.case_type,leads.labels,leads.lead_status,leads.case_description,leads.lead_id_number,lawyers_to_leads.created_at, users.name, lawyers_to_leads.id as lawyer_to_leads_id, lawyers_to_leads.unique_code as lawyer_to_lead_unique_code, leads.date_closed');
        $this->db->from('leads');
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ');
        $this->db->join('users', 'lawyers_to_leads.lawyer_id = users.id ');
        $this->db->where("is_archive_lawyer !=", 1);
        if(!$type){
            $this->db->where('lawyers_to_leads.lawyer_id', $id);
         }else{
             $this->db->join('lawyers_lawstaff', 'lawyers_to_leads.lawyer_id = lawyers_lawstaff.lawyers_id ');
             $this->db->where('lawyers_lawstaff.lawyerstaff_id', $id);
         }
         if ($status) {
            if($status == 10){
                $this->db->where_in('leads.lead_status', array("3", "12"));
            }else{
                $this->db->where('leads.lead_status', $status);
            }
        } //$status != ''
        else if($status == '0'){
            $this->db->where_not_in('leads.lead_status', array("11", "13", "14"));
        }
        if($status != '0'){
            if(!$status) {
                $this->db->where_not_in('leads.lead_status', array("3", "9", "11", "12", "13", "14", "15"));
            }
        }
        $this->db->order_by('created_at', 'desc');
        $query = $this->db->get();
		/* echo $this->db->last_query();
		exit(); */
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
/**
 * function fetchLeadsOfLawyersLimit($id, $limit, $status)
 *     {
 *         $this->db->select('leads.id,leads.first_name,leads.last_name,leads.caller_name,leads.caller_id,leads.email,leads.mobile,leads.case_type,leads.labels,leads.lead_status,leads.case_description,leads.lead_id_number,lawyers_to_leads.created_at, users.name, lawyers_to_leads.id as lawyer_to_leads_id, lawyers_to_leads.unique_code as lawyer_to_lead_unique_code');
 *         $this->db->from('leads');
 *         $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ');
 * 		$this->db->join('users', 'lawyers_to_leads.lawyer_id = users.id ');
 *         $this->db->where('lawyers_to_leads.lawyer_id', $id);
 *         if ($status != '') {
 *             $this->db->where('leads.lead_status', $status);
 *         } //$status != ''
 *         $this->db->order_by('created_at', 'desc');
 *         $query = $this->db->get();
 *         if ($query->num_rows() > 0) {
 *             return $query->result();
 *         } //$query->num_rows() > 0
 *         else {
 *             return false;
 *         }
 *     }
 */

function LawfirmsLeads($id, $limit, $status)
{
    $this->db->select('leads.id,leads.first_name,leads.last_name,leads.caller_name,leads.caller_id,leads.email,leads.mobile,leads.case_type,leads.labels,leads.lead_status,leads.case_description,leads.lead_id_number,lawyers_to_leads.created_at, users.name,lawyers_to_leads.id as lawyer_to_leads_id, lawyers_to_leads.unique_code as lawyer_to_lead_unique_code, leads.date_closed');
    $this->db->from('users');
    $this->db->join('law_firms', 'users.law_firms = law_firms.id');
    $this->db->join('lawyers_to_leads', 'users.id = lawyers_to_leads.lawyer_id');
    $this->db->join('leads', 'leads.id = lawyers_to_leads.lead_id'); 
    $this->db->where('users.law_firms', $id);
    $this->db->where("is_archive_lawyer !=", 1);
    if($status != '0'){
        if(!$status) {
            $this->db->where_not_in('lead_status', array("3", "9", "11", "12", "13", "14", "15"));
        }
    }
    if ($status) {
        if($status == 10){
            $this->db->where_in('leads.lead_status', array("3", "12"));
        }else{
             $this->db->where('leads.lead_status', $status);
         }
    } //$status != ''
    else if($status == '0'){
        $this->db->where_not_in('leads.lead_status', array("11", "13", "14"));
    }
    $this->db->order_by('leads.created_at', 'desc');
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
        return $query->result();
    } //$query->num_rows() > 0
    else {
        return false;
    }
}
	/**
 * function LawfirmsLeads($id, $limit, $status)
 *     {
 *         $this->db->select('leads.id,leads.first_name,leads.last_name,leads.caller_name,leads.caller_id,leads.email,leads.mobile,leads.case_type,leads.labels,leads.lead_status,leads.case_description,leads.lead_id_number,lawyers_to_leads.created_at, users.name,lawyers_to_leads.id as lawyer_to_leads_id, lawyers_to_leads.unique_code as lawyer_to_lead_unique_code');
 *         $this->db->from('users');
 * 		$this->db->join('law_firms', 'users.law_firms = law_firms.id');
 *         $this->db->join('lawyers_to_leads', 'users.id = lawyers_to_leads.lawyer_id');
 * 		$this->db->join('leads', 'leads.id = lawyers_to_leads.lead_id'); 
 * 		$this->db->where('users.law_firms', $id);
 * 		if ($status != '') {
 * 			$this->db->where('leads.lead_status', $status);
 * 		} //$status != ''
 * 		$this->db->order_by('leads.created_at', 'desc');
 *         $query = $this->db->get();
 *         if ($query->num_rows() > 0) {
 *             return $query->result();
 *         } //$query->num_rows() > 0
 *         else {
 *             return false;
 *         }
 *     }
 */
    public function fetchAll($limit = NULL, $start = NULL)
    {
        $this->db->select('*');
        $this->db->from('users');
        if (isset($limit) and !empty($limit)) {
            $this->db->limit($limit, $start);
        } //isset($limit) and !empty($limit)
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function save($data)
    {
        $this->db->set($data);
        $this->db->insert('leads');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        }
    }
    function saveLeadFee($data)
    {
        $this->db->set($data);
        $this->db->insert('laywer_lead_fee');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        }
    }
    function insertArchieveLead($data)
    {
        $this->db->set($data);
        $this->db->insert('archieve_lead');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        }
    }
    function lawyerToLead($data)
    {
        $this->db->set($data);
        $this->db->insert('lawyers_to_leads');
        $insertId = $this->db->insert_id();
        //echo $this->db->last_query();exit();
        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        }
    }
    function deleteLawyerToLead($id)
    {
        $this->db->where('lead_id', $id);
        $this->db->delete('lawyers_to_leads');
        //echo $this->db->affected_rows();
        //echo $this->db->last_query();exit();
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function deleteLeadFee($lawyer_id, $lead_id)
    {
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->where('lead_id', $lead_id);
        $this->db->delete('laywer_lead_fee');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function update($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('leads', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function updateLeadFee($data, $id)
    {
        $this->db->where('lead_id', $id);
        $this->db->update('laywer_lead_fee', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function updateEmailTemplate($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('email_templates', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    public function fetchUnassignEmailTemplate()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', 8);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchLawyerRow($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchLawyerLAwFirmName($id)
    {
        $this->db->select('*');
        $this->db->from('law_firms');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchRow($id, $view = '')
    {
        $this->db->select('*');
        if($view == ''){
         $this->db->from('leads');
     }else if($view !== ''){
         $this->db->from('deleted_leads');
     }
     $this->db->where('id', $id);
     $query = $this->db->get();
     if ($query->num_rows() == 1) {
        return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchArchieveRowToLeadId($id)
    {
        $this->db->select('*');
        $this->db->from('archieve_lead');
        $this->db->where('lead_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchLeadRow($id)
    {
        $this->db->select('*');
        $this->db->from('leads');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchNotes($lead_id, $lawyer_id)
    {
        $this->db->select('*');
        $this->db->from('lawyers_to_leads');
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->where('lead_id', $lead_id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchAdminNotesOnly($id, $Admin_id)
    {
        $this->db->select('*');
        $this->db->from('admin_to_lead');
        $this->db->where('lead_id', $id);
        $this->db->where('Admin_id', $Admin_id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchAdminNotes($id, $Admin_id)
    {
        $this->db->select('a_t_l.*,l_t_l.notes as notes');
        $this->db->from('admin_to_lead a_t_l');
        $this->db->join('lawyers_to_leads l_t_l', 'a_t_l.lead_id = l_t_l.lead_id', 'LEFT OUTER');
        $this->db->where('a_t_l.lead_id', $id);
        $this->db->where('a_t_l.Admin_id', $Admin_id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchContactAttempts($lead_id, $lawyer_id)
    {
        $this->db->select('*');
        $this->db->from('lead_contact_attempts');
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->where('lead_id', $lead_id);
        $this->db->where('action', 'Contact Attempted');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function insertData($data)
    {
        $this->db->set($data);
        $this->db->insert('credit_card_transactions');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        }
    }
    public function fetchContactReached($lead_id, $lawyer_id)
    {
        $this->db->select('*');
        $this->db->from('lead_contact_attempts');
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->where('lead_id', $lead_id);
        $this->db->where('action', 'Contact Reached');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchLeadReached($lead_id, $lawyer_id)
    {
        $this->db->select('*');
        $this->db->from('lead_contact_attempts');
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->where('lead_id', $lead_id);
        $this->db->where('action', 'Lead Reached');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchContactScheduled($lead_id, $lawyer_id)
    {
        $this->db->select('*');
        $this->db->from('lead_contact_attempts');
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->where('lead_id', $lead_id);
        $this->db->where('action', 'Consultation Scheduled');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchLeadRetained($lead_id, $lawyer_id)
    {
        $where = "lead_id = '" . $lead_id . "' and action = 'Lead Not Retained'";
        $this->db->select('*');
        $this->db->from('lead_contact_attempts');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function checkLeadReached($lead_id, $lawyer_id)
    {
        $this->db->select('*');
        $this->db->from('lead_contact_attempts');
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->where('lead_id', $lead_id);
        $this->db->where('action', 'Lead Reached');
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function checkLeadRetained($lead_id, $lawyer_id)
    {
        $where = "lawyer_id='" . $lawyer_id . "' and lead_id = '" . $lead_id . "' and (action = 'Lead Not Retained' || action = 'Lead Retained')";
        $this->db->select('*');
        $this->db->from('lead_contact_attempts');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function deleteRow($id, $view = '')
    {
        $this->db->where('id', $id);
        if($view !== '' && $view == 'deletedLeads'){
         $this->db->delete('deleted_leads');
     }else{
         $this->db->delete('leads');
     }
     if ($this->db->affected_rows() > 0) {
        return TRUE;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function insertAttemptsLawyers($data)
    {
        $this->db->set($data);
        $this->db->insert('lead_contact_attempts');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        }
    }
    function InsertAdminNotes($data)
    {
        $this->db->set($data);
        $this->db->insert('admin_to_lead');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        }
    }
    function updateAttemptsLawyers($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('lead_contact_attempts', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function updateContactReached($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('leads', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function updateLawyerNotesOnLeads($data_notes, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('lawyers_to_leads', $data_notes);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function lawyerAddNotes($data, $lead_id, $lawyer_id)
    {
        $this->db->where('lead_id', $lead_id);
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->update('lawyers_to_leads', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function AddAdminNotes($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('admin_to_lead', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    public function deleteAttemptsLawyers($lawyer_id, $lead_id, $action)
    {
        $this->db->where('lead_id', $lead_id);
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->where('action', $action);
        $this->db->delete('lead_contact_attempts');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    public function fetchLawyer($lead_id)
    {
        $this->db->select('*');
        $this->db->from('lawyers_to_leads');
        $this->db->where('lead_id', $lead_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    public function fetchLawyerNotesOnLeads($lead_id)
    {
        $this->db->select('*');
        $this->db->from('lawyers_to_leads');
        $this->db->where('lead_id', $lead_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    public function fetchUnreachedLeads()
    {
        $query = $this->db->query("SELECT * FROM leads WHERE lead_status = 1 AND id NOT IN (SELECT lead_id from lawyers_to_leads)");
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    public function fetchUnreachedLeadsLawyer($user_id)
    {
        $query = $this->db->query("SELECT leads.*, lawyers_to_leads.created_at as assigned_at, lawyers_to_leads.unique_code FROM leads, lawyers_to_leads WHERE leads.id = lawyers_to_leads.lead_id AND leads.lead_status = 2 AND lawyers_to_leads.lawyer_id=" . $user_id);
        if ($query->num_rows() > 0) {
            return $query->result();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function insertLeadFee($data)
    {
        $this->db->set($data);
        $this->db->insert('laywer_lead_fee');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        }
    }
    function template($id)
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return array();
        }
    }
    function updateStatus($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('leads', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    function get_lawyer($id)
    {
        $this->db->select('*');
        $this->db->from('lawyers_to_leads');
        $this->db->where('lead_id', $id);
        $query = $this->db->get();
		//echo $this->db->last_query(); exit;  
        if ($query->num_rows() > 0) {
            return $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    
    public function to_lead()
    {
		$where = "";
		$market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:"";
		$status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status']:'';
		$case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:'';
		$advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:$advance_search_type;
		$text = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']: '';
        
        $column = "default_order";
        $sort = "asc";
		if(isset($_GET["sort"])){
		  if($_GET["name"] != "case_fee_received" && $_GET["name"] != "referral_fee_received"){
    	      $column = $_GET["name"];
              if($column == "date_received"){
                $column = "created_at";
              }else if($column == "date_assigned"){
                $column = "assigned_date";
              }else if($column == "date_opened"){
                $column = "lead_open_datetime";
              }else if($column == "date_completed"){
                $column = "date_closed";
              }
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }else if($column == "case_fee_received"){
            $column = "default_order";
          }else if($column == "referral_fee_received"){
            $column = "default_order";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        
		if($market != "" || $status != "" || $case_type != ""){
			$where_sql = "";
			if($market != ""){
				$where_sql .= "AND leads.market = '$market'";
			}
			if($status != ""){
				$where_sql .= "AND leads.lead_status = '$status'";
			}else{
				$where_sql .= "AND (leads.lead_status = 1 || leads.lead_status = 2)";
			}
			if($case_type != ""){
				$where_sql .= "AND leads.case_type = '$case_type'";
			}
			//$where .= trim($where_sql, "AND ") . " ";
			$where = $where_sql;
		}
		
        if ($text != '' && $advance_search_type == 'Lawyer') {
            #$sql = "SELECT l_t_l.lawyer_id, l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE (u_a.name LIKE '%" . $text . "%' OR u_a.lname LIKE '%" . $text . "%' OR CONCAT(u_a.name, ' ', u_a.lname) LIKE '%" . $text . "%') AND (u_a.id = l_t_l.lawyer_id) AND (l.id = l_t_l.lead_id) AND l.is_archive = 0 $where GROUP BY l.id";
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as referral_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name`, (CASE WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 6 THEN 2 WHEN leads.lead_status = 4 THEN 3 WHEN leads.lead_status = 2 THEN 4 WHEN leads.lead_status = 5 THEN 5 WHEN leads.lead_status = 8 THEN 6 WHEN leads.lead_status = 7 THEN 7 ELSE 99 END) AS default_order FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND (u.name LIKE "%' . $text . '%" OR u.lname LIKE "%' . $text . '%" OR CONCAT(u.name, " ", u.lname) LIKE "%' . $text . '%") '.$where.' group by leads.id ORDER BY '.$column.' '.$sort.' LIMIT 1';
        } else
        if ($text != '' && $advance_search_type == 'Law_Firm') {
            #$sql = "SELECT l_t_l.lawyer_id, l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE ( law_f.law_firm LIKE '%" . $text . "%'  AND  law_f.id = u_a.law_firms AND u_a.id = l_t_l.lawyer_id and l.id = l_t_l.lead_id ) AND l.is_archive = 0 $where";
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as referral_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name`, (CASE WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 6 THEN 2 WHEN leads.lead_status = 4 THEN 3 WHEN leads.lead_status = 2 THEN 4 WHEN leads.lead_status = 5 THEN 5 WHEN leads.lead_status = 8 THEN 6 WHEN leads.lead_status = 7 THEN 7 ELSE 99 END) AS default_order FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND lf.law_firm LIKE "%' . $text . '%" '.$where.' group by leads.id ORDER BY '.$column.' '.$sort.' LIMIT 1';
        } else
        if ($text != '' && $advance_search_type == 'Lead_Name') {
            #$sql = "SELECT l_t_l.lawyer_id, l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE (l.first_name LIKE '%" . $text . "%' OR l.last_name LIKE '%" . $text . "%' OR CONCAT(l.first_name, ' ', l.last_name) LIKE '%" . $text . "%') AND l.is_archive = 0 $where GROUP BY l.id";
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as referral_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name`, (CASE WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 6 THEN 2 WHEN leads.lead_status = 4 THEN 3 WHEN leads.lead_status = 2 THEN 4 WHEN leads.lead_status = 5 THEN 5 WHEN leads.lead_status = 8 THEN 6 WHEN leads.lead_status = 7 THEN 7 ELSE 99 END) AS default_order FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND (leads.first_name LIKE "%' . $text . '%" OR leads.last_name LIKE "%' . $text . '%" OR CONCAT(leads.first_name, " ", leads.last_name) LIKE "%' . $text . '%") '.$where.' group by leads.id ORDER BY '.$column.' '.$sort.' LIMIT 1';
        } else {
            #$sql = "SELECT l_t_l.lawyer_id, l.* FROM leads as l left join lawyers_to_leads as l_t_l on l.id = l_t_l.lead_id WHERE l.is_archive = 0 $where ORDER BY l.first_name DESC";
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as referral_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name`, (CASE WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 6 THEN 2 WHEN leads.lead_status = 4 THEN 3 WHEN leads.lead_status = 2 THEN 4 WHEN leads.lead_status = 5 THEN 5 WHEN leads.lead_status = 8 THEN 6 WHEN leads.lead_status = 7 THEN 7 ELSE 99 END) AS default_order FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 '.$where.' ORDER BY '.$column.' '.$sort.'';
        }
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    /**
     * Leads_model::fetchNotRetainedDeclinedLeads()
     * 
     * @param mixed $lawyer_id
     * @return
     */
    public function fetchNotRetainedDeclinedLeads($lawyer_id) {
        
        return $this->db->query("SELECT l.id, IF(llh.status = 1, 'Not Retained', IF(llh.status = 2, 'Declined', 'N/A')) AS status, l.lead_id_number AS case_id, CONCAT(u.name, ' ', u.lname) AS lawyer_name, CONCAT(l.first_name, ' ',l.last_name) AS client_name, l.caller_name, ct.type AS case_type, llh.created_at AS action_date FROM lawyer_lead_history AS llh LEFT JOIN leads AS l ON llh.lead_id = l.id LEFT JOIN users AS u ON llh.lawyer_id = u.id LEFT JOIN case_type AS ct ON l.case_type = ct.id WHERE llh.lawyer_id = '{$lawyer_id}'")->result();
        
    }
    
    /**
     * Leads_model::fetchNotRetainedDeclinedLeadsMultiple()
     * 
     * @param mixed $lawyer_id
     * @return
     */
    public function fetchNotRetainedDeclinedLeadsMultiple($lawyer_id) {
        
        return $this->db->query("SELECT l.id, IF(llh.status = 1, 'Not Retained', IF(llh.status = 2, 'Declined', 'N/A')) AS status, l.lead_id_number AS case_id, CONCAT(u.name, ' ', u.lname) AS lawyer_name, CONCAT(l.first_name, ' ',l.last_name) AS client_name, l.caller_name, ct.type AS case_type, llh.created_at AS action_date FROM lawyer_lead_history AS llh LEFT JOIN leads AS l ON llh.lead_id = l.id LEFT JOIN users AS u ON llh.lawyer_id = u.id LEFT JOIN case_type AS ct ON l.case_type = ct.id WHERE llh.lawyer_id IN('".implode("', '", $lawyer_id)."')")->result();
        
    }
    
    /**
     * Leads_model::fetchLeadDetailRetained()
     * 
     * @param mixed $id
     * @return
     */
    function fetchLeadDetailRetained($id)
    {
       $this->db->select('l.*,llh.assigned_date_time');
       $this->db->from('leads l');
	   $this->db->join('lawyer_lead_history llh', 'llh.lead_id = l.id ', 'left');
	   $this->db->where('llh.lead_id', $id);
	   $query = $this->db->get();
	   if ($query->num_rows() > 0) {
		return $query->row();
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
}