<?php
class Login_model extends CI_Model
{
    function _construct()
    {
        parent::_construct();
    }
    function login($email, $password)
    {
        $this->db->select('');
        $this->db->from('users');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->where('active', '1');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result();
        } //$query->num_rows() == 1
        else {
            return false;
        }
    }
    function Block_User($email)
    {
        $this->db->where('email', $email AND 'active', '1');
        $this->db->update('active', '0');
        if ($this->db->affected_rows() > 0) {
            return true;
        } //$this->db->affected_rows() > 0
        else {
            return false;
        }
    }
    public function checkEmailTrack($user_id, $id)
    {
        $this->db->select('*');
        $this->db->from('email_tracking_records');
        $this->db->where('user_id', $user_id);
        $this->db->where('email_tracking_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
    function saveEmailTrack($data)
    {
        $this->db->set($data);
        $this->db->insert('email_tracking_records');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        }
    }
    function saveEmailTrackOpenRecords($data)
    {
        $this->db->set($data);
        $this->db->insert('email_open_count');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } //$insertId > 0
        else {
            return false;
        }
    }
    function updateEmailOpenCount($email_id)
    {
        $this->db->select('*');
        $this->db->from('email_tracking');
        $this->db->where('id', $email_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row                = $query->row();
            $count              = $row->Open_Count;
            $data['Open_Count'] = $count + 1;
            $this->db->where('id', $email_id);
            $this->db->update('email_tracking', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            } //$this->db->affected_rows() > 0
            else {
                return false;
            }
        } //$query->num_rows() > 0
        else {
            return false;
        }
    }
}

