<?php
class Users_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function delete_lawstaff($id)
    {
        $this->db->where('lawyerstaff_id', $id);
        $this->db->delete('lawyers_lawstaff');
    }

    function update_lawyers_lawstaff($data, $lawyer_id)
    {
        $this->db->where('lawyers_id', $lawyer_id);
        $this->db->update('lawyers_lawstaff', $data);
        return true;
    }

    function getLawfirmLawyers($lawfirm_id, $lawyer_id = '')
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('law_firms', $lawfirm_id);
        $this->db->where('role !=', 7);
        if ($lawyer_id != '') {
            $this->db->where('id !=', $lawyer_id);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    function add_lawyerstaff($data)
    {
        $this->db->insert('lawyers_lawstaff', $data);
    }

    function delete_from_lawyers_lawstaff($id)
    {
        $this->db->where('lawyerstaff_id', $id);
        $this->db->delete('lawyers_lawstaff');
    }

    function getLawyersLawstaff($lawyer_id, $type = null)
    {
        if ($lawyer_id) {
            if ($type != null) {
                $this->db->select('ll.*, u.email,u.send_email as send_email_user, u.is_sms, u.phone, u.name, u.lname, u.lawyer');
            } else {
                $this->db->select('ll.*, u.email,u.send_email as send_email_user, u.is_sms,u.name, u.lname, u.phone');
            }
            $this->db->from('lawyers_lawstaff ll');
            $this->db->join('users u', 'll.lawyerstaff_id = u.id', 'LEFT');
            if ($type != null) {
                $this->db->where_in('lawyers_id', $lawyer_id);
                $this->db->group_by('u.email');
            } else {
                $this->db->where('lawyers_id', $lawyer_id);
            }
            $this->db->order_by('ll.id', 'ASC');
            $result = $this->db->get();
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getLawyerStaffLawyer($lawyer_staff_id, $table = null)
    {
        $this->db->select('*');
        if ($table == null) {
            $this->db->from('users');
            $this->db->where('id', $lawyer_staff_id);
            $this->db->where('role', 7);
        } else {
            $this->db->from('lawyers_lawstaff');
            $this->db->where('lawyerstaff_id', $lawyer_staff_id);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            if ($table == null) {
                return $result->row();
            } else {
                return $result->result_array();
            }
        } else {
            return false;
        }
    }

    function checkEmail($email)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }
    }
    function fetchPasswordUpdateTemplate()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', 3);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
    }
    function fetchLawyerRecordOnEmail($email)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return array();
        }
    }
    function fetchAllUsers($role = '', $view = null)
    {
        if ($view != null) {
            $this->db->select('u.*, lf.law_firm');
        } else {
            $this->db->select('u.*');
        }
        $this->db->from('users u');
        if ($view != null) {
            $this->db->join('law_firms lf', 'u.law_firms = lf.id', 'LEFT');
        }
        if ($role != '') {
            $this->db->where('u.role', $role);
            $this->db->order_by('u.name', 'ASC');
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchAllUserss($role = '')
    {
        $this->db->select('*');
        $this->db->from('users');
        if ($role != '') {
            $this->db->where('role', $role);
        }
        $this->db->order_by('name', 'ASC');
        $this->db->where('active', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchLawyerMonthlyBillDetails($id, array $data = null)
    {
        $this->db->select('*');
        $this->db->from('monthly_payment');
        $this->db->where('lawyer_id', $id);
        if ($data['date_from'] && $data['date_to']) {
            $this->db->where('Date(created_at) >= date("' . $data['date_from'] . '")');
            $this->db->where('Date(created_at) <= date("' . $data['date_to'] . '")');
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchAllStates()
    {
        $this->db->select('*');
        $this->db->from('jurisdiction');
        $query = $this->db->get();
        return $query->result();
    }
    function fetchAllJurisdictions()
    {
        $this->db->select('*');
        $this->db->from('jurisdiction_n');
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    function fetchAllAmericanStates()
    {
        $this->db->select('*');
        $this->db->from('jurisdiction_states');
        $query = $this->db->get();
        return $query->result();
    }
    function fetchEmailTemplateData($id)
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchAllLawyers()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role', '2');
        $this->db->where('active', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchLawyerLeads($id)
    {
        $this->db->select('leads.*, laywer_lead_fee.*');
        $this->db->from('laywer_lead_fee');
        $this->db->join('leads', 'leads.id = laywer_lead_fee.lead_id ', 'right');
        $this->db->where('laywer_lead_fee.lawyer_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchMonthlyTotal($id, $month_year)
    {
        $this->db->select('*');
        $this->db->select('SUM(monthly_bill) as total_m_bill');
        $this->db->from('monthly_payment');
        $this->db->where('lawyer_id', $id);
        $this->db->where('date_time', $month_year);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchDates($id)
    {
        $this->db->select('*');
        $this->db->from('monthly_payment');
        $this->db->where('lawyer_id', $id);
        $this->db->group_by('date_time');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchMonthlyTotalAjax($id, $new_date)
    {
        $this->db->select('SUM(monthly_bill) as total_m_bill');
        $this->db->from('monthly_payment');
        $this->db->where('lawyer_id', $id);
        $this->db->where('date_time', $new_date);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchLawyerMonthlyTotal($id, $new_date_from, $new_date_to)
    {
        $this->db->select('SUM(monthly_bill) as total_m_bill');
        $this->db->from('monthly_payment');
        $this->db->where('lawyer_id', $id);
        if ($new_date_from != '' && $new_date_to != '') {
            $this->db->where("DATE_FORMAT(STR_TO_DATE(date_time, '%c-%Y'), '%m-%Y') BETWEEN '" .
                $new_date_from . "' AND '" . $new_date_to . "'");
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchLeadsPaymentTotal($id)
    {
        $this->db->select('SUM(fee) as total_lead_bill');
        $this->db->from('laywer_lead_fee');
        $this->db->where('lawyer_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchMonthlyPaidTotal($id)
    {
        $this->db->select('SUM(monthly_bill) as total_m_bill');
        $this->db->from('monthly_payment');
        $this->db->where('lawyer_id', $id);
        $this->db->where('payment_status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchLeadsPaymentPaidTotal($id)
    {
        $this->db->select('SUM(fee) as total_lead_bill');
        $this->db->from('laywer_lead_fee');
        $this->db->where('lawyer_id', $id);
        $this->db->where('payment_status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchLawyerDetails($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchLawyerAllLeads($id, array $data = null)
    {
        $this->db->select('leads.* ,users.name, users.lname ,users.id,lawyers_to_leads.*');
        $this->db->from('lawyers_to_leads');
        $this->db->join('leads', 'leads.id = lawyers_to_leads.lead_id');
        $this->db->join('users', 'lawyers_to_leads.lawyer_id = users.id');
        $this->db->where('lawyers_to_leads.lawyer_id', $id);
        $this->db->where('lawyers_to_leads.billed', "yes");
        if ($data['date_from'] && $data['date_to']) {
            $this->db->where('Date(lawyers_to_leads.assigned_date) >= date("' . $data['date_from'] .
                '")');
            $this->db->where('Date(lawyers_to_leads.assigned_date) <= date("' . $data['date_to'] .
                '")');
        } else {
            $this->db->where('Date(lawyers_to_leads.assigned_date) BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()');
        }
        $this->db->order_by('lawyers_to_leads.assigned_date', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchLeadPaymentOfLawyer($id, $date_from, $date_to)
    {
        $this->db->select('leads.*, Jurisdiction_fee.*');
        $this->db->from('Jurisdiction_fee');
        $this->db->join('leads', 'leads.jurisdiction = Jurisdiction_fee.Jurisdiction ',
            'left outer');
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ',
            'left outer');
        $this->db->where('lawyers_to_leads.lawyer_id', $id);
        if ($date_from != '' && $date_to != '') {
            $this->db->where('lawyers_to_leads.assigned_date BETWEEN "' . date('Y-m-d',
                strtotime($date_from)) . '" and "' . date('Y-m-d', strtotime($date_to)) . '"');
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchPayment($id)
    {
        $this->db->select('leads.*, market_fee.*, lawyers_to_leads.*, laywer_lead_fee.payment_status as pay_status');
        $this->db->from('market_fee');
        $this->db->join('leads',
            'leads.market = market_fee.market AND leads.case_type = market_fee.case_type',
            'left outer');
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ',
            'left outer');
        $this->db->join('laywer_lead_fee', 'leads.id = laywer_lead_fee.lead_id ',
            'left outer');
        $this->db->where('lawyers_to_leads.lawyer_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchAllOperatorsUsers($role = '')
    {
        $this->db->select('*');
        $this->db->from('users');
        if ($role != '') {
            $this->db->where('role', $role);
        }
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchAllCaseTpe()
    {
        $this->db->select('*');
        $this->db->from('case_type');
        $this->db->order_by('type', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchAllLawFirms()
    {
        $this->db->select('*');
        $this->db->from('law_firms');
        $this->db->order_by('law_firm', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchAllJurisdiction()
    {
        $this->db->select('*');
        $this->db->from('jurisdiction');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function lawType()
    {
        $this->db->select('*');
        $this->db->from('law_firms');
        $query = $this->db->get();
        return $query->result();
    }
    function lawTypee()
    {
        $this->db->select('*');
        $this->db->from('law_firms');
        $query = $this->db->get();
        return $query->result();
    }
    function fetchLawyersRelatedToCase($case_type, $city)
    {
        $where = "users.role= 2 AND users.city = '" . $city .
        "' AND users.active = '1' AND INSTR(users.case_type, '" . $case_type . "')";
        $this->db->select('users.id,users.name,users.email, users.phone, users.address, users.city, users.jurisdiction, users.state, users.zipcode, users.case_type, max(lawyers_to_leads.created_at) as assign_date');
        $this->db->from('users');
        $this->db->join('lawyers_to_leads', 'users.id = lawyers_to_leads.lawyer_id ',
            'left');
        $this->db->where($where);
        $this->db->order_by("max(lawyers_to_leads.created_at)", "asc");
        $this->db->group_by(array(
            "users.id",
            "users.name",
            "users.email",
            "users.phone",
            "users.address",
            "users.city",
            "users.jurisdiction",
            "users.state",
            "users.zipcode",
            "users.case_type"));
        $this->db->limit(3);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchRoles()
    {
        $this->db->select('*');
        $this->db->from('role');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function fetchAll($limit = null, $start = null)
    {
        $this->db->select('*');
        $this->db->from('users');
        if (isset($limit) and !empty($limit)) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function fetchRowLawyer($id)
    {
        $this->db->select('u.*, lf.law_firm as law_firm_name');
        $this->db->from('users u');
        $this->db->join('law_firms lf', 'u.law_firms = lf.id', 'LEFT');
        $this->db->where('u.id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchRowLawyerByEmail($email)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchLeadfee($id, $user_id)
    {
        $this->db->select('*');
        $this->db->from('laywer_lead_fee');
        $this->db->where('lead_id', $id);
        $this->db->where('lawyer_id', $user_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function Leadfee($id)
    {
        $this->db->select('*');
        $this->db->from('laywer_lead_fee');
        $this->db->where('lead_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchLawfee($id)
    {
        $this->db->select('*');
        $this->db->from('law_firms');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchMarketCaseTypePrice($id, $market_id)
    {
        $this->db->select('*');
        $this->db->from('market_fee');
        $this->db->where('market', $market_id);
        $this->db->where('case_type', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchMarketRecord($id)
    {
        $this->db->select('*');
        $this->db->from('market_n');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchIntakerRecord($id)
    {
        $this->db->select('name');
        $this->db->from('intaker');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchSourceRecord($id)
    {
        $this->db->select('name');
        $this->db->from('source');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchInitialContact($id)
    {
        $this->db->select('datetime');
        $this->db->from('lead_contact_attempts');
        $this->db->where('lead_id', $id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchStateRecord($id)
    {
        $this->db->select('state_short_name');
        $this->db->from('jurisdiction_states');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchStateId($id)
    {
        $this->db->select('*');
        $this->db->from('market_n');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchImage($id)
    {
        $this->db->select('image');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function fetchAssignDate($id)
    {
        $this->db->select('assigned_date');
        $this->db->from('lawyers_to_leads');
        $this->db->where('lead_id', $id);
        $this->db->order_by('created_at', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function fetchLeadRecievedDate($id)
    {
        $this->db->select('contact_reached_at');
        $this->db->from('lead_contact_attempts');
        $this->db->where('lead_id', $id);
        $this->db->where('action', 'Contact Reached');
        $this->db->order_by('datetime', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function fetchStateName($id)
    {
        $this->db->select('name');
        $this->db->from('jurisdiction_states');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function fetchStateShortName($id)
    {
        $this->db->select('state_short_name');
        $this->db->from('jurisdiction_states');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function fetchJurisdictionName($id)
    {
        $this->db->select('*');
        $this->db->from('jurisdiction_n');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function fetchMarketName($id)
    {
        $this->db->select('*');
        $this->db->from('market_n');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    public function fetchCaseTypeName($id)
    {
        $this->db->select('type');
        $this->db->from('case_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    function saveLawyercreditcardDetails($data)
    {
        $this->db->set($data);
        $this->db->insert('users_credit_card');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveLawyerInitialSetupCharge($lawyer_data)
    {
        $this->db->set($lawyer_data);
        $this->db->insert('credit_card_transactions');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function save($data)
    {
        $this->db->set($data);
        $this->db->insert('users');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function update($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function updatePassword($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function update_vacation($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function updateStatus($data, $lead_id, $lawyer_id)
    {
        $this->db->where('lead_id', $lead_id);
        $this->db->where('lawyer_id', $lawyer_id);
        $this->db->update('laywer_lead_fee', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchRow($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchLawyerNames($id)
    {
        $this->db->select('name');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchLawyerFullNames($id)
    {
        $this->db->select('name,lname');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchAllTags()
    {
        $this->db->select('*');
        $this->db->from('tags_labels');
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    function fetchAssignLawyers($id)
    {
        $current_date = date('Y-m-d');
        $select_cols = "users.id, users.name, users.lname, users.law_firms, lawyers_to_leads.notes, law_firms.law_firm as law_firm_name, CASE WHEN users.holiday_to = '0000-00-00' OR users.holiday_to < '" .
        $current_date . "' THEN 0 WHEN users.holiday_to != '0000-00-00' AND users.holiday_to >= '" .
        $current_date . "' THEN 1 END as on_holiday";
        $q = "SELECT $select_cols FROM users JOIN lawyers_to_leads ON users.id = lawyers_to_leads.lawyer_id LEFT JOIN law_firms ON users.law_firms = law_firms.id WHERE lawyers_to_leads.lead_id = " .
        $id;
        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchLawyersLeads($id)
    {
        $this->db->select('leads.*, lawyers_to_leads.created_at as assign_date');
        $this->db->from('lawyers_to_leads');
        $this->db->join('leads', 'leads.id = lawyers_to_leads.lead_id ', 'right');
        $this->db->where('lawyers_to_leads.lawyer_id', $id);
        $this->db->order_by('lawyers_to_leads.created_at desc');
        $this->db->limit(5);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchLawyersVacationMode($id)
    {
        $this->db->select('holiday_from,holiday_to');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
    function fetchAllAssignLawyers()
    {
        $this->db->select('users.*, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, leads.email as lead_email, lawyers_to_leads.created_at as assigned_date');
        $this->db->from('users');
        $this->db->join('lawyers_to_leads', 'users.id = lawyers_to_leads.lawyer_id ');
        $this->db->join('leads', 'lawyers_to_leads.lead_id = leads.id ');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function fetchSetting($field)
    {
        $this->db->select($field);
        $this->db->from('settings');
        $this->db->where('id', '1');
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function deleteRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('users');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function get_single_user($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    public function get_users()
    {
        $this->db->select('*');
        $this->db->from('users');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function checkOldPassword($id, $password)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $this->db->where('password', $password);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function checkDuplicateEmail($post_email)
    {
        $this->db->where('email', $post_email);
        $query = $this->db->get('users');
        $count_row = $query->num_rows();
        if ($count_row > 0) {
            return false;
        } else {
            return true;
        }
    }
    function fetchAllPaymentRecords()
    {
        $this->db->select('*');
        $this->db->from('monthly_payment');
        $this->db->where('payment_Status', 0);
        $query = $this->db->get();
        return $query->result();
    }
    public function fetchLawyerRecord($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    /*
    public function fetchLawyersOnStatus($status)
    {
        $this->db->select('*');
        $this->db->from('leads');
        if ($status != '' && $status != '0') {
            if($status == 10){
                $this->db->where_in('lead_status', array("3", "11", "12", "13", "14"));
            }else{
                $this->db->where('lead_status', $status);
            }
        }
        $this->db->where('is_archive', 0);
        $query = $this->db->get();
        return $query->result();
    }
    */
    public function fetchLawyersOnStatus($status, $limit, $offset)
    {

        // Getting cookies
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status'] : $status;
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:'';
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:'';
        $advanceSearch = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:'';
        
        
        $this->db->select('leads.*, lawyers_to_leads.lawyer_id as lawyer_id, , lawyers_to_leads.billed as billed, , lawyers_to_leads.assigned_date as assigned_date, , lawyers_to_leads.lead_open_time as lead_open_time, CONCAT(leads.first_name, " " ,leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, lf.law_firm as lawfirm_name, ct.type as casetype, m.market_name, ls.name as status_name, (CASE WHEN leads.lead_status = 6 THEN 3 WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 2 THEN 5 WHEN leads.lead_status = 4 THEN 4 WHEN leads.lead_status = 5 THEN 6 WHEN leads.lead_status = 8 THEN 7 WHEN leads.lead_status = 15 THEN 2 WHEN leads.lead_status = 9 THEN 2 ELSE 99 END) AS default_order');
        
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ', 'left');
        $this->db->join("users as u", "lawyers_to_leads.lawyer_id = u.id", "left");
        $this->db->join("law_firms as lf", "u.law_firms = lf.id", "left");
        $this->db->join("case_type as ct", "leads.case_type = ct.id", "left");
        $this->db->join("market_n as m", "leads.market = m.id", "left");
        $this->db->join("lead_status as ls", "ls.id = leads.lead_status", "left");
        $this->db->join("leads as l", "l.id = lawyers_to_leads.lead_id", "left");
        
        $this->db->from('leads');
        
        if ($status != '' && $status != '0') {
            if($status == 10){
                $this->db->where_in('leads.lead_status', array("3", "11", "12", "13", "14"));
            }else{
                if($status != ""){
                    $this->db->where("leads.lead_status", $status);
                }
            }
        }
        
        if($case_type != ""){
            $this->db->where("leads.case_type", $case_type);
        }
        if($market != ""){
            $this->db->where("leads.market", $market);
        }
        if($advance_search_type != "" && $advanceSearch != ""){
            if($advance_search_type == "Lawyer"){
                $this->db->like("CONCAT(u.name, ' ', u.lname)", $advanceSearch);
            }else if($advance_search_type == "Law_Firm"){
                $this->db->like("lf.law_firm", $advanceSearch);
            }else if($advance_search_type == "Lead_Name"){
                $this->db->like("CONCAT(l.first_name, ' ', l.last_name)", $advanceSearch);
            }
        }
        if($status == ""){
            $this->db->where_not_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }
        $this->db->where('leads.is_archive', 0);
        $column = "default_order";
        $sort = "asc";
		if(isset($_GET["sort"])){
	      $column = $_GET["name"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        $this->db->limit($limit, $offset);
        $this->db->order_by($column, $sort);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function fetchLawyersOnStatusCount($status)
    {

        // Getting cookies
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status'] : $status;
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:'';
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:'';
        $advanceSearch = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:'';
        
        
        $this->db->select('leads.*, lawyers_to_leads.lawyer_id as lawyer_id, , lawyers_to_leads.billed as billed, , lawyers_to_leads.assigned_date as assigned_date, , lawyers_to_leads.lead_open_time as lead_open_time, CONCAT(leads.first_name, " " ,leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, lf.law_firm as lawfirm_name, ct.type as casetype, m.market_name, ls.name as status_name');
        
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ', 'left');
        $this->db->join("users as u", "lawyers_to_leads.lawyer_id = u.id", "left");
        $this->db->join("law_firms as lf", "u.law_firms = lf.id", "left");
        $this->db->join("case_type as ct", "leads.case_type = ct.id", "left");
        $this->db->join("market_n as m", "leads.market = m.id", "left");
        $this->db->join("lead_status as ls", "ls.id = leads.lead_status", "left");
        $this->db->join("leads as l", "l.id = lawyers_to_leads.lead_id", "left");
        
        $this->db->from('leads');
        
        if ($status != '' && $status != '0') {
            if($status == 10){
                $this->db->where_in('leads.lead_status', array("3", "11", "12", "13", "14"));
            }else{
                if($status != ""){
                    $this->db->where("leads.lead_status", $status);
                }
            }
        }
        
        if($case_type != ""){
            $this->db->where("leads.case_type", $case_type);
        }
        if($market != ""){
            $this->db->where("leads.market", $market);
        }
        if($advance_search_type != "" && $advanceSearch != ""){
            if($advance_search_type == "Lawyer"){
                $this->db->like("CONCAT(u.name, ' ', u.lname)", $advanceSearch);
            }else if($advance_search_type == "Law_Firm"){
                $this->db->like("lf.law_firm", $advanceSearch);
            }else if($advance_search_type == "Lead_Name"){
                $this->db->like("CONCAT(l.first_name, ' ', l.last_name)", $advanceSearch);
            }
        }
        if($status == ""){
            $this->db->where_not_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }
        $this->db->where('leads.is_archive', 0);
        $column = "id";
        $sort = "desc";
		if(isset($_GET["sort"])){
	      $column = $_GET["name"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        $this->db->order_by($column, $sort);
        $query = $this->db->get();
        return $query->result();
    }
    /*
    public function fetchLawyersOnCaseType($case_type)
    {
        $this->db->select('*');
        $this->db->from('leads');
        if ($case_type != '') {
            $this->db->where('case_type', $case_type);
        }
        $this->db->where('is_archive', 0);
        $query = $this->db->get();
        return $query->result();
    }
    */
    public function fetchLawyersOnCaseType($case_type, $limit, $offset)
    {
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status']:'';
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:$case_type;
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:'';
        $advanceSearch = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:'';

        $this->db->select('leads.*, lawyers_to_leads.lawyer_id as lawyer_id, , lawyers_to_leads.billed as billed, , lawyers_to_leads.assigned_date as assigned_date, , lawyers_to_leads.lead_open_time as lead_open_time, CONCAT(leads.first_name, " " ,leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, lf.law_firm as lawfirm_name, ct.type as casetype, m.market_name, ls.name as status_name, (CASE WHEN leads.lead_status = 6 THEN 3 WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 2 THEN 5 WHEN leads.lead_status = 4 THEN 4 WHEN leads.lead_status = 5 THEN 6 WHEN leads.lead_status = 8 THEN 7 WHEN leads.lead_status = 15 THEN 2 WHEN leads.lead_status = 9 THEN 2 ELSE 99 END) AS default_order');
        
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ', 'left');
        $this->db->join("users as u", "lawyers_to_leads.lawyer_id = u.id", "left");
        $this->db->join("law_firms as lf", "u.law_firms = lf.id", "left");
        $this->db->join("case_type as ct", "leads.case_type = ct.id", "left");
        $this->db->join("market_n as m", "leads.market = m.id", "left");
        $this->db->join("lead_status as ls", "ls.id = leads.lead_status", "left");
        $this->db->join("leads as l", "l.id = lawyers_to_leads.lead_id", "left");
        
        $this->db->from('leads');
        
        if($status != ""  && $status != 0 && $status != 10){
            $this->db->where("leads.lead_status", $status);
        }
        if($case_type != ""){
            $this->db->where("leads.case_type", $case_type);
        }
        if($market != ""){
            $this->db->where("leads.market", $market);
        }
        if($advance_search_type != "" && $advanceSearch != ""){
            if($advance_search_type == "Lawyer"){
                $this->db->like("CONCAT(u.name, ' ', u.lname)", $advanceSearch);
            }else if($advance_search_type == "Law_Firm"){
                $this->db->like("lf.law_firm", $advanceSearch);
            }else if($advance_search_type == "Lead_Name"){
                $this->db->like("CONCAT(l.first_name, ' ', l.last_name)", $advanceSearch);
            }
        }
        $this->db->where('leads.is_archive', 0);
        
        if($status == ""){
            $this->db->where_not_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }else if($status == "10"){
            $this->db->where_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }
        $column = "default_order";
        $sort = "asc";
		if(isset($_GET["sort"])){
	      $column = $_GET["name"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        $this->db->limit($limit, $offset);
        $this->db->order_by($column, $sort);
        $query = $this->db->get();
        return $query->result();
    }
    public function fetchLawyersOnCaseTypeCount($case_type)
    {
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status']:'';
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:$case_type;
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:'';
        $advanceSearch = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:'';

        $this->db->select('leads.*, lawyers_to_leads.lawyer_id as lawyer_id, , lawyers_to_leads.billed as billed, , lawyers_to_leads.assigned_date as assigned_date, , lawyers_to_leads.lead_open_time as lead_open_time, CONCAT(leads.first_name, " " ,leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, lf.law_firm as lawfirm_name, ct.type as casetype, m.market_name, ls.name as status_name');
        
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ', 'left');
        $this->db->join("users as u", "lawyers_to_leads.lawyer_id = u.id", "left");
        $this->db->join("law_firms as lf", "u.law_firms = lf.id", "left");
        $this->db->join("case_type as ct", "leads.case_type = ct.id", "left");
        $this->db->join("market_n as m", "leads.market = m.id", "left");
        $this->db->join("lead_status as ls", "ls.id = leads.lead_status", "left");
        $this->db->join("leads as l", "l.id = lawyers_to_leads.lead_id", "left");
        
        $this->db->from('leads');
        
        if($status != ""  && $status != 0 && $status != 10){
            $this->db->where("leads.lead_status", $status);
        }
        if($case_type != ""){
            $this->db->where("leads.case_type", $case_type);
        }
        if($market != ""){
            $this->db->where("leads.market", $market);
        }
        if($advance_search_type != "" && $advanceSearch != ""){
            if($advance_search_type == "Lawyer"){
                $this->db->like("CONCAT(u.name, ' ', u.lname)", $advanceSearch);
            }else if($advance_search_type == "Law_Firm"){
                $this->db->like("lf.law_firm", $advanceSearch);
            }else if($advance_search_type == "Lead_Name"){
                $this->db->like("CONCAT(l.first_name, ' ', l.last_name)", $advanceSearch);
            }
        }
        $this->db->where('leads.is_archive', 0);
        
        if($status == ""){
            $this->db->where_not_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }else if($status == "10"){
            $this->db->where_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }
        $column = "id";
        $sort = "desc";
		if(isset($_GET["sort"])){
	      $column = $_GET["name"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        $this->db->order_by($column, $sort);
        $query = $this->db->get();
        return $query->result();
    }
    /*
    public function fetchLawyersOnMarket($market)
    {
        $this->db->select('*');
        $this->db->from('leads');
        if ($market != '') {
            $this->db->where('market', $market);
        }
        $this->db->where('is_archive', 0);
        $query = $this->db->get();
        return $query->result();
    }
    */
    public function fetchLawyersOnMarket($market, $limit, $offset)
    {
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:$market;
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status']:'';
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:'';
        $advanceSearch = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:'';
        
        $this->db->select('leads.*, lawyers_to_leads.lawyer_id as lawyer_id, , lawyers_to_leads.billed as billed, , lawyers_to_leads.assigned_date as assigned_date, , lawyers_to_leads.lead_open_time as lead_open_time, CONCAT(leads.first_name, " " ,leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, lf.law_firm as lawfirm_name, ct.type as casetype, m.market_name, ls.name as status_name, (CASE WHEN leads.lead_status = 6 THEN 3 WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 2 THEN 5 WHEN leads.lead_status = 4 THEN 4 WHEN leads.lead_status = 5 THEN 6 WHEN leads.lead_status = 8 THEN 7 WHEN leads.lead_status = 15 THEN 2 WHEN leads.lead_status = 9 THEN 2 ELSE 99 END) AS default_order');
        
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ', 'left');
        $this->db->join("users as u", "lawyers_to_leads.lawyer_id = u.id", "left");
        $this->db->join("law_firms as lf", "u.law_firms = lf.id", "left");
        $this->db->join("case_type as ct", "leads.case_type = ct.id", "left");
        $this->db->join("market_n as m", "leads.market = m.id", "left");
        $this->db->join("lead_status as ls", "ls.id = leads.lead_status", "left");
        $this->db->join("leads as l", "l.id = lawyers_to_leads.lead_id", "left");
        
        $this->db->from('leads');
        
        if($status != ""  && $status != 0 && $status != 10){
            $this->db->where("leads.lead_status", $status);
        }
        if($case_type != ""){
            $this->db->where("leads.case_type", $case_type);
        }
        if($market != ""){
            $this->db->where("leads.market", $market);
        }
        if($advance_search_type != "" && $advanceSearch != ""){
            if($advance_search_type == "Lawyer"){
                $this->db->like("CONCAT(u.name, ' ', u.lname)", $advanceSearch);
            }else if($advance_search_type == "Law_Firm"){
                $this->db->like("lf.law_firm", $advanceSearch);
            }else if($advance_search_type == "Lead_Name"){
                $this->db->like("CONCAT(l.first_name, ' ', l.last_name)", $advanceSearch);
            }
        }
        if($status == ""){
            $this->db->where_not_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }else if($status == "10"){
            $this->db->where_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }
        $this->db->where('leads.is_archive', 0);
        $column = "default_order";
        $sort = "asc";
		if(isset($_GET["sort"])){
	      $column = $_GET["name"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        $this->db->limit($limit, $offset);
        $this->db->order_by($column, $sort);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function fetchLawyersOnMarketCount($market)
    {
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:$market;
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status']:'';
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:'';
        $advanceSearch = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:'';
        
        $this->db->select('leads.*, lawyers_to_leads.lawyer_id as lawyer_id, , lawyers_to_leads.billed as billed, , lawyers_to_leads.assigned_date as assigned_date, , lawyers_to_leads.lead_open_time as lead_open_time, CONCAT(leads.first_name, " " ,leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, lf.law_firm as lawfirm_name, ct.type as casetype, m.market_name, ls.name as status_name');
        
        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ', 'left');
        $this->db->join("users as u", "lawyers_to_leads.lawyer_id = u.id", "left");
        $this->db->join("law_firms as lf", "u.law_firms = lf.id", "left");
        $this->db->join("case_type as ct", "leads.case_type = ct.id", "left");
        $this->db->join("market_n as m", "leads.market = m.id", "left");
        $this->db->join("lead_status as ls", "ls.id = leads.lead_status", "left");
        $this->db->join("leads as l", "l.id = lawyers_to_leads.lead_id", "left");
        
        $this->db->from('leads');
        
        if($status != ""  && $status != 0 && $status != 10){
            $this->db->where("leads.lead_status", $status);
        }
        if($case_type != ""){
            $this->db->where("leads.case_type", $case_type);
        }
        if($market != ""){
            $this->db->where("leads.market", $market);
        }
        if($advance_search_type != "" && $advanceSearch != ""){
            if($advance_search_type == "Lawyer"){
                $this->db->like("CONCAT(u.name, ' ', u.lname)", $advanceSearch);
            }else if($advance_search_type == "Law_Firm"){
                $this->db->like("lf.law_firm", $advanceSearch);
            }else if($advance_search_type == "Lead_Name"){
                $this->db->like("CONCAT(l.first_name, ' ', l.last_name)", $advanceSearch);
            }
        }
        if($status == ""){
            $this->db->where_not_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }else if($status == "10"){
            $this->db->where_in('leads.lead_status', array("3", "11", "12", "13", "14"));
        }
        $this->db->where('leads.is_archive', 0);
        $column = "id";
        $sort = "desc";
		if(isset($_GET["sort"])){
	      $column = $_GET["name"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        $this->db->order_by($column, $sort);
        $query = $this->db->get();
        return $query->result();
    }
    function UpdateleadOpenDateTime($data, $lawyer_id, $lead_id)
    {
        $this->db->where('id', $lead_id);
        $this->db->where('lead_open_datetime', null);
        $this->db->update('leads', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function fetchLawyersOnCaseIdCount($case_id)
    {
        // Getting cookies
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status']:'';
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:'';
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:'';
        $advanceSearch = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:'';
        
        $column = "id";
        $sort = "desc";
		if(isset($_GET["sort"])){
	      $column = $_GET["name"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        
        if ($case_id != '') {
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name` FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND RIGHT(`leads`.`lead_id_number`, 5) LIKE "%' . $case_id . '%" ORDER BY '. $column .' ' . $sort . '';
            
            #$sql = "SELECT * FROM leads WHERE is_archive = 0 AND RIGHT(lead_id_number, 5) LIKE '%" . $case_id . "%'";
        } else {
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name` FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 '.($status != "" && $status != 0 && $status != 10 ? "AND `leads`.`lead_status` = '{$status}'" : "").' '.($case_type != "" ? "AND `leads`.`case_type` = '{$case_type}'" : "" ).' '.($market != "" ? "AND `leads`.`market` = '{$market}'" : "" ).' ORDER BY '. $column .' ' . $sort . '';
            
            #$sql = "SELECT * FROM leads WHERE (lead_status = 1 || lead_status = 2) AND is_archive = 0 ORDER BY first_name DESC";
        }
        $query = $this->db->query($sql);
        return $query->result();
    }
    function fetchLawyersOnCaseId($case_id, $limit, $offset)
    {
        // Getting cookies
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status']:'';
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:'';
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:'';
        $advanceSearch = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:'';
        
        $column = "default_order";
        $sort = "asc";
		if(isset($_GET["sort"])){
	      $column = $_GET["name"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        
        if ($case_id != '') {
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name`, (CASE WHEN leads.lead_status = 6 THEN 3 WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 2 THEN 5 WHEN leads.lead_status = 4 THEN 4 WHEN leads.lead_status = 5 THEN 6 WHEN leads.lead_status = 8 THEN 7 WHEN leads.lead_status = 15 THEN 2 WHEN leads.lead_status = 9 THEN 2 ELSE 99 END) AS default_order FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND RIGHT(`leads`.`lead_id_number`, 5) LIKE "%' . $case_id . '%" ORDER BY '. $column .' ' . $sort . ' LIMIT ' . $offset . ", " . $limit;
            
            #$sql = "SELECT * FROM leads WHERE is_archive = 0 AND RIGHT(lead_id_number, 5) LIKE '%" . $case_id . "%'";
        } else {
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name`, (CASE WHEN leads.lead_status = 6 THEN 3 WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 2 THEN 5 WHEN leads.lead_status = 4 THEN 4 WHEN leads.lead_status = 5 THEN 6 WHEN leads.lead_status = 8 THEN 7 WHEN leads.lead_status = 15 THEN 2 WHEN leads.lead_status = 9 THEN 2 ELSE 99 END) AS default_order FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 '.($status != "" && $status != 0 && $status != 10 ? "AND `leads`.`lead_status` = '{$status}'" : "").' '.($case_type != "" ? "AND `leads`.`case_type` = '{$case_type}'" : "" ).' '.($market != "" ? "AND `leads`.`market` = '{$market}'" : "" ).' ORDER BY '. $column .' ' . $sort . ' LIMIT ' . $offset . ', ' . $limit;
            
            #$sql = "SELECT * FROM leads WHERE (lead_status = 1 || lead_status = 2) AND is_archive = 0 ORDER BY first_name DESC";
        }
        $query = $this->db->query($sql);
        return $query->result();
    }
    /*
    public function advanceSearch($text, $advance_search_type)
    {
        if ($text != '' && $advance_search_type == 'Lawyer') {
            $sql = "SELECT l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE (u_a.name LIKE '%" .
            $text . "%' OR u_a.lname LIKE '%" . $text .
            "%' OR CONCAT(u_a.name, ' ', u_a.lname) LIKE '%" . $text .
            "%') AND (u_a.id = l_t_l.lawyer_id) AND (l.id = l_t_l.lead_id) AND l.is_archive = 0 GROUP BY l.id";
        } else
        if ($text != '' && $advance_search_type == 'Law_Firm') {
            $sql = "SELECT l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE ( law_f.law_firm LIKE '%" .
            $text . "%'  AND  law_f.id = u_a.law_firms AND u_a.id = l_t_l.lawyer_id and l.id = l_t_l.lead_id ) AND l.is_archive = 0";
        } else
        if ($text != '' && $advance_search_type == 'Lead_Name') {
            $sql = "SELECT l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE (l.first_name LIKE '%" .
            $text . "%' OR l.last_name LIKE '%" . $text .
            "%' OR CONCAT(l.first_name, ' ', l.last_name) LIKE '%" . $text .
            "%') AND l.is_archive = 0 GROUP BY l.id";
        } else {
            $sql = "SELECT * FROM leads WHERE (lead_status = 1 || lead_status = 2) AND is_archive = 0 ORDER BY first_name DESC";
        }
        $query = $this->db->query($sql);
        return $query->result();
    }
    */
    public function advanceSearch($text, $advance_search_type, $limit, $offset)
    {
        $where = "";
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:"";
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status']:'';
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:$advance_search_type;
        $text = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:$text;
        
        $column = "default_order";
        $sort = "asc";
		if(isset($_GET["sort"])){
		  if($_GET["name"] != "case_fee_received" && $_GET["name"] != "referral_fee_received"){
    	      $column = $_GET["name"];
              if($column == "date_received"){
                $column = "created_at";
              }else if($column == "date_assigned"){
                $column = "assigned_date";
              }else if($column == "date_opened"){
                $column = "lead_open_datetime";
              }else if($column == "date_completed"){
                $column = "date_closed";
              }
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }else if($column == "case_fee_received"){
            $column = "id";
          }else if($column == "referral_fee_received"){
            $column = "id";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        
        
        if($market != "" || $status != "" || $case_type != ""){
            $where_sql = "";
            if($market != ""){
                $where_sql .= "AND leads.market = '$market'";
            }
            if($status != ""){
                $where_sql .= "AND leads.lead_status = '$status'";
            }else{
                $where_sql .= "AND (leads.lead_status = 1 || leads.lead_status = 2)";
            }
            if($case_type != ""){
                $where_sql .= "AND leads.case_type = '$case_type'";
            }
            //$where .= trim($where_sql, "AND ") . " ";
            $where = $where_sql;
        }
        
        
        if ($text != '' && $advance_search_type == 'Lawyer') {
            #$sql = "SELECT l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE (u_a.name LIKE '%" . $text . "%' OR u_a.lname LIKE '%" . $text . "%' OR CONCAT(u_a.name, ' ', u_a.lname) LIKE '%" . $text . "%') AND (u_a.id = l_t_l.lawyer_id) AND (l.id = l_t_l.lead_id) AND l.is_archive = 0 $where GROUP BY l.id";
            
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name`, (CASE WHEN leads.lead_status = 6 THEN 3 WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 2 THEN 5 WHEN leads.lead_status = 4 THEN 4 WHEN leads.lead_status = 5 THEN 6 WHEN leads.lead_status = 8 THEN 7 WHEN leads.lead_status = 15 THEN 2 WHEN leads.lead_status = 9 THEN 2 ELSE 99 END) AS default_order FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND (u.name LIKE "%' . $text . '%" OR u.lname LIKE "%' . $text . '%" OR CONCAT(u.name, " ", u.lname) LIKE "%' . $text . '%") '.$where.' group by leads.id ORDER BY '.$column.' '.$sort.' LIMIT ' . $offset . ', ' . $limit;
            
        } else
        if ($text != '' && $advance_search_type == 'Law_Firm') {
            #$sql = "SELECT l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE ( law_f.law_firm LIKE '%" . $text . "%'  AND  law_f.id = u_a.law_firms AND u_a.id = l_t_l.lawyer_id and l.id = l_t_l.lead_id ) AND l.is_archive = 0 $where";
            
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name`, (CASE WHEN leads.lead_status = 6 THEN 3 WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 2 THEN 5 WHEN leads.lead_status = 4 THEN 4 WHEN leads.lead_status = 5 THEN 6 WHEN leads.lead_status = 8 THEN 7 WHEN leads.lead_status = 15 THEN 2 WHEN leads.lead_status = 9 THEN 2 ELSE 99 END) AS default_order FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND lf.law_firm LIKE "%' . $text . '%" '.$where.' group by leads.id ORDER BY '.$column.' '.$sort.' LIMIT ' . $offset . ', ' . $limit;
            
        } else
        if ($text != '' && $advance_search_type == 'Lead_Name') {
            #$sql = "SELECT l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE (l.first_name LIKE '%" . $text . "%' OR l.last_name LIKE '%" . $text . "%' OR CONCAT(l.first_name, ' ', l.last_name) LIKE '%" . $text . "%') AND l.is_archive = 0 $where GROUP BY l.id";
            
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name`, (CASE WHEN leads.lead_status = 6 THEN 3 WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 2 THEN 5 WHEN leads.lead_status = 4 THEN 4 WHEN leads.lead_status = 5 THEN 6 WHEN leads.lead_status = 8 THEN 7 WHEN leads.lead_status = 15 THEN 2 WHEN leads.lead_status = 9 THEN 2 ELSE 99 END) AS default_order FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND (leads.first_name LIKE "%' . $text . '%" OR leads.last_name LIKE "%' . $text . '%" OR CONCAT(leads.first_name, " ", leads.last_name) LIKE "%' . $text . '%") '.$where.' group by leads.id ORDER BY '.$column.' '.$sort.' LIMIT ' . $offset . ', ' . $limit;
            
        } else {
            #$sql = "SELECT l.* FROM leads as l WHERE l.is_archive = 0 $where ORDER BY l.first_name DESC";
            
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name`, (CASE WHEN leads.lead_status = 6 THEN 3 WHEN leads.lead_status = 1 THEN 1 WHEN leads.lead_status = 2 THEN 5 WHEN leads.lead_status = 4 THEN 4 WHEN leads.lead_status = 5 THEN 6 WHEN leads.lead_status = 8 THEN 7 WHEN leads.lead_status = 15 THEN 2 WHEN leads.lead_status = 9 THEN 2 ELSE 99 END) AS default_order FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 '.$where.' ORDER BY '.$column.' '.$sort.' LIMIT ' . $offset . ', ' . $limit;
            
        }
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function advanceSearchCount($text, $advance_search_type)
    {
        $where = "";
        $market = isset($_COOKIE['market']) && $_COOKIE['market'] != '' ? $_COOKIE['market']:"";
        $status = isset($_COOKIE['status']) && $_COOKIE['status'] != '' ? $_COOKIE['status']:'';
        $case_type = isset($_COOKIE['case_type']) && $_COOKIE['case_type'] != '' ? $_COOKIE['case_type']:'';
        $advance_search_type = isset($_COOKIE['advance_search_type']) && $_COOKIE['advance_search_type'] != '' ? $_COOKIE['advance_search_type']:$advance_search_type;
        $text = isset($_COOKIE['advanceSearch']) && $_COOKIE['advanceSearch'] != '' ? $_COOKIE['advanceSearch']:$text;
        
        $column = "id";
        $sort = "desc";
		if(isset($_GET["sort"])){
		  if($_GET["name"] != "case_fee_received" && $_GET["name"] != "referral_fee_received"){
    	      $column = $_GET["name"];
              if($column == "date_received"){
                $column = "created_at";
              }else if($column == "date_assigned"){
                $column = "assigned_date";
              }else if($column == "date_opened"){
                $column = "lead_open_datetime";
              }else if($column == "date_completed"){
                $column = "date_closed";
              }
          }
	      $sort = $_GET["sort"];
		}else if(isset($_COOKIE["column"]) && $_COOKIE["column"] != "" && isset($_COOKIE["sort"]) && $_COOKIE["sort"] != ""){
		  $column = $_COOKIE["column"];
          if($column == "date_received"){
            $column = "created_at";
          }else if($column == "date_assigned"){
            $column = "assigned_date";
          }else if($column == "date_opened"){
            $column = "lead_open_datetime";
          }else if($column == "date_completed"){
            $column = "date_closed";
          }else if($column == "case_fee_received"){
            $column = "id";
          }else if($column == "referral_fee_received"){
            $column = "id";
          }
          $sort = $_COOKIE["sort"];
          $sort = $sort == "ascending" ? "ASC" : "DESC";  
		}
        
        
        if($market != "" || $status != "" || $case_type != ""){
            $where_sql = "";
            if($market != ""){
                $where_sql .= "AND leads.market = '$market'";
            }
            if($status != ""){
                $where_sql .= "AND leads.lead_status = '$status'";
            }else{
                $where_sql .= "AND (leads.lead_status = 1 || leads.lead_status = 2)";
            }
            if($case_type != ""){
                $where_sql .= "AND leads.case_type = '$case_type'";
            }
            //$where .= trim($where_sql, "AND ") . " ";
            $where = $where_sql;
        }
        
        
        if ($text != '' && $advance_search_type == 'Lawyer') {
            #$sql = "SELECT l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE (u_a.name LIKE '%" . $text . "%' OR u_a.lname LIKE '%" . $text . "%' OR CONCAT(u_a.name, ' ', u_a.lname) LIKE '%" . $text . "%') AND (u_a.id = l_t_l.lawyer_id) AND (l.id = l_t_l.lead_id) AND l.is_archive = 0 $where GROUP BY l.id";
            
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name` FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND (u.name LIKE "%' . $text . '%" OR u.lname LIKE "%' . $text . '%" OR CONCAT(u.name, " ", u.lname) LIKE "%' . $text . '%") '.$where.' group by leads.id ORDER BY '.$column.' '.$sort.'';
            
        } else
        if ($text != '' && $advance_search_type == 'Law_Firm') {
            #$sql = "SELECT l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE ( law_f.law_firm LIKE '%" . $text . "%'  AND  law_f.id = u_a.law_firms AND u_a.id = l_t_l.lawyer_id and l.id = l_t_l.lead_id ) AND l.is_archive = 0 $where";
            
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name` FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND lf.law_firm LIKE "%' . $text . '%" '.$where.' group by leads.id ORDER BY '.$column.' '.$sort.'';
            
        } else
        if ($text != '' && $advance_search_type == 'Lead_Name') {
            #$sql = "SELECT l.* ,u_a.name as lawyer_f_name, u_a.lname as lawyer_l_name FROM leads as l , users as u_a , lawyers_to_leads as l_t_l , law_firms as law_f WHERE (l.first_name LIKE '%" . $text . "%' OR l.last_name LIKE '%" . $text . "%' OR CONCAT(l.first_name, ' ', l.last_name) LIKE '%" . $text . "%') AND l.is_archive = 0 $where GROUP BY l.id";
            
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name` FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 AND (leads.first_name LIKE "%' . $text . '%" OR leads.last_name LIKE "%' . $text . '%" OR CONCAT(leads.first_name, " ", leads.last_name) LIKE "%' . $text . '%") '.$where.' group by leads.id ORDER BY '.$column.' '.$sort.'';
            
        } else {
            #$sql = "SELECT l.* FROM leads as l WHERE l.is_archive = 0 $where ORDER BY l.first_name DESC";
            
            $sql = 'SELECT `leads`.*, `lawyers_to_leads`.`lawyer_id` as `lawyer_id`, `lawyers_to_leads`.`billed` as `billed`, `lawyers_to_leads`.`assigned_date` as `assigned_date`, `lawyers_to_leads`.`lead_open_time` as `lead_open_time`, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, CONCAT(u.name, " ", u.lname) as lawyer_name, `lf`.`law_firm` as `lawfirm_name`, `ct`.`type` as `casetype`, `m`.`market_name`, `ls`.`name` as `status_name` FROM `leads` LEFT JOIN `lawyers_to_leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT JOIN `users` as `u` ON `lawyers_to_leads`.`lawyer_id` = `u`.`id` LEFT JOIN `law_firms` as `lf` ON `u`.`law_firms` = `lf`.`id` LEFT JOIN `case_type` as `ct` ON `leads`.`case_type` = `ct`.`id` LEFT JOIN `market_n` as `m` ON `leads`.`market` = `m`.`id` LEFT JOIN `lead_status` as `ls` ON `ls`.`id` = `leads`.`lead_status` LEFT JOIN `leads` as `l` ON `l`.`id` = `lawyers_to_leads`.`lead_id` WHERE `leads`.`is_archive` =0 '.$where.' ORDER BY '.$column.' '.$sort.'';
            
        }
        $query = $this->db->query($sql);
        return $query->result();
    }
    function lawFirmPayment($law_firm_id)
    {
        $this->db->select('(select SUM(price) from leads l, lawyers_to_leads ltl, law_firms lf, users u where lf.id=u.law_firms AND u.id=ltl.lawyer_id AND l.id=ltl.lead_id AND lf.id=' .
            $law_firm_id . ' AND ltl.billed="yes") as total_lead_price');
        $this->db->select('(select sum(monthly_bill) from monthly_payment mp, law_firms lf, users u where lf.id=u.law_firms AND u.id=mp.lawyer_id  AND lf.id=' .
            $law_firm_id . ') as monthly_payment');
        $this->db->select('(select sum(amount) from credit_card_transactions cct, law_firms lf, users u where lf.id=u.law_firms AND u.id=cct.lawyer_id AND cct.status = 1 AND lf.id=' .
            $law_firm_id . ') as total_charge_bill');
        $this->db->select('(select sum(amount) from credit_card_transactions cct, law_firms lf, users u where lf.id=u.law_firms AND u.id=cct.lawyer_id AND lf.id=' .
            $law_firm_id . ' AND cct.status = 2) as paidTransactions');
        $this->db->from('law_firms lf, users u, lawyers_to_leads ltl, leads l, monthly_payment mp');
        $this->db->group_by('total_lead_price');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
    function lawFirmsPayment()
    {
        $this->db->select('(select SUM(price) from leads l, lawyers_to_leads ltl, law_firms lf, users u where lf.id=u.law_firms AND u.id=ltl.lawyer_id AND l.id=ltl.lead_id AND ltl.billed="yes") as total_lead_price');
        $this->db->select('(select sum(monthly_bill) from monthly_payment mp, law_firms lf, users u where lf.id=u.law_firms AND u.id=mp.lawyer_id) as monthly_payment');
        $this->db->select('(select sum(amount) from credit_card_transactions cct, law_firms lf, users u where lf.id=u.law_firms AND u.id=cct.lawyer_id AND cct.status = 1) as total_charge_bill');
        $this->db->select('(select sum(amount) from credit_card_transactions cct, law_firms lf, users u where lf.id=u.law_firms AND u.id=cct.lawyer_id AND cct.status = 2) as paidTransactions');
        $this->db->from('law_firms lf, users u, lawyers_to_leads ltl, leads l, monthly_payment mp');
        $this->db->group_by('total_lead_price');
        $query = $this->db->get();
        //echo $this->db->last_query();exit();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }
    function lawFirms()
    {
        $this->db->select('law_firms.*');
        $this->db->select('(select SUM(price) from leads l, lawyers_to_leads ltl, law_firms lf, users u where lf.id=u.law_firms AND u.id=ltl.lawyer_id AND l.id=ltl.lead_id AND lf.id=law_firms.id AND ltl.billed="yes") as total_lead_price');
        $this->db->select('(select sum(monthly_bill) from monthly_payment mp, law_firms lf, users u where lf.id=u.law_firms AND u.id=mp.lawyer_id AND lf.id=law_firms.id) as monthly_payment');
        $this->db->select('(select sum(amount) from credit_card_transactions cct, law_firms lf, users u where lf.id=u.law_firms AND u.id=cct.lawyer_id AND cct.status = 1 AND lf.id=law_firms.id) as total_charge_bill');
        $this->db->select('(select sum(amount) from credit_card_transactions cct, law_firms lf, users u where lf.id=u.law_firms AND u.id=cct.lawyer_id AND lf.id=law_firms.id AND cct.status = 2) as paidTransactions');
        $this->db->from('law_firms');
        $this->db->group_by('law_firms.id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function LawyerBilling($lawfirm_id, $role, $active, array $like_array = null, $search_type = null)
    {
        $this->db->select('users.*');
        $this->db->select('(select SUM(price) from leads l, lawyers_to_leads ltl, law_firms lf, users u where lf.id=u.law_firms AND u.id=ltl.lawyer_id AND l.id=ltl.lead_id AND u.id=users.id AND ltl.billed="yes") as total_lead_price');
        $this->db->select('(select sum(monthly_bill) from monthly_payment mp, law_firms lf, users u where lf.id=u.law_firms AND u.id=mp.lawyer_id AND u.id=users.id) as monthly_payment');
        $this->db->select('(select sum(amount) from credit_card_transactions cct, law_firms lf, users u where lf.id=u.law_firms AND u.id=cct.lawyer_id AND cct.status = 1 AND u.id=users.id) as total_charge_bill');
        $this->db->select('(select sum(amount) from credit_card_transactions cct, law_firms lf, users u where lf.id=u.law_firms AND u.id=cct.lawyer_id AND u.id=users.id AND cct.status = 2) as paidTransactions');
        $this->db->from('users');
        $this->db->where('users.law_firms', $lawfirm_id);
        $this->db->where('users.active', $active);
        $this->db->where('users.role', $role);
        foreach ($like_array as $where => $string) {
            $this->db->like('users.' . $where, $string, $search_type);
        } //$like_array as $where => $string
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function lead_assigned_users($role, $active, $jurisdiction, $case_type)
    {
        $current_date = date('Y-m-d');
        $select_cols = "u.*, l.case_type as casetype_value, l.jurisdiction as jurisdiction_value, CASE WHEN u.holiday_to = '0000-00-00' OR u.holiday_to < '" .
        $current_date . "' THEN 0 WHEN u.holiday_to != '0000-00-00' AND u.holiday_to >= '" .
        $current_date . "' THEN 1 END as on_holiday";

        $q = "SELECT $select_cols FROM (`users` `u`, `leads` `l`) JOIN `jurisdiction_n` ON `jurisdiction_n`.`id` = $jurisdiction AND `u`.`jurisdiction` LIKE CONCAT(\"%\",jurisdiction_n.name,\"%\") JOIN `case_type` ON `case_type`.`id` = $case_type AND `u`.`case_type` LIKE CONCAT(\"%\",case_type.type,\"%\") WHERE `u`.`role` = $role AND `u`.`active` = $active GROUP BY `u`.`id`";
        $query = $this->db->query($q);


        /*$this->db->select('u.*, l.case_type as casetype_value, l.jurisdiction as jurisdiction_value');
        $this->db->from('users u, leads l');
        $this->db->join('jurisdiction_n', 'jurisdiction_n.id = '.$jurisdiction.' AND u.jurisdiction LIKE CONCAT("%",jurisdiction_n.name,"%")', false);	
        $this->db->join('case_type', 'case_type.id = '.$case_type.' AND u.case_type LIKE CONCAT("%",case_type.type,"%")', false);
        $this->db->where('u.role', $role);
        $this->db->where('u.active', $active);
        $this->db->group_by('u.id');
        $query = $this->db->get();
        echo $this->db->last_query();exit();*/
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getAllLawyers()
    {
        $current_date = date('Y-m-d');
        $q = "SELECT *, CASE WHEN holiday_to = '0000-00-00' OR holiday_to < '" . $current_date .
        "' THEN 0 WHEN holiday_to != '0000-00-00' AND holiday_to >= '" . $current_date .
        "' THEN 1 END as on_holiday FROM `users` WHERE `role` = '2' AND `active` = '1'";
        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function get_firm($id)
    {
        $this->db->select('*');
        $this->db->from('law_firms');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    
    /**
     * Users_model::fetchJurisdictionPrice()
     * 
     * @param mixed $id
     * @param mixed $state
     * @param mixed $case_type
     * @return
     */
    public function fetchJurisdictionPrice($id, $state = null, $case_type = null) {
        $this->db->where('Jurisdiction', $id);
        if($state)
        $this->db->where('state', $state);
        if($case_type)
        $this->db->where('case_type', $case_type);
        $result = $this->db->get('Jurisdiction_fee');
        return $result->row();
    }
}
