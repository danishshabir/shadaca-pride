<?php
class Mail_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function getAllData($user_type, $id, $action)
    {
        $this->db->select('mail.*, users.name as user_name, law_firms.contact_name as lawyer_name');
        $this->db->from('mail');
        $this->db->join('users', 'mail.sender_id = users.id AND users.role = mail.sender_role', 'left');
        $this->db->join('law_firms', 'mail.sender_id = law_firms.id AND law_firms.role = mail.sender_role', 'left');
        $this->db->where('mail.' . $user_type . '_id', $id);
        $this->db->where('mail.action', $action);
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    function getMailById($id)
    {
        $this->db->select('mail.*, users.name as user_name, users.email as user_email, law_firms.contact_name as lawyer_name,, law_firms.contact_email as lawyer_email');
        $this->db->from('mail');
        $this->db->join('users', 'mail.sender_id = users.id AND users.role = mail.sender_role', 'left');
		$this->db->join('law_firms', 'mail.sender_id = law_firms.id AND law_firms.role = mail.sender_role', 'left');
        $this->db->where('mail.id', $id);
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get();
        return $query->row();
    }
    function getThreadData($sender_id, $id)
    {
        $where = "(mail.sender_id = $sender_id AND mail.reciever_id = $id) OR (mail.sender_id = $id AND mail.reciever_id = $sender_id)";
        $this->db->select('mail.*, users.name as user_name, users.email as user_email, law_firms.contact_name as lawyer_name,, law_firms.contact_email as lawyer_email');
        $this->db->from('mail');
        $this->db->join('users', 'mail.sender_id = users.id AND users.role = mail.sender_role', 'left');
		$this->db->join('law_firms', 'mail.sender_id = law_firms.id AND law_firms.role = mail.sender_role', 'left');
        $this->db->where($where);
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    function getAllUnreadData($user_type, $id, $action)
    {
        $this->db->select('mail.*, users.name as user_name');
        $this->db->from('mail');
        $this->db->join('users', 'mail.sender_id = users.id', 'left');
        $this->db->where('mail.' . $user_type . '_id', $id);
        $this->db->where('mail.action', $action);
        $this->db->where('mail.status', '0');
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }
    function getUnreadCount($id)
    {
        $this->db->select('*');
        $this->db->from('mail');
        $this->db->where('reciever_id', $id);
        $this->db->where('status', '0');
        $this->db->where('action', '1');
        $query = $this->db->get();
        return $query->num_rows();
    }
    function getAffiliatedById($id)
    {
        $this->db->select('*');
        $this->db->from('affiliated');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    function checkSource($name)
    {
        $this->db->select('*');
        $this->db->from('affiliated');
        $this->db->where('name', $name);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function saveMessage($data)
    {
        $this->db->set($data);
        $this->db->insert('mail');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function updateStatus($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('mail', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteAffiliated($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('affiliated');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    function fetchEmailTemplatedata($id)
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function fetchallusers()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role', '2');
        $query = $this->db->get();
        return $query->result_array();
    }
    function insert_mail_data($data)
    {
        $this->db->set($data);
        $this->db->insert('email_tracking');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function getAllRecord()
    {
        $this->db->select('*');
        $this->db->from('email_tracking');
        $query = $this->db->get();
        return $query->result();
    }
    function fetchUserCount($user_id, $tracking_id)
    {
        $this->db->select('*');
        $this->db->from('email_tracking_records');
        $this->db->where('user_id', $user_id);
        $this->db->where('email_tracking_id', $tracking_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function fetchsubscriberDetail($id)
    {
        $this->db->select('id, name , email');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function fetch_record($id)
    {
        $this->db->select('id, name, email');
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}