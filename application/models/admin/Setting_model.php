<?php
class Setting_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function checkIfMarketFeeExists($market, $case_type)
    {
        $this->db->select('*');
        $this->db->from('market_fee');
        $this->db->where('market', $market);
        $this->db->where('case_type', $case_type);
        $result = $this->db->get();
        return $result->num_rows(); 
    }
    
    function fetchSettings()
    {
        $this->db->select('*');
        $this->db->from('settings');
        $this->db->where('id', '1');
        $query = $this->db->get();
        return $query->row();
    }
    function fetchAllAdmins()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role', '1');
        $query = $this->db->get();
        return $query->result();
    }
    function save_survey($data)
    {
        $this->db->where('id', '1');
        $this->db->update('settings', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function caseType()
    {
        $this->db->select('*');
        $this->db->from('case_type');
        $this->db->order_by("type", "asc");
        $query = $this->db->get();
        return $query->result();
    }
    function jurisdiction_n()
    {
        $this->db->select('*');
        $this->db->from('jurisdiction_n');
        $this->db->order_by("name", "asc");
        $query = $this->db->get();
        return $query->result();
    }
    function intakerRecords()
    {
        $this->db->select('*');
        $this->db->from('intaker');
        $this->db->order_by("name", "asc");
        $query = $this->db->get();
        return $query->result();
    }
    function sourceRecords()
    {
        $this->db->select('*');
        $this->db->from('source');
        $this->db->order_by("name", "asc");
        $query = $this->db->get();
        return $query->result();
    }
    function fetchAllStates()
    {
        $this->db->select('*');
        $this->db->from('jurisdiction_states');
        $query = $this->db->get();
        return $query->result();
    }
    function fetchAllJurisdiction()
    {
        $this->db->select('*');
        $this->db->from('jurisdiction_n');
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    function fetchAllMarkets()
    {
        $this->db->select('*');
        $this->db->from('market_n');
        $query = $this->db->get();
        return $query->result();
    }
    function fetchAllCaseType()
    {
        $this->db->select('*');
        $this->db->from('case_type');
        $this->db->order_by('type', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    function tagsType()
    {
        $this->db->select('*');
        $this->db->from('tags_labels');
        $query = $this->db->get();
        return $query->result();
    }
    function emailType()
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $query = $this->db->get();
        return $query->result();
    }
    function lawType()
    {
        $this->db->select('*');
        $this->db->from('law_firms');
        $this->db->order_by('law_firm', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    function caseTypeEdit()
    {
        $this->db->select('*');
        $this->db->from('case_type');
        $query = $this->db->get();
        return $query->result();
    }
    function saveCaseType($data)
    {
        $this->db->set($data);
        $this->db->insert('case_type');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveLawType($data)
    {
        $this->db->set($data);
        $this->db->insert('law_firms');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveTagType($data)
    {
        $this->db->set($data);
        $this->db->insert('tags_labels');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveJurisdiction_n($data)
    {
        $this->db->set($data);
        $this->db->insert('jurisdiction_n');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveIntaker($data)
    {
        $this->db->set($data);
        $this->db->insert('intaker');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveSource($data)
    {
        $this->db->set($data);
        $this->db->insert('source');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveJurisdiction($data)
    {
        $this->db->set($data);
        $this->db->insert('jurisdiction');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveMarket_n($data)
    {
        $this->db->set($data);
        $this->db->insert('market_n');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveMarket($data)
    {
        $this->db->set($data);
        $this->db->insert('market');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveJurisdictionFee($data)
    {
        $this->db->set($data);
        $this->db->insert('Jurisdiction_fee');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveMarketFee($data)
    {
        $this->db->set($data);
        $this->db->insert('market_fee');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function saveEmailTemplate($data)
    {
        $this->db->set($data);
        $this->db->insert('email_templates');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function fetchCaseType()
    {
        $this->db->select('*');
        $this->db->from('case_type');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchJurisdiction()
    {
        $this->db->select('*');
        $this->db->from('jurisdiction');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function marketRecords()
    {
        $this->db->select('*');
        $this->db->from('market_n');
        $this->db->order_by('market_name', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchMarket_n()
    {
        $this->db->select('*');
        $this->db->from('market_n');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchJurisdictionFee()
    {
        $this->db->select('*');
        $this->db->from('Jurisdiction_fee');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function fetchMarketFee()
    {
        $this->db->select('*');
        $this->db->from('market_fee');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    public function fetchRow($id)
    {
        $this->db->select('*');
        $this->db->from('case_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchJurisdiction_n_Row($id)
    {
        $this->db->select('*');
        $this->db->from('jurisdiction_n');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchIntakerRow($id)
    {
        $this->db->select('*');
        $this->db->from('intaker');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchSourceRow($id)
    {
        $this->db->select('*');
        $this->db->from('source');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function deleteLawRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('law_firms');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteTagRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tags_labels');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteMarket($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('market');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteMarket_n_Row($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('market_n');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteJurisdictionRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('jurisdiction');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteJurisdictionFeeRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('Jurisdiction_fee');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteMarketFeeRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('market_fee');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteTemplateRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('email_templates');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteCaseRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('case_type');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteJurisdiction_n_Row($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('jurisdiction_n');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteIntakerRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('intaker');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function deleteSourceRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('source');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
    public function fetchLawRow($id)
    {
        $this->db->select('*');
        $this->db->from('law_firms');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchTagRow($id)
    {
        $this->db->select('*');
        $this->db->from('tags_labels');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchMarketRow($id)
    {
        $this->db->select('*');
        $this->db->from('market');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchMarket_n_Row($id)
    {
        $this->db->select('*');
        $this->db->from('market_n');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchJurisdictionRow($id)
    {
        $this->db->select('*');
        $this->db->from('jurisdiction');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchJurisdictionFeeRow($id)
    {
        $this->db->select('*');
        $this->db->from('Jurisdiction_fee');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchMarketFeeRow($id)
    {
        $this->db->select('*');
        $this->db->from('market_fee');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function fetchTemplateRow($id)
    {
        $this->db->select('*');
        $this->db->from('email_templates');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    function caseTypeRow($id)
    {
        $this->db->select('*');
        $this->db->from('case_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
    }
    function updateMarket_n($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('market_n', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function updateMarket($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('market', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function updateJurisdiction_n_Row($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('jurisdiction_n', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function updateIntakerRow($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('intaker', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function updateSourceRow($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('source', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function updateJurisdictionFee($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('Jurisdiction_fee', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function updateMarketFee($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('market_fee', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function updateJurisdiction($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('jurisdiction', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
	public function login_users()
	{
		$SQL = "SELECT * from (SELECT `users`.`last_login_date` as last_login_date ,`users`.`email` as email , `users`.`name` as first_name, `users`.`lname` as last_name, `users`.`ip_address` as ip_address, 'Lawyer' as account_type FROM `users` where role = 2
		UNION ALL
		SELECT `users`.`last_login_date` as last_login_date ,`users`.`email` as email , `users`.`name` as first_name, `users`.`lname` as last_name, `users`.`ip_address` as ip_address, 'Admin' as account_type FROM `users` where role = 1
		UNION ALL
		SELECT `law_firms`.`last_login_date` as last_login_date ,`law_firms`.`contact_email` as email , `law_firms`.`contact_name` as first_name, `law_firms`.`contact_last_name` as last_name, `law_firms`.`ip_address` as ip_address, 'Law Firm' as account_type FROM `law_firms`
		) table1 order by last_login_date DESC";
		$query = $this->db->query($SQL);
        return $query->result();
	}
    
    public function get_maintenance_data($id = '', $return_type = '')
    {
        $this->db->select('*');
        $this->db->from('maintenance_mode');
        if($id != ''){
            $this->db->where('id', $id);
        }
        $this->db->order_by('id', 'ASC');
        $result = $this->db->get();
        return ( $return_type == '' ? $result->row() : $result->result_array() );
    }
    
    public function update_maintenance_data($data, $id = '')
    {
        if($id != ''){
            $this->db->where('id', $id);
        }
        $this->db->update('maintenance_mode', $data);
    }
    
    public function remove_maintenance_data($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('maintenance_mode');
    }
    
    public function save_maintenance_data($data)
    {
        $this->db->insert('maintenance_mode', $data);
    }
}