<?php

class Forgot_password_model extends CI_Model {

    function _construct() {
// Call the Model constructor
        parent::_construct();
    }
	
	function fetchByCode($unique_code) {
		$this->db->select('*');
        $this->db->from('users');
		$this->db->where('unique_code',$unique_code);
        
		$query = $this->db->get();

        if ($query->num_rows() > 0) {
			 return $query->row();
		} else {
			return false;
		}
    }

	function unique_str($data, $email) {
		$this->db->where('email', $email);
		$this->db->update('users', $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
    }
	
	function update($data, $unique_code) {
		$this->db->where('unique_code', $unique_code);
		$this->db->update('users', $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		} else {
			return false;
		}
    }

	
}//end