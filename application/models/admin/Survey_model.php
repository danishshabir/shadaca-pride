<?php
class Survey_model extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }

 
    function save_survey($data) 
    {
		
        $this->db->set($data);

        $this->db->insert('question_survey_lawyer');

        $insertId = $this->db->insert_id();

        if ($insertId > 0) 
        {

            return $insertId;

        } else 
        {

            return false;

        }

    }


    //save options 

    function save_survey_options($data2) 
    {
        
        $this->db->set($data2);

        $this->db->insert('answer_options');

        $insertId = $this->db->insert_id();

        if ($insertId > 0) 
        {

            return $insertId;

        } else 
        {

            return false;

        }

    }
//Fetch Question

    function get_question() 
    {
        
       

        $this->db->select('*');
         $this->db->from('question_survey_lawyer');
		 $this->db->order_by('question_id','desc');
         $query=$this->db->get();

        $insertId = $this->db->insert_id();

        return $query->result_array();

    }

    
    function get_options_answer($q_id) 
    {
        
       

        $this->db->select('*');
        $this->db->from('answer_options');
        $this->db->where('question_id',$q_id);

         $query=$this->db->get();

        return $query->result_array();

    }


     function add_question_answer($qdata) 
    {
        
       
        $this->db->set($qdata);

        $this->db->insert('question_answer');
      

    }
       function add_option_answer($q_id,$optn_id,$u_id) 
    {
        $qdata['question_id']=$q_id;
        $qdata['answer_options_id']=$optn_id;
        $qdata['user_id']=$u_id;

       
        $this->db->set($qdata);

        $this->db->insert('anser_for_checkboxoptions');
      

    }
    function get_all_questions()
    {
        $this->db->select("*");
        $this->db->from("");
        $get_questions=$this->db->get();
        return $get_questions->result_array();
    }

    public function deleteRow($id) {



       $this->db->where('question_id', $id);
	   
	   $this->db->delete('question_survey_lawyer');

        if ($this->db->affected_rows() > 0) {

            return TRUE;

        } else {

            return false;

        }

    }
	public function deleteAnswers($id) 
    {



       $this->db->where('question_id', $id);
	   
	   $this->db->delete('question_answer');

        if ($this->db->affected_rows() > 0) {

            return TRUE;

        } else {

            return false;

        }

    }
    public function get_answers_questions() 
    {

        $this->db->select('*');
         $this->db->from('question_answer');
         
         $query=$this->db->get();

        $insertId = $this->db->insert_id();

        return $query->result_array();


    }
//Get single question
    function get_single_question($id) 
    {
        
       

        $this->db->select('*');
         $this->db->from('question_survey_lawyer');
         $this->db->where('question_id',$id);
         $query=$this->db->get();
        return $query->row();

    }
    function check_answered($question_id,$user_id) 
    {
        $this->db->select('*');
        $this->db->from('question_answer');
        $this->db->where('question_id',$question_id);
        $this->db->where('user_id',$user_id);
        $query=$this->db->get();
        return $query->row();
    }
    function get_user_answers($user_id) 
    {
        $this->db->select('*');
        $this->db->from('question_answer');
        $this->db->where('user_id',$user_id);
        $query=$this->db->get();
        return $query->result_array();
    }
    
	    function get_checkbox_answer($id) 
    {
        $this->db->select('*');
        $this->db->from('answer_options');
        $this->db->where('id',$id);
        $query=$this->db->get();
        return $query->row();
    }
	 
    
}