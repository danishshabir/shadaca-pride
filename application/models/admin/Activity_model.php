<?php
class Activity_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function fetchAllLeads()
    {
        $this->db->select('*');
        $this->db->from('activities');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }	
	
	/* New Function */		
	public function fetchAll($lead_id, $lawyer_id = null)
    {
		$this->db->select('a.*, u1.name as lawyer_first_name, u1.lname as lawyer_last_name, u2.name as staff_first_name, u2.lname as staff_last_name,u3.name as admin_first_name, u3.lname as admin_last_name, lf.law_firm');
		$this->db->from('activities a');
		$this->db->join('users u1', 'u1.id = a.lawyer_id', 'LEFT OUTER');
		$this->db->join('users u2', 'u2.id = a.lawyer_staff', 'LEFT OUTER');
		$this->db->join('users u3', 'u3.id = a.admin_id', 'LEFT OUTER');
		$this->db->join('law_firms lf', 'lf.id = a.lawfirm_id', 'LEFT OUTER');
		$this->db->where('a.lead_id', $lead_id);
        if($this->session->role != 1) {
            if($lawyer_id) $this->db->where('a.lawyer_id', $lawyer_id);
        }
        $query = $this->db->get();
        //echo $this->db->last_query(); exit();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }		
    /* public function fetchAll($lead_id, $lawyer_id = NUll)
    {
        $this->db->select('activities.*, users.name');
        $this->db->from('activities');
        $this->db->join('users', 'users.id = activities.lawyer_id', 'LEFT OUTER');
        $this->db->where('activities.lead_id', $lead_id);
        if($lawyer_id) $this->db->where('activities.lawyer_id', $lawyer_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    } */
    function save($data)
    {
        $this->db->set($data);
        $this->db->insert('activities');
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {
            return $insertId;
        } else {
            return false;
        }
    }
    function update($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('activities', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchRow($id)
    {
        $this->db->select('*');
        $this->db->from('activities');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    public function deleteRow($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('activities');
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        } else {
            return false;
        }
    }
}