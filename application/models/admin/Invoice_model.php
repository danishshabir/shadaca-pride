<?php

class Invoice_model extends CI_Model

{

    function __construct()

    {

        parent::__construct();

    }

    function fetchLeadsPaymentBillingQuantity($id, $jur_id)

    {

        $this->db->select('leads.*, Jurisdiction_fee.*');

        $this->db->from('Jurisdiction_fee');

        $this->db->join('leads', 'leads.jurisdiction = Jurisdiction_fee.Jurisdiction ', 'left outer');

        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ', 'left outer');

        $this->db->where('lawyers_to_leads.lawyer_id', $id);

        $this->db->where('leads.jurisdiction', $jur_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->num_rows();

        } else {

            return false;

        }

    }

    function fetchLeadsPaymentBilling($id)

    {

        $this->db->select('leads.*, market_fee.*');

        $this->db->group_by('leads.market');

        $this->db->from('market_fee');

        $this->db->join('leads', 'leads.market = market_fee.market ', 'left outer');

        $this->db->join('lawyers_to_leads', 'leads.id = lawyers_to_leads.lead_id ', 'left outer');

        $this->db->where('lawyers_to_leads.lawyer_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function fetchLawyerDetails($id)

    {

        $this->db->select('*');

        $this->db->from('users');

        $this->db->where('id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->row();

        } else {

            return false;

        }

    }

    function fetchHeaderDetails($id)

    {

        $this->db->select('*');

        $this->db->from('settings');

        $this->db->where('id', 1);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->row();

        } else {

            return false;

        }

    }

    function fetchInvoiceDueDate($id)

    {

        $this->db->select('*');

        $this->db->from('settings');

        $this->db->where('id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->row();

        } else {

            return false;

        }

    }

    function fetchLawyerLawFirmDetails($id)

    {

        $this->db->select('*');

        $this->db->from('law_firms');

        $this->db->where('id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->row();

        } else {

            return false;

        }

    }

    function transactions($data)

    {

		if($data['date_from'] && $data['date_to']) {

			$where_date = "AND Date(transaction_date) BETWEEN date('".$data['date_from']."') AND date('".$data['date_to']."')";

		} else {

		      // - INTERVAL 1 MONTH

			$where_date = 'AND YEAR(transaction_date) = YEAR(CURRENT_DATE) AND MONTH(transaction_date) = MONTH(CURRENT_DATE)';

		}

		if($data['charges'] == 1) {

			$SQL = "select * from (SELECT `leads`.`price` as amount ,  CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`billed` = 'yes'

			UNION ALL

			SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment`

			UNION ALL

			SELECT `amount` as amount , description as description,  transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1

			UNION ALL

			SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 2) {

			$SQL = "select * from (SELECT `amount` as amount , CONCAT('Credit/debit payment detail (Discover (', credit_debit, '))') as description, transaction_date , lawyer_id as lawyer_id, 'Payment' as status FROM `credit_card_transactions` WHERE `status` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 4) {

			$SQL = "select * from (SELECT `leads`.`price` as amount ,  CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`billed` = 'yes'

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 5) {

			$SQL = "select * from (SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment`

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 6) {

			$SQL = "select * from (SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} else {

			$SQL = "select * from (SELECT `leads`.`price` as amount ,  CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`billed` = 'yes'

			UNION ALL

			SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment`

			UNION ALL

			SELECT `amount` as amount , description as description,  transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1

			UNION ALL

			SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2

			UNION ALL

			SELECT `amount` as amount , CONCAT('Credit/debit payment detail (Discover (', credit_debit, '))') as description, transaction_date , lawyer_id as lawyer_id, 'Payment' as status FROM `credit_card_transactions` WHERE `status` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		}

		$query = $this->db->query($SQL);

        return $query->result();

    }

    function fetchLawyerLastMonthlyBill($id)

    {

        $SQL   = "SELECT `monthly_payment`.`monthly_bill` as monthly_bill , count(*) as quantity FROM monthly_payment  WHERE `monthly_payment`.`lawyer_id` = '" . $id . "' order by id Limit 1";

        $query = $this->db->query($SQL);

        return $query->result();

    }

    

    /**

     * Invoice_model::fetchAllStatements()

     * 

     * @param string $lawyer_id

     * @return

     */

    public function fetchAllStatements($lawyer_id = '' , $past_statement=false)

    {

        if ($past_statement)
        {
            $past_where = 'ms.month != MONTHNAME(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))';
        } else {
            $past_where = 'ms.month = MONTHNAME(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))';
        }

        $this->db->select('ms.*, CONCAT(u.name, " ", u.lname) AS lawyer_name, lf.law_firm AS law_firm_name');

        $this->db->from('monthly_statements as ms');

        $this->db->join('users as u', 'ms.lawyer_id = u.id', 'left');

        $this->db->join('law_firms as lf', 'u.law_firms = lf.id', 'left');

        if($lawyer_id){
            $this->db->where('ms.lawyer_id', $lawyer_id);
        } 

        $this->db->where($past_where);
        $this->db->order_by("ms.created_at", "DESC");
        $result = $this->db->get();

        return $result->result();

    }

    

    /**

     * Invoice_model::fetchStatementsByLawfirm()

     * 

     * @param mixed $lawfirm_id

     * @param string $lawyer_id

     * @return

     */

    public function fetchStatementsByLawfirm($lawfirm_id, $lawyer_id = '')

    {
       
        $this->db->select('ms.*, CONCAT(u.name, " ", u.lname) AS lawyer_name, lf.law_firm AS law_firm_name');

        $this->db->from('monthly_statements as ms');

        $this->db->join('users as u', 'ms.lawyer_id = u.id', 'left');

        $this->db->join('law_firms as lf', 'u.law_firms = lf.id', 'left');

        $this->db->where('u.law_firms', $lawfirm_id);

        $this->db->where('ms.month = MONTHNAME(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))');

        if($lawyer_id) $this->db->where('ms.lawyer_id', $lawyer_id);
       
        $result = $this->db->get();

       
        return $result->result();

    }

    

    /**

     * Invoice_model::fetchStatementsByLawyer()

     * 

     * @param mixed $lawyer_id

     * @return

     */

    public function fetchStatementsByLawyer($lawyer_id, $past_statement = false)

    {

        if ($past_statement)
        {
            $past_where = 'ms.month != MONTHNAME(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))';
        } else {
            $past_where = 'ms.month = MONTHNAME(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))';
        }

        return $this->db->select('ms.*, CONCAT(u.name, " ", u.lname) AS lawyer_name, lf.law_firm AS law_firm_name')

            ->from('monthly_statements as ms')

            ->join('users as u', 'ms.lawyer_id = u.id', 'left')

            ->join('law_firms as lf', 'u.law_firms = lf.id', 'left')

            ->where('u.id', $lawyer_id)

            ->Where( $past_where )

            ->order_by("ms.created_at", "DESC")
            
            ->get()

            ->result();

    }

    

    

    /**

     * Invoice_model::fetchStatementsByLawyerStaff()

     * 

     * @param mixed $lawyer_staff_id

     * @return

     */

    public function fetchStatementsByLawyerStaff($lawyer_staff_id)

    {

        return $this->db->select('ms.*, CONCAT(u.name, " ", u.lname) AS lawyer_name, lf.law_firm AS law_firm_name')

            ->from('monthly_statements as ms')

            ->join('users as u', 'ms.lawyer_id = u.id', 'left')

            ->join('law_firms as lf', 'u.law_firms = lf.id', 'left')

            ->join('lawyers_lawstaff as ll', 'u.id = ll.lawyers_id', 'left')

            ->where('ll.lawyerstaff_id', $lawyer_staff_id)

            ->get()

            ->result();

    }

}

