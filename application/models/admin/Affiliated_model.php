<?php
class Affiliated_model extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }

 	function getAllAffiliated() 
    {
             

        $this->db->select('*');
        $this->db->from('affiliated');

        $query=$this->db->get();

        return $query->result();

    }
	
	function getAffiliatedById($id) 
    {
             

        $this->db->select('*');
        $this->db->from('affiliated');
		$this->db->where('id', $id);

        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return $query->row();
		}
		else
		{
			return false;
		}

        

    }
	
	function checkSource($name) 
    {
             

        $this->db->select('*');
        $this->db->from('affiliated');
		$this->db->where('name', $name);

        $query=$this->db->get();
		
		if($query->num_rows() > 0)
		{
        	return true;
		}
		else
		{
			return false;
		}
    }
	
	function saveAffiliated($data) {
		
        $this->db->set($data);

        $this->db->insert('affiliated');

        $insertId = $this->db->insert_id();

        if ($insertId > 0) {

            return $insertId;

        } else {

            return false;

        }

    }
 
    function updateAffiliated($data,$id) 
    {
		
        $this->db->where('id', $id);
	   
	   $this->db->update('affiliated', $data);

        if ($this->db->affected_rows() > 0) {

            return true;

        } else {

            return false;

        }


	}
	
	 public function deleteAffiliated($id) 
    {



       $this->db->where('id', $id);
	   
	   $this->db->delete('affiliated');

        if ($this->db->affected_rows() > 0) {

            return TRUE;

        } else {

            return false;

        }

    }
    
}