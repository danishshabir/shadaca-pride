<?php

class Reporting_model extends CI_Model

{

    function __construct()

    {

        parent::__construct();

    }

    function fetchLawyerdetail($id)

    {

        $this->db->select('*');

        $this->db->from('users');

        $this->db->where('id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->row();

        } else {

            return false;

        }

    }

    function fetchLawyers()

    {

        $this->db->select('*');

        $this->db->from('users');

        $this->db->where('role', 2);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function fetchAllLawyerTransactions($id)

    {

        $this->db->select('*');

        $this->db->from('credit_card_transactions');

        $this->db->where('lawyer_id', $id);

        $this->db->order_by('transaction_date', 'ASC');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function fetchLawyerTrasanctionsOnDateRange($id, $date_from, $date_to)

    {

        $this->db->select('*');

        $this->db->from('credit_card_transactions');

        $this->db->where('lawyer_id', $id);

        $this->db->where('transaction_date >=', date('Y-m-d H:i:s', strtotime($date_from)));

        $this->db->where('transaction_date <=', date('Y-m-d H:i:s', strtotime($date_to)));

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function checkLawyerRefrenceId($id)

    {

        $this->db->select('refrence_id');

        $this->db->from('users');

        $this->db->where('id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->row();

        } else {

            return false;

        }

    }

    public function updateLawyerRefrenceId($id, $dataNew)

    {

        $this->db->where('id', $id);

        $this->db->update('users', $dataNew);

        if ($this->db->affected_rows() > 0) {

            return true;

        } else {

            return false;

        }

    }

    function insertTransactionRecord($data)

    {

        $this->db->insert('credit_card_transactions', $data);

        $insertId = $this->db->insert_id();

        if ($insertId > 0) {

            return $insertId;

        } else {

            return false;

        }

    }

    function fetchLawyerMonthlyBill($id)

    {

        $this->db->select('*');

        $this->db->from('monthly_payment');

        $this->db->where('lawyer_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function fetchLawyerPaidTransactions($id)

    {

        $this->db->select('sum(amount) as total');

        $this->db->from('credit_card_transactions');

        $this->db->where('lawyer_id', $id);

		$this->db->where('status', 2);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function fetchLawyerAllLeads($id)

    {

        $this->db->select('leads.* ,users.name ,users.lname ,lawyers_to_leads.*');

        $this->db->from('lawyers_to_leads');

        $this->db->join('leads', 'leads.id = lawyers_to_leads.lead_id ', 'left outer');

        $this->db->join('users', 'users.id = lawyers_to_leads.lawyer_id ', 'left outer');

        $this->db->where('lawyers_to_leads.lawyer_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function fetchPeriodicLeads($id, $date_from, $date_to)

    {

        $this->db->select('leads.* ,users.name ,users.lname ,lawyers_to_leads.*');

        $this->db->from('lawyers_to_leads');

        $this->db->join('leads', 'leads.id = lawyers_to_leads.lead_id ', 'left outer');

        $this->db->join('users', 'users.id = lawyers_to_leads.lawyer_id ', 'left outer');

        $this->db->where('lawyers_to_leads.lawyer_id', $id);

        $this->db->where('lawyers_to_leads.created_at >=', date('Y-m-d H:i:s', strtotime($date_from)));

        $this->db->where('lawyers_to_leads.created_at <=', date('Y-m-d H:i:s', strtotime($date_to)));

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function fetchLawyersOnSearch($name, $law_firms)

    {

        $this->db->select('*');

        $this->db->from('users');

        $this->db->like('name', $name, 'after');

        if ($law_firms != '0') {

            $this->db->where('law_firms', $law_firms);

        }

        $this->db->where('role', 2);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function search_transactions($data)

    {
        
        $lawyer_id = isset($data['lawyer_id']) ? $data['lawyer_id'] : '';

		if($data['date_from'] && $data['date_to']) {

			$where_date = "AND Date(transaction_date) BETWEEN date('".$data['date_from']."') AND date('".$data['date_to']."')";

		} else {

			$where_date = 'AND Date(transaction_date) BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()';

		}
        
        /* added by hassan */
        if($data['date_from'] && $data['date_to']) {

			$where_date_single = "date('".$data['date_from']."') AND date('".$data['date_to']."')";

		} else {

			$where_date_single = 'CURDATE() - INTERVAL 30 DAY AND SYSDATE()';

		}

		if($data['charges'] == 1) {

			$SQL = "select * from (SELECT `leads`.`price` as amount ,  CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` AND `lawyers_to_leads`.`billed` = 'yes'

			UNION ALL

			SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment`

			UNION ALL

			SELECT `amount` as amount , description as description,  transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1

			UNION ALL

			SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 2) {

			$SQL = "select * from (SELECT `amount` as amount , CONCAT('Credit/debit payment detail (Discover (', credit_debit, '))') as description, transaction_date , lawyer_id as lawyer_id, 'Payment' as status FROM `credit_card_transactions` WHERE `status` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 4) {

			$SQL = "select * from (SELECT `leads`.`price` as amount ,  CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` AND `lawyers_to_leads`.`billed` = 'yes'

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 5) {

			$SQL = "select * from (SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment`

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 6) {

			$SQL = "select * from (SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} else {
		  
            /*$SQL = '';
            
            $SQL .= "SELECT * FROM(";
            
            $SQL .= "SELECT `leads`.`price` as amount , CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` AND `lawyers_to_leads`.`billed` = 'yes' WHERE lawyers_to_leads.assigned_date != '' ".($lawyer_id ? "and lawyers_to_leads.lawyer_id = '{$lawyer_id}'" : ""). " and Date(lawyers_to_leads.assigned_date) BETWEEN {$where_date_single}";
            
            $SQL .= " UNION ALL ";
            
            $SQL .= "SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment` WHERE monthly_payment.created_at != '' ".($lawyer_id ? "and monthly_payment.lawyer_id = '{$lawyer_id}'" : "")." and Date(monthly_payment.created_at) BETWEEN {$where_date_single}";
            
            $SQL .= " UNION ALL ";
            
            $SQL .= "SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1  and credit_card_transactions.transaction_date != '' ".($lawyer_id ? "and credit_card_transactions.lawyer_id = '{$lawyer_id}'" : "")." and Date(credit_card_transactions.transaction_date) BETWEEN {$where_date_single}";
            
            $SQL .= " UNION ALL ";
            
            $SQL .= "SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2  and credit_card_transactions.transaction_date != '' ".($lawyer_id ? "and credit_card_transactions.lawyer_id = '{$lawyer_id}'" : "")." and Date(credit_card_transactions.transaction_date) BETWEEN {$where_date_single}";
            
            $SQL .= ")";
            
            $SQL .= " table1 ";
            
            $SQL .= " ORDER BY transaction_date DESC";*/
            
            $SQL = "select * from (SELECT `leads`.`price` as amount ,  CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` WHERE `lawyers_to_leads`.`billed` = 'yes'
			UNION ALL
			SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment`
			UNION ALL
			SELECT `amount` as amount , description as description,  transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1
			UNION ALL
			SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2
			UNION ALL
			SELECT `amount` as amount , CONCAT('Credit/debit payment detail (Discover (', credit_debit, '))') as description, transaction_date , lawyer_id as lawyer_id, 'Payment' as status FROM `credit_card_transactions` WHERE `status` = 2
			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

			/**
            $SQL = "select * from (SELECT `leads`.`price` as amount ,  CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` AND `lawyers_to_leads`.`billed` = 'yes'

			UNION ALL

			SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment`

			UNION ALL

			SELECT `amount` as amount , description as description,  transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1

			UNION ALL

			SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";
            */

		}

		$query = $this->db->query($SQL);
        
        return $query->result();

    }
    
    /**
     * Reporting_model::search_transactions_cronjob()
     * 
     * @param mixed $data
     * @return
     */
    function search_transactions_cronjob($data)

    {
        
        $lawyer_id = isset($data['lawyer_id']) ? $data['lawyer_id'] : '';

		if($data['date_from'] && $data['date_to']) {

			$where_date = "AND Date(transaction_date) BETWEEN date('".$data['date_from']."') AND date('".$data['date_to']."')";

		} else {

			$where_date = 'AND Date(transaction_date) BETWEEN CURDATE() - INTERVAL 30 DAY AND SYSDATE()';

		}
        
        /* added by hassan */
        if($data['date_from'] && $data['date_to']) {

			$where_date_single = "date('".$data['date_from']."') AND date('".$data['date_to']."')";

		} else {

			$where_date_single = 'CURDATE() - INTERVAL 30 DAY AND SYSDATE()';

		}

		if($data['charges'] == 1) {

			$SQL = "select * from (SELECT `leads`.`price` as amount ,  CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` AND `lawyers_to_leads`.`billed` = 'yes'

			UNION ALL

			SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment`

			UNION ALL

			SELECT `amount` as amount , description as description,  transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1

			UNION ALL

			SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 2) {

			$SQL = "select * from (SELECT `amount` as amount , CONCAT('Credit/debit payment detail (Discover (', credit_debit, '))') as description, transaction_date , lawyer_id as lawyer_id, 'Payment' as status FROM `credit_card_transactions` WHERE `status` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 4) {

			$SQL = "select * from (SELECT `leads`.`price` as amount ,  CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` AND `lawyers_to_leads`.`billed` = 'yes'

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 5) {

			$SQL = "select * from (SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment`

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} elseif($data['charges'] == 6) {

			$SQL = "select * from (SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";

		} else {
		  
            $SQL = '';
            
            $SQL .= "SELECT * FROM(";
            
            $SQL .= "SELECT `leads`.`price` as amount , CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` AND `lawyers_to_leads`.`billed` = 'yes' WHERE lawyers_to_leads.assigned_date != '' ".($lawyer_id ? "and lawyers_to_leads.lawyer_id = '{$lawyer_id}'" : ""). " and Date(lawyers_to_leads.assigned_date) BETWEEN {$where_date_single}";
            
            $SQL .= " UNION ALL ";
            
            $SQL .= "SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment` WHERE monthly_payment.created_at != '' ".($lawyer_id ? "and monthly_payment.lawyer_id = '{$lawyer_id}'" : "")." and Date(monthly_payment.created_at) BETWEEN {$where_date_single}";
            
            $SQL .= " UNION ALL ";
            
            $SQL .= "SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, description as status FROM `credit_card_transactions` WHERE `bill_type` = 1  and credit_card_transactions.transaction_date != '' ".($lawyer_id ? "and credit_card_transactions.lawyer_id = '{$lawyer_id}'" : "")." and Date(credit_card_transactions.transaction_date) BETWEEN {$where_date_single}";
            
            $SQL .= " UNION ALL ";
            
            $SQL .= "SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Payment' as status FROM `credit_card_transactions` WHERE `bill_type` = 2  and credit_card_transactions.transaction_date != '' ".($lawyer_id ? "and credit_card_transactions.lawyer_id = '{$lawyer_id}'" : "")." and Date(credit_card_transactions.transaction_date) BETWEEN {$where_date_single}";
            
            $SQL .= " UNION ALL ";
            
            $SQL .= "SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Initial setup charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 3  and credit_card_transactions.transaction_date != '' ".($lawyer_id ? "and credit_card_transactions.lawyer_id = '{$lawyer_id}'" : "")." and Date(credit_card_transactions.transaction_date) BETWEEN {$where_date_single}";
            
            $SQL .= ")";
            
            $SQL .= " table1 ";
            
            $SQL .= " ORDER BY transaction_date DESC";

			/**
            $SQL = "select * from (SELECT `leads`.`price` as amount ,  CONCAT('Lead Case ID# (', leads.lead_id_number, ')') as description ,`lawyers_to_leads`.`assigned_date` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id, 'Bill' as status FROM `lawyers_to_leads` JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id` AND `lawyers_to_leads`.`billed` = 'yes'

			UNION ALL

			SELECT monthly_bill as amount , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `monthly_payment`

			UNION ALL

			SELECT `amount` as amount , description as description,  transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 1

			UNION ALL

			SELECT `amount` as amount , description as description, transaction_date , lawyer_id as lawyer_id, 'Charge' as status FROM `credit_card_transactions` WHERE `bill_type` = 2

			) table1 WHERE transaction_date != '' AND lawyer_id = ".$data['lawyer_id']." ".$where_date." order by transaction_date DESC";
            */

		}

		$query = $this->db->query($SQL);

        return $query->result();

    }

    function fetchLawyerAllLeadsPrice($id)

    {

        $this->db->select('sum(price) as total');

        $this->db->from('lawyers_to_leads');

        $this->db->join('leads', 'leads.id = lawyers_to_leads.lead_id ', 'left outer');

        $this->db->join('users', 'users.id = lawyers_to_leads.lawyer_id ', 'left outer');

        $this->db->where('lawyers_to_leads.lawyer_id', $id);

        $this->db->where('lawyers_to_leads.billed', "yes");

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function fetchLawyerMonthlyBillTotal($id)

    {

        $this->db->select('sum(monthly_bill) as total');

        $this->db->from('monthly_payment');

        $this->db->where('lawyer_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function fetchLawyerAllPayments($id)

    {

        $this->db->select('sum(amount) as total');

        $this->db->from('credit_card_transactions');

        $this->db->where('lawyer_id', $id);

		$this->db->where('status', 2);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

    function total_charge_bill($id)

    {

        $this->db->select('sum(amount) as total');

        $this->db->from('credit_card_transactions');

        $this->db->where('lawyer_id', $id);

        $this->db->where('status', 1);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();

        } else {

            return false;

        }

    }

	public function export_transactions($date_from, $date_to, $charges)

	{

		if ($charges == 'C' && $date_from && $date_to) {

            $SQL = "select * from (SELECT amount as lead_bill , 'Payment' as description, transaction_date , lawyer_id as lawyer_id FROM `credit_card_transactions` WHERE `status` = 2) table1  WHERE transaction_date BETWEEN '$date_from' AND '$date_to' order by transaction_date DESC";

        }

        if ($charges == 'C' && (!$date_from || !$date_to)) {

            $SQL = "select * from (SELECT amount as lead_bill , 'Payment' as description, transaction_date , lawyer_id as lawyer_id FROM `credit_card_transactions` WHERE `status` = 2) table1  WHERE transaction_date != '' order by transaction_date DESC";

        } else if ($charges == 'B' && $date_from && $date_to) {

            $SQL = "select * from (SELECT `leads`.`price` as lead_bill ,  'Lead bill' as description ,`leads`.`created_at` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id`

			UNION ALL

			SELECT monthly_bill as monthly_bill , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id FROM `monthly_payment`

			UNION ALL

			SELECT sum(`amount`) as intial_amount , 'Initial setup charge' as description,  transaction_date , lawyer_id as lawyer_id FROM `credit_card_transactions`  WHERE `status` = 3

			UNION ALL

			SELECT sum(`amount`) as one_time_amount , 'One time charge' as description, transaction_date, lawyer_id as lawyer_id FROM `credit_card_transactions` WHERE `status` = 4

			UNION ALL

			SELECT amount as payment_amount , 'Payment' as description, transaction_date , lawyer_id as lawyer_id FROM `credit_card_transactions` WHERE `status` = 2) table1 WHERE transaction_date BETWEEN '$date_from' AND '$date_to' order by transaction_date DESC";

        } else if ($charges == 'B' && (!$date_from || !$date_to)) {

            $SQL = "select * from (SELECT `leads`.`price` as lead_bill ,  'Lead bill' as description ,`leads`.`created_at` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id`

			UNION ALL

			SELECT monthly_bill as monthly_bill , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id FROM `monthly_payment`

			UNION ALL

			SELECT sum(`amount`) as intial_amount , 'Initial setup charge' as description,  transaction_date , lawyer_id as lawyer_id FROM `credit_card_transactions`  WHERE `status` = 3

			UNION ALL

			SELECT sum(`amount`) as one_time_amount , 'One time charge' as description, transaction_date, lawyer_id as lawyer_id FROM `credit_card_transactions` WHERE `status` = 4

			UNION ALL

			SELECT amount as payment_amount , 'Payment' as description, transaction_date , lawyer_id as lawyer_id FROM `credit_card_transactions` WHERE `status` = 2) table1 WHERE transaction_date != '' order by transaction_date DESC";

        } else if ($charges == 'D' && $date_from && $date_to) {

            $SQL = "select * from (SELECT `leads`.`price` as lead_bill ,  'Lead bill' as description ,`leads`.`created_at` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id`

			UNION ALL

			SELECT monthly_bill as monthly_bill , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id FROM `monthly_payment`

			UNION ALL

			SELECT sum(`amount`) as intial_amount , 'Initial setup charge' as description,  transaction_date , lawyer_id as lawyer_id FROM `credit_card_transactions` WHERE `status` = 3

			UNION ALL

			SELECT sum(`amount`) as one_time_amount , 'One time charge' as description, transaction_date , lawyer_id as lawyer_id FROM `credit_card_transactions` WHERE `status` = 4

			) table1 WHERE transaction_date BETWEEN '$date_from' AND '$date_to' order by transaction_date DESC";

        } else if ($charges == 'D' && (!$date_from || !$date_to)) {

            $SQL = "select * from (SELECT `leads`.`price` as lead_bill ,  'Lead bill' as description ,`leads`.`created_at` as transaction_date , `lawyers_to_leads`.`lawyer_id` as lawyer_id FROM `lawyers_to_leads` LEFT OUTER JOIN `leads` ON `leads`.`id` = `lawyers_to_leads`.`lead_id` LEFT OUTER JOIN `users` ON `users`.`id` = `lawyers_to_leads`.`lawyer_id`

			UNION ALL

			SELECT monthly_bill as monthly_bill , 'Monthly bill' as description , created_at as transaction_date , lawyer_id as lawyer_id FROM `monthly_payment`

			UNION ALL

			SELECT sum(`amount`) as intial_amount , 'Initial setup charge' as description,  transaction_date , lawyer_id as lawyer_id FROM `credit_card_transactions` WHERE `status` = 3

			UNION ALL

			SELECT sum(`amount`) as one_time_amount , 'One time charge' as description, transaction_date , lawyer_id as lawyer_id FROM `credit_card_transactions` WHERE `status` = 4

			) table1 WHERE transaction_date != '' order by transaction_date DESC";

        }

        return $query = $this->db->query($SQL);

	}

}

