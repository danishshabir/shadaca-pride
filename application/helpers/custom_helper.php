<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function formatNumber($number)
{
    $formatted = number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $number)), 2);
    return $number < 0 ? "({$formatted})" : "{$formatted}";
}
function fetchLawyerIdByLeadId($lead_id){
    $CI =& get_instance();
    $row = $CI->Central_model->first("lawyers_to_leads", "lead_id", $lead_id);
    return $row->lawyer_id;
}
function check_status_session($session_value, $search_type){
    $CI =& get_instance();
    if($CI->session->has_userdata("last_status_session") && $CI->session->userdata("lead_status_session") !== "" && $CI->session->userdata("last_status_session") == $session_value && $CI->session->userdata("searchType") == $search_type){
        $CI->session->unset_userdata("last_status_session");
        return "selected=''";
    }else{
        return "";
    }
}
function getLawfirmLawyers($law_firm_id){
    $CI =& get_instance();
    $CI->load->model('admin/Users_model');
    $data = $CI->Users_model->getLawfirmLawyers($law_firm_id);
    $html = "";
    foreach($data as $lawyer){
      $html .= "<option id='fee' value=".$lawyer['id'].">".$lawyer['name']." ".$lawyer['lname']."</option>";
  }
  if(count($data) > 1){
      $html .= "<option value='0'>Select All</option>";
  }
  return $html;
}

function write($str, $stop = true){
    echo $str ? $str . '<br />' : ''; if($stop)exit;
}

function getDeclinedReason($lead_id, $lawyer_id)
{
    $CI = &get_instance();
    #echo "select reason from lead_contact_attempts where lead_id = '$lead_id' and lawyer_id = '$lawyer_id' and action = 'Declined'"; exit();
    $result = $CI->db->query("select reason from lead_contact_attempts where lead_id = '$lead_id' and lawyer_id = '$lawyer_id' and action = 'Declined'")->
    row();
    #echo $CI->db->last_query(); exit();
    if (!empty($result)) {
        return $result;
    } else {
        return '';
    }
}
function debug($array)
{
    echo '<pre>';
    print_r($array);
    exit();
}
function last_query($stop = true)
{
    $CI =& get_instance();
    echo $CI->db->last_query() . '<br />';
    if($stop)exit();
}
function chechUserSession()
{
    $CI =& get_Instance();
    if ($CI->session->userdata('logged_in') == TRUE) {
        return true;
    } //$CI->session->userdata('logged_in') == TRUE
    else {
        redirect($CI->config->item('base_url') . 'admin/login');
    }
}
function getLawyerTotalBalance($id)
{
    $data = array();
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $CI->load->model('admin/Reporting_model');
    $lawyer_leads_price             = $CI->Reporting_model->fetchLawyerAllLeadsPrice($id);
    $Lawyer_all_monthly_bill        = $CI->Reporting_model->fetchLawyerMonthlyBillTotal($id);
    $total_charge_bill                   = $CI->Reporting_model->total_charge_bill($id);
    $lawyerPaidTransactions_new     = $CI->Reporting_model->fetchLawyerPaidTransactions($id);
    $data['total']                  = number_format(($lawyer_leads_price[0]->total + $Lawyer_all_monthly_bill[0]->total + $total_charge_bill[0]->total) - $lawyerPaidTransactions_new[0]->total, 2);
    return $data['total'];
}
function checkLawyerCardDetails($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchLawyerDetails($id);
}
function lawyerDetails($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchLawyerDetails($id);
}
function getlawyerDetails($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $lawyer_details = $CI->Users_model->fetchLawyerDetails($id);
    return $lawyer_details;
}
function getLawyersLeads($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchLawyersLeads($id);
}
function getImage($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchImage($id);
}
function getLawFee($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchLawfee($id);
}
function getMarketName($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $data = $CI->Users_model->fetchMarketRecord($id);
    return $data;
}
function getIntakerName($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $data = $CI->Users_model->fetchIntakerRecord($id);
    echo $data->name;
}
function getSourceName($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $data = $CI->Users_model->fetchSourceRecord($id);
    echo $data->name;
}
function getInitialContactDate($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $data = $CI->Users_model->fetchInitialContact($id);
    if ($data->datetime != '') {
        echo date("m-d-Y H:i:s", strtotime($data->datetime)) . "<br/>";
    } //$data->datetime != ''
    else {
        echo "Empty" . "<br/>";
    }
}
function getStateShortName($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $data = $CI->Users_model->fetchStateRecord($id);
    echo $data->state_short_name;
}
function getQuantity($id, $jur_id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Invoice_model');
    return $CI->Invoice_model->fetchLeadsPaymentBillingQuantity($id, $jur_id);
}
function getStateName($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchStateShortName($id);
}
function getJurisdictionName($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchJurisdictionName($id);
}
function getJurisdictionNamee($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $data = $CI->Users_model->fetchJurisdictionName($id);
    return $data[0]['name'];
}
function checkFee($id, $user_id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchLeadfee($id, $user_id);
}
function getLeadFee($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->Leadfee($id);
}
function getAssignDate($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchAssignDate($id);
}
function getLeadRecievedDate($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchLeadRecievedDate($id);
}
function updateLoggedInStatus($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->update(array(
        "login_first_time" => "1"
    ), $id);
}
function getCaseTypeName($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchCaseTypeName($id);
}
function getFee($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchCaseTypeName($id);
}
function getJurisdictionFee($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Leads_model->fetchJurisdictionFee($id);
}
function generateRandomPassword($length = 6)
{
    $str        = "";
    $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
    $max        = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    } //$i = 0; $i < $length; $i++
    return $str;
}
function mail_from_template($id, $email_to, $user_name)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $mail_template_result = array();
    $id;
    $email_to;
    $user_name;
    $mail_template_result = $CI->Users_model->fetchEmailTemplateData($id);
    return $mail_template_result;
    $to                 = $email_to;
    $from               = $mail_template_result['email_from'];
    $your_name          = 'Pride Legal';
    $subject            = $mail_template_result['subject'];
    $content            = $mail_template_result['content'];
    $oldname            = '<Client Name>';
    $newname            = $data_mailing['name'];
    $newcontent         = str_replace($oldname, $newname, $content);
    $message            = $newcontent;
    $config['protocol'] = 'sendmail';
    $config['mailpath'] = '/usr/sbin/sendmail';
    $config['charset']  = 'iso-8859-1';
    $config['wordwrap'] = TRUE;
    $config['mailtype'] = 'html';
    $this->email->initialize($config);
    $this->email->from($from, $your_name);
    $this->email->to($to);
    $this->email->subject($subject);
    $this->email->message($message);
    $this->email->send();
    echo $this->email->print_debugger();
}
function getLawyersMode($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $data         = $CI->Users_model->fetchLawyersVacationMode($id);
    $holiday_from = $data['holiday_from'];
    $holiday_to   = $data['holiday_to'];
    $current_date = date("Y-m-d");
    if ($holiday_to < $current_date) {
        echo "" . " ";
    } //$holiday_to <= $current_date
    else {
        echo "(On vacation)" . " ";
    }
}
function getLawyer($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    return $CI->Users_model->fetchRow($id);
}
function getLawyerName($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $exploded = explode(',', $id);
    $lawyers  = array();
    foreach ($exploded as $lawyer_id) {
        $id_array  = array();
        $id_array  = $CI->Users_model->fetchLawyerNames($lawyer_id);
        $lawyers[] = $id_array->name;
    } //$exploded as $lawyer_id
    return implode(',', $lawyers);
}
function getLawyerFullName($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $exploded = explode(',', $id);
    $lawyers  = array();
    foreach ($exploded as $lawyer_id) {
        $id_array  = array();
        $id_array  = $CI->Users_model->fetchLawyerFullNames($lawyer_id);
        $lawyers['fname'] = $id_array->name;
        $lawyers['lname'] = $id_array->lname;
    } //$exploded as $lawyer_id
    return implode(' ', $lawyers);
}
function getAdminName($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $user_info = $CI->Central_model->first('users', 'id', $id);
    return $user_info;
}
function getLawyerLastLeadDate($lawyer_id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Leads_model');
    $data = $CI->Leads_model->fetchLawyerLastLeadDate($lawyer_id);
    if ($data->created_at != '') {
        return $data->created_at;
    } //$data->created_at != ''
    else {
        return 'No Lead';
    }
}
function getLawsNames($data)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Leads_model');
    $exploded  = explode(',', $data);
    $law_firms = array();
    foreach ($exploded as $law_id) {
        $firm_array = array();
        $firm_array = $CI->Leads_model->fetchLawsNames($law_id);
        if ($firm_array) {
            $law_firms[] = $firm_array->law_firm;
        } //$firm_array
    } //$exploded as $law_id
    return implode(',', $law_firms);
}
function generateRandomString($length = 10)
{
    $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString     = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    } //$i = 0; $i < $length; $i++
    return $randomString;
}
function getLeadsLawyer($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Leads_model');
    $result      = $CI->Leads_model->fetchLeadsLayers($id);
    $data        = array();
    $assigned_to = '';
    if (!empty($result)) {
        foreach ($result as $res) {
            $assigned_to .= $res->assigned_to . " " . $res->last_name . ', ';
            $assigned_date = $res->assigned_date;
            $row           = $CI->Leads_model->fetchLeadsContactDateTime($id, $res->lawyer_id, 'Contact Reached');
            if ($row) {
                if ($row->contact_reached_at != '1969-12-31 16:00:00') {
                    $data['contact_initial'] = date('M d, Y h:i a', strtotime($row->contact_reached_at));
                } //$row->contact_reached_at != '1969-12-31 16:00:00'
                else {
                    $data['contact_initial'] = 'Empty';
                }
            } //$row
            else {
                $data['contact_initial'] = 'Empty';
            }
        } //$result as $res
        $assigned_to           = rtrim($assigned_to, ', ');
        $data['assigned_to']   = $assigned_to;
        $data['assigned_date'] = date('d F, Y h:i a', strtotime($assigned_date));
    } //!empty($result)
    return $data;
}
function getLeadsLawyerLawfirm($id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Leads_model');
    $result      = $CI->Leads_model->fetchLeadsLayers($id);
    $data        = array();
    $assigned_to = '';
    if (!empty($result)) {
        foreach ($result as $res) {
            $assigned_to .= $res->law_firm_name . ', ';
            $assigned_date = $res->assigned_date;
            $row           = $CI->Leads_model->fetchLeadsContactDateTime($id, $res->lawyer_id, 'Contact Reached');
            if ($row) {
                if ($row->contact_reached_at != '1969-12-31 16:00:00') {
                    $data['contact_initial'] = date('M d, Y h:i a', strtotime($row->contact_reached_at));
                } //$row->contact_reached_at != '1969-12-31 16:00:00'
                else {
                    $data['contact_initial'] = 'Empty';
                }
            } //$row
            else {
                $data['contact_initial'] = 'Empty';
            }
        } //$result as $res
        $assigned_to           = rtrim($assigned_to, ', ');
        $data['assigned_to']   = $assigned_to;
        $data['assigned_date'] = date('d F, Y h:i a', strtotime($assigned_date));
    } //!empty($result)
    return $data;
}
function generateDate($date)
{
    $original_date  = date('Y-m-d', strtotime($date));
    $curent_date    = date('Y-m-d');
    $yesterday_date = date('Y-m-d', strtotime($curent_date . ' -1 days'));
    if ($original_date == $curent_date) {
        $message = 'Today at ' . date('h:ia', strtotime($date));
    } //$original_date == $curent_date
    elseif ($original_date == $yesterday_date) {
        $message = 'Yesterday at ' . date('h:ia', strtotime($date));
    } //$original_date == $yesterday_date
    else {
        $message = date('m-d-Y h:i:s A', strtotime($date));
    }
    return $message;
}
function shortPara($string, $letters)
{
    $string = strip_tags($string);
    if (strlen($string) > $letters) {
        $stringCut = substr($string, 0, $letters);
        $string    = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
    } //strlen($string) > $letters
    return $string;
}
function unreadCount()
{
    $CI =& get_Instance();
    $CI->load->model('admin/Mail_model');
    $id = $CI->session->userdata('id');
    return $CI->Mail_model->getUnreadCount($id);
}
function unreadMails()
{
    $CI =& get_Instance();
    $CI->load->model('admin/Mail_model');
    $id = $CI->session->userdata('id');
    return $CI->Mail_model->getAllData('reciever', $id, '1');
}
function getEmailTread($sender_id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Mail_model');
    $id = $CI->session->userdata('id');
    return $CI->Mail_model->getThreadData($sender_id, $id);
}
function getUnreachedLeads()
{
    $CI =& get_Instance();
    $CI->load->model('admin/Leads_model');
    return $CI->Leads_model->fetchUnreachedLeads();
}
function getUnreachedLeadsLawyer($user_id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Leads_model');
    return $CI->Leads_model->fetchUnreachedLeadsLawyer($user_id);
}
function getUserTrackingCount($user_id, $tracking_id)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Mail_model');
    return $CI->Mail_model->fetchUserCount($user_id, $tracking_id);
}
function getTime($created_date)
{
    $cur_date            = date('Y-m-d H:i:s');
    $timeFirst           = strtotime($created_date);
    $timeSecond          = strtotime($cur_date);
    $differenceInSeconds = $timeSecond - $timeFirst;
    $time                = $differenceInSeconds . ' sec ago';
    if ($differenceInSeconds > 60) {
        $newseconds          = $differenceInSeconds % 60;
        $differenceInMinutes = ($differenceInSeconds - ($newseconds)) / 60;
        $time                = $differenceInMinutes . ' min ' . $newseconds . ' sec ago';
        if ($differenceInMinutes > 60) {
            $newminute         = $differenceInMinutes % 60;
            $differenceInHours = ($differenceInMinutes - ($newminute)) / 60;
            $time              = $differenceInHours . ' hours ' . $newminute . ' mins ' . $newseconds . ' secs ago';
            if ($differenceInHours > 24) {
                $newhours         = $differenceInHours % 24;
                $differenceInDays = ($differenceInHours - ($newhours)) / 24;
                $time             = $differenceInDays . ' days ' . $newhours . ' hours ' . $newminute . ' mins ' . $newseconds . ' secs ago';
            } //$differenceInHours > 24
        } //$differenceInMinutes > 60
    } //$differenceInSeconds > 60
    return $time;
}
function send_email($to,$from,$name,$subject,$message, $cc = null, $bcc = null)
{
	$CI =& get_instance();

/**
 *     $config = Array(
 *         'protocol' => 'smtp',
 *         'smtp_host' => 'smtp.sendgrid.net',
 *         'smtp_port' => 587,
 *         'smtp_user' => 'pride90069',
 *         'smtp_pass' => '@Pride2018!',
 *         'mailtype'  => 'html',
 *         'charset' => 'utf-8'
 *     );
 */

    $CI->load->library('email'); // load library
    $CI->email->set_mailtype("html");
    $CI->email->from($from, $name);
    $CI->email->to($to);
    if($cc)$CI->email->cc($cc);
    if($bcc)$CI->email->bcc($bcc);
    $CI->email->subject($subject);
    $CI->email->message($message);
    $sent = $CI->email->send();
    return $CI->email->print_debugger();
    
/**

 * 	# Sending Email Through cURL
 * 	$url = 'https://api.sendgrid.com/';
 * 	$user = 'pride90069 ';
 * 	$pass = '@Pride2018!';
 * 	
 * 	$params = array(
 * 		'api_user' => $user,
 * 		'api_key' => $pass,
 * 		'to' => $to,
 * 		'subject' => $subject,
 * 		'html' => $message,
 * 		'text' => $message,
 * 		'from' => $from,
 * 		'fromname'  => $name,
 * 	);

 * 	$request = $url.'api/mail.send.json';

 * 	// Generate curl request
 * 	$session = curl_init($request);

 * 	// Tell curl to use HTTP POST
 * 	curl_setopt ($session, CURLOPT_POST, true);

 * 	// Tell curl that this is the body of the POST
 * 	curl_setopt ($session, CURLOPT_POSTFIELDS, $params);

 * 	// Tell curl not to return headers, but do return the response
 * 	curl_setopt($session, CURLOPT_HEADER, false);
 * 	curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

 * 	// obtain response
 * 	$response = curl_exec($session);
 * 	curl_close($session);

 * 	// print everything out
 * 	return $response;
 *     
 */

}
function sms($is_sms, $number, $message)
{
    $CI =& get_Instance();
    $CI->load->model('admin/Users_model');
    $settings = $CI->Central_model->first('settings', 'id', 1);
    if ($settings->sms_active == 0 || $is_sms == 0) // if sms service not active
    {
        return false;
    }
    if($number) {
      $CI =& get_instance();
      $CI->load->library('twilio');
      $CI->load->config('twilio', TRUE);
      $from = $CI->config->item('number', 'twilio');
		//$to = '+13104352176';
      $to = $number;
      $response = $CI->twilio->sms($from, $to, $message);
      if($response->IsError)
         return $response->ErrorMessage;
     else
         return $to;
 } else {
  return false;
}
}
function partition(Array $list, $p) {
    $listlen = count($list);
    $partlen = floor($listlen / $p);
    $partrem = $listlen % $p;
    $partition = array();
    $mark = 0;
    for($px = 0; $px < $p; $px ++) {
        $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
        $partition[$px] = array_slice($list, $mark, $incr);
        $mark += $incr;
    }
    return $partition;
}
function contact_attempts($lead_id, $lawyer_id = NULL)
{
	$CI =& get_Instance();
    $CI->load->model('Central_model');
	# $results = $CI->Central_model->select_all_array('lead_contact_attempts', array('lead_id' => $lead_id, 'action' => 'Contact Attempted', 'lawyer_id' => $lawyer_id));
    $results = $CI->Leads_model->get_lead_contact_attempts($lead_id, $lawyer_id);
    if($results) return $results; else return false;
}
function contact_reached($lead_id, $lawyer_id = NULL)
{
	$CI =& get_Instance();
    $CI->load->model('admin/Leads_model');
    return $CI->Central_model->select_array("lead_contact_attempts", array("lead_id" => $lead_id, "action" => 'Contact Reached', 'lawyer_id' => $lawyer_id));
}
function lead_retained_reason($lead_id, $action, $lawyer_id = NULL)
{
	$CI =& get_Instance();
    $CI->load->model('Central_model');
    return $CI->Central_model->select_array("lead_contact_attempts", array("lead_id" => $lead_id, "action" => $action, 'lawyer_id' => $lawyer_id));
}
function admin_lead_reached($lead_id, $action, $lead_status, $lawyer_id = NULL)
{
	$CI =& get_Instance();
    $CI->load->model('Central_model');
    return $count_rows = $CI->Central_model->count_rows("lead_contact_attempts", array("lead_id" => $lead_id, "action" => $action, "lead_reached_type" => $lead_status, 'lawyer_id' => $lawyer_id));
}
function admin_lead_scheduled($lead_id, $action, $lawyer_id = NULL)
{
	$CI =& get_Instance();
    $CI->load->model('Central_model');
    return $count_rows = $CI->Central_model->count_rows("lead_contact_attempts", array("lead_id" => $lead_id, "action" => $action, 'lawyer_id' => $lawyer_id));
}
function admin_lead_retained($lead_id, $action, $lawyer_id = NULL)
{
	$CI =& get_Instance();
    $CI->load->model('Central_model');
    return $count_rows = $CI->Central_model->count_rows("lead_contact_attempts", array("lead_id" => $lead_id, "action" => $action, 'lawyer_id' => $lawyer_id));
}
function admin_lead_consultation_completed($lead_id, $action, $lawyer_id = NULL)
{
	$CI =& get_Instance();
    $CI->load->model('Central_model');
    return $count_rows = $CI->Central_model->count_rows("lead_contact_attempts", array("lead_id" => $lead_id, "action" => $action, 'lawyer_id' => $lawyer_id));
}
function cms_pages()
{
	$CI =& get_Instance();
    $CI->load->model('Central_model');
    $results = $CI->Central_model->select_all_array('pages', array('is_cms' => 1));
    if($results) return $results; else return false;
}
function _dateFormat($date)
{
	if($date) {
		$res = explode("-", $date);
		$changedDate = $res[2]."-".$res[0]."-".$res[1];
		return $changedDate;
	} else return NULL;
}
function _dateTimeFormat($date)
{
	if($date) {
		$date_time = explode(" ", $date);
		$res = explode("-", $date_time[0]);
		$changedDate = $res[2]."-".$res[0]."-".$res[1].' '.$date_time[1].' '.$date_time[2];
		return date('Y-m-d H:i:s', strtotime($changedDate));
	} else return NULL;
}
function lead_declined($lead_id, $action, $lawyer_id = NULL)
{
	$CI =& get_Instance();
    $CI->load->model('Central_model');
    return $CI->Central_model->select_array("lead_contact_attempts", array("lead_id" => $lead_id, "action" => $action, 'lawyer_id' => $lawyer_id));
}
function find($haystack, $needle){
    if(is_array($haystack) || is_object($haystack)){
        foreach($haystack as $key => $val){
            if($val == $needle){
                return true;
            }
        }
    }else{
        if(strpos($haystack, $needle) !== FALSE){
            return true;
        }
    }
    return false;
}
function custom_sort($column_name)
{
    $uri = base_url("admin/dashboard/manage?") . $_SERVER["QUERY_STRING"];
    $href = add_or_update_params($uri, "name", $column_name);
    $href = add_or_update_params($href, "sort", (isset($_GET["sort"]) && $_GET["name"] == "$column_name" ? ($_GET["sort"] == "ASC" ? "DESC" : "ASC") : "ASC"));
    
    
    echo $href;
}
function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = is_object($row) ? $row->$col : $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}
function add_or_update_params($url,$key,$value){
    $a = parse_url($url);
    $query = isset($a['query']) ? $a['query'] : '';
    parse_str($query,$params);
    $params[$key] = $value;
    $query = http_build_query($params);
    $result = '';
    if($a['scheme']){
        $result .= $a['scheme'] . ':';
    }
    if($a['host']){
        $result .= '//' . $a['host'];
    }
    if($a['path']){
        $result .=  $a['path'];
    }
    if($query){
        $result .=  '?' . $query;
    }
    return $result;
}
function sort_image($column)
{
    $path = base_url("assets/admin/images/sorting_images/");
    return isset($_GET["sort"]) && $_GET["name"] == $column ? ($_GET["sort"] == "ASC" ? $path . "/sort_desc.png" : $path . "/sort_asc.png") : (isset($_COOKIE["column"]) && $_COOKIE["column"] == $column && isset($_COOKIE["sort"]) ? ($_COOKIE["sort"] == "ascending" ? $path . "/sort_desc.png" : $path . "/sort_asc.png") : $path . "/sort_both.png");
}

function dump($array, $exit = true) {
    echo '<pre>';
    print_r($array);
    echo '</pre>';
    if($exit) exit();
}