<?php

class Invoice_model extends CI_Model{

    

    function __construct(){

        parent::__construct();

    }

	
	function fetchLeadsPaymentBillingQuantity($id,$jur_id){
		$this->db->select('leads.*, Jurisdiction_fee.*');
		//$this->db->group_by('leads.jurisdiction');
        $this->db->from('Jurisdiction_fee');
		//$this->db->order_by('lead_id', 'desc');
		//$this->db->order_by('lawyer_id', 'asc');
        $this->db->join('leads','leads.jurisdiction = Jurisdiction_fee.Jurisdiction ', 'left outer');
		$this->db->join('lawyers_to_leads','leads.id = lawyers_to_leads.lead_id ', 'left outer');
		$this->db->where('lawyers_to_leads.lawyer_id',$id);
		$this->db->where('leads.jurisdiction',$jur_id);
		
		//$this->db->where('lead_payment.lead_id',$lead_id);
	
        $query = $this->db->get();
        
		if ($query->num_rows() > 0) {

            return $query->num_rows();
			
        } else {

            return false;
        }
					
	}
	

	function fetchLeadsPaymentBilling($id){
		$this->db->select('leads.*, market_fee.*');
		$this->db->group_by('leads.market');
        $this->db->from('market_fee');
		//$this->db->order_by('lead_id', 'desc');
		//$this->db->order_by('lawyer_id', 'asc');
        $this->db->join('leads','leads.market = market_fee.market ', 'left outer');
		$this->db->join('lawyers_to_leads','leads.id = lawyers_to_leads.lead_id ', 'left outer');
		$this->db->where('lawyers_to_leads.lawyer_id',$id);
		//$this->db->where('lead_payment.lead_id',$lead_id);
	
        $query = $this->db->get();
        
		if ($query->num_rows() > 0) {

            return $query->result();
			
        } else {

            return false;
        }
					
	}
	
	
		function fetchLawyerDetails($id){
	
		$this->db->select('*');
        $this->db->from('users');
		$this->db->where('id',$id);
		//$this->db->where('lead_payment.lead_id',$lead_id);
	
        $query = $this->db->get();
        
		if ($query->num_rows() > 0) {

            return $query->row();
			
        } else {

            return false;
        }
					
	}
	
	function fetchHeaderDetails($id){
	
		$this->db->select('*');
        $this->db->from('settings');
		$this->db->where('id',1);
	
        $query = $this->db->get();
        
		if ($query->num_rows() > 0) {

            return $query->row();
			
        } else {

            return false;
        }
					
	}
	
	
	function fetchInvoiceDueDate($id){
	
		$this->db->select('*');
        $this->db->from('settings');
		$this->db->where('id',$id);
		//$this->db->where('lead_payment.lead_id',$lead_id);
        $query = $this->db->get();
		if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }				
	}
	


	function fetchLawyerLawFirmDetails($id){
	
		$this->db->select('*');
        $this->db->from('law_firms');
		$this->db->where('id',$id);
		//$this->db->where('lead_payment.lead_id',$lead_id);
	
        $query = $this->db->get();
        
		if ($query->num_rows() > 0) {

            return $query->row();
			
        } else {

            return false;
        }
					
	}
	
	function fetchLawyerTransactionsOfCurrentMonth($id){
	
		$this->db->select('*');
        $this->db->from('credit_card_transactions');
		$this->db->where('lawyer_id',$id);
		//$this->db->where('transaction_date',date('Y-m-d'));
	
        $query = $this->db->get();
        
		if ($query->num_rows() > 0) {

            return $query->result();
			
        } else {

            return false;
        }
					
	}
	
	
}
