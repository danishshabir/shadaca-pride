<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Add New Affiliated</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Affiliated Form</h3>
            </div>
            <?php echo $this->session->flashdata('message')?>
            <div class="porlets-content">
              <form action="<?php echo base_url();?>admin/affiliated/save" method="post" parsley-validate novalidate>
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="name" parsley-trigger="change" required placeholder="Enter name" class="form-control">
                </div><!--/form-group-->
                
                 <div class="form-group">
                  <label>Email address</label>
                  <input type="email" name="email" parsley-trigger="change" required placeholder="Enter email" class="form-control" value="<?php echo $this->session->flashdata('email')?>">
                </div><!--/form-group-->
                
                <div class="form-group">
                  <label>Phone</label>
                    <input type="phone" name="phone" parsley-trigger="change" required placeholder="Enter phone" class="form-control" value="<?php echo $this->session->flashdata('phone')?>">
                </div>
                
                <div class="form-group">
                  <label>Address</label>
                    <textarea class="form-control" name="address"><?php echo $this->session->flashdata('address')?></textarea>
                </div>
                <div class="form-group">
                  <label>Gender</label>
                    <select name="gender" class="form-control">
                    	<option value="">Select Gender</option>
                    	<option <?php if($this->session->flashdata('gender') == 'Male'){?> selected="selected" <?php }?> value="Male">Male</option>
                        <option <?php if($this->session->flashdata('gender') == 'Female'){?> selected="selected" <?php }?> value="Female">Female</option>
                    </select>
                </div>
               
             
              
                <button class="btn btn-primary" type="submit">Submit</button>
                       <a href="<?php echo base_url();?>admin/affiliated" class="btn btn-default">Cancel</a>
			</form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>