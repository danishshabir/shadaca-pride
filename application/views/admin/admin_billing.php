<div id="main-content">

    <div class="page-content">

        <span id="payment_form"></span>
        <div class="row">

            <div class="col-md-12">

                <h2>Lawyers Billing <?php echo $type; ?></h2>

            </div><!--/col-md-12-->

        </div><!--/row-->


        <div class="row">

            <div class="col-md-12">

                <div class="block-web">

                    <div class="header">


                        <div class="actions"><a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a
                                class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down"
                                                                                             href="#"><i
                                    class="fa fa-times"></i></a></div>

                        <?php
                        $lawyer_id = $this->uri->segment(4);
                        //getLawyerTotalBalance($lawyer_id);
                        $lawyer_details = lawyerDetails($lawyer_id);
                        $lawyer_details->name;
                        $lawyer_details->lname;

                        $lawyer_details->refrence_id . "<br>";
                        $lawyer_details->cust_prof_id . "<br>";
                        $lawyer_details->paym_prof_id . "<br>";


                        if ($lawyer_id != '') {
                            ?>
                            <h3 class="content-header text-center"
                                style="font-weight:700;"><?php echo "Lawyer name: " . $lawyer_details->name . " " . $lawyer_details->lname; ?>    </h3>
                            <h3 class="content-header text-center"
                                style="font-weight:700;">   <?php getLawyerTotalBalance($lawyer_id); ?> </h3>
                        <?php } ?>


                    </div>

                    <?php if ($this->session->flashdata('message')) {
                        echo '<span id="flash_message">' . $this->session->flashdata('message') . '</span>';
                    } ?>


                    <?php $lawyer_id = $this->uri->segment(4);
                    if ($lawyer_id != '') {
                        ?>

                        <?php if (!empty($lawyer_details->cust_prof_id) && !empty($lawyer_details->refrence_id)) { ?>

                            <br/><br/>
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#existingCard">Use existing card</a></li>
                                <li><a data-toggle="tab" href="#newCard">Use new card</a></li>
                                <li><a data-toggle="tab" href="#pay_check">Pay by cheque</a></li>
                            </ul>


                            <div class="col-md-12">
                                <span style="color:green; text-align:center; font-size:22px"
                                      id="success_message"></span>

                                <h4 class="text-center"> Payment form</h4>
                                <br/>

                                <div class="tab-content">
                                    <div id="existingCard" class="tab-pane fade in active">

                                        <!-- full bill charge form -->

                                        <form
                                            action="<?php echo base_url(); ?>admin/transaction/chargeExistingCreditCard/<?php //echo $lawyer_id;?>"
                                            method="post" parsley-validate novalidate>


                                            <div class="form-group col-sm-4">
                                                <label>Amount</label>
                                                <input type="text" name="amount" parsley-trigger="change" required
                                                       placeholder="Enter amount" class="form-control">
                                            </div>

                                            <div class="form-group col-sm-4">
                                                <label>Card code</label>
                                                <input type="text" name="card_code" parsley-trigger="change" required
                                                       placeholder="Enter card code" class="form-control">
                                            </div>
                                            <?php
                                            $customerProfileId = $lawyer_details->cust_prof_id;
                                            $customerPaymentProfileId = $lawyer_details->paym_prof_id;


                                            ?>
                                            <input type="hidden" name="customerProfileId"
                                                   value="<?php echo $customerProfileId; ?>" parsley-trigger="change"
                                                   class="form-control">
                                            <input type="hidden" name="customerPaymentProfileId"
                                                   value="<?php echo $customerPaymentProfileId; ?>"
                                                   parsley-trigger="change" class="form-control">

                                            <input type="hidden" name="lawyer_id"
                                                   value="<?php echo $this->uri->segment(4); ?>"
                                                   parsley-trigger="change" class="form-control">
                                            <input type="hidden" name="status" value="<?php echo '0'; ?>"
                                                   parsley-trigger="change" class="form-control">


                                            <div class="clearfix"></div>

                                            <div class="form-group col-sm-1">
                                                <button class="btn btn-primary" type="submit">Pay now</button>
                                            </div>


                                        </form>
                                    </div>

                                    <div id="newCard" class="tab-pane fade">
                                        <form
                                            action="<?php echo base_url(); ?>admin/transaction/chargeCreditCard/<?php //echo $lawyer_id;?>"
                                            method="post" parsley-validate novalidate>

                                            <div class="form-group col-sm-4">
                                                <label>Select card type</label>

                                                <select placeholder="Card type" class="form-control" name="card_type"
                                                        required>
                                                    <option value=""> Select type</option>
                                                    <option value="visa">Visa</option>
                                                    <option value="Mastercard"> Mastercard</option>
                                                </select>
                                            </div>


                                            <div class="form-group col-sm-4">
                                                <label>Credit card number</label>
                                                <input type="text" name="card_no" value="6011000000000012"
                                                       parsley-trigger="change" required
                                                       placeholder="Enter credit card number" class="form-control">
                                            </div>


                                            <div class="form-group col-sm-4">
                                                <label>Expiry date</label>
                                                <input type="text" id="datepicker" name="exp_date" value="04/15"
                                                       parsley-trigger="change" required
                                                       placeholder="Enter expiry date:" class="form-control">
                                            </div>

                                            <div class="clearfix"></div>


                                            <div class="form-group col-sm-4">
                                                <label>Card Code Verification (CCV) </label>
                                                <input type="text" name="credit_card_code" value="123"
                                                       parsley-trigger="change" required
                                                       placeholder="Enter Card Code (CCV) " class="form-control">
                                            </div>

                                            <div class="form-group col-sm-4">
                                                <label>Amount</label>
                                                <input type="text" name="amount" parsley-trigger="change" required
                                                       placeholder="Enter amount" class="form-control">
                                            </div>


                                            <input type="hidden" name="lawyer_id"
                                                   value="<?php echo $this->uri->segment(4); ?>"
                                                   parsley-trigger="change" class="form-control">
                                            <input type="hidden" name="status" value="<?php echo '2'; ?>"
                                                   parsley-trigger="change" class="form-control">
                                            <input type="hidden" name="newCard" value="<?php echo '1'; ?>"
                                                   parsley-trigger="change" class="form-control">


                                            <div class="clearfix"></div>

                                            <div class="form-group col-sm-1">
                                                <button class="btn btn-primary" type="submit">Pay now</button>
                                            </div>


                                        </form>
                                    </div>

                                    <div id="pay_check" class="tab-pane fade">

                                        <!-- full bill charge form -->

                                        <form
                                            action="<?php echo base_url(); ?>admin/transaction/chargeExistingCreditCard/<?php //echo $lawyer_id;?>"
                                            method="post" parsley-validate novalidate>

                                            <div class="form-group col-sm-5">
                                                <label>Amount</label>
                                                <input type="text" name="amount" parsley-trigger="change" required
                                                       placeholder="Enter amount" class="form-control">
                                            </div>

                                            <div class="form-group col-sm-5">
                                                <label>Cheque number / cash / promotional code</label>
                                                <input type="text" name="pay_check" parsley-trigger="change" required
                                                       placeholder="Enter cheque number / cash / promotional code"
                                                       class="form-control">
                                            </div>

                                            <input type="hidden" name="lawyer_id"
                                                   value="<?php echo $this->uri->segment(4); ?>"
                                                   parsley-trigger="change" class="form-control">
                                            <input type="hidden" name="status" value="<?php echo '2'; ?>"
                                                   parsley-trigger="change" class="form-control">


                                            <div class="clearfix"></div>

                                            <div class="form-group col-sm-1">
                                                <button class="btn btn-primary" type="submit">Pay now</button>
                                            </div>


                                        </form>
                                    </div>


                                </div>
                                <div class="clearfix"></div>


                            </div>


                        <?php } else { ?>

                            <br/><br/>
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#newCard2">Use card</a></li>
                                <li><a data-toggle="tab" href="#pay_check2">Pay by cheque</a></li>
                            </ul>
                            <br/>
                            <div class="tab-content">
                                <div id="newCard2" class="tab-pane fade in active">
                                    <form
                                        action="<?php echo base_url(); ?>admin/transaction/chargeCreditCard/<?php //echo $lawyer_id;?>"
                                        method="post" parsley-validate novalidate>

                                        <div class="form-group col-sm-4">
                                            <label>Select card type</label>

                                            <select placeholder="Card type" class="form-control" name="card_type"
                                                    required>
                                                <option value=""> Select type</option>
                                                <option value="visa">Visa</option>
                                                <option value="Mastercard"> Mastercard</option>
                                            </select>
                                        </div>


                                        <div class="form-group col-sm-4">
                                            <label>Credit card number</label>
                                            <input type="text" name="card_no" value="6011000000000012"
                                                   parsley-trigger="change" required
                                                   placeholder="Enter credit card number" class="form-control">
                                        </div>


                                        <div class="form-group col-sm-4">
                                            <label>Expiry date</label>
                                            <input type="text" id="datepicker" name="exp_date" value="04/15"
                                                   parsley-trigger="change" required placeholder="Enter expiry date:"
                                                   class="form-control">
                                        </div>

                                        <div class="clearfix"></div>


                                        <div class="form-group col-sm-4">
                                            <label>Card Code Verification (CCV) </label>
                                            <input type="text" name="credit_card_code" value="123"
                                                   parsley-trigger="change" required
                                                   placeholder="Enter Card Code (CCV) " class="form-control">
                                        </div>

                                        <div class="form-group col-sm-4">
                                            <label>Amount</label>
                                            <input type="text" name="amount" parsley-trigger="change" required
                                                   placeholder="Enter amount" class="form-control">
                                        </div>


                                        <input type="hidden" name="lawyer_id"
                                               value="<?php echo $this->uri->segment(4); ?>" parsley-trigger="change"
                                               class="form-control">
                                        <input type="hidden" name="status" value="<?php echo '2'; ?>"
                                               parsley-trigger="change" class="form-control">
                                        <input type="hidden" name="newCard" value="<?php echo '1'; ?>"
                                               parsley-trigger="change" class="form-control">


                                        <div class="clearfix"></div>

                                        <div class="form-group col-sm-1">
                                            <button class="btn btn-primary" type="submit">Pay now</button>
                                        </div>


                                    </form>
                                </div>

                                <div id="pay_check2" class="tab-pane fade">

                                    <!-- full bill charge form -->

                                    <form
                                        action="<?php echo base_url(); ?>admin/transaction/chargeExistingCreditCard/<?php //echo $lawyer_id;?>"
                                        method="post" parsley-validate novalidate>

                                        <div class="form-group col-sm-5">
                                            <label>Amount</label>
                                            <input type="text" name="amount" parsley-trigger="change" required
                                                   placeholder="Enter amount" class="form-control">
                                        </div>

                                        <div class="form-group col-sm-5">
                                            <label>Cheque number / cash / promotional code</label>
                                            <input type="text" name="pay_check" parsley-trigger="change" required
                                                   placeholder="Enter cheque number / cash / promotional code"
                                                   class="form-control">
                                        </div>

                                        <input type="hidden" name="lawyer_id"
                                               value="<?php echo $this->uri->segment(4); ?>" parsley-trigger="change"
                                               class="form-control">
                                        <input type="hidden" name="status" value="<?php echo '2'; ?>"
                                               parsley-trigger="change" class="form-control">


                                        <div class="clearfix"></div>

                                        <div class="form-group col-sm-1">
                                            <button class="btn btn-primary" type="submit">Pay now</button>
                                        </div>


                                    </form>
                                </div>
                            </div>


                        <?php }
                    } ?>


                    <!--
              <form action="<?php echo base_url(); ?>admin/dashboard/ChecklawyerBilling" method="post" parsley-validate novalidate class="ajaxFrm" id="search_Lawyer_billing">


			  	<div class="form-group col-md-3">
					<label>Select billing type</label>
                 <select placeholder="Billing type" class="form-control" name="billing_type" id="billing_type">

					 <option value=""> Select billing type </option>
					 <option value="1"> Leads billing </option>
					 <option value="2"> Monthly billing </option>

                    </select>
                </div>



                <div class="form-group col-md-2">

                  <label>Date from</label>

                  <input type="text" name="date_from" id="datepicker" parsley-trigger="change" placeholder="Enter Date From" class="form-control">

                </div>


				 <div class="form-group col-md-2">

                  <label>Date to</label>

                  <input type="text" name="date_to" id="datepicker2"  parsley-trigger="change" placeholder="Enter Date To" class="form-control">

                </div>

			<div class="form-group col-md-2">
				 <label style="color:#fff">Search</label><br/>
                <button class="btn btn-primary" type="submit">Search</button>

			</div>
			</form>
	-->

                    <div class="clearfix"></div>

                    <span style="margin-top:20px; padding-top:20px;"></span>


                    <div class="form-group col-sm-6" style="margin-top:10px;">
                        <label>Name</label>
                        <input type="text" name="name" id="name" parsley-trigger="change" placeholder="Enter name"
                               class="form-control searchLawyers">
                    </div>


                    <div class="form-group col-sm-6" style="margin-top:10px;">
                        <label>law firm</label>
                        <select placeholder="Law Type" class="form-control searchLawfirms" name="law_firms"
                                id="law_firms">
                            <option value="0"> Select law firm</option>

                            <?php foreach ($law_firms as $law_firm) { ?>

                                <option id="fee"
                                        value="<?php echo $law_firm->id; ?>"> <?php echo $law_firm->law_firm; ?> </option>

                            <?php } ?>

                        </select>
                    </div>


                    <div class="clearfix"></div>
                    <div id="success_message" class="text-center center_block"
                         style="color:green; font-weight:700; font-size:20px; display:none;"> Success
                    </div>


                    <div class="form-group col-md-12"></div>


                    <div class="porlets-content">

                        <div id="all_lawyers_table">


                            <div class="table-responsive">
                                <div class="clearfix"></div>
                                <div class="margin-top-10"></div>


                                <table class="display table table-bordered table-striped" id="dynamic-table">

                                    <thead>
                                    <tr>
                                        <th style="width:150px;">Name</th>
                                        <th>Email</th>
                                        <th>Law firm</th>
                                        <th>Balance</th>
                                        <th>Leads Bill</th>
                                        <!--  <th>Leads Bill</th>  -->
                                        <th>Monthly Bill</th>
                                        <th>Payments</th>
                                        <th>Pay Bill</th>

                                    </tr>
                                    </thead>


                                    <tbody class="searchable" id="show_table"></tbody>
                                    <tbody class="searchable" id="hide_table">
                                    <?php
                                    if ($all_lawyers) {
                                        $i = 0;
                                        foreach ($all_lawyers as $all_lawyer) {
                                            ?>
                                            <tr>
                                                <td><?php echo $all_lawyer->name; ?></td>
                                                <td><?php echo $all_lawyer->email; ?></td>
                                                <td><?php echo getLawsNames($all_lawyer->law_firms); ?></td>
                                                <td><?php echo "$" . $totalArrays[$i] . ".00"; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>admin/dashboard/LawyerAllLeads/<?php echo $all_lawyer->id; ?>">Details</a>
                                                </td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>admin/dashboard/LawyerBillings/<?php echo $all_lawyer->id; ?>">Details</a>
                                                </td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>admin/dashboard/LawyerBills/<?php echo $all_lawyer->id; ?>">Details</a>
                                                </td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>admin/dashboard/LawyerBilling/<?php echo $all_lawyer->id . '#payment_form'; ?>">
                                                        <button class="btn btn-info">Make payment</button>
                                                    </a>
                                                    <a href="<?php echo base_url(); ?>admin/dashboard/History/<?php echo $all_lawyer->id; ?>">
                                                        <button class="btn btn-success text-center" style="margin:3px;">
                                                            History
                                                        </button>
                                                    </a>
                                                </td>
                                                <!--
				<td>
				<a href="<?php //echo base_url();
                                                ?>admin/dashboard/LawyerMonthlyBilling/<?php //echo $all_lawyer->id;
                                                ?>">Details</a></td>
		-->
                                                <!--
				<td>
				<a href="<?php //echo base_url();
                                                ?>admin/leadInvoice/LawyerLeadInvoice/<?php //echo $all_lawyer->id;
                                                ?>" target="_blank">Invoice</a></td>
		-->

                                            </tr>
                                            <?php $i++;
                                        }
                                    } ?>
                                    </tbody>

                                </table>

                            </div><!--/table-responsive-->

                            <div class="clearfix"></div>

                        </div>

                    </div><!--/porlets-content-->

                    <div class="porlets-content">


                    </div><!--/porlets-content-->

                </div><!--/block-web-->

            </div><!--/col-md-6-->

        </div><!--/row-->

    </div><!--/page-content end-->

</div>

<script>
    $(function () {
        $("#datepicker").datepicker();
    });
    $(function () {
        $("#datepicker2").datepicker();
    });


    $("#search_Lawyer_billing").submit(function (e) {
        $form = $(this);
        var url = $form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: $form.serialize(),
            success: function (response) {
                //jQuery.parseJSON(data);
                if (response != '') {
                    //alert(response.html);
                    $("#table_content").html(response);
                    $("#search_result").show();
                    $("#success_message").html('Success');
                    //$("#all_lawyers_table").hide();
                    $('#success_message').delay(5000).fadeOut('slow');

                } else {
                    $("#success_message").hide();
                    $("#search_result").hide();
                    //$("#all_lawyers_table").show();
                }


            }
        });
        e.preventDefault();
    });


</script>

<script>
    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 700;  //time in ms, 5 second for example

    //on keyup, start the countdown
    $('.searchLawyers').on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(searchByName, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $('.searchLawyers').on('keydown', function () {
        clearTimeout(typingTimer);
    });

    function searchByName()
    {
        var name = $('#name').val();
        var law_firm = $('#law_firms').val();
        var url = '<?php echo base_url();?>admin/reporting/SearchLawyer';

        $.ajax({
            type: "POST",
            url: url,
            data: {name: name, law_firm: law_firm},
            success: function (response) {
                //jQuery.parseJSON(data);
                if (response != '') {
                    $("#show_table").show();
                    $("#show_table").html(response);
                    $("#search_result").show();
                    $("#success_message").show();
                    $("#hide_table").hide();
                    $('#success_message').delay(5000).fadeOut('slow');

                } else {
                    $("#success_message").hide();
                    $("#search_result").hide();
                    $("#hide_table").show();
                    $("#show_table").hide();
                }
            }
        });
    }


    $(".searchLawfirms").change(function (e) {
        var name = $('#name').val();
        var law_firm = $('#law_firms').val();
        var url = '<?php echo base_url();?>admin/reporting/SearchLawyer';

        $.ajax({
            type: "POST",
            url: url,
            data: {name: name, law_firm: law_firm},
            success: function (response) {
                //jQuery.parseJSON(data);
                if (response != '') {
                    $("#show_table").show();
                    $("#show_table").html(response);
                    $("#search_result").show();
                    $("#success_message").show();
                    $("#hide_table").hide();
                    $('#success_message').delay(5000).fadeOut('slow');

                } else {
                    $("#success_message").hide();
                    $("#search_result").hide();
                    $("#hide_table").show();
                    $("#show_table").hide();
                }


            }
        });
        e.preventDefault();
    });

</script>
