<?php echo $this->session->flashdata('message');?>
<div class="col-sm-9 col-lg-10">
          <div class="block-web">
            <div class="pull-right">
              <div class="btn-group">
                <!--<button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips archive_mail" data-original-title="Archive"><i class="glyphicon glyphicon-hdd"></i></button>-->
                <!--<button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips" data-original-title="Report Spam"><i class="glyphicon glyphicon-exclamation-sign"></i></button>-->
                <!--<button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips delete_mail" data-original-title="Delete"><i class="glyphicon glyphicon-trash"></i></button>-->
              </div>
              <div class="btn-group">
                <button type="button" class="btn btn-white"><i class="glyphicon glyphicon-chevron-left"></i></button>
                <button type="button" class="btn btn-white"><i class="glyphicon glyphicon-chevron-right"></i></button>
              </div>
            </div> 
            
            <strong>Messages</strong>
            <p class="text-muted">Showing <?php echo sizeof($inbox_arr);?> - <?php echo sizeof($inbox_arr);?> of <?php echo sizeof($inbox_arr);?> messages</p>
            <div class="table-responsive">
              <table class="table table-email">
                <tbody>
                	<?php foreach($inbox_arr as $inbox){?>
                    	<form action="<?php base_url();?>mail/read" id="readmailfrm_<?php echo $inbox->id;?>" method="post">
                        	<input type="hidden" name="message_id" value="<?php echo $inbox->id;?>">
                            <input type="hidden" name="type" value="inbox">
                            <input type="hidden" name="status" value="<?php echo $inbox->status;?>">
                        </form>
                  		<tr <?php if($inbox->status == 0){?> class="unread" <?php }?>>
                    <td><div class="ckbox ckbox-primary">
                       <!-- <input type="checkbox" id="message_id" name="message_id[]" value="<?php echo $inbox->id;?>">
                        <label for="checkbox1"></label>-->
                      </div></td>
                    <td><a class="star" href=""><i class="glyphicon glyphicon-star"></i></a></td>
                    <td><div class="media" id="<?php echo $inbox->id;?>">
                        <div class="media-body"> <span class="media-meta pull-right"><?php echo generateDate($inbox->created_at);?></span>
                          <h4 class="text-primary"><?php echo $inbox->user_name;?></h4>
                          <small class="text-muted"></small>
                          <p class="email-summary"><?php echo shortPara($inbox->message, '100');?></p>
                        </div>
                      </div></td>
                  </tr>
                  	<?php }?>
                </tbody>
              </table>
            </div><!-- /table-responsive --> 
          </div><!--/ block-web --> 
        </div><!-- /col-sm-9 --> 