
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12"> </div>
			<!--/col-md-12--> </div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Add Message</h3>
					</div>
					<div class="clearfix"></div>
					<?php 
			
			
			if($this->session->flashdata('message')){
				
				echo $this->session->flashdata('message');
			
			}
			
			?>
					<div class="tab-content paddingZero">
						<form action="<?php echo base_url();?>admin/mail/sendEmail" method="post" parsley-validate novalidate>
						<div class="porlets-content"  >
						<div class="block-web tab-pane fade in active padding20px0" id="home">
						<form method="post" action="" role="form-horizontal">
							<div class="form-group col-sm-4">
								<label style="font-weight:700;">Subject</label>
								<input type="text" name="subject" parsley-trigger="change" required placeholder="Enter subject" class="form-control">
							</div>
							<!--/form-group-->
							<div class="clearfix"></div>
							<div class="form-group col-sm-6">
								<p>Email Tags</p>
								<p>For <span style="font-weight:700;">Name </span>write {name}</p>
								<p>For <span style="font-weight:700;">Username </span>write {username}</p>
								<p>For <span style="font-weight:700;">organization name</span> write {organization}</p>
								<p>For <span style="font-weight:700;">Email</span> write {email}</p>
								<p>For <span style="font-weight:700;">City</span> write {city}</p>
								<p>For <span style="font-weight:700;">State</span> write {state}</p>
								<p>For <span style="font-weight:700;">Country</span> write {country}</p>
								<p>For <span style="font-weight:700;">URL</span> write {URL}</p>
								<p>For <span style="font-weight:700;">Tags</span> write {tags} </p>
							</div>
							
							<div class="clearfix"></div>
							<div class="form-group col-sm-12">
							  <label class="" for="to">To:</label>
							  <select class="form-control chosen-select" name="Sent_to[]" multiple="multiple">
								<?php foreach($users as $user){?>
									<option value="<?php echo $user['id']; ?>"><?php echo $user['name']; ?> (<?php echo $user['email']; ?>)</option>
								<?php }?>
							  </select>
							</div>
							<div class="compose-mail">
								<div class="compose-editor" style="margin-top:10px;">
									<textarea id="text-editor" placeholder="Enter text ..." class="col-xs-12" name="detail" rows="25">									<?php echo $email_template_data->content?>								  </textarea>
								</div>
								<!--<div class="compose-editor">								  <input type="file" class="default" name="file[]" multiple>								</div>--> </div>
							<br>
							
						<!--	
							<?php //foreach($users as $user){ ?>
							<input type="checkbox"/ style="cursor: pointer;height: 30px;opacity: 10 !important;position: absolute;width: 18px;transform: scale(1.4);z-index: 2;" name="Sent_to[]" value="<?php //echo $user['id']?>" id="checkbox">
							<?php //echo "<h4 style='padding-left:30px; padding-bottom:10px;'>" . $user['email'] . "</h4>"?> <br>
							<?php //}?>
						-->	
							
							
							<div class="bottom">
								<button type="submit" class="btn btn-primary">Send</button>
							</div>
						</form>
					</div>
					<!--/ block-web --> </div>
				<!--/porlets-content-->
				</form>
			</div>
			<!--/block-web--> </div>
		<!--/block-web--> </div>
	<!--/col-md-12--> </div>
<!--/row-->
</div>
<!--/page-content end-->
</div>
<!--/main-content end-->
</div>

<script type="text/javascript">
  var config = {
	'.chosen-select'           : {},
	'.chosen-select-deselect'  : {allow_single_deselect:true},
	'.chosen-select-no-single' : {disable_search_threshold:10},
	'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	'.chosen-select-width'     : {width:"95%"}
  }
for (var selector in config) {
	$(selector).chosen(config[selector]);
  }

</script>