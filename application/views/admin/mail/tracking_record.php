
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12"> </div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Email Tracking Stats</h3>
					</div>
					<div class="clearfix"></div>
					<div class="tab-content">
						<div class="porlets-content"  >
							<div class="table-responsive tab-pane" id="menu1">
								<div class="clearfix"> </div>
								<div class="margin-top-10"></div>
								<table  class="display table table-bordered table-striped" >
									<thead>
										<tr>
											<th>No:</th>
											<th>Subject</th>
											<th>Detail</th>
											<th>Date/time</th>
											<th>number of recipients</th>
											<th>Open Count</th>
											
											<!-- <th>Sent to</th> --> 
										</tr>
									</thead>
									<tbody>
										<?php 			
	  $i = 1;		
	  ?>
										<?php				
	  foreach($all_record as $getAllRecords){?>
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $getAllRecords->subject;?></td>
											<td><form method="post" id='frm_detail<?php echo $i;?>' action="<?php echo base_url();?>admin/EmailTracking/subscriberDetail/">
													<input type='hidden' value='<?php echo $getAllRecords->Sent_to;?>' name='send_to'>
													<input type='hidden' value='<?php echo $getAllRecords->id;?>' name='track_id'>
												</form>
												<a href="javascript:void(0);" onclick="$('#frm_detail<?php echo $i;?>').submit();">Details</a></td>
											<td><?php echo $newDate = date("m-d-Y h:i:s", strtotime($getAllRecords->date_time));?></td>
											<td><?php echo $getAllRecords->number_of_recipients;?></td>
											<td><?php echo $getAllRecords->Open_Count;?></td>
											<!--   <td><?php //echo $getAllRecords->Sent_to;?></td>--> 
										</tr>
										<?php $i++;?>
										<?php }?>
									</tbody>
								</table>
							</div>
							<!--/table-responsive--> 
						</div>
						<!--/porlets-content--> 
					</div>
					<!--/block-web--> 
				</div>
				<!--/block-web--> 
				
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row--> 
	</div>
	<!--/page-content end--> 
</div>
<!--/main-content end-->
</div>
