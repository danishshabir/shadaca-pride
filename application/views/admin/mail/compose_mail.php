<div class="col-sm-9 col-lg-10">
	<div class="block-web">
		<form method="post" action="<?php echo base_url();?>admin/mail/sendMessage" role="form-horizontal">
			<div class="compose-mail">
					<?php if ($this->session->userdata('role') == 1){?>
						<div class="form-group">
							<label class="" for="to">To:</label>
							<select class="form-control chosen-select" name="to[]" multiple="multiple">
							<?php foreach($users as $user){?>
								<option value="<?php echo $user->id.'|'.$user->role; ?>"><?php echo $user->name; ?> (<?php echo $user->email; ?>)</option>
							<?php }
							foreach($law_frims as $law_firm){?>
								<option value="<?php echo $law_firm->id.'|'.$law_firm->role; ?>"><?php echo $law_firm->law_firm; ?> (<?php echo $law_firm->contact_email; ?>)</option>
							<?php } ?>
						</select>
						</div>
					<?php }else{ ?>
						<h5>To: <strong>support@pridelegal.com</strong></h5>
						<input type="hidden" name="to[]" value="212|1">
					<?php } ?>
				<div class="compose-editor" style="margin-top:10px;">
					<textarea id="text-editor" placeholder="Enter text ..." class="col-xs-12" name="message" rows="25"></textarea>
				</div>
			</div>
			<div class="bottom">
				<button type="submit" class="btn btn-primary">Send</button>
			</div>
		</form>
	</div>
	<!--/ block-web -->
</div>
<!-- /col-sm-9 -->
<script type="text/javascript">
	var config = {
		'.chosen-select'           : {},
		'.chosen-select-deselect'  : {allow_single_deselect:true},
		'.chosen-select-no-single' : {disable_search_threshold:10},
		'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		'.chosen-select-width'     : {width:"95%"}
	}
	for (var selector in config) {
		$(selector).chosen(config[selector]);
	}
</script>