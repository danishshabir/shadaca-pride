<div class="col-sm-9 col-lg-10">
	<div class="block-web">
		<div class="pull-right">
			<div class="btn-group">
				<!--<button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips" data-original-title="Archive"><i class="glyphicon glyphicon-hdd"></i></button>-->
				<!--<button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips" data-original-title="Report Spam"><i class="glyphicon glyphicon-exclamation-sign"></i></button>-->
				<!--<button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips" data-original-title="Delete"><i class="glyphicon glyphicon-trash"></i></button>-->
			</div>
		</div>
		<!-- pull-right -->
		<div class="btn-group">
			<button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips" data-original-title="Read Previous Email"><i class="glyphicon glyphicon-chevron-left"></i></button>
			<button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips" data-original-title="Read Next Email"><i class="glyphicon glyphicon-chevron-right"></i></button>
		</div>
		<div class="read-panel">
			<?php
				echo $this->session->flashdata('message');
				$module_type = $this->session->userdata('module_type');
				$base_id = $read->sender_id;
				$id = $this->session->userdata('id');
				foreach(getEmailTread($base_id) as $mail){?>
					<div class="<?php if($mail->sender_id == $id){?> myMail <?php }?>allMail">
						<div class="media">
							<div class="media-body">
								<span class="media-meta pull-right"><?php echo generateDate($mail->created_at);?></span>
								<h4 class="text-primary"><?php echo $mail->user_name;?></h4>
								<small class="text-muted">From: <?php echo $mail->user_email;?></small> 
							</div>
						</div>
						<br>                  
						<p><?php echo $mail->message;?></p>
					</div>
				<?php } ?>
			<br>
			<div class="media">
				<div class="block-web">
					<form action="<?php echo base_url();?>admin/mail/replyMessage" method="post">
						<input type="hidden" name="reciever_id" value="<?php echo $read->sender_id;?>" />
						<input type="hidden" name="reciever_role" value="<?php echo $read->sender_role;?>" />
						<div class="compose-editor" style="margin-top:10px;">
							<textarea id="text-editor" placeholder="Reply here..." class="col-xs-12" name="message" rows="25"></textarea>
						</div>
						<div class="bottom">
							<button type="submit" class="btn btn-primary">Reply</button>
						</div>
					</form>
				</div>
			</div>
			<!-- /media -->
		</div>
		<!--/ read-panel -->    
	</div>
	<!--/ block-web --> 
</div>
<!-- /col-sm-9 -->