<div class="col-sm-9 col-lg-10">
          <div class="block-web">
            <div class="pull-right">
              <div class="btn-group">
                <button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips" data-original-title="Un Archive"><i class="glyphicon glyphicon-hdd"></i></button>
                <!--<button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips" data-original-title="Report Spam"><i class="glyphicon glyphicon-exclamation-sign"></i></button>-->
                <button title="" data-toggle="tooltip" type="button" class="btn btn-white tooltips" data-original-title="Delete"><i class="glyphicon glyphicon-trash"></i></button>
              </div>
              <div class="btn-group">
                <button type="button" class="btn btn-white"><i class="glyphicon glyphicon-chevron-left"></i></button>
                <button type="button" class="btn btn-white"><i class="glyphicon glyphicon-chevron-right"></i></button>
              </div>
            </div> 
            
            <strong>Archived</strong>
            <p class="text-muted">Showing 1 - <?php echo sizeof($archived_arr);?> of <?php echo sizeof($archived_arr);?> messages</p>
            <div class="table-responsive">
              <table class="table table-email">
                <tbody>
                	<?php foreach($archived_arr as $archived){?>
                      <form action="<?php base_url();?>read" id="readmailfrm" method="post">
                        	<input type="hidden" name="message_id" value="<?php echo $archived->id;?>">
                      </form>
                      <tr>
                        <td><div class="ckbox ckbox-primary">
                            <input type="checkbox" id="checkbox1" name="message_id[]" value="<?php echo $archived->id;?>">
                            <label for="checkbox1"></label>
                          </div></td>
                        <td><a class="star" href=""><i class="glyphicon glyphicon-star"></i></a></td>
                        <td><div class="media">
                            <div class="media-body"> <span class="media-meta pull-right"><?php echo generateDate($archived->created_at);?></span>
                              <h4 class="text-primary"><?php echo $archived->user_name;?></h4>
                              <small class="text-muted"></small>
                              <p class="email-summary"><?php echo shortPara($archived->message, '100');?></p>
                            </div>
                          </div></td>
                      </tr>
       				<?php }?>
                </tbody>
              </table>
            </div><!-- /table-responsive --> 
          </div><!--/ block-web --> 
        </div><!-- /col-sm-9 --> 