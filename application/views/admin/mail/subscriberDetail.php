IQ
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12"> </div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Add Message</h3>
					</div>
					<div class="clearfix"></div>
					<div class="tab-content">
						<div class="porlets-content"  >
							<div class="table-responsive tab-pane" id="menu1">
								<div class="clearfix"> </div>
								<div class="margin-top-10"></div>
								<table  class="display table table-bordered table-striped">
									<thead>
										<tr>
											<th>No:</th>
											<th>Name</th>
											<th>Email</th>
											<!--<th>Organization Name</th>  -->
											<th>Open Count</th>
										</tr>
									</thead>
									<tbody>
										<?php 
				   $i = 1;
				   ?>
										<?php foreach($subscriberDetail as $subscriberDetails){?>
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $subscriberDetails->name;?></td>
											<td><?php echo $subscriberDetails->email;?></td>
											<!-- <td><?php //echo $subscriberDetail->name;?></td>-->
											<td><?php echo getUserTrackingCount($subscriberDetails->id, $email_track_id);?></td>
										</tr>
										<?php $i++;  ?>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<!--/table-responsive--> 
							
						</div>
						<!--/porlets-content--> 
						
					</div>
					<!--/block-web--> 
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row--> 
		
	</div>
	<!--/page-content end--> 
</div>
<!--/main-content end-->
</div>
