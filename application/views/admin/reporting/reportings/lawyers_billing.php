<div class="table-responsive">
	<div class="clearfix"></div>
	<div class="margin-top-10"></div>
	<table  class="display table table-bordered table-striped" id="dynamic-table">
		<thead>
			<tr>
				<th style="width:150px;">Name</th>
				<th>Balance</th>
				<th>Law Firm</th>
				<th>Date Of Last Lead</th>
				<th>Leads Detail</th>          
				<th>Account Billing</th> 
			</tr>
		</thead>
		<tbody class="searchable">
			<?php
				$total_balance;
				if($lawyers)
				{
					$i = 0;	
					foreach($lawyers as $row){
						$balance = number_format(($row->total_lead_price+$row->monthly_payment+$row->intialSetupChargeTotal+$row->oneTimeChargeTotal-$row->paidTransactions), 2);
						$total_balance = $total_balance + $balance;
						?>       
						<tr>
							<td><a href="<?php echo base_url();?>admin/reporting/reportings/<?php echo $row->id;?>"><?php echo $row->name; ?></a></td>
							<td align="right"><?php echo '$' . $balance; ?></td>
							<td><?php echo getLawsNames($row->law_firms); ?></td>
							<td><?php echo getLawyerLastLeadDate($row->id); ?></td>
							<td> <a href="<?php echo base_url();?>admin/dashboard/LawyerAllLeads/<?php echo $row->id;?>">Details</a></td>
							<td><a href="<?php echo base_url();?>admin/dashboard/LawyerBills/<?php echo $row->id;?>">Details</a></td>
						</tr>
						<?php 
						$i++;
					} 
				} ?>
		</tbody>
	</table>
</div>
<!--/table-responsive-->
<h3 class="content-header">Total Balance: <?php echo "$" . $total_balance; ?></h3>