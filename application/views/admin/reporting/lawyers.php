<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Lawyers Reporting <?php echo $type; ?></h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
					</div>
					<div class="porlets-content">
						<div class="porlets-content">
							<div class="col-md-12 text-center">
								<span style="color:green; text-align:center; font-size:22px; font-weight:700; display:none;" id="success_message"> Success </span>
							</div>
							<div class="col-md-12">
								<span id="show_table2"></span>
							</div>
							<div id="search_result" style="display:none;">
								<div class="table-responsive">
									<div class="clearfix"></div>
									<div class="margin-top-10"></div>
									<table class="display table table-bordered table-striped" >
										<tbody id="table_content">			  </tbody>
									</table>
									<br/>
								</div>
							</div>
							<div id="lawyers_table">
								<?php $this->load->view('admin/reporting/reportings/lawyers_billing'); ?>   
							</div>
						</div>
						<!--/porlets-content-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->               
	</div>
	<!--/page-content end--> 
</div>