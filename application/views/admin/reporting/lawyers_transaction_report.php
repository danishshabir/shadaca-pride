<?php if($date_from_transactions && $date_to_transactions && $charges) { ?>
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>admin/reporting/export_transactions/<?php echo $date_from_transactions . "/" . $date_to_transactions . "/" . $charges;?>"> <button style="float:right" class="btn btn-info">Export</button> </a>
	</div>
<?php } ?>
<div class="table-responsive">
	<div class="clearfix"></div>
	<div class="margin-top-10"></div>
	<?php if(count($transactions) > 0) { ?>
		<table  class="display table table-bordered table-striped" id="dynamic-table">
			<thead>
				<tr>
					<th>Amount</th>
					<th>Credit debit</th>
					<th>Lawyer name</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($transactions as $lawyer_data) {
					$transaction_date = date('Y-m', strtotime($lawyer_data->transaction_date));
					if ($lawyer_data->lead_bill != '') {
						$bill = $lawyer_data->lead_bill;
					} else {
						$bill = $lawyer_data->monthly_bill;
					}
					$total_bill_var = number_format($bill, 2);
					$lawyer_name    = getLawyer($lawyer_data->lawyer_id);
					$d              = $lawyer_data->transaction_date;
					$date           = strtotime($d);
					$new_date       = date('M d Y - H:i', $date);
					?>
					<tr>
						<td><?php echo '$' . $total_bill_var;?></td>
						<td><?php echo $lawyer_data->description; ?></td>
						<td><?php echo $lawyer_name->name . " " . $lawyer_name->lname;  ?></td>
						<td><?php echo $new_date;?></td>
					</tr>
					<?php 
					$total_amount += $lawyer_data->amount;
				}
				?>
			</tbody>
		</table>
	<?php } else {
		?><div class="text-center" style="font-size:20px; font-weight:700;">No result Found</div><?php
	} ?>
</div>