<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Reportings </h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header text-center" style="font-weight:700;"><?php echo "Lawyer name: " . $lawyer->name . " " . $lawyer->lname; ?>	</h3>
					</div>
					<?php if($this->session->userdata('role') == '1') { ?>
					<div class="btn-group pull-right">
						<a class="print_invoice" href="javascript:void(0)"><button class="btn btn-success">Print </button></a> 
					</div>
					<?php } else { ?>		   
					<div class="btn-group pull-right">
						<a href="<?php echo base_url();?>admin/leadInvoice/LawyerLeadInvoice/<?php echo $lawyer->id;?>" target="_blank"><button class="btn btn-success">Print </button></a> 
					</div>
					<?php } ?>				   
					<?php $l_id = $this->uri->segment(4); ?>
					<?php if($this->session->userdata('role') == '1') { ?>
						<a href="<?php echo base_url();?>admin/dashboard/oneTimeCharge/<?php echo $l_id;?>" class="btn btn-info"> Make one time charge</a>
					<?php } ?>
					<div class="tab-content">
						<div>
							<div class="porlets-content">
								<div class="table-responsive">
									<table  class="display table table-bordered table-striped">
										<thead>
											<tr>
												<th>Lawyer name</th>
												<th>Leads bill</th>
												<th>Monthly bill</th>
												<th>Other Charges</th>
												<th>Total payments</th>
												<th>Balance </th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<?php 
												$lawyer_data = getLawyer($lawyer->id);?>
												<td>
													<?php echo $lawyer_data->name . " " . $lawyer_data->lname; ?>
												</td>
												<td><?php echo "$" . number_format((float)$lead_bill_new, 2, ".", ""); ?></td>
												<td><?php echo "$" . number_format((float)$monthly_bill_new, 2, ".", ""); ?></td>
												<td><?php echo "$" . number_format((float)$total_charge_bill, 2, ".", ""); ?></td>
												<td><?php echo "$" . number_format((float)$total_payments, 2, ".", ""); ?></td>
												<td align="right"><?php echo "$" . number_format((float)$total, 2, ".", ""); ?></td>
											</tr>
										</tbody>
									</table>
								</div>
								<!--/table-responsive-->         
							</div>
							<!--/porlets-content-->
						</div>
						<!--/fade_in-->	
					</div>
					<!--/tab-content-->
					<div class="form-group col-md-3">
						<label>Date from</label>
						<input type="text" name="date_from" parsley-trigger="change" placeholder="Enter Date From" class="form-control datepicker-here" data-date-format="mm-dd-yyyy" data-language="en">
					</div>
					<div class="form-group col-md-3">
						<label>Date to</label>
						<input type="text" name="date_to" parsley-trigger="change" placeholder="Enter Date To" class="form-control datepicker-here" data-date-format="mm-dd-yyyy" data-language="en">
					</div>
					<div class="form-group col-md-3">
						<label>Charges/ Payments</label>
						<select placeholder="Charges/Payments" class="form-control" name="charges">
							<option value=""> Select type </option>
							<option value="1"> Charges </option>
							<option value="2"> Payments </option>
							<option value="3"> Charges and Payments </option>
							<option value="4"> Lead Charges </option>
							<option value="5"> Monthly Fees </option>
							<option value="6"> One Time Charges </option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>&nbsp;</label>
						<button class="form-control btn btn-success search_transactions">search</button>
					</div>
					<div class="col-md-12">
						<h4 style="text-align:center;">Lawyer Transaction </h4>
					</div>
					<!--/col-md-12--> 
					<div class="clearfix"></div>
					<div class="porlets-content">
						<span id="show_table2"></span>  
						<div class="table-responsive transactions">
							<?php $this->load->view('admin/users/transactions/transactions'); ?>
						</div>
						<!--/table-responsive-->         
					</div>
					<!--/porlets-content-->	
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<script>
	$(document).ready(function(){
		load_dataTable();
	});
	function load_dataTable()
	{
		$('.transactions table').dataTable({
			"bSort": false,
			"iDisplayLength": 100
		});
	}
	$(".search_transactions").click(function(e) 
	{
		$(".page_loader").show();
		$.ajax({
			type: "POST",
			url: '<?php echo base_url();?>admin/reporting/search_transactions',
			data: {
				date_from: $("[name=date_from]").val(),
				date_to: $("[name=date_to]").val(),
				charges: $("[name=charges]").val(),
				lawyer_id: '<?php echo $lawyer->id; ?>'
			},
			success: function(response) {
				$(".page_loader").hide();
				data = jQuery.parseJSON(response);
				$(".transactions").html(data.html);
				load_dataTable();
			}
		});
		e.preventDefault();
	});
	$(".print_invoice").click(function(e) 
	{
		var date_from = $("[name=date_from]").val();
		var date_to = $("[name=date_to]").val();
		var charges = ($("[name=charges]").val() ? $("[name=charges]").val() : 3);
		if(date_from && date_to) {
			window.open('<?php echo base_url();?>admin/leadInvoice/LawyerLeadInvoice/<?php echo $lawyer->id;?>/'+charges+'/'+date_from+'/'+date_to, '_blank');
		} else {
			window.open('<?php echo base_url();?>admin/leadInvoice/LawyerLeadInvoice/<?php echo $lawyer->id;?>', '_blank');
		} 
	});
</script>