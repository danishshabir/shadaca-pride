<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Reporting (Law Firm)</h2>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Reporting (Law Firm)</h3>
					</div>
					<div class="porlets-content">
						<div class="table-responsive">
							<?php echo $this->session->flashdata('message')?>
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
								<tr>
									<th>Law Firm</th>
									<th>Contact Email</th>
									<th>Phone</th>
									<th>City</th>
									<th>State</th>
									<!--
                                    <th>Logo</th>
                                    -->
								</tr>
								</thead>
								<tbody>
								<?php foreach($casetype as $type){?>
									<tr class="gradeX">
										<td><a href="<?php echo base_url();?>admin/reporting/lawyers_report/<?php echo $type->id; ?>"><?php echo $type->law_firm; ?></a></td>
										<td><?php echo $type->contact_email; ?></td>
										<td><?php echo $type->phone; ?></td>
										<td><?php echo $type->city; ?></td>
										<td><?php echo getStateShortName($type->state); ?></td>
										<!--
                                        <td class="text-center"> <?php if($type->image != ''){?><img src="<?php echo base_url();?>uploads/<?php echo $type->image;?>" height="40" width="40"> <?php }else{?> <img src="<?php echo base_url();?>assets/admin/images/no-image.jpg" height="40" width="40"> <?php }?></td>
                                        -->
                                    </tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web-->
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end-->
</div>
<!--/main-content end-->
</div>