<div id="main-content">

    <div class="page-content">

      <div class="row">

        <div class="col-md-12">

          <h2>Lawyer Reporting <?php echo $type; ?></h2>

        </div><!--/col-md-12--> 

      </div><!--/row-->

      

      

      

      <div class="row">

        <div class="col-md-12">

          <div class="block-web">

            <div class="header">

              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>

             

            </div>

            <div class="porlets-content">

			
			<!--
              <form action="<?php echo base_url();?>admin/dashboard/ChecklawyerBilling" method="post" parsley-validate novalidate class="ajaxFrm" id="search_Lawyer_billing">

			  
			  	<div class="form-group col-md-3">
					<label>Select billing type</label>
                 <select placeholder="Billing type" class="form-control" name="billing_type" id="billing_type">
                    
					 <option value=""> Select billing type </option> 
					 <option value="1"> Leads billing </option> 
					 <option value="2"> Monthly billing </option> 
					
                    </select>
                </div>
				
			  
			  
                <div class="form-group col-md-2">

                  <label>Date from</label>

                  <input type="text" name="date_from" id="datepicker" parsley-trigger="change" placeholder="Enter Date From" class="form-control">

                </div>
				
				
				 <div class="form-group col-md-2">

                  <label>Date to</label>

                  <input type="text" name="date_to" id="datepicker2"  parsley-trigger="change" placeholder="Enter Date To" class="form-control">

                </div>
				
			<div class="form-group col-md-2">
				 <label style="color:#fff">Search</label><br/>
                <button class="btn btn-primary" type="submit">Search</button>
				
			</div>   
			</form>
	-->
	
	<div class="form-group col-md-12"></div>
					

 <div class="porlets-content">    
			  
	<div id="all_lawyers_table">      
	
	
	<div class="table-responsive">      
			  <div class="clearfix"></div> 
			  <div class="margin-top-10"></div>  
			 
			  
              <table  class="display table table-bordered table-striped" id="dynamic-table">
			  			  
			  <thead>    
			  <tr>     
			  <th style="width:150px;">Name</th>   
			  <th>Balance</th>                  
		<!--  <th>Leads Bill</th>         
			  <th>Monthly Bill</th> -->
			<!--  <th>Lawyer Invoice</th> -->
			  
			  </tr>                  
			  </thead>    

			  <tbody class="searchable">          
			      
			  <tr>      
			    <td><a href="<?php echo base_url();?>admin/reporting/reportings/<?php echo $all_lawyer->id;?>"><?php echo $all_lawyer->name; ?></a></td> 
			    <td><?php echo '$' . $totalArrays[0] . '.00'; ?></td> 
			 <!--   <td> <a href="<?php //echo base_url();?>admin/dashboard/LawyerAllLeads/<?php //echo $all_lawyer->id;?>">Details</a></td>  
			   
			   <td> <a href="<?php //echo base_url();?>admin/dashboard/LawyerBillings/<?php //echo $all_lawyer->id;?>">Details</a></td>  
			    <td> 
				<a href="<?php //echo base_url();?>admin/dashboard/LawyerMonthlyBilling/<?php //echo $all_lawyer->id;?>">Details</a></td> 
		-->
		<!--
				<td> 
				<a href="<?php //echo base_url();?>admin/leadInvoice/LawyerLeadInvoice/<?php //echo $all_lawyer->id;?>" target="_blank">Invoice</a></td> 
		-->		
								
			  </tr>     
             
			  </tbody>		   		
		  			
	</table>  
			
			  </div><!--/table-responsive-->   
      </div>
     </div><!--/porlets-content-->
			  

<div class="col-md-12 text-center">
<span style="color:green; text-align:center; font-size:22px" id="success_message"></span>
</div>

<div id="search_result" style="display:none;">
    			  
<div class="table-responsive">    
<div class="clearfix"></div>
<div class="margin-top-10"></div>  
<table class="display table table-bordered table-striped" >

<tbody id="table_content">			  
</tbody>		   				
</table>  
<br/>


</div>	  
			  </div>	  
			  
            </div><!--/porlets-content-->

          </div><!--/block-web--> 

        </div><!--/col-md-6-->

      </div><!--/row-->               

    </div><!--/page-content end--> 

  </div>
  
    <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  $(function() {
    $( "#datepicker2" ).datepicker();
  });
    
	
	
	
		 $("#search_Lawyer_billing").submit(function(e){
        $form = $(this);
        var url = $form.attr('action');
		
			$.ajax({
            type: "POST",
            url: url,
            data: $form.serialize(),
            success: function(response)
            {
                  //jQuery.parseJSON(data);
                    if (response != '') {
						//alert(response.html);
						$("#table_content").html(response);
						$("#search_result").show();	
						$("#success_message").html('Success');	
						$("#all_lawyers_table").hide();
						$('#success_message').delay(5000).fadeOut('slow');
												
                    }else{
                        $("#success_message").hide();
                        $("#search_result").hide();
						$("#all_lawyers_table").show();
                    }
               

            }
        });	
        e.preventDefault();
    });
	
	
  </script>