<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Email Templates</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Email Templates </h3>
					</div>
					<div class="porlets-content">
						<div class="table-responsive">
							<div class="clearfix">
								<!--
									<div class="btn-group">
									    <a href="<?php //echo base_url();?>admin/settings/addEmailTemplate"> <!-- 2 is role that define type. 2 is for lawyers -->
								<!-- <button class="btn btn-primary">
									Add New Email Template<i class="fa fa-plus"></i>
									</button></a>
									</div> -->
							</div>
							<?php echo $this->session->flashdata('message')?>
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
										<th>Title (Trigger) </th>
										<th>Email From</th>
										<th>Subject</th>
										<th>Content</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($email_templates as $template){?>
									<tr class="gradeX">
										<td><?php echo $template->title; ?></td>
										<td><?php echo $template->email_from; ?></td>
										<td><?php echo $template->subject; ?></td>
										<td><?php echo str_replace("{image}", '<img src="'.base_url().'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $template->content); ?></td>
										<td style="cursor:pointer"><a href="<?php echo base_url();?>admin/settings/editEmailTemplate/<?php echo $template->id;?>" >Edit </a> | <a data-toggle="modal" data-target="#emailTestModal<?php echo $template->id;?>">Test email template</a> | <a data-toggle="modal" data-target="#myModal<?php echo $template->id;?>">Delete </a></td>
										<!-- 2 define role which is specify it is lawyer -->					  					  
										<div class="modal fade" id="myModal<?php echo $template->id;?>" role="dialog">
											<div class="modal-dialog modal-sm">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>							  
														<h4 class="modal-title">Confirm Delete</h4>
													</div>
													<div class="modal-body">
														<p>Are you sure you want to delete this email template?</p>
													</div>
													<div class="modal-footer">							  <a class="btn btn-primary" <a href="<?php echo base_url();?>admin/settings/delete_email_template/<?php echo $template->id;;?>" >Yes </a>							  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>							</div>
												</div>
											</div>
										</div>

                                        <div class="modal fade" id="emailTestModal<?php echo $template->id;?>" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Test email template</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label>Please enter email address: </label>
                                                            <input class="form-control" type="text" name="emailaddress<?php echo $template->id;?>" id="emailaddress<?php echo $template->id;?>" value="<?php echo $admin_email ?>">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-primary" href="javascript:void(0)" onclick="testEmailTemplate('<?php echo base_url();?>', '<?php echo $template->id;?>')">Send </a></div>
                                                </div>
                                            </div>
                                        </div>

									</tr>
									<?php } ?> 
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<!--/main-content end-->
</div>