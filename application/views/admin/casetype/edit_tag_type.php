<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Edit Tags And Labels </h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Tags And Labels Form</h3>
            </div>
            <div class="porlets-content">
             
			 <form action="<?php echo base_url();?>admin/leads/update_tag_type/<?php echo $id;?>" method="post" onsubmit="return validatePhone();" parsley-validate novalidate>
                
				
            <div class="col-md-4">
				<div class="form-group">
                  <label>Tag</label>
                  <input type="text" name="tags" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $tag->tags; ?>">
                </div><!--/form-group-->             </div>			 			  <div class="col-md-12">
					<button class="btn btn-primary" type="submit">Submit</button>
                    <a href="<?php echo base_url();?>admin/settings/tags_label" class="btn btn-default">Cancel</a>			  </div>							<div class="clearfix"></div>					
			 </form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>
   <script type="text/javascript">
  	function validatePhone()
	{
		var home_phone = $('#home_phone');
		var work_phone = $('#work_phone');
		var mobile = $('#mobile');
		if(home_phone.val() == '' && work_phone.val() == '' && mobile.val() == '')
		{
			alert('Please enter a phone no!');
			home_phone.focus();
			return false;
		}
	}
  </script>