<div id="main-content">

	<div class="page-content">

		<div class="row">

			<div class="col-md-12">

				<h2>Add New Law Firm</h2>

			</div>

			<!--/col-md-12--> 

		</div>

		<!--/row-->

		<div class="row">

			<div class="col-md-12">

				<div class="block-web">

					<div class="header">

						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>

						<h3 class="content-header">Law Firm</h3>

					</div>

					<?php echo $this->session->flashdata('message')?>

					<div class="porlets-content">

						<form action="<?php echo current_url();?>" method="post" enctype="multipart/form-data">

							<div class="form-group col-md-4">

								<label>Firm name *</label>

								<input type="text" name="law_firm"   placeholder="Firm name" class="form-control" value="<?php echo $this->input->post('law_firm'); ?>">

								<?php echo (form_error('law_firm') ? form_error('law_firm') : ''); ?>

							</div>

							<!--/form-group-->

							<div class="form-group col-md-4">

								<label>Phone *</label>

								<input type="text" name="phone"   placeholder="Enter phone no:" class="form-control" value="<?php echo $this->input->post('phone'); ?>">

								<?php echo (form_error('phone') ? form_error('phone') : ''); ?>

							</div>

							<!--/form-group-->

							<div class="form-group col-md-4">

								<label>Website</label>

								<input type="text" name="website"   placeholder="Enter website" class="form-control" value="<?php echo $this->input->post('website'); ?>">

								<?php echo (form_error('website') ? form_error('website') : ''); ?>

							</div>

							<div class="form-group col-md-4">

								<label>Contact first name *</label>

								<input type="text" name="contact_name" placeholder="Contact first name" class="form-control" value="<?php echo $this->input->post('contact_name'); ?>">

								<?php echo (form_error('contact_name') ? form_error('contact_name') : ''); ?>

							</div>

							<!--/form-group-->

							<div class="form-group col-md-4">

								<label>Contact last name *</label>

								<input type="text" name="contact_last_name" placeholder="Contact last name" class="form-control" value="<?php echo $this->input->post('contact_last_name'); ?>">

								<?php echo (form_error('contact_last_name') ? form_error('contact_last_name') : ''); ?>

							</div>

							<!--/form-group-->

							<div class="form-group col-md-4">

								<label>Contact email *</label>

								<input type="email" name="contact_email"   placeholder="Enter contact email" class="form-control" value="<?php echo $this->input->post('contact_email'); ?>">

								<?php echo (form_error('contact_email') ? form_error('contact_email') : ''); ?>

							</div>

							<!--/form-group-->

							<div class="form-group col-md-4">

								<label>Street *</label>

								<input type="text" name="street"   placeholder="Enter street" class="form-control" value="<?php echo $this->input->post('street'); ?>">

								<?php echo (form_error('street') ? form_error('street') : ''); ?>

							</div>

							<!--/form-group-->

							<div class="form-group col-md-4">

								<label>City *</label>

								<input type="text" name="city"   placeholder="Enter city name" class="form-control" value="<?php echo $this->input->post('city'); ?>">

								<?php echo (form_error('city') ? form_error('city') : ''); ?>

							</div>

							<!--/form-group-->

							<div class="form-group col-md-2">

								<div class="form-group">

									<label>State *</label>

									<select placeholder="state" class="form-control" name="state" >

										<option value="" > Select state </option>

										<?php foreach($states_short_names as $state){?>

										<option value="<?php echo $state->id;?>"> <?php echo $state->state_short_name;?> </option>

										<?php }?>

									</select>

									<?php echo (form_error('state') ? form_error('state') : ''); ?>

								</div>

							</div>

							<div class="form-group col-md-2">

								<label>Zip *</label>

								<input type="text" name="zip"   placeholder="Enter zip" class="form-control" maxlength="5" value="<?php echo $this->input->post('zip'); ?>">

								<?php echo (form_error('zip') ? form_error('zip') : ''); ?>

							</div>

							<!--/form-group-->
                                <div class="form-group col-md-12">

						<label>Law firm - internal note (visible to admin only)</label>

						<textarea name="internal_notes"   placeholder="Add internal notes" class="form-control" rows="4"></textarea>

					</div>
					 <div class="form-group col-md-12">

						   <label>Law firm - account notes (visible to law firm)</label>

						<textarea name="shared_notes"   placeholder="Add law firm notes" class="form-control" value = "<?php echo $this->input->post('shared_notes'); ?>" rows="4"></textarea>

					      </div>
							<!--<div class="form-group col-md-4">

								<label>Company logo</label>

								<input type="file" name="image"  class="form-control no-margin" />

							</div>-->

							<div class="form-group col-md-4">

								<div class="form-group">

									<label>Fee type *</label>

									<select placeholder="Fee Type" class="form-control" name="fee_type" >

										<option value="" > Select Type </option>

										<option value="0"> Monthly </option>

										<option value="1"> Annual </option>

									</select>

									<?php echo (form_error('fee_type') ? form_error('fee_type') : ''); ?>

								</div>

							</div>

							<div class="form-group col-md-4">

								<label>Fee *</label>

								<input type="text" name="law_firm_fee"   placeholder="Fee" class="form-control" value="<?php echo $this->input->post('law_firm_fee'); ?>">

								<?php echo (form_error('law_firm_fee') ? form_error('law_firm_fee') : ''); ?>

							</div>

							<!--/form-group-->

							<input type="hidden" name="password" value="<?php echo generateRandomPassword(6); ?>" />

							<input type="hidden" name="submit_form_type" value="0" />

							<div class="clearfix"></div>

							<div class="form-group col-md-12">

								<button class="btn btn-primary form_submit" type="submit" data-type="0">Save and Exit</button>

								<button class="btn btn-primary form_submit" type="submit" data-type="1">Add New Lawyer</button>

								<a href="<?php echo base_url();?>admin/settings/lawFirms" class="btn btn-default">Cancel</a>

							</div>

							<div class="clearfix"></div>

						</form>

					</div>

					<!--/porlets-content-->

				</div>

				<!--/block-web--> 

			</div>

			<!--/col-md-6-->

		</div>

		<!--/row-->

	</div>

	<!--/page-content end--> 

</div>

<script>

$(".form_submit").click(function(e){

	e.preventDefault();

	$("[name='submit_form_type']").val($(this).attr('data-type'));

	$(this).parents('form').first().submit();

});

$("[name='state']").val('<?php echo ($this->input->post('state')) ?>');

$("[name='fee_type']").val('<?php echo ($this->input->post('fee_type')) ?>');

</script>