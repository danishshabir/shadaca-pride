<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Law Firms</h2>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Law Firms</h3>
					</div>
					<div class="porlets-content">
						<div class="table-responsive">
							<div class="clearfix">
								<div class="btn-group">
									<a href="<?php echo base_url();?>admin/settings/addFirmtype">
										<!-- 2 is role that define type. 2 is for lawyers -->
										<button class="btn btn-primary">
										Add New <i class="fa fa-plus"></i>
										</button>
									</a>
								</div>
								<button style="float: right;" type="button" id="exportCsv" data-name="lawfirms" class="btn btn-primary">Export CSV</button>
							</div>
							<?php echo $this->session->flashdata('message')?>
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
										<th>Law Firm</th>
										<th>Contact Email</th>
										<th>Phone</th>
										<th>City</th>
										<th>State</th>
										<!--<th>Logo</th>-->
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($casetype as $type){?>
									<tr class="gradeX">
										<td><?php echo $type->law_firm; ?></td>
										<td><?php echo $type->contact_email; ?></td>
										<td><?php echo $type->phone; ?></td>
										<td><?php echo $type->city; ?></td>
										<td><?php echo getStateShortName($type->state); ?></td>
										<!--<td class="text-center"> <?php if($type->image != ''){?> <img src="<?php echo base_url();?>uploads/<?php echo $type->image;?>" height="40" width="40"> <?php }else{?> <img src="<?php echo base_url();?>assets/admin/images/no-image.jpg" height="40" width="40"> <?php }?></td>-->
										<td style="cursor:pointer"><a href="<?php echo base_url();?>admin/users/frm_view/<?php echo $type->id ?>" >View</a> | <a href="<?php echo base_url();?>admin/settings/editLawtype/<?php echo $type->id;?>" >Edit</a> | <a data-toggle="modal" data-target="#myModal<?php echo $type->id;?>">Delete</a> | <a href="<?php echo base_url();?>admin/users/manage_payment_profile/<?php echo $type->id ?>" >Payment Info</a>
											<?php if ($type->can_login == '0')
											{ ?>
												<a id="unblockBtn_<?php  echo $type->id; ?>" href="javascript:void(0);" onclick="unblockUser('<?php echo $type->id; ?>','6');">| Unblock</a>&nbsp;
											<?php }
											?>
										|&nbsp;<a href="<?php echo base_url();?>admin/users/view_as/<?php echo $type->id;?>/lawfirm" >View As</a>
										</td>
										<!-- 2 define role which is specify it is lawyer -->
										<div class="modal fade" id="myModal<?php echo $type->id;?>" role="dialog">
											<div class="modal-dialog modal-sm">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Confirm Delete</h4>
													</div>
													<div class="modal-body">
														<p>Are you sure you want to delete this record ?</p>
													</div>
													<div class="modal-footer">							  <a class="btn btn-primary" <a href="<?php echo base_url();?>admin/settings/delete_law/<?php echo $type->id;?>" >Yes </a>							  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>							</div>
												</div>
											</div>
										</div>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web-->
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end-->
</div>
<!--/main-content end-->
</div>