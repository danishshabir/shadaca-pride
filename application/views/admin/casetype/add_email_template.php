<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Add New Email Template</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Email Template</h3>
            </div>
            <?php echo $this->session->flashdata('message')?>
            <div class="porlets-content">
              <form action="<?php echo base_url();?>admin/settings/saveEmailTemplate" method="post" parsley-validate novalidate>
                
				
				
				
				<div class="form-group col-md-6">
                  <label>Email From *</label>
                  <input type="email" name="email_from" parsley-trigger="change" required placeholder="Enter Email" class="form-control">
                </div><!--/form-group-->
				
				<div class="form-group col-md-6">
                  <label>Subject *</label>
                  <input type="text" name="subject" parsley-trigger="change" required placeholder="Enter Subject" class="form-control">
                </div><!--/form-group-->
				
				
				
				<div class="form-group col-md-6">
                  <label>Text Message *</label>
                  <input type="text" name="text_message" parsley-trigger="change" required placeholder="Enter Message" class="form-control">
                </div><!--/form-group-->
				
				<div class="form-group col-md-6">
                  <label>Tittle *</label>
                  <input type="text" name="title" parsley-trigger="change" required placeholder="Enter Tittle" class="form-control">
                </div><!--/form-group-->

				
				<div class="form-group col-md-6">
                  <label>App Alert *</label>
                  <input type="text" name="app_alert" parsley-trigger="change" required placeholder="Enter App Alert" class="form-control">
                </div><!--/form-group-->
				<div class="clearfix"></div>
				
				
				<br>
				<div class="form-group">
				
				<label>Content *</label>
      
				  <div class="compose-editorr" style="margin-top:10px;">
                  <textarea id="text-editor" placeholder="Enter Content" class="col-xs-12" name="content" rows="8"      parsley-trigger="change" required></textarea>
                  </div>
				  
               </div>
				
				
				
				 
              

				
              
                <button class="btn btn-primary" type="submit">Submit</button>
                     <a href="<?php echo base_url();?>admin/settings/email_templates" class="btn btn-default">Cancel</a>
			</form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>