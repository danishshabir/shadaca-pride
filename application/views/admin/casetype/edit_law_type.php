<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Edit Law Firm</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Edit Law Firm Form</h3>
					</div>
					<?php echo $this->session->flashdata('message')?>
					<div class="porlets-content">
						<form action="<?php echo current_url();?>" method="post" enctype="multipart/form-data">
							<div class="form-group col-md-4">
								<label>Firm name *</label>
								<input type="text" name="law_firm"  value="<?php echo ($this->input->post('law_firm') ? $this->input->post('law_firm') : $lead->law_firm); ?>"  placeholder="Firm name" class="form-control">
								<?php echo (form_error('law_firm') ? form_error('law_firm') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Phone *</label>
								<input type="text" name="phone" value="<?php echo ($this->input->post('phone') ? $this->input->post('phone') : $lead->phone); ?>"   placeholder="Enter phone no:" class="form-control">
								<?php echo (form_error('phone') ? form_error('phone') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Website</label>
								<input type="text" name="website" value="<?php echo ($this->input->post('website') ? $this->input->post('website') : $lead->website); ?>"   placeholder="Enter website" class="form-control">
								<?php echo (form_error('website') ? form_error('website') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Contact first name *</label>
								<input type="text" name="contact_name" value="<?php echo ($this->input->post('contact_name') ? $this->input->post('contact_name') : $lead->contact_name); ?>"   placeholder="Contact first name" class="form-control">
								<?php echo (form_error('contact_name') ? form_error('contact_name') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Contact last name *</label>
								<input type="text" name="contact_last_name"  value="<?php echo ($this->input->post('contact_last_name') ? $this->input->post('contact_last_name') : $lead->contact_last_name); ?>"  placeholder="Contact last name" class="form-control">
								<?php echo (form_error('contact_last_name') ? form_error('contact_last_name') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Contact email *</label>
								<input type="email" name="contact_email" value="<?php echo ($this->input->post('contact_email') ? $this->input->post('contact_email') : $lead->contact_email); ?>"   placeholder="Enter contact email" class="form-control">
								<?php echo (form_error('contact_email') ? form_error('contact_email') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Street *</label>
								<input type="text" name="street"  value="<?php echo ($this->input->post('street') ? $this->input->post('street') : $lead->street); ?>"  placeholder="Enter street" class="form-control">
								<?php echo (form_error('street') ? form_error('street') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>City *</label>
								<input type="text" name="city"   placeholder="Enter city here" class="form-control" value="<?php echo ($this->input->post('city') ? $this->input->post('city') : $lead->city); ?>">
								<?php echo (form_error('city') ? form_error('city') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-2">
								<div class="form-group">
									<label>State *</label>
									<select placeholder="state" class="form-control" name="state" >
										<option value="" > Select state </option>
										<?php foreach($states_short_names as $state){?>
										<option value="<?php echo $state->id;?>"> <?php echo $state->state_short_name;?> </option>
										<?php }?>
									</select>
									<?php echo (form_error('state') ? form_error('state') : ''); ?>
								</div>
							</div>
							<div class="form-group col-md-2">
								<label>Zip *</label>
								<input type="text" name="zip"   placeholder="Enter zip here" class="form-control" value="<?php echo ($this->input->post('zip') ? $this->input->post('zip') : $lead->zip); ?>" maxlength="5">
								<?php echo (form_error('zip') ? form_error('zip') : ''); ?>
							</div>                            <div class="form-group col-md-12">							 <label>Law firm - internal note (visible to admin only)</label>					<textarea name="internal_notes"   value="<?php echo ($this->input->post('internal_notes') ? $this->input->post('internal_notes') : $lead->internal_notes); ?>" class="form-control" rows="4" ><?php echo $lead->internal_notes; ?></textarea>							 							 </div>							 <div class="form-group col-md-12">                          						  	 <label>Law firm - account notes (visible to law firm)</label>					<textarea name="shared_notes"   value="<?php echo ($this->input->post('shared_notes') ? $this->input->post('shared_notes') : $lead->shared_notes); ?>" class="form-control" rows="4" ><?php echo $lead->shared_notes; ?></textarea>							 							 </div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<div class="form-group">
									<label>Fee type *</label>
									<select placeholder="Fee Type" class="form-control" name="fee_type" >
										<option value="" > Select Type </option>
										<option value="0"> Monthly </option>
										<option value="1"> Annual </option>
									</select>
									<?php echo (form_error('fee_type') ? form_error('fee_type') : ''); ?>
								</div>
							</div>
							<div class="form-group col-md-4">
								<label>Fee *</label>
								<input type="text" name="law_firm_fee"  value="<?php echo number_format($lead->law_firm_fee, 2); ?>"  placeholder="Fee" class="form-control" value="<?php echo ($this->input->post('law_firm_fee') ? $this->input->post('law_firm_fee') : $lead->law_firm_fee); ?>">
								<?php echo (form_error('law_firm_fee') ? form_error('law_firm_fee') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Customer ID</label>
								<input type="text" value="<?php echo $lead->customer_id; ?>"  placeholder="Enter customer ID" class="form-control" readonly>
							</div>
							<div class="form-group col-md-12">
								<label>&nbsp;</label>
								<p><a class="btn btn-primary" id="resetPass" attr-user-type="law_firms" attr-user-id="<?php echo $lead->unique_code; ?>" href="javascript:void(0);">Reset Password</a></p>
							</div>
							<div class="clearfix"></div>
							<!--/form-group-->
							<!--<div class="form-group col-md-4">
								<label>Company logo</label><br>
								<?php if($lead->image == ''){?>
								<img src="<?php echo base_url();?>/assets/admin/images/no-image.jpg" style="width:105px; height:125px;"></img>
								<?php
									}else{?>
								<img src="<?php echo base_url();?>uploads/<?php echo $lead->image;?>" style="width:105px; height:125px;"></img>
								<?php }?>
								<input type="file" name="image"/>
								<input type="hidden" name="image" value="<?php echo $lead->image;?>"/>
							</div>-->
							<!--/form-group-->
							<div class="clearfix"></div>
							<div class="form-group col-md-12">
								<button class="btn btn-primary" type="submit">Submit</button>
								<!--<button class="btn btn-default">Cancel</button>-->
								<a href="<?php echo base_url();?>admin/settings/lawFirms" class="btn btn-default">Cancel</a>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<script type="text/javascript">
	$("[name='state']").val('<?php echo ($this->input->post('state') ? $this->input->post('state') : $lead->state) ?>');
	$("[name='fee_type']").val('<?php echo ($this->input->post('fee_type') ? $this->input->post('fee_type') : $lead->fee_type) ?>');
	
	$('#resetPass').on('click', function(){
		if(confirm('Are You Sure? You Want To Reset Password?')){
			var user_id = $(this).attr('attr-user-id');
			var user_type = $(this).attr('attr-user-type');
			window.location.href = "<?php echo base_url(); ?>admin/ResetPassword/reset_admin/" + user_type + "/" + user_id;
		}
	});
</script>