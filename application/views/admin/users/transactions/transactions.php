<table  class="display table table-bordered table-striped">
	<thead>
		<tr>
			<th>Date</th>
			<th>Type (charge/bill or payment)</th>
			<th>Description</th>
			<th width="10%">Amount</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($transactions as $row) {  ?>
			<tr>
				<td><?php echo date('m-d-Y h:i:s A', strtotime($row->transaction_date)); ?></td>
				<td><?php echo $row->status; ?></td>
				<td><?php echo $row->description; ?></td>
				<td align="right"><?php echo (stripos($row->status, 'Payment') !== FALSE) ? '-' : ''; ?><?php echo "$" . number_format($row->amount, 2); ?></td>
			</tr>
		<?php  } ?>	
	</tbody>
</table>