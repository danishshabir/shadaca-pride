<!doctype html>
<html style="page-break-after: avoid;">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Statement Print</title>
		<!--<link media="all" rel="stylesheet" href="css/all.css" type="text/css">-->
		<style>
			*{
			max-height:1000000px;
			box-sizing:border-box;
			-webkit-box-sizing:border-box;
			}
			body {
			margin:0;
			color:#333;
			font:12px/15px Arial, Helvetica, sans-serif;
			background:#fff;
			min-width:320px;
			-webkit-text-size-adjust: 100%;
			-ms-text-size-adjust: none;
			}
			img {border-style:none;}
			a {
			text-decoration:none;
			color:#000;
			}
			a:hover {text-decoration:underline;}
			a:active {background-color: transparent;}
			input,
			textarea,
			select {
			font:100% Arial, Helvetica, sans-serif;
			vertical-align:middle;
			color:#000;
			}
			form, fieldset {
			margin:0;
			padding:0;
			border-style:none;
			}
			header, footer, article, section, hgroup, nav, figure { display: block; }
			.print-block{
			margin:0 auto;
			padding:50px 0;
			overflow:hidden;
			max-width:752px;
			}
			.print-block h1{
			font-size:18px;
			margin:0 0 30px;
			line-height:26px;
			text-align:right;
			text-transform:uppercase;
			}
            .print-block h3 {
                font-size: 12px;
            }
			.print-block .head{
			overflow:hidden;
			padding:0 0 15px;
			border-bottom:1px solid #e5e5e5;
			}
			.print-block dl{
			width:200px;
			float:right;
			margin:0 0 20px;
			}
			.print-block dl dt{
			float:left;
			min-width:100px;
			text-align:right;
			margin:0 10px 0 0;
			}
			.print-block dl dd{
			clear:right;
			margin:0 0 5px;
			min-width:330px;
			}
			.print-block .price-total{
			clear:both;
			width:160px;
			float:right;
			padding:10px;
			text-align:center;
			border-radius:4px;
			border:1px solid #e5e5e5;
			}
			.print-block .price-total span,
			.print-block .price-total strong{
			display:inline-block;
            width: 100% !important;
			}
			.print-block .price-total strong{
			font-size:18px;
			line-height:22px;
			}
			.print-block .table-box{
			overflow:hidden;
			padding:15px 0 0;
			}
			.print-block .table-box h2{
			margin:0 0 10px;
			font-size:16px;
			line-height:20px;
			font-weight:normal;
			}
			.print-block .table-box .sub-title{
			display:block;
			font-weight:normal;
			}
			.print-block .table-box header{
			overflow:hidden;
			padding:0 0 30px;
			}
			.print-block .table{
			border:0;
			width:100%;
			margin:0 0 15px;
			table-layout:fixed;
			vertical-align:middle;
			border-collapse:collapse;
			}
            .print-block .table-box .table {
                text-align:right;
            }
			.print-block .table th,
			.print-block .table td{
			padding:0;
			padding:6px 15px;
			}
			.print-block .table th{
			border-top:1px solid #e5e5e5;
			border-bottom:1px solid #e5e5e5;
			}
			.print-block .table th:first-child{
			border-left:1px solid #e5e5e5;
			}
			.print-block .table th:last-child{
			border-right:1px solid #e5e5e5;
			}
			.print-block .table th:first-child,
			.print-block .table td:first-child{
			text-align:left;
			}
			.print-block .table td{
			border-bottom:1px solid #e5e5e5;
			}
			.print-block .table table{
			border:0;
			width:292px;
			float:right;
			text-align:right;
			margin:-1px 0 0;
			table-layout:fixed;
			border-collapse:collapse;
			}
			.print-block .table td.inner{
			border:0;
			padding:0;
			}
			.print-block .table table td{
			border:1px solid #e5e5e5;
			}
			.print-block .table table td:first-child{
			text-align:right;
			}
			.print-block .table-box footer{
			overflow:hidden;
			padding:15px 0 0;
			border-top:1px solid #e5e5e5;
			}
			.print-block .table-box footer .col{
			width:48%;
			vertical-align:top;
			display:inline-block;
			}
			.print-block .table-box footer .title{
			display:block;
			font-size:14px;
			margin:0 0 5px;
			line-height:20px;
			font-weight:normal;
			}
			.print-block .table-box footer p{ margin:0;}
			.header{text-align:center;}
            table, th, td {
                border: 1px solid #e5e5e5;
                border-collapse: collapse;
            }
            th {
                text-align: center !important;
            }
            .print-block .price-total{
                text-align: right;
                padding: 5px 0px;
                border: none;
            }
            .div-table {
                display: table !important;
                width: 100% !important;
            }
            .div-row {
                display: table-row !important;
                width: inherit !important;
            }
            .div-cell {
                display: table-cell !important;
            }
            .div-cell.top {
                vertical-align: top !important;
            }
            .div-cell.middle {
                vertical-align: middle !important;
            }
            .div-cell.bottom {
                vertical-align: bottom !important;
            }
            .text-left {
                text-align: left !important;
            }
            .text-center {
                text-align: center !important;
            }
            .text-right {
                text-align: right !important;
            }
            ._p-5 {
                padding: 5px 0px !important;
            }
            ._w-40 {
                width: 40% !important;
            }
            ._mb-10 {
                margin-bottom: 10px !important;
            }
            ._clean-table,
            ._clean-table td,
            ._clean-table tr,
            ._clean-table th {
                border: none !important;
                margin: 0px;
                padding: 0px;
                text-align: left;
            }
            .table-block {
                width: 100% !important;
            }
            ._tw-100 {
                width: 100px !important;
            }
            ._tw-80 {
                width: 80px !important;
            }
            ._tw-200 {
                width: 200px !important;
            }
            ._tw-120 {
                width: 120px !important;
            }
            ._tw-115 {
                width: 115px !important;
            }
            ._tw-110 {
                width: 110px !important;
            }
            ._clean {
                margin: 0px !important;
                padding: 0px !important;
                border: none !important;
            }
            ._w-75 {
                width: 75% !important;
            }
		</style>
	</head>
	<body>
		<?php 
		$transactions_records;
		$total_amount = 0;
		foreach($transactions as $row) { 
			$transactions_records .= "<tr>
				<td align='left'>".date('m-d-Y', strtotime($row->transaction_date))."</td>
				<td align='left'>".$row->status."</td>
				<td align='left'>".$row->description."</td>
				<td align='right'>".((stripos($row->status, 'Payment') !== FALSE) ? '-' : '')."$" . number_format($row->amount, 2)."</td>
			</tr>";
            if(stripos($row->status, 'Payment') !== FALSE) {
                if($total_amount > 0)
                    $total_amount = $total_amount - $row->amount;
            }else{
                $total_amount = $total_amount + $row->amount;
            }
		} ?>
		<h1 class="text-center header"><?php echo $InvoiceDetails->h_name;?></h1>
		<h3 class="text-center header" style="margin:3px;"><?php echo $InvoiceDetails->h_address;?></h3>
		<h4 class="text-center header" style="margin:3px;"><?php echo $InvoiceDetails->h_city . ", " . $InvoiceDetails->h_state . " " . $InvoiceDetails->h_zip; ?></h4>
		<h4 class="text-center header" style="margin:3px;"><?php echo $InvoiceDetails->h_phone ;?></h4>
		<h3 class="text-center header"></h3>
		<h3 class="text-center header"></h3>
		<div class="print-block">
            <table class="table table-block">
                <tr class="_clean">
                    <td class="_clean">
                        <h1 style="font-size: 18px;" class="text-left">Bill To</h1>
                    </td>
                    <td align="right" class="_clean">
                        <!-- Statement -->
                        <h1 style="font-size: 18px;" class="text-right"></h1>
                    </td>
                </tr>
            </table>
			<header class="head">
                <table class="_clean-table table _mb-10 table-block">
                    <tbody>
                        <tr>
                            <td class="_p-5 _tw-200"><?php echo $law_firm->law_firm; ?></td>
                            <td class="_p-5 _tw-120"><!--Firm Name:--></td>
                            <td class="_w-40 _p-5"></td>
                            <!--<td class="text-right _p-5 _tw-110">Invoice#:</td>
                            <td class="text-right _p-5 _tw-80">1112-0031</td>-->
                            <td class="text-right _p-5 _tw-120">Statement date:</td>
                            <td class="text-right _p-5 _tw-80"><?php echo date('m-d-Y'); ?></td>
                        </tr>
                        <tr>
                            <td class="_p-5"><?php echo $law_firm->street; ?></td>
                            <td class="_p-5"><!--Street:--></td>
                            <td class="_w-40 _p-5"></td>
                            <?php if(false) { ?>
                            <td class="text-right _p-5">Due date:</td>
                            <!--<td class="text-right _p-5"><?php echo date("m") . "-" . date('d', strtotime($InvoiceDetails->invoice_due_date)) . '-' . date("Y") ;?></td>-->
                            <td class="text-right _p-5"><?php echo date('m-d-Y', strtotime("+$InvoiceDetails->invoice_due_date days", time())) ;?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="_p-5"><?php echo $law_firm->city . ', ' . $jurisdiction_state->state_short_name . ' ' . $law_firm->zip; ?></td>
                            <td class="_p-5"><!--City, State, Zip:--></td>
                            <td class="_w-40 _p-5"></td>
                        </tr>
                    </tbody>
                </table>
                <!--<table style="margin-bottom: 10px;text-align: right;border: none; width: 100%;">
                    <tbody>
                        <tr>
                            <td style="border: none; width: 75%;"></td>
                            <td style="border: none;padding: 5px 10px;">Invoice#:</td>
                            <td style="border: none;">1112-0031</td>
                        </tr>
                        <tr>
                            <td style="border: none; width: 75%;"></td>
                            <td style="border: none;padding: 5px 10px;">Invoice date:</td>
                            <td style="border: none;"><?php echo date('m-d-Y'); ?></td>
                        </tr>
                        <tr>
                            <td style="border: none; width: 75%;"></td>
                            <td style="border: none;padding: 5px 10px;">Due date</td>
                            <td style="border: none;"><?php echo date("m") . "-" . date('d', strtotime($InvoiceDetails->invoice_due_date)) . '-' . date("Y") ;?></td>
                        </tr>
                    </tbody>
                </table>-->
				<div class="price-total">
                    <!-- Amount due -->
					<span></span>
                    <br />
					<strong style="font-size: 15px;"><?php /* echo ($total_amount > 0 ? '$'.number_format($total_amount, 2).' USD' : '$0.00 USD'); */ ?></strong>
				</div>
			</header>
			<div class="table-box">
				<!--<header>
					<h2>Bill To:</h2>
					<strong class="sub-title"><?php echo $law_firm->law_firm; ?></strong>
					<strong class="sub-title"><?php echo $law_firm->street; ?></strong>
					<strong class="sub-title"><?php echo $law_firm->city . ' , ' . $jurisdiction_state->state_short_name . ' , ' . $law_firm->zip; ?></strong>
				</header>-->
				<h3 style="font-size: 12px;"> Charges and payments from <?php echo $transactions_month; ?></h3>
				<table class="table">
					<tr>
						<th>Date</th>
						<th>Type (charge/bill or payment)</th>
						<th>Description</th>
						<th>Amount</th>
					</tr>
					<?php echo $transactions_records; ?>	
					<tr>
						<td colspan="4" class="inner">
							<table>
								<tr>
									<td colspan="2"><strong>Total</strong></td>
									<td colspan="2"><strong><?php echo ($total_amount > 0 ? '$'.number_format($total_amount, 2).' USD' : '$0.00 USD') ?></strong></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
                <footer>
                    <table style="width: 100%; border: none; text-align: left;">
                        <thead>
                            <tr>
                                <th style="border: none;text-align: left !important;width: 50%;">Notes</th>
                                <th style="border: none;text-align: left !important;width: 50%;">Terms and Conditions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="border: none; text-align: left; padding-right: 10px;"><?php echo $InvoiceDetails->terms_notes_global; ?></td>
                                <td style="border: none; text-align: left; padding-right: 10px;"><?php echo $InvoiceDetails->terms_conditions; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </footer>
			</div>
		</div>
	</body>
</html>