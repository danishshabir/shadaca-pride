<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Company logo</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Company logo</h3>
					</div>
					<?php if($this->session->flashdata('message')) {
						echo $this->session->flashdata('message');
					} ?>
					<div class="porlets-content">
						<form action="<?php echo base_url();?>admin/users/update_lawfirm_profile/<?php echo $this->session->userdata('id');?>" method="post" parsley-validate novalidate enctype="multipart/form-data">
							<div class="form-group col-md-4">
								<label>Company logo image</label><br>
								<?php if(file_exists('./uploads/'.$law_firm->image)) { ?>
									<img src="<?php echo base_url();?>uploads/<?php echo $law_firm->image;?>" style="width:105px; height:125px;"></img>
								<?php
								} else { ?>
									<img src="<?php echo base_url();?>/assets/admin/images/no-image.jpg" style="width:105px; height:125px;"></img>
								<?php } ?>
								<input type="file" name="image"/>
								<input type="hidden" name="image" value="<?php echo $law_firm->image;?>"/>
							</div>
							<!--/form-group-->
							<div class="clearfix"></div>
							<div class="form-group col-md-12">
								<button class="btn btn-primary" type="submit">Submit</button>
								<!--<button class="btn btn-default">Cancel</button>-->
								<a href="<?php echo base_url();?>admin/settings/lawFirms" class="btn btn-default">Cancel</a>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
					<!--/porlets-content-->
				</div>
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>