<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Edit <?php echo $type; ?></h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header"><?php echo $type; ?> Form</h3>
						<?php 
							if($this->session->flashdata('message')){	
								echo $this->session->flashdata('message');
							}		
							?>
					</div>
					<div class="porlets-content">
						<form action="<?php echo current_url();?>" method="post" enctype="multipart/form-data">
							<input type="hidden" name="role" value="<?php echo $user->role;?>" />
							<div class="form-group col-md-4">
								<label>First name *</label>
								<input type="text" name="name"   placeholder="Enter first name" class="form-control" value="<?php echo ($this->input->post('name') ? $this->input->post('name') : $user->name); ?>">
								<?php echo (form_error('name') ? form_error('name') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Last name *</label>
								<input type="text" name="lname"   placeholder="Enter last name" class="form-control" value="<?php echo ($this->input->post('lname') ? $this->input->post('lname') : $user->lname); ?>">
								<?php echo (form_error('lname') ? form_error('lname') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Email address *</label>
								<input type="email" name="email"   placeholder="Enter email" class="form-control" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $user->email); ?>">
								<?php echo (form_error('email') ? form_error('email') : ''); ?>
							</div>
                            <?php if($user->role == 7) { ?>
								<div class="form-group col-md-4">
									<label>Select law firm</label>
									<select class="form-control" onchange="getLawfirmLawyers(this.value);" name="law_firms">
										<option value="" > Select law firm </option>
										<?php foreach($law_firms as $law_firm) { ?>
											<option id="fee" value="<?php echo $law_firm->id;?>" <?php if($law_firm->id == $user->law_firms) echo 'selected';?>><?php echo $law_firm->law_firm;?></option>
										<?php } ?>  
									</select>
									<?php echo (form_error('law_firms') ? form_error('law_firms') : ''); ?>
								</div>
								<div class="form-group col-md-4">
									<label>Select lawyer *</label>
									<select class="form-control" id="lawyers" name="lawyer">
										<option value="<?php echo ($curr_lawyer == 'All' ? 0 : $curr_lawyer->id); ?>" id="fee"><?php echo ($curr_lawyer == 'All' ? 'All' : $curr_lawyer->name . ' ' . $curr_lawyer->lname)?></option>
										<?php foreach($other_lawyers as $lawyers){ ?>
											<option value="<?php echo $lawyers['id']; ?>" id="fee"><?php echo $lawyers['name']; ?> <?php  echo $lawyers['lname'];?></option>
										<?php  }?>
                                        <?php if($curr_lawyer != 'All'){ ?>
                                            <option value="0">All</option>
                                        <?php  }?>
									</select>
									<?php echo (form_error('lawyer') ? form_error('lawyer') : ''); ?>
								</div>
							<?php  }?>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Mobile *</label>
								<input type="text" name="phone"   placeholder="Enter mobile number" class="form-control" value="<?php echo ($this->input->post('phone') ? $this->input->post('phone') : $user->phone); ?>">
								<?php echo (form_error('phone') ? form_error('phone') : ''); ?>
							</div>
							<!--/form-group-->
							<?php if($user->role != '2') { ?>
								<div class="form-group col-md-4">
									<label>Activate</label>
									<select class="form-control" name="active">
										<option <?php if($user->active == 0){?> selected="selected" <?php }?> value="0">Inactive</option>
										<option <?php if($user->active == 1){?> selected="selected" <?php }?> value="1">Active</option>
									</select>
									<?php echo (form_error(active) ? form_error('active') : ''); ?>
								</div>
                                <?php if($user->role == 7){ ?>
									<!---<div class="form-group col-md-4">
										<label>&nbsp;</label>
										<p><a class="btn btn-primary" id="resetPass" attr-user-type="lawstaff" attr-user-id="<?php echo $user->unique_code; ?>" href="javascript:void(0);">Reset Password</a></p>
									</div>  -->
								<?php } ?>
							<?php } ?>  
							<?php 
							if($user->role == 2) { ?>
								<div class="form-group col-md-4" id="lawFirm">
									<label>Select law firm</label>
									<select placeholder="Law Type" class="form-control" name="law_firms" <!--onchange="selectLawFirm(this.value,'admin/Dashboard/getLawFirm','');"-->>
										<option value="" > Select law firm </option>
										<?php foreach($law_firms as $law_firm){?>
										<option id="fee" value="<?php echo $law_firm->id;?>" <?php if($law_firm->id == $user->law_firms) echo 'selected';?>> <?php echo $law_firm->law_firm;?> </option>
										<?php }?>  
									</select>
									<?php echo (form_error('law_firms') ? form_error('law_firms') : ''); ?>
								</div>
								<!--/form-group-->
								<div class="form-group col-md-4">
									<label>Website</label>
									<input type="text" name="website"  placeholder="Enter website" class="form-control" value="<?php echo ($this->input->post('website') ? $this->input->post('website') : $user->website); ?>">
									<?php echo (form_error('website') ? form_error('website') : ''); ?>
							</div>
								<div class="clearfix"></div> 
								<div class="form-group col-md-4">
									<label>Address *</label>
									<input type="text" class="form-control" value="<?php echo ($this->input->post('address') ? $this->input->post('address') : $user->address); ?>" name="address"  placeholder="Lawyer address" >
									<?php echo (form_error('address') ? form_error('address') : ''); ?>
								</div>
								<div class="form-group col-md-4">
									<label>City *</label>
									<input type="text" name="city"   placeholder="Enter city name" class="form-control" value="<?php echo ($this->input->post('city') ? $this->input->post('city') : $user->city); ?>">
									<?php echo (form_error('city') ? form_error('city') : ''); ?>
								</div>
								<!--/form-group-->
								<div class="form-group col-md-2">
									<div class="form-group">
										<label>State *</label>
										<select placeholder="state" class="form-control" name="state" >
											<option value="" > Select state </option>
											<?php foreach($states_short_names as $state){?>
											<option value="<?php echo $state->id;?>" <?php if($state->id == $user->state){?> selected <?php }?>> <?php echo $state->state_short_name;?> </option>
											<?php }?>
										</select>
										<?php echo (form_error('state') ? form_error('state') : ''); ?>
									</div>
								</div>
								<div class="form-group col-md-2">
									<label>Zip *</label>
									<input type="text" name="zipcode"   placeholder="Enter zip here" class="form-control" value="<?php echo ($this->input->post('zipcode') ? $this->input->post('zipcode') : $user->zipcode); ?>">
									<?php echo (form_error('zipcode') ? form_error('zipcode') : ''); ?>
								</div>
								<div class="clearfix"></div> 
								<!--/form-group-->
								<div class="form-group col-md-4">
									<label>Initial setup charge *</label>
									<input type="text" name="initial_setup_charge"   placeholder="Enter initial setup charge" value="<?php echo ($this->input->post('initial_setup_charge') ? number_format($this->input->post('initial_setup_charge'), 2) : number_format($user->initial_setup_charge, 2)); ?>" class="form-control">
									<?php echo (form_error('initial_setup_charge') ? form_error('initial_setup_charge') : ''); ?>
								</div>
								<!--/form-group-->
								<div class="form-group col-md-4">
									<div class="form-group">
										<label>Fee type *</label>
										<select placeholder="Fee type" class="form-control" name="fee_type" >
											<option value="" > Select Type </option>
											<option value="0"> Monthly </option>
											<option value="1"> Annual </option>
										</select>
										<?php echo (form_error('fee_type') ? form_error('fee_type') : ''); ?>
									</div>
								</div>
								<div class="form-group col-md-4">
									<label>Fee *</label>
									<input type="text" id="sub_cat"  name="law_firm_fee"   placeholder="Fee" value="<?php echo ($this->input->post('law_firm_fee') ? number_format($this->input->post('law_firm_fee'), 2) : number_format($user->law_firm_fee, 2)); ?>" class="form-control">
									<?php echo (form_error('law_firm_fee') ? form_error('law_firm_fee') : ''); ?>
								</div>
								<!--/form-group-->
								<div class="clearfix"></div> 
								<?php 
								$case_types = ($this->input->post('case_type') ? $this->input->post('case_type') : explode(',', $user->case_type));
								$jurisdictions = ($this->input->post('jurisdiction') ? $this->input->post('jurisdiction') : explode(',', $user->jurisdiction));
								?>
								<div class="form-group col-md-12 selection_boxes" style="padding:20px 15px;">
									<label>Case type *</label>
									<span class="button-checkbox">
										<button type="button" class="btn" data-color="primary">Select All</button>
										<input type="checkbox" class="hidden" />
									</span>
									<br />
									<?php $case_type_chunks = partition($case_type, 3); ?>
									<div class="row">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($case_type_chunks[0] as $c_type) { ?>
												<label class="checkbox-inline labelFullED">
												<input type="checkbox" <?php if(in_array(trim($c_type->type),$case_types)) echo "checked"; ?> value="<?php echo trim($c_type->type);?>" name="case_type[]" class="checkbox" id="inlinecheckbox1">
												<span class="custom-checkbox"></span> <?php echo trim($c_type->type);?> </label>
											<?php } ?>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($case_type_chunks[1] as $c_type) { ?>
												<label class="checkbox-inline labelFullED">
												<input type="checkbox" <?php if(in_array(trim($c_type->type),$case_types)) echo "checked"; ?> value="<?php echo trim($c_type->type);?>" name="case_type[]" class="checkbox" id="inlinecheckbox1">
												<span class="custom-checkbox"></span> <?php echo trim($c_type->type);?> </label>
											<?php } ?>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($case_type_chunks[2] as $c_type) { ?>
												<label class="checkbox-inline labelFullED">
												<input type="checkbox" <?php if(in_array(trim($c_type->type),$case_types)) echo "checked"; ?> value="<?php echo trim($c_type->type);?>" name="case_type[]" class="checkbox" id="inlinecheckbox1">
												<span class="custom-checkbox"></span> <?php echo trim($c_type->type);?> </label>
											<?php } ?>
										</div>
									</div>
									<?php echo (form_error('case_type[]') ? form_error('case_type[]') : ''); ?>
								</div>
								<div class="form-group col-md-12 selection_boxes" style="padding:20px 15px;">
									<label>Jurisdiction *</label>
									<span class="button-checkbox">
										<button type="button" class="btn" data-color="primary">Select All</button>
										<input type="checkbox" class="hidden" />
									</span>
									<br />
									<?php $jurisdiction_chunks = partition($jurisdiction, 3); ?>
									<div class="row">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($jurisdiction_chunks[0] as $c_jurisdiction) { ?>
												<label class="checkbox-inline labelFullED">
												<input type="checkbox" <?php if(in_array(trim($c_jurisdiction->name),$jurisdictions)) echo "checked"; ?> value="<?php echo trim($c_jurisdiction->name);?>" name="jurisdiction[]" class="checkbox" id="inlinecheckbox1">
												<span class="custom-checkbox"></span> <?php echo trim($c_jurisdiction->name);?> </label>
											<?php } ?>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($jurisdiction_chunks[1] as $c_jurisdiction) { ?>
												<label class="checkbox-inline labelFullED">
												<input type="checkbox" <?php if(in_array(trim($c_jurisdiction->name),$jurisdictions)) echo "checked"; ?> value="<?php echo trim($c_jurisdiction->name);?>" name="jurisdiction[]" class="checkbox" id="inlinecheckbox1">
												<span class="custom-checkbox"></span> <?php echo trim($c_jurisdiction->name);?> </label>
											<?php } ?>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($jurisdiction_chunks[2] as $c_jurisdiction) { ?>
												<label class="checkbox-inline labelFullED">
												<input type="checkbox" <?php if(in_array(trim($c_jurisdiction->name),$jurisdictions)) echo "checked"; ?> value="<?php echo trim($c_jurisdiction->name);?>" name="jurisdiction[]" class="checkbox" id="inlinecheckbox1">
												<span class="custom-checkbox"></span> <?php echo trim($c_jurisdiction->name);?> </label>
											<?php } ?>
										</div>
									</div>
									<?php echo (form_error('jurisdiction[]') ? form_error('jurisdiction[]') : ''); ?>
								</div>
							<?php } ?>	
							<?php if($user->role == 2 || $user->role == 6) {?>									
								<div class="form-group col-md-12">
									<label>Lawyer - internal notes (visible to admin only)</label>
									<textarea name="internal_notes" placeholder="Add internal notes" class="form-control" value="<?php echo ($this->input->post('internal_notes') ? $this->input->post('internal_notes') : $user->internal_notes); ?>" rows="4"><?php echo ($this->input->post('internal_notes') ? $this->input->post('internal_notes') : $user->internal_notes); ?></textarea>
								</div>
								<div class="form-group col-md-12">
                                    <label>Lawyer - account notes (visible to lawyer)</label>
									<textarea name="shared_notes" placeholder="Add lawyer notes" class="form-control" value="<?php echo ($this->input->post('shared_notes') ? $this->input->post('shared_notes') : $user->shared_notes); ?>" rows="4"><?php echo ($this->input->post('shared_notes') ? $this->input->post('shared_notes') : $user->shared_notes); ?></textarea>
								</div>
							<?php }?>
							<div class="clearfix"></div> 
							<!--<div class="form-group col-md-4">
								<label>Profile image</label><br>
								<?php if(!empty($user->image)) {?>
								<img src="<?php echo base_url();?>uploads/<?php echo $user->image;?>" style="width:105px; height:125px;"></img>
								<?php }else{?>
								<a href="#"> <img src="<?php echo base_url();?>assets/admin/images/avatar1.jpg" style="width:95px; height:105px;"> </a>
								<?php }?>
								<input type="file" name="image"/>
								<input type="hidden" name="image" value="<?php echo $user->image;?>"/>
								<?php echo (form_error('image') ? form_error('image') : ''); ?>
							</div>-->
							<!--/form-group-->
							<div class="clearfix"></div>
							<?php if($user->role == '2') { ?>
							<div class="form-group col-md-4">
								<label>Holiday start date </label>
								<input type="text" name="holiday_from"  placeholder="Enter holiday start date" class="form-control datepicker-here" value="<?php echo ($this->input->post('holiday_from') ? $this->input->post('holiday_from') : ($user->holiday_from ? date('m-d-Y', strtotime($user->holiday_from)) : '')); ?>" data-date-format="mm-dd-yyyy" data-language="en">
								<?php echo (form_error('holiday_from') ? form_error('holiday_from') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Holiday end date</label>
								<input type="text" name="holiday_to"   placeholder="Enter holiday end date" class="form-control datepicker-here" value="<?php echo ($this->input->post('holiday_to') ? $this->input->post('holiday_to') : ($user->holiday_to ? date('m-d-Y', strtotime($user->holiday_to)) : '')); ?>" data-date-format="mm-dd-yyyy" data-language="en">
								<?php echo (form_error('holiday_to') ? form_error('holiday_to') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>&nbsp;</label>
								<p><a class="btn btn-primary reset">Reset</a></p>
							</div>
							<div class="clearfix"></div> 
							<div class="form-group col-md-4">
								<label>Activate</label>
								<select class="form-control" name="active">
									<option <?php if($user->active == 0){?> selected="selected" <?php }?> value="0">Inactive</option>
									<option <?php if($user->active == 1){?> selected="selected" <?php }?> value="1">Active</option>
								</select>
								<?php echo (form_error('active') ? form_error('active') : ''); ?>
							</div>
							<div class="form-group col-md-4">
								<label>Text notification</label>
								<select class="form-control" name="is_sms">
									<option <?php if($user->is_sms == 0){?> selected="selected" <?php }?> value="0">Inactive</option>
									<option <?php if($user->is_sms == 1){?> selected="selected" <?php }?> value="1">Active</option>
								</select>
								<?php echo (form_error('is_sms') ? form_error('is_sms') : ''); ?>
							</div>
							<?php } else{ ?>
							 <!--	<div class="form-group col-md-4">
								<label>&nbsp;</label>
								<p><a class="btn btn-primary" id="resetPass" attr-user-type="lawyer" attr-user-id="<?php echo $user->unique_code; ?>" href="javascript:void(0);">Reset Password</a></p>
							</div>  -->
							<?php }  ?>   
							<div class="form-group col-md-12">
								<button class="btn btn-primary" id="submitBtn" type="submit"><?php if($user->role == 1){echo 'Update Admin';}elseif($user->role == 2){echo 'Update Lawyer';}elseif($user->role == 7){echo 'Update Lawyer Staff';}else{echo 'Update';}?></button>
								<a class="btn btn-default" href="<?php echo base_url(); ?>admin/users/admin">Cancel</a>
								<a class="btn btn-primary" id="resetPass" attr-user-type="lawyer" attr-user-id="<?php echo $user->unique_code; ?>" href="javascript:void(0);">Reset Password</a>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<script>
function selectLawFirm(id, actionUrl, reloadUrl) {
	if (id == '') {
		$("#sub_cat").val('');
		return true;
	}
	$.post( "<?php echo base_url();?>"+ actionUrl, { id: id }, function( data ) {
		$('#sub_cat').val(data.fee);
		$("[name='fee_type']").val(data.fee_type);
	}, "json");
}

$(function () {
	$('.button-checkbox')
		.each(function () {
			// Settings
			var $widget = $(this)
				, $button = $widget.find('button')
				, $checkbox = $widget.find('input:checkbox')
				, color = $button.data('color')
				, settings = {
					on: {
						icon: 'glyphicon glyphicon-check'
					}
					, off: {
						icon: 'glyphicon glyphicon-unchecked'
					}
				};
			// Event Handlers
			$button.on('click', function () {
				$checkbox.prop('checked', !$checkbox.is(':checked'));
				$checkbox.triggerHandler('change');
			});
			$checkbox.on('change', function () {
				updateDisplay();
			});
			// Actions
			function updateDisplay() {
				var isChecked = $checkbox.is(':checked');
				$button.data('state', (isChecked) ? "on" : "off");
				$button.find('.state-icon')
					.removeClass()
					.addClass('state-icon ' + settings[$button.data('state')].icon);
				if (isChecked) {
					$button.removeClass('btn-default').addClass('btn-' + color + ' active');
					$widget.parents('.selection_boxes').first().find('.checkbox').prop('checked', true);
				} else {
					$button.removeClass('btn-' + color + ' active').addClass('btn-default');
					$widget.parents('.selection_boxes').first().find('.checkbox').prop('checked', false);
				}
			}
			// Initialization
			function init() {
				var isChecked = $checkbox.is(':checked');
				$button.data('state', ($widget.parents('.selection_boxes').first().find('.checkbox:checked').length == $widget.parents('.selection_boxes').first().find('.checkbox').length ? "on" : "off"));
				$button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);
				if($widget.parents('.selection_boxes').first().find('.checkbox:checked').length == $widget.parents('.selection_boxes').first().find('.checkbox').length)
				{
					$button.removeClass('btn-default').addClass('btn-' + color + ' active');
					$checkbox.prop('checked', true);
				} else {
					$button.removeClass('btn-' + color + ' active').addClass('btn-default');
					$checkbox.prop('checked', false);
				}
				if ($button.find('.state-icon').length == 0) {
					$button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
				}
			}
			init();
		});
		
		$("[name='law_firms']").val('<?php echo ($this->input->post('law_firms') ? $this->input->post('law_firms') : $user->law_firms) ?>');
		$("[name='state']").val('<?php echo ($this->input->post('state') ? $this->input->post('state') : $user->state) ?>');
		$("[name='fee_type']").val('<?php echo ($this->input->post('fee_type') ? $this->input->post('fee_type') : $user->fee_type) ?>');
		$("[name='active']").val('<?php echo ($this->input->post('active') ? $this->input->post('active') : $user->active) ?>');
		$("[name='is_sms']").val('<?php echo ($this->input->post('is_sms') ? $this->input->post('is_sms') : $user->is_sms) ?>');

		$('#resetPass').on('click', function(){
			if(confirm('Are You Sure? You Want To Reset Password?')){
				var user_id = $(this).attr('attr-user-id');
				var user_type = $(this).attr('attr-user-type');
				window.location.href = "<?php echo base_url(); ?>admin/ResetPassword/reset_admin/" + user_type + "/" + user_id;
			}
		});
});
        function getLawfirmLawyers(id)
        	{
        		$('#submitBtn').css('cursor','not-allowed').attr('disabled', true);
        		if(id !== ''){
        			$.ajax({
        				url: '<?php echo base_url();?>admin/Dashboard/getLawfirmLawyers',
        				data: {id: id},
        				type: 'post',
        				success: function(data){
        					if(data != ''){
        						$('#submitBtn').removeAttr('disabled').css('cursor','pointer');
        						$('#lawyers').removeAttr('disabled').html(data);
        					} else {
        						$('#lawyers').attr('disabled', true).html('');
        					}
        				}
        			});
        		} else {
        			$('#lawyers').attr('disabled', true).html('');
        		}
        	}
</script>