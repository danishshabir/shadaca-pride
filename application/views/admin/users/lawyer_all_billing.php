<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Billing</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<?php $balance = getLawyerTotalBalance($lawyer->id);
						if($lawyer->id) { ?>
							<h3 class="content-header text-center"><?php echo   "Lawyer name: " . $lawyer->name . " " . $lawyer->lname ; ?></h3><br>
							<h3 class="content-header text-center"><?php echo ($balance ? 'Balance: $'.$balance : '') ?></h3>
						<?php } ?>
					</div>
					<div class="porlets-content">
						<div class="form-group col-md-12"></div>
						<div class="porlets-content">
							<div id="all_lawyers_table">
								<div class="table-responsive">
									<div class="clearfix"></div>
									<div class="margin-top-10"></div>
									<table  class="display table table-bordered table-striped" id="dynamic-table">
										<thead>
											<tr>
												<th style="width:150px;">Name</th>
												<th>Email</th>
												<th>Balance</th>
												<th>Leads Detail</th>      
												<th>Account Billing</th>
											</tr>
										</thead>
										<tbody class="searchable">
											<tr>
												<td><?php echo $lawyer->name; ?></td>
												<td><?php echo $lawyer->email; ?></td>
												<td>
													<?php $lawyerTransaction_bill_var = number_format($totalArrays[0], 2); 
													echo "$" .$lawyerTransaction_bill_var ; ?>
												</td>
												<td> <a href="<?php echo base_url();?>admin/dashboard/LawyerAllLeads/<?php echo $lawyer->id;?>">Details</a></td>
												<td><a href="<?php echo base_url();?>admin/dashboard/LawyerBills/<?php echo $lawyer->id;?>">Details</a></td>	
											</tr>
										</tbody>
									</table>
								</div>
								<!--/table-responsive-->   
							</div>
						</div>
						<!--/porlets-content-->
						<div class="col-md-12 text-center">
							<span style="color:green; text-align:center; font-size:22px" id="success_message"></span>
						</div>
						<div id="search_result" style="display:none;">
							<div class="table-responsive">
								<div class="clearfix"></div>
								<div class="margin-top-10"></div>
								<table class="display table table-bordered table-striped" >
									<tbody id="table_content">			  </tbody>
								</table>
								<br/>
							</div>
						</div>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->               
	</div>
	<!--/page-content end--> 
</div>