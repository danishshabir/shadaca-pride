<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>View Law Firm</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">View Law Firm </h3>
					</div>
					
					<div class="porlets-content">
						<form action="" method="post" enctype="multipart/form-data">
							<div class="form-group col-md-4">
								<label>Firm name *</label>
								<input type="text" name="law_firm"  value="<?php echo $result[0]->law_firm; ?>"  placeholder="Firm name" class="form-control" disabled>
							
							</div>
							<div class="form-group col-md-4">
								<label>Contact first name *</label>
								<input type="text" name="contact_name" value="<?php echo $result[0]->contact_name; ?>"   placeholder="Contact first name" class="form-control" disabled>
								
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Contact last name *</label>
								<input type="text" name="contact_last_name"  value="<?php echo $result[0]->contact_last_name; ?>"  placeholder="Contact last name" class="form-control" disabled>
								
							</div>
							<div class="form-group col-md-4">
								<label>Phone *</label>
								<input type="text" name="phone" value="<?php echo $result[0]->phone; ?>"   placeholder="Enter phone no:" class="form-control" disabled>
							
							</div>
							<div class="form-group col-md-4">
								<label>Contact email *</label>
								<input type="email" name="contact_email" value="<?php echo $result[0]->contact_email; ?>"   placeholder="Enter contact email" class="form-control" disabled>
								
							</div>
							<!--/form-group-->
							
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Website</label>
								<input type="text" name="website" value="<?php echo $result[0]->website; ?>"   placeholder="Enter website" class="form-control" disabled >
								
							</div>
							<!--/form-group-->
							
							<!--/form-group-->
							
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Street *</label>
								<input type="text" name="street"  value="<?php echo $result[0]->street; ?>"  placeholder="Enter street" class="form-control" disabled>
								
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>City *</label>
								<input type="text" name="city"   placeholder="Enter city here" class="form-control" value="<?php echo $result[0]->city; ?>" disabled>
								
							</div>
							<!--/form-group-->
							<div class="form-group col-md-2">
								<div class="form-group">
									<label>State *</label>
									<select placeholder="state" class="form-control" name="state" disabled>
										
										<?php ///foreach($states_short_names as $state){?>
										<option value="<?php echo $result[0]->state;?>"><?php echo $result[0]->state;?></option>
										<?php //}?>
									</select>
								
								</div>
							</div>
							<div class="form-group col-md-2">
								<label>Zip *</label>
								<input type="text" name="zip"   placeholder="Enter zip here" class="form-control" value="<?php echo $result[0]->zip; ?>" maxlength="5" disabled>
								
							</div>
                            <!--/form-group-->
        					<div class="form-group col-md-4">
        						<label>Created at</label>
        						<input type="text" name="created_at" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo  ( $result[0]->created_at ? date("m-d-Y h:i:s A", strtotime($result[0]->created_at)) : "No Record" );?>" disabled="disabled">
        					</div>
							
							<div class="form-group col-md-12">

						<label>Law firm - internal note (visible to admin only)</label>

						<textarea name="internal_notes"   value="<?php echo $result[0]->internal_notes; ?>" class="form-control" rows="4" disabled><?php echo $result[0]->internal_notes; ?></textarea>

					</div>
					<div class="form-group col-md-12">

						<label>Law firm - account notes (visible to lawfirm)</label>

						<textarea name="shared_notes"   value="<?php echo $result[0]->shared_notes; ?>" class="form-control" rows="4" disabled><?php echo $result[0]->shared_notes; ?></textarea>

					</div>
							
						<div class="form-group col-md-4">								
						<label>Customer ID *</label>								
						<input type="text"   value="<?php echo $result[0]->customer_id; ?>"  class="form-control" disabled>	
						
						</div>
						
						<div class="form-group col-md-4">								
						<label>Last login date </label>								
						<input type="text"   value="<?php echo date("m-d-Y", strtotime($result[0]->last_login_date)); ?>"  class="form-control" disabled>	
						
						</div>
						<div class="form-group col-md-4">								
						<label>Last Ip address</label>								
						<input type="text"   value="<?php echo $result[0]->last_login_ip; ?>"  class="form-control" disabled>	
						
						</div>
							<!--/form-group-->
							<!--<div class="form-group col-md-4">
								<label>Customer ID</label>
								<input type="text" value="<?php //echo $lead->customer_id; ?>"  placeholder="Enter customer ID" class="form-control" readonly>
							</div>--->
							<div class="clearfix"></div>
							
							<div class="clearfix"></div>
							
							<div class="clearfix"></div>
						</form>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
