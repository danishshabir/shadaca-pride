<?php $session_id=$this->session->userdata('id'); ?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Add Holiday </h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Holiday / Vacations</h3>
					</div>
					<div class="porlets-content">
						<form action="<?php echo base_url();?>admin/users/update_holiday/<?php echo $session_id;?>" method="post">
							<div class="form-group col-sm-4">					
								<label>Holiday Start Date </label>
								<input type="text" name="holiday_from" placeholder="Enter Holiday Start date" class="form-control datepicker-here" data-date-format="mm-dd-yyyy" data-language="en" value="<?php echo ($lawyer->holiday_from ? date('m-d-Y', strtotime($lawyer->holiday_from)) : ''); ?>">
							</div>
							<!--/form-group-->
							<div class="form-group col-sm-4">
								<label>Holiday End Date</label>
								<input type="text" name="holiday_to" placeholder="Enter Holiday End Date" class="form-control datepicker-here" data-date-format="mm-dd-yyyy" data-language="en" value="<?php echo ($lawyer->holiday_to ? date('m-d-Y', strtotime($lawyer->holiday_to)) : ''); ?>">
							</div>
							<!--/form-group-->             
							<div class="clearfix"></div>
							<div class="form-group col-sm-12">
								<button class="btn btn-primary" type="submit">Submit</button>
                                <input type="reset" class="btn btn-primary" value="Reset" />
                                
								<a class="btn btn-default" href="<?php echo base_url(); ?>admin/dashboard/manage">Cancel</a>
							</div>
						</form>
						<div class="clearfix"></div>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>