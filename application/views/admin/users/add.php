<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Add New <?php echo $type; ?></h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header"><?php echo $type; ?> Form</h3>
					</div>
					<?php if($this->session->flashdata('message'))
					{ echo $this->session->flashdata('message'); }?>	
					<div class="porlets-content">
						<form action="<?php echo current_url();?>" method="post" enctype="multipart/form-data">
							<div class="form-group col-md-4">
								<label>First name *</label>
								<input type="text" name="name"   placeholder="Enter first name" class="form-control" value="<?php echo $this->input->post('name'); ?>">
								<?php echo (form_error('name') ? form_error('name') : ''); ?>
							</div>
							<!--/form-group-->			
							<div class="form-group col-md-4">
								<label>Last name *</label>
								<input type="text" name="lname"   placeholder="Enter last name" class="form-control" value="<?php echo $this->input->post('lname'); ?>">
								<?php echo (form_error('lname') ? form_error('lname') : ''); ?>
							</div>
							<!--/form-group-->               
							<div class="form-group col-md-4">
								<label>Email address *</label>
								<input type="email" name="email"   placeholder="Enter email" class="form-control" value="<?php echo $this->input->post('email'); ?>">
								<?php echo (form_error('email') ? form_error('email') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="clearfix"></div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Mobile *</label>
								<input type="text" name="phone"   placeholder="Enter mobile number" class="form-control" value="<?php echo $this->input->post('phone'); ?>">
								<?php echo (form_error('phone') ? form_error('phone') : ''); ?>
							</div>
                            <?php if($role == 7) { ?>
								<div class="form-group col-md-4" id="lawFirm">
									<label>Select law firm *</label>
									<select placeholder="Law Type" class="form-control" onchange="getLawfirmLawyers(this.value);" name="law_firms">
										<option value="" > Select law firm </option>
										<?php foreach($law_firms as $law_firm){?>
										<option id="fee" value="<?php echo $law_firm->id;?>" <?php echo $this->input->post('law_firms') == $law_firm->id ? 'selected=""' : '' ?>> <?php echo $law_firm->law_firm;?> </option>
										<?php }?>  
									</select>
									<?php echo (form_error('law_firms') ? form_error('law_firms') : ''); ?>
								</div>
								<div class="form-group col-md-4" id="lawFirm">
									<label>Select lawyer *</label>
									<select class="form-control" id="lawyers" name="lawyer" <?php echo !($this->input->post('law_firms')) ? 'disabled' : $this->input->post('law_firms') == '' ? 'disabled' : '' ?> required>
									   <?php if($this->input->post('law_firms')) echo getLawfirmLawyers($this->input->post('law_firms')); ?>
									</select>
									<?php echo (form_error('lawyer') ? form_error('lawyer') : ''); ?>
								</div>
							<?php  }?>
							<!--/form-group-->
							<?php if($role == 2) { ?>     
								<div class="form-group col-md-4" id="lawFirm">
									<label>Select law firm *</label>
									<select placeholder="Law Type" class="form-control" name="law_firms">
										<option value="" > Select law firm </option>
										<?php foreach($law_firms as $law_firm){?>
										<option id="fee" value="<?php echo $law_firm->id;?>" <?php echo $this->input->post('law_firms') == $law_firm->id ? 'selected=""' : '' ?>> <?php echo $law_firm->law_firm;?> </option>
										<?php }?>  
									</select>
									<?php echo (form_error('law_firms') ? form_error('law_firms') : ''); ?>
								</div>
								<!--/form-group-->
								<div class="form-group col-md-4">
									<label>Website</label>
									<input type="text"  name="website"  placeholder=" Enter website" class="form-control" value="<?php echo $this->input->post('website'); ?>">
									<?php echo (form_error('website') ? form_error('website') : ''); ?>
								</div>
								<!--/form-group-->
								<div class="form-group col-md-4">
									<label>Address *</label>
									<input type="text" class="form-control" name="address"  placeholder="Lawyer address" value="<?php echo $this->input->post('address'); ?>">
									<?php echo (form_error('address') ? form_error('address') : ''); ?>
								</div>
								<div class="form-group col-md-4">
									<label>City *</label>
									<input type="text" name="city"   placeholder="Enter city name" class="form-control" value="<?php echo $this->input->post('city'); ?>">
									<?php echo (form_error('city') ? form_error('city') : ''); ?>
								</div>
								<!--/form-group-->
								<div class="form-group col-md-2">
									<div class="form-group">
										<label>State *</label>
										<select placeholder="state" class="form-control" name="state" >
											<option value="" > Select state </option>
											<?php foreach($states_short_names as $state){?>
											<option value="<?php echo $state->id;?>" <?php echo $this->input->post('state') == $state->id ? 'selected=""' : '' ?>> <?php echo $state->state_short_name;?> </option>
											<?php }?>
										</select>
										<?php echo (form_error('state') ? form_error('state') : ''); ?>
									</div>
								</div>
								<div class="form-group col-md-2">
									<label>Zip *</label>
									<input type="text" name="zipcode"   placeholder="Enter zip here" class="form-control" maxlength="5" value="<?php echo $this->input->post('zipcode'); ?>">
									<?php echo (form_error('zipcode') ? form_error('zipcode') : ''); ?>
								</div>
								<div class="clearfix"></div>
								<!--/form-group-->
								<div class="form-group col-md-4">
									<label>Initial setup charge *</label>
									<input type="text" name="initial_setup_charge"   placeholder="Enter initial setup charge" class="form-control" value="<?php echo $this->input->post('initial_setup_charge'); ?>">
									<?php echo (form_error('initial_setup_charge') ? form_error('initial_setup_charge') : ''); ?>
								</div>
								<div class="form-group col-md-4">
									<div class="form-group">
										<label>Fee type *</label>
										<select placeholder="Fee type" class="form-control" name="fee_type">
											<option value="" > Select Type </option>
											<option value="0" <?php echo isset($_POST['fee_type']) && $this->input->post('fee_type') == 0 ? 'selected=""' : '' ?>> Monthly </option>
											<option value="1" <?php echo $this->input->post('fee_type') == 1 ? 'selected=""' : '' ?>> Annual </option>
										</select>
										<?php echo (form_error('fee_type') ? form_error('fee_type') : ''); ?>
									</div>
								</div>
								<!--/form-group-->
								<div class="form-group col-md-4">
									<label>Fee *</label>
									<input type="text"  id="sub_cat" name="law_firm_fee"  placeholder="Fee" class="form-control" value="<?php echo ($this->input->post('law_firm_fee') ? $this->input->post('law_firm_fee') : 0.00); ?>">
									<?php echo (form_error('law_firm_fee') ? form_error('law_firm_fee') : ''); ?>
								</div>
								<div class="clearfix"></div>
								<?php 
								$post_case_types = ($this->input->post('case_type') ? $this->input->post('case_type') : array());
								$post_jurisdictions_array = ($this->input->post('jurisdiction') ? $this->input->post('jurisdiction') : array());
								?>
								<div class="form-group col-md-12 selection_boxes" style="padding:20px 15px;">
									<label>Case type *</label>
									<span class="button-checkbox">
										<button type="button" class="btn" data-color="primary">Select All</button>
										<input type="checkbox" class="hidden" />
									</span>
									<br />
									<?php $case_type_chunks = partition($case_type, 3); ?>
									<div class="row">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($case_type_chunks[0] as $c_type) { ?>
											<label class="checkbox-inline labelFullED">
											<input type="checkbox" value="<?php echo $c_type->type;?>" name="case_type[]" class="checkbox" id="inlinecheckbox1" <?php if(in_array($c_type->type,$post_case_types)) echo "checked"; ?>>
											<span class="custom-checkbox"></span> <?php echo $c_type->type;?>
											</label>
											<?php } ?>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($case_type_chunks[1] as $c_type) { ?>
											<label class="checkbox-inline labelFullED">
											<input type="checkbox" value="<?php echo $c_type->type;?>" name="case_type[]" class="checkbox" id="inlinecheckbox1" <?php if(in_array($c_type->type,$post_case_types)) echo "checked"; ?>>
											<span class="custom-checkbox"></span> <?php echo $c_type->type;?>
											</label>
											<?php } ?>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($case_type_chunks[2] as $c_type) { ?>
											<label class="checkbox-inline labelFullED">
											<input type="checkbox" value="<?php echo $c_type->type;?>" name="case_type[]" class="checkbox" id="inlinecheckbox1" <?php if(in_array($c_type->type,$post_case_types)) echo "checked"; ?>>
											<span class="custom-checkbox"></span> <?php echo $c_type->type;?>
											</label>
											<?php } ?>
										</div>
									</div>
									<?php echo (form_error('case_type[]') ? form_error('case_type[]') : ''); ?>
									<div class="clearfix"></div>
								</div>
								<div class="form-group col-md-12 selection_boxes" style="padding:20px 15px;">
									<label>Jurisdiction *</label>
									<span class="button-checkbox">
										<button type="button" class="btn" data-color="primary">Select All</button>
										<input type="checkbox" class="hidden" />
									</span>
									<br />
									<?php $jurisdiction_chunks = partition($jurisdictions, 3); ?>
									<div class="row">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($jurisdiction_chunks[0] as $jurisdiction) { ?>
											<label class="checkbox-inline labelFullED">
											<input type="checkbox" value="<?php echo $jurisdiction->name;?>" name="jurisdiction[]" class="checkbox" id="inlinecheckbox1" <?php if(in_array($jurisdiction->name,$post_jurisdictions_array)) echo "checked"; ?>>
											<span class="custom-checkbox"></span> <?php echo $jurisdiction->name;?> </label>
											<?php } ?>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($jurisdiction_chunks[1] as $jurisdiction) { ?>
											<label class="checkbox-inline labelFullED">
											<input type="checkbox" value="<?php echo $jurisdiction->name;?>" name="jurisdiction[]" class="checkbox" id="inlinecheckbox1" <?php if(in_array($jurisdiction->name,$post_jurisdictions_array)) echo "checked"; ?>>
											<span class="custom-checkbox"></span> <?php echo $jurisdiction->name;?> </label>
											<?php } ?>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<?php foreach($jurisdiction_chunks[2] as $jurisdiction) { ?>
											<label class="checkbox-inline labelFullED">
											<input type="checkbox" value="<?php echo $jurisdiction->name;?>" name="jurisdiction[]" class="checkbox" id="inlinecheckbox1" <?php if(in_array($jurisdiction->name,$post_jurisdictions_array)) echo "checked"; ?>>
											<span class="custom-checkbox"></span> <?php echo $jurisdiction->name;?> </label>
											<?php } ?>
										</div>
									</div>
									<?php echo (form_error('jurisdiction[]') ? form_error('jurisdiction[]') : ''); ?>
								</div>
							<?php } ?>
							<?php if($role == 2 || $role == 6) { ?>     
							<div class="form-group col-md-12">

						   <label>Lawyer - internal note (visible to admin only)</label>

						<textarea name="internal_notes"   placeholder="Add internal notes" class="form-control" value = "<?php echo $this->input->post('internal_notes'); ?>" rows="4"></textarea>

					      </div>
						  <div class="form-group col-md-12">

						   <label>Lawyer - account notes (visible to lawyer)</label>

						<textarea name="shared_notes"   placeholder="Add Lawyer Notes" class="form-control" value = "<?php echo $this->input->post('shared_notes'); ?>" rows="4"></textarea>

					      </div>
							<?php }?>
							<!--<div class="form-group col-md-4">
								<label>Profile image</label>
								<input type="file" name="image"  class="form-control" style="padding: 0 12px;"/>
								<?php echo (form_error('image') ? form_error('image') : ''); ?>
							</div>-->
							<?php if($role){?>	
							<div class="clearfix"></div>
							<?php }?>
							<div class="form-group col-md-4">
								<label>Activate</label>
								<select class="form-control" name="active">
									<option value="0" <?php echo $this->input->post('active') == 0 ? 'selected=""' : '' ?>>Inactive</option>
									<option value="1" <?php echo $this->input->post('active') == 1 ? 'selected=""' : $this->input->post('active') == '' ? 'selected=""' : '' ?>>Active</option>
								</select>
								<?php echo (form_error('active') ? form_error('active') : ''); ?>
							</div>
							<input type="hidden" name="password" value="<?php echo generateRandomPassword(6); ?>" />
							<div class="form-group col-md-12">
								<button class="btn btn-primary" id="submitBtn" type="submit"><?php if($role == 1){echo 'Add Admin';}elseif($role == 2){echo 'Add Lawyer';}elseif($role == 7){echo 'Add Lawyer Staff';}else{echo 'Add';}?></button>
								<a class="btn btn-default" href="<?php echo base_url(); ?>admin/users/admin">Cancel</a>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<script>
	function selectLawFirm(id, actionUrl, reloadUrl) {
		if (id == '') {
			$("#sub_cat").val('');
			return true;
		}
		$.post("<?php echo base_url();?>" + actionUrl, {
			id: id
		}, function(data) {
			$('#sub_cat').val(data.fee);
			$("[name='fee_type']").val(data.fee_type);
		}, "json");
	}

	$(function() {

		$('.button-checkbox').each(function() {

			// Settings

			var $widget = $(this)

				,
				$button = $widget.find('button')

				,
				$checkbox = $widget.find('input:checkbox')

				,
				color = $button.data('color')

				,
				settings = {

					on: {

						icon: 'glyphicon glyphicon-check'

					}

					,
					off: {

						icon: 'glyphicon glyphicon-unchecked'

					}

				};

			// Event Handlers

			$button.on('click', function() {

				$checkbox.prop('checked', !$checkbox.is(':checked'));

				$checkbox.triggerHandler('change');

			});

			$checkbox.on('change', function() {

				updateDisplay();

			});

			// Actions

			function updateDisplay() {

				var isChecked = $checkbox.is(':checked');

				$button.data('state', (isChecked) ? "on" : "off");

				$button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);

				if (isChecked) {

					$button.removeClass('btn-default').addClass('btn-' + color + ' active');

					$widget.parents('.selection_boxes').first().find('.checkbox').prop('checked', true);

				} else {

					$button.removeClass('btn-' + color + ' active').addClass('btn-default');

					$widget.parents('.selection_boxes').first().find('.checkbox').prop('checked', false);

				}

			}

			// Initialization

			function init() {

				var isChecked = $checkbox.is(':checked');

				$button.data('state', ($widget.parents('.selection_boxes').first().find('.checkbox:checked').length == $widget.parents('.selection_boxes').first().find('.checkbox').length ? "on" : "off"));

				$button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);

				if ($widget.parents('.selection_boxes').first().find('.checkbox:checked').length == $widget.parents('.selection_boxes').first().find('.checkbox').length) {
					$button.removeClass('btn-default').addClass('btn-' + color + ' active');
					$checkbox.prop('checked', true);
				} else {
					$button.removeClass('btn-' + color + ' active').addClass('btn-default');
					$checkbox.prop('checked', false);
				}
				if ($button.find('.state-icon').length == 0) {
					$button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
				}
			}
			init();
		});
	});
    function getLawfirmLawyers(id)
	{
		$('#submitBtn').css('cursor','not-allowed').attr('disabled', true);
		if(id !== ''){
			$.ajax({
				url: '<?php echo base_url();?>admin/Dashboard/getLawfirmLawyers',
				data: {id: id},
				type: 'post',
				success: function(data){
					if(data != ''){
						$('#submitBtn').removeAttr('disabled').css('cursor','pointer');
						$('#lawyers').removeAttr('disabled').html(data);
					} else {
						$('#lawyers').attr('disabled', true).html('');
					}
				}
			});
		} else {
			$('#lawyers').attr('disabled', true).html('');
		}
	}
</script>