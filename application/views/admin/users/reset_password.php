<?php $session_id=$this->session->userdata('id'); ?>
<?php if(isset($_GET['firstTime'])){ ?>
<script>
	alert('You logged in for the first time. Please update your password.');
</script>
<?php updateLoggedInStatus($session_id); } ?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Change Password</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Change Password</h3>
					</div>
					<?php if($this->session->flashdata('message_success')){
						echo $this->session->flashdata('message_success');
					} ?>
					<span class="pwd-note">Passwords must be at least 6 characters and contain one number and one uppercase letter.</span><br>
					<div class="porlets-content">
						<form action="<?php echo base_url();?>admin/users/reset_password/<?php echo $session_id;?>" method="post" parsley-validate novalidate>
							<input type="hidden" name="user_id" value="<?php echo $session_id;?>">
							<div class="col-sm-4 form-group">
								<label>Write your old password *</label>
								<input type="password" name="old" parsley-trigger="change"  required placeholder="Enter Your new password" class="form-control">
								<?php echo (form_error('old') ? form_error('old') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="col-sm-4 form-group">
								<label>Write your new password *</label>
								<input type="password" name="password" parsley-trigger="change"  required placeholder="Enter Your new password" class="form-control">
								<?php echo (form_error('password') ? form_error('password') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="col-sm-4 form-group">
								<label>Confirm your new password *</label>
								<input type="password" name="confirm_password" parsley-trigger="change"  required placeholder="Enter Your new password" class="form-control">
								<?php echo (form_error('confirm_password') ? form_error('confirm_password') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="clearfix"></div>
							<div class="col-sm-3 form-group">
								<button class="btn btn-primary" type="submit">Reset password</button>
								<a class="btn btn-default" href="<?php echo base_url();?>admin/dashboard/manage"> Cancel</a>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>