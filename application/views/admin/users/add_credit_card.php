<?php 

$session_id=$this->session->userdata('id');



?>
<div id="main-content">

    <div class="page-content">

      <div class="row">

        <div class="col-md-12">

          <h2>Add Credit Card </h2>

        </div><!--/col-md-12--> 

      </div><!--/row-->

      

      

      

      <div class="row">

        <div class="col-md-12">

          <div class="block-web">

            <div class="header">

              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>

              <h3 class="content-header">Add credit card</h3>

            </div>
  	<?php 			if($this->session->flashdata('message')){				echo '<span id="flash_message">' . $this->session->flashdata('message') . '</span>';		}				    ?>    
            <div class="porlets-content">

              <form action="<?php echo base_url();?>admin/dashboard/saveCreditCard/<?php echo $session_id;?>" method="post" parsley-validate novalidate>

               <div class="form-group col-sm-4">                  <label>Select card type *</label>                                     <select placeholder="Card type" class="form-control parsley-validated" name="card_type" required="">                       	<option value=""> Select type </option>                      	<option value="visa">Visa</option>                      	<option value="Mastercard"> Mastercard</option>      			   </select>                </div>
                 <div class="form-group col-sm-4">
                  <label>Credit card number *</label>
                  <input type="text" name="card_no" parsley-trigger="change"  required placeholder="Enter credit card number" class="form-control">
                </div><!--/form-group-->  				<div class="form-group col-sm-4">
                  <label>Expiry date *</label>
                  <input type="text" name="exp_date" parsley-trigger="change" id="datepicker" required placeholder="Enter expiry date" class="form-control">
                </div><!--/form-group--> 								<div class="form-group col-sm-4">
                  <label>Card Code Verification (CCV) *</label>
                  <input type="text" name="ccv" parsley-trigger="change" id="ccv" required placeholder="Enter card code verification (CCV)" class="form-control">
                </div><!--/form-group-->              				<div class="clearfix"></div>	
				<div class="form-group col-sm-4">
                <button class="btn btn-primary" type="submit">Submit</button>
				<a href="<?php echo base_url(); ?>admin/dashboard/manage" class="btn btn-default">Cancel</a>	
                
				</div><!--/form-group-->			  		</form>	<div class="clearfix"></div>		
            </div><!--/porlets-content-->

          </div><!--/block-web--> 

        </div><!--/col-md-6-->

        

         

      </div><!--/row-->

      

     

      

    </div><!--/page-content end--> 

  </div>
  
    <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  $(function() {
    $( "#datepicker2" ).datepicker();
  });
  </script>
  