<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Operators</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Operators</h3>
            </div>									<?php 						if($this->session->flashdata('message')){					echo $this->session->flashdata('message');			}										 //chechUserAccess();								?>
         <div class="porlets-content">
            <div class="table-responsive">
            <div class="clearfix">
                              <div class="btn-group">
                                  <a href="<?php echo base_url();?>admin/users/add/5"> <!-- 2 is role that define type. 2 is for lawyers -->
								   <button class="btn btn-primary">
                                      Add New <i class="fa fa-plus"></i>
                                  </button></a>
                              </div>
                              <!--
							  <div class="btn-group pull-right">
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                  </button>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="#">Print</a></li>
                                      <li><a href="#">Save as PDF</a></li>
                                      <li><a href="#">Export to Excel</a></li>
                                  </ul>
                              </div>
							 --> 
                          </div>
                          <div class="margin-top-10"></div>
                <table  class="display table table-bordered table-striped" id="dynamic-table">
                  <thead>
                    <tr>
                      <th>First name</th>
                      <th>Last name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <!--
					  <th>City</th>
                      <th>State</th>
                      <th>Zip</th>
                      <th>Address</th>
					-->  
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php 
				   if($users != ''){
					   
				   foreach($users as $user){?>
                    <tr class="gradeX">
                      <td><?php echo $user->name; ?></td>
                      <td><?php echo $user->lname; ?></td>
                      <td><?php echo $user->email; ?></td>
                      <td><?php echo $user->phone; ?></td>
					 
                   <!--   <td><?php //echo $user->city; ?></td>
                      <td><?php //echo $user->state; ?></td>
                      <td><?php //echo $user->zipcode; ?></td>
                      <td><?php //echo $user->address; ?></td> -->
					  
                      <td style="cursor:pointer"><a href="<?php echo base_url();?>admin/users/edit/<?php echo $user->id;?>" >Edit </a> | <a data-toggle="modal" data-target="#myModal<?php echo $user->id;?>">Delete </a></td> <!-- 2 define role which is specify it is lawyer -->					   					   <div class="modal fade" id="myModal<?php echo $user->id;?>" role="dialog">						<div class="modal-dialog modal-sm">						  <div class="modal-content">							<div class="modal-header">							  <button type="button" class="close" data-dismiss="modal">&times;</button>							  <h4 class="modal-title">Confirm Delete</h4>							</div>							<div class="modal-body">							  <p>Are you sure you want to delete this record?</p>							</div>							<div class="modal-footer">							  <a class="btn btn-primary" <a href="<?php echo base_url();?>admin/users/delete/<?php echo $user->id;?>/5" >Yes </a>							  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>							</div>						  </div>						</div>					  </div>					  					  
                    </tr>
                   <?php
				   }
				   } ?> 
                  </tbody>
                 
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
            
            
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
       
 
    </div><!--/page-content end--> 
  </div><!--/main-content end-->
</div>