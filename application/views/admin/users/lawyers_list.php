<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Lawyer View</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header" style="margin-bottom:40px;">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
					</div>
					<div class="form-group col-md-4">
						<label>First name</label>
						<input type="text" name="name" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->name; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Last name</label>
						<input type="text" name="lname" parsley-trigger="change" required placeholder="Enter last name" class="form-control" value="<?php echo $lawyer->lname; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Email</label>
						<input type="text" name="email" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->email; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="clearfix"></div>
					<div class="form-group col-md-4">
						<label>Phone</label>
						<input type="text" name="phone" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->phone; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Law firms</label>
						<input type="text" name="law_firms" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->law_firm_name; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Law firm fee</label>
						<input type="text" name="law_firms" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->law_firm_fee; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Website</label>
						<input type="text" name="website" parsley-trigger="change" required placeholder="Enter website" class="form-control" value="<?php echo $lawyer->website; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Address</label>
						<input type="text" name="address" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->address; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>City</label>
						<input type="text" name="city" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->city; ?>" disabled="disabled">
					</div>
					<div class="form-group col-md-4">
						<label>State</label>
						<input type="text" name="city" parsley-trigger="change" required placeholder="Enter state name" class="form-control" value="<?php echo getStateShortName($lawyer->state); ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Zip code</label>
						<input type="text" name="city" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->zipcode; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Created at</label>
						<input type="text" name="created_at" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $newDate = date("m-d-Y h:i:s", strtotime($lawyer->created_at));				   ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-12">
						<label>Case type</label>
						<textarea name="city" parsley-trigger="change" required placeholder="Enter first name" class="form-control" disabled="disabled" rows="6"><?php echo str_replace(",", ", ", $lawyer->case_type); ?></textarea>
					</div>
					<!--/form-group-->
					<div class="form-group col-md-12">
						<label>Jurisdiction</label>
						<textarea name="city" parsley-trigger="change" required placeholder="Enter jurisdiction" class="form-control" disabled="disabled" rows="6"><?php echo str_replace(",", ", ", $lawyer->jurisdiction); ?></textarea>
					</div>
					<div class="form-group col-md-12">
						<label>Lawyer - internal note (visible to admin only)</label>
						<textarea name="internal_notes" class="form-control" rows="4" disabled><?php echo $lawyer->internal_notes; ?></textarea>
					</div>
					<div class="form-group col-md-12">
						<label>Lawyer - account notes (visible to lawyer)</label>
						<textarea name="shared_notes" class="form-control" rows="4" disabled><?php echo $lawyer->shared_notes; ?></textarea>
					</div>
					<div class="form-group col-md-4">
						<label>Last login date </label>
						<input type="text"  value="<?php echo date("m-d-Y", strtotime($lawyer->last_login_date)); ?>"  class="form-control" disabled>													</div>						<div class="form-group col-md-4">														<label>Last Ip address</label>														<input type="text"   value="<?php echo $lawyer->last_login_ip; ?>"  class="form-control" disabled>													</div>
					<!--/form-group-->
					<div class="clearfix"></div>
					<!--<div class="form-group col-md-3">
						<label>Profile image</label>
						<?php if(!empty($lawyer->image)) {?>
						<a href="#"> <img alt="user" src="<?php echo base_url();?>uploads/<?php echo $lawyer->image;?>" style="height:180px; width:265px;"> </a>
						<?php }else{?>	
						<a href="#"> <img src="<?php echo base_url();?>assets/admin/images/avatar1.jpg" style="height:120px; width:185px;"> </a>
						<?php }?>
					</div>-->
					<!--/form-group-->
					<div class="clearfix"></div>
				</div>
				<!--/porlets-content-->
			</div>
			<!--/block-web--> 
		</div>
		<!--/col-md-6-->
	</div>
	<!--/row-->
</div>
<!--/page-content end--> 
</div>