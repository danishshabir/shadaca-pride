<?php $session_id=$this->session->userdata('id'); ?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Edit Profile Image</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
					</div>
					<div class="porlets-content">
						<form action="<?php echo base_url();?>admin/users/updateProfile/<?php echo $session_id;?>" method="post" parsley-validate novalidate enctype="multipart/form-data">
							<div class="form-group col-md-4">
								<label>Profile Image</label><br>
								<img src="<?php echo base_url();?>uploads/<?php echo $user_information->image;?>" style="width:105px; height:125px;"></img>
								<input type="file" name="image"/>
								<input type="hidden" name="image" value="<?php echo $user_information->image;?>"/>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-12">
								<button class="btn btn-primary" type="submit">Submit</button>
								<a class="btn btn-default" href="<?php echo base_url();?>admin/users/userProfile/<?php echo $session_id?>"> Cancel</a>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>