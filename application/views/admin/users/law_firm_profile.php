<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Law Firm Profile</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Law Firm Profile</h3>
						<span class="note-text">Contact <a href="mailto:support@pridelegal.com">support@pridelegal.com</a> to update your law firm account information.</span>
					</div>
					<div class="porlets-content">
						<div class="form-group col-md-4">
							<label>Firm Name *</label>
							<input type="text" value="<?php echo ($this->input->post('law_firm') ? $this->input->post('law_firm') : $lead->law_firm); ?>"  placeholder="Firm Name" class="form-control" disabled="disabled">
						</div>
						<!--/form-group-->
						<div class="form-group col-md-4">
							<label>Phone *</label>
							<input type="text" value="<?php echo ($this->input->post('phone') ? $this->input->post('phone') : $lead->phone); ?>"   placeholder="Enter phone no:" class="form-control" disabled="disabled">
						</div>
						<!--/form-group-->
						<div class="form-group col-md-4">
							<label>Website</label>
							<input type="text" value="<?php echo ($this->input->post('website') ? $this->input->post('website') : $lead->website); ?>"   placeholder="Enter website" class="form-control" disabled="disabled">
						</div>
						<!--/form-group-->
						<div class="form-group col-md-4">
							<label>Contact First Name *</label>
							<input type="text" value="<?php echo ($this->input->post('contact_name') ? $this->input->post('contact_name') : $lead->contact_name); ?>"   placeholder="Contact First Name" class="form-control" disabled="disabled">
						</div>
						<!--/form-group-->
						<div class="form-group col-md-4">
							<label>Contact Last Name *</label>
							<input type="text" value="<?php echo ($this->input->post('contact_last_name') ? $this->input->post('contact_last_name') : $lead->contact_last_name); ?>"  placeholder="Contact Last Name" class="form-control" disabled="disabled">
						</div>
						<!--/form-group-->
						<div class="form-group col-md-4">
							<label>Contact email *</label>
							<input type="email" value="<?php echo ($this->input->post('contact_email') ? $this->input->post('contact_email') : $lead->contact_email); ?>"   placeholder="Enter contact email" class="form-control" disabled="disabled">
						</div>
						<!--/form-group-->
						<div class="form-group col-md-4">
							<label>Street *</label>
							<input type="text" value="<?php echo ($this->input->post('street') ? $this->input->post('street') : $lead->street); ?>"  placeholder="Enter street" class="form-control" disabled="disabled">
						</div>
						<!--/form-group-->
						<div class="form-group col-md-4">
							<label>City *</label>
							<input type="text" placeholder="Enter city here" class="form-control" value="<?php echo ($this->input->post('city') ? $this->input->post('city') : $lead->city); ?>" disabled="disabled">
						</div>
						<!--/form-group-->
						<div class="form-group col-md-2">
							<div class="form-group">
								<label>State *</label>
								<select placeholder="state" class="form-control" id="state" disabled="disabled">
									<option value="" > Select state </option>
									<?php foreach($states_short_names as $state){?>
									<option value="<?php echo $state->id;?>"> <?php echo $state->state_short_name;?> </option>
									<?php }?>
								</select>
							</div>
						</div>
						<div class="form-group col-md-2">
							<label>Zip *</label>
							<input type="text" placeholder="Enter zip here" class="form-control" value="<?php echo ($this->input->post('zip') ? $this->input->post('zip') : $lead->zip); ?>" maxlength="5" disabled="disabled">
						</div>
						<!--/form-group-->
						<div class="form-group col-md-4">
							<div class="form-group">
								<label>Fee Type *</label>
								<select placeholder="Fee Type" class="form-control" id="fee_type" disabled="disabled">
									<option value="" > Select Type </option>
									<option value="0"> Monthly </option>
									<option value="1"> Annual </option>
								</select>
							</div>
						</div>
						<div class="form-group col-md-4">
							<label>Fee *</label>
							<input type="text" id="law_firm_fee" value="<?php echo number_format($lead->law_firm_fee, 2); ?>"  placeholder="Fee" class="form-control" value="<?php echo ($this->input->post('law_firm_fee') ? $this->input->post('law_firm_fee') : $lead->law_firm_fee); ?>" disabled="disabled">
						</div>
						<!--/form-group-->
						<div class="form-group col-md-4">
							<label>Customer ID</label>
							<input type="text" value="<?php echo $lead->customer_id; ?>"  placeholder="Enter customer ID" class="form-control" disabled="disabled">
						</div>
						<div class="clearfix"></div>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row--><br>
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header"><?php echo $law_firm->law_firm; ?> Lawyers</h3>
					</div>
					<div class="porlets-content">
						<div class="table-responsive">
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped dynamic-table">
								<thead>
									<tr>
										<th>First name</th>
										<th>Last name</th>
										<th>Case Types</th>
										<th>Jurisdictions</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($users as $user){?>
									<tr class="gradeX <?php if($user->active == '0'){?> red_simple <?php }?>">
										<td><?php echo $user->name; ?></td>
										<td><?php echo $user->lname; ?></td>
										<td><?php echo str_replace(",", ", ", $user->case_type); ?></td>
										<td><?php echo str_replace(",", ", ", $user->jurisdiction); ?></td>
										<!-- 2 define role which is specify it is lawyer -->
									</tr>
									<?php } ?> 
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
        <!--/row--><br>
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header"><?php echo $law_firm->law_firm; ?> Law Staff</h3>
					</div>
					<div class="porlets-content">
						<div class="table-responsive">
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped dynamic-table">
								<thead>
									<tr>
										<th>First name</th>
										<th>Last name</th>
										<th>Email</th>
										<th>Lawyer Assignment</th>
                                        <th>Email On</th>
									</tr>
								</thead>
								<tbody>
                                    <?php //echo '<pre>'; print_r($lawstaff); exit(); ?>
									<?php $i = 0; foreach($lawstaff as $staff){?>
									<tr>
										<td><?php echo $staff['name']; ?></td>
										<td><?php echo $staff['lname']; ?></td>
										<td><?php echo $staff['email']; ?></td>
										<td><?php echo ( $curr_lawyer[$i] == 'All' ? 'All' : $curr_lawyer[$i]->name . ' ' . $curr_lawyer[$i]->lname); ?></td>
										<td><input type="checkbox" class="change_send_email" attr-user-id="<?php echo $staff['lawyerstaff_id']; ?>" value="1" style="opacity: 1;" <?php echo ( $staff['send_email_user'] == 1 ? 'checked=""' : '' ); ?> /></td>
                                        <!-- 7 define role which is specify it is lawyer staff -->
									</tr>
									<?php $i++; } ?> 
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<script type="text/javascript">
	$("#state").val('<?php echo ($this->input->post('state') ? $this->input->post('state') : $lead->state) ?>');
	$("#fee_type").val('<?php echo ($this->input->post('fee_type') ? $this->input->post('fee_type') : $lead->fee_type) ?>');
</script>