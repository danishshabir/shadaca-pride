<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Manage Payment Profile</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<form action="<?php echo base_url();?>admin/users/manage_payment_profile/<?php echo $law_firm->id;?>" method="post" parsley-validate novalidate enctype="multipart/form-data">
					<div class="block-web">
						<div class="header">
							<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
							<h3 class="content-header">Law Firm Payment Profile</h3>
						</div>
						<?php if($this->session->flashdata('message')) {
							echo $this->session->flashdata('message');
						} ?>
						<div class="porlets-content">
							<div class="form-group col-md-4">
								<label>Law firm name *</label>
								<input type="text" name="law_firm" parsley-trigger="change" value="<?php echo $law_firm->law_firm; ?>" required placeholder="Enter law firm" class="form-control">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Contact email *</label>
								<input type="email" name="contact_email" value="<?php echo $law_firm->contact_email; ?>" parsley-trigger="change" required placeholder="Enter contact email" class="form-control">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Customer ID</label>
								<input type="text" name="customer_id" value="<?php echo $law_firm->customer_id; ?>" parsley-trigger="change" placeholder="Enter customer ID" class="form-control" readonly>
							</div>
							<!--/form-group-->
							<div class="clearfix"></div>
						</div>
						<!--/porlets-content-->
					</div>
					<br>
					<div class="block-web">
						<div class="header">
							<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
							<h3 class="content-header">Add Payment Information</h3>
						</div>
						<div class="porlets-content">
							<div class="form-group col-md-4">
								<label>First name on card *</label>
								<input type="text" name="first_name" parsley-trigger="change" value="<?php echo set_value('first_name'); ?>" required placeholder="First name" class="form-control">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Last name on card *</label>
								<input type="text" name="last_name" value="" parsley-trigger="change" required placeholder="Last name" class="form-control">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Company *</label>
								<input type="text" name="company" value="" parsley-trigger="change" required placeholder="Company" class="form-control">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Card billing address *</label>
								<input type="text" name="billing_address" value="" parsley-trigger="change" required placeholder="Card billing address" class="form-control">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<div class="form-group">
									<label>State *</label>
									<select placeholder="state" class="form-control" name="state" required>
										<option value="" required> Select state </option>
										<?php foreach($states_short_names as $state){?>
										<option value="<?php echo $state->id;?>"> <?php echo $state->state_short_name;?> </option>
										<?php }?>
									</select>
								</div>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>City *</label>
								<input type="text" name="city" parsley-trigger="change" required placeholder="Enter city here" class="form-control" value="">
							</div>
							<div class="clearfix"></div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Zip *</label>
								<input type="text" name="zip" parsley-trigger="change" required placeholder="Enter zip here" class="form-control" value="" maxlength="5">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Phone *</label>
								<input type="text" name="phone" value="" parsley-trigger="change" required placeholder="Enter phone no:" class="form-control">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Credit card number *</label>
								<input type="text" name="card_number" parsley-trigger="change" required placeholder="Credit card number" class="form-control">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Expiration date (MM/YYYY) *</label>
								<input type="text" name="expiry" parsley-trigger="change" required placeholder="Expiration date" class="form-control">
							</div>
							<!--/form-group-->
							<!--
							<div class="form-group col-md-4">
								<label>CVV Code *</label>
								<input type="text" name="<?php //echo 'cw_code'; ?>" parsley-trigger="change" required placeholder="CVV Code" class="form-control">
							</div>
							-->
							<!--/form-group-->
							<div class="form-group col-sm-4">
								<label>Select card type</label>
								<select placeholder="Card type" class="form-control" name="card_type" required>
									<option value=""> Select type </option>
									<option value="Visa">Visa</option>
									<option value="MasterCard"> Mastercard</option>
									<option value="American Express"> American Express</option>
									<option value="Discover"> Discover</option>
									<option value="JCB"> JCB</option>
								</select>
							</div>
							<div class="form-group col-md-12">
								<button class="btn btn-primary" type="submit">Submit</button>
							</div>
							<div class="clearfix"></div>
						</div>
						<!--/porlets-content-->
					</div>
				</form>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div><br>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header"><?php echo $law_firm->law_firm; ?></h3>
					</div>
					<div class="porlets-content">
						<div class="table-responsive">
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="payment-table">
								<thead>
									<tr>
										<th>Default</th>
										<th>Profile ID</th>
										<th>Cardholder Name</th>
										<th>Card Number</th>
										<th>Expiration</th>
										<th>CVV</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($CustomerProfiles as $row){ 
										$response = $this->functions->getCustomerProfile($row->customer_profile_id,$row->profile_id);
										if($response['customerPaymentProfileId']) { ?>
											<tr class="gradeX ">
												<td><?php echo ($row->default == 1 ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-times" aria-hidden="true"></i>'); ?></td>
												<td><?php echo $response['customerPaymentProfileId']; ?></td>
												<td><?php echo $response['first_name'].' '.$response['last_name']; ?></td>
												<td><?php echo $response['card_number']; ?></td>
												<td><?php echo $response['expiry']; ?></td>
												<td><?php echo $row->cw_code; ?></td>
												<td><a href="#" data-toggle="modal" data-target="#myModal<?php echo $row->id;?>">Delete </a><?php if($row->default == 0) { ?> | <a href="<?php echo base_url();?>admin/users/default_payment_profile/<?php echo $law_firm->id;?>/<?php echo $row->id ?>" >Set Default </a><?php } ?></td>
											</tr>
											<div class="modal fade" id="myModal<?php echo $row->id;?>" role="dialog" >
												<div class="modal-dialog modal-sm">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h4 class="modal-title">Confirm Delete</h4>
														</div>
														<div class="modal-body">
															<p>Are you sure you want to delete this record ?</p>
														</div>
														<div class="modal-footer">
															<a class="btn btn-primary" href="<?php echo base_url();?>admin/users/delete_payment_profile/<?php echo $law_firm->id;?>/<?php echo $row->id ?>">Yes </a>
															<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
														</div>
													</div>
												</div>
											</div>
									<?php }
									} ?> 
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<script>
	$(document).ready(function() {
		$('#payment-table').dataTable( {
			"iDisplayLength": 100,
			"oLanguage": { "sEmptyTable": "No payment information saved" }
		} );
	});
</script>