<form action="<?php echo base_url();?>admin/transaction/addBatchProcessing/<?php echo $law_firm->id; ?>" id="addBatchProcessing" method="post" parsley-validate novalidate>
	<div class="table-responsive" id="hide_table">
		<div class="clearfix"></div>
		<div class="margin-top-10"></div>
		<table  class="display table table-bordered table-striped" id="dynamic-table">
			<thead>
				<tr>
                    <?php #if($is_credit_card > 0 && $balance > 0){ ?>
                    <?php if($this->session->userdata('role') == 1){ ?>
					   <th>Select Batch</th>
                    <?php } ?>
                    <?php #} ?>
					<th style="width:150px;">Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Law Firm</th>
					<th width="10%">Balance</th>
					<th>Leads Detail</th>      
					<th>Account Billing</th>      
					<?php #if($is_credit_card > 0 && $balance > 0){ ?>
                    <?php if($this->session->userdata('role') == 1){ ?>
                        <th>Pay Bill</th>
                    <?php } ?>
                    <?php #} ?>
				</tr>
			</thead>
			<tbody class="searchable">
				<?php
				$is_credit_card = $this->Central_model->count_rows("credit_cards", array("law_firm" => $law_firm->id));
				if($lawyers) {
					$i = 0;  
					foreach($lawyers as $row) {
						$balance = number_format(($row->total_lead_price+$row->monthly_payment+$row->total_charge_bill-$row->paidTransactions), 2);
						?>       
						<tr>
                            <?php if($this->session->userdata('role') == 1){ ?>
                            <td>
                                <?php if($is_credit_card > 0 && $balance > 0){ ?>
                                    <input type="checkbox" style="cursor: pointer;height: 18px;opacity: 10 !important;position: absolute;width: 18px;transform: scale(1.2);z-index: 2; " name="lawyers_ids[]" value="<?php echo $row->id;?>" id="checkbox" />
                                <? } ?>
                            </td>
                            <? } ?>
							<td><?php echo $row->name; ?></td>
							<td><?php echo $row->email; ?></td>
							<td><?php echo $row->phone; ?></td>
							<td><?php echo getLawsNames($row->law_firms); ?></td>
							<td align="right"><?php echo "$" . $balance; ?></td>
							<td> <a href="<?php echo base_url();?>admin/dashboard/LawyerAllLeads/<?php echo $row->id;?>">Details</a></td>
							<td> <a href="<?php echo base_url();?>admin/dashboard/LawyerBills/<?php echo $row->id;?>">Details</a></td>
                            <?php if($this->session->userdata('role') == 1){ ?>
                            <td>
                                <?php if($is_credit_card > 0 && $balance > 0){ ?>
    								<a class="btn btn-info" href="<?php echo base_url();?>admin/dashboard/makePayment/<?php echo $law_firm->id; ?>/<?php echo $row->id;?>">
    									Make payment  <!--<button class="btn btn-info"></button> -->
    								</a>
                                <?php } ?>	
							</td>
                            <?php } ?>
						</tr>
						<?php 
						$i++; 
					} 
				} 
				?>
			</tbody>
		</table>
	</div>
	<!--/table-responsive-->   
	<div class="clearfix"></div><br>
    <?php if($this->session->userdata('role') == 1){ ?>
	   <button class="btn btn-primary addBatchProcessing" type="submit">Add For Batch Processing</button>
    <?php } ?>
</form>