<table  class="display table table-bordered table-striped" id="dynamic-table">
	<thead>
		<tr>
			<th >Date</th>
			<th >Case Id</th>
			<th >Lead Name</th>
			<th >Market</th>
			<th >Case Type</th> 
			<th width="10%">Amount</th>     
		</tr>
	</thead>
	<tbody>
		<?php foreach($leads_details as $leads_detail)
		{
			$caseTypeName =  getCaseTypeName($leads_detail->case_type);
			$market_name =  getMarketName($leads_detail->market); 
			$stateName =  getStateName($market_name->state);
			?>
			<tr>
				<td><?php echo ($leads_detail->assigned_date ? date("m-d-Y h:i:s A", strtotime($leads_detail->assigned_date)) : "Not open") ?></td>
				<td><a href="<?php echo base_url(); ?>admin/leads/view/<?php echo $leads_detail->lead_id . '/' . $leads_detail->unique_code; ?>"><?php echo $leads_detail->lead_id_number;?></a></td>
				<td><?php echo $leads_detail->first_name.' '.$leads_detail->last_name; ?></td>
				<td><?php echo  $market_name->market_name  . "-" . $stateName[0]['state_short_name']; ?></td>
				<td><?php echo $caseTypeName[0]['type']; ?></td> 
				<td align="right"><?php echo '$'.number_format($leads_detail->price, 2); ?></td>
			</tr>
			<?php 
			$total_Billing += $leads_detail->price;
			$total_balance = number_format($total_Billing, 2);
		} ?> 	
	</tbody>
</table>
<?php if($total_balance > 0) { ?>
	<h3 class="content-header">Total = <b><?php echo "$".$total_balance; ?></b></h3><br>
<?php } ?>