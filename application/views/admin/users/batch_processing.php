<?php $session_id=$this->session->userdata('id'); ?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2> Batch processing </h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Batch processing </h3>
					</div>
					<?php if($this->session->flashdata('message')){
						echo $this->session->flashdata('message');
						} ?>
					<div class="porlets-content">
						<form action="<?php echo base_url();?>admin/transaction/chargeBatchProcessing" method="post" parsley-validate="" novalidate="" id="_form">
							<table  class="display table table-bordered table-striped">
								<thead>
									<tr>
										<th>Lawyer name</th>
										<th>Law firm</th>
										<th>Balance</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$i = 0;
									foreach($totalArraysIds as $lawyer_id){ 
										$lawyer_data = getLawyer($lawyer_id);	  
									?>
									<tr class="row_<?php echo $lawyer_id;?>">
										<input type="hidden" name="amount[]" parsley-trigger="change" placeholder="Enter amount" class="form-control" value="<?php echo $totalArrays[$i]; ?>" >
										<input type="hidden" name="lawyer_id[]" parsley-trigger="change" placeholder="Enter lawyer id" class="form-control" value="<?php echo $lawyer_id; ?>" >
										<td><?php echo $lawyer_data->name . " " . $lawyer_data->lname; ?></td>
										<td> <?php echo getLawsNames($lawyer_data->law_firms);?></td>
										<td><?php echo number_format($totalArrays[$i], 2); ?></td>
									</tr>
									<?php $i++; } ?>
									<input type="hidden" name="lawfirm_id" value="<?php echo $lawfirm_id;?>" parsley-trigger="change"  class="form-control">
								</tbody>
							</table>
							<br/>
							<div>
								<button class="btn btn-success">Pay now</button>
							</div>
						</form>
						<div class="clearfix"></div>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<script>
	$("#_form").submit(function(){
		var $this = $(this);
		var url = $this.attr('action');
		$.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			data: $this.serialize(),
			success: function(data) {
				$.each(data, function (index, value) {
					if(data['success'] == 0)
					{
						alert(data['text']);
					}
					else if(value['success'] == 1) {
						$('<i class="fa fa-check" aria-hidden="true"></i>').appendTo(".row_"+index+" td:first");
					} else {
						$('<i class="fa fa-times-circle" aria-hidden="true"></i>').appendTo(".row_"+index+" td:first");
					}
					window.location.href = '<?php echo base_url(); ?>admin/dashboard/LawyerBilling/'+data['lawfirm_id'];
				});
			}
		});
		return false;
	});
</script>