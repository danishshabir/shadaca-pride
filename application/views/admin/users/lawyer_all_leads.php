<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Lawyer Leads Bill </h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header text-center" style="font-weight:700;"><?php echo "Lawyer name: " . $lawyer->name . " " . $lawyer->lname; ?>	</h3>
					</div>
					<div class="form-group col-md-3">
						<label>Date from</label>
						<input type="text" name="date_from" parsley-trigger="change" placeholder="Enter Date From" class="form-control datepicker-here" data-date-format="mm-dd-yyyy" data-language="en">
					</div>
					<div class="form-group col-md-3">
						<label>Date to</label>
						<input type="text" name="date_to" parsley-trigger="change" placeholder="Enter Date To" class="form-control datepicker-here" data-date-format="mm-dd-yyyy" data-language="en">
					</div>
					<div class="form-group col-md-3">
						<label>&nbsp;</label>
						<button class="form-control btn btn-success search">Search</button>
					</div>
					<div class="col-md-12">
                        <p><b>Default display shows last 30 days activity.</b></p>
					</div>
					<div class="porlets-content">
						<div class="table-responsive">
							<div id="records">
								<?php $this->load->view('admin/users/billings/leads_bill'); ?>
							</div>
							<?php $total_var = number_format($total, 2);
							if($total_var > 0) { ?>
								<h3 class="content-header">Balance = <b><?php echo "$".$total_var; ?></b></h3><br>
							<?php } 
							if($this->session->userdata('role') == '1') { ?>
								<a class="btn btn-info" href="<?php echo base_url();?>admin/dashboard/makePayment/<?php echo $lawyer->law_firms; ?>/<?php echo $lawyer->id;?>">
									Make payment
								</a>
							<?php } ?>
						</div>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<script>	
	function load_dataTable()
	{
		$('#dynamic-table').dataTable({
			"ordering": true,
			"iDisplayLength": 100
			, "aaSorting": [[4, "desc"]]
		});
	}
	$(".search").click(function(e) 
	{
		if($("[name=date_from]").val() && $("[name=date_to]").val()) {
			$(".page_loader").show();
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>admin/dashboard/LawyerAllLeads/<?php echo $lawyer->id;?>',
				data: {
					date_from: $("[name=date_from]").val(),
					date_to: $("[name=date_to]").val()
				},
				success: function(response) {
					$(".page_loader").hide();
					data = jQuery.parseJSON(response);
					$("#records").html(data.html);
					load_dataTable();
				}
			});
		}
		e.preventDefault();
	});
</script>