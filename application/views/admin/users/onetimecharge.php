<?php $session_id=$this->session->userdata('id'); ?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>One time charge </h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">One time charge </h3>
					</div>
					<?php echo $this->session->flashdata('message');?>
					<div class="porlets-content">
						<form action="<?php echo current_url(); ?>" method="post" parsley-validate="" novalidate="">
							<div class="form-group col-md-4">
								<label>Amount</label>
								<input type="text" name="amount" parsley-trigger="change" required placeholder="Enter amount" class="form-control" value="<?php echo $this->input->post('amount'); ?>">
								<?php echo (form_error('amount') ? form_error('amount') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Description</label>
								<input type="text" name="description" parsley-trigger="change" required placeholder="Enter Description" class="form-control" value="<?php echo ($this->input->post('description') ? $this->input->post('description') : 'One Time Charge'); ?>">
								<?php echo (form_error('description') ? form_error('description') : ''); ?>
							</div>
							<!--/form-group-->
							<input type="hidden" name="status" value="1"/>
							<div class="form-group col-md-12">
								<a href="<?php echo base_url(); ?>admin/dashboard/LawyerBills/<?php echo $this->uri->segment(4); ?>" class="btn btn-default">Back</a>
								<button class="btn btn-success">Save</button>
							</div>
							<div class="clearfix"></div>
						</form>
						<div class="clearfix"></div>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>