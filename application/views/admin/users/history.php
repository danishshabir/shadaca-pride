<div id="main-content">
	<div class="page-content">
		<span id="payment_form"></span>
		<div class="row">
			<div class="col-md-12">
				<h2>History</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<span style="margin-top:20px; padding-top:20px;"></span>
						<div class="clearfix"></div>
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header text-center" ><?php echo "Lawyer name: " . $lawyer->name . " " . $lawyer->lname ; ?>	</h3>
					</div>
					<div class="form-group col-md-3">
						<label>Date from</label>
						<input type="text" name="date_from" parsley-trigger="change" placeholder="Enter Date From" class="form-control datepicker-here" data-date-format="mm-dd-yyyy" data-language="en">
					</div>
					<div class="form-group col-md-3">
						<label>Date to</label>
						<input type="text" name="date_to" parsley-trigger="change" placeholder="Enter Date To" class="form-control datepicker-here" data-date-format="mm-dd-yyyy" data-language="en">
					</div>
					<div class="form-group col-md-3">
						<label>Charges/ Payments</label>
						<select placeholder="Charges/Payments" class="form-control" name="charges">
							<option value="0"> Select type </option>
							<option value="1"> Charges </option>
							<option value="2"> Payments </option>
							<option value="3"> Payments and charges </option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>&nbsp;</label>
						<button class="form-control btn btn-success search_transactions">search</button>
					</div>
					<div class="col-md-12">
						<h4 style="text-align:center;">Lawyer Payment Transactions </h4>
					</div>
					<div class="porlets-content">
						<div id="all_lawyers_table">
							<div class="table-responsive transactions">
								<?php $this->load->view('admin/users/transactions/transactions'); ?>
							</div>
							<!--/table-responsive-->   
							<div class="clearfix"></div>
						</div>
					</div>
					<!--/porlets-content-->
					<div class="porlets-content">
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->               
	</div>
	<!--/page-content end--> 
</div>
<script>
	function load_dataTable()
	{
		$('#dynamic-table').dataTable({
			"ordering": true,
			"iDisplayLength": 100
			, "aaSorting": [[4, "desc"]]
		});
	}
	$(".search_transactions").click(function(e) 
	{
		$(".page_loader").show();
		$.ajax({
			type: "POST",
			url: '<?php echo base_url();?>admin/reporting/search_transactions',
			data: {
				date_from: $("[name=date_from]").val(),
				date_to: $("[name=date_to]").val(),
				charges: $("[name=charges]").val(),
				lawyer_id: '<?php echo $id; ?>'
			},
			success: function(response) {
				$(".page_loader").hide();
				data = jQuery.parseJSON(response);
				$(".transactions").html(data.html);
				load_dataTable();
			}
		});
		e.preventDefault();
	});
</script>