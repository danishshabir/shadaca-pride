<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Text Notification</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Text Notification</h3>
						<?php 
							if($this->session->flashdata('message')){	
								echo $this->session->flashdata('message');
							}		
							?>
					</div>
					<div class="porlets-content">
						<form action="<?php echo current_url();?>" method="post" enctype="multipart/form-data">
							<div class="form-group col-md-4">
								<label>Phone Number *</label>
								<input type="text" name="phone" placeholder="Enter phone number" class="form-control" value="<?php echo ($this->input->post() ? $this->input->post('phone') : $user->phone); ?>">
								<?php echo (form_error('phone') ? form_error('phone') : ''); ?>
							</div>
							<!--/form-group--> 
							<div class="form-group col-md-4">
								<label>Activate</label>
								<select class="form-control" name="is_sms">
									<option <?php if($user->is_sms == 0){?> selected="selected" <?php }?> value="0">Inactive</option>
									<option <?php if($user->is_sms == 1){?> selected="selected" <?php }?> value="1">Active</option>
								</select>
								<?php echo (form_error('is_sms') ? form_error('is_sms') : ''); ?>
							</div>  
							<div class="form-group col-md-12">
								<button class="btn btn-primary" type="submit">Update</button>
								<a class="btn btn-default" href="<?php echo base_url(); ?>admin/users/admin">Cancel</a>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<script>
	$("[name='is_sms']").val('<?php echo ($this->input->post('is_sms') ? $this->input->post('is_sms') : $user->is_sms) ?>');
});
</script>