<div id="main-content">

    <div class="page-content">

      <div class="row">

        <div class="col-md-12">

          <h2>Add New <?php echo $type; ?></h2>

        </div><!--/col-md-12--> 

      </div><!--/row-->

      

      

      

      <div class="row">

        <div class="col-md-12">

          <div class="block-web">

            <div class="header">

              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>

              <h3 class="content-header"><?php echo $type; ?> Form</h3>

            </div>

            <div class="porlets-content">

              <form action="<?php echo base_url();?>admin/users/save/<?php echo $role;?>" method="post" parsley-validate novalidate>

                <div class="form-group">

                  <label>First Name</label>

                  <input type="text" name="name" parsley-trigger="change" required placeholder="Enter name" class="form-control">

                </div><!--/form-group-->

                

                 <div class="form-group">

                  <label>Email address</label>

                  <input type="email" name="email" parsley-trigger="change" required placeholder="Enter email" class="form-control">

                </div><!--/form-group-->

                

                 <div class="form-group">

                  <label>Password</label>

                  <input type="password" name="password" parsley-trigger="change" required placeholder="Enter password" class="form-control">

                </div><!--/form-group-->

                

                 <div class="form-group">

                  <label>Mobile</label>

                  <input type="text" name="phone" parsley-trigger="change" required placeholder="Enter mobile number" class="form-control">

                </div><!--/form-group-->

                

                <div class="form-group">

                  <label>City</label>

                  <input type="text" name="city" parsley-trigger="change" required placeholder="Enter city name" class="form-control">

                </div><!--/form-group-->

                <div class="form-group">

                  <label>State</label>

                  <input type="text" name="state" parsley-trigger="change" required placeholder="Enter state" class="form-control">

                </div><!--/form-group-->

                <div class="form-group">

                  <label>Zip</label>

                  <input type="text" name="zipcode" parsley-trigger="change" required placeholder="Enter source here" class="form-control">

                </div><!--/form-group-->

                <?php if($role == 2){?>

                <div class="form-group">

                  <label>Case Type</label><br />

						<?php foreach($case_type as $c_type){?>
                        	<label class="checkbox-inline">

                              <input type="checkbox" value="<?php echo $c_type->type;?>" name="case_type[]" id="inlinecheckbox1">
        
                              <span class="custom-checkbox"></span> <?php echo $c_type->type;?> </label>
						<?php }?>

                </div>

                <div class="form-group">

                  <label>Jurisdiction</label>

                  <input type="text" name="jurisdiction" parsley-trigger="change" required placeholder="Enter Jurisdiction here" class="form-control">

                </div>

                

                <?php } ?>

                <div class="form-group">

                  <label>Address</label>

                    <textarea class="form-control" name="address"></textarea>

                </div>

               

               	<div class="form-group">

                  <label>Activate</label>

                    <select class="form-control" name="active">

                    	<option value="0">Inactive</option>

                        <option value="1">Active</option>

                    </select>

                </div>

             

              

                <button class="btn btn-primary" type="submit">Submit</button>

                <button class="btn btn-default">Cancel</button>

              </form>

            </div><!--/porlets-content-->

          </div><!--/block-web--> 

        </div><!--/col-md-6-->

        

         

      </div><!--/row-->

      

     

      

    </div><!--/page-content end--> 

  </div>