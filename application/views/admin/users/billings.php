<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Law Firm Billing</h2>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Law Firm Billing</h3>
					</div>
					<div class="porlets-content">
						<div class="table-responsive">
							<?php echo $this->session->flashdata('message')?>
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
										<th>Law Firm</th>
										<th>Contact</th>
										<th>Phone</th>
										<th>Email</th>
										<th width="10%">Balance</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$total_balance;
									foreach($law_firms as $type) { 
										$balance = $type->total_lead_price+$type->monthly_payment+$type->total_charge_bill-$type->paidTransactions;
										$total_balance = $total_balance + $balance;
										?>
										<tr class="gradeX">
											<td><a href="<?php echo base_url().'admin/dashboard/LawyerBilling/'.$type->id; ?>"><?php echo $type->law_firm; ?></a></td>
											<td><?php echo $type->contact_name.' '.$type->contact_last_name; ?></td>
											<td><?php echo $type->phone; ?></td>
											<td><?php echo $type->contact_email; ?></td>
											<td align="right"><?php echo "$" . number_format(($balance), 2); ?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
					<h3 class="content-header">Total Balance: <?php echo "$" . number_format(($total_balance), 2); ?></h3>
				</div>
				<!--/block-web-->
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end-->
</div>
<!--/main-content end-->
</div>