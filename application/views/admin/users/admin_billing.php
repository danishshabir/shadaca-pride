<div id="main-content">
	<div class="page-content">
		<span id="payment_form"></span>
		<div class="row">
			<div class="col-md-12">
				<h2><?php echo $law_firm->law_firm; ?></h2>
				<h4>Lawyers Billing</h4>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<?php //$balance = $this->Users_model->lawFirmPayment($law_firm->id); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<!--<h3 class="content-header text-center" style="font-weight:700;"><?php echo "Law Firm Name: " . $law_firm->law_firm; ?></h3>
						<h3 class="content-header text-center" style="font-weight:700;"><?php echo "Balance: $" . number_format(($balance['total_lead_price']+$balance['monthly_payment']+$balance['total_charge_bill']-$balance['paidTransactions']), 2); ?> </h3>-->
					</div>
					<?php if($this->session->flashdata('message')){ echo '<span id="flash_message">' . $this->session->flashdata('message') . '</span>';} ?>
					<div class="clearfix"></div>
					<span style="margin-top:20px; padding-top:20px;"></span>
					<?php if($this->session->userdata('role') != 6) { ?>
						<div class="form-group col-sm-6" style="margin-top:10px;">
							<label>Search Name</label>
							<input type="text" name="name" id="name" parsley-trigger="change" placeholder="Enter name" class="form-control searchLawyers">
						</div>
						<div class="form-group col-sm-6" style="margin-top:10px;">
							<label>Law Firm</label>
							<select placeholder="Law Type" class="form-control searchLawfirms" name="law_firm">
								<option value="">Select law firm</option>
								<?php foreach($law_firms as $row){?>
									<option id="fee" value="<?php echo $row->id;?>"> <?php echo $row->law_firm;?> </option>
								<?php }?>
							</select>
						</div>
					<?php } ?>
					<div class="clearfix"></div>
					<div id="success_message" class="text-center center_block" style="color:green; font-weight:700; font-size:20px; display:none;"> Success </div>
					<div id="error_message" class="text-center center_block" style="display:none;"> Warning </div>
					<div class="form-group col-md-12"></div>
					<div class="porlets-content">
						<span id="div_loader" style="display:none;" > <img width="250" src="<?php echo base_url().'assets/admin/images/loading.gif';?>">  </span>
						<span id="show_table">  </span>
						<div id="lawyers_table">
							<?php $this->load->view('admin/users/billings/lawyers_billing'); ?>
						</div>
					</div>
					<!--/porlets-content-->
					<div class="porlets-content">
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web-->
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end-->
</div>
<script>
	//setup before functions
	var typingTimer;                //timer identifier
	var doneTypingInterval = 700;  //time in ms, 5 second for example

	//on keyup, start the countdown
	$('.searchLawyers').on('keyup', function () {
		clearTimeout(typingTimer);
		typingTimer = setTimeout(searchByName, doneTypingInterval);
	});

	//on keydown, clear the countdown
	$('.searchLawyers').on('keydown', function () {
		clearTimeout(typingTimer);
	});


	function searchByName()
	{
		var name = $('#name').val();
		var law_firm = ($('[name=law_firm] option:selected').val() ? $('[name=law_firm] option:selected').val() : '<?php echo $law_firm->id; ?>');
		var url = '<?php echo base_url();?>admin/reporting/SearchLawyer';
		$(".page_loader").show();
		$.ajax({
			type: "POST"
			, url: url
			, data: {
				name: name
				, law_firm: law_firm
			}
			, success: function (response) {
				$(".page_loader").hide();
				var json = jQuery.parseJSON(response);
				$("#lawyers_table").html(json.html);
				$('#dynamic-table').dataTable( { "ordering": true,
					"iDisplayLength": 100 } );
			}
		});
	}


$(".searchLawfirms")
	.change(function (e) {
		e.preventDefault();
		var name = $('#name').val();
		var law_firm = $('[name=law_firm]').val();
		if(law_firm) {
			var url = '<?php echo base_url();?>admin/reporting/SearchLawyer';
			$(".page_loader").show();
			$.ajax({
				type: "POST"
				, url: url
				, data: {
					name: name
					, law_firm: law_firm
				}
				, success: function (response) {
					$(".page_loader").hide();
					var json = jQuery.parseJSON(response);
					$("#lawyers_table").html(json.html);
					$('#dynamic-table').dataTable( { "ordering": true,
						"iDisplayLength": 100 } );
				}
			});
		}
	});
$(document).on("click", ".addBatchProcessing", function (e) {
	e.preventDefault();
	if ($('input[name="lawyers_ids[]"]:checked')
		.length > 0) {
		$("#addBatchProcessing")
			.submit();
	} else {
		$("html, body")
			.animate({
				scrollTop: 0
			}, "slow");
		$("#error_message")
			.show()
			.html('Please select first any lawyer!');
		$('#error_message')
			.delay(5000)
			.fadeOut('slow');
	}
});
</script>