<?php $session_id=$this->session->userdata('id'); ?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
        	<div class="col-md-12">
        		<?php if($this->session->role == 7){ ?>
        			<h2>Lawyer Staff Profile</h2>
        		<?php }else{ ?>
        			<h2>Lawyer Profile</h2>
        		<?php } ?>
        	</div>
        	<!--/col-md-12--> 
        </div>
        <!--/row-->
        <div class="row">
        	<div class="col-md-12">
        		<div class="block-web">
        			<div class="header">
        				<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
        				<h3 class="content-header">Lawyer Profile</h3>
        				<span class="note-text">Contact <a href="mailto:support@pridelegal.com">support@pridelegal.com</a> to update your account profile information.</span>
        			</div>
        			<?php if($this->session->flashdata('message')){
        				echo $this->session->flashdata('message');
        			} ?>
        			<form action="<?php echo base_url();?>admin/users/reset_password/<?php echo $session_id;?>" method="post" parsley-validate novalidate>
        				<input type="hidden" name="user_id" value="<?php echo $session_id;?>">
        				<div class="form-group col-md-4">
        					<label>First name</label>
        					<input type="text" name="name" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $user_information->name; ?>" disabled="disabled">
        				</div>
        				<!--/form-group-->
        				<div class="form-group col-md-4">
        					<label>Last name</label>
        					<input type="text" name="lname" parsley-trigger="change" required placeholder="Enter last name" class="form-control" value="<?php echo $user_information->lname; ?>" disabled="disabled">
        				</div>
        				<!--/form-group-->
        				<div class="form-group col-md-4">
        					<label>Email Address</label>
        					<input type="text" name="email" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $user_information->email; ?>" disabled="disabled">
        				</div>
        				<!--/form-group-->
        				<div class="clearfix"></div>
        				<div class="form-group col-md-4">
        					<label>Mobile</label>
        					<input type="text" name="phone" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $user_information->phone; ?>" disabled="disabled">
        				</div>
        				<!--/form-group-->
        				<div class="form-group col-md-4">
        					<label>Law firms</label>
        					<input type="text" name="law_firms" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo getLawsNames($user_information->law_firms); ?>" disabled="disabled">
        				</div>
        				<!--/form-group-->
                        <?php if($this->session->role != '7'){ ?>
        				<div class="form-group col-md-4">
        					<label>Website</label>
        					<input type="text" name="website" parsley-trigger="change" required placeholder="Enter website" class="form-control" value="<?php echo $user_information->website; ?>" disabled="disabled">
        				</div>
        				<!--/form-group-->
        				<div class="form-group col-md-4">
        					<label>Address</label>
        					<input type="text" name="address" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $user_information->address; ?>" disabled="disabled">
        				</div>
        				<!--/form-group-->
        				<div class="form-group col-md-4">
        					<label>City</label>
        					<input type="text" name="city" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $user_information->city; ?>" disabled="disabled">
        				</div>
        				<div class="form-group col-md-2">
        					<label>State</label>
        					<input type="text" name="city" parsley-trigger="change" required placeholder="Enter state name" class="form-control" value="<?php echo getStateShortName($user_information->state); ?>" disabled="disabled">
        				</div>
        				<!--/form-group-->
        				<div class="form-group col-md-2">
        					<label>Zip</label>
        					<input type="text" name="city" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $user_information->zipcode; ?>" disabled="disabled">
        				</div>
                        <?php }else{ ?>
        					<div class="form-group col-md-4">
        						<label>Lawyer</label>
        						<input type="text" name="lawyer" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo ($curr_lawyer == 'All' ? 'All' : $curr_lawyer->name . ' ' . $curr_lawyer->lname)?>" disabled="disabled">
        					</div>
        				<?php } ?>
        				<!--/form-group-->
        				<div class="form-group col-md-4">
        					<label>Created at</label>
        					<input type="text" name="created_at" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $newDate = date("m-d-Y h:i:s", strtotime($user_information->created_at));?>" disabled="disabled">
        				</div>
        				<!--/form-group-->
                        <?php if($this->session->role != '7'){ ?>
        				<div class="form-group col-md-4">
        					<div class="form-group">
        						<label>Fee Type</label>
        						<select placeholder="Fee Type" class="form-control" name="fee_type" disabled="disabled">
        							<option value="" > Select Type </option>
        							<option value="0"> Monthly </option>
        							<option value="1"> Annual </option>
        						</select>
        					</div>
        				</div>
        				<div class="form-group col-md-4">
        					<label>Fee</label>
        					<input type="text" id="sub_cat"  name="law_firm_fee"   placeholder="Fee" value="<?php echo number_format($user_information->law_firm_fee, 2); ?>" class="form-control" disabled="disabled">
        				</div>
        				<div class="form-group col-md-12">
        					<label>Case type</label>
        					<textarea class="form-control" disabled="disabled" rows="6"> <?php echo str_replace(",", ", ", $user_information->case_type); ?></textarea>
        				</div>
        				<!--/form-group-->
        				<div class="form-group col-md-12">
        					<label>Jurisdiction</label>
        					<textarea type="text" class="form-control" disabled="disabled" rows="6"><?php echo str_replace(",", ", ", $user_information->jurisdiction); ?></textarea>
        				</div>
                        <?php } ?>
        				<!--/form-group-->
        				<!--<div class="form-group col-sm-4">
        					<label>Profile image</label><br>
        					<?php if(file_exists('./uploads/'.$user_information->image)) { ?>
        						<a href="#"> <img alt="user" src="<?php echo base_url();?>uploads/<?php echo $user_information->image;?>" style="height:200px; width:200px;"> </a>
        					<?php } else { ?>
        						<a href="#"> <img src="<?php echo base_url();?>assets/admin/images/avatar1.jpg" style="height:120px; width:185px;"> </a>
        					<?php } ?>
        				</div>-->
        				<!--/form-group-->
        			</form>
                    <div class="clearfix"></div>
                </div>
                <!--/block-web end-->
            </div>
            <!--/col-md-12 end-->
        </div>
        <!--/row end-->
        <br />
        <div class="row">
        	<div class="col-md-12">
        		<div class="block-web">
        			<div class="header">
        				<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
        				<h3 class="content-header"><?php echo $user_information->name . ' ' . $user_information->lname . ' Lawyer Staff' ?></h3>
        			</div>
                    <div class="porlets-content">
						<div class="table-responsive">
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
										<th>First name</th>
										<th>Last name</th>
										<th>Email</th>
                                        <th>Email On</th>
									</tr>
								</thead>
								<tbody>
                                    <?php //echo '<pre>'; print_r($lawstaff); exit(); ?>
									<?php $i = 0; foreach($lawstaff as $staff){?>
									<tr>
										<td><?php echo $staff['name']; ?></td>
										<td><?php echo $staff['lname']; ?></td>
										<td><?php echo $staff['email']; ?></td>
                                        <td><input type="checkbox" class="change_send_email" attr-user-id="<?php echo $staff['lawyerstaff_id']; ?>" value="1" style="opacity: 1;" <?php echo ( $staff['send_email_user'] == 1 ? 'checked=""' : '' ); ?> /></td>
                                        <!-- 7 define role which is specify it is lawyer staff -->
									</tr>
									<?php $i++; } ?> 
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
                    <div class="clearfix"></div>
                </div>
                <!--/block-web end-->
            </div>
            <!--/col-md-12 end-->
            <!--
            <div class="col-md-12">
        		<div class="block-web">
                    <div class="porlets-content">
						<form action="<?php echo base_url(); ?>admin/users/send_email_to_staff" method="post">
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label>Send Email To Lawyer Staff:</label>
                                    <select name="send_email" class="form-control">
                                        <option value="1" <?php echo ($user_information->send_email == 1 ? 'selected=""' : '' )  ?>>On</option>
                                        <option value="0" <?php echo ($user_information->send_email == 0 ? 'selected=""' : '' )  ?>>Off</option>
                                    </select>
                                </div>
                            </div>
                            <input value="Submit" class="btn btn-primary" type="submit" />
                        </form>
					</div>
					<!--/porlets-content-->
                    <!--
                    <div class="clearfix"></div>
                </div>
                <!--/block-web end-->
                <!--
            </div>
            <!--/col-md-12 end-->
        </div>
        <!--/row end-->
    </div>
    <!--/page-content end--> 
</div>
<!--/main-content end-->
<script>
	$("[name='fee_type']").val('<?php echo $user_information->fee_type; ?>');
</script>