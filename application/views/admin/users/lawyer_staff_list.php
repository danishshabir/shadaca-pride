<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Lawyer Staff View</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header" style="margin-bottom:40px;">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
					</div>
					<div class="form-group col-md-4">
						<label>First name</label>
						<input type="text" name="name" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->name; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Last name</label>
						<input type="text" name="lname" parsley-trigger="change" required placeholder="Enter last name" class="form-control" value="<?php echo $lawyer->lname; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Email</label>
						<input type="text" name="email" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->email; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="clearfix"></div>
					<div class="form-group col-md-4">
						<label>Phone</label>
						<input type="text" name="phone" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->phone; ?>" disabled="disabled">
					</div>
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Law firms</label>
						<input type="text" name="law_firms" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lawyer->law_firm_name; ?>" disabled="disabled">
					</div>
					<div class="form-group col-md-4">
						<label>Lawyer</label>
						<input type="text" name="law_firms" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo ($curr_lawyer == 'All' ? 'All' : $curr_lawyer->name . ' ' . $curr_lawyer->lname)?>" disabled="disabled">
					</div>
					<div class="form-group col-md-4">
						<label>Created at</label>
						<input type="text" name="created_at" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $newDate = date("m-d-Y h:i:s", strtotime($lawyer->created_at));				   ?>" disabled="disabled">
					</div>
					
					<!--/form-group-->
					<div class="form-group col-md-4">
						<label>Last login date </label>
						<input type="text"  value="<?php echo date("m-d-Y", strtotime($lawyer->last_login_date));?>" class="form-control" disabled="disabled">
					</div>
					<div class="form-group col-md-4">
						<label>Last Ip address</label>
						<input type="text" value="<?php echo $lawyer->ip_address;?>" class="form-control" disabled="disabled">					
					</div>					
					<div class="clearfix"></div>
					<!--/form-group-->
					<div class="clearfix"></div>
				</div>
				<!--/porlets-content-->
			</div>
			<!--/block-web--> 
		</div>
		<!--/col-md-6-->
	</div>
	<!--/row-->
</div>
<!--/page-content end--> 
</div>