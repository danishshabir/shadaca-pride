<?php $session_id=$this->session->userdata('id'); ?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Payment</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Payment</h3>
					</div>
					<div class="porlets-content">
						<?php $balance = getLawyerTotalBalance($lawyer->id);
						if($lawyer->id) { ?>
							<h3 class="content-header text-center"><?php echo   "Lawyer name: " . $lawyer->name . " " . $lawyer->lname ; ?></h3><br>
							<h3 class="content-header text-center"><?php echo ($balance ? 'balance: $'.$balance : '') ?></h3>
						<?php } ?>
					</div>
					<?php if($this->session->flashdata('message')){	echo '<span id="flash_message">' . $this->session->flashdata('message') . '</span>';} ?>  
					<?php if($lawyer->id) { ?>
						<div class="row">
							<span id="success_message"></span>
							<div class="tab-content">
								<div id="existingCard" class="tab-pane fade in active">
									<!-- full bill charge form -->
									<form action="<?php echo base_url();?>admin/transaction/chargeCustomerProfile/<?php //echo $lawyer->id;?>" method="post" parsley-validate novalidate>
										<div class="row">
											<div class="form-group col-sm-4">
												<label>Customer Profile</label>
												<select placeholder="Card type" class="form-control" name="customer_profile" required>
													<option value=""> Select Card</option>
														<?php foreach($customer_profiles as $row) {
															$response = $this->functions->getCustomerProfile($row->customer_profile_id,$row->profile_id); 
															if($response['customerPaymentProfileId']) { ?>
																<option value="<?php echo $row->id;?>"><?php echo chunk_split($response['card_number'], 4, ' '); ?></option>
															<?php }
														} ?>
												</select>
											</div>
											<div class="form-group col-sm-4">
												<label>Amount</label>
												<input type="text" name="amount" parsley-trigger="change" required placeholder="Enter amount" class="form-control">
											</div>
										</div>
										<input type="hidden" name="lawfirm_id" value="<?php echo $lawfirm_id;?>" parsley-trigger="change"  class="form-control">
										<input type="hidden" name="lawyer_id" value="<?php echo $lawyer->id;?>" parsley-trigger="change"  class="form-control">
										<div class="clearfix"></div>
										<br>
										<div class="form-group">
											<a href="<?php echo base_url();?>admin/dashboard/billings" class="btn btn-default">Back</a>
											<button class="btn btn-primary" type="submit">Pay now</button>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					<?php } ?>
				</div>
				<!--/porlets-content-->
				<div class="clearfix"></div>
			</div>
			<!--/block-web--> 
		</div>
		<!--/col-md-6-->
	</div>
	<!--/row-->
</div>
<!--/page-content end--> 
</div>