<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Survey List</h2>
        </div><!--/col-md-12--> 
      
      </div><!--/row-->
  <div class="block-web">
            <div class="header">
              
              <h3 class="content-header">Survey List</h3>
            </div>    
      
     
           <table class="display table table-bordered table-striped" id="dynamic-table">
                <thead>
                
                <th>User</th>
                <th>View</th>
                </thead>
                <tr>
                <?php
                foreach ($users as $usr) 
                {?>
                <td><?php echo $usr['name'];?></td>
                <td><a href="<?php echo base_url();?>admin/settings/view_survey/<?php echo $usr['id'];?>">View</a></td>
                </tr>
                <?php 
                }
                 ?>

           </table>
          </div><!--/block-web--> 
     
          
    
      
    


    </div><!--/page-content end--> 
  </div>

  <script type="text/javascript">

$(document).ready(function()
{

$("#checkbox_id").hide();
$("#radio_id").hide();
});

$("#question_type").change(function () 
{
 
     var type=$("#question_type").val();
     if(type=="checkbox")
     {
      $("#checkbox_id").show(1000);
     }
     else
     {
      $("#checkbox_id").hide();
     }
if(type=="radio")
     {
      $("#radio_id").show(1000);
     }
     else
     {
      $("#radio_id").hide();
     }
 
});

$("#whole-form").click(function()
{
        
     $(".whole-form-body").clone().appendTo(".whole-form-body-append");
});



$('.multi-field-wrapper').each(function() 
{
    var $wrapper = $('.multi-fields', this);

    $("#add-field", $(this)).click(function(e) 
    {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
    });

    $('.multi-field #remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });



 
});
//for whole form








  </script>