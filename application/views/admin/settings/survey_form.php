<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Answer the Survey</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              
              <h3 class="content-header">Survey Form</h3>
            </div>
			<?php 
			
			if($this->session->flashdata('message')){	
			echo $this->session->flashdata('message');
			}
			
			?>


  

            <div class="porlets-content">
              <form action="<?php echo base_url();?>admin/settings/addsurvey_form" method="post" parsley-validate novalidate>
               
                

<?php

$count_radio=0;
$survey_check=0;
                foreach ($question_data as $question)
                 {
               // echo $question['question_title']."<br>";
               
                    
                    if($question['question_type']=="text")
                    {
                      //echo "text";

                      $check_answered=$this->Survey_model->check_answered($question['question_id'],$user_id);

                        $check_=false;
                       

                        ?>

                      <div class="form-group">
                      <label><?php echo $question['question_title'] ; ?></label>
                      <textarea name="question_text[]" parsley-trigger="change" required  class="form-control">

                      </textarea>
                     
                        
                      </div><!--/form-group-->

                         <?php  
                    }

//Check boxex

                    if($question['question_type']=="checkbox")
                    {

                      $options=$this->Survey_model->get_options_answer($question['question_id']);
                    $check_answered=$this->Survey_model->check_answered($question['question_id'],$user_id);

                ?>       
              
                
              
                  <div class="form-group">
                  

				  <label><?php echo  $question['question_title'] ; ?></label>
					
                  <?php 
				  
                    foreach ($options as $optn) 
                    {
                     $o=$optn['id'];
                     $o_option=$optn['option_answer']
                    ?>
                      
					  <label class="checkbox" style="display:none;">
						<input type="checkbox"  name="options_checkbox_array[]" required>
						<span class="custom-checkbox"></span> 
                    </label>
					  
                    <label class="checkbox">
                      <input type="checkbox" value="<?php echo $o;?>"  name="options_checkbox_array[]">
                      <span class="custom-checkbox"></span> 
                      <?php echo $o_option; ?> 
                    </label>

                       

                  <?php 
                    } ?>
					</div>                 

                      <?php 
                    
                    }


              if($question['question_type']=="radio")
                    {

                      $options=$this->Survey_model->get_options_answer($question['question_id']);
                    $check_answered=$this->Survey_model->check_answered($question['question_id'],$user_id);

                      

                   
              ?>
                  <div class="form-group">
                    <label><?php echo $question['question_title'] ; ?></label>

                  <?php 

                    foreach ($options as $optn) 
                    {
                     $o=$optn['id'];
                     $o_option=$optn['option_answer']
                    ?>
                      
					   <label class="radio" style="display:none;">
                      <input type="radio" name="radio_option[<?php echo $count_radio; ?>]"  style="display:none;" required>
                    </label>

					  
					  
                    <label class="radio">
                      <input type="radio" value="<?php echo $o_option;?>"  name="radio_option[<?php echo $count_radio; ?>]">
                      <span class="custom-radio"></span> 
                      <?php echo $o_option; ?> 
                    </label>

                       

                  <?php 
                    } 
                    ?> 



                	</div><!--/form-group-->

                <?php 
              
              $count_radio=$count_radio+1;
              } 

                  ?> 
                      
                    

     <input type="hidden" name="question_id[]" value="<?php echo $question['question_id'];?>">
     <input type="hidden" name="question_type[]" value="<?php echo $question['question_type'];?>">
               
                    <?php

                    }
       
 ?>
                
                 
                

               
          
              

                <button class="btn btn-primary" type="submit">Submit</button>
                <button class="btn btn-default">Cancel</button>
            

              </form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>