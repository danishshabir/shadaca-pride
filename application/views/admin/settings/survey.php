<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Add New Question</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              
              <h3 class="content-header">Question Form</h3>
            </div>
           <!-- <button class="btn btn-success" id="whole-form"><span class="glyphicon glyphicon-plus"></span>Add</button>
            !-->
            <div class="porlets-content">
              
              <form action="<?php echo base_url();?>admin/settings/save_survey" method="post" parsley-validate novalidate>
                
                <div class="whole-form-body-append">

                  <div class="whole-form-body">
					
				<div class="col-md-6">
					<div class="form-group">
					  <label>Question title *</label>
					  <input type="text" name="question_title" parsley-trigger="change" required placeholder="Enter question title" class="form-control">
					</div><!--/form-group-->
                </div>
				
				
			<div class="col-md-6">	
                <div class="form-group">
                  <label>Question type</label>
                 
                    <select placeholder="Case Type" class="form-control" name="question_type" id="question_type">
                      <option value="text"> Text </option>
                      <option value="checkbox"> Checkbox </option>
                      <option value="radio"> Radio </option>
                      
                    </select>
                 
                </div>
            </div>

			<div class="clearfix"></div>
               <!-- <div id="radio_id_old">
<input name="radio1" type="text" placeholder="First Option">
<input name="radio2" type="text" placeholder="Second Option">
                </div>!-->

                  <div id="radio_id">
                      <div class="multi-field-wrapper-radio">
                        <div class="multi-fields-radio">
                          <div class="multi-field-radio">
                            <input type="text" name="option_answer_radio[]">


                           
                            <button type="button" class="btn btn-danger" id="remove-field-radio"><span class="glyphicon glyphicon-remove"></span></button>
                          </div>
                        </div>
                      <button type="button" style="margin-top:10px;" class="btn btn-success" id="add-field-radio" ><span class="glyphicon glyphicon-plus"></span>Add</button>
                    </div>
					<br><br>
                  </div>







                 




                  <div id="checkbox_id">
                      <div class="multi-field-wrapper">
                        <div class="multi-fields">
                          <div class="multi-field">
                            <input type="text" name="option_answer[]">


                           
                            <button type="button" class="btn btn-danger" id="remove-field"><span class="glyphicon glyphicon-remove"></span></button>
                          </div>
                        </div>
                      <button type="button" style="margin-top:10px;" class="btn btn-success" id="add-field" ><span class="glyphicon glyphicon-plus"></span>Add</button>
                    </div>
                  </div>

                </div>


              </div>
               
                <br>
				
			<div class="col-md-12">	
                <button class="btn btn-primary" type="submit">Submit</button>
             </div> 
					
			<div class="clearfix"></div>
			  </form>
           
            </div><!--/porlets-content-->
			<?php 
			
			if($this->session->flashdata('message')){	
				echo $this->session->flashdata('message');
			}
					
					 //chechUserAccess();
					
			?>
			
			
            <br>
           <h3>Questions List</h3>

           <table class="display table table-bordered table-striped" id="dynamic-table">
<thead>
<th>Question Title</th>
<th>Type</th>
<th>Action</th>
</thead>
<tr>
<?php
foreach ($questions as $q) 
{
?>
<td><?php echo $q['question_title']; ?></td>
<td><?php echo $q['question_type']; ?></td>
<td style="cursor:pointer"><a data-toggle="modal" data-target="#myModal<?php echo $q['question_id'];?>">Delete </a></td>


  
					   <div class="modal fade" id="myModal<?php echo $q['question_id'];?>" role="dialog">
						<div class="modal-dialog modal-sm">
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							  <h4 class="modal-title">Conform Delete</h4>
							</div>
							<div class="modal-body">
							  <p>Are You Sure You want to Delete This ??</p>
							</div>
							<div class="modal-footer">
							  <a class="btn btn-primary" <a href="<?php echo base_url();?>admin/settings/deleteSurvey/<?php echo $q['question_id'];?>" >Yes </a>
							  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
							</div>
						  </div>
						</div>
					  </div>


</tr>
<?php 
}
 ?>

           </table>
          </div><!--/block-web--> 

        </div><!--/col-md-6-->
        
          
    
      </div><!--/row-->
      
    


    </div><!--/page-content end--> 
  </div>

  <script type="text/javascript">

$(document).ready(function()
{

$("#checkbox_id").hide();
$("#radio_id").hide();
});

$("#question_type").change(function () 
{
 
     var type=$("#question_type").val();
     if(type=="checkbox")
     {
      $("#checkbox_id").show(1000);
     }
     else
     {
      $("#checkbox_id").hide();
     }
if(type=="radio")
     {
      $("#radio_id").show(1000);
     }
     else
     {
      $("#radio_id").hide();
     }
 
});

$("#whole-form").click(function()
{
        
     $(".whole-form-body").clone().appendTo(".whole-form-body-append");
});

//Add multiple inputs for checkbox

$('.multi-field-wrapper').each(function() 
{
    var $wrapper = $('.multi-fields', this);

    $("#add-field", $(this)).click(function(e) 
    {
        $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
   
    });

    $('.multi-field #remove-field', $wrapper).click(function() {
        if ($('.multi-field', $wrapper).length > 1)
            $(this).parent('.multi-field').remove();
    });



 
});
//Add multiple inputs for radio

$('.multi-field-wrapper-radio').each(function() 
{
    var $wrapper = $('.multi-fields-radio', this);

    $("#add-field-radio", $(this)).click(function(e) 
    {
        $('.multi-field-radio:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
   
    });

    $('.multi-field-radio #remove-field-radio', $wrapper).click(function() {
        if ($('.multi-field-radio', $wrapper).length > 2)
            $(this).parent('.multi-field-radio').remove();
    });



 
});







  </script>