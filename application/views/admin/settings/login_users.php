<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Last Logins</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Last Logins</h3>
					</div>
					<div class="porlets-content">
						<div class="table-responsive">
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="has_datatable">
								<thead>
									<tr>
										<th>Last Login Date</th>
										<th>Ip Address</th>
										<th>Type</th>
										<th>Username</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($login_users as $row){?>
										<tr class="gradeX">
											<td><?php echo date("m-d-Y H:i:s", strtotime($row->last_login_date)); ?></td>
											<td><?php echo $row->ip_address; ?></td>
											<td><?php echo $row->account_type; ?></td>
											<td><?php echo $row->first_name.' '.$row->last_name; ?></td>
										</tr>
									<?php } ?> 
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<!--/main-content end-->
</div>
<script>
	var sorting = false;
</script>