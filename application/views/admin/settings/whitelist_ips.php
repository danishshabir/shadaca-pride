<div id="main-content">

	<div class="page-content">

		<div class="row">

			<div class="col-md-12">

				<h2>Add IP To Be Whitelist</h2>

			</div>

			<!--/col-md-12--> 

		</div>

		<!--/row-->

		<div class="row">
			
			
			<div class="col-md-12">

				<div class="block-web">

					<div class="header">

						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>

						<h3 class="content-header">Add IP Address</h3>

					</div>

					<?php echo $this->session->flashdata('message')?>

					<div class="porlets-content">
						
						<div class="col-md-2"></div>
						
						<div class="col-md-8">
						
							<form action="<?php echo ( $this->uri->segment(4) == '' ? current_url() : base_url() . 'admin/settings/whitelist_ips/update_ip/' . $edit_ip->id )?>" method="post" class="form-horizontal">

								<div class="form-group">
							  
									<label style="text-align: left;" for="ip_address" class="col-sm-3 control-label"><strong>Enter IP Address:</strong></label>
									
									
									<div class="col-sm-9">
								
										<input type="text" id="ip_address" name="ip_address"  placeholder="Enter IP Address" class="form-control" value="<?php if(isset($edit_ip)){ echo $edit_ip->white_listed_ips; } ?>" required />
										
										<?php echo (form_error('ip_address') ? form_error('ip_address') : ''); ?>
										
										<?php if(isset($edit_ip)){ ?>
										
											<input type="hidden" name="ip_id" value="<?php echo $edit_ip->id; ?>">
											
										<?php } ?>
								  
									</div>
									
									<input style="float: right; margin-top: 5px; margin-right: 15px;" type="submit" class="btn-primary btn btn-sm" value="<?php echo ( $this->uri->segment(4) == '' ? 'Submit' : 'Update' ); ?>" />
								
								</div>
							  
							</form>
						
						</div>
							
						<div class="col-md-2"></div>

						<div class="clearfix"></div>

						<hr/>
							
						<div class="row">
						
							<div class="col-md-2"></div>
							
							<div class="ip-table col-md-8">
							
							<table class="table table-hover table-bordered">
							
								<thead>
							
								<tr>
								
									<th class="col-sm-1">#</th>
									
									<th>Ip Address</th>
									
									<th class="col-sm-2">Actions</th>
									
								</tr>
								
								</thead>
								
								<?php if(count($ips) > 1){ ?>
								
								<?php $i = 1; foreach($ips as $ip){ ?>
								
									<?php if($ip['id'] != 1){ ?>
									
								<tr>
								
									<td><?php echo $i; ?></td>
									
									<td><?php echo $ip['white_listed_ips']; ?></td>
									
									<td><a href="<?php echo base_url(); ?>admin/settings/whitelist_ips/edit_ip/<?php echo $ip['id']; ?>">Edit</a>&nbsp;|&nbsp;<a href="<?php echo base_url(); ?>admin/settings/whitelist_ips/remove_ip/<?php echo $ip['id']; ?>">Delete</a></td>
									
								</tr>
									<?php $i++; } ?>
									
								<?php } ?>
								
								<?php }else{ ?>
								
									<tr>
									
										<td colspan="3" style="text-align: center;">No IPs Found!</td>
										
									</tr>
									
								<?php } ?>
								
							</table>
							
							</div>
							
							<div class="col-md-2"></div>
							
						</div>


					</div>

					<!--/porlets-content-->

				</div>

				<!--/block-web--> 

			</div>

			<!--/col-md-6-->

		</div>

		<!--/row-->

	</div>

	<!--/page-content end--> 

</div>


<script>

//$('[name="ip_address"]').mask('0ZZ.0ZZ.0ZZ.0ZZ', {translation:  {'Z': {pattern: /[0-9]/, optional: true}}});
	$('[name="ip_address"]').mask('099.099.099.099');

</script>