<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Settings</h2>
			</div><!--/col-md-12-->
		</div><!--/row-->



		<div class="row">
			<div class="col-md-12">
				<div class="block-web">

					<div class="header">

						<h3 class="content-header">Settings</h3>
					</div>
           <!-- <button class="btn btn-success" id="whole-form"><span class="glyphicon glyphicon-plus"></span>Add</button>
           	!-->
           	<div class="porlets-content">
           		<?php echo $this->session->flashdata('message');?>
           		<form action="<?php echo base_url();?>admin/settings/save_settings" method="post" parsley-validate novalidate>

           			<div class="whole-form-body-append">

           				<div class="whole-form-body">
           					
           					<div class="row">
           						<div class="col-md-4">
           							<div class="form-group">
           								<label>Follow up hours</label>
           								<input type="number" name="follow_up_hours" parsley-trigger="change" required placeholder="Enter follow up hours" class="form-control" value="<?php echo $settings->follow_up_hours;?>">
           							</div><!--/form-group-->
           						</div>


           						<div class="col-md-4">
           							<div class="form-group">
           								<label>Invoice due date</label>
           								<!--<input type="text" name="invoice_due_date" id="time-holder" parsley-trigger="change" required="" placeholder="Enter invoice due date" class="form-control form_datetime-adv datepicker-here" value="<?php echo date("m-d-Y h:i A", strtotime($settings->invoice_due_date)); ?>" data-date-format="mm-dd-yyyy" data-language="en" data-timepicker="true">-->
           								<input type="text" onkeypress="if(event.keyCode != 13) return /\d+/.test(String.fromCharCode(event.keyCode));" name="invoice_due_date" parsley-trigger="change" required="" placeholder="Enter invoice due date" class="form-control" value="<?php echo $settings->invoice_due_date; ?>">
           							</div><!--/form-group-->
           						</div>


           						<div class="col-md-4">
           							<div class="form-group">
           								<label>Super admin </label>

           								<select placeholder="Super Admin" class="form-control" name="super_admin_id" required>
           									<option value="" required> Select super admin </option>

           									<?php foreach($admins as $admin){?>
           									<option <?php if($settings->super_admin_id == $admin->id) echo 'selected="selected"'; ?>  value="<?php echo $admin->id;?>"> <?php echo $admin->name;?> </option>
           									<?php }?>

           								</select>

           							</div>
           						</div>
           					</div>
           					<div class="row">
           						<div class="col-md-4">
           							<div class="form-group">
           								<label>Secondary super admin </label>

           								<select placeholder="Secondary super admin" class="form-control" name="second_super_admin_id" required>
           									<option value="" required> Select secondary super admin </option>

           									<?php foreach($admins as $admin){?>
           									<option <?php if($settings->second_super_admin_id == $admin->id) echo 'selected="selected"'; ?>  value="<?php echo $admin->id;?>"> <?php echo $admin->name;?> </option>
           									<?php }?>

           								</select>

           							</div>
           						</div>
           					</div>

           					<div class="clearfix"></div>


           					<div class="col-md-12">
           						<div class="form-group">
           							<label>Terms and conditions</label>
           							<div class="compose-editorr" style="margin-top:10px;">
           								<textarea id="text-editor" placeholder="Enter Terms and Conditions" class="col-xs-12" name="terms_conditions" rows="8"  parsley-trigger="change" required><?php echo $settings->terms_conditions;?></textarea>
           							</div>
           						</div>
           					</div>


           					<div class="col-md-12">
           						<div class="form-group">
           							<br/>
           							<label>Global terms and notes</label>
           							<textarea  placeholder="Enter Global Terms and Notes" class="col-xs-12" name="terms_notes_global" rows="4"  parsley-trigger="change"><?php echo $settings->terms_notes_global;?></textarea>
           						</div>
           					</div>



           					<h3><label style="margin-top:20px; margin-left:15px;">Working hours:</label></h3>



           					<div class="col-md-4">
           						<div class="form-group">
           							<label>Working hour from</label>
           							<input type="text" name="work_hour_from" parsley-trigger="change" required placeholder="Enter working hour from" class="form-control" value="<?php echo $settings->work_hour_from;?>">
           						</div><!--/form-group-->
           					</div>

           					<div class="col-md-4">
           						<div class="form-group">
           							<label>Working hour to</label>
           							<input type="text" name="work_hour_to" parsley-trigger="change" required placeholder="Enter working hour to" class="form-control" value="<?php echo $settings->work_hour_to;?>">
           						</div><!--/form-group-->
           					</div>


           					<div class="clearfix"></div>

           					<h3><label style="margin-top:20px; margin-left:15px;">SMS service (active / in-active):</label></h3>

           					<div class="col-md-4">
           						<div class="form-group">
           							<input type="radio" name="sms_active" value="1" id="active_sms" <?php echo ($settings->sms_active == '1' ? 'checked' : ''); ?>><label for="active_sms">Active</label>&nbsp;&nbsp;&nbsp;&nbsp;
           							<input type="radio" name="sms_active" value="0" id="inactive_sms" <?php echo ($settings->sms_active == '0' ? 'checked' : ''); ?>><label for="inactive_sms">In-active</label>

           						</div><!--/form-group-->

           					</div>

           					<div class="clearfix"></div>

           					<h3><label style="margin-top:20px; margin-left:15px;">Invoice statement header payment information:</label></h3>


           					<div class="col-md-4">
           						<div class="form-group">
           							<label>Name *</label>
           							<input type="text" name="h_name" parsley-trigger="change" required placeholder="Enter Name" class="form-control" value="<?php echo $settings->h_name;?>">
           						</div><!--/form-group-->
           					</div>

           					<div class="col-md-4">
           						<div class="form-group">
           							<label>Address *</label>
           							<input type="text" name="h_address" parsley-trigger="change" required placeholder="Enter Address" class="form-control" value="<?php echo $settings->h_address;?>">
           						</div><!--/form-group-->
           					</div>

           					<div class="col-md-4">
           						<div class="form-group">
           							<label>City *</label>
           							<input type="text" name="h_city" parsley-trigger="change" required placeholder="Enter City" class="form-control" value="<?php echo $settings->h_city;?>">
           						</div><!--/form-group-->
           					</div>

           					<div class="form-group col-md-4">
           						<div class="form-group">
           							<label>State *</label>

           							<select placeholder="state" class="form-control" name="h_state" required>
           								<option value="" required> Select state </option>

           								<?php foreach($states_short_names as $state){?>
           								<option value="<?php echo $state->state_short_name;?>" <?php if($state->state_short_name == $settings->h_state){?> selected <?php }?>> <?php echo $state->state_short_name;?> </option>
           								<?php }?>

           							</select>

           						</div>
           					</div>

           					<div class="col-md-4">
           						<div class="form-group">
           							<label>Zip *</label>
           							<input type="text" name="h_zip" parsley-trigger="change" required placeholder="Enter Zip" class="form-control" value="<?php echo $settings->h_zip;?>">
           						</div><!--/form-group-->
           					</div>


           					<div class="col-md-4">
           						<div class="form-group">
           							<label>Phone *</label>
           							<input type="text" name="h_phone" parsley-trigger="change" required placeholder="Enter Phone" class="form-control" value="<?php echo $settings->h_phone;?>">
           						</div><!--/form-group-->
           					</div>
           					<div class="clearfix"></div>


           				</div>


           			</div>


           			<div class="col-md-12">
           				<button class="btn btn-primary" type="submit">Submit</button>
           			</div>

           		</form>

           	</div><!--/porlets-content-->
           	<br>
           </div><!--/block-web-->

       </div><!--/col-md-6-->



   </div><!--/row-->




</div><!--/page-content end-->
</div>
