<?php 
foreach($payments as $payment){
	
	$user_id = $this->session->userdata('id');
	$jur_id = $payment->jurisdiction;	
	
	if($this->session->userdata('role') == '2'){
		$user_id = $this->session->userdata('id');
	}
	else{
	 $user_id = $this->uri->segment(4);
	}
	
	$lead_quantity = getQuantity($user_id,$jur_id); 
	
	$quantity = $lead_quantity;
	$amount = $quantity * $payment->fee;
	$fee += $amount;
	
	}
	$law_firm_quantity = 1;
	$total_membership = $lawyer_lawfirm_detail->law_firm_fee * $law_firm_quantity;
	
	$final_due_date = $fee += $total_membership;
	
	?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Print</title>
    <!--<link media="all" rel="stylesheet" href="css/all.css" type="text/css">-->
	<style>
	
	*{
	max-height:1000000px;
	box-sizing:border-box;
	-webkit-box-sizing:border-box;
}
body {
	margin:0;
  	color:#333;
	font:12px/15px Arial, Helvetica, sans-serif;
	background:#fff;
	min-width:320px;
	-webkit-text-size-adjust: 100%;
	-ms-text-size-adjust: none;
}
img {border-style:none;}
a {
        text-decoration:none;
        color:#000;
}
a:hover {text-decoration:underline;}
a:active {background-color: transparent;}
input,
textarea,
select {
	font:100% Arial, Helvetica, sans-serif;
	vertical-align:middle;
  	color:#000;
}
form, fieldset {
	margin:0;
	padding:0;
	border-style:none;
}
header, footer, article, section, hgroup, nav, figure { display: block; }
.print-block{
	margin:0 auto;
	padding:50px 0;
	overflow:hidden;
	max-width:752px;
}
.print-block h1{
	font-size:22px;
	margin:0 0 30px;
	line-height:26px;
	text-align:right;
	text-transform:uppercase;
}
.print-block .head{
	overflow:hidden;
	padding:0 0 15px;
	border-bottom:1px solid #e5e5e5;
}
.print-block dl{
	width:200px;
	float:right;
	margin:0 0 20px;
}
.print-block dl dt{
	float:left;
	min-width:100px;
	text-align:right;
	margin:0 10px 0 0;
}
.print-block dl dd{
	clear:right;
	margin:0 0 5px;
}
.print-block .price-total{
	clear:both;
	width:160px;
	float:right;
	padding:10px;
	text-align:center;
	border-radius:4px;
	border:1px solid #e5e5e5;
}
.print-block .price-total span,
.print-block .price-total strong{
	display:block;
}
.print-block .price-total strong{
	font-size:18px;
	line-height:22px;
}
.print-block .table-box{
	overflow:hidden;
	padding:15px 0 0;
}
.print-block .table-box h2{
	margin:0 0 10px;
	font-size:16px;
	line-height:20px;
	font-weight:normal;
}
.print-block .table-box .sub-title{
	display:block;
	font-weight:normal;
}
.print-block .table-box header{
	overflow:hidden;
	padding:0 0 30px;
}
.print-block .table{
	border:0;
	width:100%;
	text-align:right;
	margin:0 0 15px;
	table-layout:fixed;
	vertical-align:middle;
	border-collapse:collapse;
}
.print-block .table th,
.print-block .table td{
	padding:0;
	padding:6px 15px;
}
.print-block .table th{
	border-top:1px solid #e5e5e5;
	border-bottom:1px solid #e5e5e5;
}
.print-block .table th:first-child{
	border-left:1px solid #e5e5e5;
}
.print-block .table th:last-child{
	border-right:1px solid #e5e5e5;
}
.print-block .table th:first-child,
.print-block .table td:first-child{
	text-align:left;
}
.print-block .table td{
	border-bottom:1px solid #e5e5e5;
}

.print-block .table table{
	border:0;
	width:292px;
	float:right;
	text-align:right;
	margin:-1px 0 0;
	table-layout:fixed;
	border-collapse:collapse;
}
.print-block .table td.inner{
	border:0;
	padding:0;
}
.print-block .table table td{
	border:1px solid #e5e5e5;
}
.print-block .table table td:first-child{
	text-align:right;
}
.print-block .table-box footer{
	overflow:hidden;
	padding:15px 0 0;
	border-top:1px solid #e5e5e5;
}
.print-block .table-box footer .col{
	width:48%;
	vertical-align:top;
	display:inline-block;
}
.print-block .table-box footer .title{
	display:block;
	font-size:14px;
	margin:0 0 5px;
	line-height:20px;
	font-weight:normal;
}
.print-block .table-box footer p{ margin:0;}
	</style>
</head>
<body>
	<div class="print-block">
    	<h1>Invoice</h1>
        <header class="head">
        	<dl>
            	<dt>Invoice#:</dt>
                <dd>1112-0031</dd>
                <dt>Invoice date:</dt>
                <dd><?php echo date("d/m/Y");?></dd>
                <dt>Terms:</dt>
                <dd><?php echo $InvoiceDetails->terms_conditions;?></dd>
                <dt>Due date</dt>
                
				<dd><?php echo $InvoiceDetails->invoice_due_date . date("/d/Y")?></dd>
            </dl>
            <div class="price-total">
            	<span>Amount due</span>
                <strong><?php echo "$" .$final_due_date . ".00"; ?></strong>
            </div>
        </header>
        <div class="table-box">
        	<header>
            	<h2>Bill To:</h2>
                <strong class="sub-title"><?php echo $lawyer_lawfirm_detail->law_firm?> &amp; <?php echo $lawyer_lawfirm_detail->Address?> </strong>
            </header>
            <table class="table">
            	<tr>
                	<th>Description</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Amount</th>
                </tr>
                <tr>
                	<td>Monthly Membership</td>
                    <td>1</td>
                    <td><?php echo  "$" .$lawyer_lawfirm_detail->law_firm_fee . ".00"?></td>
                    
					<td><?php $amount = $lawyer_lawfirm_detail->law_firm_fee * 1;
					$new_amount = "$" . $amount . ".00";
					echo $new_amount;
					?> </td>
                </tr>
                
				<?php foreach($payments as $payment){?> 
				<tr>
               
			   <td>
			   <?php 
			   $jurisdiction_name =  getJurisdictionName($payment->jurisdiction);
				
				$jurisdiction_state =  getStateName($jurisdiction_name[0]['state']);
				
				echo "Leads -" . $jurisdiction_name[0]['jurisdiction_name'] . "-" . $jurisdiction_state[0]['name']; 
				
				?></td> 
					
					<td>
					<?php
					if($this->session->userdata('role') == '2'){
						$user_id = $this->session->userdata('id');
					}
					else{
						$user_id = $this->uri->segment(4);
					}
					
					$jur_id = $payment->jurisdiction;	
					$lead_quantity = getQuantity($user_id,$jur_id); 
					echo $lead_quantity;
					?></td>
					
                    <td><?php echo "$" . $payment->fee . ".00" ;?></td>
                    <td><?php $lead_Fee = $payment->fee;
					$total_lead_fee = $lead_Fee * $lead_quantity;
					echo  "$" .$total_lead_fee . ".00";
					$total_lead_fee;
					?> </td>
                </tr>
                <?php $total_amount += $total_lead_fee; }
				?>
                
				<tr>
                	<td colspan="4" class="inner">
                    	<table>
                        	<tr>
                            	<td colspan="2">Subtotal</td>
                                <td colspan="2"><?php $sub_total = $total_amount + $amount;
								
								echo "$" . $sub_total;
								?></td>
                            </tr>
                           <tr>
                            	<td colspan="2"><strong>Total</strong></td>
                                <td colspan="2"><strong><?php echo $total_amount + $amount;?> USD</strong></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <footer>
            	<div class="col">
                	<strong class="title">Notes</strong>
                    <p>Add place for notes if desired</p>
                </div>
                <div class="col">
                	<strong class="title">Terms and Conditions </strong>
                    <p>Add place for terms and conditions</p>
                </div>
            </footer>
        </div>
    </div>
</body>
</html>

<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script>
    
    $( window ).load(function() {
		window.print();
        console.log( "window loaded" );
    });
    </script>

 
