<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Edit Jurisdiction Fee</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Edit Jurisdiction Fee Form</h3>
            </div>
            <div class="porlets-content">
             
			 <form action="<?php echo base_url();?>admin/settings/updateJurisdictionFee/<?php echo $id;?>" method="post" onsubmit="return validatePhone();" parsley-validate novalidate>
                
				
<?php 
 /*
 print_r($jurisdiction);
 echo "<br>";
print_r($caseType);
exit;
*/
?>
				 
				<div class="form-group col-md-4" id="lawFirm">
                  <label>Select jurisdiction</label>
                 
                    <select placeholder="jurisdiction" class="form-control" name="jurisdiction" onchange="selectLawFirm(this.value,'admin/Dashboard/getStateId','');">
                      <option value="">Select Jurisdiction</option>
                      <?php 
					 
					  foreach($jurisdiction as $jurisdictions){
						  $stateName =  getStateName($jurisdictions->state);
							$stateName[0]['name'];
						  ?>
					  <option value="<?php echo $jurisdictions->id?>" <?php if($jurisdiction_fee->Jurisdiction == $jurisdictions->id) echo 'selected';?>> <?php echo $jurisdictions->jurisdiction_name . " - ". $stateName[0]['name'];?> </option>
                      <?php }?>
                    </select>                 
                </div>
				
				<div class="form-group col-md-4">
                  <label>Select case type</label>
                 
                    <select placeholder="Case Type" class="form-control" name="Case_type">
                      <option value="">Select case type</option>
                      
					  <?php

					  foreach($caseType as $caseTypes){?>
					  <option value="<?php echo $caseTypes->id?>" <?php if($jurisdiction_fee->case_type == $caseTypes->id) echo 'selected';?>> <?php echo $caseTypes->type;?> </option>
                      <?php }?>
                    </select>                 
                </div>
            

				
                <div class="form-group col-md-4">
				<div class="form-group">
                  <label>Jurisdiction fee</label>
                  <input type="text" name="fee" parsley-trigger="change" required placeholder="Enter jurisdiction fee" class="form-control" value="<?php echo $jurisdiction_fee->fee; ?>">
                </div><!--/form-group-->
                </div>
				
			
				<div class="form-group">
                 
                  <input type="hidden" id="sub_cat" value="<?php echo $jurisdiction_fee->state; ?>" name="state" parsley-trigger="change" required placeholder="Enter State" class="form-control">
				  
                </div><!--/form-group-->
				
				 <div class="clearfix"></div>
			
				<div class="form-group col-md-12">
                
                <button class="btn btn-primary" type="submit">Submit</button>
                    <a href="<?php echo base_url();?>admin/settings/jurisdictionFee" class="btn btn-default">Cancel</a>
				 </div>
				 
				 <div class="clearfix"></div>
				 
			 </form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>
   <script type="text/javascript">
  	function validatePhone()
	{
		var home_phone = $('#home_phone');
		var work_phone = $('#work_phone');
		var mobile = $('#mobile');
		if(home_phone.val() == '' && work_phone.val() == '' && mobile.val() == '')
		{
			alert('Please enter a phone no!');
			home_phone.focus();
			return false;
		}
	}
	 function selectLawFirm(id, actionUrl, reloadUrl) {

        if (id == '') {
            $("#sub_cat").val('');
            return true;
        }
        $.post( "<?php echo base_url();?>"+actionUrl,{ id: id }, function( data ) {
		  $('#sub_cat').val(data);
		});
    }

  </script>
  
  
  