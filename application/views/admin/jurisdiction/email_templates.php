<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Email Templates</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Email Templates </h3>
            </div>
         <div class="porlets-content">
            <div class="table-responsive">
            <div class="clearfix">
            				
                              <div class="btn-group">
                                  <a href="<?php echo base_url();?>admin/settings/addEmailTemplate"> <!-- 2 is role that define type. 2 is for lawyers -->
								   <button class="btn btn-primary">
                                      Add New Email Template<i class="fa fa-plus"></i>
                                  </button></a>
                              </div>
                          </div>
                          
                          <?php echo $this->session->flashdata('message')?>
                          <div class="margin-top-10"></div>
                <table  class="display table table-bordered table-striped" id="dynamic-table">
                  <thead>
                    <tr>
                     
                      <th>Title (Trigger) </th>
                      <th>Email From</th>
                      <th>Subject</th>
                      <th>Content</th>
                      
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php foreach($email_templates as $template){?>
                    <tr class="gradeX">
                      
                      <td><?php echo $template->title; ?></td>
                      <td><?php echo $template->email_from; ?></td>
                      <td><?php echo $template->subject; ?></td>
                      <td><?php echo $template->content; ?></td>

                      <td><a href="<?php echo base_url();?>admin/settings/editEmailTemplate/<?php echo $template->id;?>" >Edit </a> | <a href="<?php echo base_url();?>admin/settings/delete_email_template/<?php echo $template->id;?>" >Delete </a></td> <!-- 2 define role which is specify it is lawyer -->
                    </tr>
                   <?php } ?> 
                  </tbody>
                 
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
            
            
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
       
 
    </div><!--/page-content end--> 
  </div><!--/main-content end-->
</div>