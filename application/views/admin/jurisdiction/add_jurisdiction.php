<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Add New Jurisdiction</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Jurisdiction</h3>
            </div>
            <?php echo $this->session->flashdata('message')?>
            <div class="porlets-content">
              <form action="<?php echo base_url();?>admin/settings/saveJurisdiction" method="post" parsley-validate novalidate>
            
			<div class="form-group col-md-4">		
				<div class="form-group">
                  <label>Jurisdiction name *</label>
                  <input type="text" name="jurisdiction_name" parsley-trigger="change" required placeholder="Enter jurisdiction name" class="form-control">
                </div><!--/form-group-->
			</div>
			
			<div class="form-group col-md-4">		
				 <div class="form-group">
                  <label>State *</label>
                 
                    <select placeholder="state" class="form-control" name="state" required>
                       	<option value="" required> Select state </option>
                     
					  <?php foreach($states as $state){?>
                      	<option value="<?php echo $state->id;?>"> <?php echo $state->state_short_name;?> </option>
                      <?php }?>
               
      			   </select>
                 
                </div>
            </div>
              
			<div class="clearfix"></div>
				
              <div class="form-group col-md-12">
                <button class="btn btn-primary" type="submit">Submit</button>
                  <a href="<?php echo base_url();?>admin/settings/jurisdiction" class="btn btn-default">Cancel</a>
			  </div>	
			<div class="clearfix"></div>
			
			</form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>