<body>

<div class="login-container">

  <div class="middle-login">

    <div class="block-web">

      <div class="head">

        <h3 class="text-center"><?php echo $this->config->item('project_title'); ?></h3>

      </div>

      <div style="background:#fff;">

        <form action="<?php echo current_url();?>" method="post" class="form-horizontal pwd-reset">

          <input type="hidden" name="unique_code" value="<?php echo $user->unique_code;?>">

          <div class="content">
			<?php if ($this->session->flashdata('message')) {
				echo $this->session->flashdata('message');
			} ?>
            <h4 class="title">Set Password</h4>

			<span class="pwd-note">Password must be at least 6 characters and contain one number and one uppercase letter.</span>

            <div class="form-group email">

              <div class="col-sm-12">

                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-user"></i></span>

                  <input type="text" class="form-control" placeholder="Email" value="<?php echo ($this->uri->segment(4) == 'lawyer' || $this->uri->segment(4) == 'lawstaff' ? $user->email : $user->contact_email); ?>" disabled>

                  <input type="hidden" name="email" value="<?php echo ($this->uri->segment(4) == 'lawyer' || $this->uri->segment(4) == 'lawstaff' ? $user->email : $user->contact_email); ?>" >

                </div>

              </div>

            </div>

            <div class="form-group">

              <div class="col-sm-12">

                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>

                  <input type="password" name="password" parsley-trigger="change"  required placeholder="New Password" class="form-control">

				  <?php echo (form_error('password') ? form_error('password') : ''); ?>

                </div>

              </div>

            </div>

            <div class="form-group">

              <div class="col-sm-12">

                <div class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>

                  <input  type="password" name="confirm_password" parsley-trigger="change"  required placeholder="Confirm Password" class="form-control">

				  <?php echo (form_error('confirm_password') ? form_error('confirm_password') : ''); ?>

                </div>

              </div>

            </div>

          </div>

          <div class="foot">

          	<!--<div class="text-center pull-left"><a class="btn btn-default" href="<?php /*echo base_url();*/?>"> Login</a></div>-->

            <button class="btn btn-primary" type="submit">Save</button>

          </div>

        </form>

      </div>

    </div>

    <input type="hidden" id="base_url" value="<?php echo base_url();?>">

  </div>

</div>