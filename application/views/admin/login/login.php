<body>
	<div class="login-container">
		<div class="middle-login">
			<div class="block-web">
				<div class="head">
					<h3 class="text-center"><?php echo $this->config->item('project_title'); ?></h3>
				</div>
				<div style="background:#fff;">
					<form action="<?php echo base_url();?>admin/login/login" class="form-horizontal login-form" style="margin-bottom: 0px !important;" method="post">
						<div class="content">
							<h4 class="title">Login Access
							</h4>
							<?php if ($this->session->flashdata('message')) {
								echo $this->session->flashdata('message');
							} ?>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="text" class="form-control" id="email" placeholder="Email" name="email" >
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<div class="input-group"> <span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" class="form-control" id="password" placeholder="Password" name="password">
									</div>
								</div>
							</div>
						</div>
						<div class="foot">
							<div class="text-center forgot"><a href="" id="forgot_pss" data-toggle="modal" data-target="#myModal">Forgot password?</a></div>
							<!-- <div class="text-center forgot"><a href="#">Forgot username?</a></div>-->
							<button type="submit" data-dismiss="modal" class="btn btn-primary">Log in</button>
							<!--<a href="<?php echo 'base_url';?>lawyer/lawyer_index/register">Register</a>-->
						</div>
					</form>
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" id="myModalLabel">Enter user email to reset password</h4>
								</div>
								<div class="forgot_error"></div>
								<div class="forgot_success"></div>
								<div class="modal-body">
									<div class="col-sm-12">
										<div class="input-group"> 
											<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
											<input type="text" class="form-control" id="sendemail" placeholder="Email" name="sendemail" >
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="button" id="sendEmail" class="btn btn-primary">Send</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="text-center out-links"><a href="#">&copy;  <?php echo $this->config->item('copy_right').' '.date('Y') ?>. </a></div>
			<input type="hidden" id="base_url" value="<?php echo base_url();?>">
		</div>
	</div>
	<?php if ($this->session->flashdata('email') && $this->session->flashdata('password')) { ?>
		<script>
			$(function() {
				$('.login-form [name=email]').val('<?php echo $this->session->flashdata('email'); ?>');
				$('.login-form [name=password]').val('<?php echo $this->session->flashdata('password'); ?>');
			   $('.login-form').submit();
			});
		</script>
	<?php } ?>