<?php 

	//date_default_timezone_set('America/Los_Angeles');

?>

<div id="main-content">

	<div class="page-content">

		<div class="row">

			<div class="col-md-12">

				<h2>Quick Lead Form</h2>

			</div>

			<!--/col-md-12--> 

		</div>

		<!--/row-->

		<div class="row">

			<div class="col-md-12">

				<div class="block-web">

					<div class="header">

						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>

						<h3 class="content-header">Lead Form</h3>

					</div>

					<div class="porlets-content row">

						<form action="<?php echo base_url();?>admin/leads/save" method="post" onsubmit="return validatePhone();" parsley-validate novalidate>

							<span class="col-md-12 description_text">



							</span>

							<div class="form-group col-md-4">

								<label>First name *(confirm spelling)</label>

								<input type="text" name="first_name" parsley-trigger="change" required placeholder="Enter first name" class="form-control">

							</div>

							<!--/form-group-->

							<div class="form-group col-md-4">

								<label>Last name (confirm spelling)</label>

								<input type="text" name="last_name" parsley-trigger="change" placeholder="Enter last name" class="form-control">

							</div>

							<!--/form-group-->

							<div class="form-group col-md-4">

								<label>Email address</label>

								<input type="email" name="email" parsley-trigger="change" placeholder="Enter email" class="form-control">

							</div>

							<!--/form-group-->

							<span class="col-md-12 description_text"></span>

							<div class="form-group col-md-4">

								<label>Caller name if different</label>

								<input type="text" name="caller_name" parsley-trigger="change"  placeholder="Enter caller name if different" class="form-control">

							</div>

							<!--/form-group-->

							<div class="form-group col-md-4">

								<label>Caller ID if different</label>

								<input type="text" name="caller_id" parsley-trigger="change"  placeholder="Enter caller ID if different" class="form-control">

							</div>

							<!--/form-group-->			



							<!--/form-group-->
							<span class="col-md-12 description_text"></span>
							<div class="form-group col-md-4">

								<label>Mobile*</label>

								<input type="text" name="mobile" id="mobile" parsley-trigger="change" required placeholder="Enter mobile number" class="form-control">

							</div>
							<div class="form-group col-md-4">

								<label>Home phone</label>

								<input type="text" name="home_phone" id="home_phone" parsley-trigger="change"  placeholder="Enter home phone number" class="form-control">

							</div>

							
							<div class="form-group col-md-4">
								<label>Work phone</label>
								<input type="text" name="work_Phone"  parsley-trigger="change"  placeholder="Enter number" class="form-control" value="">
							</div>

							<!--
						   <div class="form-group col-md-4">
								<label>Street</label>
								<input type="text" name="street"  parsley-trigger="change"  placeholder="Enter street" class="form-control">
							</div>
						-->

						<div class="clearfix"></div>

						<span class="col-md-12 description_text"></span>
						<div class="form-group col-md-4">

							<label>City</label>

							<input type="text" name="city" parsley-trigger="change" placeholder="Enter city name" class="form-control">

						</div>

						<div class="form-group col-md-4">

							<div class="form-group">

								<label>State *</label>

								<select placeholder="state" class="form-control" name="state" required>

									<option value="" required> Select state </option>

									<?php foreach($states_short_names as $state){?>

									<option value="<?php echo $state->id;?>" <?php echo ( $state->state_short_name == ' CA ' || $state->id == '5' ? 'selected=""' : '' ) ?>> <?php echo $state->state_short_name;?> </option>

									<?php }?>

								</select>

							</div>

						</div>
                        
                        <div class="col-md-4">
							<div class="form-group">
								<label>Zip</label>
								<input type="text" name="zip"  parsley-trigger="change"  placeholder="Enter zip code" class="form-control" value="">
							</div>
						</div>
                        
                        <div class="clearfix"></div>

						<div class="form-group col-md-4">
							<div class="form-group">
								<label>Source</label>
								<select placeholder="source" class="form-control" name="source">
									<option value="" required> Select source </option>
									<?php foreach($sources as $source){?>
									<option value="<?php echo $source->id;?>"> <?php echo $source->name;?> </option>
									<?php }?>
								</select>
							</div>
						</div>

						<!--<span class="col-md-12 description_text"></span>-->

						
						<div class="col-md-4">

							<label>Jurisdiction *</label>

							<select placeholder="Jurisdiction" class="form-control" name="jurisdiction" required>

								<option value="">Select jurisdiction</option>

								<?php foreach($jurisdiction as $jurisdictions){

										//$stateName =  getStateName($jurisdictions->state);

										//$stateName[0]['state_short_name'];

									?>

									<option <?= $jurisdictions->id == 39 ? 'selected=""' : ''; ?> attr-select-value="<?php echo $jurisdictions->value; ?>" value="<?php echo $jurisdictions->id;?>"> <?php echo $jurisdictions->name;?> </option>

									<?php }?>

								</select>

							</div>
						<div class="col-md-4">

							<div class="form-group">

								<label>Market *</label>

								<select placeholder="market" class="form-control" name="market" id="m_id" onchange="selectCaseTypeMarketFees(this.value,'admin/Dashboard/getMarketCaseTypePrice','');" required>

									<option value="" required> Select market </option>

									<?php foreach($markets as $market){

										$stateName =  getStateName($market->state);

										$stateName[0]['state_short_name'];

										?>

										<option <?= $market->id == 6 ? 'selected=""' : ''; ?> value="<?php echo $market->id;?>"> <?php echo $market->market_name . " - " . $stateName[0]['state_short_name'];?> </option>

										<?php }?>

									</select>

								</div>

							</div>
                            <div class="clearfix"></div>
                            <span class="col-md-12 description_text"></span>
							<div class="form-group col-md-4">

								<label>Case type *</label>

								<select placeholder="Case Type" class="form-control" name="case_type" id="case_type_id" onchange="selectCaseTypeMarketFee(this.value,'admin/Dashboard/getMarketCaseTypePrice','');" required>

									<option value="">Select case type</option>

									<?php foreach($case_type as $case){?>

									<option value="<?php echo $case->id;?>"> <?php echo $case->type;?> </option>

									<?php }?>

								</select>

							</div>
							<div class="clearfix"></div>
							<span class="col-md-12 description_text"></span>

							<div class="form-group col-md-4">

								<div class="">

									<label>Legal issue / case description</label>

									<textarea class="form-control" name="case_description" parsley-trigger="change" placeholder="Enter legal issue / case description"></textarea>



								</div>

							</div>


							<div class="form-group col-md-8">

								<label>Intake notes</label>

								<textarea class="form-control" name="my_notes"></textarea>

							</div>


							<div class="form-group col-md-4">

								<label>Lead date/time</label>

								<input type="text" name="created_at" id="time-holder" parsley-trigger="change" placeholder="Enter lead date/time" class="form-control form_datetime-adv datepicker-here" data-date-format="mm-dd-yyyy" data-language="en" data-timepicker="true">

							</div>

							<!--/form-group-->

							<div class="form-group col-md-4">

								<label>Add Date</label></br>

								<button class="btn btn-primary" type="button" value="time" name="timer" id="time">Now</button>

							</div>

							<!--/form-group-->

							<div class="clearfix"></div>

							<div class="form-group">

								<input type="hidden" name="lead_status" value="1" class="form-control">

							</div>

							<!--/form-group-->

							<div class="clearfix"></div>

							<div class="form-group col-md-12">
                            
                                <button class="btn btn-primary" type="submit" name="submit" value="assign_button">Save & Assign</button>

								<button class="btn btn-primary pull-left" type="submit" style="margin-right: 4px;" name="submit" value="save_button">Save & Hold</button>

								<button class="btn btn-primary" type="submit" name="submit" value="submit_button">Save & Submit</button>

							</div>

							<div class="clearfix"></div>

							<!--<button class="btn btn-default">Cancel</button>-->

						</form>

					</div>

					<!--/porlets-content-->

				</div>

				<!--/block-web--> 

			</div>

			<!--/col-md-6-->

		</div>

		<!--/row-->

	</div>

	<!--/page-content end--> 

</div>

<script>

	$(

		function() {



			$('#time').click(function() {

				$('#time-holder').val('<?php echo date('m-d-Y h:i:s A') ?>');

			});



		}

		);



	function selectCaseTypeMarketFee(id, actionUrl, reloadUrl) {



		var m_id = $('#m_id').val();



		if (id == '') {

			$("#sub_cat").val('');

			return true;

		}

		$.post("<?php echo base_url();?>" + actionUrl, {

			id: id,

			m_id: m_id

		}, function(data) {

			$('#sub_cat').val(data);

		});

	}



	function selectCaseTypeMarketFees(id, actionUrl, reloadUrl) {



		var c_id = $('#case_type_id').val();



		if (id == '') {

			$("#sub_cat").val('');

			return true;

		}

		$.post("<?php echo base_url();?>" + actionUrl, {

			c_id: c_id,

			id: id

		}, function(data) {

			$('#sub_cat').val(data);

		});

	}



	function validatePhone() {

		var home_phone = $('#home_phone');

		var work_phone = $('#work_phone');

		var mobile = $('#mobile');

		if (home_phone.val() == '' && work_phone.val() == '' && mobile.val() == '') {

			alert('Please enter a phone no!');

			home_phone.focus();

			return false;

		}

	}



	function tagid(id, value) {



		var selectedVal = value;

    //document.getElementById("tags_2").value = selectedVal;

    $('#span').append('<span class="spanclass"><input type="button" class="link" value="' + value + '"/><a class="btnremove" id="removetag">x</a><input type="hidden" name="labels[]" value="' + value + '"></span>');

    var tags_labels = $('#tags_labels').val();

    $('#tags_labels').val(tags_labels + selectedVal + ',');





}

$(document).ready(function() {



	$(document).on('click', '.spanclass a', function() {

		$(this).parent().remove();

	});
	
	$('[name="jurisdiction"]').on('change', function(e){
		
		if($(this).val() != ''){
			
			var selected = $('[name="market"] option:selected').val();
			
			var option = $('option:selected', this).attr('attr-select-value');
			
			if(option != selected){
				
				$('[name="market"] option').prop('selected', false);
				
				$('[name="market"] option[value="' + option + '"]').prop('selected', true);
				
			}
			
		}else{
			
			$('[name="market"] option').prop('selected', false);
			
		}
		
	});

});

</script>