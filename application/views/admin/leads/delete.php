<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Deleted Leads</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="porlets-content">
                        <div class="row">
            				<div class="form-group col-md-2">
            					<input type="text" name="case_id" id="case_id" parsley-trigger="change" placeholder="Enter Last 5 digits of Case Id" class="form-control searchByCaseId">
            				</div>
            			</div>
						<div class="table-responsive">
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
										<th>Case Id</th>
										<th>Law Firm</th>
										<th>Lawyer Name</th>
										<th>Lead Name</th>
										<th>Case Type</th>
										<th>Market</th>
										<th>Case Description</th>
										<th>Deleted By</th>
										<th>Date Deleted</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="resultBody">
									<?php
									if(!empty($deleted_leads))
									{
										foreach($deleted_leads as $lead)
										{
											if($lead->lead_status != 1)
											{ 
												$assigned_detail = getLeadsLawyer($lead->id);
												$assigned_lawyer_law_firm = getLeadsLawyerLawfirm($lead->id);
											}
											?>
											<tr>
												<td><?php echo $lead->lead_id_number; ?></td>
												<td><?php if($lead->lead_status != 1 && $lead->lead_status != 4){ echo $assigned_lawyer_law_firm['assigned_to'];} else{ echo 'Not Assigned';}?></td>
												<td><?php if($lead->lead_status != 1 && $lead->lead_status != 4){ echo $assigned_detail['assigned_to'];} else{ echo 'Not Assigned';}?></td>
												<td><?php echo $lead->first_name.' '.$lead->last_name; ?></td>
												<td><?php $caseTypeName =  getCaseTypeName($lead->case_type); echo $caseTypeName[0]['type']; ?></td>
												<td><?php  $newJurisdiction = getMarketName($lead->market); $stateName =  getStateName($newJurisdiction->state); $stateName[0]['state_short_name']; echo $newJurisdiction->market_name . " - " . $stateName[0]['state_short_name']; ?></td> 
												<td><?php echo $lead->case_description; ?></td>
												<td><?php echo $lead->deleted_by; ?></td>
												<td><?php echo date('M d, Y h:i a', strtotime($lead->deleted_on)); ?></td>
												<td><a href="<?php echo base_url().'admin/leads/viewLead/'.$lead->id; ?>/deletedLead">View Lead</a>&nbsp;|&nbsp;<a href="<?php echo base_url().'admin/leads/undelete/'.$lead->id; ?>">Undelete Lead</a></td>
											</tr>
									<?php } } ?> 
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<!--/main-content end-->
</div>
<script type="text/javascript">
    $(function(){
        $(".searchByCaseId").keyup(function (e) {
            e.preventDefault();
            var case_id = $(this).val();
    		$(".page_loader").show();
            $.post('<?php echo base_url('admin/deleted/searchByCaseId'); ?>', {case_id: case_id}, function(data){
                $('#resultBody').html(data);
                $(".page_loader").hide();
            });
    	});
    });
</script>