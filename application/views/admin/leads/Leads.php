<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Leads extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('latest_url', current_url());
        chechUserSession();
        $this->load->model('admin/Leads_model');
        $this->load->model('admin/Users_model');
        $this->load->model('admin/Activity_model');
        $this->load->model('admin/Survey_model');
        $this->load->model('admin/Setting_model');
        $this->load->model('Central_model');
    }

    public function index()
    {
        $data = array();
        if ($this->session->userdata('role') == 1) {
            $data['leads'] = $this->Leads_model->fetchAllLeads();
            $view = 'admin/leads/list';
        } //$this->session->userdata('role') == 1
        elseif ($this->session->userdata('role') == 2) {
            $view = 'admin/leads/lawyers_leads';
            $data['leads'] = $this->Leads_model->fetchAllLeadsOfLawyers($this->session->userdata('id'));
        } //$this->session->userdata('role') == 2
        $data['class_leads'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view($view, $data);
        $this->load->view('layouts/admin/footer', $data);
    }

    public function add()
    {
        $data = array();
        $data['class_leads'] = 'active';
        $data['jurisdiction'] = $this->Setting_model->fetchAllJurisdiction();
        $data['case_type'] = $this->Leads_model->fetchCaseType();
        $data['tags_type'] = $this->Leads_model->fetchTagType();
        $data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
        $data['markets'] = $this->Setting_model->marketRecords();
        $data['intakers'] = $this->Setting_model->intakerRecords();
        $data['sources'] = $this->Setting_model->sourceRecords();
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/leads/add_lead_operator', $data);
        $this->load->view('layouts/admin/footer', $data);
    }

    public function addLawFee()
    {
        $data = array();
        $data['class_leads'] = 'active';
        $this->load->view('layouts/admin/header', $data);
        $this->load->view('layouts/admin/sidebar', $data);
        $this->load->view('admin/leads/addLawFee', $data);
        $this->load->view('layouts/admin/footer', $data);
    }

    public function save()
    {
        $data = array();
        $activity = array();
        $content = array();
        $data = $this->input->post();
        if (empty($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        } //empty($data['created_at'])
        else {
            $data['created_at'] = _dateTimeFormat($this->input->post('created_at'));
        }
        $data['labels'] = implode(',', $data['labels']);
        unset($data['submit']);
        $formSubmit = $this->input->post('submit');
        if ($formSubmit == 'submit_button') {
            $data['lead_status'] = "1";
        } //$formSubmit == 'submit_button'
        if ($formSubmit == 'save_button') {
            $data['lead_status'] = "4";
        } //$formSubmit == 'save_button'
		if ($formSubmit == 'assign_button') {
            $data['lead_status'] = "1";
        } //$formSubmit == 'assign_button'
        $state = $this->Leads_model->fetchStateName($data['state']);
        $market = $this->Leads_model->fetchMarketName($data['market']);
        $case_type = $this->Leads_model->fetchCasetypeName($data['case_type']);
        $market_two_character = substr($market->market_name, 0, 2);
        $random_number = substr(str_shuffle(str_repeat("0123456789", 5)), 0, 5);
        $sp_rep_sk = array(
            "@" => "",
            "(" => "",
            ")" => "",
            "<>" => "",
            "'" => "",
            "-" => ""
        );
        $market_two_character = strtr($market_two_character, $sp_rep_sk);
        if (strlen($market_two_character) == '1') {
            $market_two_character = str_pad($market_two_character, 2, "0", STR_PAD_LEFT);
        } //strlen($market_two_character) == '1'
        $case_type_two_character = str_pad($data['case_type'], 2, "0", STR_PAD_LEFT);
        $data['lead_id_number'] = $state->state_short_name . "-" . $market_two_character . "-" . $case_type_two_character . "-" . $random_number;
        $insert_id = $this->Leads_model->save($data);
        $super_admin = $this->Leads_model->fetchSuperAdminRecord();
        $template = $this->Leads_model->template(5);
        if ($insert_id) {
            $lead_detail = $this->Leads_model->fetchLeadDetail($insert_id);
            $state_name = $this->Leads_model->fetchStateName($lead_detail->state);
            $subject = $template->subject;
            $subject = str_replace("{ref}", "ref_no: " . $lead_detail->id, $subject);
            $subject = str_replace("{Area of Law}", $lead_detail->case_type, $subject);
            $subject = str_replace("CA", $state_name->state_short_name, $subject);
            $to = $lead_detail->email;
            $from = $template->email_from;
            $name = $this->config->item('project_title');
            $caseTypeName = getCaseTypeName($lead_detail->case_type);
            $content['lead_detail'] = $lead_detail;
            $content['caseTypeName'] = $caseTypeName;
            /*Email To Lawyer*/
            $message = $template->content;
			$message = str_replace("{name}", $lead_detail->first_name.' '.$lead_detail->last_name, $message);
			$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
			$message = str_replace("{rootpath}", base_url(), $message);
			$message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
			$message = str_replace("{case_id}", $lead_detail->lead_id_number, $message);
			$message = str_replace("{referal_date}", date('M-d-Y H:i:s a'), $message);
			$message = str_replace("{phone}", $lead_detail->mobile, $message);
			$message = str_replace("{email}", $lead_detail->email, $message);
			$message = str_replace("{description}", $lead_detail->case_description, $message);
			$message = str_replace("{jurisdiction}", getJurisdictionNamee($lead_detail->jurisdiction), $message);
			$message = str_replace("{case_type}", $caseTypeName[0]['type'], $message);
			$message = str_replace("{location}", $lead_detail->street, $message);
			$message = $this->load->view('admin/leads/emails/new_lead_template',array("message" => $message), true);
            send_email($to, $from, $name, $subject, $message);    
		    /*Email To Admin*/
            send_email($super_admin->superAdminEmail, $from, $name, $subject, $message);
			/*Send SMS*/
			$sms_message = $template->text_message;
			$sms_message = str_replace("{area_of_law}", $caseTypeName[0]['type'], $sms_message);
			sms(1, $lead_detail->mobile, $sms_message);
			/*Save Activity*/
            $activity['created_at'] = date('Y-m-d H:i:s');
            $activity['lead_id'] = $insert_id;
            $activity['event'] = 'Entry Created';
            $activity_insert_id = $this->Activity_model->save($activity);
            $this->session->set_flashdata('message', '<p class="message_success">Lead added successfully.</p>');
			if ($formSubmit == 'assign_button') {
				redirect($this->config->item('base_url') . 'admin/leads/view/'.$insert_id);
			}else{
				redirect($this->config->item('base_url') . 'admin/dashboard/manage');
			}
            //redirect($this->config->item('base_url') . 'admin/dashboard/manage');
        } //$insert_id
    }

    public function saveLeadFee()
    {
        $data = array();
        $activity = array();
        $data = $this->input->post();
        $data['date_time'] = date('Y-m-d H:i:s');
        unset($data['submit']);
        $insert_id = $this->Leads_model->saveLeadFee($data);
        if ($insert_id > 0) {
            $this->session->set_flashdata('message', '<p class="message_success"> Added successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/dashboard/manage');
        } //$insert_id > 0
    }

    public function edit()
    {
        $data = array();
        $data['id'] = $this->uri->segment(4);
        $data['class_leads'] = 'active';
        $data['lead'] = $this->Leads_model->fetchRow($data['id']);
        $data['case_type'] = $this->Leads_model->fetchCaseType();
        $data['tags_type'] = $this->Leads_model->fetchTagType();
        $data['jurisdiction'] = $this->Setting_model->fetchAllJurisdiction();
        $data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
        $data['markets'] = $this->Setting_model->marketRecords();
        $data['intakers'] = $this->Setting_model->intakerRecords();
        $data['sources'] = $this->Setting_model->sourceRecords();
        $lawyer = $this->Leads_model->fetchLawyer($data['id']);
        $data['activities'] = $this->Activity_model->fetchAll($data['lead']->id);
		$data['lawyers_notes'] = $this->Leads_model->get_lawyer_notes($data['lead']->id);
        # $data['lawyers_notes'] = $this->Central_model->select_all_array('lead_notes', array('lead_id' => $data['lead']->id, 'role' => 2));
		$Admin_id = $this->session->userdata('id');
		$data['Admin_notes'] = $this->Leads_model->get_admin_notes($Admin_id, $data['id'], $this->session->userdata('role'));
        if (!empty($data['lead'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/leads/edit', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['lead'])
    }

    public function editLead()
    {
        $data = array();
        $data['id'] = $this->uri->segment(4);
        $data['class_leads'] = 'active';
        $data['lead'] = $this->Leads_model->fetchRow($data['id']);
        $data['case_type'] = $this->Leads_model->fetchCaseType();
        $data['tags_type'] = $this->Leads_model->fetchTagType();
        $data['jurisdiction'] = $this->Setting_model->fetchAllJurisdiction();
        $data['states_short_names'] = $this->Users_model->fetchAllAmericanStates();
        $data['markets'] = $this->Setting_model->marketRecords();
        $data['intakers'] = $this->Setting_model->intakerRecords();
        $data['sources'] = $this->Setting_model->sourceRecords();
        if (!empty($data['lead'])) {
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            $this->load->view('admin/leads/edit_lead', $data);
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['lead'])
    }

    public function view($id, $code = Null)
    {   
        $data = array();
        $data['id'] = $id;
        $data['class_leads'] = 'active';
        $data['lead'] = $this->Leads_model->fetchRow($data['id']);
        $data['lawyers_all'] = $this->Users_model->getAllLawyers();
        $data['tags_type'] = $this->Leads_model->fetchTagType();
        if (!empty($data['lead'])) {
            $data['assign_lawyers'] = $this->Users_model->fetchAssignLawyers($data['id']); // on_holiday done		
            $data['case_type'] = $this->Leads_model->fetchCaseType();
            $data['lawyers'] = ($data['lead']->jurisdiction && $data['lead']->case_type ? $this->Users_model->lead_assigned_users(2, 1, $data['lead']->jurisdiction, $data['lead']->case_type) : []); // on_holiday done
            $this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
            if ($this->session->userdata('role') == 1) {
                $Admin_id = $this->session->userdata('id');
                # $data['Admin_notes'] = $this->Central_model->select_all_array('lead_notes', array('user_id' => $Admin_id, 'lead_id' => $data['id'], 'role' => $this->session->userdata('role')));
                # $data['lawyers_notes'] = $this->Central_model->select_all_array('lead_notes', array('lead_id' => $data['id'], 'role !=' => 1));
				$data['Admin_notes'] = $this->Leads_model->get_admin_notes($Admin_id, $data['id'], $this->session->userdata('role'));
                $data['lawyers_notes'] = $this->Leads_model->get_lawyer_notes($data['id']);
                $data['activities'] = $this->Activity_model->fetchAll($data['id']);
				$lawyer = $this->Leads_model->fetchLawyer($data['id']);
                if (!empty($lawyer)) {
                    $data['user_id'] = $lawyer->lawyer_id;
                } //!empty($lawyer)
                $data['case_type'] = $this->Leads_model->fetchCaseType();
                $this->load->view('admin/leads/view', $data);
            } //$this->session->userdata('role') == 1 || $this->session->userdata('role') == 5
            elseif ($this->session->userdata('role') == 2) {
                /*Lead into Archive and Complete*/
                /*End*/
                $user_id = $this->session->userdata('id');
                $count_rows = $this->Central_model->count_rows("lawyers_to_leads", array("lead_id" => $data['id'], "lawyer_id" => $user_id, "unique_code" => $code));
                if ($count_rows != 0) {
					$dataUpdateTime['lead_open_datetime'] = Date('Y-m-d H:i:s');
					$updateData = $this->Users_model->UpdateleadOpenDateTime($dataUpdateTime, $user_id, $data['id']);
					$data['rand_code'] = $code;
					$data['lawyers_notes'] = $this->Leads_model->get_lawyer_notes($data['id']);
					# $data['lawyers_notes'] = $this->Central_model->select_all_array('lead_notes', array('lead_id' => $data['id'], 'role !=' => 1));
					$data['activities'] = $this->Activity_model->fetchAll($data['id'], $user_id);
					$data['lawyer_id'] = $user_id;
					$this->load->view('admin/leads/lawyers_lead_view', $data);
                } else {
                    show_404();
                }
            } //$this->session->userdata('role') == 2 
			elseif($this->session->userdata('role') == 6) {
				$lead_assigned_info = $this->Central_model->first("lawyers_to_leads", "lead_id", $data['id']);
				$user_id = $lead_assigned_info->lawyer_id;
				$dataUpdateTime['lead_open_datetime'] = Date('Y-m-d H:i:s');
				$updateData = $this->Users_model->UpdateleadOpenDateTime($dataUpdateTime, $user_id, $data['id']);
				$data['rand_code'] = $code;	
				$data['lawyers_notes'] = $this->Leads_model->get_lawyer_notes($data['id']);
				# $data['lawyers_notes'] = $this->Central_model->select_all_array('lead_notes', array('lead_id' => $data['id'], 'role !=' => 1));
				$data['activities'] = $this->Activity_model->fetchAll($data['id'], $user_id);
				$data['lawyer_id'] = $user_id;
				$this->load->view('admin/leads/lawyers_lead_view', $data);	
			}
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['lead'])
    }

    public function viewLead($id, $view = '')
    {
        $data = array();
        $data['id'] = $id;
        $data['class_leads'] = 'active';
		if($view !== '' && $view == 'deletedLead'){
			$data['lead'] = $this->Leads_model->fetchRow($data['id'], $view);
		}else{
			$data['lead'] = $this->Leads_model->fetchRow($data['id']);
		}
        //$data['lead'] = $this->Leads_model->fetchRow($data['id']);
        $data['activities'] = $this->Activity_model->fetchAll($data['id']);
        $data['tags_type'] = $this->Leads_model->fetchTagType();
        if (!empty($data['lead']) && $this->session->userdata('role') == 1) {
            $data['case_type'] = $this->Leads_model->fetchCaseType();
			$Admin_id = $this->session->userdata('id');
			$data['Admin_notes'] = $this->Leads_model->get_admin_notes($Admin_id,$data['id'],$this->session->userdata('role'));
			$data['lawyers_notes'] = $this->Leads_model->get_lawyer_notes($data['id']);
			# $data['Admin_notes'] = $this->Central_model->select_all_array('lead_notes', array('user_id' => $Admin_id, 'lead_id' => $data['id'], 'role' => $this->session->userdata('role')));
			# $data['lawyers_notes'] = $this->Central_model->select_all_array('lead_notes', array('lead_id' => $data['id'], 'role' => 2));
			$lawyer = $this->Leads_model->fetchLawyer($data['id']);
			if (!empty($lawyer)) {
				$user_id = $lawyer->lawyer_id;
			} //!empty($lawyer)
			$this->load->view('layouts/admin/header', $data);
            $this->load->view('layouts/admin/sidebar', $data);
			if($view !== '' && $view == 'deletedLead'){
				$this->load->view('admin/leads/viewDeletedLead', $data);
			}else{
				$this->load->view('admin/leads/viewLead', $data);
            }
            $this->load->view('layouts/admin/footer', $data);
        } //!empty($data['lead'])
    }

    public function update()
    {
        $id = $this->uri->segment(4);
        $data = array();
        $data = $this->input->post();
        $formSubmit = $this->input->post('submit');
        if ($formSubmit == 'submit_button') {
            $data['lead_status'] = "1";
        } //$formSubmit == 'submit_button'
        $data['created_at'] = _dateTimeFormat($this->input->post('created_at'));
        $data['updated_at'] = date('Y-m-d H:i:s');
        unset($data['submit']);
        $update = $this->Leads_model->update($data, $id);
        if ($update) {
            $activity['created_at'] = date('Y-m-d H:i:s');
            $activity['lead_id'] = $id;
            $activity['event'] = 'Entry Updated';
            $insert_id = $this->Activity_model->save($activity);
            $this->session->set_flashdata('message', '<p class="message_success">Lead updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/dashboard/manage/' . $id);
        } //$update
    }

    public function updateLeadFee()
    {
        $id = $this->uri->segment(4);
        $data = array();
        $data = $this->input->post();
        unset($data['submit']);
        $update = $this->Leads_model->updateLeadFee($data, $id);
        if ($update) {
            $this->session->set_flashdata('message', '<p class="message_success">Lead Fee updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/dashboard/manage/' . $id);
        } //$update
    }

    public function update_type($id)
    {
        $data = array();
        $data = $this->input->post();
        $result = $this->Leads_model->updateTypeCase($data, $id);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Source updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/caseType/' . $id);
        } //$result
    }

    public function update_tag_type($id)
    {
        $data = array();
        $data = $this->input->post();
        $result = $this->Leads_model->updateTag($data, $id);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Source updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/tags_label/' . $id);
        } //$result
    }

    public function update_email_template($id)
    {
        $data = array();
        $data = $this->input->post();
        $data['updated_at'] = date('m:d:Y H:i:s');
        $result = $this->Leads_model->updateEmailTemplate($data, $id);
        if ($result) {
            $this->session->set_flashdata('message', '<p class="message_success">Template updated successfully.</p>');
            redirect($this->config->item('base_url') . 'admin/settings/email_templates/' . $id);
        } //$result
    }

    public function delete()
    {
        $data = array();
        $id = $this->uri->segment(4);
        if ($id) {
			// inserting into deleted leads table
			$lead_row[] = $this->Leads_model->fetchLeadDetail($id);
			foreach($lead_row as $row){
				$row->deleted_by = $this->session->full_name;
				$this->Leads_model->insertIntoDelete($row);
			}
            $data = $this->Leads_model->fetchArchieveLead($id);
            $data->lead_id = $data->id;
            unset($data->lead_open_datetime);
            unset($data->id);
            unset($data->lead_complete_time);
            $this->Leads_model->insertArchieveLead($data);
            $result = $this->Leads_model->deleteRow($id);
            if ($result) {
				$activity = [
					'lead_id' => $id,
					'event' => 'Lead Deleted',
					'admin_id' => $this->session->id
				];
				if($this->session->role == '6'){
					$activity['lawfirm_id'] = $this->session->id;
				}
				$this->Activity_model->save($activity);
                $this->session->set_flashdata('message', '<p class="message_success"> Deleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/dashboard/manage/Lead-View');
            } //$result
        } //$id
    }
	
	public function undelete()
    {
        $data = array();
        $id = $this->uri->segment(4);
        if ($id) {
			$lead_row[] = $this->Leads_model->fetchLeadDetail($id, $view = 'deletedLeads');
			foreach($lead_row as $row){
				$this->Leads_model->save($row);
			}
            $result = $this->Leads_model->deleteRow($id, $view = 'deletedLeads');
            if ($result) {
				$activity = [
					'lead_id' => $id,
					'event' => 'Lead Un-Deleted',
					'admin_id' => $this->session->id
				];
				if($this->session->role == '6'){
					$activity['lawfirm_id'] = $this->session->id;
				}
				$this->Activity_model->save($activity);
                $this->session->set_flashdata('message', '<p class="message_success"> Undeleted successfully.</p>');
                redirect($this->config->item('base_url') . 'admin/dashboard/manage/Lead-View');
            }
        }
    }

    public function assignLawyer()
    {
        $data = array();
        $this->load->helper('string');
        $data['lead_id'] = $this->uri->segment(4);
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['assigned_date'] = date('Y-m-d H:i:s');
        $lawyer = $this->input->post('lawyer_id');
        $leadDetail = $this->Leads_model->fetchLeadRow($data['lead_id']);
        $this->Leads_model->deleteLawyerToLead($data['lead_id']);
        //foreach ($lawyers as $lawyer) {
        if ($lawyer != '') {
            $user = $this->Leads_model->fetchLawyerRow($lawyer);
            $law_firm_name = $this->Leads_model->fetchLawyerLAwFirmName($user->law_firms);
            $data['lawyer_id'] = $lawyer;
            $data['unique_code'] = time() . random_string('alnum', 5);
            if ($user->email != '') {
                $lawyer_name = $user->name;
                $state_name = $this->Leads_model->fetchStateName($leadDetail->state);
				$template = $this->Leads_model->template(6);
				/*Send Email*/
                $subject = $template->subject;
                $subject = str_replace("{case_id}", $leadDetail->lead_id_number, $subject);
                $message = $template->content;
                $message = str_replace("{date} {time} (time lead assigned, not pended)", date('m-d-Y H:i:s a'), $message);
                $message = str_replace("{law_office_name}", $law_firm_name->law_firm, $message);
                $message = str_replace("{rootpath}", $this->config->item('base_url') . 'admin/Leads/view/' . $data['lead_id'] . '/' . $data['unique_code'], $message);
                $message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
                $message = str_replace("{name}", $lawyer_name, $message);
                $message = str_replace("{base_url}", base_url(), $message);
                $message = str_replace("{project_title}", $this->config->item('project_title'), $message);
                $from = $template->email_from;
                $name = $this->config->item('project_title');
                $to = $user->email;
				if(!$this->input->post('assign_wo_email')){
					send_email($to, $from, $name, $subject, $message);
				}
				/*Send SMS*/
				$sms_message = $template->text_message;
				$sms_message = str_replace("{case_number}", $leadDetail->lead_id_number, $sms_message);
				sms($user->is_sms, $user->phone, $sms_message); 
            } //$user->email != ''
            $dataNewArray = array();
            $dataNewArray['lead_id'] = $data['lead_id'];
            $dataNewArray['created_at'] = $data['created_at'];
            $dataNewArray['lawyer_id'] = $lawyer;
            $dataNewArray['date_time'] = date('Y-m-d H:i:s');
            $dataNewArray['payment_Status'] = '0';
            $leadRecord = $this->Leads_model->fetchLeadDetail($dataNewArray['lead_id']);
            $lead_fee = getJurisdictionFee($leadRecord->jurisdiction);
            $dataNewArray['fee'] = $lead_fee->fee;
            $LeadFee = $this->Leads_model->insertLeadFee($dataNewArray);
            $insert_id = $this->Leads_model->lawyerToLead($data);
            if ($insert_id > 0) {
                $activity['created_at'] = date('Y-m-d H:i:s');
                $activity['created_by'] = $this->session->id;
                $activity['lead_id'] = $data['lead_id'];
                $activity['event'] = 'Entry Assigned Lawyer';
                $status['lead_status'] = 2;
                $activity['lawyer_id'] = $lawyer;
                $this->Leads_model->update($status, $data['lead_id']);
				/*Check activity if already exist then deleted*/
				$this->Central_model->del("activities", array("event" => $activity['event'],"lawyer_id" => $activity['lawyer_id']));
                $insert_id = $this->Activity_model->save($activity);
                $this->session->set_flashdata('message', '<p class="message_success"> Lead assigned successfully.</p>');
            }
            redirect($this->config->item('base_url') . 'admin/Leads/view/' . $data['lead_id']);
        }else{
            redirect($this->config->item('base_url') . 'admin/Leads');
        } //$lawyer != ''
    }

    
    public function reassignLawyer($id)
    {
        $data = array();
        $this->load->helper('string');
        $data['lead_id'] = $id;
        $data['created_at'] = date('Y-m-d H:i:s');
		$leadRecord = $this->Leads_model->fetchLeadDetail($data['lead_id']);
        $lawyer = $this->input->post('lawyer_id');
        if ($this->input->post('reassign_lawyer') == 1) {
			$this->Leads_model->deleteLawyerToLead($data['lead_id']);
			$data['lawyer_id'] = $lawyer;
			$data['assigned_date'] = date('Y-m-d H:i:s');
			$data['unique_code'] = time() . random_string('alnum', 5);
			$dataNewArray = array();
			$dataNewArray['lead_id'] = $data['lead_id'];
			$dataNewArray['created_at'] = $data['created_at'];
			$dataNewArray['lawyer_id'] = $lawyer;
			$dataNewArray['date_time'] = date('Y-m-d H:i:s');
			$dataNewArray['payment_Status'] = '0';
			$lead_fee = getJurisdictionFee($leadRecord->jurisdiction);
			$dataNewArray['fee'] = $lead_fee->fee;
			$LeadFee = $this->Leads_model->insertLeadFee($dataNewArray);
			$insert_id = $this->Leads_model->lawyerToLead($data);
			$user = $this->Leads_model->fetchLawyerRow($lawyer);
			$law_firm_name = $this->Leads_model->fetchLawyerLAwFirmName($user->law_firms);
			if ($insert_id > 0) {
				$template = $this->Leads_model->template(6);
				/*Send Email*/
				$lawyer_name = $user->name;
				$state_name = $this->Leads_model->fetchStateName($leadRecord->state);
				$subject = $template->subject;
				$subject = str_replace("{case_id}", $leadRecord->lead_id_number, $subject);
				$message = $template->content;
				$message = str_replace("{date} {time}", date('m-d-Y H:i:s a'), $message);
				$message = str_replace("{law_office_name}", $law_firm_name->law_firm, $message);
				$message = str_replace("{rootpath}", $this->config->item('base_url') . 'admin/Leads/view/' . $data['lead_id'] . '/' . $data['unique_code'], $message);
				$message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/hl_logo_em.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
				$message = str_replace("{name}", $lawyer_name, $message);
				$message = str_replace("{base_url}", '<a href="'.base_url().'">'.base_url().'</a>', $message);
				$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
				$from = $template->email_from;
				$name = $this->config->item('project_title');
				$to =$user->email;
				if(!$this->input->post('assign_wo_email')){
					send_email($to, $from, $name, $subject, $message);
				}
				//send_email($to, $from, $name, $subject, $message);
				/*Send SMS*/
				$sms_message = $template->text_message;
				$sms_message = str_replace("{case_number}", $leadRecord->lead_id_number, $sms_message);
				sms($user->is_sms, $user->phone, $sms_message);
				/*Save Activity*/
				$activity['created_at'] = date('Y-m-d H:i:s');
				$activity['created_by'] = $this->session->id;
				$activity['lead_id'] = $data['lead_id'];
				$activity['event'] = 'Entry Reassigned Lawyer';
				$leadDetail['lead_status'] = 2;
				$leadDetail['lead_open_datetime'] = Null;
				$activity['lawyer_id'] = $lawyer;
				$this->Leads_model->update($leadDetail, $data['lead_id']);
				/*Check activity if already exist then deleted*/
				$this->Central_model->del("activities", array("event" => $activity['event'],"lawyer_id" => $activity['lawyer_id']));
				$insert_id = $this->Activity_model->save($activity);
				$this->session->set_flashdata('message', '<p class="message_success">Lawyer assigned to lead successfully.</p>');
				redirect($this->config->item('base_url') . 'admin/Leads/view/' . $data['lead_id']);
            }
        } //$this->input->post('reassign_lawyer') == 1
        elseif ($this->input->post('unassign_lawyer') == 1) {
            $this->session->set_flashdata('message', '<p class="message_success">Lead unassigned successfully.</p>');
            $lawyer_hidden_id = $this->input->post('lawyer_hidden_id');
            $template = $this->Leads_model->template(8);
            $state_name = $this->Leads_model->fetchStateName($leadRecord->state);
            foreach ($lawyer_hidden_id as $lawyer) {
                $user = $this->Leads_model->fetchLawyerRow($lawyer);
                $subject = $template->subject;
                $subject = str_replace("{ref}", "ref no: " . $leadRecord->id, $subject);
                $subject = str_replace("CA", $state_name->state_short_name, $subject);
                $to = $user->email;
                $message = $template->content;
                $message = str_replace("Determine lead reference", "<a href='" . $this->config->item('base_url') . "admin/leads/view/" . $data['lead_id'] . "'>View Unassigned Lead</a>", $message);
                $message = str_replace("{name}", $user->name, $message);
                $message = str_replace("{project_title}", $this->config->item('project_title'), $message);
                $message = str_replace("{rootpath}", base_url(), $message);
                $message = str_replace("{lead reference}", $this->config->item('base_url') . 'admin/Leads/view/' . $data['lead_id'], $message);
                $message = str_replace("{base_url}", base_url(), $message);
                $message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
                $from = $template->email_from;
                $name = $this->config->item('project_title');
				if(!$this->input->post('assign_wo_email')){
					send_email($to, $from, $name, $subject, $message);
				}
                //send_email($to, $from, $name, $subject, $message);
				/*Send SMS*/
				$sms_message = $template->text_message;
				$sms_message = str_replace("{case_number}", $leadRecord->lead_id_number, $sms_message);
				sms($user->is_sms, $user->phone, $sms_message);
            } //$lawyer_hidden_id as $lawyer
            if ($this->Leads_model->deleteLawyerToLead($data['lead_id'])) {
                $status['lead_status'] = 1;
                $status['lead_open_datetime'] = Null;
                if ($this->Leads_model->update($status, $data['lead_id'])) {
                    $activity['created_at'] = date('Y-m-d H:i:s');
                    $activity['created_by'] = $this->session->id;
                    $activity['lead_id'] = $data['lead_id'];
                    $activity['event'] = 'Entry Unassigned Lawyer';
					$activity['lawyer_id'] = $lawyer;
					/*Check activity if already exist then deleted*/
					$this->Central_model->del("activities", array("event" => $activity['event'],"lawyer_id" => $activity['lawyer_id']));
                    $insert_id = $this->Activity_model->save($activity);
                    $this->session->set_flashdata('message', '<p class="message_success">Unassigned lead successfully.</p>');
                    redirect($this->config->item('base_url') . 'admin/Leads/view/' . $data['lead_id']);
                } //$this->Leads_model->update($status, $data['lead_id'])
            } //$this->Leads_model->deleteLawyerToLead($data['lead_id'])
        } //$this->input->post('unassign_lawyer') == 1
    }

    public function addNotesLawyer($id)
    {
        $data = array();
        $lawyers_id = $this->session->userdata('id');
        $result = $this->Central_model->save('lead_notes', array('user_id' => $lawyers_id, 'lead_id' => $id, 'notes' => $this->input->post('notes'), 'date' => date("Y-m-d H:i:s"), 'role' => $this->session->userdata('role')));
        if ($result) {
            $this->session->set_flashdata('note_message', '<div class="message_success">Note Added Successfully.</div>');
        } //$update_notes
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
    }

    public function addNotesAdmin($id)
    {
        $lead_id = $this->uri->segment(4);
        $data = array();
        $user_id = $this->session->userdata('id');
        $date = date("m-d-Y H:i:s");
        $result = $this->Central_model->save('lead_notes', array('user_id' => $user_id, 'lead_id' => $id, 'notes' => $this->input->post('notes'), 'date' => date("Y-m-d H:i:s"), 'role' => $this->session->userdata('role')));
        if ($result) {
            $this->session->set_flashdata('note_message', '<div class="message_success m-notes">Note Added Successfully.</div>');
        }
		$this->load->library('user_agent');
        redirect($this->agent->referrer());
    }

    public function addAttemptsLawyers($id)
    {
        $data = array();
        if($this->session->userdata('role') == 2) {
			$lawyers_id = $this->session->userdata('id');
		} else if($this->session->userdata('role') == 6) {
			$row = $this->Central_model->first("lawyers_to_leads", "lead_id", $id);
			$lawyers_id = $row->lawyer_id;
		}
        $data['attempt_type'] = $this->input->post('attempt_type');
		if($this->session->role == '6'){
			$data['lawfirm_id'] = $this->session->id;
		}
        $data['lawyer_id'] = $lawyers_id;
        $data['lead_id'] = $id;
        $data['datetime'] = date('Y-m-d H:i:s');
        $data['action'] = $this->input->post('action_type');
        $insert_id = $this->Leads_model->insertAttemptsLawyers($data);
        if ($insert_id > 0) {
            $activity['created_at'] = date('Y-m-d H:i:s');
            $activity['lead_id'] = $id;
            $activity['event'] = $data['action'];
            $activity['via'] = $data['attempt_type'];
			if($this->session->role == '6'){
				$activity['lawfirm_id'] = $this->session->id;
			}
            $activity['lawyer_id'] = $lawyers_id;
            $insert_id = $this->Activity_model->save($activity);
            $this->session->set_flashdata('message', '<p class="message_success">Attempt added successfully.</p>');
            redirect($this->input->post('current_url'));
        } //$insert_id > 0
    }

    public function saveContactReached($id)
    {
        $data = array();
        $lawyers_id = $this->session->userdata('id');
        $date = str_replace(' - ', ' ', $this->input->post('contact_reached'));
        $data['contact_reached_at'] = date("Y-m-d H:i:s", strtotime($date));
        $data['datetime'] = date("Y-m-d H:i:s");
        $data['lawyer_id'] = $lawyers_id;
        $data['lead_id'] = $id;
        $data['action'] = $this->input->post('action_type');
        $insert_id = $this->Leads_model->insertAttemptsLawyers($data);
        if ($insert_id > 0) {
            $activity['created_at'] = date("Y-m-d H:i:s");
            $activity['lead_id'] = $id;
            $activity['event'] = $data['action'];
            $activity['lawyer_id'] = $lawyers_id;
            $insert_id = $this->Activity_model->save($activity);
            $this->session->set_flashdata('message', '<p class="message_success">Date saved successfully.</p>');
            redirect($this->input->post('current_url'));
        } //$insert_id > 0
    }

    public function action()
    {
        $data = array();
        $id = $this->input->post('lead_id');
		if($this->session->userdata('role') == 2) {
			$lawyers_id = $this->session->userdata('id');
		} else if($this->session->userdata('role') == 6) {
			$row = $this->Central_model->first("lawyers_to_leads", "lead_id", $id);
			$lawyers_id = $row->lawyer_id;
		}
        $data['lawyer_id'] = $lawyers_id;
        $data['lead_id'] = $id;
		$status = array();
        $data['action'] = $this->input->post('action');
        if ($data['action'] == "Lead Reached") {
            $lead_status = $this->input->post('lead_status');
            $data['lead_reached_type'] = $lead_status;
			$status['lead_status'] = '8';
			$this->Leads_model->updateStatus($status,$id);
			if($this->input->post('status') == 1) {
				if($data['lead_reached_type'] == 'Closed-No Show') {
					$leadDetail = $this->Leads_model->fetchLeadRow($data['lead_id']);
					$template = $this->Leads_model->template(9);
					$user = $this->Leads_model->fetchLawyerRow($lawyers_id);
					$status['lead_status'] = '6';
					$this->Leads_model->updateStatus($status,$id);
					$to = $user->email;
					$state_name = $this->Leads_model->fetchStateName($leadDetail->state);
					$subject = $template->subject;
					$subject = str_replace("lead_id", $leadDetail->id, $subject);
					$subject = str_replace("CA", $state_name->state_short_name, $subject);
					$message = $template->content;
					$message = str_replace("{lead reference}", "<a href='" . base_url() . "admin/leads/view/" . $id . "'>" . base_url() . "Closed_Lead</a>", $message);
					//echo $message; exit;
					$message = str_replace("{name}", $user->name, $message);
					$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
					$message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/hl_logo_em.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
					$from = $template->email_from;
					$name = $this->config->item('project_title');	
					$to = $user->email;
					send_email($to, $from, $name, $subject, $message);
					/*Send SMS*/
					$sms_message = $template->text_message;
					$sms_message = str_replace("{case_number}", $leadDetail->lead_id_number, $sms_message);
					sms($user->is_sms, $user->phone, $sms_message);
				} 
			} 
            /*For lead_contact_attempts*/
            if ($this->input->post('status') == 1) {
                $this->Central_model->save('lead_contact_attempts', $data);
                /*For Activities*/
                $activity['event'] = $lead_status;
                $activity['created_at'] = date("Y-m-d H:i:s");
                $activity['lead_id'] = $id;
				if($this->session->role == '6'){
					$activity['lawfirm_id'] = $this->session->id;
				}
                $activity['lawyer_id'] = $lawyers_id;
                $insert_id = $this->Central_model->save('activities', $activity);
            } else {
				$status['lead_status'] = '5';
				$this->Leads_model->updateStatus($status,$id);
                $this->Central_model->del("lead_contact_attempts", array("lead_id" => $id,"action" => $data['action'], "lead_reached_type" => $data['lead_reached_type'], "lawyer_id" => $lawyers_id));
                $this->Central_model->del("activities", array("lead_id" => $id,"event" => $data['lead_reached_type'], "lawyer_id" => $lawyers_id));
            }
        } else if ($data['action'] == "Consultation Scheduled") {
            /*For lead_contact_attempts*/
            if ($this->input->post('status') == 1) {
                $this->Central_model->save('lead_contact_attempts', $data);
                /*For Activities*/
                $activity['created_at'] = date("Y-m-d H:i:s");
                $activity['lead_id'] = $id;
				if($this->session->role == '6'){
					$activity['lawfirm_id'] = $this->session->id;
				}
                $activity['lawyer_id'] = $lawyers_id;
                $activity['event'] = $data['action'];
                $insert_id = $this->Central_model->save('activities', $activity);
            } else {
                $this->Central_model->del('lead_contact_attempts', array('lead_id' => $id,"action" => $data['action'], "lawyer_id" => $lawyers_id));
                $this->Central_model->del("activities", array("lead_id" => $id,"event" => $data['action'], "lawyer_id" => $lawyers_id));
            }
        } else if ($data['action'] == "Consultation complete") {
            $data['lead_status'] = ($this->input->post('status') == 1 ? 3 : 5); 
            if ($data['lead_status'] == 3) {
                $data['lead_complete_time'] = date('Y-m-d');
                $leadDetail = $this->Leads_model->fetchLeadRow($data['lead_id']);
                $template = $this->Leads_model->template(10);
                $user = $this->Leads_model->fetchLawyerRow($lawyers_id);
                $state_name = $this->Leads_model->fetchStateName($leadDetail->state);
				$activity['created_at'] = date("Y-m-d H:i:s");
                $activity['lead_id'] = $id;
				if($this->session->role == '6'){
					$activity['lawfirm_id'] = $this->session->id;
				}
                $activity['lawyer_id'] = $lawyers_id;
                $activity['event'] = $data['action'];
				$insert_id = $this->Central_model->save('activities', $activity);
				$this->Central_model->save('lead_contact_attempts', $data);
				$subject = $template->subject;
                $subject = str_replace("{Ref}", "{ Ref." . $id . " }", $subject);
                $subject = str_replace("CA", $state_name->state_short_name, $subject);
                $subject = str_replace("{Area of Law}", $leadDetail->case_type, $subject);
                $message = htmlspecialchars_decode($template->content);
                $message = str_replace("{name}", $user->name, $message);
                $message = str_replace("{lead_reference}", $leadDetail->lead_id_number, $message);
                $message = str_replace("{lead id}", $leadDetail->lead_id_number, $message);
                $message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/hl_logo_em.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
                $message = str_replace("{project_title}", $this->config->item('project_title'), $message);
                $from = $template->email_from;
                $name = $this->config->item('project_title');
                $to = $user->email;
                send_email($to, $from, $name, $subject, $message);
				/*Send SMS*/
				$sms_message = $template->text_message;
				$sms_message = str_replace("{case_number}", $leadDetail->lead_id_number, $sms_message);
				sms($user->is_sms, $user->phone, $sms_message);
            } else if($data['lead_status'] == 5) {
				$this->Central_model->del('lead_contact_attempts', array('lead_id' => $id,"action" => $data['action'], "lawyer_id" => $lawyers_id));
				$this->Central_model->del("activities", array("lead_id" => $id,"event" => $data['action'], "lawyer_id" => $lawyers_id));
			}
            return $this->Central_model->update("leads", array('lead_status' => $data['lead_status'], 'lead_complete_time' => date('Y-m-d')), 'id', $id);
        } else if ($data['action'] == "Lead Retained") {
            $lead_reached_status = $this->input->post('action');
            $data['action'] = $lead_reached_status;
			//$status['lead_status'] = '7';
			//$this->Leads_model->updateStatus($status,$id);
            if ($this->input->post('status') == 1) {
                $this->Central_model->save('lead_contact_attempts', $data);
                /*For Activities*/
                $activity['created_at'] = date("Y-m-d H:i:s");
                $activity['lead_id'] = $id;
				if($this->session->role == '6'){
					$activity['lawfirm_id'] = $this->session->id;
				}
                $activity['lawyer_id'] = $lawyers_id;
                $activity['event'] = $lead_reached_status;
                $insert_id = $this->Central_model->save('activities', $activity);
            } else {
				//$status['lead_status'] = '5';
				//$this->Leads_model->updateStatus($status,$id);
                $this->Central_model->del('lead_contact_attempts', array('lead_id' => $id,"action" => $data['action'], "lawyer_id" => $lawyers_id));
                $this->Central_model->del("activities", array("lead_id" => $id, "event" => $data['action'], "lawyer_id" => $lawyers_id));
            }
			/*For Lead Complete*/
			$data['lead_status'] = ($this->input->post('status') == 1 ? 3 : 5); 
			if ($data['lead_status'] == 3) {
                $data['lead_complete_time'] = date('Y-m-d');
                $leadDetail = $this->Leads_model->fetchLeadRow($data['lead_id']);
                $template = $this->Leads_model->template(10);
                $user = $this->Leads_model->fetchLawyerRow($lawyers_id);
                $state_name = $this->Leads_model->fetchStateName($leadDetail->state);
				$subject = $template->subject;
                $subject = str_replace("{Ref}", "{ Ref." . $id . " }", $subject);
                $subject = str_replace("CA", $state_name->state_short_name, $subject);
                $subject = str_replace("{Area of Law}", $leadDetail->case_type, $subject);
                $message = htmlspecialchars_decode($template->content);
                $message = str_replace("{name}", $user->name, $message);
                $message = str_replace("{lead_reference}", $leadDetail->lead_id_number, $message);
                $message = str_replace("{lead id}", $leadDetail->lead_id_number, $message);
                $message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/hl_logo_em.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
                $message = str_replace("{project_title}", $this->config->item('project_title'), $message);
                $from = $template->email_from;
                $name = $this->config->item('project_title');
                $to = $user->email;
                send_email($to, $from, $name, $subject, $message);
				/*Send SMS*/
				$sms_message = $template->text_message;
				$sms_message = str_replace("{case_number}", $leadDetail->lead_id_number, $sms_message);
				sms($user->is_sms, $user->phone, $sms_message);
            }
			return $this->Central_model->update("leads", array('lead_status' => $data['lead_status'], 'lead_complete_time' => date('Y-m-d')), 'id', $id);
        } else if ($data['action'] == "Lead Not Retained" || $data['action'] == "Contact Reached" || $data['action'] == "Contact Attempted" || $data['action'] == "Declined") {
            $lead_reached_status = $this->input->post('action');
            $data['action'] = $lead_reached_status;
			$status['lead_status'] = '6';
			$this->Leads_model->updateStatus($status,$id);
            if ($this->input->post('status') == 0) {
				$status['lead_status'] = '5';
				$this->Leads_model->updateStatus($status,$id);
                $this->Central_model->del('lead_contact_attempts', array('lead_id' => $id, "action" => $data['action'], "lawyer_id" => $lawyers_id));
                $this->Central_model->del("activities", array("lead_id" => $id,"event" => $data['action'], "lawyer_id" => $lawyers_id));
            }
        }
    }

    public function save_action()
    {
        $data = array();
		$id = $this->input->post('lead_id');
		if($this->session->userdata('role') == 2) {
			$lawyers_id = $this->session->userdata('id');
		} else if($this->session->userdata('role') == 6) {
			$row = $this->Central_model->first("lawyers_to_leads", "lead_id", $id);
			$lawyers_id = $row->lawyer_id;
		}
        $unique_code = $this->input->post('unique_code');
		if($this->session->role == '6'){
			$data['lawfirm_id'] = $this->session->id;
		}
		$data['lawyer_id'] = $lawyers_id;
        $data['action'] = $this->input->post('action_type');
		$this->Central_model->update("leads",array("lead_status" => $this->input->post('status')),'id',id);
        $data['lead_id'] = $id;
        $data['lead_not_retained_reason'] = $this->input->post('lead_not_retained_reason');
        $data['reason'] = $this->input->post('reason');
        $activity['created_at'] = date("Y-m-d H:i:s");
        $activity['lead_id'] = $id;
        $activity['event'] = $data['action']; 
		if($this->session->role == '6'){
			$activity['lawfirm_id'] = $this->session->id;
		}
        $activity['lawyer_id'] = $lawyers_id; 
		$this->Central_model->save('lead_contact_attempts', $data);
		$insert_id = $this->Central_model->save('activities', $activity);
		if($data['action'] == "Declined") {
			if ($this->Leads_model->deleteLawyerToLead($data['lead_id'])) {
				$template = $this->Leads_model->template(12);
				$lead = $this->Leads_model->fetchLeadDetail($data['lead_id']);
				$super_admin = $this->Leads_model->fetchSuperAdminRecord();
				$subject = $template->subject;
				$to = $super_admin->superAdminEmail;
				$from = $template->email_from;
				$name = $this->config->item('project_title');
				$case = getCaseTypeName($lead->case_type);
				$message = $template->content;
				$message = str_replace("{name}", $lead->first_name.' '.$lead->last_name, $message);
				$message = str_replace("{project_title}", $this->config->item('project_title'), $message);
				$message = str_replace("{rootpath}", base_url(), $message);
				$message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/hl_logo_em.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
				$message = str_replace("{case_id}", $lead->lead_id_number, $message);
				$message = str_replace("{referral_date}", date('m-d-Y h:i:s A'), $message);
				$message = str_replace("{phone}", $lead->mobile, $message);
				$message = str_replace("{email}", $lead->email, $message);
				$message = str_replace("{description}", $lead->case_description, $message);
				$message = str_replace("{jurisdiction}", getJurisdictionNamee($lead->jurisdiction), $message);
				$message = str_replace("{case_type}", $case[0]['type'], $message);
				$message = str_replace("{location}", $lead->street, $message);
				$message = $this->load->view('admin/leads/emails/new_lead_template',array("message" => $message), true);
				send_email($to, $from, $name, $subject, $message);
				/*Send SMS*/
				$sms_message = $template->text_message;
				$sms_message = str_replace("{case_number}", $lead->lead_id_number, $sms_message);
				$sms_message = str_replace("{project_title}", $this->config->item('project_title'), $sms_message);
				sms($super_admin->is_sms, $super_admin->superAdminPhone, $sms_message);
				redirect($this->config->item('base_url') . 'admin/dashboard/manage');
			}
		} 
        redirect($this->config->item('base_url') . 'admin/Leads/view/' . $id.'/'.$unique_code);
    }

    public function changeLeadReachedStatusComplete()
    {
        $data = array();
        $lawyers_id = $this->session->userdata('id');
        $id = $this->input->post('lead_id');
        $lead_reached_status = $this->input->post('lead_reached_status');
		if($this->session->role == '6'){
			$data['lawfirm_id'] = $this->session->id;
		}
        $data['lawyer_id'] = $lawyers_id;
        $data['lead_id'] = $id;
        $data['action'] = 'Lead Reached';
        $data['lead_reached_type'] = $lead_reached_status;
        if ($data['lead_reached_type'] == 'Closed-No Show') {
            $leadDetail = $this->Leads_model->fetchLeadRow($data['lead_id']);
            $template = $this->Leads_model->template(9);
            $user = $this->Leads_model->fetchLawyerRow($lawyers_id);
            $to = $user->email;
            $state_name = $this->Leads_model->fetchStateName($leadDetail->state);
            $subject = $template->subject;
            $subject = str_replace("ref", $leadDetail->id, $subject);
            $subject = str_replace("CA", $state_name->state_short_name, $subject);
            $message = $template->content;
            $message = str_replace("lead reference", "<a href='" . base_url() . "admin/leads/view/" . $id . "'>" . base_url() . "Completed_Lead</a>", $message);
            $message = str_replace("{name}", $user->name, $message);
            $message = str_replace("{project_title}", $this->config->item('project_title'), $message);
            $message = str_replace("{image}", '<img src="'.base_url().'assets/admin/images/pride_legal_image.png" class="fr-fin fr-tag" data-pin-nopin="true">', $message);
            $from = $template->email_from;
            $name = $this->config->item('project_title');;
            $to = $user->email;
            send_email($to, $from, $name, $subject, $message);
			/*Send SMS*/
			$sms_message = $template->text_message;
			$sms_message = str_replace("{case_number}", $leadDetail->lead_id_number, $sms_message);
			sms($user->is_sms, $user->phone, $sms_message);
        } //$data['lead_reached_type'] == 'Closed-No Show'
        $lead_status = $this->Leads_model->checkLeadReached($id, $lawyers_id);
        if (!$lead_status) {
            $activity['created_at'] = date("Y-m-d H:i:s");
            $activity['lead_id'] = $id;
            $activity['event'] = $lead_reached_status;
            $insert_id = $this->Activity_model->save($activity);
            return $this->Leads_model->insertAttemptsLawyers($data);
        } //!$lead_status
        else {
            $activity['created_at'] = date("Y-m-d H:i:s");
            $activity['lead_id'] = $id;
            $activity['event'] = $lead_reached_status;
            $insert_id = $this->Activity_model->save($activity);
            return $this->Leads_model->updateAttemptsLawyers($data, $lead_status->id);
        }
    }

    public function updateLawyerToLeadBilled()
    {
        //echo 'function start';exit();
        $billed_lawyer_id = $this->input->post('billed_lawyer_id');
        $lawyerToLeadData['billed'] = 'yes';

        $lawyer_to_lead_detail = $this->Central_model->select_array('lawyers_to_leads', array('id' => $billed_lawyer_id));
        $lead_id = $lawyer_to_lead_detail->lead_id;
        $lead_detail = $this->Central_model->select_array('leads', array('id' => $lead_id));
        //echo 'here '.$lead_detail->lead_open_datetime;exit();
        /*if ($lead_detail->lead_open_datetime == '' || $lead_detail->lead_open_datetime == null || $lead_detail->lead_open_datetime == '0000-00-00 00:00:00')
        {*/
        if ($lead_detail->lead_status == 2)
        {
            $LeadData['lead_open_datetime'] = date('Y-m-d H:i:s');
            $LeadData['lead_status'] = 5;
            $this->Central_model->update_row('leads', $LeadData, array('id' => $lead_id));
        }
        //}
        $this->Central_model->update_row('lawyers_to_leads', $lawyerToLeadData, array('id' => $billed_lawyer_id));
    }

    public function duplicateLead()
    {
        $case_type = $this->input->post('case_type');
        $lead_id = $this->input->post('dup_lead_id');
        $lead_data = (array)$this->Central_model->first('leads', 'id', $lead_id);
        unset($lead_data['id']);
        $lead_data['case_type'] = $case_type;
        $lead_data['lead_status'] = 1;

        $state = $this->Leads_model->fetchStateName($lead_data['state']);
        $market = $this->Leads_model->fetchMarketName($lead_data['market']);
        $case_type = $this->Leads_model->fetchCasetypeName($lead_data['case_type']);
        $market_two_character = substr($market->market_name, 0, 2);
        $random_number = substr(str_shuffle(str_repeat("0123456789", 5)), 0, 5);
        $sp_rep_sk = array(
            "@" => "",
            "(" => "",
            ")" => "",
            "<>" => "",
            "'" => "",
            "-" => ""
        );
        $market_two_character = strtr($market_two_character, $sp_rep_sk);
        if (strlen($market_two_character) == '1') {
            $market_two_character = str_pad($market_two_character, 2, "0", STR_PAD_LEFT);
        } //strlen($market_two_character) == '1'
        $case_type_two_character = str_pad($lead_data['case_type'], 2, "0", STR_PAD_LEFT);
        $lead_data['lead_id_number'] = $state->state_short_name . "-" . $market_two_character . "-" . $case_type_two_character . "-" . $random_number;

        $saved_lead_id = $this->Central_model->save('leads', $lead_data);
        $super_admin = $this->Leads_model->fetchSuperAdminRecord();
        $template = $this->Leads_model->template(5);
        if ($saved_lead_id) {
            $lead_detail = $this->Leads_model->fetchLeadDetail($saved_lead_id);
            $state_name = $this->Leads_model->fetchStateName($lead_detail->state);
            $subject = $template->subject;
            $subject = str_replace("{ref}", "ref_no: " . $lead_detail->id, $subject);
            $subject = str_replace("{Area of Law}", $lead_detail->case_type, $subject);
            $subject = str_replace("CA", $state_name->state_short_name, $subject);
            $name = $this->config->item('project_title');
            $caseTypeName = getCaseTypeName($lead_detail->case_type);
            $content['lead_detail'] = $lead_detail;
            $content['caseTypeName'] = $caseTypeName;
            $message = $this->load->view('admin/leads/emails/new_lead_to_admin', $content, true);
            send_email($super_admin->superAdminEmail, 'support@pridelegal.com', $name, $subject, $message);
			/*Send SMS*/
			$sms_message = $template->text_message;
			$sms_message = str_replace("{area_of_law}", $caseTypeName[0]['type'], $sms_message);
			sms($super_admin->is_sms, $super_admin->superAdminPhone, $sms_message);
		    $activity['created_at'] = date('Y-m-d H:i:s');
            $activity['lead_id'] = $saved_lead_id;
            $activity['event'] = 'Entry Duplicated';
            $insert_id = $this->Activity_model->save($activity);
        } //$insert_id

        if ($saved_lead_id) {
            $response['status'] = true;
            $response['lead_id_number'] = $lead_data['lead_id_number'];
            $response['message'] = '<p class="message_success">Lead duplicated successfully. Lead ID No. is ' . $lead_data['lead_id_number'] . '.<br>Please click <a href="' . base_url('admin/leads/view/' . $saved_lead_id) . '">here</a> to view lead details.</p>';
        }else{
            $response['status'] = false;
            $response['lead_id_number'] = "";
            $response['message'] = '<p>Lead failed to be duplicated. Please try again.</p>';
        }
        echo json_encode($response);
        exit();

    }
}
