<?php if($this->session->userdata('logged_in')){
	$role = $this->session->userdata('role');
	$statusArray = array(3, 12, 20);
}
?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Lead View</h2>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Lead View</h3>
					</div>
					<div class="porlets-content two-col">
						<div class="row">
							<div class="col col-md-6">
								<div class="row">
									<div class="col-md-6 subcol">
										<label>First name:</label>
										<span class="desp"><?php echo $lead->first_name; ?>&nbsp;</span>
									</div>
									<div class="col-md-6 subcol">
										<label>Last name:</label>
										<span class="desp"><?php echo $lead->last_name; ?>&nbsp;</span>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-6 subcol">
										<label>Mobile no:</label>
										<span class="desp"><?php echo $lead->mobile; ?>&nbsp;</span>
									</div>
									<div class="col-md-6 subcol">
										<label>Email:</label>
										<span class="desp"><?php echo $lead->email; ?>&nbsp;</span>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-6 subcol">
										<label>Caller name:</label>
										<span class="desp"><?php echo $lead->caller_name; ?>&nbsp;</span>
									</div>
									<div class="col-md-6 subcol">
										<label>Caller ID:</label>
										<span class="desp"><?php echo $lead->caller_id; ?>&nbsp;</span>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-6 subcol">
										<label>Home phone:</label>
										<span class="desp"><?php echo $lead->home_phone; ?>&nbsp;</span>
									</div>
									<div class="col-md-6 subcol">
										<label>Work phone:</label>
										<span class="desp"><?php echo $lead->work_phone; ?>&nbsp;</span>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-12 subcol">
										<label>Street:</label>
										<span class="desp"><?php echo $lead->street; ?>&nbsp;</span>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-6 subcol">
										<label>City:</label>
										<span class="desp"><?php echo $lead->city; ?>&nbsp;</span>
									</div>
									<div class="col-md-6 subcol">
										<label>State:</label>
										<span class="desp"><?php echo getStateShortName($lead->state); ?>&nbsp;</span>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-6 subcol">
										<label>Zip:</label>
										<span class="desp"><?php echo $lead->zip; ?>&nbsp;</span>
									</div>
									<div class="col-md-6 subcol">
										<label>Jurisdiction:</label>
										<span class="desp">
											<?php  $newJurisdiction = getJurisdictionName($lead->jurisdiction); echo $newJurisdiction[0]['name'];
											?>
										</span>
									</div>
									<div class="col-md-6 subcol">
										<label>Market:</label>
										<span class="desp">
											<?php
											$market_name =  getMarketName($lead->market);
											$stateName =  getStateName($market_name->state);
											$stateName[0]['state_short_name'];
											echo $market_name->market_name  . "-" . $stateName[0]['state_short_name'];
											?>
										</span>
									</div>
									<div class="col-md-6 subcol">
										<label>Case ID:</label>
										<span class="desp"><?php echo $lead->lead_id_number;?>&nbsp;</span>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-6 subcol">
										<label>Case type:</label>
										<span class="desp"><?php $caseTypeName =  getCaseTypeName($lead->case_type); echo $caseTypeName[0]['type']; ?>&nbsp;</span>
									</div>
									<div class="col-md-6 subcol">
										<label>Lead price:</label>
										<span class="desp">$<?php echo number_format($lead->price, 2);?>&nbsp; </span>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-12 subcol">
										<label>Legal issue:</label>
										<span class="desp"><?php echo $lead->case_description;?>&nbsp;</span>
									</div>
									<div class="col-md-12 subcol">
										<label>Date/time:</label>
										<span class="desp">
											<?php
											$newDate = getAssignDate($lead->id);
											$new_date = date('m-d-Y h:i:s A', strtotime($newDate[0]['assigned_date']));
											?>
											<?php echo $new_date; ?>&nbsp;
										</span>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-12 subcol">
										<label>Intake notes:</label>
										<span class="spanclass desp">
											<?php echo $lead->my_notes; ?>
										</span>
									</div>
								</div>
							</div>
							<div class="col col-md-6">
								<div class="form-group">
									<h3>Actions</h3>
									<div class="textFieldArea-2">
										<?php $contact_attempts = contact_attempts($lead->id, $lawyer_id); 
										?>
										<label id="attempts" class="checkbox-inline">
											<!--<input type="checkbox" attr-action="Contact Attempted" class="contact_attempts" id="inlinecheckbox1" <?php if(!empty($contact_attempts)) { ?> checked="checked" <?php }?>>
												<span class="custom-checkbox"></span>--> Contact attempts : <?php if(!empty($contact_attempts)){ echo sizeof($contact_attempts); } else { echo '0'; }?> times
											</label>
											<?php
											if(!empty($contact_attempts))
											{
												$i = 1;
												foreach($contact_attempts as $attempt)
												{
													if($attempt->law_firm !== null){
														$name = $attempt->law_firm;
													}else if($attempt->staff_first_name !== null && $attempt->staff_last_name !== null){
														$name = $attempt->staff_first_name . ' ' . $attempt->staff_last_name;
													}else if($attempt->admin_first_name !== null && $attempt->admin_last_name !== null){
														$name = $attempt->admin_first_name . ' ' . $attempt->admin_last_name . " (Admin)"; 
													}else{
														$name = $attempt->lawyer_first_name . ' ' . $attempt->lawyer_last_name;
													}
													?>
													<label>Attempt <?php echo $i;?>: <?php echo date('m-d-Y h:i:s A', strtotime($attempt->datetime)) . ' (' . $attempt->attempt_type . ') by ' . $name; ?></label>
												<!--<label class="checkbox-inline">
												<input type="checkbox" <?php if($attempt->attempt_type == "Phone"){?> checked="checked" <?php echo 'disabled';}else{ echo 'disabled';}?> id="inlinecheckbox">
												<span class="custom-checkbox"></span> Phone  </label>
												<label class="checkbox-inline">
												<input type="checkbox" <?php if($attempt->attempt_type == "Email"){?> checked="checked" <?php echo 'disabled';}else{ echo 'disabled';}?> id="inlinecheckbox1">
												<span class="custom-checkbox"></span> Email </label>-->
												<?php $i++;
											}
										}
										if($lead->lead_status != '3' && !(in_array($lead->lead_status, $statusArray))) { ?>
										<?php if($this->session->view_as == ''){ ?>
										<div class="panel panel-default">
											<div class="panel-heading warning">
												<h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#ac4-2"> <i class="fa fa-plus"></i> Add more attempt entries </a> </h4>
											</div>
											<div id="ac4-2" class="panel-collapse collapse">
												<div class="panel-body">
													<form action="<?php echo base_url();?>admin/leads/addAttemptsLawyers/<?php echo $id;?>" method="post" class="l-actions" parsley-validate novalidate>
														<input type="hidden" value="Contact Attempted" name="action_type">
														<div style="display: inline-block; width: auto;" class="radio radioWithError">
															<label style="display: inline-block;">
																<input type="radio" id="optionsRadios1" value="Phone" name="attempt_type" required>
																<span class="custom-radio"></span> Phone  </label>
															</div>
															<div style="display: inline-block; width: auto;" class="radio radioWithError">
																<label style="display: inline-block;">
																	<input type="radio" id="optionsRadios1" value="Email" name="attempt_type" required>
																	<span class="custom-radio"></span> Email </label><br />
																</div>
																<div class="row">
																	<div class="form-group col-md-8">
																		<label>Contact Date/Time</label>
																		<input type="text" name="created_at" id="time-holder" parsley-trigger="change" placeholder="Enter Contact Date/Time" class="form-control form_datetime-adv datepicker-here" data-date-format="mm-dd-yyyy" data-language="en" data-timepicker="true" required>
																	</div>
																	<div class="form-group col-md-4">
																		<label>Add Date</label>
																		<button class="btn btn-primary" type="button" value="time" name="timer" id="time">Now</button>
																	</div>
																</div>
																<input type="hidden" class="current_url" name="current_url" value="<?php echo current_url(); ?>"/>
																<button class="btn btn-primary" type="submit">Submit</button>
															</form>
														</div>
													</div>
												</div>
												<?php } ?>
												<?php } ?>
										<!--<?php $contact_reached = contact_reached($lead->id, $lawyer_id); ?>
										<label class="checkbox-inline">
											<?php if(!empty($contact_reached)) { ?>
												<input type="checkbox" attr-action="Contact Reached" attr-action="Contact Reached" id="inlinecheckbox1" class="contact_reached_check" <?php if($contact_reached->contact_reached_at != '0000-00-00 00:00:00'){?> checked="checked" <?php }?>>
												<span class="custom-checkbox"></span> Contact reached <?php if($contact_reached->contact_reached_at != '0000-00-00 00:00:00'){?>on <?php echo date("m-d-Y h:i:s A", strtotime($contact_reached->contact_reached_at)); }?>
											<?php } else { ?>
												<input type="checkbox" id="inlinecheckbox1" class="contact_reached_check" >
												<span class="custom-checkbox"></span> Contact reached
											<?php }?>
										</label>
										<div class="form-group col-md-8 contact_reached_div" style="display:none;">
											<form action="<?php echo base_url();?>admin/leads/saveContactReached/<?php echo $id;?>" method="post" parsley-validate novalidate>
												<input type="hidden" value="Contact Reached" name="action_type">
												<label>Date *</label>
												<input type="text" name="contact_reached" class="form-control datepicker-here" data-time-format="hh:ii aa" data-timepicker ="true" data-language="en" required><br />
												<input type="hidden" class="current_url" name="current_url" value="<?php echo current_url(); ?>"/>
												<button class="btn btn-primary" type="submit">Submit</button>
											</form>
										</div>
										<div class="clearfix"></div>
										<?php
										$count_rows = admin_lead_reached($lead->id, "Lead Reached", "Lead Reached (non-communication)", $lawyer_id);?>
										<label class="checkbox-inline">
											<input type="checkbox" value="Lead Reached (non-communication)" name="" id="inlinecheckbox1" attr-action="Lead Reached" class="action" <?php if($count_rows > 0){?> checked="checked" <?php } ?> >
											<span class="custom-checkbox"></span> Lead reached (non-communication)
										</label>
										<?php $count_rows = admin_lead_reached($lead->id, "Lead Reached", "Closed-No Show", $lawyer_id); ?>
										<label class="checkbox-inline">
											<input type="checkbox" value="Closed-No Show" name="" id="inlinecheckbox1" attr-action="Lead Reached" class="action"  <?php if($count_rows > 0){ ?> checked="checked" <?php } ?>>
											<span class="custom-checkbox"></span> Closed-no show
										</label>-->
										<?php $count_rows = admin_lead_reached($lead->id, "Lead Reached", "Lead Reached (communication)", $lawyer_id); ?>
										<label class="checkbox-inline">
											<input type="checkbox" value="Lead Reached (communication)" name="" id="inlinecheckbox1" attr-action="Lead Reached" class="action" <?php if($count_rows > 0) {?> checked="checked" <?php } ?><?php echo ($this->session->view_as == '' && !(in_array($lead->lead_status, $statusArray)) ? 'enabled' : 'disabled'); ?>>
											<span class="custom-checkbox"></span> Lead reached (communication)
										</label>
										<!--<?php $count_rows = admin_lead_scheduled($lead->id, "Consultation Scheduled", $lawyer_id); ?>
										<label class="checkbox-inline">
											<input type="checkbox" attr-action="Consultation Scheduled" name="" id="inlinecheckbox1" class="action" <?php if($count_rows > 0) { ?> checked="checked" <?php } ?>>
											<span class="custom-checkbox"></span> Consultation scheduled
										</label>									
										<?php $count_rows = admin_lead_consultation_completed($lead->id, "Consultation complete", $lawyer_id); ?>
										<label class="checkbox-inline">
											<input type="checkbox" value="<?php echo $id;?>" attr-action="Consultation complete" id="inlinecheckbox1" class="cons_complete action" <?php if($count_rows >0){?> checked="checked"<?php }?>>
											<span class="custom-checkbox"></span> Consultation complete (Lead will be marked as complete)
										</label>-->	
										<?php $count_rows = admin_lead_retained($lead->id, "Lead Retained", $lawyer_id); ?>
										<label class="checkbox-inline">
											<input type="checkbox" attr-action="Lead Retained" id="inlinecheckbox1" class="action" <?php if($count_rows > 0){ ?> checked="checked" <?php } ?><?php echo ($this->session->view_as == '' && !(in_array($lead->lead_status, $statusArray)) ? 'enabled' : 'disabled'); ?>>
											<span class="custom-checkbox"></span> Lead retained (completed)
										</label>
										<?php $count_rows = admin_lead_retained($lead->id, "Lead Not Retained", $lawyer_id); ?>
										<label class="checkbox-inline">
											<input type="checkbox" attr-action="Lead Not Retained" name="" id="inlinecheckbox1" class="lead_not_retained_status" <?php if($count_rows > 0) { ?> checked="checked" <?php } ?><?php echo ($this->session->view_as == '' && !(in_array($lead->lead_status, $statusArray)) ? 'enabled' : 'disabled'); ?>>
											<span class="custom-checkbox"></span> Lead not retained
										</label>
										<?php $lead_retained_reason = lead_retained_reason($lead->id, "Lead Not Retained", $lawyer_id); ?>
										<?php if($lead_retained_reason > 0) { ?>
										<div class="form-group col-md-8">
											<label>Reason: </label>
											<input type="text" name="lead_not_retained_reason" class="form-control" value="<?php echo $lead_retained_reason->lead_not_retained_reason;?>" disabled>
										</div>
										<?php } else { ?>
										<div class="form-group col-md-8 lead_retained_div" style="display:none;">
											<form action="<?php echo base_url();?>admin/Leads/save_action" method="post" parsley-validate novalidate>
												<input type="hidden" name="lead_id" value="<?php echo $id;?>" />
												<input type="hidden" name="unique_code" value="<?php echo $this->uri->segment(5); ?>"/>
												<input type="hidden" value="Lead Not Retained" name="action_type">
												<label>Reason: </label>
												<input type="text" name="lead_not_retained_reason" class="form-control" value="">
												<input type="hidden" name="status" value="6"><br>
												<button class="btn btn-primary" type="submit">Submit</button>
											</form>
										</div>
										<?php } ?>
										<div class="clearfix"></div>
										<?php $lead_declined = lead_declined($lead->id, "Declined",  $lawyer_id); ?>
										<label class="checkbox-inline">
											<input type="checkbox" attr-action="Declined" name="" id="inlinecheckbox1" class="lead_declined_status" <?php if($lead_declined) { ?> checked="checked" <?php } ?><?php echo ($this->session->view_as == '' && !(in_array($lead->lead_status, $statusArray)) ? 'enabled' : 'disabled'); ?>>
											<span class="custom-checkbox"></span> Refer to another/declined
										</label>
										<?php if($lead_declined) { ?>
										<div class="form-group col-md-8">
											<label>Reason: </label>
											<input type="text" name="reason" class="form-control" value="<?php echo $lead_declined->reason;?>" disabled>
										</div>
										<?php } else { ?>
										<div class="form-group col-md-8 lead_declined_div" style="display:none;">
											<form action="<?php echo base_url();?>admin/Leads/save_action" method="post" parsley-validate>
												<input type="hidden" name="lead_id" value="<?php echo $id;?>" />
												<input type="hidden" name="unique_code" value="<?php echo $this->uri->segment(5); ?>"/>
												<input type="hidden" value="Declined" name="action_type">
												<label>Reason: </label>
                                                <select name="reason" class="form-control" id="declined_reason">
                                                    <option value="Not my Area of Law">Not my Area of Law</option>
                                                    <option value="Not my Jurisdiction">Not my Jurisdiction</option>
                                                    <option value="">Other</option>
                                                </select>
                                                <div id="mand_field"></div>
												<input type="hidden" name="status" value="2"><br>
												<button class="btn btn-primary" type="submit">Submit</button>
											</form>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="row col-sm-12">
								<?php 
								if($role == 6 || $role == 2 || $role == 7)
									{ ?>							
										<div class="form-group col-md-6">								
											<h3>Activities</h3>								
											<div class="textFieldArea">								
												<?php if(!empty($activities)) 
												{
													$action_array = array();
													foreach($activities as $activity)
													{		
														if($activity->law_firm !== null){
															$name = $activity->law_firm;
														}else if($activity->staff_first_name !== null && $activity->staff_last_name !== null){
															$name = $activity->staff_first_name . ' ' . $activity->staff_last_name;
														}else if($activity->admin_first_name !== null && $activity->admin_last_name !== null){
															$in_array = array(
																"Reminder Email Sent",
																"Contact Attempted",
																"Lead Reached (communication)",
																"Lead Retained",
																"Lead Not Retained",
																"Declined",
															);
															$name = $activity->admin_first_name . ' ' . $activity->admin_last_name;
															if(in_array($activity->event, $in_array)) $name .= " (Admin)";
														}else{
															$name = $activity->lawyer_first_name . ' ' . $activity->lawyer_last_name;
														}
														if($activity->event != "Entry Created" && $activity->event != "Entry Duplicated" && $activity->event != "Entry Unassigned Lawyer" && $activity->event !== 'Lead Deleted' && $activity->event !== 'Lead Un-Deleted')	{
															
															if($activity->event == "Entry Reassigned Lawyer" || $activity->event == "Entry Assigned Lawyer")	
															{
																$act = $activity->event . " (".$name.")";
															} else {
																$act = $activity->event;
															}
															$reason = getDeclinedReason($activity->lead_id, $activity->lawyer_id);
															if (!in_array($act, $action_array)) {
																echo '<div class="note-single">';
																if($activity->event !== 'Contact Attempted' && $activity->event !== 'Entry Reassigned Lawyer' && $activity->event !== "Entry Assigned Lawyer"){
																	echo $act.' at '. date("m-d-Y h:i:s A", strtotime($activity->created_at)) . ' by ' .$name;
																	echo $reason->reason != '' && $activity->event == 'Declined' ? '<br/>' . 'Reason: ' . $reason->reason : '';
																}else if($activity->event == 'Contact Attempted'){
																	echo $act.' at '. date("m-d-Y h:i:s A", strtotime($activity->created_at));
																	if($activity->via) echo ' via '.$activity->via . ' by ' . $name;
																}else{
																	echo $act.' at '. date("m-d-Y h:i:s A", strtotime($activity->created_at));
																}									
																echo '</div>';
															}
															/* Comment by Hassan. It prevents same activity event to display twice. Which was removed by client. */
													//array_push($action_array, $act.$activity->via);
														}							
													}		
												} ?>								
											</div>		
										</div> 
										<?php } ?>
										<div class="col-md-6 l-notes">
											<?php if($this->session->flashdata('note_message')) {
												echo $this->session->flashdata('note_message');
											} ?>
											<?php if($this->session->view_as == '' && !(in_array($lead->lead_status, $statusArray))){ ?>
											<?php if($role == 2 || $role == 7) { ?>
											<form action="<?php echo base_url();?>admin/leads/addNotesLawyer/<?php echo $id;?>" method="post" parsley-validate novalidate>
												<h3>Lawyer notes</h3>
												<textarea rows="3" cols="3" name="notes" class="form-control txt-area"></textarea>
												<input type="hidden" name="unique_code" value="<?php echo $this->uri->segment(5); ?>"/>
												<input type="submit" class="btn btn-primary" value="Submit Note">
											</form>
											<?php } ?>
											<?php } ?>
										</div>
										<div class="form-group col-md-6">
											<h3>Lawyer notes</h3>
											<div class="textFieldArea">
												<?php  if($lawyers_notes) { ?>
												<?php //echo '<pre>'; print_r($lawyers_notes); exit(); ?>
													<?php foreach($lawyers_notes as $row) { ?>
													<?php if($row->law_firm !== null){
														$name = $row->law_firm;
													}else if($row->staff_first_name !== null && $row->staff_last_name !== null){
														$name = $row->staff_first_name . ' ' . $row->staff_last_name;
													}else if($row->admin_first_name !== null && $row->admin_last_name !== null){
														$name = $row->admin_first_name . ' ' . $row->admin_last_name . ' - Admin';
													}else{
														$name = $row->first_name . ' ' . $row->last_name;
													} ?>
													<div class="note-single">
														<span>
															<?php echo date('m-d-Y h:i:s A', strtotime($row->date)) . ' - ' . $name; ?>
														</span>
														<p><?php echo $row->notes;?></p>
													</div>
													<?php } ?>
													<?php } ?>
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
							<!--/block-web-->
						</div>
						<!--/col-md-6-->
					</div>
					<!--/row-->
				</div>
				<!--/page-content end-->
			</div>
			<input type="hidden" value="<?php echo $id;?>" class="cons_complete">
			<script type="text/javascript">
				$(function() {
					$('#time').click(function() {
						$('#time-holder').val('<?php echo date('m-d-Y h:i:s A') ?>');
					});
                    $("#declined_reason").change(function(){
                        if($(this).val() == ""){
                            $(this).removeAttr("name");
                            $("#mand_field").html('<input style="margin-top: 5px;" type="text" name="reason" placeholder="Other reason" class="form-control" required="">');
                        }else{
                            $(this).attr("name", "reason");
                            $("#mand_field").html('');
                        }
                    });
				});
			</script>