<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Edit Lead View</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Edit Lead Form</h3>
            </div>
            <div class="porlets-content">
              <form action="<?php echo base_url();?>admin/leads/update/<?php echo $id;?>" method="post" onsubmit="return validatePhone();" parsley-validate novalidate>
               
			   <div class="form-group col-md-4">
                  <label>First Name *</label>
                  <input type="text" name="first_name" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lead->first_name; ?>">
                </div><!--/form-group-->
				
				
				
                 <div class="form-group col-md-4">
                  <label>Last name *</label>
                  <input type="text" name="last_name" parsley-trigger="change" required placeholder="Enter last name" class="form-control" value="<?php echo $lead->last_name; ?>">
                </div><!--/form-group-->
				
				
				
                 <div class="form-group col-md-4">
                  <label>Email address *</label>
                  <input type="email" name="email" parsley-trigger="change" required placeholder="Enter email" class="form-control" value="<?php echo $lead->email; ?>">
                </div><!--/form-group-->
				
				<div class="form-group col-md-4">
                  <label>Caller name if different</label>
                  <input type="text" name="caller_name" parsley-trigger="change" placeholder="Enter caller name if different" class="form-control" value="<?php echo $lead->caller_name;?>">
                </div><!--/form-group-->
				
				
                 <div class="form-group col-md-4">
                  <label>Caller Id if different</label>
                  <input type="text" name="caller_id" parsley-trigger="change" placeholder="Enter caller id if different" class="form-control" value="<?php echo $lead->caller_id; ?>">
                   
                </div><!--/form-group-->
				
				
				
				<div class="form-group col-md-4">
                  <label>Mobile *</label>
                  <input type="text" name="mobile" required parsley-trigger="change" placeholder="Enter mobile number" class="form-control" value="<?php echo $lead->mobile; ?>">
                </div><!--/form-group-->
				
				
                 <div class="form-group col-md-4">
                  <label>Home phone</label>
                  <input type="text" name="home_phone" parsley-trigger="change" placeholder="Enter home phone number" class="form-control" value="<?php echo $lead->home_phone; ?>">
                </div><!--/form-group-->
				
				
				
				
                 <div class="form-group col-md-4">
                  <label>Work phone</label>
                  <input type="text" name="work_phone" parsley-trigger="change" placeholder="Enter work place phone number" class="form-control" value="<?php echo $lead->work_phone; ?>">
                </div><!--/form-group-->
				
                 
				
                <div class="form-group col-md-4">
                  <label>Street</label>
                  <input type="text" name="street" parsley-trigger="change" placeholder="Enter street" class="form-control" value="<?php echo $lead->street; ?>">
                </div><!--/form-group-->
				
                <div class="form-group col-md-4">
                  <label>City *</label>
                  <input type="text" name="city" parsley-trigger="change" required placeholder="Enter city name" class="form-control" value="<?php echo $lead->city; ?>">
                </div><!--/form-group-->
				
               
				
				 <div class="form-group col-md-4">		
				 <div class="form-group">
                  <label>State *</label>
                 
                    <select placeholder="state" class="form-control" name="state" required>
                       	<option value="" required> Select state </option>
                     
					  <?php foreach($states_short_names as $state){?>
                      	<option value="<?php echo $state->id;?>" <?php if($state->id == $lead->state){?> selected <?php }?>> <?php echo $state->state_short_name;?> </option>
                      <?php }?>
               
      			   </select>
                 
                </div>
               </div>
				
				
				
                <div class="form-group col-md-4">
                  <label>Zip</label>
                  <input type="text" name="zip" parsley-trigger="change" placeholder="Enter source here" class="form-control"  value="<?php echo $lead->zip; ?>" maxlength="5">
                </div><!--/form-group-->
				
				<div class="clearfix"></div>
				
				               <div class="form-group col-md-4">
                  <label>Jurisdiction *</label>
                 
                    <select placeholder="Jurisdiction" class="form-control" name="jurisdiction" onchange="selectJurisdiction(this.value,'admin/Dashboard/getJurisdiction','');">
                      <option value="" required>Select Jurisdiction</option>
                      <?php foreach($jurisdiction as $jurisdictions){
							
						  ?>
                      	<option value="<?php echo $jurisdictions->id;?>" <?php if($lead->jurisdiction == $jurisdictions->id) echo 'selected';?>> <?php echo $jurisdictions->name;?> </option>
                      <?php }?>
                    </select>                 
                </div>
		<!--	
			<div class="form-group col-md-4">		
				 <div class="form-group">
                  <label>Market *</label>
                 
                    <select placeholder="market" class="form-control" name="market" id="m_id" onchange="selectCaseTypeMarketFees(this.value,'admin/Dashboard/getMarketCaseTypePrice','');">
                       	<option value="" required> Select market </option>
                     
					  <?php foreach($markets as $market){
						  $stateName =  getStateName($market->state);
						  $stateName[0]['state_short_name'];
						  ?>
					  
					  
                      	<option value="<?php //echo $market->id;?>" <?php //if($market->id == $lead->market) echo 'selected';?>> <?php //echo $market->market_name . "-" . $stateName[0]['state_short_name'];?> </option>
                      <?php }?>
               
      			   </select>
                 
                </div>
               </div>
		-->
			
				<!--
				 <div class="form-group col-md-4">
                  <label>Case Type *</label>
                 
                    <select placeholder="Case Type" class="form-control" name="case_type" id="case_type_id" onchange="selectCaseTypeMarketFee(this.value,'admin/Dashboard/getMarketCaseTypePrice','');">
                      <option value="" required>Select Case Type</option>
                      <?php foreach($case_type as $case){?>
                      	<option value="<?php //echo $case->id;?>" <?php //if($lead->case_type == $case->id) echo 'selected';?>> <?php //echo $case->type;?> </option>
                      <?php }?>
                    </select>                 
                </div>
				-->
				
			
			   
			<div class="clearfix"></div>   
			   
			   
				
			<!--	
				<div class="form-group col-md-4">		
				 <div class="form-group">
                  <label>Source *</label>
                 
                    <select placeholder="source" class="form-control" name="source">
                       	<option value="" required> Select source </option>
                     
					  <?php foreach($sources as $source){?>
                      	<option value="<?php //echo $source->id;?>" <?php //if($source->id == $lead->source) echo 'selected';?>> <?php echo $source->name;?> </option>
                      <?php }?>
               
      			   </select>
                 
                </div>
               </div>
			
              
			    <div class="form-group col-md-4">		
				 <div class="form-group">
                  <label>Intaker *</label>
                 
                    <select placeholder="intaker" class="form-control" name="intaker" required>
                       	<option value="" required> Select intaker </option>
                     
					  <?php foreach($intakers as $intaker){?>
                      	<option value="<?php //echo $intaker->id;?>" <?php //if($intaker->id == $lead->intaker) echo 'selected';?>> <?php //echo $intaker->name;?> </option>
                      <?php }?>
               
      			   </select>
                 
                </div>
               </div>
			   
			   <div class="form-group col-md-4">
                  <label>Price *</label>
                  <input type="text" name="price" id="sub_cat" parsley-trigger="change" required placeholder="Enter price" class="form-control" value="<?php //echo number_format($lead->price, 2); ?>">
                </div>
		-->		   
				
				
				
              
                <div class="form-group col-md-12">
                  <label>Case Description</label>
                    <textarea disabled class="form-control" name="case_description"><?php echo $lead->case_description;?></textarea>
                </div>
				<!--
                <div class="form-group col-md-12">
                  <label>Intake Notes</label>
                    <textarea disabled class="form-control" name="my_notes"><?php echo $lead->my_notes;?>   </textarea>
                </div>
				-->
				<div class="form-group col-md-12">
                  <label>Tags / Labels</label>
				  <br/>
					<?php
					$i=0;
					 foreach($tags_type as $tag){ 
					//echo $tag->tags .  " , "; 
					 ?>
					 <input type="button" id="t<?php echo $i;?>" class="link" onclick="tagid('<?php echo $i;?>','<?php echo $tag->tags?>')" value="<?php echo $tag->tags?>" />
					 <?php
						$i++;
					 }					 
					 ?>
					 <br/><br/>
					 <label>Selected tags are </label>
					 <br/>
					 <div id="tagarea" class="taglist">
                         <span id="span" class="link">
                         	<?php $tags_array = explode(',',$lead->labels);
								foreach($tags_array as $tags)
								{ ?>
								<?php if(!empty($tags)){?>
								<span class="spanclass"><input type="button" class="link" value="<?php echo $tags;?>"/><a class="btnremove" id="removetag"> x </a><input type="hidden" name="labels[]" value="<?php echo $tags;?>"></span>	
								<?php }?>
								<?php	}
							?>
                         </span>
					 </div>
                </div><!--/form-group-->
				
				
			<div class="clearfix"></div>
                <div class="form-group col-md-12">
                <button class="btn btn-primary" type="submit">Submit</button>
                 <a class="btn btn-default" href="<?php echo base_url();?>admin/dashboard/manage/Lead-View"> Cancel</a>
				 <div class="clearfix"></div>
            </div>
              </form>
			  <div class="clearfix"></div>
            
			
			
			
			</div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>
  
  
      <script>

     function selectJurisdiction(id, actionUrl, reloadUrl) {

        if (id == '') {
            $("#sub_cat").val('');
            return true;
        }
        $.post( "<?php echo base_url();?>"+actionUrl,{ id: id }, function( data ) {
		  $('#sub_cat').val(data);
		});
    }



</script>
  
  
    
  <script>

     function selectCaseTypeMarketFee(id, actionUrl, reloadUrl) {
		
		var m_id = $('#m_id').val();
		
        if (id == '') {
            $("#sub_cat").val('');
            return true;
        }
        $.post( "<?php echo base_url();?>"+actionUrl,{ id: id, m_id: m_id }, function( data ) {
		  $('#sub_cat').val(data);
		});
    }



</script>

  <script>

     function selectCaseTypeMarketFees(id, actionUrl, reloadUrl) {
		
		var c_id = $('#case_type_id').val();
		
        if (id == '') {
            $("#sub_cat").val('');
            return true;
        }
        $.post( "<?php echo base_url();?>"+actionUrl,{ c_id: c_id, id: id }, function( data ) {
		  $('#sub_cat').val(data);
		});
    }



</script>
  
  
  
   <script type="text/javascript">
  	function validatePhone()
	{
		var home_phone = $('#home_phone');
		var work_phone = $('#work_phone');
		var mobile = $('#mobile');
		if(home_phone.val() == '' && work_phone.val() == '' && mobile.val() == '')
		{
			alert('Please enter a phone no!');
			home_phone.focus();
			return false;
		}
	}
	  function tagid(id, value){
  
	var selectedVal = value;
	//document.getElementById("tags_2").value = selectedVal;
  $('#span').append("<span class='spanclass'><input type='button' class='link' value='"+value+"'/><a class='btnremove'>x</a><input type='hidden' name='labels[]' value='"+value+"'></span>");
  		var tags_labels = $('#tags_2').val();
		$('#tags_2').val(tags_labels + selectedVal + ',');
  }
  $(document).ready(function(){
  $(document).on('click', '.spanclass a', function() {$(this).parent().remove();
});
});
  </script>