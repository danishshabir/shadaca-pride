<div id="main-content">
    <div class="page-content">
     <div class="row">
        <div class="col-md-12">
          <h2>Lead View</h2>
        </div><!--/col-md-12--> 
      </div>
        
          <div class="tab-pane cont active">    
      		<div class="row">
                <div class="col-md-12">
                  <div class="block-web">
                   <div class="header">
                      <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
                      <h3 class="content-header">Leads</h3>
                    </div>
                 <div class="porlets-content">
                    <div class="table-responsive">
                    <div class="clearfix">
                                     <div class="btn-group pull-right">
                                          <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                          </button>
                                          <ul class="dropdown-menu pull-right">
                                              <li><a href="#">Print</a></li>
                                              <li><a href="#">Save as PDF</a></li>
                                              <li><a href="#">Export to Excel</a></li>
                                          </ul>
                                      </div>
                                  </div>
                        <div class="margin-top-10"></div>
                        <table  class="display table table-bordered table-striped" id="dynamic-table">
                          <thead>
                            <tr>
                              <th>Assign Date</th>
                              <th>Caller Name</th>
                              <th>Client Name</th>
                              <th>Email</th>
                              <th>Case Type</th>
                              <th>Tages / Labels</th>
                               <th>Action</th>
                             
                            </tr>
                          </thead>
                          <tbody>
                           <?php foreach($leads as $lead){?>
                            <tr class="gradeX status-<?php echo $lead->lead_status;?>">
                              <td><?php echo date("F j, Y, g:i a",strtotime($lead->created_at)); ?></td>
                              <td><?php echo $lead->caller_name; ?></td>
                              <td><?php echo $lead->first_name.' '.$lead->last_name; ?></td>
                              <td><?php echo $lead->email; ?></td>
                              <td><?php echo $lead->case_type; ?></td>
                              <td><?php echo $lead->labels; ?></td>
                              <td><a href="<?php echo base_url();?>admin/leads/view/<?php echo $lead->id;?>">More</a> | Call| Email</td>
                              
                            </tr>
                           <?php } ?> 
                          </tbody>
                         
                        </table>
                      </div><!--/table-responsive-->
                    </div><!--/porlets-content-->
                    
                    
                  </div><!--/block-web--> 
                </div><!--/col-md-12--> 
              </div><!--/row-->
      	  </div>
      
       
 
    </div><!--/page-content end--> 
  </div><!--/main-content end-->
</div>