<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Leads</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Leads</h3>
            </div>
         <div class="porlets-content">
            <div class="table-responsive">
            <div class="clearfix">
                              <div class="btn-group">
                                  <a href="<?php echo base_url();?>admin/leads/add">
								   <button class="btn btn-primary">
                                      Add New <i class="fa fa-plus"></i>
                                  </button></a>
                              </div>
                              <div class="btn-group pull-right">
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                  </button>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="#">Print</a></li>
                                      <li><a href="#">Save as PDF</a></li>
                                      <li><a href="#">Export to Excel</a></li>
                                  </ul>
                              </div>
                          </div>
                <div class="margin-top-10"></div>
                <table  class="display table table-bordered table-striped" id="dynamic-table">
                  <thead>
                    <tr>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Email</th>
                      <th>Caller Id</th>
                      <th>Home Phone</th>
                      <th>Work Phone</th>
                      <th>Cell</th>
                      <th>Action</th>
                     <!-- <th>Street</th>
                      <th>City</th>
                      <th>State</th>
                      <th>Zip</th> -->
                    </tr>
                  </thead>
                  <tbody>
                   <?php foreach($leads as $lead){?>
                    <tr class="gradeX">
                      <td><?php echo $lead->first_name; ?></td>
                      <td><?php echo $lead->last_name; ?></td>
                      <td><?php echo $lead->email; ?></td>
                      <td><?php echo $lead->caller_id; ?></td>
                      <td><?php echo $lead->home_phone; ?></td>
                      <td><?php echo $lead->work_phone; ?></td>
                      <td><?php echo $lead->mobile; ?></td>
                      <td><a href="<?php echo base_url();?>admin/leads/view/<?php echo $lead->id;?>">View</a> | <a href="<?php echo base_url();?>admin/leads/edit/<?php echo $lead->id;?>">Edit</a> | <a href="<?php echo base_url();?>admin/leads/delete/<?php echo $lead->id;?>">Delete</a></td>
                      <!--<td><?php echo $lead->street; ?></td>
                      <td><?php echo $lead->city; ?></td>
                      <td><?php echo $lead->state; ?></td>
                      <td><?php echo $lead->zip; ?></td>  -->
                    </tr>
                   <?php } ?> 
                  </tbody>
                 
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
            
            
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
       
 
    </div><!--/page-content end--> 
  </div><!--/main-content end-->
</div>