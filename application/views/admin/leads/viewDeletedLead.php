<?php $lead_id = $this->uri->segment(4); ?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2><?php echo ($lead->lead_status == 4 ? 'Draft View' : 'Lead View'); ?></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions">
							<a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
							<a class="refresh" href="#"><i class="fa fa-repeat"></i></a>
							<a class="close-down" href="#"><i class="fa fa-times"></i></a>
						</div>
						<h3 class="content-header"><?php echo ($lead->lead_status == 4 ? 'Draft View' : 'Lead View'); ?></h3>
						<h3 class="text-center">
							<?php if($this->session->flashdata('message')){
								echo $this->session->flashdata('message'); } ?>
							</h3>
						</div>
						<div class="porlets-content two-col">
							<div class="row">
								<div class="col col-md-6">
									<div class="row">
										<div class="col-md-6 subcol">
											<label>First name:</label>
											<span class="desp"><?php echo $lead->first_name; ?>&nbsp;</span>
										</div>
										<div class="col-md-6 subcol">
											<label>Last name:</label>
											<span class="desp"><?php echo $lead->last_name; ?>&nbsp;</span>
										</div>
										
										<div class="clearfix"></div>
										
										<div class="col-md-6 subcol">
											<label>Email:</label>
											<span class="desp"><?php echo $lead->email; ?>&nbsp;</span>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-6 subcol">
											<label>Caller name:</label>
											<span class="desp"><?php echo $lead->caller_name; ?>&nbsp;</span>
										</div>
										<div class="col-md-6 subcol">
											<label>Caller ID:</label>
											<span class="desp"><?php echo $lead->caller_id; ?>&nbsp;</span>
										</div>
										<div class="col-md-6 subcol">
											<label>Mobile*:</label>
											<span class="desp"><?php echo $lead->mobile; ?>&nbsp;</span>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-6 subcol">
											<label>Home phone:</label>
											<span class="desp"><?php echo $lead->home_phone; ?>&nbsp;</span>
										</div>
										<div class="col-md-6 subcol">
											<label>Work phone:</label>
											<span class="desp"><?php echo $lead->work_phone; ?>&nbsp;</span>
										</div>
										<div class="col-md-6 subcol">
											<label>Street:</label>
											<span class="desp"><?php echo $lead->street; ?>&nbsp;</span>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-6 subcol">
											<label>City:</label>
											<span class="desp"><?php echo $lead->city; ?>&nbsp;</span>
										</div>
										<div class="col-md-6 subcol">
											<label>State:</label>
											<span class="desp"><?php echo getStateShortName($lead->state); ?>&nbsp;</span>
										</div>
										<div class="col-md-6 subcol">
											<label>Zip:</label>
											<span class="desp"><?php echo $lead->zip; ?>&nbsp;</span>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-6 subcol">
											<label>Jurisdiction:</label>
											<span class="desp">
												<?php $newJurisdiction = getJurisdictionName($lead->jurisdiction); echo $newJurisdiction[0]['name'];?>&nbsp;
											</span>
										</div>
										<div class="col-md-6 subcol">
											<label>Market:</label>
											<span class="desp">
												<?php
												$market_name =  getMarketName($lead->market);
												$stateName =  getStateName($market_name->state);
												$stateName[0]['state_short_name'];
												echo $market_name->market_name  . "-" . $stateName[0]['state_short_name'];
												?>&nbsp;
											</span>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-6 subcol">
											<label>Case type:</label>
											<span class="desp"><?php $caseTypeName =  getCaseTypeName($lead->case_type); echo $caseTypeName[0]['type']; ?>&nbsp;</span>
										</div>
										<div class="col-md-6 subcol">
											<label>Lead price:</label>
											<span class="desp">$<?php echo number_format($lead->price, 2);?>&nbsp; </span>
										</div>
										<div class="col-md-12 subcol">
											<label>Legal issue:</label>
											<span class="spanclass desp">
												<?php echo $lead->Legal_issue;?>
											</span>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 subcol">
											<label>Intake Notes:</label>
											<span class="spanclass desp">
												<?php echo $lead->my_notes;?>
											</span>
										</div>
										<div class="col-md-6 subcol">
											<label>Case ID:</label>
											<span class="desp"><?php echo $lead->lead_id_number;?>&nbsp;</span>
										</div>
										
										<div class="clearfix"></div>
									<!--<div class="col-md-6 subcol">
										<label>Case desc:</label>
										<span class="desp"><?php echo $lead->case_description;?>&nbsp;</span>
									</div>-->
									<div class="col-md-6 subcol">
										<label>Case ref #:</label>
										<span class="desp"><?php echo $lead_id; ?>&nbsp;</span>
									</div>
									
									<div class="clearfix"></div>
									<div class="col-md-12 subcol">
										<label>Date/time:</label>
										<span class="desp">
											<?php
											$d = $lead->created_at;
											$date = strtotime($d);
											$new_date = date('m-d-Y h:i:s A', $date);
											?>
											<?php echo $new_date; ?>&nbsp;
										</span>
									</div>
									<div class="clearfix"></div>
									
									
								</div>
							</div>
							<?php if($this->session->userdata('role') == 1) { ?>
							<div class="clearfix"></div>
							<div class="col col-md-6">
								<div class="form-group">
									<h3>Lawyer notes history</h3>
									<div class="textFieldArea">
										<?php if($lawyers_notes) { ?>
										<?php foreach($lawyers_notes as $row) { ?>
										<?php if($row->law_firm !== null){
											$name = $row->law_firm;
										}else if($row->staff_first_name !== null && $row->staff_last_name !== null){
											$name = $row->staff_first_name . ' ' . $row->staff_last_name;
										}else if($row->admin_first_name !== null && $row->admin_last_name !== null){
											$name = $row->admin_first_name . ' ' . $row->admin_last_name . ' - Admin';
										}else{
											$name = $row->first_name . ' ' . $row->last_name;
										} ?>
										<div class="note-single">
											<span>
												<?php echo date('m-d-Y h:i:s A', strtotime($row->date)) . ' - ' . $name; ?>
											</span>
											<p><?php echo $row->notes;?></p>
										</div>
										<?php } ?>
										<?php } ?>
									</div>
								</div>
							</div>

							<div class="col col-md-6">
								<div class="subcol">
									<h3>Admin notes history</h3>
									<div class="textFieldArea">
										<?php if($Admin_notes) { ?>
										<?php foreach($Admin_notes as $row) { ?>
										<?php
										$name = $row->first_name . ' ' . $row->last_name;
										?>
										<div class="note-single">
											<span><?php echo date('m-d-Y h:i:s A', strtotime($row->date)) . ' - ' . $name; ?></span>
											<p><?php echo $row->notes;?></p>
										</div>
										<?php } ?>
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col col-md-6">
								<h3>Actions</h3>
								<div class="textFieldArea-2 add">
										<!--<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span>
											Lead reached (non-communication)
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span>
											Closed-no show
										</label>-->
										<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span>
											Lead reached (communication)
										</label>
										<!--
										<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span>
											Consultation scheduled
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span>
											Consultation completed
										</label>-->
										<label class="checkbox-inline">
											<input type="checkbox" value="Lead Retained" disabled="disabled">
											<span class="custom-checkbox"></span> Lead retained
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" value="Lead Not Retained" disabled="disabled">
											<span class="custom-checkbox"></span> Lead not retained
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span> Refer to another/declined
										</label>
									</div>
								</div><!--checked="checked"-->
								<?php } ?>
								<div class="form-group col-md-6">
									<h3>Activities</h3>
									<div class="textFieldArea box-activities">
										<?php if (!empty($activities)) {
											foreach ($activities as $activity) {
												if($activity->law_firm !== null){
													$name = $activity->law_firm;
												}else if($activity->staff_first_name !== null && $activity->staff_last_name !== null){
													$name = $activity->staff_first_name . ' ' . $activity->staff_last_name;
												}else if($activity->admin_first_name !== null && $activity->admin_last_name !== null){
													$in_array = array(
														"Reminder Email Sent",
														"Contact Attempted",
														"Lead Reached (communication)",
														"Lead Retained",
														"Lead Not Retained",
														"Declined",
                                                        "Entry Updated",
													);
													$name = $activity->admin_first_name . ' ' . $activity->admin_last_name;
													if(in_array($activity->event, $in_array)) $name .= " (Admin)";
												}else{
													$name = $activity->lawyer_first_name . ' ' . $activity->lawyer_last_name;
												}
												$reason = getDeclinedReason($activity->lead_id, $activity->lawyer_id);
												if($activity->event != "Entry Duplicated") {
													echo '<div class="note-single">';
													if ($activity->event == "Entry Assigned Lawyer"  || $activity->event == "Entry Reassigned Lawyer" || $activity->event == "Entry Unassigned Lawyer" && $id != '') {
														echo $activity->event . " (" . $name . ') at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at));
													}else if($activity->event == 'Entry Created') {
														echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at));
													}else if($activity->via){
														echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at)) . ' via ' . $activity->via . ' by ' . $name;
													} else {
														echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at)) . ' by ' . $name;
														echo $reason->reason != '' && $activity->event == 'Declined' ? '<br/>' . 'Reason: ' . $reason->reason : '';
													}
													echo '</div>'; 
												}
											}
										}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal for duplicate lead -->
	<div class="modal fade" id="duplicateLead" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Duplicate Lead</h4>
				</div>
				<form action="<?php echo base_url(); ?>/admin/leads/duplicateLead" method="post" class="duplicateLeadForm" onsubmit="return false;">
					<div class="modal-body">
						<p>Select area of law</p>

						<select class="form-control" name="case_type" id="case_type_id" required>
							<option value="" disabled selected>Select</option>
							<?php foreach($case_type as $case){?>
							<option value="<?php echo $case->id;?>"> <?php echo $case->type;?> </option>
							<?php }?>
						</select>
						<input type="hidden" name="dup_lead_id" value="<?php echo $lead_id; ?>">
						<br>
						<span class="duplicate_response_msg" style="height: 100px;"></span>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary duplicateLeadSaveBtn">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>