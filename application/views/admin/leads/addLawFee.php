<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Add Lead fee</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Lead Fee Form</h3>
            </div>
			
            <div class="porlets-content">
              <form action="<?php echo base_url();?>admin/leads/saveLeadFee" method="post" onsubmit="return validatePhone();" parsley-validate novalidate>
			  
                <div class="form-group col-md-4">
                  <label>Lead Fee</label>
                  <input type="text" name="fee" parsley-trigger="change" required placeholder="Enter Lead Fee" class="form-control">
                </div><!--/form-group-->
				
				
				
				<div class="form-group">
                  <input type="hidden" name="lead_id" value="<?php echo $this->uri->segment(4)?>" class="form-control">
                </div><!--/form-group-->
				
				
				<div class="form-group">
                  <input type="hidden" name="lawyer_id" value="<?php echo $this->session->userdata('id')?>" class="form-control">
                </div><!--/form-group-->
				
				
				<div class="clearfix"></div>
				
				<div class="form-group col-md-4">
                <button class="btn btn-primary" type="submit">Submit</button>
				<a class="btn btn-default" href="<?php echo base_url();?>admin/dashboard/manage/Lead-View"> Cancel</a>
                <!--<button class="btn btn-default">Cancel</button>-->
				</div>
<div class="clearfix"></div>
				</form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>
  
  
  <script type="text/javascript">
  	function validatePhone()
	{
		var home_phone = $('#home_phone');
		var work_phone = $('#work_phone');
		var mobile = $('#mobile');
		if(home_phone.val() == '' && work_phone.val() == '' && mobile.val() == '')
		{
			alert('Please enter a phone no!');
			home_phone.focus();
			return false;
		}
	}
 

 
 
  function tagid(id, value){
  
		var selectedVal = value;
		//document.getElementById("tags_2").value = selectedVal;
  		$('#span').append('<span class="spanclass"><input type="button" class="link" value="'+value+'"/><a class="btnremove" id="removetag">x</a><input type="hidden" name="labels[]" value="'+value+'"></span>');
		var tags_labels = $('#tags_labels').val();
		$('#tags_labels').val(tags_labels + selectedVal + ',');
		
		
  }
  /*
  function removetag(tagvalue)
  {
	  //var tagvalue = $(this).parent().children('input.link').val();
	  var tags_labels = $('#tags_labels').val();
	  var tags_labels_arr = tags_labels.split(',');  
	  
	  var new_tags = '';
	  for(var i = 0; i < tags_labels_arr.length; i++)
	  {
		  if(tags_labels_arr[i] != tagvalue)
		  {
			  new_tags = new_tags + tagvalue + ',';
		  }
	  }
	  $('#tags_labels').val(new_tags);
  }*/
  
  $(document).ready(function(){
	  /*$("#removetag").on('click', function(){
			
	  });*/
	
  $(document).on('click', '.spanclass a', function() {$(this).parent().remove();
});
});
	</script>
  
  
  

  