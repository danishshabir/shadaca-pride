<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Add Operator</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Operator Form</h3>
            </div>
            <div class="porlets-content">
              <form action="<?php echo base_url();?>admin/leads/save" method="post" onsubmit="return validatePhone();" parsley-validate novalidate>
                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" name="first_name" parsley-trigger="change" required placeholder="Enter first name" class="form-control">
                </div><!--/form-group-->
                 <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" name="last_name" parsley-trigger="change" required placeholder="Enter last name" class="form-control">
                </div><!--/form-group-->
                 <div class="form-group">
                  <label>Email address</label>
                  <input type="email" name="email" parsley-trigger="change" required placeholder="Enter email" class="form-control">
                </div><!--/form-group-->
                 <div class="form-group">
                  <label>Caller Id</label>
                  <input type="text" name="caller_id" parsley-trigger="change" required placeholder="Enter caller id" class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>Caller Name</label>
                  <input type="text" name="caller_name" parsley-trigger="change" required placeholder="Enter caller name here" class="form-control">
                </div><!--/form-group-->
                 <div class="form-group">
                  <label>Home Phone</label>
                  <input type="text" name="home_phone" id="home_phone" parsley-trigger="change"  placeholder="Enter home phone number" class="form-control">
                </div><!--/form-group-->
                 <div class="form-group">
                  <label>Work Phone</label>
                  <input type="text" name="work_phone" id="work_phone" parsley-trigger="change"  placeholder="Enter work place phone number" class="form-control">
                </div><!--/form-group-->
                 <div class="form-group">
                  <label>Mobile</label>
                  <input type="text" name="mobile" id="mobile" parsley-trigger="change" placeholder="Enter mobile number" class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>Street</label>
                  <input type="text" name="street" parsley-trigger="change" placeholder="Enter street" class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>City</label>
                  <input type="text" name="city" parsley-trigger="change" required placeholder="Enter city name" class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>State</label>
                  <input type="text" name="state" parsley-trigger="change" required placeholder="Enter state" class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>Zip</label>
                  <input type="text" name="zip" parsley-trigger="change" required placeholder="Enter source here" class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>Case Type</label>
                 
                    <select placeholder="Case Type" class="form-control" name="case_type">
                      <option value="">Select Case Type</option>
                      <?php foreach($case_type as $case){?>
                      	<option value="<?php echo $case->type;?>"> <?php echo $case->type;?> </option>
                      <?php }?>
                    </select>
                 
                </div>
               
               <div class="form-group">
                  <label>Tags / Labels</label>
                  <input type="text" name="labels" parsley-trigger="change" required placeholder="Enter labels here" class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>Intaker</label>
                  <input type="text" name="intaker" parsley-trigger="change" required placeholder="Intaker" class="form-control">
                </div><!--/form-group-->
                <div class="form-group">
                  <label>Case Description</label>
                    <textarea class="form-control" name="case_description"></textarea>
                </div>
                <div class="form-group">
                  <label>Master Notes (Internal)</label>
                    <textarea class="form-control" name="my_notes"></textarea>
                </div>
                
                <button class="btn btn-primary" type="submit">Submit</button>
                <button class="btn btn-default">Cancel</button>
              </form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>
  <script type="text/javascript">
  	function validatePhone()
	{
		var home_phone = $('#home_phone');
		var work_phone = $('#work_phone');
		var mobile = $('#mobile');
		if(home_phone.val() == '' && work_phone.val() == '' && mobile.val() == '')
		{
			alert('Please enter a phone no!');
			home_phone.focus();
			return false;
		}
	}
  </script>