<style>	td{		padding: 10px;		font-weight: 500;	}</style><div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Declined/Not Retained Lead View</h2>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Lead View - contact support to reopen a Declined or Not Retained lead</h3>
					</div>
					<div class="porlets-content two-col">
						<div class="row">
							<div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6 subcol">
                                        <label>First name:</label>
										<span class="desp"><?php echo $lead->first_name; ?>&nbsp;</span>
                                    </div>
                                    <div class="col-md-6 subcol">
                                        <label>Last name:</label>
										<span class="desp"><?php echo $lead->last_name; ?>&nbsp;</span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 subcol">
                                        <label>Case type:</label>
										<span class="desp"><?php echo getCaseTypeName($lead->case_type)[0]['type']; ?>&nbsp;</span>
                                    </div>
                                    <div class="col-md-6 subcol">
                                        <label>Jurisdiction:</label>
										<span class="desp"><?php echo getJurisdictionName($lead->jurisdiction)[0]['name']; ?>&nbsp;</span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 subcol">
                                        <label>Market:</label>
										<span class="desp">
                                        <?php
											$market_name =  getMarketName($lead->market);
											$stateName =  getStateName($market_name->state);
											$stateName[0]['state_short_name'];
											echo $market_name->market_name  . "-" . $stateName[0]['state_short_name'];
										?>
                                        &nbsp;</span>
                                    </div>
                                    <div class="col-md-6 subcol">
                                        <label>Case ID:</label>
										<span class="desp"><?php echo $lead->lead_id_number; ?>&nbsp;</span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 subcol">
                                        <label>Date/time:</label>
                                        <?php $date = date('m-d-Y h:i:s A',strtotime($lead->assigned_date_time)); ?>
										<span class="desp"><?php echo $date != '12-31-1969 04:00:00 PM' ? $date : 'N/A'; ?>&nbsp;</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3>Activities</h3>
                                <div class="textFieldArea box-activities">
                                <?php if (!empty($activities)) {
                                    foreach ($activities as $activity) {
                                        if ($activity->law_firm !== null) {
                                            $name = $activity->law_firm;
                                        } else if ($activity->staff_first_name !== null && $activity->staff_last_name !== null) {
                                                $name = $activity->staff_first_name . ' ' . $activity->staff_last_name;
                                        } else if ($activity->admin_first_name !== null && $activity->admin_last_name !== null) {
                                            $in_array = array(
                                                "Reminder Email Sent",
                                                "Contact Attempted",
                                                "Lead Reached (communication)",
                                                "Lead Retained",
                                                "Lead Not Retained",
                                                "Declined",
                                                "Case Complete - No Fee Received",
                                                "Case Complete - Fee Received",
                                                "Entry Updated",
                                                "Case Fee Payment Updated",
                                                "Case Fee Payment Deleted",
                                                "Case Fee Payment Received",
                                                "Lead Fee Payment Received",
                                                "Case Fee Payment Reversed",
                                                );
                                            $name = $activity->admin_first_name . ' ' . $activity->admin_last_name;
                                            if (in_array($activity->event, $in_array)) {
                                                $name .= " (Admin)";
                                            } else if (strpos($activity->event, "Fee Amount Changed To") !== false) {
                                                $name .= " (Admin)";
                                            } else if (strpos($activity->event, "Fee Payment Received") !== false) {
                                                $name .= " (Admin)";
                                            }
                                        } else if ($activity->lawfirm_id == "" && $activity->lawyer_staff == "" && $activity->lawyer_id == "" && $activity->admin_id == "") {
                                            $name = "System Admin";
                                        } else {
                                            $name = $activity->lawyer_first_name . ' ' . $activity->lawyer_last_name;
                                        }
                                        $reason = getDeclinedReason($activity->lead_id, $activity->lawyer_id);
                                        if ($activity->event != "Entry Duplicated") {
                                            echo '<div class="note-single">';
                                        if ($activity->event == "Entry Assigned Lawyer" || $activity->event == "Entry Reassigned Lawyer" || $activity->event == "Entry Unassigned Lawyer" && $id != '') {
                                            echo $activity->event . " (" . $name . ') at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at));
                                        } else if ($activity->event == 'Entry Created') {
                                            echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at));
                                        } else if ($activity->via) {
                                            echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at)) . ' via ' . $activity->via . ' by ' . $name;
                                        } else {
                                            echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at)) . ' by ' . $name;
                                            echo $reason->reason != '' && $activity->event == 'Declined' ? '<br/>' . 'Reason: ' . $reason->reason : '';
                                            echo $activity->fee_recieved != '' ? '<br />' . 'Fee Recieved: ' . $activity->fee_recieved : '';
                                        }
                                            echo '</div>';
                                        }
                                    }
                                } ?>
                                </div>
                            </div>
									<div class="clearfix"></div>
									<div class="row" style="margin-top:20px;">			
										<?php if($this->session->userdata('role') == 6 || $this->session->userdata('role') == 2 || $this->session->userdata('role') == 7) { ?>		
                                            <?php if(!(in_array($lead->lead_status, $statusArray))){ ?>
												<div class="col-md-6 l-notes">
													<?php if($this->session->flashdata('note_message')) {
														echo $this->session->flashdata('note_message');
													} ?>
													<?php if($this->session->view_as == ''){ ?>
													<?php if($role == 2 || $role == 6 || $role == 7) { ?>
													<form action="<?php echo base_url();?>admin/leads/addNotesLawyer/<?php echo $id;?>" method="post" parsley-validate novalidate>
														<h3>Lawyer Notes</h3>
														<textarea rows="3" cols="3" name="notes" class="form-control txt-area"></textarea>
														<input type="hidden" name="unique_code" value="<?php echo $this->uri->segment(5); ?>"/>
														<input type="submit" class="btn btn-primary" value="Submit Note">
													</form>
													<?php } ?>
													<?php } ?>
												</div>
												<?php } ?>	
                                                <div class="form-group col-md-6">
													<h3>Lawyer Notes</h3>
													<div class="textFieldArea">
														<?php  if($lawyers_notes) { ?>
														<?php foreach($lawyers_notes as $row) { ?>
														<?php if($row->law_firm_name !== null){
															$name = $row->law_firm_name;
														}else if($row->staff_first_name !== null && $row->staff_last_name !== null){
															$name = $row->staff_first_name . ' ' . $row->staff_last_name;
														}else if($row->admin_first_name !== null && $row->admin_last_name !== null){
															$name = $row->admin_first_name . ' ' . $row->admin_last_name . ' - Admin';
														}else{
															$name = $row->first_name . ' ' . $row->last_name;
														} ?>
														<div class="note-single">
															<span>
																<?php echo date('m-d-Y h:i:s A', strtotime($row->date)) . ' - ' . $name; ?>
															</span>
															<p><?php echo $row->notes;?></p>
														</div>
														<?php } ?>
														<?php } ?>
													</div>
												</div>	 
												<?php } ?>
                                                </div>
                                                <div class="row" style="margin-top: 10px;">
											</div>
											<div class="clearfix"></div>
                                           
										</div>
									</div>
								</div>
								<!--/block-web-->
							</div>
							<!--/col-md-6-->
						</div>
						<!--/row-->
					</div>
					<!--/page-content end-->
				</div>
				<input type="hidden" value="<?php echo $id;?>" class="cons_complete">
				<script type="text/javascript">
					$(function() {
						$('#time').click(function() {
							$('#time-holder').val('<?php echo date('m-d-Y h:i:s A') ?>');
						});
                        $("#declined_reason").change(function(){
                            if($(this).val() == ""){
                                $(this).removeAttr("name");
                                $("#mand_field").html('<input style="margin-top: 5px;" type="text" name="reason" placeholder="Other reason" class="form-control" required="">');
                            }else{
                                $(this).attr("name", "reason");
                                $("#mand_field").html('');
                            }
                        });
					});
				</script>