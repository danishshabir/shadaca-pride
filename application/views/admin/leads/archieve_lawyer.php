<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<?php
					if($this->uri->segment(2) == "closed_comp_lawyer"){
						if($this->session->role == 2){
							$title = "Lawyer Closed/Completed";
						}else if($this->session->role == 6){
							$title = "Lawfirm Closed/Completed";
						}else if($this->session->role == 7){
							$title = "Lawyer Staff Closed/Completed";
						}
					}else{
						if($this->session->role == 2){
							$title = "Lawyer Archived";
						}else if($this->session->role == 6){
							$title = "Lawfirm Archived";
						}else if($this->session->role == 7){
							$title = "Lawyer Staff Archived";
						}
					}
				?>
				<h2><?php echo $title; ?> Leads</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
                <h3 class="text-center">
				<span style = "color: green;" id = "message"></span>
                    <?php if ($this->session->flashdata('message')) {
                        echo $this->session->flashdata('message');
                    } ?>
                </h3>
					<div class="porlets-content">
						<div class="table-responsive">
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
                                        <th>Status</th>
										<th>Case Id</th>
										<th>Law Firm</th>
										<th>Lawyer Name</th>
										<th>Referral Name</th>
										<th>Case Type</th>
										<th>Market</th>
										<th>Case Description</th>
										<th>Date Completed</th>
										<th>Date Archived</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									if(!empty($archive_leads))
									{
										foreach($archive_leads as $lead)
										{
											if($lead->lead_status != 1)
											{ 
												$assigned_detail = getLeadsLawyer($lead->id);
												$assigned_lawyer_law_firm = getLeadsLawyerLawfirm($lead->id);
											}
                                            if($lead->lead_status == 11){
                                                $status = "Closed - Incomplete";
                                            }else if($lead->lead_status == 12){
                                                $status = "Case Complete - Fee Received";
                                            }else if($lead->lead_status == 13){
                                                $status = "Closed - Unassigned";
                                            }else if($lead->lead_status == 14){
                                                $status = "Closed - Declined";
                                            }else if($lead->lead_status == 3){
                                                $status = "Case Complete - Retained";
                                            }else if($lead->lead_status == 20) {
                                                $status = "Closed - Contacted";
                                            }
											?>
											<tr>
                                                <td><?php echo $status; ?></td>
												<td><?php echo $lead->lead_id_number; ?></td>
												<td><?php if($lead->lead_status != 1 && $lead->lead_status != 4){ echo $assigned_lawyer_law_firm['assigned_to'] != "" ? $assigned_lawyer_law_firm['assigned_to'] : "Not Assigned";} else{ echo 'Not Assigned';}?></td>
												<td><?php if($lead->lead_status != 1 && $lead->lead_status != 4){ echo $assigned_detail['assigned_to'] != "" ? $assigned_detail['assigned_to'] : "Not Assigned";} else{ echo 'Not Assigned';}?></td>
												<td><?php echo $lead->first_name.' '.$lead->last_name; ?></td>
												<td><?php $caseTypeName =  getCaseTypeName($lead->case_type); echo $caseTypeName[0]['type']; ?></td>
												<td><?php  $newJurisdiction = getMarketName($lead->market); $stateName =  getStateName($newJurisdiction->state); $stateName[0]['state_short_name']; echo $newJurisdiction->market_name . " - " . $stateName[0]['state_short_name']; ?></td> 
												<td><?php echo $lead->case_description == '' ? 'N/A' : $lead->case_description; ?></td>
												<td><?php echo $lead->date_closed == '' ? 'N/A' : date('M d, Y h:i a', strtotime($lead->date_closed)); ?></td>
												<td><?php echo $lead->date_archived == '' ? 'N/A' : date('M d, Y h:i a', strtotime($lead->date_archived)); ?></td>
												<td><a href="<?php echo base_url().'admin/leads/view/'.$lead->id.($lead->lawyer_to_lead_unique_code != '' ? '/'.$lead->lawyer_to_lead_unique_code : '');?>">View Lead</a></td>
											</tr>
									<?php } } ?> 
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<!--/main-content end-->
</div>