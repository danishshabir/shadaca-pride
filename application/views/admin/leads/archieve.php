<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Archived Leads</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="porlets-content">
                        <div class="row">
            				<div class="form-group col-md-2">
            					<input type="text" name="case_id" id="case_id" parsley-trigger="change" placeholder="Enter Last 5 digits of Case Id" class="form-control searchByCaseId">
            				</div>
            			</div>
						<div class="table-responsive">
                            <p>Archived items are closed/completed leads over 30 days old.</p>
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
                                        <th>Status</th>
										<th>Case Id</th>
										<th>Law Firm</th>
										<th>Lawyer Name</th>
										<th>Lead Name</th>
										<th>Case Type</th>
										<th>Market</th>
										<th>Case Description</th>
										<th>Date Completed</th>
										<th>Date Archived</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="resultBody">
									<?php
									if(!empty($archive_leads))
									{
										foreach($archive_leads as $lead)
										{
											if($lead->lead_status != 1)
											{ 
												$assigned_detail = getLeadsLawyer($lead->id);
												$assigned_lawyer_law_firm = getLeadsLawyerLawfirm($lead->id);
											}
                                            if($lead->lead_status == 11){
                                                $status = "Closed - Incomplete";
                                            }else if($lead->lead_status == 12){
                                                $status = "Case Complete - Fee Received";
                                            }else if($lead->lead_status == 13){
                                                $status = "Closed - Unassigned";
                                            }else if($lead->lead_status == 14){
                                                $status = "Closed - Declined";
                                            }else if($lead->lead_status == 3){
                                                $status = "Case Complete - Retained";
                                            }else if($lead->lead_status == 20){
                                                $status = 'Closed - Contacted';
                                            }
											?>
											<tr>
                                                <td><?php echo $status; ?></td>
												<td><?php echo $lead->lead_id_number; ?></td>
												<td><?php if($lead->lead_status != 1 && $lead->lead_status != 4){ echo $assigned_lawyer_law_firm['assigned_to'] != "" ? $assigned_lawyer_law_firm['assigned_to'] : "Not Assigned";} else{ echo 'Not Assigned';}?></td>
												<td><?php if($lead->lead_status != 1 && $lead->lead_status != 4){ echo $assigned_detail['assigned_to'] != "" ? $assigned_detail['assigned_to'] : "Not Assigned";}?></td>
												<td><?php echo $lead->first_name.' '.$lead->last_name; ?></td>
												<td><?php $caseTypeName =  getCaseTypeName($lead->case_type); echo $caseTypeName[0]['type']; ?></td>
												<td><?php  $newJurisdiction = getMarketName($lead->market); $stateName =  getStateName($newJurisdiction->state); $stateName[0]['state_short_name']; echo $newJurisdiction->market_name . " - " . $stateName[0]['state_short_name']; ?></td> 
												<td><?php echo $lead->case_description == '' ? 'N/A' : $lead->case_description; ?></td>
												<td><?php echo $lead->date_closed == '' ? 'N/A' : date('M d, Y h:i a', strtotime($lead->date_closed)); ?></td>
												<td><?php echo $lead->date_archived == '' ? 'N/A' : date('M d, Y h:i a', strtotime($lead->date_archived)); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url().'admin/leads/view/'.$lead->id;?>">View Lead</a>
                                                    <?php if(!in_array($lead->lead_status, array(3, 12, 20))){ ?>
                                                        | <a onclick="confirm('Are you sure? You want to perform this action?') ? window.location.href = '<?php echo base_url("admin/leads/updateLeadStatus/$lead->id/reopen_lead"); ?>' : false" href="javascript:void(0);">Reopen Lead</a>
                                                    <?php } ?>    
                                                </td>
											</tr>
									<?php } } ?> 
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<!--/main-content end-->
</div>
<script type="text/javascript">
    $(function(){
        $(".searchByCaseId").keyup(function (e) {
            e.preventDefault();
            var case_id = $(this).val();
    		$(".page_loader").show();
            $.post('<?php echo base_url('admin/archive/searchByCaseId'); ?>', {case_id: case_id}, function(data){
                $('#resultBody').html(data);
                $(".page_loader").hide();
            });
    	});
    });
</script>