A lead has been submitted to <?php echo $this->config->item('project_title'); ?> and needs to be assigned. <br/>
<!DOCTYPE html>
<html>
	<head>
		<style>
			th, td {
			border: 1px solid black;
			padding:15px;
			margin:15px;
			}
		</style>
	</head>
	<body>
		<table style="width:100%">
			<tr>
				<th>Case ID #:</th>
				<th>Date Referral:</th>
				<th>Name:</th>
				<th>Phone:</th>
				<th>Email:</th>
				<th>Loc:</th>
				<th>Jurisdiction:</th>
				<th>Case Type:</th>
				<th>Case Comments:</th>
			</tr>
			<tr>
				<td><?php echo $lead_detail->lead_id_number; ?></td>
				<td><?php echo date('M-d-Y H:i:s a'); ?></td>
				<td><?php echo $lead_detail->first_name; ?></td>
				<td><?php echo $lead_detail->mobile; ?></td>
				<td><?php echo $lead_detail->email; ?></td>
				<td><?php echo $lead_detail->street; ?></td>
				<td><?php echo getJurisdictionNamee($lead_detail->jurisdiction); ?></td>
				<td><?php echo $caseTypeName[0]['type']; ?></td>
				<td><?php echo $lead_detail->case_description; ?></td>
			</tr>
		</table>
	</body>
</html>