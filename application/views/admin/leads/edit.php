<?php $lead_id = $this->uri->segment(4); ?>
<?php $lawyer_id = fetchLawyerIdByLeadId($lead_id); ?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Edit Lead View</h2>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Edit Lead Form</h3>
					</div>
					<div class="porlets-content row">
						<form id="main_form" action="<?php echo base_url();?>admin/leads/update/<?php echo $id;?>" method="post" onsubmit="return validatePhone();" parsley-validate novalidate>
							<div class="form-group col-md-4">
								<label>First name *</label>
								<input type="text" name="first_name" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $lead->first_name; ?>">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Last name</label>
								<input type="text" name="last_name" parsley-trigger="change" placeholder="Enter last name" class="form-control" value="<?php echo $lead->last_name; ?>">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Email address</label>
								<input type="email" name="email" parsley-trigger="change" placeholder="Enter email" class="form-control" value="<?php echo $lead->email; ?>">
							</div>
							<!--/form-group-->
							<div class="clearfix"></div>
							<div class="form-group col-md-4">
								<label>Caller name if different</label>
								<input type="text" name="caller_name" parsley-trigger="change"  placeholder="Enter caller name if different" class="form-control" value="<?php echo $lead->caller_name;?>">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Caller ID if different</label>
								<input type="text" name="caller_id" parsley-trigger="change"  placeholder="Enter caller ID if different" class="form-control" value="<?php echo $lead->caller_id; ?>">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Mobile *</label>
								<input type="text" name="mobile" parsley-trigger="change" required placeholder="Enter mobile number" class="form-control" value="<?php echo $lead->mobile; ?>">
							</div>
							<!--/form-group-->
							<div class="clearfix"></div>
							<div class="form-group col-md-4">
								<label>Home phone</label>
								<input type="text" name="home_phone" parsley-trigger="change" placeholder="Enter home phone number" class="form-control" value="<?php echo $lead->home_phone; ?>">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Work phone</label>
								<input type="text" name="work_phone" parsley-trigger="change" placeholder="Enter work place phone number" class="form-control" value="<?php echo $lead->work_phone; ?>">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Street</label>
								<input type="text" name="street"  parsley-trigger="change"  placeholder="Enter street" class="form-control" value="<?php echo $lead->street; ?>">
							</div>
							<!--/form-group-->
							<div class="clearfix"></div>
							<div class="form-group col-md-4">
								<label>City</label>
								<input type="text" name="city" parsley-trigger="change" placeholder="Enter city name" class="form-control" value="<?php echo $lead->city; ?>">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<div class="form-group">
									<label>State *</label>
									<select placeholder="state" class="form-control" name="state" required>
										<option value="" required> Select state </option>
										<?php foreach($states_short_names as $state){?>
										<option value="<?php echo $state->id;?>" <?php if($state->id == $lead->state){?> selected <?php }?>> <?php echo $state->state_short_name;?> </option>
										<?php }?>
									</select>
								</div>
							</div>
							<div class="form-group col-md-4">
								<label>Zip</label>
								<input type="text" name="zip" parsley-trigger="change"  placeholder="Enter source here" class="form-control"  value="<?php echo $lead->zip; ?>" maxlength="5">
							</div>
							<!--/form-group-->
							<div class="clearfix"></div>
							
							<div class="form-group col-md-4">
								<label>Jurisdiction *</label>
								<select placeholder="Jurisdiction" class="form-control" required name="jurisdiction">
									<option value="" required>Select Jurisdiction</option>
									<?php foreach($jurisdiction as $jurisdictions){
										?>
										<option data-market="<?php echo $jurisdictions->value; ?>" value="<?php echo $jurisdictions->id;?>" <?php if($lead->jurisdiction == $jurisdictions->id) echo 'selected';?>> <?php echo $jurisdictions->name;?> </option>
										<?php }?>
									</select>
								</div>
								<div class="form-group col-md-4">
									<div class="form-group">
										<label>Market *</label>
										<select placeholder="market" class="form-control" name="market" required id="m_id">
											<option value="" required> Select market </option>
											<?php foreach($markets as $market){
												$stateName =  getStateName($market->state);
												$stateName[0]['state_short_name'];
												?>
												<option value="<?php echo $market->id;?>" <?php if($market->id == $lead->market) echo 'selected';?>> <?php echo $market->market_name . "-" . $stateName[0]['state_short_name'];?> </option>
												<?php }?>
											</select>
										</div>
									</div>
									<div class="form-group col-md-4">
										<label>Case type *</label>
										<select placeholder="Case Type" class="form-control" required name="case_type" id="case_type_id">
											<option value="" required>Select Case Type</option>
											<?php foreach($case_type as $case){?>
											<option value="<?php echo $case->id;?>" <?php if($lead->case_type == $case->id) echo 'selected';?>> <?php echo $case->type;?> </option>
											<?php }?>
										</select>
									</div>
									<div class="clearfix"></div>

							<div class="form-group col-md-4">
								<label>Legal issue / case description</label>
								<textarea class="form-control" name="case_description"><?php echo $lead->case_description;?></textarea>
							</div>
							<div class="form-group col-md-4">
								<label>Intake notes</label>
								<textarea class="form-control" name="my_notes"><?php echo $lead->my_notes;?></textarea>
							</div>
							<div class="form-group col-md-4">
								<label>Admin notes (select to make visible to admin or lawyer)</label>
								<textarea parsley-trigger="change" id="admin_notes"  placeholder="Enter admin Notes" class="form-control"></textarea>
								<?php if($lead->lead_status != 1 && $lead->lead_status != 4 && $lead->lead_status != 9){ ?>
								<button id="addNoteLawyer" style="float: right; margin-top: 5px;" type="button" class="btn btn-primary btn-sm">Add Lawyer Note</button>
								<?php } ?>
								<button id="addNote" style="float: right; margin: 5px 5px 0px 0px;" class="btn btn-primary btn-sm" type="button">Add Admin Note</button>
							</div>
							<div class="form-group col-md-4">
								<div class="form-group">
									<label>Source</label>
									<select placeholder="source" class="form-control" name="source">
										<option value="" required> Select source </option>
										<?php foreach($sources as $source){?>
										<option value="<?php echo $source->id;?>" <?php if($source->id == $lead->source) echo 'selected';?>> <?php echo $source->name;?> </option>
										<?php }?>
									</select>
								</div>
							</div>
							
							<!--<div class="form-group col-md-4">
								<div class="form-group">
									<label>Intaker *</label>
									<select placeholder="intaker" class="form-control" name="intaker" >
										<option value="" required> Select intaker </option>
										<?php foreach($intakers as $intaker){?>
										<option value="<?php echo $intaker->id;?>" <?php if($intaker->id == $lead->intaker) echo 'selected';?>> <?php echo $intaker->name;?> </option>
										<?php }?>
									</select>
								</div>
							</div>-->
							<div class="form-group col-md-4">
								<label>Price *</label>
								<input type="text" name="price" id="sub_cat" parsley-trigger="change"  placeholder="Enter price" class="form-control" value="<?php echo number_format($lead->price, 2);?>">
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Lead date/time</label>
								<input type="text"  name="created_at" parsley-trigger="change" placeholder="Enter Lead Date/Time" value="<?php echo date('m-d-Y h:i:s A', strtotime($lead->created_at));?>" class="form-control form_datetime-adv datepicker-here" data-date-format="mm-dd-yyyy" data-language="en" data-timepicker="true">
							</div>
                            <div class="clearfix"></div>
							<div class="form-group col-md-4">
								<label>Case ID</label>
								<input type="text" class="form-control" value="<?php echo $lead->lead_id_number; ?>" disabled>
							</div>
							<div class="form-group col-md-4">
								<label>Case Ref #</label>
								<input type="text" class="form-control" value="<?php echo $lead->id; ?>" disabled>
							</div>
                            <div class="clearfix"></div>
							<!--/form-group-->
							<?php // (Commented on 01-nov-2017 for updating view) ?>
							<!--<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col col-md-12">
											<h3>Activities</h3>
											<div class="textFieldArea box-activities doubleED">
												<?php if(!empty($activities)) {
													foreach($activities as $activity) {
														if($activity->event != "Entry Duplicated") {
															$admin_name = '';
															if (!is_null($activity->created_by)) {
																$admin_name = getAdminName($activity->created_by);
																$admin_name = ' by ' . $admin_name->name . ' ' . $admin_name->lname;
															}
															echo '<div class="note-single">';
															if ($activity->event == "Entry Assigned Lawyer"  || $activity->event == "Entry Reassigned Lawyer" && $activity->lawyer_id != '') {
																echo $activity->event . " ( " . getLawyerName($activity->lawyer_id) . ' ) at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at));
															} else {
																echo $activity->event . ' at ' . $newDate =
																		date("m-d-Y h:i:s A", strtotime($activity->created_at));
															}
															if ($activity->via) echo ' via ' . $activity->via;
															if ($admin_name != '') echo $admin_name;
															echo '</div>'; 
														}
													}
												}
												?>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<h3>Notes History</h3>
												<div class="textFieldArea">
													<?php if($lawyers_notes) { ?>
														<?php foreach($lawyers_notes as $row) { ?>
                                                            <?php if($row->law_firm !== null){
                												$name = $row->law_firm;
                											}else if($row->staff_first_name !== null && $row->staff_last_name !== null){
                												$name = $row->staff_first_name . ' ' . $row->staff_last_name;
                											}else{
                												$name = $row->first_name . ' ' . $row->last_name;
                											} ?>
															<div class="note-single">
																<!--<span><?php /*echo date('M d, Y h:i a', strtotime($row->date)); */?></span>-->
																<!--<span><?php echo date('m-d-Y H:i:s', strtotime($row->date)); ?></span>
																<p><?php echo $row->notes;?></p>
															</div>
														<?php } ?>
													<?php } ?>
												</div>
											</div>
										</div>
										<div class="col col-md-12">
											<h3>Actions</h3>
											<div class="textFieldArea add deFaultEdPadd">
												<div class="row">
													<!--<label class="col-md-12">
														<input type="checkbox" disabled="disabled">
														<span class="custom-checkbox"></span>
														Lead reached (non-communication)
													</label>
													<label class="col-md-12">
														<input type="checkbox" disabled="disabled">
														<span class="custom-checkbox"></span>
														Closed-no show
													</label>-->
													<!--<label class="col-md-12">
														<input type="checkbox" disabled="disabled">
														<span class="custom-checkbox"></span>
														Lead reached (communication)
													</label>
													<!--
													<label class="col-md-12">
														<input type="checkbox" disabled="disabled">
														<span class="custom-checkbox"></span>
														Consultation scheduled
													</label>
													<label class="col-md-12">
														<input type="checkbox" disabled="disabled">
														<span class="custom-checkbox"></span>
														Consultation completed
													</label>-->
													<!--<label class="col-md-12">
														<input type="checkbox" value="Lead Retained" disabled="disabled">
														<span class="custom-checkbox"></span> Lead retained
													</label>
													<label class="col-md-12">
														<input type="checkbox" value="Lead Not Retained" disabled="disabled">
														<span class="custom-checkbox"></span> Lead not retained
													</label>
													<label class="col-md-12">
														<input type="checkbox" disabled="disabled">
														<span class="custom-checkbox"></span> Refer to another/declined
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<script type="text/javascript">
								$('#main_form input').on("keypress", function(event){
									if(event.which == 13){
										$('#submitBtn').trigger("click");
									}
								});
							</script>
							<div style="position: absolute; opacity: 0;">
								<?php if($lead->lead_status == '4'){ ?>
								<button class="btn btn-primary" type="submit" id="save_button" name="submit" value="save_button">Save & hold</button>
								<button class="btn btn-primary" type="submit" id="submit_button" name="submit" value="submit_button">Save & submit</button>
								<button class="btn btn-primary" type="submit" id="assign_button" name="submit" value="assign_button">Save & Assign</button>
								<button class="btn btn-primary" type="submit" id="closed_incomplete" name="submit" value="closed_incomplete">Closed - Incomplete</button>
								<?php } else {?>
                                <button class="btn btn-primary" type="submit" id="assign_button" name="submit" value="assign_button">Save & Assign</button>
								<button class="btn btn-primary" type="submit" id="submit_button" name="submit" value="submit_button">Save & submit</button>
								<?php }?>
							</div>
						</form>
						<div class="clearfix"></div>
						<div class="col col-md-6">
							<div class="form-group">
								<h3>Lawyer notes history</h3>
								<div class="textFieldArea">
									<?php if($lawyers_notes) { ?>
									<?php foreach($lawyers_notes as $row) { ?>
									<?php if($row->law_firm !== null){
										$name = $row->law_firm;
									}else if($row->staff_first_name !== null && $row->staff_last_name !== null){
										$name = $row->staff_first_name . ' ' . $row->staff_last_name;
									}else if($row->admin_first_name !== null && $row->admin_last_name !== null){
										$name = $row->admin_first_name . ' ' . $row->admin_last_name . ' - Admin';
									}else{
										$name = $row->first_name . ' ' . $row->last_name;
									} ?>
									<div class="note-single">
										<span>
											<?php echo date('m-d-Y h:i:s A', strtotime($row->date)) . ' - ' . $name; ?>
										</span>
										<p><?php echo $row->notes;?></p>
									</div>
									<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>

						<div class="col col-md-6">
							<div class="subcol">
								<h3>Admin notes history</h3>
								<div class="textFieldArea">
									<?php if($Admin_notes) { ?>
									<?php foreach($Admin_notes as $row) { ?>
									<?php
									$name = $row->first_name . ' ' . $row->last_name;
									?>
									<div class="note-single">
										<span><?php echo date('m-d-Y h:i:s A', strtotime($row->date)) . ' - ' . $name; ?></span>
										<p><?php echo $row->notes;?></p>
									</div>
									<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col col-md-6">
							<h3>Actions</h3>
							<div class="textFieldArea-2">
								<?php $contact_attempts = contact_attempts($lead->id, $lawyer_id); 
								?>
								<label id="attempts" class="checkbox-inline">
									Contact attempts : <?php if(!empty($contact_attempts)){ echo sizeof($contact_attempts); } else { echo '0'; }?> times
								</label>
								<?php
								if(!empty($contact_attempts))
								{
									$i = 1;
									foreach($contact_attempts as $attempt)
									{
										if($attempt->law_firm !== null){
											$name = $attempt->law_firm;
										}else if($attempt->staff_first_name !== null && $attempt->staff_last_name !== null){
											$name = $attempt->staff_first_name . ' ' . $attempt->staff_last_name;
										}else if($attempt->admin_first_name !== null && $attempt->admin_last_name !== null){
											$name = $attempt->admin_first_name . ' ' . $attempt->admin_last_name . " (Admin)"; 
										}else{
											$name = $attempt->lawyer_first_name . ' ' . $attempt->lawyer_last_name;
										}
										?>
										<label style="display: block; margin-right: 5px 0px;">Attempt <?php echo $i;?>: <?php echo date('m-d-Y h:i:s A', strtotime($attempt->datetime)) . ' (' . $attempt->attempt_type . ') by ' . $name; ?></label>
										<?php $i++;
									}
								}
								if($lead->lead_status != '3' && !(in_array($lead->lead_status, $statusArray))) { ?>
								<?php if($this->session->view_as == '' && $lawyer_id){ ?>
								<div class="panel panel-default">
									<div class="panel-heading warning">
										<h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#ac4-2"> <i class="fa fa-plus"></i> Add more attempt entries </a> </h4>
									</div>
									<div id="ac4-2" class="panel-collapse collapse">
										<div class="panel-body">
											<form action="<?php echo base_url();?>admin/leads/addAttemptsLawyers/<?php echo $lead->id; ?>" method="post" class="l-actions">
												<input type="hidden" value="Contact Attempted" name="action_type">
												<div>
													<div style="display: inline-block; width: auto; margin-right: 25px;" class="radio radioWithError">
														<label style="display: inline-block;">
															<input type="radio" id="optionsRadios1" style="opacity: 0; position: absolute;" value="Phone" name="attempt_type" required>
															<span class="custom-radio"></span> Phone  </label>
														</div>
														<div style="display: inline-block; width: auto;" class="radio radioWithError">
															<label style="display: inline-block;">
																<input type="radio" id="optionsRadios1" style="opacity: 0; position: absolute;" value="Email" name="attempt_type" required>
																<span class="custom-radio"></span> Email </label>
															</div>
														</div>
														<div class="row">
															<div class="form-group col-md-8">
																<label>Contact Date/Time</label>
																<input type="text" name="created_at" id="time-holder" placeholder="Enter Contact Date/Time" class="form-control datepicker-here" data-date-format="mm-dd-yyyy" data-language="en" data-timepicker="true" required>
															</div>
															<div class="form-group col-md-4">
																<label style="display: block;">Add Date</label>
																<button class="btn btn-primary" style="width: 100px;" onclick="document.getElementById('time-holder').value = '<?php echo date("m-d-Y h:i:s A"); ?>'" type="button" value="time" name="timer" id="time">Now</button>
															</div>
														</div>
														<input type="hidden" class="current_url" name="current_url" value="<?php echo current_url(); ?>"/>
														<button class="btn btn-primary" type="submit">Submit</button>
													</form>
												</div>
											</div>
										</div>
										<?php } ?>
										<?php } ?>
										<?php $count_rows = admin_lead_reached($lead->id, "Lead Reached", "Lead Reached (communication)", $lawyer_id); ?>
										<label style="width: 100%; margin-bottom: 7px;" class="checkbox-inline">
											<input type="checkbox" value="Lead Reached (communication)" name="" id="inlinecheckbox1" attr-action="Lead Reached" class="action" <?php if($count_rows > 0) {?> checked="checked" <?php } ?><?php echo ($this->session->view_as == '' && $lawyer_id && !(in_array($lead->lead_status, $statusArray)) ? 'enabled' : 'disabled'); ?>>
											<span class="custom-checkbox"></span> Lead reached (communication)
										</label>
										<?php $count_rows = admin_lead_retained($lead->id, "Lead Retained", $lawyer_id); ?>
										<label style="width: 100%; margin-bottom: 7px;" class="checkbox-inline">
											<input type="checkbox" attr-action="Lead Retained" id="inlinecheckbox1" class="action cons_complete" value="<?php echo $id; ?>" <?php if($count_rows > 0){ ?> checked="checked" <?php } ?><?php echo ($this->session->view_as == '' && $lawyer_id && !(in_array($lead->lead_status, $statusArray)) ? 'enabled' : 'disabled'); ?>>
											<span class="custom-checkbox"></span> Lead retained (completed)
										</label>
										<?php $count_rows = admin_lead_retained($lead->id, "Lead Not Retained", $lawyer_id); ?>
										<label style="width: 100%; margin-bottom: 7px;" class="checkbox-inline">
											<input type="checkbox" attr-action="Lead Not Retained" name="" id="inlinecheckbox1" class="lead_not_retained_status" <?php if($count_rows > 0) { ?> checked="checked" <?php } ?><?php echo ($this->session->view_as == '' && $lawyer_id && !(in_array($lead->lead_status, $statusArray)) ? 'enabled' : 'disabled'); ?>>
											<span class="custom-checkbox"></span> Lead not retained
										</label>
										<?php $lead_retained_reason = lead_retained_reason($lead->id, "Lead Not Retained", $lawyer_id); ?>
										<?php if($lead_retained_reason > 0) { ?>
										<div class="form-group col-md-8">
											<label>Reason: </label>
											<input type="text" name="lead_not_retained_reason" class="form-control" value="<?php echo $lead_retained_reason->lead_not_retained_reason;?>" disabled>
										</div>
										<?php } else { ?>
										<div class="form-group col-md-8 lead_retained_div" style="display:none;">
											<form action="<?php echo base_url();?>admin/Leads/save_action" method="post" parsley-validate novalidate>
												<input type="hidden" name="lead_id" value="<?php echo $id;?>" />
												<input type="hidden" name="unique_code" value="<?php echo $this->uri->segment(5); ?>"/>
												<input type="hidden" value="Lead Not Retained" name="action_type">
												<label>Reason: </label>
												<input type="text" name="lead_not_retained_reason" class="form-control" value="">
												<input type="hidden" name="status" value="6"><br>
												<button class="btn btn-primary" type="submit">Submit</button>
											</form>
										</div>
										<?php } ?>
										<div class="clearfix"></div>
										<?php $lead_declined = lead_declined($lead->id, "Declined",  $lawyer_id); ?>
										<label style="width: 100%; margin-bottom: 7px;" class="checkbox-inline">
											<input type="checkbox" attr-action="Declined" name="" id="inlinecheckbox1" class="lead_declined_status" <?php if($lead_declined) { ?> checked="checked" <?php } ?><?php echo ($this->session->view_as == '' && $lawyer_id && !(in_array($lead->lead_status, $statusArray)) ? 'enabled' : 'disabled'); ?>>
											<span class="custom-checkbox"></span> Refer to another/declined
										</label>
										<?php if($lead_declined) { ?>
										<div class="form-group col-md-8">
											<label>Reason: </label>
											<input type="text" name="reason" class="form-control" value="<?php echo $lead_declined->reason;?>" disabled>
										</div>
										<?php } else { ?>
										<div class="form-group col-md-8 lead_declined_div" style="display:none;">
											<form action="<?php echo base_url();?>admin/Leads/save_action" method="post" parsley-validate novalidate>
												<input type="hidden" name="lead_id" value="<?php echo $id;?>" />
												<input type="hidden" name="unique_code" value="<?php echo $this->uri->segment(5); ?>"/>
												<input type="hidden" value="Declined" name="action_type">
												<label>Reason: </label>
												<input type="text" name="reason" class="form-control">
												<input type="hidden" name="status" value="2"><br>
												<button class="btn btn-primary" type="submit">Submit</button>
											</form>
										</div>
										<?php } ?>
									</div>
								</div><!---checked="checked"-->
								<div class="form-group col-md-6">
									<h3>Activities</h3>
									<div class="textFieldArea box-activities">
										<?php if (!empty($activities)) {
											foreach ($activities as $activity) {
												if($activity->law_firm !== null){
													$name = $activity->law_firm;
												}else if($activity->staff_first_name !== null && $activity->staff_last_name !== null){
													$name = $activity->staff_first_name . ' ' . $activity->staff_last_name;
												}else if($activity->admin_first_name !== null && $activity->admin_last_name !== null){
													$in_array = array(
														"Reminder Email Sent",
														"Contact Attempted",
														"Lead Reached (communication)",
														"Lead Retained",
														"Lead Not Retained",
														"Declined",
                                                        "Entry Updated",
													);
													$name = $activity->admin_first_name . ' ' . $activity->admin_last_name;
													if(in_array($activity->event, $in_array)) $name .= " (Admin)";
												}else{
													$name = $activity->lawyer_first_name . ' ' . $activity->lawyer_last_name;
												}
												$reason = getDeclinedReason($activity->lead_id, $activity->lawyer_id);
												if($activity->event != "Entry Duplicated") {
													echo '<div class="note-single">';
													if ($activity->event == "Entry Assigned Lawyer"  || $activity->event == "Entry Reassigned Lawyer" || $activity->event == "Entry Unassigned Lawyer" && $id != '') {
														echo $activity->event . " (" . $name . ') at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at));
													}else if($activity->event == 'Entry Created') {
														echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at));
													}else if($activity->via){
														echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at)) . ' via ' . $activity->via . ' by ' . $name;
													} else {
														echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at)) . ' by ' . $name;
														echo $reason->reason != '' && $activity->event == 'Declined' ? '<br/>' . 'Reason: ' . $reason->reason : '';
													}
													echo '</div>'; 
												}
											}
										}
										?>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="form-group col-md-12">
									<?php if($lead->lead_status == '4'){ ?>
									<button class="btn btn-primary trigger-btn" type="button" value="save_button">Save & hold</button>
									<button class="btn btn-primary trigger-btn" type="button" value="submit_button">Save & submit</button>
									<button class="btn btn-primary trigger-btn" type="button" value="assign_button">Save & Assign</button>
									<button class="btn btn-primary trigger-btn" type="button" value="closed_incomplete">Closed - Incomplete</button>
									<a class="btn btn-default" href="<?php echo base_url();?>admin/dashboard/manage/Lead-View"> Cancel</a>
									<?php } else {?>
									<button class="btn btn-primary trigger-btn" type="button" value="submit_button">Save</button>
                                    <button class="btn btn-primary trigger-btn" type="button" value="assign_button">Save & Assign</button>
									<a class="btn btn-default" href="<?php echo base_url();?>admin/dashboard/manage/Lead-View"> Cancel</a>
									<?php }?>
								</div>
								<div class="clearfix"></div>
							</div>
							<!--/porlets-content-->
						</div>
						<!--/block-web-->
					</div>
					<!--/col-md-6-->
				</div>
				<!--/row-->
			</div>
			<!--/page-content end-->
		</div>
		<script>
			$(document).on("click", ".trigger-btn", function(){
				var btn_name= $(this).val();
				if(btn_name == ""){
					$("#submitBtn").trigger("click");
				}else{
					if (btn_name == "closed_incomplete") {
						if (confirm('Are you sure you want to perform this action?')) {
							$("button#" + btn_name).trigger("click");
						}else{
							return false;
						}
					}else{
						$("button#" + btn_name).trigger("click");
					}
				}
			});

			function validatePhone() {
				var home_phone = $('#home_phone');
				var work_phone = $('#work_phone');
				var mobile = $('#mobile');
				if (home_phone.val() == '' && work_phone.val() == '' && mobile.val() == '') {
					alert('Please enter a phone no!');
					home_phone.focus();
					return false;
				}
			}
            
            function fetchPrice() {
                var market = $('[name="market"]').val();
                var case_type = $('[name="case_type"]').val();
                $('.page_loader').fadeIn();
                $.post('<?php echo base_url('admin/dashboard/fetch_price'); ?>', {market: market, case_type: case_type}, function(data){
                    $('[name="price"]').val(data);
                    $('.page_loader').fadeOut();
                });
            }
            
			$(document).ready(function () {
				$(document).on('click', '.spanclass a', function () {
					$(this).parent().remove();
				});
                
                $('#addNote').on('click', function(){
    				var note = $('#admin_notes').val();
    				if(note !== ''){
    					$.ajax({
    						url: '<?php echo base_url(); ?>admin/leads/addNotesAdmin/<?php echo $lead_id ?>',
    						type: 'post',
    						data: {
    							notes: note,
    						},
    						success: function(data){
    							window.location.reload();
    						}
    					});
    				} else {
    					bootbox.alert({
    					   size: 'small',
                           message: 'Note field cannot be empty',
    					});
    				}
    			});
                
                $('#addNoteLawyer').on('click', function(){
    				var note = $('#admin_notes').val();
    				if(note !== ''){
    					$.ajax({
    						url: '<?php echo base_url(); ?>admin/leads/addNotesAdmin/<?php echo $lead_id ?>',
    						type: 'post',
    						data: {
    							notes: note,
    							lawyer_note: 'lawyer_note'
    						},
    						success: function(data){
    							window.location.reload();
    						}
    					});
    				} else {
    					bootbox.alert({
    					   size: 'small',
                           message: 'Note field cannot be empty',
    					});
    				}
    			});
                
                $('[name="jurisdiction"]').change(function(e){
    			    var market = $(this).find("option:selected").data('market');
                    $('[name="market"]').val(market);
    			});
                
                $('[name="market"], [name="case_type"], [name="jurisdiction"]').change(function(e){
                    fetchPrice();
                });
                
			});
		</script>