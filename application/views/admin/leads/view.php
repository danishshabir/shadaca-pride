<?php $lead_id = $this->uri->segment(4);
$user_role_id = $this->session->userdata('role');
?>
<div id="main-content">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <h2>Lead <?php echo $lead->lead_status == 1 ? "Assignment" : "Reassignment"; ?></h2>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="block-web">
                    <div class="header">
                       
                        <div class="actions">
                            <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
                            <a class="refresh" href="#"><i class="fa fa-repeat"></i></a>
                            <a class="close-down" href="#"><i class="fa fa-times"></i></a>
                        </div>
                        <?php if(!in_array($lead->lead_status, array("3","11","12","13","14","20"))){ ?>
                        <?php if ($this->session->userdata('role') == 1) { ?>
                        <a href="<?php echo base_url(); ?>admin/leads/edit/<?php echo $lead_id; ?>">
                            <button class="btn btn-primary edit" type="submit">Edit Leads</button>
                        </a>
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#duplicateLead">
                            <button class="btn btn-primary edit" onclick="$('.duplicateLeadSaveBtn').attr('disabled', false);$('.duplicate_response_msg').html('');$('.duplicateLeadForm')[0].reset();">
                               Duplicate
                           </button>
                       </a>
                       <?php } ?>
                       <?php } ?>
                       <h3 class="content-header">
                        Lead <?php echo $lead->lead_status == 1 ? "Assignment" : "Reassignment"; ?>
                    </h3>
                    <h3 class="text-center">
                      <span style = "color: green;" id = "message"></span>
                      <?php if ($this->session->flashdata('message')) {
                        echo $this->session->flashdata('message');
                    } ?>
                </h3>
            </div>
            <div class="porlets-content two-col">
                <div class="row">
                    <div class="col col-md-6">
                        <div class="row">
                            <div class="col-md-6 subcol">
                                <label>First name:</label>
                                <span class="desp"><?php echo $lead->first_name; ?>&nbsp;</span>
                            </div>
                            <div class="col-md-6 subcol">
                                <label>Last name:</label>
                                <span class="desp"><?php echo $lead->last_name; ?>&nbsp;</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 subcol">
                                <label>Mobile no:</label>
                                <span class="desp"><?php echo $lead->mobile; ?>&nbsp;</span>
                            </div>
                            <div class="col-md-6 subcol">
                                <label>Email:</label>
                                <span class="desp"><?php echo $lead->email; ?>&nbsp;</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 subcol">
                                <label>Caller name:</label>
                                <span class="desp"><?php echo $lead->caller_name; ?>&nbsp;</span>
                            </div>
                            <div class="col-md-6 subcol">
                                <label>Caller ID:</label>
                                <span class="desp"><?php echo $lead->caller_id; ?>&nbsp;</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 subcol">
                                <label>Home phone:</label>
                                <span class="desp"><?php echo $lead->home_phone; ?>&nbsp;</span>
                            </div>
                            <div class="col-md-6 subcol">
                                <label>Work phone:</label>
                                <span class="desp"><?php echo $lead->work_phone; ?>&nbsp;</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 subcol">
                                <label>City:</label>
                                <span class="desp"><?php echo $lead->city; ?>&nbsp;</span>
                            </div>
                            <div class="col-md-6 subcol">
                                <label>State:</label>
                                <span class="desp"><?php echo getStateShortName($lead->state); ?>&nbsp;</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 subcol">
                                <label>Jurisdiction:</label>
                                <span class="desp">
                                  <?php $newJurisdiction = getJurisdictionName($lead->jurisdiction);
                                  echo $newJurisdiction[0]['name']; ?>&nbsp;
                              </span>
                          </div>
                          <div class="col-md-6 subcol">
                            <label>Market:</label>
                            <span class="desp">
                             <?php
                             $market_name = getMarketName($lead->market);
                             $stateName = getStateName($market_name->state);
                             $stateName[0]['state_short_name'];
                             echo $market_name->market_name . "-" . $stateName[0]['state_short_name'];
                             ?>&nbsp;
                         </span>
                     </div>
                     <div class="clearfix"></div>
                     <div class="col-md-6 subcol">
                        <label>Case ID:</label>
                        <span class="desp"><?php echo $lead->lead_id_number; ?>&nbsp;</span>
                    </div>
                    <div class="col-md-6 subcol">
                        <label>Case type:</label>
                        <span class="desp"><?php $caseTypeName = getCaseTypeName($lead->case_type);
                        echo $caseTypeName[0]['type']; ?>&nbsp;</span>
                    </div>
                    <div class="col-md-6 subcol">
                      <label>Lead price:</label>
                      <span class="desp">$<?php echo number_format($lead->price, 2);?>&nbsp; </span>
                  </div>
                  <div class="clearfix"></div>
                                    <div class="col-md-6 subcol">
                                        <label>Legal issue:</label>
                                        <span class="desp"><?php echo $lead->case_description; ?>&nbsp;</span>
                                    </div>
                                    <div class="col-md-6 subcol">
                                        <label>Case ref #:</label>
                                        <span class="desp"><?php echo $lead_id; ?>&nbsp;</span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 subcol">
                                        <label>Date/time:</label>
                                        <span class="desp">
                                         <?php
                                         $d = $lead->created_at;
                                         $date = strtotime($d);
                                         $new_date = date('m-d-Y h:i:s A', $date);
                                         ?>
                                         <?php echo $new_date; ?>&nbsp;
                                     </span>
                                 </div>
                                 <div class="clearfix"></div>
                                 <div class="col-md-12 subcol">
                                    <label>Intake notes:</label>
                                    <span class="spanclass desp">
                                      <?php echo $lead->my_notes; ?>
                                  </span>
                              </div>
                          </div>
                      </div>
                      <?php if ($this->session->userdata('role') == 1) { ?>
                      <div class="col col-md-6">
                        <?php if($lead->is_archive == 0){ ?>
                        <?php if(!(in_array($lead->lead_status, array("3", "11","12","13","14","20")))){ ?>
                        <div class="form-group">
                            <?php if ($this->session->flashdata('note_message')) {
                                echo $this->session->flashdata('note_message');
                            } ?>
                            <h3>Admin notes</h3>
                            <form
                            action="<?php echo base_url(); ?>admin/leads/addNotesAdmin/<?php echo $id; ?>"
                            method="post">
                            <textarea rows="5" name="notes" class="form-control txt-area"></textarea>
                            <button class="btn btn-primary" type="submit">Add Admin Note</button>
                            <?php if($lead->lead_status != 1 && $lead->lead_status != 4 && $lead->lead_status != 9){ ?>
                            <input type="submit" class="btn btn-primary" name="lawyer_note" value="Add Lawyer Note"/>
                            <?php } ?>
                        </form>
                    </div>
                    <?php } ?>  
                    <?php } ?>  
                </div>
                <div class="clearfix"></div>
                <div class="col col-md-6">
                    <div class="form-group">
                        <h3>Lawyer notes history</h3>
                        <div class="textFieldArea">
                         <?php if($lawyers_notes) { ?>
                         <?php foreach($lawyers_notes as $row) { ?>
                         <?php if($row->law_firm !== null){
                            $name = $row->law_firm;
                        }else if($row->staff_first_name !== null && $row->staff_last_name !== null){
                            $name = $row->staff_first_name . ' ' . $row->staff_last_name;
                        }else if($row->admin_first_name !== null && $row->admin_last_name !== null){
                         $name = $row->admin_first_name . ' ' . $row->admin_last_name . ' - Admin';
                     }else{
                        $name = $row->first_name . ' ' . $row->last_name;
                    } ?>
                    <div class="note-single">
                      <span>
                         <?php echo date('m-d-Y h:i:s A', strtotime($row->date)) . ' - ' . $name; ?>
                     </span>
                     <p><?php echo $row->notes;?></p>
                 </div>
                 <?php } ?>
                 <?php } ?>
             </div>
         </div>
     </div>
     <div class="col col-md-6">
        <div class="subcol">
            <h3>Admin notes history</h3>
            <div class="textFieldArea">
             <?php if($Admin_notes) { ?>
             <?php foreach($Admin_notes as $row) { ?>
             <?php
             $name = $row->first_name . ' ' . $row->last_name;
             ?>
             <div class="note-single">
              <span><?php echo date('m-d-Y h:i:s A', strtotime($row->date)) . ' - ' . $name; ?></span>
              <p><?php echo $row->notes;?></p>
          </div>
          <?php } ?>
          <?php } ?>
      </div>
  </div>
</div>
<div class="clearfix"></div>
<div class="col col-md-6">
   <h3>Actions</h3>
   <div class="textFieldArea-2 add">
										<!--<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span>
											Lead reached (non-communication)
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span>
											Closed-no show
										</label>-->
										<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span>
											Lead reached (communication)
										</label>
										<!--
										<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span>
											Consultation scheduled
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span>
											Consultation completed
										</label>-->
										<label class="checkbox-inline">
											<input type="checkbox" value="Lead Retained" disabled="disabled">
											<span class="custom-checkbox"></span> Lead retained
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" value="Lead Not Retained" disabled="disabled">
											<span class="custom-checkbox"></span> Lead not retained
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" disabled="disabled">
											<span class="custom-checkbox"></span> Refer to another/declined
										</label>
									</div>
								</div><!--checked="checked"-->
                                <?php } ?>
                                <?php if ($user_role_id == 1)
                                { ?>
                                    <div class="form-group col-md-6">
                                        <h3>Activities</h3>
                                        <div class="textFieldArea box-activities">
                                            <?php if (!empty($activities)) {
                                              foreach ($activities as $activity) {
                                                 if($activity->law_firm !== null){
                                                    $name = $activity->law_firm;
                                                }else if($activity->staff_first_name !== null && $activity->staff_last_name !== null){
                                                    $name = $activity->staff_first_name . ' ' . $activity->staff_last_name;
                                                }else if($activity->admin_first_name !== null && $activity->admin_last_name !== null){
                                                    $in_array = array(
                                                        "Reminder Email Sent",
                                                        "Contact Attempted",
                                                        "Lead Reached (communication)",
                                                        "Lead Retained",
                                                        "Lead Not Retained",
                                                        "Declined",
                                                        "Entry Updated",
                                                    );
                                                    $name = $activity->admin_first_name . ' ' . $activity->admin_last_name;
                                                    if(in_array($activity->event, $in_array)) $name .= " (Admin)";
                                                }else{
                                                    $name = $activity->lawyer_first_name . ' ' . $activity->lawyer_last_name;
                                                }
                                                $reason = getDeclinedReason($activity->lead_id, $activity->lawyer_id);
                                                if($activity->event != "Entry Duplicated") {
                                                    echo '<div class="note-single">';
                                                    if ($activity->event == "Entry Assigned Lawyer"  || $activity->event == "Entry Reassigned Lawyer" || $activity->event == "Entry Unassigned Lawyer" && $id != '') {
                                                       echo $activity->event . " (" . $name . ') at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at));
                                                   }else if($activity->event == 'Entry Created') {
                                                       echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at));
                                                   }else if($activity->via){
                                                    echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at)) . ' via ' . $activity->via . ' by ' . $name;
                                                } else {
                                                    echo $activity->event . ' at ' . $newDate = date("m-d-Y h:i:s A", strtotime($activity->created_at)) . ' by ' . $name;
                                                    echo $reason->reason != '' && $activity->event == 'Declined' ? '<br/>' . 'Reason: ' . $reason->reason : '';
                                                }
                                                echo '</div>'; 
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($this->session->userdata('role') == 1 && $lead->is_archive == 0 && !(in_array($lead->lead_status, array("3", "11","12","13","14","20")))) { ?>
                            <div class="col-xs-12">
                               <?php
                               $form_action = '';
                               if (!$user_id) {
                                  $form_action = "assignLawyer/" . $id;
                              } elseif ($user_id) {
                                  $form_action = "reassignLawyer/" . $id;
                              }
                              ?>
                              <form action="<?php echo base_url(); ?>admin/leads/<?php echo $form_action; ?>" id="assign_frm" method="post" parsley-validate novalidate>
                                  <div class="row" style="margin-bottom: 30px;">
                                     <?php if ($user_id) { ?>
                                     <div class="form-group col-md-4">
                                       <h3>Assignment</h3>
                                       <?php
                                       $assigned_lawyers = '';
                                       foreach ($assign_lawyers as $lawyer) {
                                          $assigned_lawyers .= $lawyer->name . " " . $lawyer->lname . " (" . $lawyer->law_firm_name . ") " . ' & ';
                                      }
                                      $assigned_lawyers = rtrim($assigned_lawyers, ' & '); ?>
                                      <?php
                                      foreach ($assign_lawyers as $lawyer) { ?>
                                      <input type="hidden" name="lawyer_hidden_id[]"
                                      value="<?php echo $lawyer->id ?>">
                                      <?php } ?>
                                      <?php if ($assigned_lawyers) { ?>
                                      <label class="checkbox-inline">
                                         <input type="checkbox" id="inlinecheckbox1" checked="checked" disabled>
                                         <span class="custom-checkbox"></span> <?php echo $assigned_lawyers; ?>
                                     </label>
                                     <?php } ?>
                                     <label class="checkbox-inline">
                                      <input type="checkbox" name="reassign_lawyer"
                                      value="1" <?php if ($lead->lead_status == 3) { ?> disabled="disabled" <?php } ?>
                                      id="inlinecheckbox" class="reassign_lawyer">
                                      <span class="custom-checkbox"></span> Reassign to other lawyer
                                  </label>
                                  <div class="row">
                                      <div class="form-group col-md-12 reassign_div" style="display:none;">
                                          <br/>
                                          <div class="clearfix"></div>
                                          <label>Suggested lawyers</label>
                                          <?php if (!empty($lawyers)) {
                                             foreach ($lawyers as $lawyer) {
                                                $check_arry[] = $lawyer->id; ?>
                                                <label class="checkbox-inline" style="font-weight: bold;">
                                                   <input type="radio" value="<?php echo $lawyer->id; ?>" name="lawyer_id" id="inlinecheckbox1">
                                                   <span class="custom-checkbox"></span> <?php echo ' ' . $lawyer->name . ' ' . $lawyer->lname . ' - ' . getLawsNames($lawyer->law_firms) . ' ' . ($lawyer->on_holiday == 1 ? ' (On vacation) ' : ''); ?>
                                               </label>
                                               <?php $lawyer_leads = getLawyersLeads($lawyer->id);
                                               if (!empty($lawyer_leads)) { ?>
                                               <table
                                               class="display table  dataTable hmz">
                                               <thead>
                                                  <tr>
                                                     <th style="font-weight: normal;">Last Lead Name</th>
                                                     <th style="font-weight: normal;">Assign Date</th>
                                                 </tr>
                                             </thead>
                                             <tbody>
                                              <?php foreach ($lawyer_leads as $leads) { ?>
                                              <tr>
                                                <td style="padding: 0px 8px; line-height: normal;"><?php echo $leads->first_name . ' ' . $leads->last_name; ?></td>
                                                <td style="padding: 0px 8px; line-height: normal; text-align: left;"><?php echo date('m-d-Y', strtotime($leads->assign_date)); ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table><br/>
                                    <?php }
                                }
                            } ?>
                            <br/>
                            <label>Others</label>
                            <select name="other_lawyer_id">
                             <option value="">Select</option>
                             <?php foreach ($lawyers_all as $lawyer) {
                                if (!in_array($lawyer->id, $check_arry)) { ?>
                                <option value="<?php echo $lawyer->id; ?>">
                                  <?php echo $lawyer->name . " " . $lawyer->lname . " - " . getLawsNames($lawyer->law_firms) . ($lawyer->on_holiday == 1 ? ' (On vacation) ' : ''); ?>
                              </option>
																	<!--<label class="checkbox-inline">
																		<input type="radio" value="<?php echo $lawyer->id; ?>" name="lawyer_id" id="inlinecheckbox1">
																		<span class="custom-checkbox"></span> <?php echo $lawyer->name . " " . $lawyer->lname . " - " . getLawsNames($lawyer->law_firms) . ($lawyer->on_holiday == 1 ? ' (On vacation) ' : ''); ?>
																	</label>-->

                                                                 <?php }}?>
                                                             </select>
                                                             <div class="clearfix"></div>
                                                         </div>
                                                     </div>
                                                     <label class="checkbox-inline">
                                                      <input type="checkbox" name="unassign_lawyer" value="1" class="unassign_lawyer" id="inlinecheckbox1" <?php if ($lead->lead_status == 3) { ?> disabled="disabled" <?php } ?>>
                                                      <span class="custom-checkbox"></span> Unassign
                                                  </label>
                                              </div>
                                              <?php } elseif (!$user_id) { ?>
                                              <div class="form-group col-md-4">
                                               <label>Assign to</label><br/>
                                               <label>Suggested lawyers</label>
                                               <?php
                                               if (!empty($lawyers)) {

                                                  foreach ($lawyers as $lawyer) {

                                                     $check_arry[] = $lawyer->id; ?>
                                                     <label class="checkbox-inline" style="font-weight: bold;">
                                                        <input type="radio" value="<?php echo $lawyer->id; ?>" name="lawyer_id" id="inlinecheckbox1">
                                                        <span class="custom-checkbox"></span> <?php echo $lawyer->name . ' ' . $lawyer->lname . ' - ' . getLawsNames($lawyer->law_firms) . ($lawyer->on_holiday == 1 ? ' (On vacation) ' : ''); ?>
                                                    </label>
                                                    <?php $lawyer_leads = getLawyersLeads($lawyer->id);
                                                    if (!empty($lawyer_leads)) { ?>
                                                    <table
                                                    class="display table  dataTable hmz">
                                                    <thead>
                                                       <tr>
                                                          <th style="font-weight: normal;">Last Lead Name</th>
                                                          <th style="font-weight: normal;">Assign Date</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody>
                                                   <?php foreach ($lawyer_leads as $leads) { ?>
                                                   <tr>
                                                     <td style="padding: 0px 8px; line-height: normal;"><?php echo $leads->first_name . ' ' . $leads->last_name; ?></td>
                                                     <td style="padding: 0px 8px; line-height: normal; text-align: left;"><?php echo date('m-d-Y', strtotime($leads->assign_date)); ?></td>
                                                 </tr>
                                                 <?php } ?>
                                             </tbody>
                                         </table><br/>
                                         <?php }
                                     }

                                 } ?>
                                 <br/>
                                 <label>Others</label>
                                 <select name="other_lawyer_id">
                                  <option value="">Select</option>
                                  <?php foreach ($lawyers_all as $lawyer) {
                                     if (!in_array($lawyer->id, $check_arry)) { ?>
                                     <option value="<?php echo $lawyer->id; ?>">
                                       <?php echo $lawyer->name . " " . $lawyer->lname . " - " . getLawsNames($lawyer->law_firms) . ($lawyer->on_holiday == 1 ? ' (On vacation) ' : ''); ?>
                                   </option>
																<!--<label class="checkbox-inline">
																	<input type="radio" value="<?php echo $lawyer->id; ?>" name="lawyer_id" id="inlinecheckbox1">
																	<span class="custom-checkbox"></span> <?php echo $lawyer->name . " " . $lawyer->lname . " - " . getLawsNames($lawyer->law_firms) . ($lawyer->on_holiday == 1 ? ' (On vacation) ' : ''); ?>
																</label>-->

                                                              <?php }}?>
                                                          </select>
                                                      </div>
                                                      <?php } ?>
                                                  </div>
                                                  <div
                                                  class="form-group col-md-12 submit_button">
                                                  <button id="assign_w_email" class="btn btn-primary" type="submit">Submit And Send Email</button>
                                                  <input type="submit" name="assign_wo_email" value="Submit Without Email" class="btn btn-primary"> 
                                                  <button type="button" onclick="if(confirm('Are you sure you want to perform this action?')){ document.getElementById('change_status').value = this.value; document.getElementById('assign_frm').submit();}else{false;}" value="closed_unassigned" class="btn btn-primary">Closed - Unassigned</button>                                       
                                                  <button type="button" onclick="if(confirm('Are you sure you want to perform this action?')){ document.getElementById('change_status').value = this.value; document.getElementById('assign_frm').submit();}else{false;}" value="closed_declined" class="btn btn-primary">Closed - Declined</button>
                                                  <input type="hidden" name="change_status" class="form-control" id="change_status" />
                                              </div>
                                          </form>
                                      </div>
                                      <?php } ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <!-- Modal for duplicate lead -->
          <div class="modal fade" id="duplicateLead" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Duplicate Lead</h4>
                    </div>
                    <form action="<?php echo base_url(); ?>/admin/leads/duplicateLead" method="post" class="duplicateLeadForm"
                      onsubmit="return false;">
                      <div class="modal-body">
                        <p>Select area of law</p>

                        <select class="form-control" name="case_type" id="case_type_id" required>
                            <option value="" disabled selected>Select</option>
                            <?php foreach ($case_type as $case) { ?>
                            <option value="<?php echo $case->id; ?>"> <?php echo $case->type; ?> </option>
                            <?php } ?>
                        </select>
                        <input type="hidden" name="dup_lead_id" value="<?php echo $lead_id; ?>">
                        <br>
                        <span class="duplicate_response_msg" style="height: 100px;"></span>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary duplicateLeadSaveBtn" id = "refduplicate">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
        <!-- Modal for duplicate lead -->
        <div class="modal fade" id="nextDeclinedModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Select Action</h4>
                    </div>
                  <div class="modal-body">
                    <p>The lead has been re-assigned to the selected lawyer, please select an option below to continue:</p>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo base_url('admin/dashboard/manage'); ?>" class="btn btn-primary">Return to main table (leads)</a>
                    <a href="<?php echo $next_declined_lead_id ? base_url("admin/leads/view/$next_declined_lead_id") : 'javascript:void(0);'; ?>" class="<?php echo $next_declined_lead_id ? '' : 'disabled '; ?>btn btn-primary">Go to next declined lead</a>
                </div>
            </div>
        </div>
    </div>
    <script>
/* $('#refduplicate').click(function() {
	//alert($('#case_type_id').val());
	if($('#case_type_id').val() != null)
	 $('#duplicateLead').modal('toggle');
 $('#message').html('Referal duplicated successfully');
       // history.go(-1);
 
    
   }); */
   <?php if(in_array($lead->lead_status, array('9', '15'))) { ?>
   $('[name="assign_wo_email"]').click(function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        var url = form.attr('action');
        var d = form.serialize() + '&from_sajax=1&assign_wo_email=Submit%20Without%20Email';
        bootbox.confirm({
            size: 'small',
            title: 'Confirm',
            message: 'Are you sure you want to perform this action?',
            callback: function(result) {
                if(result) {
                    $('.page_loader').show();
                    $.post(url, d, function(data){
                        $('.page_loader').hide();
                        $('.submit_button button, .submit_button input, .submit_button a').attr('disabled', true);
                        $('#nextDeclinedModal').modal({
                            backdrop: 'static',
                        });
                        $('#nextDeclinedModal').modal('show');
                        $('#nextDeclinedModal').on('hidden.bs.modal', function(){
                            window.location.href = '<?php echo base_url('admin/dashboard/manage'); ?>';
                        });
                    });
                }else{
                    bootbox.hideAll();
                    return false;
                }
            }
        });
   });
   
   $('#assign_w_email').click(function(e) {
        e.preventDefault();
        var form = $(this).parents('form');
        var url = form.attr('action');
        var d = form.serialize() + '&from_sajax=1';
		//if (!$(".unassign_lawyer").is(':checked') && !$('.reassign_lawyer').is(':checked')) {
		  
          bootbox.confirm({
            size: 'small',
            title: 'Confirm',
            message: 'You assigned lead to lawyer and an email will be sent to lawyer. Press Ok to confirm!',
            callback: function(result) {
                if(result) {
                    $('.page_loader').show();
                    $.post(url, d, function(data){
                        $('.page_loader').hide();
                        $('.submit_button button, .submit_button input, .submit_button a').attr('disabled', true);
                        $('#nextDeclinedModal').modal({
                            backdrop: 'static',
                        });
                        $('#nextDeclinedModal').modal('show');
                        $('#nextDeclinedModal').on('hidden.bs.modal', function(){
                            window.location.href = '<?php echo base_url('admin/dashboard/manage'); ?>';
                        });
                    });
                }else{
                    bootbox.hideAll();
                    return false;
                }
            }
        });
        
		//}
	});
    
    <?php }else{ ?>
   
   $('#assign_w_email').click(function(e) {
        var form = $(this).parents('form');
		  form.submit();
	});
    <?php } ?>
   
   $('[name="other_lawyer_id"]').on('change', function(){
       if($(this).val() != ''){
          $('input[name="lawyer_id"]').prop('checked', false);
      }
  });
   $('[name="lawyer_id"]').on('click', function(){
       if($(this).is(':checked')){
          $('[name="other_lawyer_id"] option[value=""]').prop('selected', true);
      }
  });
</script>