<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->userdata('role') == 6){ ?>
				<h2>Law Firm Dashboard</h2>
				<?php }else if($this->session->userdata('role') == 7) { ?>
				<h2>Lawyer Staff Dashboard</h2>
				<?php }else{ ?>
				<h2>Not Retained/Declined Leads</h2>
				<?php } ?>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">

					<?php echo $this->session->flashdata('message'); ?>
					<!--<div class="form-group col-md-3">
						<div class="form-group">
							<select  placeholder="state" class="form-control searchByStatus" name="status" id="status" >
								<option value=""> Select Status </option>
								<option value="<?php echo "All";?>" <?php if($this->uri->segment(4) == 'All'){ echo "selected"; }?>> <?php echo "All";?> </option>
								<option value="<?php echo "New";?>" <?php if($this->uri->segment(4) == 'New'){ echo "selected"; }?>> <?php echo "New";?> </option>
								<option value="<?php echo "Opened";?>" <?php if($this->uri->segment(4) == 'Opened'){ echo "selected"; }?>> <?php echo "Opened";?> </option>
								<option value="<?php echo "Contacted";?>" <?php if($this->uri->segment(4) == 'Contacted'){ echo "selected"; }?>> <?php echo "Contacted";?> </option>
								<!--<option value="<?php echo "NotRetained";?>" <?php if($this->uri->segment(4) == 'NotRetained'){ echo "selected"; }?>> <?php echo "Not Retained";?> </option>-->
								<!--<option value="<?php echo "Retained";?>" <?php if($this->uri->segment(4) == 'Retained'){ echo "selected"; }?>> <?php echo "Retained";?> </option>
								<option value="<?php echo "Completed";?>" <?php if($this->uri->segment(4) == 'Completed'){ echo "selected"; }?>> <?php echo "Completed";?> </option>
							</select>
						</div>
					</div>-->
					<div class="clearfix"></div>
					<div class="tab-content">
						<div class="tab-pane cont active" id="leads">
							<div class="table-responsive">
								<table  class="display table table-bordered table-striped dynamic-table" id="has_datatable">
									<thead>
										<tr>
                                            <th>Status</th>
                                            <th>Case Id</th>
                                            <th>Lawyer Name</th>
											<th>Client name</th>
											<th>Caller name</th>
											<th>Case type</th>
											<th>Not Retained/Declined Date</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
                                    <?php if($leads) { ?>
                                        <?php foreach($leads as $lead){ ?>
                                        <tr>
                                            <td><?php echo $lead->status ? $lead->status : 'N/A'; ?></td>
                                            <td><?php echo $lead->case_id ? $lead->case_id : 'N/A'; ?></td>
                                            <td><?php echo $lead->lawyer_name ? $lead->lawyer_name : 'N/A'; ?></td>
                                            <td><?php echo $lead->client_name ? $lead->client_name : 'N/A'; ?></td>
                                            <td><?php echo $lead->caller_name ? $lead->caller_name : 'N/A'; ?></td>
                                            <td><?php echo $lead->case_type ? $lead->case_type : 'N/A'; ?></td>
                                            <td><?php echo date('F d, Y', $lead->action_date); ?></td>
                                            <td><a href="<?php echo base_url('admin/leads/not_retained_declined_view') . '/' . $lead->id; ?>">View</a></td>
                                        </tr>
                                        <?php } ?>
                                    <?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/row-->
	</div>
	<!--/page-content end-->
</div>
<!--/main-content end-->
</div>