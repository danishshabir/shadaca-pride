<div class="tab-pane cont active">
	<div class="export">
		<button type="button" id="exportCsv" data-name="leads" class="btn btn-primary btn-sm">Export CSV</button>
	</div>
    <div class="clearfix"></div>
	<div class="table-responsive">
		<table class="display table table-bordered table-striped" style="margin-top: 10px;">
            <caption>
                <div>
                    <ul style="margin: 5px 0px;" class="pagination pull-left">
                      <?php $uri = base_url("admin/dashboard/manage?") . $_SERVER["QUERY_STRING"]; ?>
                      <?php for($i = 1; $i <= $num_links; $i++){ ?>
                      <?php
                      $href = add_or_update_params($uri, "page", $i);
                      ?>
                        <li class="<?php echo isset($_GET["page"]) ? ($_GET['page'] == $i ? "active" : "") : ($i == 1 ? "active" : ""); ?>"><a href="<?php echo $href; ?>" <?php echo isset($_GET["page"]) ? ($_GET['page'] == $i ? 'class="btn-primary"' : "") : ($i == 1 ? 'class="btn-primary"' : ""); ?>><?php echo $i; ?></a></li>
                      <?php } ?>
                    </ul>
                    <div style="float: left;margin: 5px 10px;width: 20%;">
                        <select style="float: left; width: 50%;" class="form-control" id="records_per_page" onchange="change_per_page(this)">
                            <option value="10">10</option>
                            <option value="50">50</option>
                            <option value="100" <?php echo !($_GET["per_page"]) || $_GET["per_page"] == "" ? "selected" : ""; ?>>100</option>
                            <option value="200">200</option>
                            <option value="500">500</option>
                        </select>
                        <span style="margin: 5px 0px; float: right;">Records Per Page</span>
                    </div>
                </div>
            </caption>
			<thead>
				<tr>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "status_name" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="status_name" class="save_sorting"><a href="<?php custom_sort("status_name"); ?>">Status +<img src="<?php echo sort_image("status_name"); ?>"/></a></th>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "lead_id_number" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="lead_id_number" class="save_sorting"><a href="<?php custom_sort("lead_id_number"); ?>">Case Id<img src="<?php echo sort_image("lead_id_number"); ?>"/></a></th>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "lawfirm_name" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="lawfirm_name" class="save_sorting"><a href="<?php custom_sort("lawfirm_name"); ?>">Law Firm<img src="<?php echo sort_image("lawfirm_name"); ?>"/></a></th>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "lawyer_name" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="lawyer_name" class="save_sorting"><a href="<?php custom_sort("lawyer_name"); ?>">Lawyer Name<img src="<?php echo sort_image("lawyer_name"); ?>"/></a></th>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "lead_name" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="referral_name" class="save_sorting"><a href="<?php custom_sort("lead_name"); ?>">Lead Name<img src="<?php echo sort_image("lead_name"); ?>"/></a></th>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "casetype" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="casetype" class="save_sorting"><a href="<?php custom_sort("casetype"); ?>">Case Type +<img src="<?php echo sort_image("casetype"); ?>"/></a></th>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "market_name" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="market_name" class="save_sorting"><a href="<?php custom_sort("market_name"); ?>">Market +<img src="<?php echo sort_image("market_name"); ?>"/></a></th>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "date_received" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="date_received" class="save_sorting"><a href="<?php custom_sort("date_received"); ?>">Date Received<img src="<?php echo sort_image("date_received"); ?>"/></a></th>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "date_assigned" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="date_assigned" class="save_sorting"><a href="<?php custom_sort("date_assigned"); ?>">Date Assigned<img src="<?php echo sort_image("date_assigned"); ?>"/></a></th>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "date_opened" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="date_opened" class="save_sorting"><a href="<?php custom_sort("date_opened"); ?>">Date Opened<img src="<?php echo sort_image("date_opened"); ?>"/></a></th>
					<th aria-sort="<?php echo isset($_GET["sort"]) && $_GET["name"] == "date_completed" ? ($_GET["sort"] == "ASC" ? "descending" : "ascending") : "ascending"; ?>" aria-column="date_completed" class="save_sorting"><a href="<?php custom_sort("date_completed"); ?>">Date Completed<img src="<?php echo sort_image("date_completed"); ?>"/></a></th>
					<th class="save_sorting">Action</th>
				</tr>
			</thead>
			<tbody>            
				<?php if (!empty($leads)) {
					foreach ($leads as $lead) {
						if ($lead->lead_status != 1) {
							$assigned_detail = getLeadsLawyer($lead->id);
							$assigned_lawyer_law_firm = getLeadsLawyerLawfirm($lead->id);
						}
						$datetime1 = strtotime($lead->created_at);
						$datetime2 = strtotime(date("Y-m-d H:i:s"));
						$interval = abs($datetime2 - $datetime1);
						$minutes = round($interval / 60);
						if ($lead->lead_status == 1) {
							if ($minutes >= 30 && $minutes < 60) {
								$class = 'yellow_simple';
							} elseif ($minutes >= 60) {
								$class = 'red_simple';
							} else {
								$class = 'green_simple';
							}
						} elseif ($lead->lead_status != 1 && $lead->lead_status != 4) {
							$class = 'black';
						} elseif ($lead->lead_status == 4) {
							$class = 'blue_simple';
						}
						
						/* Assigned To */
						if($lead->lead_status != 1 && $lead->lead_status != 4){
							$assigned_to_firm = $assigned_lawyer_law_firm['assigned_to'] != "" ? $assigned_lawyer_law_firm['assigned_to'] : "N/A";
						} else {
							$assigned_to_firm = 'Not Assigned';
						}
						if($lead->lead_status != 1 && $lead->lead_status != 4){
							$assigned_to = $assigned_detail['assigned_to'] != "" ? $assigned_detail['assigned_to'] : "N/A";
						} else {
							$assigned_to = 'Not Assigned';
						}
						?>
						<tr class="<?php echo $class; ?>">
							<td><?php echo $lead->status_name; ?></td>
							<td><?php echo $lead->lead_id_number; ?></td>
							<td><?php echo $assigned_to_firm; ?></td>
							<td><?php echo $assigned_to; ?></td>
							<td><?php echo $lead->first_name . ' ' . $lead->last_name; ?></td>
							<td><?php echo $lead->casetype; ?></td>
							<td><?php $newJurisdiction = getMarketName($lead->market);
							$stateName = getStateName($newJurisdiction->state);
							$stateName[0]['state_short_name'];
							echo $newJurisdiction->market_name . " - " . $stateName[0]['state_short_name']; ?></td>
							<td><?php echo date('m-d-Y h:i:s A', strtotime($lead->created_at)); ?></td>
                            
							<?php if($lead->assigned_date) { ?>
							<td style="cursor:pointer; width:160px;">
								<?php echo date("m-d-Y h:i:s A", strtotime($lead->assigned_date)); ?>
							</td>
							<?php }else { ?>
							<td style="cursor:pointer; width:160px;">
								Not assigned
							</td>
							<?php } ?>
                            
							<?php
							$in_array = [2,3,5,6,8,7,11,12,13,14];
                            if(in_array($lead->lead_status, $in_array)){
                                if($lead->lead_open_datetime){                                
						    ?>
                            <td style="cursor:pointer; width:160px;">
								<?php echo  date("m-d-Y h:i:s A", strtotime($lead->lead_open_datetime)); ?>
							</td>                       
                            <?php }else{ ?>
                            <td style="cursor:pointer; width:160px;">
								Not open
							</td>
                            <?php }}else{ ?>
                            <td style="cursor:pointer; width:160px;">
								Not open
							</td>
                            <?php } ?>
                                                        
							<td>
								<?php echo $lead->date_closed != "" ? date("m-d-Y h:i:s A", strtotime($lead->date_closed)) : "N/A" ?>
							</td>
							<td style="cursor:pointer">
								<a href="<?php echo base_url(); ?>admin/leads/viewLead/<?php echo $lead->id; ?>">View</a>
                                <?php if(in_array($lead->lead_status, array("11", "13", "14"))){ ?>
                                | <a href="<?php echo base_url("admin/leads/reopen_lead/$lead->id"); ?>">Re-open Lead</a>
                                <?php } ?>
								<?php if ($lead->lead_status != 4) { ?>
								<?php if(!(in_array($lead->lead_status, array("3", "11","12","13","14")))){ ?>
								| <a href="<?php echo base_url(); ?>admin/leads/view/<?php echo $lead->id; ?>"> <?php if ($lead->lead_status != 1) { ?> Re-assign Lead <?php } else { ?> Assign Lead <?php } ?> </a>
								<?php } ?>
								<?php } ?>
								<?php if(!(in_array($lead->lead_status, array("3", "11","12","13","14")))){ ?>
								| <a href="<?php echo base_url(); ?>admin/leads/edit/<?php echo $lead->id; ?>">Edit</a>
								| <a data-toggle="modal" data-target="#myModal<?php echo $lead->id; ?>">Delete </a>
								<?php if ($lead->lead_status == 2 || $lead->lead_status == 8 || $lead->lead_status == 5 || $lead->lead_status == 7) { ?>                                |
								<a href="javascript:void(0);" onclick="if(confirm('Are you sure you want to send email reminder?')){window.location.href='<?php echo base_url(); ?>admin/leads/send_reminder/<?php echo $lead->id; ?>'}">Reminder Email</a>
								<?php } ?>
								<?php } ?>
							</td>
						</tr>
						<div class="modal fade" id="myModal<?php echo $lead->id; ?>" role="dialog">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Confirm Delete</h4></div>
										<div class="modal-body"><p>Are you sure you want to delete this record ?</p></div>
										<div class="modal-footer">
											<a class="btn btn-primary" href="<?php echo base_url(); ?>admin/leads/delete/<?php echo $lead->id; ?>">Yes </a>
											<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
										</div>
									</div>
								</div>
							</div>
							<?php }
						}else{ ?>
                        <tr>
                            <td class="text-center" colspan="12">No data available!</td>
                        </tr>
                        <?php } ?>
					</tbody>
                    <tfoot>
                        <tr>
                            <td colspan="12">
                                <div>
                                    <ul style="margin: 5px 0px;" class="pagination">
                                      <?php for($i = 1; $i <= $num_links; $i++){ ?>
                                      <?php
                                      $href = add_or_update_params($uri, "page", $i);
                                      ?>
                                        <li class="<?php echo isset($_GET["page"]) ? ($_GET['page'] == $i ? "active" : "") : ($i == 1 ? "active" : ""); ?>"><a href="<?php echo $href; ?>" <?php echo isset($_GET["page"]) ? ($_GET['page'] == $i ? 'class="btn-primary"' : "") : ($i == 1 ? 'class="btn-primary"' : ""); ?>><?php echo $i; ?></a></li>
                                      <?php } ?>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
				</table>
			</div>
		</div>
		<script type="text/javascript">
			$(document).on("click", ".save_sorting", function(){

				window.setCookie = true;
				var sort = $(this).attr("aria-sort");
				sort = sort == "" ? "ascending" : sort;
				var column = $(this).attr("aria-column");
				set_cookie("sort", sort, window.setCookie);
				set_cookie("column", column, window.setCookie);
			});
			$(document).ready(function(){
                var ccolumn = Cookies.get("column");
                var csort = Cookies.get("sort");

				$("th").click(function(){});
                
                if(ccolumn != "" && csort != ""){
    				setTimeout(function(){
    					if(csort == "ascending"){
    						$("th:contains("+ccolumn+")").click();
    					}else if(csort == "descending"){
    						$("th:contains("+ccolumn+")").click().click();
    					}
    				}, 900);
                }
			});
			function set_cookie(cookie_name, value, set){
			     Cookies.remove(cookie_name, { path: '/pride' });
				if(set == true){
				    Cookies.set(cookie_name, value, { expires: 30, path: '/pride' });
				}

			}
            function updateQueryStringParameter(uri, key, value) {
                if(uri.charAt(uri.length - 1) == "?"){
                    uri = uri.substring(0, uri.length - 1);
                }
              var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
              var separator = uri.indexOf('?') !== -1 ? "&" : "?";
              if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
              }
              else {
                return uri + separator + key + "=" + value;
              }
            }
            function change_per_page(ele){
                var value = ele.value;
                var url = updateQueryStringParameter('<?php echo $uri; ?>', 'per_page', value);
                window.location.href = url;
            }
		</script>
        <?php if(isset($_GET["per_page"]) && $_GET['per_page'] != ""){ ?>
            <script>
                document.getElementById("records_per_page").value = <?php echo $_GET['per_page']; ?>;
            </script>
        <?php } ?>