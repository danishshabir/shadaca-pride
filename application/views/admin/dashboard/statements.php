<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Statements</h2>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<?php echo $this->session->flashdata('message'); ?>
					<div class="clearfix"></div>
					<div class="tab-content">
						<div class="tab-pane cont active" id="leads">
							<div class="table-responsive">
								<table  class="table-hover display table table-bordered table-striped dynamic-table" id="has_datatable">
									<thead>
										<tr>
											<th>#</th>
											<th>Lawyer Name</th>
											<th>Law Firm</th>
											<th>Month</th>
											<th>Year</th>
											<th>Created At</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php if($statements && count($statements) > 0){ ?>
                                            <?php $i = 1; foreach($statements as $statement) { ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $statement->lawyer_name; ?></td>
                                                    <td><?php echo $statement->law_firm_name; ?></td>
                                                    <td><?php echo $statement->month; ?></td>
                                                    <td><?php echo $statement->year; ?></td>
                                                    <td><?php echo date('m-d-Y h:i A', strtotime($statement->created_at)); ?></td>
                                                    <td>
                                                        <a target="_blank" href="<?php echo base_url("admin/dashboard/view_statement_pdf/$statement->file_name"); ?>">View PDF</a>
                                                        <?php if(in_array($this->session->userdata('role'), array('1','6',)) && !($this->uri->segment(4))) { ?>
                                                        &nbsp;|&nbsp;
                                                        <a href="<?php echo base_url("admin/dashboard/statements/$statement->lawyer_id/history"); ?>">Past statements</a>
														<?php } 
														elseif(in_array($this->session->userdata('role'), array('2'))) {?>
														&nbsp;|&nbsp;
														<a href="<?php echo base_url("admin/dashboard/statements?p=history"); ?>">Past statements</a>
														<?php } ?> 
                                                    </td>
                                                </tr>
                                            <?php $i++; } ?>
                                        <?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/row-->
	</div>
	<!--/page-content end-->
</div>
<!--/main-content end-->
</div>