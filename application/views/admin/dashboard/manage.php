<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->userdata('role') == 6){ ?>
				<h2>Law Firm Dashboard</h2>
				<?php }else if($this->session->userdata('role') == 7) { ?>
				<h2>Lawyer Staff Dashboard</h2>
				<?php }else{ ?>
				<h2>Leads Dashboard</h2>
				<?php } ?>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<?php echo $this->session->flashdata('message'); ?>
					<div class="form-group col-md-3">
						<div class="form-group">
							<select placeholder="state" class="form-control searchByStatus" name="status" id="status" >
								<option value=""> Select Status </option>
								<option value="<?php echo "All";?>" <?php if($this->uri->segment(4) == 'All'){ echo "selected"; }?>> <?php echo "All";?> </option>
								<option value="<?php echo "New";?>" <?php if($this->uri->segment(4) == 'New'){ echo "selected"; }?>> <?php echo "New";?> </option>
								<option value="<?php echo "Opened";?>" <?php if($this->uri->segment(4) == 'Opened'){ echo "selected"; }?>> <?php echo "Opened";?> </option>
								<option value="<?php echo "Contacted";?>" <?php if($this->uri->segment(4) == 'Contacted'){ echo "selected"; }?>> <?php echo "Contacted";?> </option>
								<!--<option value="<?php echo "NotRetained";?>" <?php if($this->uri->segment(4) == 'NotRetained'){ echo "selected"; }?>> <?php echo "Not Retained";?> </option>-->
								<option value="<?php echo "Completed";?>" <?php if($this->uri->segment(4) == 'Completed'){ echo "selected"; }?>> <?php echo "Completed";?> </option>
								<option value="<?php echo "Declined";?>" <?php if($this->uri->segment(4) == 'Declined'){ echo "selected"; }?>> <?php echo "Declined";?> </option>
							</select>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="tab-content">
						<div class="tab-pane cont active" id="leads">
							<div class="table-responsive">
								<table  class="display table table-bordered table-striped dynamic-table" id="has_datatable">
									<thead>
										<tr>
											<th>Status</th>
											<th>Case Id</th>
											<th>Lawyer Name</th>
											<th>Client name</th>
											<th>Caller name</th>
											<th>Case type</th>
											<th>Assignment date</th>
											<?php if($this->uri->segment(4) == 'Completed'){ ?>
											<th>Date Completed</th>
											<?php } ?>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if(!empty($leads))
										{
											foreach($leads as $lead) { ?>
											<?php if($lead->lead_status != 6){ ?>
											<tr>
                                                <td>
													<?php
													if ($lead->lead_status == 1)
													{
														$leadStatus = 'Pending';
													} elseif ($lead->lead_status == 2)
													{
														//$leadStatus = 'Assigned';
														$leadStatus = 'New';
													} elseif ($lead->lead_status == 3)
													{
														$leadStatus = 'Case Completed - Retained';
													} elseif ($lead->lead_status == 4)
													{
														$leadStatus = 'Drafted';
													} elseif ($lead->lead_status == 5)
													{
														$leadStatus = 'Opened';
													} elseif ($lead->lead_status == 6)
													{
														$leadStatus = 'Not Retained';
													} elseif ($lead->lead_status == 8)
													{
														$leadStatus = 'Contacted';
													} elseif ($lead->lead_status == 15){
                                                        $leadStatus = 'Declined';
													}else{
														$leadStatus = 'Completed';
													}

													if ($lead->lead_status == 1 || $lead->lead_status == 2)
													{
														$openViewText = 'Open';
													}else{
														$openViewText = 'View';
													}
													$statusArray = array(3, 11, 12, 13, 14);
													?>
													<?php echo $leadStatus; ?>
												</td>
                                                <td><?php echo $lead->lead_id_number; ?></td>
                                                <td><?php echo $lead->name;?></td>
                                                <td><?php echo $lead->first_name.' '.$lead->last_name; ?></td>
                                                <td><?php echo $lead->caller_name;?></td>
                                                <td>
													<?php
													$caseTypeName =  getCaseTypeName($lead->case_type);
													echo $caseTypeName[0]['type']; ?>
												</td>
												<td><?php echo date('m-d-Y h:i:s A', strtotime($lead->created_at));?></td>
												<input type="hidden" id="lawyer_id" name="lawyer_id" value="<?php echo $this->session->userdata('id'); ?>"/>
												<input type="hidden" id="lead_id" name="lead_id" value="<?php echo $lead->id; ?>"/>
												<?php if($this->uri->segment(4) == 'Completed'){  ?>
												<td><?php echo $lead->date_closed == '' ? 'N/A' : date('M d, Y h:i a', strtotime($lead->date_closed)); ?></td>
												<?php } ?>
												<td>
													<span class="green-text">
														<a style="cursor:pointer" class="leadOpenDateTime makeBilled" data-type="open" data-billed-lawyer-id="<?php echo ($this->session->view_as == '' ? $lead->lawyer_to_leads_id : 0 )?>" data-url="<?php echo base_url().'admin/leads/view/'.$lead->id.($lead->lawyer_to_lead_unique_code != '' ? '/'.$lead->lawyer_to_lead_unique_code : '');?>"><?php echo $openViewText; ?></a>
													</span><?php if($this->session->view_as == '' && !(in_array($lead->lead_status, $statusArray))){ ?>|
													<a class="makeBilled" data-type="call" data-billed-lawyer-id="<?php echo $lead->lawyer_to_leads_id; ?>" href="tel: <?php echo $lead->mobile;?>">Call</a>&nbsp;|
													<a class="makeBilled" data-type="email" data-billed-lawyer-id="<?php echo $lead->lawyer_to_leads_id; ?>" href="mailto: <?php echo $lead->email;?>">Email</a>
													<?php } ?>
												</td>
											</tr>
											<?php } ?>
											<!--<tr>
												<td colspan="7">
													<strong>Case description: </strong><?php /*echo $lead->case_description;*/?>
												</td>
											</tr>-->
											<?php }
										} ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/row-->
	</div>
	<!--/page-content end-->
</div>
<!--/main-content end-->
</div>
<script>
	$(".searchByStatus")

	.change(function(e) {

		var status = $('#status')

		.val();

		if (status == 'All') {

			location.replace('<?php echo base_url(); ?>admin/dashboard/manage/All');

		} else if (status == 'New') {

			location.replace('<?php echo base_url(); ?>admin/dashboard/manage/New');

		} else if (status == 'Opened') {

			location.replace('<?php echo base_url(); ?>admin/dashboard/manage/Opened');

		} else if (status == 'NotRetained') {

			location.replace('<?php echo base_url(); ?>admin/dashboard/manage/NotRetained');

		} else if (status == 'Completed') {

			location.replace('<?php echo base_url(); ?>admin/dashboard/manage/Completed');

		}else if (status == 'Contacted') {

			location.replace('<?php echo base_url(); ?>admin/dashboard/manage/Contacted');

		}else if (status == 'Declined') {

			location.replace('<?php echo base_url(); ?>admin/dashboard/manage/Declined');

		}
		e.preventDefault();

	});
</script>