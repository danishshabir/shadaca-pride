<?php $session_id=$this->session->userdata('id'); ?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Lead View</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="clearfix"></div>
				<div class="clearfix"></div>
				<div class="tab-content">
					<div class="row">
						<div class="form-group col-md-2">
							<input type="text" name="case_id" id="case_id" parsley-trigger="change" placeholder="Enter Last 5 digits of Case Id" class="form-control searchByCaseId">
						</div>
						<div class="form-group col-md-2">
							<div class="form-group">
								<select placeholder="state" class="form-control" onchange="searchByStatus(this.value);" name="status" id="status" >
									<option value=""> Select Status </option>
									<option value="<?php echo "0";?>"> <?php echo "All";?> </option>
									<option value="<?php echo "1";?>"> <?php echo "Pending";?> </option>
									<option value="<?php echo "2";?>"> <?php echo "Assigned";?> </option>
									<option value="<?php echo "5";?>"> <?php echo "Opened";?> </option>
									<option value="<?php echo "8";?>"> <?php echo "Contacted";?> </option>
									<option value="<?php echo "6";?>"> <?php echo "Not-Retained";?> </option>
									<option value="<?php echo "10";?>"> <?php echo "Completed/Closed";?> </option>
									<option value="<?php echo "15";?>"> <?php echo "Declined";?> </option>
									<option value="<?php echo "4";?>"> <?php echo "Drafted";?> </option>
								</select>
							</div>
						</div>
						<div class="form-group col-md-2">
							<div class="form-group">
								<select placeholder="state" class="form-control" onchange="searchByCaseType(this.value)" name="case_type" id="case_type" >
									<option value=""> Select Case Type </option>
									<?php foreach($case_type as $c_type) {?>
									<option value="<?php echo $c_type->id;?>"> <?php echo $c_type->type;?> </option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group col-md-2">
							<div class="form-group">
								<select placeholder="state" class="form-control" onchange="searchByMarket(this.value)" name="market" id="market" >
									<option value=""> Select Market </option>
									<?php foreach($all_markets as $all_market) {?>
									<option value="<?php echo $all_market->id;?>"> <?php echo $all_market->market_name;?> </option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group col-md-2">
							<div class="form-group">
								<select placeholder="Search Type" class="form-control" onchange="advanceSearchType(this.value)" name="advance_search_type" id="advance_search_type" >
									<option value=""> Select Search Type </option>
									<option value="<?php echo "Lawyer";?>"> <?php echo "Search Lawyer";?> </option>
									<option value="<?php echo "Law_Firm";?>"> <?php echo "Search Law Firm";?> </option>
									<option value="<?php echo "Lead_Name";?>"> <?php echo "Search Lead Name";?> </option>
								</select>
							</div>
						</div>
						<div class="form-group col-md-2">
							<input type="text" name="text" id="text" parsley-trigger="change" placeholder="Enter Search Value " class="form-control advanceSearch">
						</div>
					</div>
					<?php 
					if($this->session->flashdata('message')){	
						echo $this->session->flashdata('message');
					} ?>
					<div class="lead-data">
						<?php $this->load->view('admin/dashboard/leads/leads'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/row-->
</div>
<!--/page-content end--> 
</div><!--/main-content end-->
</div>
<script>
	function load_dataTable()
	{
		$('#dynamic-table').dataTable( {
			"ordering": true,
			"iDisplayLength": 100,
			"aaSorting": [[1, "desc"]],
			"order": [[ 1, "desc" ]],
			"sDom": '<flp>rt<ip><"clear">',
		});

	}
	function date_sort_fix()
	{
		setTimeout(function () {
			$(".not-open").html('Not open');
		},200);
		$('#dynamic-table thead').on( 'click', 'tr th:eq(9)', function (){
			$(".not-open").html('');
			setTimeout(function () {
				$(".not-open").html('Not open');
			},100);
		});
		setTimeout(function () {
			$(".not-assigned").html('Not Assigned');
		},200);
		$('#dynamic-table thead').on( 'click', 'tr th:eq(8)', function (){
			$(".not-assigned").html('');
			setTimeout(function () {
				$(".not-assigned").html('Not Assigned');
			},100);

		});
	}
	function column_sort_fix(){
		var ccolumn = Cookies.get("column");
		var csort = Cookies.get("sort");

		$("th").click(function(){});

		if(ccolumn != "" && csort != ""){
			setTimeout(function(){
				if(csort == "ascending"){
					$("th:contains("+ccolumn+")").click();
				}else if(csort == "descending"){
					$("th:contains("+ccolumn+")").click().click();
				}
			}, 900);
		}
	}
	$(".advanceSearch")
	.keyup(function (e) {
		setTimeout(function() {
			var text = $('#text').val();

			/*
			 document.cookie = 'advanceSearch' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
			 			document.cookie = 'advanceSearch' + "=" + text + ";"
			 */
            
            Cookies.set('advanceSearch', text, { expires: 30, path: "/pride" });

			var advance_search_type = $('#advance_search_type').val();
			$(".page_loader").show();
			$.ajax({
				type: "POST"
				, url: '<?php echo base_url();?>admin/dashboard/advanceSearch'
				, data: {
					text: text
					, advance_search_type: advance_search_type
				}
				, success: function (response) {
					$(".page_loader").hide();
					var json = jQuery.parseJSON(response);
					$(".lead-data").html(json.html);
					//load_dataTable();
					//date_sort_fix();
					//column_sort_fix();
				}
			});
		}, 1000);
		e.preventDefault();
	});

	function advanceSearchType(advance_search_type){
		/*
		 document.cookie = 'advance_search_type' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		 		document.cookie = 'advance_search_type' + "=" + advance_search_type + ";"
		 */
        
        Cookies.set('advance_search_type', advance_search_type, { expires: 30, path: "/pride" });
		var text = $('#text').val();
		$(".page_loader").show();
		$.ajax({
			type: "POST"
			, url: '<?php echo base_url();?>admin/dashboard/advanceSearch'
			, data: {
				text: text
				, advance_search_type: advance_search_type
			}
			, success: function (response) {
				$(".page_loader").hide();
				var json = jQuery.parseJSON(response);
				$(".lead-data").html(json.html);
				//load_dataTable();
				//date_sort_fix();
				//column_sort_fix();
			}
		});
	}

	$(".searchByCaseId")
	.keyup(function (e) {
		var case_id = $('#case_id').val();
		$(".page_loader").show();
		$.ajax({
			type: "POST"
			, url: '<?php echo base_url();?>admin/dashboard/searchByCaseId'
			, data: {
				case_id: case_id
			}
			, success: function (response) {
				$(".page_loader").hide();
				var json = jQuery.parseJSON(response);
				$(".lead-data").html(json.html);
				//load_dataTable();
				//date_sort_fix();
				//column_sort_fix();
			}
		});
		e.preventDefault();
	});

	function searchByStatus(status){
		/*
		 document.cookie = 'status' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		 		document.cookie = 'status' + "=" + status + ";"
		 */
        
        Cookies.set('status', status, { expires: 30, path: "/pride" });
		$(".page_loader").show();
		$.ajax({
			type: "POST"
			, url: '<?php echo base_url();?>admin/dashboard/SearchByStatus'
			, data: {
				status: status,
			}
			, success: function (response) {
				$(".page_loader").hide();
				var json = jQuery.parseJSON(response);
				$(".lead-data").html(json.html);
				//load_dataTable();
				//date_sort_fix();
				//column_sort_fix();
			}
		});
	}
	function getCookie_value(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}
	$(document).ready(function(){
		
        /*
         var status = getCookie_value('status');
         		var case_type = getCookie_value('case_type');
         		var market = getCookie_value('market');
         		var advance_search_type = getCookie_value('advance_search_type');
         		var advanceSearch = getCookie_value('advanceSearch');
         */
        
        var status = Cookies.get('status');
        var case_type = Cookies.get('case_type');
        var market = Cookies.get('market');
        var advance_search_type = Cookies.get('advance_search_type');
        var advanceSearch = Cookies.get('advanceSearch');

		$('#status option[value="'+status+'"]').prop("selected",true);
		$('#case_type option[value="'+case_type+'"]').prop("selected",true);
		$('#market option[value="'+market+'"]').prop("selected",true);
		$('#advance_search_type option[value="'+advance_search_type+'"]').prop("selected",true);
		$("#text").val(advanceSearch);
	});
	function reset_cookies(){
	   /*
		document.cookie = 'status' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		document.cookie = 'case_type' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		document.cookie = 'market' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		document.cookie = 'advance_search_type' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		document.cookie = 'advanceSearch' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		document.cookie = 'sort' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		document.cookie = 'column' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
        */
        
        Cookies.remove('status', { path: '/pride' });
        Cookies.remove('case_type', { path: '/pride' });
        Cookies.remove('market', { path: '/pride' });
        Cookies.remove('advance_search_type', { path: '/pride' });
        Cookies.remove('advanceSearch', { path: '/pride' });
        Cookies.remove('sort', { path: '/pride' });
        Cookies.remove('column', { path: '/pride' });
	}

	function searchByCaseType(case_type){
		/*
		 document.cookie = 'case_type' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		 		document.cookie = 'case_type' + "=" + case_type + ";"
		 */
        
        Cookies.set('case_type', case_type, { expires: 30, path: "/pride" });
		$(".page_loader").show();
		$.ajax({
			type: "POST"
			, url: '<?php echo base_url();?>admin/dashboard/searchByCaseType'
			, data: {
				case_type: case_type,
			}
			, success: function (response) {
				$(".page_loader").hide();
				var json = jQuery.parseJSON(response);
				$(".lead-data").html(json.html);
				//load_dataTable();
				//date_sort_fix();
				//column_sort_fix();
			}
		});
	}

	function searchByMarket(market){
		/*
		 document.cookie = 'market' +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		 		document.cookie = 'market' + "=" + market + ";"
		 */
        Cookies.set('market', market, { expires: 30, path: "/pride" });
		$(".page_loader").show();
		$.ajax({
			type: "POST"
			, url: '<?php echo base_url();?>admin/dashboard/searchByMarket'
			, data: {
				market: market,
			}
			, success: function (response) {
				$(".page_loader").hide();
				var json = jQuery.parseJSON(response);
				$(".lead-data").html(json.html);
				//load_dataTable();
				//date_sort_fix();
				//column_sort_fix();
			}
		});
	}
	
</script>