<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Lawyers</h2>
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Lawyers</h3>
					</div>
					<?php echo $this->session->flashdata('message'); ?>
					<div class="porlets-content">
						<div class="table-responsive">
							<div class="clearfix">
								<div class="btn-group">
									<a href="<?php echo base_url();?>admin/users/add/2">
										<!-- 2 is role that define type. 2 is for lawyers -->
										<button class="btn btn-primary">
										Add New <i class="fa fa-plus"></i>
										</button>
									</a>
								</div>
								<button style="float: right;" type="button" id="exportCsv" data-name="lawyers" class="btn btn-primary">Export CSV</button>
							</div>
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
										<th>First name</th>
										<th>Last name</th>
										<th>Email</th>
										<th>Phone</th>
										<th>City</th>
										<th>State</th>
										<!-- <th>Working Status</th>-->
										<th>Law Firms</th>
										<th>Zip</th>
										<th>Address</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($users as $user){?>
									<tr class="gradeX <?php if($user->active == '0'){?> red_simple <?php }?>">
										<td><?php echo $user->name; ?></td>
										<td><?php echo $user->lname; ?></td>
										<td><?php echo $user->email; ?></td>
										<td><?php echo $user->phone; ?></td>
										<td><?php echo $user->city; ?></td>
										<td><?php echo getStateShortName($user->state); ?></td>
										<!--  <td> getLawyersMode($user->id); </td>-->
										<td><?php echo getLawsNames($user->law_firms); ?></td>
										<td><?php echo $user->zipcode; ?></td>
										<td><?php echo $user->address; ?></td>
										<td style="cursor:pointer"><a href="<?php echo base_url();?>admin/users/view_lawyer/<?php echo $user->id;?>" > View </a> | <a href="<?php echo base_url();?>admin/users/edit/<?php echo $user->id;?>" >Edit </a> | <a data-toggle="modal" data-target="#myModal<?php echo $user->id;?>">Delete </a>
											<?php if ($user->can_login == '0')
											{ ?>
												<a id="unblockBtn_<?php  echo $user->id; ?>" href="javascript:void(0);" onclick="unblockUser('<?php echo $user->id; ?>','2');">| Unblock </a>
											<?php }
											?>																						
											|<a href="<?php echo base_url();?>admin/users/view_as/<?php echo $user->id;?>" > View As</a>
										</td>
										<!-- 2 define role which is specify it is lawyer -->
										<div class="modal fade" id="myModal<?php echo $user->id;?>" role="dialog">
											<div class="modal-dialog modal-sm">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Confirm Delete</h4>
													</div>
													<div class="modal-body">
														<p>Are you sure you want to delete this record ?</p>
													</div>
													<div class="modal-footer">
														<a class="btn btn-primary" <a href="<?php echo base_url();?>admin/users/delete/<?php echo $user->id;?>/2" >Yes </a>
														<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
													</div>
												</div>
											</div>
										</div>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web-->
			</div>
			<!--/col-md-12-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end-->
</div>
<!--/main-content end-->
</div>