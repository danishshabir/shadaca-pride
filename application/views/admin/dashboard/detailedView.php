<?php $session_id=$this->session->userdata('id');?>
<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Dashboard</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
			</div>
			<div class="clearfix"></div>
			<div class="tab-content">
				<div class="tab-pane cont active" id="leads">
					<div class="table-responsive">
						<table  class="display table table-bordered table-striped" id="dynamic-table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Status:</th>
									<th>Case Id</th>
									<th>Lead detail:</th>
									<th>Case description: </th>
									<th>Source detail:</th>
									<th>Date lead received:</th>
									<th>Action:</th>
								</tr>
							</thead>
							<tbody>
								<?php
									if(!empty($leads))
									{
									  foreach($leads as $lead){
									  if($lead->lead_status != 1){ 
										$assigned_detail = getLeadsLawyer($lead->id);
									  }
									  
									  ?>
								<?php 
									$datetime1 = strtotime($lead->created_at);
									$datetime2 = strtotime(date("Y-m-d H:i:s"));	
									
									$interval  = abs($datetime2 - $datetime1);
									$minutes   = round($interval / 60);
										if($lead->lead_status == 1){
											if($minutes >= 30 && $minutes < 60)
											{
												$class = 'yellow_simple';
											}
											elseif($minutes >= 60){
												$class = 'red_simple';
											}
											else{
												$class = 'green_simple';	
											}
											 
											 
										 }
										 elseif($lead->lead_status != 1 && $lead->lead_status != 4){
											 $class = 'black';
											 
										 }
									
										 elseif($lead->lead_status == 4){
											 $class = 'blue_simple';
											 
										 }
										 
										 
										 /*
										 elseif($new_date < strtotime($time_now) && strtotime($time_now) < $new_date2 ){
											 $class = 'yellow';
											 
										 }elseif($new_date2 < strtotime($time_now)){
											 $class = 'red';
										 }
										 
										 */
													
									   ?>
								<tr class="<?php echo $class;?>">
									<td><?php echo $lead->first_name.' '.$lead->last_name; ?></td>
									<?php   $i = 1; ?>
									<td style="width:240px;">
										<?php
											$assigned_detail['assigned_to'];
											
											$assigned_detail_new = explode(',' , $assigned_detail['assigned_to']);
											
											//print_r($assigned_detail_new);
											
											//if($lead->lead_status != 1 && $lead->lead_status != 4){ echo $assigned_detail['assigned_to'];} else{ echo 'Not Assigned';}
											foreach($assigned_detail_new as $lawyer_names){
											 
											 echo  "Lawyer " . $i . " - " . $lawyer_names . " Last Lead";
											if($lead->lead_status == 2 || $lead->lead_status == 3){ 
											  $newDate = getAssignDate($lead->id);
											  $newDate[0]['assigned_date']; 
											  $caseTypeName =  getCaseTypeName($lead->case_type);
											  echo  " & Type: " . date("m-d-Y h:i:s A", strtotime($newDate[0]['assigned_date']))  . " , " . $caseTypeName[0]['type'] ."<br>";
											  } 
											  $i++;
											  
											echo "<br>";
											}
											
											
											  
											  ?>
									</td>
									<td><?php echo $lead->lead_id_number; ?></td>
									<td style="width:270px;">
										<div class="col-md-10 col-sm-12 detail">
											<?php 
												$caseTypeName =  getCaseTypeName($lead->case_type);
												$caseTypeName[0]['type'];
												
												$market_name =  getMarketName($lead->market); 
												$stateName =  getStateName($market_name->state);
												$stateName[0]['state_short_name'];
												
												echo "Market: "  . $market_name->market_name  . "-" . $stateName[0]['state_short_name'] . "<br>";
												echo "Case Type: "  . $caseTypeName[0]['type'] . "<br>";
												echo " Tags: " . $lead->labels . "<br>";	
												echo " Caller: " . $lead->caller_name . "<br>";	
												echo " City: " . $lead->city . "<br>";
												echo " State: ";
												echo getStateShortName($lead->state) . "<br>";
												
												
												$newJurisdiction = getJurisdictionName($lead->jurisdiction);
												$newJurisdiction[0]['name'];
												//$stateName =  getStateName($newJurisdiction[0]['state']);
												//$stateName[0]['name'];
												
												echo " Jurisdiction: " . 
												
												$newJurisdiction[0]['name'] //$newJurisdiction[0]['jurisdiction_name'] . " - " . $stateName[0]['name'] 
												. "<br>";
												echo " Email: " . $lead->email . "<br>";
												echo " Street: " . $lead->street . "<br>";
												echo " Mobile: " . $lead->mobile . "<br>";
												echo " Home Phone: " . $lead->home_phone . "<br>";
												echo " Work Phone: " . $lead->work_phone . "<br>";
												
												?>
										</div>
									</td>
									<td style="width:240px;"> <?php echo "Case Description: "  . $lead->case_description;
										?>  </td>
									<td style="width:240px;"><?php 
										echo " Lead Id No: " . $lead->id . "<br/> ";
										
										 if($lead->lead_status != 1 && $lead->lead_status != 4){
										  echo "<br/>"."Initial Contact Date : "; 
										  echo getInitialContactDate($lead->id) ;} else{ echo 'Empty' . "<br>";}
										//echo " Received: " . "<br>";
										echo " Source: ";
										echo  getSourceName($lead->source) ."<br>";
										?></td>
									<?php 
										//echo  $lead->email;
										//echo  $lawyers->holiday_from;
										
										//if($date_vacation_from >= $date_vacation_from && $date_vacation_to <= $date_vacation_to){ echo ['On vacation'];} else{ echo 'Available';}
										
										//getLawyersMode(11);
										?>
									<td style="cursor:pointer; width:180px;"> <?php
										if($lead->lead_status == 2 || $lead->lead_status == 3){ 
										  $newDate = getAssignDate($lead->id);
										  $newDate[0]['assigned_date']; 
										  echo  date("m-d-Y h:i:s A", strtotime($newDate[0]['assigned_date']));
										} else{?> No Date <?php }?> </td>
									<td style="cursor:pointer"><a href="<?php echo base_url();?>admin/leads/viewLead/<?php echo $lead->id;?>">View</a> | <a href="<?php echo base_url();?>admin/leads/edit/<?php echo $lead->id;?>">Edit</a> | <a data-toggle="modal" data-target="#myModal<?php echo $lead->id;?>">Delete </a></td>
									<div class="modal fade" id="myModal<?php echo $lead->id;?>" role="dialog" >
										<div class="modal-dialog modal-sm">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Confirm Delete</h4>
												</div>
												<div class="modal-body">
													<p>Are you sure you want to delete this record ?</p>
												</div>
												<div class="modal-footer">
													<a class="btn btn-primary" <a href="<?php echo base_url();?>admin/leads/delete/<?php echo $lead->id;?>" >Yes </a>
													<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
												</div>
											</div>
										</div>
									</div>
									<?php } }else{?>
								<tr>
									<td colspan="7">
										<div class="col-md-10 col-sm-12 more">No Records</div>
									</td>
								</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--/row-->
</div><!--/page-content end--> 
</div><!--/main-content end-->
</div>