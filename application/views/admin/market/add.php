<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Add New Case Type</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Case Type</h3>
            </div>
            <?php echo $this->session->flashdata('message')?>
            <div class="porlets-content">
              <form action="<?php echo base_url();?>admin/settings/saveCaseType" method="post" parsley-validate novalidate>
                <div class="form-group">
                  <label>Type</label>
                  <input type="text" name="type" parsley-trigger="change" required placeholder="Enter name" class="form-control">
                </div><!--/form-group-->  

				<!--<div class="form-group">
                  <label>Tags / Labels</label>
                  <input type="text"  name="label" parsley-trigger="change" required placeholder="Enter Tags / Labels" class="form-control">
                </div><!--/form-group-->           
              
                <button class="btn btn-primary" type="submit">Submit</button>
                <a href="<?php echo base_url();?>admin/settings/caseType" class="btn btn-default">Cancel</a>

              </form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>