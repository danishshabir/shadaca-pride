<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Market Fee </h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Market Fee</h3>
            </div>
			
         <div class="porlets-content">
            <div class="table-responsive">
            <div class="clearfix">
            				
                              <div class="btn-group">
                                  <a href="<?php echo base_url();?>admin/settings/addMarketFee"> <!-- 2 is role that define type. 2 is for lawyers -->
								   <button class="btn btn-primary">
                                      Add Market Fee <i class="fa fa-plus"></i>
                                  </button></a>
                              </div>
                          </div>
                          
                          <?php echo $this->session->flashdata('message')?>
                          <div class="margin-top-10"></div>
                <table  class="display table table-bordered table-striped" id="dynamic-table">
                  <thead>
                    <tr>
                      <th>Market</th>
                      <th>Case type</th>
                      <th>Fee</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php foreach($market_fee as $market_fee_records){
					   $stateName =  getStateName($market_fee_records->state);
					   $stateName[0]['state_short_name'];
				   ?>
				   
                    <tr class="gradeX">
                      <td><?php //echo $jurisdiction_fee->Jurisdiction; 
					  $market_name = getMarketName($market_fee_records->market); 
					  echo $market_name->market_name . " - " . $stateName[0]['state_short_name']; 
					  ?></td>
                      <td><?php
						//echo $jurisdiction_fee->case_type;		
						$caseType_name = getCaseTypeName($market_fee_records->case_type); 
					    echo $caseType_name[0]['type']; 
					  ?>
					  
					  </td>
                       <td>
					   <?php 
							$lead_bill_var = number_format($market_fee_records->fee, 2);
							echo "$" . $lead_bill_var; 
					   ?>
					   </td>
                      
					  <td style="cursor:pointer"><a href="<?php echo base_url();?>admin/settings/editMarketFee/<?php echo $market_fee_records->id;?>" >Edit </a> |  <a data-toggle="modal" data-target="#myModal<?php echo $market_fee_records->id;?>">Delete </a></td> <!-- 2 define role which is specify it is lawyer -->
                   
				   
				   
				      <div class="modal fade" id="myModal<?php echo $market_fee_records->id;?>" role="dialog">
						<div class="modal-dialog modal-sm">
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							   <h4 class="modal-title">Confirm Delete</h4>
							</div>
							<div class="modal-body">
							  <p>Are you sure you want to delete this record ?</p>
							</div>
							<div class="modal-footer">
							  <a class="btn btn-primary" <a href="<?php echo base_url();?>admin/settings/delete_market_fee/<?php echo $market_fee_records->id;?>" >Yes </a>
							  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
							</div>
						  </div>
						</div>
					  </div>
				   
				   
				   </tr>
                   <?php } ?> 
                  </tbody>
                 
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
            
            
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
       
 
    </div><!--/page-content end--> 
  </div><!--/main-content end-->
</div>