<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Market </h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
           <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Market</h3>
            </div>
			
         <div class="porlets-content">
            <div class="table-responsive">
            <div class="clearfix">
            				
                              <div class="btn-group">
                                  <a href="<?php echo base_url();?>admin/settings/addMarket_n"> <!-- 2 is role that define type. 2 is for lawyers -->
								   <button class="btn btn-primary">
                                      Add Market &nbsp; <i class="fa fa-plus"></i>
                                  </button></a>
                              </div>
                          </div>
                          
                          <?php echo $this->session->flashdata('message')?>
                          <div class="margin-top-10"></div>
                <table  class="display table table-bordered table-striped" id="dynamic-table">
                  <thead>
                    <tr>
                      <th>Market</th>
                      <th>State</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php foreach($market_n as $market){?>
                    <tr class="gradeX">
                      <td><?php echo $market->market_name; ?></td>
                      <td><?php $stateName = getStateName($market->state); 
					  echo $stateName[0]['state_short_name']; 
					  ?></td>
                      
                      <td style="cursor:pointer"><a href="<?php echo base_url();?>admin/settings/editMarket_n/<?php echo $market->id;?>" >Edit </a> |  <a data-toggle="modal" data-target="#myModal<?php echo $market->id;?>">Delete </a></td> <!-- 2 define role which is specify it is lawyer -->
                   
				   
				   
				      <div class="modal fade" id="myModal<?php echo $market->id;?>" role="dialog">
						<div class="modal-dialog modal-sm">
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							    <h4 class="modal-title">Confirm Delete</h4>
							</div>
							<div class="modal-body">
							  <p>Are you sure you want to delete this record ?</p>
							</div>
							<div class="modal-footer">
							  <a class="btn btn-primary" <a href="<?php echo base_url();?>admin/settings/delete_market_n/<?php echo $market->id;?>" >Yes </a>
							  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
							</div>
						  </div>
						</div>
					  </div>
				   
				   
				   </tr>
                   <?php } ?> 
                  </tbody>
                 
                </table>
              </div><!--/table-responsive-->
            </div><!--/porlets-content-->
            
            
          </div><!--/block-web--> 
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
       
 
    </div><!--/page-content end--> 
  </div><!--/main-content end-->
</div>