<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2>Add New Jurisdiction Fee</h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="block-web">
            <div class="header">
              <div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
              <h3 class="content-header">Jurisdiction Fee</h3>
            </div>
            <?php echo $this->session->flashdata('message')?>
            <div class="porlets-content">
              <form action="<?php echo base_url();?>admin/settings/saveJurisdictionFee" method="post" parsley-validate novalidate>
                
				
			<div class="form-group col-md-4">
				 <div class="form-group" id="lawFirm">
                  <label>Juriscdiction *</label>
                 
                    <select placeholder="jurisdiction" class="form-control" name="jurisdiction" onchange="selectLawFirm(this.value,'admin/Dashboard/getStateId','');" required>
                       	<option value="" required> Select juriscdiction </option>
                     
					  <?php foreach($jurisdiction as $jurisdictions){
							$stateName =  getStateName($jurisdictions->state);
							$stateName[0]['name'];
							//print_r($stateName);
							
						  ?>
                      	<option value="<?php echo $jurisdictions->id;?>"> <?php echo $jurisdictions->jurisdiction_name . " - " . $stateName[0]['name'];?> </option>
					
                      <?php }?>
      			   </select>
                </div>
            </div>
				
			<div class="form-group col-md-4">	
				<div class="form-group">
                  <label>Case type *</label>
                 
                    <select placeholder="caseType" class="form-control" name="case_type" required>
                       	<option value="" required> Select Case type </option>
                     
					  <?php foreach($caseType as $caseTypes){?>
                      	<option value="<?php echo $caseTypes->id;?>"> <?php echo $caseTypes->type;?> </option>
                      <?php }?>
               
      			   </select>
                 
                </div>
            </div>
           
				
		   <div class="form-group col-md-4">
				<div class="form-group">
                  <label>Fee *</label>
                  <input type="number" name="fee" parsley-trigger="change" required placeholder="Enter fee" class="form-control">
				  
                </div><!--/form-group-->
          </div>
				
			<div class="clearfix"></div>
			
				<div class="form-group">
                  <input type="hidden" id="sub_cat" name="state" parsley-trigger="change" required placeholder="Enter State" class="form-control">
                </div><!--/form-group-->
				
              <div class="form-group col-md-12">
                <button class="btn btn-primary" type="submit">Submit</button>
                  <a href="<?php echo base_url();?>admin/settings/jurisdictionFee" class="btn btn-default">Cancel</a>
			  </div>	
			
			<div class="clearfix"></div>
			
			</form>
            </div><!--/porlets-content-->
          </div><!--/block-web--> 
        </div><!--/col-md-6-->
        
         
      </div><!--/row-->
      
     
      
    </div><!--/page-content end--> 
  </div>
  
  <script>





     function selectLawFirm(id, actionUrl, reloadUrl) {

        if (id == '') {
            $("#sub_cat").val('');
            return true;
        }
        $.post( "<?php echo base_url();?>"+actionUrl,{ id: id }, function( data ) {
		  $('#sub_cat').val(data);
		});
    }






</script>