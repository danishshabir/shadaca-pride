<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Landing Pages</h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Landing Pages</h3>
					</div>
					<div class="porlets-content">
						<div class="table-responsive">
							<div class="clearfix">
							</div>
							<?php echo $this->session->flashdata('message')?>
							<div class="margin-top-10"></div>
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
										<th>Title</th>
										<th>Content</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($pages as $row){?>
									<tr class="gradeX">
										<td><?php echo $row->title; ?></td>
										<td><?php echo $row->content; ?></td>
										<td style="cursor:pointer"><a href="<?php echo base_url();?>admin/pages/edit/<?php echo $row->id;?>" >Edit </a></td>
									</tr>
									<?php } ?> 
								</tbody>
							</table>
						</div>
						<!--/table-responsive-->
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>
<!--/main-content end-->
</div>