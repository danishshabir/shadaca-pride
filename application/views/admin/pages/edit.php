<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Edit Page </h2>
			</div>
			<!--/col-md-12--> 
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions"> <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> <a class="close-down" href="#"><i class="fa fa-times"></i></a> </div>
						<h3 class="content-header">Edit Page</h3>
					</div>
					<?php if($message) { ?>
						<p class="message_success"> <?php echo $message; ?></p>
					<?php } ?>
					<div class="porlets-content">
						<form action="<?php echo base_url();?>admin/pages/edit/<?php echo $page->id;?>" method="post" onsubmit="return validatePhone();" parsley-validate novalidate>
							<!--/form-group-->
							<div class="form-group">
								<label>Title</label>
								<input type="text" name="title" parsley-trigger="change" required placeholder="Enter first name" class="form-control" value="<?php echo $page->title; ?>">
							</div>
							<div class="clearfix"></div>
							<div class="form-group">
								<label>Content</label>
								<div class="compose-editorr" style="margin-top:10px;">
									<textarea id="text-editor" class="col-xs-12" name="content" rows="8" parsley-trigger="change" required> <?php echo $page->content; ?> </textarea>
								</div>
							</div>
							<div class="clearfix"></div><br>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Submit</button>
								<a href="<?php echo base_url();?>admin/pages" class="btn btn-default">Cancel</a>
							</div>
						</form>
					</div>
					<!--/porlets-content-->
				</div>
				<!--/block-web--> 
			</div>
			<!--/col-md-6-->
		</div>
		<!--/row-->
	</div>
	<!--/page-content end--> 
</div>