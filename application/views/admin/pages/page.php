<div id="main-content">

	<div class="page-content">

		<div class="row">

			<div class="col-md-12">

				<div class="block-web">

					<div class="porlets-content">
						<h1><?php echo $page->title; ?></h1>

						<?php echo $page->content; ?>

					</div>

					<!--/porlets-content-->

				</div>

				<!--/block-web-->

			</div>
			<div class="col-md-12">
			<?php if($note) { ?>
				<div class="block-web" style=" margin-top: 15px;">
				
					<div class="porlets-content">
						<span style = "    font-size: 24px;
						line-height: 1.1;
						font-weight: 400;
						margin-top: 20px;
						margin-bottom: 10px;
						display: block;">Account Notes</span>
						<?php if(isset($account_notes->shared_notes))echo $account_notes->shared_notes; ?>

					</div>

					<!--/porlets-content-->

				</div>
			<?php } ?>
			<!--/block-web-->

			</div>

			<!--/col-md-12-->

		</div>

		<!--/row-->

	</div>

	<!--/page-content end-->

</div>

<!--/main-content end-->

</div>