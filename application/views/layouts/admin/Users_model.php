<?php

class Users_model extends CI_Model{

    

    function __construct(){

        parent::__construct();

    }



    function fetchAllUsers($role = ''){

        $this->db->select('*');

        $this->db->from('users');

        if($role != ''){

			$this->db->where('role',$role);			
			}
			
		$query = $this->db->get();
        
		if ($query->num_rows() > 0) {
            return $query->result();
        } 
		else {
            return false;

        }

    }
	
		function fetchAllStates() 
    {
        $this->db->select('*');
        $this->db->from('jurisdiction');

         $query=$this->db->get();

        return $query->result();
    }
	
    function fetchEmailTemplateData($id){
				
				
		$this->db->select('*');
        $this->db->from('email_templates');

        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {

            return false;
        }
					
	}


    function fetchLawyerLeads($id){
				
		/*		
		$this->db->select('*');
        $this->db->from('laywer_lead_fee');
        $this->db->where('lawyer_id', $id);
		
	$this->db->select('leads.*,laywer_lead_fee.*');
    $this->db->from('laywer_lead_fee');
	$this->db->where('lawyer_id', $id);
    $this->db->join('leads', 'laywer_lead_fee.lead_id = leads.id', 'right outer'); 
    */
		$this->db->select('leads.*, laywer_lead_fee.*');
        $this->db->from('laywer_lead_fee');
        $this->db->join('leads','leads.id = laywer_lead_fee.lead_id ', 'right');
		$this->db->where('laywer_lead_fee.lawyer_id',$id);
	
	
	
        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
					
	}
	
	
	
    function fetchMonthlyDetail($id){
		
		$this->db->select('users.*, monthly_payment.*');
        $this->db->from('monthly_payment');
        $this->db->join('users','users.id = monthly_payment.lawyer_id ', 'right');
		$this->db->where('monthly_payment.lawyer_id',$id);
	
        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result();
        } else {

            return false;
        }
					
	}
	
	
	
	
	function fetchPayment($id,$lead_id){
				
		/*		
		$this->db->select('*');
        $this->db->from('laywer_lead_fee');
        $this->db->where('lawyer_id', $id);
		
	$this->db->select('leads.*,laywer_lead_fee.*');
    $this->db->from('laywer_lead_fee');
	$this->db->where('lawyer_id', $id);
    $this->db->join('leads', 'laywer_lead_fee.lead_id = leads.id', 'right outer'); 
    
		$this->db->select('leads.*, laywer_lead_fee.*');
        $this->db->from('laywer_lead_fee');
        $this->db->join('leads','leads.id = laywer_lead_fee.lead_id ', 'right');
		$this->db->where('laywer_lead_fee.lawyer_id',$id);
	
	
	*/
		$this->db->select('leads.*, lead_payment.*');
        $this->db->from('lead_payment');
		//$this->db->order_by('lead_id', 'desc');
		$this->db->order_by('lawyer_id', 'asc');
        $this->db->join('leads','leads.id = lead_payment.lead_id ', 'right');
		$this->db->where('lead_payment.lawyer_id',$id);
		$this->db->where('lead_payment.lead_id',$lead_id);
	
        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            return $query->result();
			
        } else {

            return false;
        }
					
	}
	
	
	
	function fetchAllOperatorsUsers($role = ''){

        $this->db->select('*');

        $this->db->from('users');

        if($role != ''){

			$this->db->where('role',$role);

			

			}

		$query = $this->db->get();



        if ($query->num_rows() > 0) {



            return $query->result();



        } else {



            return false;



        }

    }
	
	function fetchAllCaseTpe(){

        $this->db->select('*');

        $this->db->from('case_type');

		$query = $this->db->get();



        if ($query->num_rows() > 0) {



            return $query->result();



        } else {



            return false;



        }

    }
	
	
	
	function fetchAllLawFirms(){

        $this->db->select('*');

        $this->db->from('law_firms');

		$query = $this->db->get();



        if ($query->num_rows() > 0) {



            return $query->result();



        } else {



            return false;



        }

    }
	
	
	
	function fetchAllJurisdiction(){

        $this->db->select('*');

        $this->db->from('jurisdiction');

		$query = $this->db->get();



        if ($query->num_rows() > 0) {



            return $query->result();



        } else {



            return false;



        }

    }

	
	
	
	
	
	function lawType() 
    {
        $this->db->select('*');
        $this->db->from('law_firms');

         $query=$this->db->get();

        return $query->result();
    }
	
	
	
	
	function lawTypee() 
    {
        $this->db->select('*');
        $this->db->from('law_firms');

         $query=$this->db->get();

        return $query->result();
    }
	
	

	 function fetchLawyersRelatedToCase($case_type, $city){

        

		$where = "users.role= 2 AND users.city = '".$city."' AND INSTR(users.case_type, '".$case_type."')"; 

		$this->db->select('users.id,users.name,users.email, users.phone, users.address, users.city, users.jurisdiction, users.state, users.zipcode, users.case_type, max(lawyers_to_leads.created_at) as assign_date');

        $this->db->from('users');

        $this->db->join('lawyers_to_leads','users.id = lawyers_to_leads.lawyer_id ', 'left');

		$this->db->where($where);

		$this->db->order_by("max(lawyers_to_leads.created_at)", "asc"); 

		$this->db->group_by(array("users.id","users.name","users.email", "users.phone", "users.address", "users.city", "users.jurisdiction", "users.state", "users.zipcode", "users.case_type")); 

		

	

		$query = $this->db->get();

		

		//echo $this->db->last_query();

		//exit;

       

        if ($query->num_rows() > 0) {



            return $query->result();



        } else {



            return false;



        }

    }

    

    function fetchRoles(){

        $this->db->select('*');

        $this->db->from('role');

        $query = $this->db->get();



        if ($query->num_rows() > 0) {



            return $query->result_array();



        } else {



            return false;



        }

    }

    

    public function fetchAll($limit = NULL, $start = NULL) {



        $this->db->select('*');

        $this->db->from('users');

		if(isset($limit) and !empty($limit)){

  		$this->db->limit($limit,$start);

		}

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {

            return false;

        }

    }

    
	
	    public function fetchRowLawyer($id) {
        $this->db->select('*');
        $this->db->from('users');

        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {

            return false;
        }

    }
	
	
	
	    public function fetchLeadfee($id,$user_id) {
        $this->db->select('*');
        $this->db->from('laywer_lead_fee');

        $this->db->where('lead_id', $id);
        $this->db->where('lawyer_id', $user_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return true;
        } else {

            return false;
        }

    }
	
	
	
	   public function Leadfee($id) {
        $this->db->select('*');
        $this->db->from('laywer_lead_fee');

        $this->db->where('lead_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {

            return false;
        }

    }

	public function fetchLawfee($id) {
        $this->db->select('*');
        $this->db->from('law_firms');

        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {

            return false;
        }

    }
	
	
		public function fetchStateId($id) {
        $this->db->select('*');
        $this->db->from('jurisdiction');

        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {

            return false;
        }

    }



	public function fetchImage($id) {
        $this->db->select('image');
        $this->db->from('users');

        $this->db->where('id', $id);
        $query = $this->db->get();
        //echo $query = $this->db->last_query();
		//exit;
        if ($query->num_rows() == 1) {

            return $query->result_array();
        } else {

            return false;
        }

    }
	
	public function fetchAssignDate($id) {
        $this->db->select('assigned_date');
        $this->db->from('lawyers_to_leads');

        $this->db->where('lead_id', $id);
        $query = $this->db->get();
        //echo $query = $this->db->last_query();
		//exit;
        if ($query->num_rows() > 0) {

            return $query->result_array();
        } else {

            return false;
        }

    }
	
	
	public function fetchStateName($id) {
        $this->db->select('name');
        $this->db->from('jurisdiction_states');

        $this->db->where('id', $id);
        $query = $this->db->get();
        //echo $query = $this->db->last_query();
		//exit;
        if ($query->num_rows() == 1) {

            return $query->result_array();
        } else {

            return false;
        }

    }
	
	
	public function fetchJurisdictionName($id) {
        $this->db->select('jurisdiction_name');
        $this->db->from('jurisdiction');

        $this->db->where('id', $id);
        $query = $this->db->get();
        //echo $query = $this->db->last_query();
		//exit;
        if ($query->num_rows() == 1) {

            return $query->result_array();
        } else {

            return false;
        }

    }
	
	public function fetchCaseTypeName($id) {
        $this->db->select('type');
        $this->db->from('case_type');

        $this->db->where('id', $id);
        $query = $this->db->get();
        //echo $query = $this->db->last_query();
		//exit;
        if ($query->num_rows() == 1) {

            return $query->result_array();
        } else {

            return false;
        }

    }
	
	
	
	

    function save($data) {

		

        $this->db->set($data);



        $this->db->insert('users');



        $insertId = $this->db->insert_id();



        if ($insertId > 0) {

		return $insertId;



        } else {



            return false;



        }



    }

    

    function update($data, $id)
	{
       $this->db->where('id', $id);

	   $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0)
			{
            return true;
			} 
		
			else {
            return false;
            }
	}
	
	public function updatePassword($id,$data){
		
		
		$this->db->where('id', $id);
	   
	   $this->db->update('users', $data);

        if ($this->db->affected_rows() > 0) {

            return true;

        } else {

            return false;

        }

	}
	

	function update_vacation($data, $id)
	{
       $this->db->where('id', $id);

	   $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0)
			{
            return true;
			} 
		
			else {
            return false;
            }
	}

    

    public function fetchRow($id) {



        $this->db->select('*');



        $this->db->from('users');



        $this->db->where('id', $id);



        $query = $this->db->get();



        if ($query->num_rows() == 1) {



            return $query->row();



        } else {



            return false;



        }



    }
    public function fetchLawyerNames($id) {

        $this->db->select('name');

        $this->db->from('users');

        $this->db->where('id', $id);
       // $this->db->last_query();

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();

        } else {

            return false;

        }

    }


    public function fetchAllTags() {
        $this->db->select('*');

        $this->db->from('tags_labels');
        //$this->db->where('id');

        $query = $this->db->get();
        if ($query->num_rows() == 1) {

		return $query->row();

        } else {

            return false;
        }

    }

    

	   function fetchAssignLawyers($id){

        

		$this->db->select('users.id,users.name,lawyers_to_leads.notes');

        $this->db->from('users');

        $this->db->join('lawyers_to_leads','users.id = lawyers_to_leads.lawyer_id ');

		$this->db->where('lawyers_to_leads.lead_id',$id);

		

		$query = $this->db->get();



        if ($query->num_rows() > 0) {



            return $query->result();



        } else {



            return false;



        }

    }

	function fetchLawyersLeads($id){

		$this->db->select('leads.*, lawyers_to_leads.created_at as assign_date');

        $this->db->from('lawyers_to_leads');

        $this->db->join('leads','leads.id = lawyers_to_leads.lead_id ', 'right');

		$this->db->where('lawyers_to_leads.lawyer_id',$id);

		$this->db->order_by('lawyers_to_leads.created_at desc');

		$this->db->limit(5);

		$query = $this->db->get();

		



        if ($query->num_rows() > 0) {



            return $query->result();



        } else {



            return false;



        }

    }
	
	
	
	
	
	function fetchLawyersVacationMode($id){

		$this->db->select('holiday_from,holiday_to');

        $this->db->from('users');
		
		$this->db->where('id',$id);


		$query = $this->db->get();

        if ($query->num_rows() > 0) {
			
			//$data =array();
			
            //echo $data->holiday_from;
            //echo $data->holiday_to;
			
			return $query->row_array();
			
			
			/*
			$data =array();
			$data = $query->result_array();
			
			echo $data['holiday_from'];
			echo $data['holiday_to'];
			
			*/
			
			
			
			
        } else {
			return false;
        }
    }

	
	
	

	function fetchAllAssignLawyers(){

        

		$this->db->select('users.*, CONCAT(leads.first_name, " ", leads.last_name) as lead_name, leads.email as lead_email, lawyers_to_leads.created_at as assigned_date');

        $this->db->from('users');

        $this->db->join('lawyers_to_leads','users.id = lawyers_to_leads.lawyer_id ');

		$this->db->join('leads','lawyers_to_leads.lead_id = leads.id ');

		//$this->db->where('lawyers_to_leads.created_at >= NOW()');

		

		$query = $this->db->get();



        if ($query->num_rows() > 0) {



            return $query->result();



        } else {



            return false;



        }

    }

	

	 public function fetchSetting($field) {



        $this->db->select($field);



        $this->db->from('settings');



        $this->db->where('id', '1');



        $query = $this->db->get();



        if ($query->num_rows() == 1) {



            return $query->row();



        } else {



            return false;



        }



    }

	

    public function deleteRow($id) 

    {







       $this->db->where('id', $id);

	   

	   $this->db->delete('users');



        if ($this->db->affected_rows() > 0) {



            return TRUE;



        } else {



            return false;



        }



    }


public function get_single_user($id) 

    {

  $this->db->select('*');

         $this->db->from('users');

         $this->db->where('id',$id);

         $query=$this->db->get();

        return $query->row();

    }

    

    public function get_users() 

    {

        $this->db->select('*');

         $this->db->from('users');

         $query=$this->db->get();

        return $query->result_array();

    }
	
	public function checkOldPassword($id,$password) 

    {

  		 $this->db->select('*');

         $this->db->from('users');

         $this->db->where('id',$id);
		 
		 $this->db->where('password',$password);

         $query=$this->db->get();
		 
		 
		 
		 if ($query->num_rows() > 0) {
			return true;
        } else {
            return false;
        }	
        

    }
	
	public function checkDuplicateEmail($post_email) {

    $this->db->where('email', $post_email);

    $query = $this->db->get('users');

    $count_row = $query->num_rows();

    if ($count_row > 0) {
      //if count row return any row; that means you have already this email address in the database. so you must set false in this sense.
        return FALSE; // here I change TRUE to false.
     } else {
      // doesn't return any row means database doesn't have this email
        return TRUE; // And here false to TRUE
     }
}



}
