<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
	<title><?php echo $this->config->item('project_title'); ?></title>
	<!-- Bootstrap -->
	<link href="<?php echo base_url();?>assets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/style-responsive.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/plugins/kalendar/kalendar.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/admin/plugins/gallery/magnific-popup.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/plugins/advanced-datatable/css/demo_table.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/plugins/advanced-datatable/css/demo_page.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/plugins/froala/froala_editor.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/admin/plugins/froala/froala_style.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/plugins/air-datepicker/css/datepicker.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/chosen/chosen.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/src/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery-ui.css" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js'></script>
	<script src="<?php echo base_url();?>assets/admin/src/jquery.tagsinput.js"></script> 
	<script src="<?php echo base_url();?>assets/admin/js/jquery-2.0.2.min.js"></script> 
	<script src="<?php echo base_url();?>assets/admin/plugins/chosen/chosen.jquery.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/plugins/mask-plugin/jquery.mask.js"></script>
	<script src="<?php echo base_url('assets/admin/plugins/data-tables/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/js.cookie.min.js"></script>
</head>
<body class="light-theme">
	<div class="page_loader"></div>
	<?php 
	if($this->session->userdata('logged_in'))
	{
		$role = $this->session->userdata('role');
		$user_id = $this->session->userdata('id');
		?>
		<div class="header navbar navbar-inverse box-shadow navbar-fixed-top">
			<div class="navbar-inner">
				<div class="header-seperation">
					<nav class="navbar navbar-default">
						<?php if($role == 1){?>
						<a href="<?php echo base_url();?>admin/dashboard/manage" class="h-logo">
							<?php }else{?>
							<a href="<?php echo base_url();?>admin/pages/welcome" class="h-logo">
								<?php }?>
								<img src="<?php echo base_url();?>assets/admin/images/logo.png" alt="Pride Legal Leads">
							</a>
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<!--	<div class="sidebar-toggle-box navbar-brand"> <a href="javascript:void(0);"><i class="fa fa-bars"></i></a> </div> -->
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
									<span class="sr-only">Toggle navigation</span>
									<h3>Menu</h3>
								</button>
								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav">
										<?php if($role == 2 || $role == 6 || $role == 7){ ?>
										<li><a href="<?php echo base_url();?>admin/pages/welcome" class="red"><strong>Pride Legal Leads</strong></a> </li>
										<li class="<?php if(in_array($this->uri->segment(2), array("dashboard","archive_lawyer")) && ($this->uri->segment(3) != 'statements')){echo "active";} ?>">
                                            <!--
                                            <a href="<?php echo base_url();?>admin/dashboard/manage" class="red"><strong>Leads</strong></a>
                                        -->
                                        <a  class="dropdown-toggle main_menu" style="cursor: pointer;" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>Leads</strong></a>
                                        <ul class="dropdown-menu">
                                        	<li class="<?php if($this->uri->segment(3)=="manage"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/dashboard/manage">Leads View</a></li>
                                        	<li class="<?php if($this->uri->segment(2)=="archive_lawyer"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/archive_lawyer">Archived</a></li>
                                            <?php if($role == 2 || $role == 6){ ?>
                                                <li class="<?php if($this->uri->segment(3)=="not_retained_declined"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/dashboard/not_retained_declined">Not Retained/Declined</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <?php } else { ?>
                                    <li> <a href="<?php echo base_url();?>admin/dashboard/manage" class="red"><strong>Pride Legal Leads</strong></a> </li>
                                    <?php } ?>
                                    <?php if($role == 1) { ?>
                                    <li class="<?php if(($this->uri->segment(2)=="leads" && $this->uri->segment(3)=="add") || $this->uri->segment(4)=="Lead-View" || $this->uri->segment(3)=="detailedView" ||$this->uri->segment(3)=="view_archieve" || $this->uri->segment(3)=="view" || $this->uri->segment(2)=="archive" ){echo "active";} ?>  ">
                                    	<a href="<?php echo base_url();?>admin/leads" class="dropdown-toggle main_menu" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>Leads</strong></a>
                                    	<ul class="dropdown-menu">
                                    		<li class="<?php if(($this->uri->segment(2)=="leads" && $this->uri->segment(3)=="add")){echo "active";} ?>"><a href="<?php echo base_url();?>admin/leads/add">Operator Lead </a></li>
                                    		<li class="<?php if(($this->uri->segment(2)=="leads" && $this->uri->segment(3)=="add_quick")){echo "active";} ?>"><a href="<?php echo base_url();?>admin/leads/add_quick">Quick Lead Form </a></li>
                                    		<li class="<?php if($this->uri->segment(4)=="Lead-View"){echo "active";} ?>"><a onclick="reset_cookies();" href="<?php echo base_url();?>admin/dashboard/manage/Lead-View">Leads View</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="detailedView"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/dashboard/detailedView">Details View</a></li>
                                    		<li class="<?php if($this->uri->segment(2)=="archive"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/archive">Archived Leads</a></li>
                                    		<li class="<?php if($this->uri->segment(2)=="deleted"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/deleted">Deleted Leads</a></li>
                                    	</ul>
                                    </li>
                                    <li class="<?php if(($this->uri->segment(2)=="users" || $this->uri->segment(2)=="settings") && in_array($this->uri->segment(3), array("admin","add","edit","admin","lawyers","lawFirms","editLawtype","addFirmtype")) ){echo "active";} ?>  ">
                                    	<a href="<?php echo base_url();?>admin/leads" class="dropdown-toggle main_menu" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>Users</strong></a>
                                    	<ul class="dropdown-menu">
                                    		<li class="<?php if($this->uri->segment(3)=="lawFirms" || $this->uri->segment(3)=="editLawtype" || $this->uri->segment(3)=="addFirmtype"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/lawFirms">Law Firms</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="lawyers"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/users/lawyers">Lawyers</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="lawyerStaff"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/users/lawyerStaff">Lawyer Staff</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="admin"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/users/admin">Admin</a></li>
                                    	</ul>
                                    </li>
                                    <li class="<?php if(in_array($this->uri->segment(3), array("caseType","editCasetype","market_n","editMarket_n","addMarket_n","marketFee","editJurisdictionFee","addMarketFee","addTagtype","editTagtype","tags_label","setting","jurisdiction_n","addMarket","editMarket","intaker","addIntaker","editIntaker","source","addSource","editSource")) && $this->uri->segment(3)!="email_templates" ){echo "active";} ?>" >
                                    	<a href="#" class="dropdown-toggle main_menu" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>Setup</strong></a>
                                    	<ul class="dropdown-menu">
                                    		<li class="<?php if($this->uri->segment(3)=="caseType" || $this->uri->segment(3)=="editCasetype"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/caseType">Case Type</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="market_n" || $this->uri->segment(3)=="editMarket_n" || $this->uri->segment(3)=="addMarket_n"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/market_n">Market</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="marketFee" || $this->uri->segment(3)=="editJurisdictionFee" || $this->uri->segment(3)=="addMarketFee"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/marketFee">Market Fee</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="tags_label" || $this->uri->segment(3)=="editTagtype" || $this->uri->segment(3)=="addTagtype"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/tags_label">Tags / Labels</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="jurisdiction_n" || $this->uri->segment(3)=="addMarket" || $this->uri->segment(3)=="editMarket"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/jurisdiction_n">Jurisdiction</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="intaker" || $this->uri->segment(3)=="addIntaker" || $this->uri->segment(3)=="editIntaker"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/intaker">Intaker</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="source" || $this->uri->segment(3)=="addSource" || $this->uri->segment(3)=="editSource"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/source">Source</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="setting"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/setting">Settings</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="login_users"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/login_users">Last Logins</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="backup_database"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/backup_database">Backup Database</a></li>
                                    		<?php
                                    		$status_no = ( $this->session->maintenance_status == 1 ? 0 : 1 );
                                    		$status = ( $this->session->maintenance_status == 1 ? 'On' : 'Off' );
                                    		?>
                                    		<li><a title="Current Status: <?php echo $status; ?>" href="<?php echo base_url();?>admin/settings/maintenance/<?php echo $status_no; ?>">Maintenance Mode&nbsp;(<?php echo $status; ?>)</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="whitelist_ips"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/whitelist_ips">Add/Remove IPs</a></li>
                                    	</ul>
                                    </li>
                                    <?php } if($role == 2 || $role == 6 || $role == 7) { ?>
                                    <li class="<?php if(in_array($this->uri->segment(3), array("resetPassword","userProfile","UpdateUserProfile","Vacation_mode","manage_payment_profile","text_notification_setting"))){echo "active";} ?>">
                                    	<a href="#" class="dropdown-toggle main_menu" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>
                                    		My Account
                                    	</strong></a>
                                    	<ul class="dropdown-menu">
                                    		<li class="<?php if($this->uri->segment(3)=="userProfile"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/users/userProfile/<?php echo $user_id ?>"><?php if($role == 6){echo 'Law Firm';}elseif($role == 2){echo 'Lawyer';}elseif($role == 7){echo 'Lawyer Staff';} ?> Profile</a></li>
                                    		<?php if($role == 2 || $role == 7) { ?>
                                    		<!--<li class="<?php if($this->uri->segment(3)=="UpdateUserProfile"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/users/UpdateUserProfile/<?php echo $user_id; ?>">Profile Image</a></li>-->
                                    		<?php if($this->session->view_as == ''){ ?>
                                    		<li class="<?php if($this->uri->segment(3)=="Vacation_mode"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/dashboard/Vacation_mode" >Vacation Mode</a></li>
                                    		<li class="<?php if($this->uri->segment(3)=="text_notification_setting"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/users/text_notification_setting" >Text Notification</a></li>
                                    		<?php } ?>
                                    		<?php } else if($role == 6) { ?>
                                    		<?php if($this->session->view_as == ''){ ?>
                                    		<!--<li class="<?php if($this->uri->segment(3)=="company_logo"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/users/company_logo/<?php echo $user_id ?>" >Company Logo</a></li>-->
                                    		<li class="<?php if($this->uri->segment(3)=="manage_payment_profile"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/users/manage_payment_profile/<?php echo $user_id ?>" >Manage Payment Profile</a></li>
                                    		<?php } ?>
                                    		<?php } ?>
                                    		<?php if($this->session->view_as == ''){ ?>
                                    		<li class="<?php if($this->uri->segment(3)=="resetPassword"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/users/resetPassword">Change Password</a></li>
                                    		<?php } ?>
                                    		<?php $cms_pages = cms_pages(); 
                                    		if($cms_pages) { 
                                    			foreach($cms_pages as $cms) {?>
                                    			<li class="<?php if($this->uri->segment(4)== $cms->id){echo "active";} ?>"><a href="<?php echo base_url();?>admin/pages/cms/<?php echo $cms->id; ?>"><?php echo ucfirst($cms->title); ?></a></li>
                                    			<?php } 
                                    		} ?>
                                    	</ul>
                                    </li>
                                    <?php if($role == 7){ ?>
                                    <li class="<?php if($this->uri->segment(3)=="statements"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/dashboard/statements" style="font-weight: bold;" >Statements</a></li>
                                    <?php } ?>
                                    <?php } ?>
                                    <?php if($role == 1){ ?>
                                    <?php if($this->session->view_as == ''){ ?>
                                    <!-- Maintenance Mode Menu Start -->
											<!--<li class="<?php if($this->uri->segment(3)=="whitelist_ips"){echo "active";} ?>">
												<?php if($role == 1){?>
												<a  class="dropdown-toggle main_menu" style="cursor: pointer;" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>Maintenance</strong></a>
												<ul class="dropdown-menu">
													<?php
														$status_no = ( $this->session->maintenance_status == 1 ? 0 : 1 );
														$status = ( $this->session->maintenance_status == 1 ? 'On' : 'Off' );
													?>
													<li><a title="Current Status: <?php echo $status; ?>" href="<?php echo base_url();?>admin/settings/maintenance/<?php echo $status_no; ?>">Maintenance Mode&nbsp;(<?php echo $status; ?>)</a></li>
													<li class="<?php if($this->uri->segment(3)=="whitelist_ips"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/whitelist_ips">Add/Remove IPs</a></li>
												</ul>
												<?php }?>
											</li>-->
											<!-- Maintenance Mode Menu End -->
                                            <!-- New Billing Menu -->
											<li class="<?php echo in_array($this->uri->segment(3), array('LawyerBills', 'History', 'LawyerBilling', 'LawyerBillings', 'LawyerAllLeads', 'LawyerMonthlyBilling', 'makePayment', 'addBatchProcessing', 'billings', 'statements')) ? 'active' : ''; ?>">
                                            <a href="#" class="dropdown-toggle main_menu" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>Billing</strong></a>
                                            <ul class="dropdown-menu">
                                                <li class="<?php echo $this->uri->segment(3) == 'billings' ? 'active' : ''; ?>"><a href="<?php echo base_url();?>admin/dashboard/billings"><strong>Billing</strong></a></li>
                                                <li class="<?php echo $this->uri->segment(3) == 'statements' ? 'active' : ''; ?>"><a href="<?php echo base_url();?>admin/dashboard/statements"><strong>Statements</strong></a></li>
                                            </ul>
                                            
                                            </li>
                                            <!--/ New Billing Menu -->
                                            
											<li class="<?php if(($this->uri->segment(2)=="reporting") && $this->uri->segment(3)=="law_firms" || $this->uri->segment(3)=="reportings" || $this->uri->segment(3)=="lawyers_report"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/reporting/law_firms" class="main_menu 2"><strong>Reporting</strong></a> </li>
											<?php } ?>
											<?php }else{ ?>
                                                <?php if($this->session->view_as){ ?>
                                                <!-- New Billing Menu -->
    											<li class="<?php echo in_array($this->uri->segment(3), array('LawyerBills', 'History', 'LawyerBilling', 'LawyerBillings', 'LawyerAllLeads', 'LawyerMonthlyBilling', 'makePayment', 'addBatchProcessing', 'billings', 'statements')) ? 'active' : ''; ?>">
                                                <a href="#" class="dropdown-toggle main_menu" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>Billing</strong></a>
                                                <ul class="dropdown-menu">
                                                    <li class="<?php echo $this->uri->segment(3) == 'billings' ? 'active' : ''; ?>"><a href="<?php echo base_url();?>admin/dashboard/billings"><strong>Billing</strong></a></li>
                                                    <li class="<?php echo $this->uri->segment(3) == 'statements' ? 'active' : ''; ?>"><a href="<?php echo base_url();?>admin/dashboard/statements"><strong>Statements</strong></a></li>
                                                </ul>
                                                
                                                </li>
                                                <!--/ New Billing Menu -->
                                                <li class="<?php if(($this->uri->segment(2)=="reporting") || $this->uri->segment(3)=="lawyers_report"){echo "active";} ?>"><a href="#" class="main_menu 4"><strong>Reporting</strong></a> </li>
                                                <?php } ?>
                                            <?php } ?>
											<?php if($role == 6){?>
											<?php if($this->session->view_as == ''){ ?>
											<!--<li class="<?php echo in_array($this->uri->segment(3), array('LawyerBills', 'History', 'LawyerBilling', 'LawyerBillings', 'LawyerAllLeads', 'LawyerMonthlyBilling', 'makePayment', 'addBatchProcessing', 'billings')) ? 'active' : ''; ?>"><a href="<?php echo base_url();?>admin/dashboard/LawyerBilling/<?php echo $user_id; ?>" class="main_menu 3"><strong>Billing</strong></a> </li>-->
                                            <!-- New Billing Menu -->
                                            <li class="<?php echo in_array($this->uri->segment(3), array('LawyerBills', 'History', 'LawyerBilling', 'LawyerBillings', 'LawyerAllLeads', 'LawyerMonthlyBilling', 'makePayment', 'addBatchProcessing', 'billings', 'statements')) ? 'active' : ''; ?>">
                                            <a href="#" class="dropdown-toggle main_menu" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>Billing</strong></a>
                                            <ul class="dropdown-menu">
                                                <li class="<?php echo $this->uri->segment(3) == 'LawyerBilling' ? 'active' : ''; ?>"><a href="<?php echo base_url();?>admin/dashboard/LawyerBilling/<?php echo $user_id; ?>"><strong>Billing</strong></a></li>
                                                <li class="<?php echo $this->uri->segment(3) == 'statements' ? 'active' : ''; ?>"><a href="<?php echo base_url();?>admin/dashboard/statements"><strong>Statements</strong></a></li>
                                            </ul>
                                            
                                            </li>
                                            <!--/ New Billing Menu -->
											<!--<li class="<?php /*if(($this->uri->segment(2)=="reporting") || $this->uri->segment(3)=="lawyers_report"){echo "active";} */?>"><a href="<?php /*echo base_url();*/?>admin/reporting/lawyers_report/<?php /*echo $user_id; */?>" class="main_menu 4"><strong>Reporting</strong></a> </li>-->
											<li class="<?php if(($this->uri->segment(2)=="reporting") || $this->uri->segment(3)=="lawyers_report"){echo "active";} ?>"><a href="#" class="main_menu 4"><strong>Reporting</strong></a> </li>
											<?php }?>
											<?php }?>
											<?php if($role == 1) { ?>
											<li class="<?php if($this->uri->segment(3)=="email_templates" || $this->uri->segment(2)=="EmailTracking" || $this->uri->segment(3)=="tracking_record"){echo "active";} ?>">
												<?php if($role == 1){?>
												<a href="<?php echo base_url();?>admin/leads" class="dropdown-toggle main_menu" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>Emails</strong></a>
												<ul class="dropdown-menu">
													<li class="<?php if($this->uri->segment(3)=="email_templates"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/settings/email_templates">Forms/Templates</a></li>
													<li class="<?php if($this->uri->segment(2)=="EmailTracking" && $this->uri->segment(3)==""){echo "active";} ?>"><a href="<?php echo base_url();?>admin/EmailTracking">Email to Subscribers</a></li>
													<li class="<?php if($this->uri->segment(3)=="tracking_record"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/EmailTracking/tracking_record">Email Tracking Stats</a></li>
												</ul>
												<?php }?>
											</li>
											<?php } ?>
											<?php if($role == 2) { ?>
											<?php if($this->session->view_as == ''){ ?>
											
                                            <!--<li class="<?php if($this->uri->segment(3)=="LawyerBills" || $this->uri->segment(3)=="History" || $this->uri->segment(3)=="LawyerBilling" || $this->uri->segment(3)=="LawyerBillings" || $this->uri->segment(3)=="LawyerAllLeads" || $this->uri->segment(3)=="LawyerMonthlyBilling" || $this->uri->segment(3)=="lawyerAllBills" || $this->uri->segment(3)=="lawyerAllBills"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/dashboard/lawyerAllBills/<?php echo $user_id ?>"><strong>Billing</strong></a></li>-->
                                            
                                            <!-- New Billing Menu -->
                                            <li class="<?php echo in_array($this->uri->segment(3), array('LawyerBills', 'History', 'LawyerBilling', 'LawyerBillings', 'LawyerAllLeads', 'LawyerMonthlyBilling', 'makePayment', 'addBatchProcessing', 'billings', 'statements', 'lawyerAllBills')) ? 'active' : ''; ?>">
                                            <a href="#" class="dropdown-toggle main_menu" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false"><strong>Billing</strong></a>
                                            <ul class="dropdown-menu">
                                                <li class="<?php echo $this->uri->segment(3) == 'lawyerAllBills' ? 'active' : ''; ?>"><a href="<?php echo base_url();?>admin/dashboard/lawyerAllBills/<?php echo $user_id; ?>"><strong>Billing</strong></a></li>
                                                <li class="<?php echo $this->uri->segment(3) == 'statements' ? 'active' : ''; ?>"><a href="<?php echo base_url();?>admin/dashboard/statements"><strong>Statements</strong></a></li>
                                            </ul>
                                            
                                            </li>
                                            <!--/ New Billing Menu -->
                                            
												<!--<li class="<?php /*if($this->uri->segment(3)=="reportings" || $this->uri->segment(3)=="reportings" || $this->uri->segment(3)=="allLawyers" || $this->uri->segment(3)=="lawyerOweAmount"){echo "active";} */?>"><a href="<?php /*echo base_url();*/?>admin/reporting/lawyerOweAmount/<?php /*echo $user_id */?>" class="main_menu 5"><strong>Reporting</strong></a> </li>-->
											<li class="<?php if($this->uri->segment(3)=="reportings" || $this->uri->segment(3)=="reportings" || $this->uri->segment(3)=="allLawyers" || $this->uri->segment(3)=="lawyerOweAmount"){echo "active";} ?>"><a href="#" class="main_menu"><strong>Reporting</strong></a> </li>
											<?php } ?>
											<?php } ?>
											<?php if($this->session->view_as == ''){ ?>
											<li class="<?php if($this->uri->segment(2)=="mail"){echo "active";} ?>"><a href="<?php echo base_url();?>admin/mail" class="main_menu 6"><strong>Messages</strong></a></li>
											<?php } ?>
											<?php if($role == 1) { ?>
											<li class="<?php if($this->uri->segment(2)=="pages" && in_array($this->uri->segment(3), array("","edit"))){echo "active";} ?>"><a href="<?php echo base_url();?>admin/pages" class="main_menu 7"><strong>Landing Pages</strong></a></li>
											<?php } ?>
											<?php if($this->session->view_as != '' && $this->session->view_as == 'admin'){ ?>
											<li> <a href="<?php echo base_url();?>admin/Users/return_to_admin" class="red"><strong>Back To Your Login</strong></a> </li>
											<?php }else{ ?>
											<li> <a onclick="reset_cookies();" href="<?php echo base_url();?>admin/login/logout" class="red"><strong>Logout</strong></a> </li>
											<?php } ?>
										</ul>
										<ul class="nav navbar-nav navbar-right">
											<li class="">
												<div class="hov">
													<?php if($role == 6 || $role == 2 || $role == 7){ ?> 

													<div class="btn-group">
														<span> &nbsp; <?php echo "Welcome"." ".$this->session->userdata('full_name'); ?></span>

													</div>
													<?php }?>
													<div class="btn-group">
														<a data-toggle="dropdown" href="" class="con"><span class="fa fa-envelope"></span>
															<?php if(unreadCount()>0){?>
															<span class="label label-success"><?php echo unreadCount();?></span>
															<?php }?>
														</a>
														<ul role="menu" class="dropdown-menu pull-right dropdown-messages" style="width: 370px;">
															<li class="title"><span class="fa fa-envelope"></span>&nbsp;&nbsp;You have <?php echo unreadCount();?> new messages to read...</li>
															<?php $inbox_arr = unreadMails();
															foreach($inbox_arr as $inbox) {
																if($inbox->status == 0) { ?>
																<form action="<?php echo base_url();?>admin/mail/read" id="readmailfrm_<?php echo $inbox->id;?>" method="post">
																	<input type="hidden" name="message_id" value="<?php echo $inbox->id;?>">
																	<input type="hidden" name="type" value="inbox">
																	<input type="hidden" name="status" value="<?php echo $inbox->status;?>">
																</form>
																<li class="message">
																	<div class="message-content inbox-detail" id="<?php echo $inbox->id;?>"><a href="#"><?php echo $inbox->user_name;?></a> <br>
																		<?php echo shortPara($inbox->message, '20');?>
																	</div>
																	<div class="message-time"><?php echo getTime($inbox->created_at);?></div>
																</li>
																<?php }
															} ?>
														</ul>
													</div>
													<?php 
													if($role == 1)
													{
														$leads_arr = getUnreachedLeads();
													}
													else
													{
														$leads_arr = getUnreachedLeadsLawyer($user_id);
													} ?>
													<div class="btn-group">
														<a data-toggle="dropdown" href="" class="con"><span class="fa fa-users"></span>
															<?php if(!empty($leads_arr)){?>
															<span class="label label-success"><?php echo sizeof($leads_arr);?></span>
															<?php }?>
														</a>
														<ul role="menu" class="dropdown-menu pull-right dropdown-messages" style="width: 370px;">
															<li class="title"><span class="fa fa-envelope"></span>&nbsp;&nbsp;You have
																<?php if(!empty($leads_arr)) { echo sizeof($leads_arr);} else { echo '0';}?>
																new leads...
															</li>
															<?php if(!empty($leads_arr)) {
																foreach($leads_arr as $leads) {
																	if($role == 1) {
																		$created_date = $leads->created_at;
																	}
																	else
																	{
																		$created_date = $leads->assigned_at;
																	} ?>
																	<li class="message">
																		<div class="message-content"><a href="<?php echo base_url();?>admin/leads/view/<?php echo $leads->id.($leads->unique_code != '' ? '/'.$leads->unique_code : '');?>"><?php echo $leads->first_name.' '.$leads->last_name;?></a> <br>
																		</div>
																		<div class="message-time"><?php echo getTime($created_date);?></div>
																	</li>
															<?php //}
														}
													}?>
												</ul>
											</div> 
										</div>
									</li>			
								</ul>
							</div>
							<!-- /.navbar-collapse --> 
						</div>
						<!-- /.container-fluid --> 
					</nav>
					<input type="hidden" id="base_url" value="<?php echo base_url();?>">
				</div>
				<!--/header-seperation--> 
			</div>
			<!--/navbar-inner--> 
		</div>
		<!--/header--> 
		<?php } ?>