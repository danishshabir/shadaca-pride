<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
        parent::__construct();
		chechUserSession();
		$this->load->model('admin/Leads_model');
		$this->load->model('admin/Users_model');
       
    }
	
	//Main index function for the controller admin login
	//loding the admin login view
	public function manage($search = '', $val = ''){
		
       $data = array();
	   $status = '';
	   if($this->session->userdata('role') == 1){
		   if($search == 'Pending')
		   {
			   $status = 1;
		   }elseif($search == 'Assigned'){
			   $status = 2;
		   }elseif($search == 'Completed'){
			   $status = 3;
		   }elseif($search == 'Drafted'){
			   $status = 4;
		   }
		   elseif($search == 'All'){
			   $status = '';
		   }elseif($search == 'case_type')
		   {
			   $status = $search;
			   $val = str_replace("-", " ", $val);
		   }elseif($search == 'lawyer')
		   {
			   $status = $search;
		   }
		    
			$data['case_type'] = $this->Leads_model->fetchCaseType();
		    $data['leads'] = $this->Leads_model->fetchLeadsLimit('5', $status, $val);
			$data['lawyers'] = $this->Users_model->fetchAllUsers('2');
			$data['class_type'] = $val;
			$view = 'admin/dashboard/manage_lead';
	   }
	   
	    if($this->session->userdata('role') == 5){
		   if($search == 'Pending')
		   {
			   $status = 1;
		   }elseif($search == 'Assigned'){
			   $status = 2;
		   }elseif($search == 'Completed'){
			   $status = 3;
		   }elseif($search == 'Drafted'){
			   $status = 4;
		   }elseif($search == 'All'){
			   $status = '';
		   }elseif($search == 'case_type')
		   {
			   $status = $search;
			   $val = str_replace("-", " ", $val);
		   }elseif($search == 'operator')
		   {
			   $status = $search;
		   }
		    $data['case_type'] = $this->Leads_model->fetchCaseType();
		    $data['leads'] = $this->Leads_model->fetchLeadsLimit('5', $status, $val);
			$data['lawyers'] = $this->Users_model->fetchAllUsers('2');
			$data['class_type'] = $val;
			$view = 'admin/dashboard/manage_lead';
		}
	   
	   
	   elseif($this->session->userdata('role') == 2){
		   if($search == 'Pending')
		   {
			   $status = 2;
		   }elseif($search == 'Completed'){
			   $status = 3;
		   }elseif($search == 'All'){
			   $status = '';
		   }
		   
		   
		   $data['case_type'] = $this->Leads_model->fetchCaseType();
		   $data['class_type'] = $search;
			$view = 'admin/dashboard/manage';
			$data['leads'] = $this->Leads_model->fetchLeadsOfLawyersLimit($this->session->userdata('id'), '5', $status);
		}
		
	   $this->session->set_flashdata('class_type', $data['class_type']);
	   $data['class'] = 'active';
	   $this->load->view('layouts/admin/header',$data);      // Location: ./application/views/layouts/admin/header.php
	   $this->load->view('layouts/admin/sidebar',$data);      // Location: ./application/views/layouts/admin/sidebar.php
	   $this->load->view($view,$data);  // Location: ./application/views/admin/login/login.php 	
	   $this->load->view('layouts/admin/footer',$data);	 // Location: ./application/views/layouts/admin/footer.php
	}
	
	
	
	
	
	public function detailedView($search = '', $val = ''){
		
		//$data['id'] = $id;
		
       $data = array();
	   $status = '';
	   if($this->session->userdata('role') == 1){
		   if($search == 'Pending')
		   {
			   $status = 1;
		   }elseif($search == 'Assigned'){
			   $status = 2;
		   }elseif($search == 'Completed'){
			   $status = 3;
		   }elseif($search == 'Drafted'){
			   $status = 4;
		   }
		   elseif($search == 'All'){
			   $status = '';
		   }elseif($search == 'case_type')
		   {
			   $status = $search;
			   $val = str_replace("-", " ", $val);
		   }elseif($search == 'lawyer')
		   {
			   $status = $search;
		   }
		   
		    
			$data['case_type'] = $this->Leads_model->fetchCaseType();
		    $data['leads'] = $this->Leads_model->fetchLeadsLimit('5', $status, $val);
			$data['lawyers'] = $this->Users_model->fetchAllUsers('2');
			$data['class_type'] = $val;
			$view = 'admin/dashboard/detailedView';
	   }
	   
	    if($this->session->userdata('role') == 5){
		   if($search == 'Pending')
		   {
			   $status = 1;
		   }elseif($search == 'Assigned'){
			   $status = 2;
		   }elseif($search == 'Completed'){
			   $status = 3;
		   }elseif($search == 'Drafted'){
			   $status = 4;
		   }elseif($search == 'All'){
			   $status = '';
		   }elseif($search == 'case_type')
		   {
			   $status = $search;
			   $val = str_replace("-", " ", $val);
		   }elseif($search == 'operator')
		   {
			   $status = $search;
		   }
		    $data['case_type'] = $this->Leads_model->fetchCaseType();
		    $data['leads'] = $this->Leads_model->fetchLeadsLimit('5', $status, $val);
			$data['lawyers'] = $this->Users_model->fetchAllUsers('2');
			$data['class_type'] = $val;
			$view = 'admin/dashboard/detailedView';
		}
	   
	   $this->session->set_flashdata('class_type', $data['class_type']);
	   $data['class'] = 'active';
	   $this->load->view('layouts/admin/header',$data);      // Location: ./application/views/layouts/admin/header.php
	   $this->load->view('layouts/admin/sidebar',$data);      // Location: ./application/views/layouts/admin/sidebar.php
	   $this->load->view($view,$data);        // Location: ./application/views/admin/login/login.php 	
	   $this->load->view('layouts/admin/footer',$data);	 // Location: ./application/views/layouts/admin/footer.php
	}
	
	
	
	
	
	public function Vacation_mode(){
		$search = '';
		$data = array();
	    $status = '';
	  

		    if($this->session->userdata('role') == 5){
		   if($search == 'Pending')
		   {
			   $status = 1;
		   }elseif($search == 'Assigned'){
			   $status = 2;
		   }elseif($search == 'Completed'){
			   $status = 3;
		   }elseif($search == 'Drafted'){
			   $status = 4;
		   }elseif($search == 'All'){
			   $status = '';
		   }elseif($search == 'case_type')
		   {
			   $status = $search;
			   $val = str_replace("-", " ", $val);
		   }elseif($search == 'operator')
		   {
			   $status = $search;
		   }
		    //$data['case_type'] = $this->Leads_model->fetchCaseType();
		    //$data['leads'] = $this->Leads_model->fetchLeadsLimit('5', $status, $val);
			//$data['lawyers'] = $this->Users_model->fetchAllUsers('2');
			//$data['class_type'] = $val;
			//$view = 'admin/dashboard/manage_lead';
		}
	   
	
	
	
	
	elseif($this->session->userdata('role') == 2){
		   if($search == 'Pending')
		   {
			   $status = 2;
		   }elseif($search == 'Completed'){
			   $status = 3;
		   }elseif($search == 'All'){
			   $status = '';
		   }
		   
		   
		   $data['case_type'] = $this->Leads_model->fetchCaseType();
		   $data['class_type'] = $search;
			$view = 'admin/dashboard/manage';
			$data['leads'] = $this->Leads_model->fetchLeadsOfLawyersLimit($this->session->userdata('id'), '5', $status);
		}
		
	   $this->session->set_flashdata('class_type', $data['class_type']);
	   $data['class'] = 'active';
	   $this->load->view('layouts/admin/header',$data);      // Location: ./application/views/layouts/admin/header.php
	   $this->load->view('layouts/admin/sidebar',$data);      // Location: ./application/views/layouts/admin/sidebar.php
	  // $this->load->view($view,$data);        // Location: ./application/views/admin/login/login.php 	
	   $this->load->view('admin/users/add_holiday');        	
	   $this->load->view('layouts/admin/footer',$data);	 // Location: ./application/views/layouts/admin/footer.php

	
	
	}
	public function check_user_password(){
		$id = $this->uri->segment(4);
		$this->load->model('admin/Activity_model'); 
		
	   
	}		
	
	
	
	public function getLawFirm()
 {
	 
  $cat_id =  $this->input->post('id');
  $this->load->model('admin/Users_model');

   $products = $this->Users_model->fetchLawfee($cat_id);
  
   echo $fee = $products->law_firm_fee;
   exit;

 }
 
 
 	public function getStateId()
 {
	 
  $cat_id =  $this->input->post('id');
  $this->load->model('admin/Users_model');

   $products = $this->Users_model->fetchStateId($cat_id);
  
   echo $fee = $products->state;
   exit;

 }
 
 public function Billing()
 {
	 $data = array();
	 $lead_id = $this->uri->segment(4);
	 $data['id'] = $this->session->userdata('id');
	 
	   $data['payments'] = $this->Users_model->fetchPayment($data['id'],$lead_id);
		/*
		$new_id = array();
		//echo $data['payments']->id;
		//print_r($data['payments']);
		//exit;
		foreach($data['payments'] as $payment){
			$new_id[] = $payment->lead_id;
			
		}
		print_r($new_id);
		exit;
		*/
	   //$this->session->set_flashdata('class_type', $data['class_type']);
	   $data['class'] = 'active';
	   $this->load->view('layouts/admin/header',$data);      // Location: ./application/views/layouts/admin/header.php
	   $this->load->view('layouts/admin/sidebar',$data);      // Location: ./application/views/layouts/admin/sidebar.php
	  // $this->load->view($view,$data);        // Location: ./application/views/admin/login/login.php 	
	   $this->load->view('admin/users/billing');        	
	   $this->load->view('layouts/admin/footer',$data);	 // Location: ./application/views/layouts/admin/footer.php


 }
 
 public function MonthlyBilling()
 {
	 
	 $data = array();
	 $data['id'] = $this->session->userdata('id');
	 
	   $data['Monthly_billings'] = $this->Users_model->fetchMonthlyDetail($data['id']);
   
	   //$this->session->set_flashdata('class_type', $data['class_type']);
	   $data['class'] = 'active';
	   $this->load->view('layouts/admin/header',$data);      // Location: ./application/views/layouts/admin/header.php
	   $this->load->view('layouts/admin/sidebar',$data);      // Location: ./application/views/layouts/admin/sidebar.php
	  // $this->load->view($view,$data);        // Location: ./application/views/admin/login/login.php 	
	   $this->load->view('admin/users/monthly_billing');        	
	   $this->load->view('layouts/admin/footer',$data);	 // Location: ./application/views/layouts/admin/footer.php


 }

 
 public function viewBillInvoice()
 {
	 
	 $data = array();
	 $data['id'] = $this->session->userdata('id');
	 
	   $data['Monthly_billings'] = $this->Users_model->fetchMonthlyDetail($data['id']);
   
	   //$this->session->set_flashdata('class_type', $data['class_type']);
	   $data['class'] = 'active';
	   $this->load->view('layouts/admin/header',$data);      // Location: ./application/views/layouts/admin/header.php
	   $this->load->view('layouts/admin/sidebar',$data);      // Location: ./application/views/layouts/admin/sidebar.php
	  // $this->load->view($view,$data);        // Location: ./application/views/admin/login/login.php 	
	   $this->load->view('admin/users/monthly_billing_invoice');        	
	   $this->load->view('layouts/admin/footer',$data);	 // Location: ./application/views/layouts/admin/footer.php
 }
	
	
}






/* End of file Index.php */
/* Location: ./application/controllers/admin/Index.php */