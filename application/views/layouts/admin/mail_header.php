<div id="main-content">
    <div class="page-content">
      <div class="row">
        <div class="col-md-12">
          <h2><?php echo($this->session->userdata('role') == 2 || $this->session->userdata('role') == 6 ? 'Support Messages' : 'Messages') ?></h2>
        </div><!--/col-md-12--> 
      </div><!--/row-->
      <?php $url_value = $this->uri->segment(3);?>
      <div class="row">
        <div class="col-sm-3 col-lg-2"> <a class="btn btn-danger btn-block btn-compose-email" href="<?php echo base_url();?>admin/mail/compose_mail">Compose Message</a>
          <ul class="nav nav-pills nav-stacked nav-email">
            <li <?php if($url_value == ''){?> class="active" <?php }?>> <a href="<?php echo base_url();?>admin/mail"><?php if(unreadCount()>0){?> <span class="badge pull-right"><?php echo unreadCount();?></span> <?php }?> <i class="glyphicon glyphicon-inbox"></i> Messages </a> </li>
            <!--<li><a href="#"><i class="glyphicon glyphicon-star"></i> Starred</a></li>-->
            <li <?php if($url_value == 'outbox'){?> class="active" <?php }?>><a href="<?php echo base_url();?>admin/mail/outbox"><i class="glyphicon glyphicon-send"></i> Outbox</a></li>
            <!--<li> <a href="#"> <span class="badge pull-right">3</span> <i class="glyphicon glyphicon-pencil"></i> Draft </a> </li>-->
            <!--<li <?php if($url_value == 'trash'){?> class="active" <?php }?>><a href="<?php echo base_url();?>admin/mail/trash"><i class="glyphicon glyphicon-trash"></i> Trash</a></li>-->
            <!--<li <?php if($url_value == 'archived'){?> class="active" <?php }?>><a href="<?php echo base_url();?>admin/mail/archived"><i class="glyphicon glyphicon-hdd"></i> Archived</a></li>-->
          </ul>
          <!--<div class="mb30"></div>
          <h5 class="subtitle">Folders</h5>
          <ul class="nav nav-pills nav-stacked nav-email mb20">
            <li> <a href="#"> <span class="badge pull-right">2</span> <i class="glyphicon glyphicon-folder-open"></i> Conference </a> </li>
            <li><a href="#"><i class="glyphicon glyphicon-folder-open"></i> Newsletter</a></li>
            <li><a href="#"><i class="glyphicon glyphicon-folder-open"></i> Invitations</a></li>
            <li> <a href="#"> <i class="glyphicon glyphicon-folder-open"></i> Promotions </a> </li>
          </ul>-->
        </div><!-- col-sm-3 -->