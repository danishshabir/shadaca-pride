<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?php echo base_url();?>assets/admin/js/jquery-2.0.2.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo base_url();?>assets/admin/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/accordion.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/common-script.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/admin/plugins/gallery/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.sparkline.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/sparkline-chart.js"></script> 
<script src="<?php echo base_url();?>assets/admin/js/graph.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/edit-graph.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/plugins/air-datepicker/js/datepicker.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/plugins/air-datepicker/js/datepicker.en.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script> 
<script src="<?php echo base_url();?>assets/admin/plugins/kalendar/kalendar.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/admin/plugins/kalendar/edit-kalendar.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/admin/plugins/knob/jquery.knob.min.js"></script> 
<script src="<?php echo base_url();?>assets/admin/plugins/data-tables/jquery.dataTables.js"></script> 
<script src="<?php echo base_url();?>assets/admin/plugins/data-tables/DT_bootstrap.js"></script> 
<script src="<?php echo base_url();?>assets/admin/plugins/data-tables/dynamic_table_init.js"></script>
<script src="<?php echo base_url();?>assets/admin/plugins/edit-table/edit-table.js"></script>
<script src="<?php echo base_url();?>assets/admin/plugins/validation/parsley.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/script.js?v=<?php echo rand(); ?>"></script>
<script src="<?php echo base_url();?>assets/admin/plugins/froala/froala_editor.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/admin/plugins/froala/colors.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/form-components.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.1.1/pdfobject.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script>
    $(document).ready(function() {
        $('#has_datatable').DataTable({

            "ordering": true,			"iDisplayLength": 100,			
			"bSort": (sorting == false ? false : true),
            columnDefs: [{
                orderable: false,
                targets: "no-sort"
            }]

        });
    });
</script>
<?php if($this->uri->segment(2) == "login" || $this->uri->segment(2) == "") { ?>
    <script>
        Cookies.remove('status', { path: '/pride' });
        Cookies.remove('case_type', { path: '/pride' });
        Cookies.remove('market', { path: '/pride' });
        Cookies.remove('advance_search_type', { path: '/pride' });
        Cookies.remove('advanceSearch', { path: '/pride' });
        Cookies.remove('sort', { path: '/pride' });
        Cookies.remove('column', { path: '/pride' });
    </script>
<?php } ?>
</body>
</html>