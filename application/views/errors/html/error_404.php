<div class="error-wrapper container">
<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-offset-1 col-xs-10">
	 <div class="error-container">
		<div class="error-main">
		  <div class="error-number"> Error - 404 </div>
		  <div class="error-description"> <h2 class="font-xl"><strong><i class="fa fa-fw fa-warning fa-lg text-warning"></i> Page <u>Not</u> Found</strong></h2></div>
		  <div class="error-description-mini">  </div>
		</div>
	  </div>
	
	</div>
</div>
</div>
