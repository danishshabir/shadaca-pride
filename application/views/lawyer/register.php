<div id="main-content">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h2>Register</h2>
			</div>
			<!--/col-md-12-->   
		</div>
		<!--/row-->        
		<div class="row">
			<div class="col-md-12">
				<div class="block-web">
					<div class="header">
						<div class="actions">
							<a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a>
							<a class="refresh" href="#"><i class="fa fa-repeat"></i></a> 
							<a class="close-down" href="#"><i class="fa fa-times"></i></a>
						</div>
						<h3 class="content-header">Registration Form</h3>
					</div>
					<?php echo $this->session->flashdata('message');?>    
					<div class="porlets-content">
						<form action="<?php echo current_url();?>" method="post" enctype="multipart/form-data">
							<div class="form-group col-md-4">
								<label>First name *</label>
								<input type="text" name="name"   placeholder="Enter first name" class="form-control" value="<?php echo $this->input->post('name'); ?>">
								<?php echo (form_error('name') ? form_error('name') : ''); ?>
							</div>
							<!--/form-group-->            
							<div class="form-group col-md-4">
								<label>Last name *</label>
								<input type="text" name="lname"   placeholder="Enter last name" class="form-control" value="<?php echo $this->input->post('lname'); ?>">
								<?php echo (form_error('lname') ? form_error('lname') : ''); ?>
							</div>
							<!--/form-group-->
							<div class="form-group col-md-4">
								<label>Email address *</label>
								<input type="email" name="email"   placeholder="Enter email" class="form-control" value="<?php echo $this->input->post('email'); ?>">
								<?php echo (form_error('email') ? form_error('email') : ''); ?>
							</div>
							<!--/form-group--> 
							<div class="clearfix"></div>
							<div class="form-group col-md-4">
								<label>Mobile *</label>
								<input type="text" name="phone"   placeholder="Enter mobile number" class="form-control" value="<?php echo $this->input->post('phone'); ?>">
								<?php echo (form_error('phone') ? form_error('phone') : ''); ?>
							</div>
							<!--/form-group-->                  
							<div class="form-group col-md-4" id="lawFirm">
								<label>Select law firm *</label>
								<select placeholder="Law Type" class="form-control" name="law_firms" onchange="selectLawFirm(this.value,'admin/Register/getLawFirm','');">
									<option value="" > Select law firm </option>
									<?php foreach($law_firms as $law_firm){?>
									<option id="fee" value="<?php echo $law_firm->id;?>"> <?php echo $law_firm->law_firm;?> </option>
									<?php }?>  
								</select>
								<?php echo (form_error('law_firms') ? form_error('law_firms') : ''); ?>
							</div>
							<div class="form-group col-md-4">
								<label>Website</label>
								<input type="text"  name="website"  placeholder="Enter website" class="form-control" value="<?php echo $this->input->post('website'); ?>">
								<?php echo (form_error('website') ? form_error('website') : ''); ?>
							</div>
							<div class="form-group col-md-4">
								<label>Address *</label>
								<input type="text" class="form-control" name="address"  placeholder="Lawyer address" value="<?php echo $this->input->post('address'); ?>">
								<?php echo (form_error('address') ? form_error('address') : ''); ?>
							</div>
							<div class="form-group col-md-4">
								<label>City *</label>
								<input type="text" name="city"   placeholder="Enter city name" class="form-control" value="<?php echo $this->input->post('city'); ?>">
								<?php echo (form_error('city') ? form_error('city') : ''); ?>
							</div>
							<!--/form-group-->	
							<div class="form-group col-md-2">
								<div class="form-group">
									<label>State *</label>
									<select placeholder="state" class="form-control" name="state" >
										<option value="" > Select state </option>
										<?php foreach($states_short_names as $state){?>
										<option value="<?php echo $state->id;?>"> <?php echo $state->state_short_name;?> </option>
										<?php }?>
									</select>
									<?php echo (form_error('state') ? form_error('state') : ''); ?>
								</div>
							</div>
							<div class="form-group col-md-2">
								<label>Zip *</label>
								<input type="text" name="zipcode"   placeholder="Enter zip here" class="form-control" maxlength="5" value="<?php echo $this->input->post('zipcode'); ?>">
								<?php echo (form_error('zipcode') ? form_error('zipcode') : ''); ?>
							</div>
							<div class="form-group col-md-4">
								<label>Initial setup charge *</label>
								<input type="text" name="initial_setup_charge"   placeholder="Enter Initial setup charge" class="form-control" value="<?php echo $this->input->post('initial_setup_charge'); ?>">
								<?php echo (form_error('initial_setup_charge') ? form_error('initial_setup_charge') : ''); ?>
							</div>	
							<div class="form-group col-md-4">
								<div class="form-group">
									<label>Fee Type *</label>
									<select placeholder="Fee Type" class="form-control" name="fee_type" >
										<option value="" > Select Type </option>
										<option value="0"> Monthly </option>
										<option value="1"> Annual </option>
									</select>
									<?php echo (form_error('fee_type') ? form_error('fee_type') : ''); ?>
								</div>
							</div>
							<div class="form-group col-md-4">
								<label>Fee *</label>
								<input type="text"  id="sub_cat" name="law_firm_fee"  placeholder="Fee" class="form-control" value="<?php echo $this->input->post('law_firm_fee'); ?>">
								<?php echo (form_error('law_firm_fee') ? form_error('law_firm_fee') : ''); ?>
							</div>
							<?php 
							$post_case_types = ($this->input->post('case_type') ? $this->input->post('case_type') : array());
							$post_jurisdictions_array = ($this->input->post('jurisdiction') ? $this->input->post('jurisdiction') : array());
							?>
							<div class="form-group col-md-12 selection_boxes" style="padding:20px 15px;">
								<label>Case type *</label><br />
								<?php $case_type_chunks = array_chunk($case_type, count($case_type)/3); ?>
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-12">
										<?php foreach($case_type_chunks[0] as $c_type) { ?>
										<label class="checkbox-inline labelFullED">
										<input type="checkbox" value="<?php echo $c_type->type;?>" name="case_type[]" id="inlinecheckbox1" <?php if(in_array($c_type->type,$post_case_types)) echo "checked"; ?>>
										<span class="custom-checkbox"></span> <?php echo $c_type->type;?>
										</label>
										<?php } ?>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-12">
										<?php foreach($case_type_chunks[1] as $c_type) { ?>
										<label class="checkbox-inline labelFullED">
										<input type="checkbox" value="<?php echo $c_type->type;?>" name="case_type[]" id="inlinecheckbox1" <?php if(in_array($c_type->type,$post_case_types)) echo "checked"; ?>>
										<span class="custom-checkbox"></span> <?php echo $c_type->type;?>
										</label>
										<?php } ?>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-12">
										<?php foreach($case_type_chunks[2] as $c_type) { ?>
										<label class="checkbox-inline labelFullED">
										<input type="checkbox" value="<?php echo $c_type->type;?>" name="case_type[]" id="inlinecheckbox1" <?php if(in_array($c_type->type,$post_case_types)) echo "checked"; ?>>
										<span class="custom-checkbox"></span> <?php echo $c_type->type;?>
										</label>
										<?php }
										foreach($case_type_chunks[3] as $c_type) { ?>
										<label class="checkbox-inline labelFullED">
										<input type="checkbox" value="<?php echo $c_type->type;?>" name="case_type[]" id="inlinecheckbox1" <?php if(in_array($c_type->type,$post_case_types)) echo "checked"; ?>>
										<span class="custom-checkbox"></span> <?php echo $c_type->type;?>
										</label>
										<?php } ?>
									</div>
								</div>
								<?php echo (form_error('case_type[]') ? form_error('case_type[]') : ''); ?>
								<div class="clearfix"></div>
							</div>
							<div class="form-group col-md-12 selection_boxes" style="padding:20px 15px;">
								<label>Jurisdiction *</label>
								<span class="button-checkbox">
								<button type="button" class="btn" data-color="primary">Selection</button>
								<input type="checkbox" class="hidden" />
								</span>
								<br />
								<?php $jurisdiction_chunks = array_chunk($jurisdictions, count($jurisdictions)/3); ?>
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-12">
										<?php foreach($jurisdiction_chunks[0] as $jurisdiction) { ?>
										<label class="checkbox-inline labelFullED">
										<input type="checkbox" value="<?php echo $jurisdiction->name;?>" name="jurisdiction[]" id="inlinecheckbox1" <?php if(in_array($jurisdiction->name,$post_jurisdictions_array)) echo "checked"; ?>>
										<span class="custom-checkbox"></span> <?php echo $jurisdiction->name;?> </label>
										<?php } ?>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-12">
										<?php foreach($jurisdiction_chunks[1] as $jurisdiction) { ?>
										<label class="checkbox-inline labelFullED">
										<input type="checkbox" value="<?php echo $jurisdiction->name;?>" name="jurisdiction[]" id="inlinecheckbox1" <?php if(in_array($jurisdiction->name,$post_jurisdictions_array)) echo "checked"; ?>>
										<span class="custom-checkbox"></span> <?php echo $jurisdiction->name;?> </label>
										<?php } ?>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-12">
										<?php foreach($jurisdiction_chunks[2] as $jurisdiction) { ?>
										<label class="checkbox-inline labelFullED">
										<input type="checkbox" value="<?php echo $jurisdiction->name;?>" name="jurisdiction[]" id="inlinecheckbox1" <?php if(in_array($jurisdiction->name,$post_jurisdictions_array)) echo "checked"; ?>>
										<span class="custom-checkbox"></span> <?php echo $jurisdiction->name;?> </label>
										<?php }
										foreach($jurisdiction_chunks[3] as $jurisdiction) { ?>
										<label class="checkbox-inline labelFullED">
										<input type="checkbox" value="<?php echo $jurisdiction->name;?>" name="jurisdiction[]" id="inlinecheckbox1" <?php if(in_array($jurisdiction->name,$post_jurisdictions_array)) echo "checked"; ?>>
										<span class="custom-checkbox"></span> <?php echo $jurisdiction->name;?> </label>
										<?php } ?>
									</div>
								</div>
								<?php echo (form_error('jurisdiction[]') ? form_error('jurisdiction[]') : ''); ?>
							</div>
							<div class="clearfix"></div>
							<div class="form-group col-md-4">
								<label>Profile Image</label>
								<input type="file" name="image"  class="form-control" style="padding: 0 12px;"/>
								<?php echo (form_error('image') ? form_error('image') : ''); ?>
							</div>
							<input type="hidden" name="password" value="<?php echo generateRandomPassword(6); ?>" />
							<div class="form-group col-md-12"> 
								<button class="btn btn-primary" type="submit">Submit</button>      
								<a href="<?php echo base_url();?>admin/login" class="btn btn-default">Cancel</a>         
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
					<!--/porlets-content-->        
				</div>
				<!--/block-web-->      
			</div>
			<!--/col-md-6-->            
		</div>
		<!--/row-->         
	</div>
	<!--/page-content end-->  
</div>
<script>    
	$(function () {
	
		$('.button-checkbox')
	
			.each(function () {
	
				// Settings
	
				var $widget = $(this)
	
					, $button = $widget.find('button')
	
					, $checkbox = $widget.find('input:checkbox')
	
					, color = $button.data('color')
	
					, settings = {
	
						on: {
	
							icon: 'glyphicon glyphicon-check'
	
						}
	
						, off: {
	
							icon: 'glyphicon glyphicon-unchecked'
	
						}
	
					};
	
				// Event Handlers
	
				$button.on('click', function () {
	
					$checkbox.prop('checked', !$checkbox.is(':checked'));
	
					$checkbox.triggerHandler('change');
	
				});
	
				$checkbox.on('change', function () {
	
					updateDisplay();
	
				});
	
				// Actions
	
				function updateDisplay() {
	
					var isChecked = $checkbox.is(':checked');
	
					$button.data('state', (isChecked) ? "on" : "off");
	
					$button.find('.state-icon')
	
						.removeClass()
	
						.addClass('state-icon ' + settings[$button.data('state')].icon);
	
					if (isChecked) {
	
						$button.removeClass('btn-default').addClass('btn-' + color + ' active');
	
						$('input[name="jurisdiction[]"]').prop('checked', true);
	
					} else {
	
						$button.removeClass('btn-' + color + ' active').addClass('btn-default');
	
						$('input[name="jurisdiction[]"]').prop('checked', false);
	
					}
	
				}
	
				// Initialization
	
				function init() {
	
					var isChecked = $checkbox.is(':checked');
	
					$button.data('state', ($('input[name="jurisdiction[]"]:checked').length == $('input[name="jurisdiction[]"]').length ? "on" : "off"));
	
					$button.find('.state-icon').removeClass().addClass('state-icon ' + settings[$button.data('state')].icon);
	
					if($('input[name="jurisdiction[]"]:checked').length == $('input[name="jurisdiction[]"]').length)
	
					{
	
						$button.removeClass('btn-default').addClass('btn-' + color + ' active');
	
						$checkbox.prop('checked', true);
	
					} else {
	
						$button.removeClass('btn-' + color + ' active').addClass('btn-default');
	
						$checkbox.prop('checked', false);
	
					}
	
					if ($button.find('.state-icon').length == 0) {
	
						$button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
	
					}
	
				}
	
				init();
	
			});
	
			$("[name='law_firms']").val('<?php echo ($this->input->post('law_firms') ? $this->input->post('law_firms') : $this->session->userdata('lawfirm_id')) ?>');
			$("[name='state']").val('<?php echo ($this->input->post('state')) ?>');
			$("[name='fee_type']").val('<?php echo ($this->input->post('fee_type')) ?>');
	
	});
	function selectLawFirm(id, actionUrl, reloadUrl) {   
		if (id == '') {        
			$("#sub_cat").val('');       
			return true;     
		}        
		$.post( "<?php echo base_url();?>"+actionUrl,{ id: id }, function( data ) {		
			$('#sub_cat').val(data);	
		});  
	}
</script>