<?php 
session_start(); 
class Backup_Database {
    
    public function __construct($host = "", $username = "", $password = "", $database = "", $charset = "")
    {
        $this->host     = $host;
        $this->username = $username;
        $this->passwd   = $passwd;
        $this->dbName   = $database;
        $this->charset  = $charset;
 
        $this->initializeDatabase();
    }
 
    protected function initializeDatabase()
    {
        $conn = mysqli_connect($this->host, $this->username, $this->passwd, $this->dbName);
        if (!mysqli_set_charset($conn,"utf8"))
        {
            mysqli_query('SET NAMES '.$this->charset);
        }
    }
 
 	public function create_zip($files = array(),$destination = '',$overwrite = false) {
			//if the zip file already exists and overwrite is false, return false
			if(file_exists($destination) && !$overwrite) { return false; }
			//vars
			$valid_files = array();
			//if files were passed in...
			if(is_array($files)) {
				//cycle through each file
				foreach($files as $file) {
					//make sure the file exists
					if(file_exists($file)) {
						$valid_files[] = $file;
					}
				}
			}
			//if we have good files...
			if(count($valid_files)) {
				//create the archive
				$zip = new ZipArchive();
				if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
					return false;
				}
				//add the files
				foreach($valid_files as $file) {
					$zip->addFile($file,$file);
				}

				//close the zip -- done!
				$zip->close();

				//check to make sure the file exists
				return file_exists($destination);
			}
			else
			{
				return false;
			}
		}
    public function backupTables($tables, $host, $username, $password, $database, $outputDir)
    {
        try
        {
            /**
            * Tables to export
            */
            if($tables == '*')
            {
                $tables = array();
				$conn = mysqli_connect($host, $username, $password, $database);
                $result = mysqli_query($conn, 'SHOW TABLES');
                while($row = mysqli_fetch_row($result))
                {
                    $tables[] = $row[0];
                }
            }
            else
            {
                $tables = is_array($tables) ? $tables : explode(',',$tables);
            }
 
			$sql = '';
 
            /**
            * Iterate tables
            */
				
            foreach($tables as $table)
            {
                $result = mysqli_query($conn, "SELECT * FROM $table");
                $numFields = mysqli_num_fields($result);
 
				$result2 = mysqli_query($conn, 'SHOW CREATE TABLE `'.$table . "`");
                $row2 = mysqli_fetch_array($result2);
                $sql .= 'DROP TABLE IF EXISTS `'.$table.'`;';
                $sql.= "\n\n".$row2[1].";\n\n";
 
                for ($i = 0; $i < $numFields; $i++)
                {
                    while($row = mysqli_fetch_row($result))
                    {
                        $sql .= 'INSERT INTO `'.$table.'` VALUES(';
                        for($j=0; $j<$numFields; $j++)
                        {
                            $row[$j] = addslashes($row[$j]);
                            $row[$j] = preg_replace('#\n#','#\\n#',$row[$j]);
                            if (isset($row[$j]))
                            {
                                $sql .= '"'.$row[$j].'"' ;
                            }
                            else
                            {
                                $sql.= '""';
                            }
 
                            if ($j < ($numFields-1))
                            {
                                $sql .= ',';
                            }
                        }
 
                        $sql.= ");\n";
                    }
                }
 
                $sql.="\n\n\n";
 
            }
        }
        catch (Exception $e)
        {
            var_dump($e->getMessage());
            return false;
        }
 
        return $this->saveFile($sql, $outputDir);
    }
 
    /**
     * Save SQL to file
     * @param string $sql
     */
    protected function saveFile(&$sql, $outputDir)
    {
        if (!$sql) return false;
 
        try
        {
			$fileName =$this->dbName.'-db-backup-'.date("Y-m-d").'.sql';
			 $handle = fopen($outputDir.'/'.$fileName ,'w+');
            fwrite($handle, $sql);
        
			
			$fp = fopen($outputDir.'/'."$fileName", "r");
			//fpassthru($fp);

			fclose($handle);
			$files_to_zip = array(
				$outputDir.'/'."$fileName",
			);
			// Calling save zip function
			$this->create_zip($files_to_zip,$outputDir . '/'. $this->dbName.'-db-backup-' . time() . '.zip');
			unlink( $outputDir.'/'."$fileName" );
        }
        catch (Exception $e)
        {
            var_dump($e->getMessage());
            return false;
        }
        return true;
	}
	
}
?>