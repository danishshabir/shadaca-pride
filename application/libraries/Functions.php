<?php
ob_start(); if (!defined('BASEPATH')) exit('No direct script access allowed');

require 'auth/vendor/autoload.php';
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
define("AUTHORIZENET_API_LOGIN_ID", "95B88Kyz6r");
define("AUTHORIZENET_TRANSACTION_KEY", "587QQzmWaG355v4P");
define("AUTHORIZENET_SANDBOX", false);
define("AUTHORIZENET_URL", (AUTHORIZENET_SANDBOX ? 'https://apitest.authorize.net' : 'https://api2.authorize.net'));

class Functions
{
	var $CI;
    public function __construct()
    {
		$this->CI =& get_instance();
		$this->CI->load->helper('url');
		$this->CI->config->item('base_url');
    }
	public function return_bytes($val) 
	{
		$val = trim($val);
		$last = strtolower($val[strlen($val)-1]);
		switch($last) 
		{
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}
		return $val;
	}
    public function uploadFilesize()
    {
        return $this->return_bytes(ini_get('upload_max_filesize'));
    }
	public function clean($string)
	{
		$string = str_replace(' ', '', $string); 
		$string = preg_replace('/[^A-Za-z.0-9\-]/', '', $string);
		return $string;
	}
	public function mres($var) 
	{
		if (get_magic_quotes_gpc()) 
		{
			$var = stripslashes(trim($var));	
		}
		return $var;
	}
	public function xssClean($data) 
	{
		return htmlspecialchars($data, ENT_QUOTES, 'UTF-8');	
	}
	public function createCustomerProfile($array)
    {
		$response_array = array();
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(AUTHORIZENET_API_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(AUTHORIZENET_TRANSACTION_KEY);
        $refId      = 'ref' . time();
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($array['card_number']);
        $creditCard->setExpirationDate($array['expiry']);
        $paymentCreditCard = new AnetAPI\PaymentType();
        $paymentCreditCard->setCreditCard($creditCard);
        $billto = new AnetAPI\CustomerAddressType();
        $billto->setFirstName($array['first_name']);
        $billto->setLastName($array['last_name']);
        $billto->setCompany($array['company']);
        $billto->setAddress($array['billing_address']);
        $billto->setCity($array['city']);
        $billto->setState($array['state']);
        $billto->setZip($array['zip']);
        $billto->setCountry("USA");
        $paymentprofile = new AnetAPI\CustomerPaymentProfileType();
        $paymentprofile->setCustomerType('business');
        $paymentprofile->setBillTo($billto);
        $paymentprofile->setPayment($paymentCreditCard);
        $paymentprofiles[] = $paymentprofile;
        $customerprofile   = new AnetAPI\CustomerProfileType();
        $customerprofile->setDescription($array['law_firm']);
        $customerprofile->setMerchantCustomerId($array['customer_id']);
        $customerprofile->setEmail($array['contact_email']);
        $customerprofile->setPaymentProfiles($paymentprofiles);
        $request = new AnetAPI\CreateCustomerProfileRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setProfile($customerprofile);
        $controller = new AnetController\CreateCustomerProfileController($request);
        $response   = $controller->executeWithApiResponse(AUTHORIZENET_URL);
        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            $paymentProfiles = $response->getCustomerPaymentProfileIdList();
			$response_array['success'] = 1;
			$response_array['profile_id'] = $paymentProfiles[0];
			$response_array['customer_profile_id'] = $response->getCustomerProfileId();
        } //($response != null) && ($response->getMessages()->getResultCode() == "Ok")
        else {
            $errorMessages = $response->getMessages()->getMessage();
			$response_array['success'] = 0;
			$response_array['code'] = $errorMessages[0]->getCode();
			$response_array['error'] = $errorMessages[0]->getText();
        }
        return $response_array;
    }
	
	public function getCustomerProfile($customerProfileId,$customerPaymentProfileId)
	{
		$response_array = array();
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(AUTHORIZENET_API_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(AUTHORIZENET_TRANSACTION_KEY);
		$refId = 'ref' . time();
		$request = new AnetAPI\GetCustomerPaymentProfileRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId( $refId);
		$request->setCustomerProfileId($customerProfileId);
		$request->setCustomerPaymentProfileId($customerPaymentProfileId);
		$controller = new AnetController\GetCustomerPaymentProfileController($request);
		$response = $controller->executeWithApiResponse(AUTHORIZENET_URL);
		if(($response != null)){
			if ($response->getMessages()->getResultCode() == "Ok")
			{
				$response_array['success'] = 1;
				$response_array['customerPaymentProfileId'] = $response->getPaymentProfile()->getCustomerPaymentProfileId();
				$response_array['address'] = $response->getPaymentProfile()->getbillTo()->getAddress();
				$response_array['card_number'] = $response->getPaymentProfile()->getPayment()->getCreditCard()->getCardNumber();
				$response_array['card_type'] = $response->getPaymentProfile()->getPayment()->getCreditCard()->getCardType();
				$response_array['expiry'] = $response->getPaymentProfile()->getPayment()->getCreditCard()->getExpirationDate();
				$response_array['first_name'] = $response->getPaymentProfile()->getbillTo()->getFirstName();
				$response_array['last_name'] = $response->getPaymentProfile()->getbillTo()->getLastName();
				$response_array['phone_number'] = $response->getPaymentProfile()->getbillTo()->getPhoneNumber();
				$response_array['email'] = $response->getPaymentProfile()->getbillTo()->getEmail();
				$response_array['company'] = $response->getPaymentProfile()->getbillTo()->getCompany();
				$response_array['city'] = $response->getPaymentProfile()->getbillTo()->getCity();
				$response_array['state'] = $response->getPaymentProfile()->getbillTo()->getState();
				$response_array['zip'] = $response->getPaymentProfile()->getbillTo()->getZip();
				$response_array['country'] = $response->getPaymentProfile()->getbillTo()->getCountry();
				if($response->getPaymentProfile()->getSubscriptionIds() != null) 
				{
					if($response->getPaymentProfile()->getSubscriptionIds() != null)
					{
						foreach($response->getPaymentProfile()->getSubscriptionIds() as $subscriptionid)
							$subscriptionid;
					}
				}
			}
			else
			{
				$response_array['success'] = 0;
				$errorMessages = $response->getMessages()->getMessage();
				$response_array['code'] = $errorMessages[0]->getCode();
				$response_array['error'] = $errorMessages[0]->getText();
			}
		}
		else{
			$response_array['success'] = 0;
			$response_array['error'] = "NULL Response Error";
		}
		
		return $response_array;
	}
	
	public function deleteCustomerPaymentProfile($customerProfileId,$customerPaymentProfileId)
	{
		$response_array = array();
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(AUTHORIZENET_API_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(AUTHORIZENET_TRANSACTION_KEY);
		$refId = 'ref' . time();
		$request = new AnetAPI\DeleteCustomerProfileRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setCustomerProfileId( $customerProfileId );
		$controller = new AnetController\DeleteCustomerProfileController($request);
		$response = $controller->executeWithApiResponse(AUTHORIZENET_URL);
		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
		{
			$response_array['success'] = 1;
			$response_array['msg'] = "SUCCESS: Delete Customer Payment Profile  SUCCESS  :";
		}
		else
		{
			$response_array['success'] = 0;
			$errorMessages = $response->getMessages()->getMessage();
			$response_array['code'] = $errorMessages[0]->getCode();
			$response_array['error'] = $errorMessages[0]->getText();
		}
		
		return $response_array;
	}
	
	public function chargeCustomerProfile($customer_profile, $data)
	{
		$response_array = array();
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(AUTHORIZENET_API_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(AUTHORIZENET_TRANSACTION_KEY);
        $refId = 'ref' . time();
		$profileToCharge = new AnetAPI\CustomerProfilePaymentType();
		$profileToCharge->setCustomerProfileId($customer_profile->customer_profile_id);
		$paymentProfile = new AnetAPI\PaymentProfileType();
		$paymentProfile->setPaymentProfileId($customer_profile->profile_id);
		$profileToCharge->setPaymentProfile($paymentProfile);
        $transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction"); 
		$transactionRequestType->setAmount($data['amount']);
		$transactionRequestType->setProfile($profileToCharge);
        $request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId( $refId);
		$request->setTransactionRequest( $transactionRequestType);
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(AUTHORIZENET_URL);
		if ($response != null) {
            if ($response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK) {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getMessages() != null) {
                    if ($tresponse->getResponseCode() == '1') {
						$response_array['success'] = 1;
						$response_array['msg'] = $tresponse->getMessages()[0]->getDescription();
						$response_array['AUTH'] = $tresponse->getAuthCode();
						$response_array['response_Code'] = $tresponse->getResponseCode();
						$data['transaction_id'] = $tresponse->getTransId();
						$response_array['code'] = $tresponse->getMessages()[0]->getCode();
						$this->CI->load->model('Reporting_model');
                        $Transaction = $this->CI->Reporting_model->insertTransactionRecord($data);
                    } //$tresponse->getResponseCode() == '1'
                } //$tresponse != null && $tresponse->getMessages() != null
                else {
                    if ($tresponse->getErrors() != null) {
                        $response_array['success'] = 0;
						$response_array['code'] = $tresponse->getErrors()[0]->getErrorCode();
						$response_array['error'] = $tresponse->getErrors()[0]->getErrorText();
                    } //$tresponse->getErrors() != null
                }
            } //$response->getMessages()->getResultCode() == \SampleCode\Constants::RESPONSE_OK
            else {
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $response_array['success'] = 0;
					$response_array['code'] = $tresponse->getErrors()[0]->getErrorCode();
					$response_array['error'] = $tresponse->getErrors()[0]->getErrorText();
                } //$tresponse != null && $tresponse->getErrors() != null
                else {
                    $response_array['success'] = 0;
					$response_array['code'] = $response->getMessages()->getMessage()[0]->getCode();
					$response_array['error'] = $response->getMessages()->getMessage()[0]->getText();
                }
            }
        } //$response != null
        else {
			$response_array['success'] = 0;
            $response_array['error'] = "No response returned";
        }
		
		return $response_array;
	}
}
?> 