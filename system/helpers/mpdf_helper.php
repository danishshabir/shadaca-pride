<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function mpdf_create($html, $filename='')
{
    require_once("mpdf/mpdf.php");

    $mpdf=new mPDF('');

    $mpdf->autoScriptToLang = true;
    $mpdf->baseScript = 1;	// Use values in classes/ucdn.php  1 = LATIN
    $mpdf->autoVietnamese = true;
    $mpdf->autoArabic = true;

    $mpdf->autoLangToFont = true;

    $mpdf->WriteHTML($html);

    $mpdf->Output($filename,'D');
}

function generate_mpdf($html, $filename='')
{
    require_once("mpdf/mpdf.php");

    $mpdf=new mPDF('');

    $mpdf->autoScriptToLang = true;
    $mpdf->baseScript = 1;	// Use values in classes/ucdn.php  1 = LATIN
    $mpdf->autoVietnamese = true;
    $mpdf->autoArabic = true;

    $mpdf->autoLangToFont = true;

    $mpdf->WriteHTML($html);

    $mpdf->Output('uploads/pdf/'.$filename, 'F');

    //file_put_contents('uploads/pdf/'.$filename, $pdf);

}
?>